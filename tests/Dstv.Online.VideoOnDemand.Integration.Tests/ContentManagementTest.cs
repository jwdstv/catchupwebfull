﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Dstv.Online.VideoOnDemand.Content;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Integration.Cms;
using Dstv.Online.VideoOnDemand.Integration.Mms;
using DstvoConnectWrapper;

namespace Dstv.Online.VideoOnDemand.Integration.Tests
{

    /// <summary>
    ///This is a test class for ContentManagementTest and is intended
    ///to contain all ContentManagementTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ContentManagementTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetPlaylist
        ///</summary>
        [TestMethod()]
        public void GetPlaylistTest()
        {
            ContentManagement target = new ContentManagement();
            PlaylistIdentifier playlistId = new PlaylistIdentifier("9");

            IPlaylist response = target.GetPlaylist(playlistId);

            response.WriteXmlToConsole();

            Assert.IsNotNull(response);
            Assert.AreNotEqual(0, response.CatalogueItemIds.Length, "No items returned");
        }

        [TestMethod()]
        public void ResolvePlaylistTest()
        {
            string validPlaylistId = "9";
            IPlaylist playlist;
            {
                ContentManagement target = new ContentManagement();
                playlist = target.GetPlaylist(new PlaylistIdentifier(validPlaylistId));

                //playlist.WriteXmlToConsole();

                foreach (CatalogueItemIdentifier thisItem in playlist.CatalogueItemIds)
                {
                    Console.WriteLine(thisItem);
                }
                Assert.IsNotNull(playlist);
                Assert.AreNotEqual(0, playlist.CatalogueItemIds.Length, "No items returned");
            }
            Console.WriteLine();
            {
                Catalogue target = new Catalogue();
                CatalogueCriteria criteria = new CatalogueCriteria();
                criteria.CatalogueItemIds = playlist.CatalogueItemIds;

                criteria.Paging.PageNumber = 1;
                criteria.Paging.ItemsPerPage = playlist.CatalogueItemIds.Length + 1;
                criteria.SortOrder = SortOrders.Ordinal;

                CatalogueResult result = target.GetCatalogueItems(criteria);

                Assert.IsNotNull(result);

                //result.WriteXmlToConsole();

                foreach (ICatalogueItem thisItem in result.CatalogueItems)
                {
                    Console.WriteLine(thisItem.CatalogueItemId);
                }

                Assert.AreNotEqual(0, result.CatalogueItems.Length, "Not all playlist items returned"); // Can not check for exact number because items expire

                int currentOrdinal = 0;
                bool isOrdinal = true;
                foreach (ICatalogueItem thisItem in result.CatalogueItems)
                {
                    bool thisItemFound = false;
                    for (int i = currentOrdinal; i <= criteria.CatalogueItemIds.Length - 1; i++)
                    {
                        if (thisItem.CatalogueItemId.Equals(criteria.CatalogueItemIds[i]))
                        {
                            thisItemFound = true;
                            currentOrdinal = i;
                            break;
                        }
                    }
                    if (!thisItemFound)
                    {
                        isOrdinal = false;
                        break;
                    }
                }

                Assert.AreEqual(true, isOrdinal, "Ordinality does not match");
            }
        }

        [TestMethod()]
        public void IsPremiumSmartCardTest()
        {

            ConnectWrapper wrapper = new ConnectWrapper();
            ConnectRemoteImpl.Services.IBSCustomerAccountDetails AD = new ConnectRemoteImpl.Services.IBSCustomerAccountDetails(wrapper.GetConnectIDByEMailAddress("avi@shafrir.org"));
            //ConnectRemoteImpl.ClientUserCredentials UC =  wrapper.LoginUser("walsh@mweb.co.za", "123456", 0, "ondemand.dstv.com");
            
            string SmartCardNumber  = wrapper.RequestProfileEntryByConnectID(AD.ConnectId, 7);

            bool isPremium = wrapper.isPremiumSubscriberBySmartCardNumber(SmartCardNumber);
                Assert.AreEqual(true,isPremium);


            //AD = new ConnectRemoteImpl.Services.IBSCustomerAccountDetails(wrapper.GetConnectIDByEMailAddress("avi@shafrir.org"));

            //AD = new ConnectRemoteImpl.Services.IBSCustomerAccountDetails(wrapper.GetConnectIDByEMailAddress("plvs@mweb.co.za"));
            

            //wrapper.isSubscriberBySmartCardNumber(smartCardNumber);


            //    Assert.AreEqual(true, isOrdinal, "Ordinality does not match");
            
        }
    }
}
