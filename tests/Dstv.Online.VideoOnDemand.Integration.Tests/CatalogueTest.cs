﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using System.Xml;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Integration.Mms;

namespace Dstv.Online.VideoOnDemand.Integration.Tests
{
    /// <summary>
    ///This is a test class for CatalogueTest and is intended
    ///to contain all CatalogueTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CatalogueTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        private const string validCatalogueItemId = "10";

        [TestMethod()]
        public void GetCatalogueItemTest()
        {
            Catalogue target = new Catalogue();
            CatalogueItemIdentifier catalogueItemId = new CatalogueItemIdentifier(validCatalogueItemId); //videoId
            bool getRelatedMedia = false;
            CatalogueResult result = target.GetCatalogueItem(catalogueItemId, getRelatedMedia);

            result.WriteXmlToConsole();

            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(0, result.TotalResultCount);
        }


        [TestMethod()]
        public void GetCatalogueItemCrossingOverTest()
        {
            Catalogue target = new Catalogue();
            CatalogueItemIdentifier catalogueItemId = new CatalogueItemIdentifier("1057"); //videoId
            bool getRelatedMedia = false;
            CatalogueResult result = target.GetCatalogueItem(catalogueItemId, getRelatedMedia);

            result.WriteXmlToConsole();

            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(0, result.TotalResultCount);
        }



        /// <summary>
        /// Gets the related box office catalogue item test.
        /// </summary>
        [TestMethod()]
        public void GetCatalogueRelatedItemTest()
        {
            Catalogue target = new Catalogue();
            CatalogueItemIdentifier catalogueItemId = new CatalogueItemIdentifier(validCatalogueItemId); //videoId
            bool getRelatedMedia = true;
            CatalogueResult result = target.GetCatalogueItem(catalogueItemId, getRelatedMedia);

            Console.WriteLine("{0} items found", result.CatalogueItems.Length);

            //result.WriteXmlToConsole();


            Assert.AreNotEqual(null, result);
            Assert.AreNotEqual(0, result.TotalResultCount, "No items returned");
            Assert.AreNotEqual(1, result.TotalResultCount, "Only one item returned");

            bool rootItemFound = false;
            int programId = result.CatalogueItems[0].ProgramId.Value;
            bool isConsistentProgramId = true;

            foreach (ICatalogueItem thisItem in result.CatalogueItems)
            {
                if (thisItem.CatalogueItemId.Equals(catalogueItemId))
                {
                    rootItemFound = true;
                }
                if (thisItem.ProgramId != programId)
                {
                    isConsistentProgramId = false;
                }
            }
            Assert.AreEqual(true, rootItemFound, "Searched item should be included in result set");
            Assert.AreEqual(true, isConsistentProgramId, "Not all items have the same program id");
        }

        ///// <summary>
        /////A test for GetCatalogueItemCastAndCrew

        /////</summary>
        //[TestMethod()]
        //public void GetCatalogueItemCastAndCrewTest()
        //{
        //    Catalogue target = new Catalogue();
        //    CatalogueItemIdentifier catalogueItemId = new CatalogueItemIdentifier(validCatalogueItemId);
        //    ICastMember[] result = target.GetCatalogueItemCastAndCrew(catalogueItemId);

        //    Assert.IsNotNull(result);

        //    result.WriteXmlToConsole();

        //    Assert.AreNotEqual(0, result.Length, "No cast and crew returned");
        //}

        /// <summary>
        ///A test for GetCatalogueItemMedia
        ///</summary>
        [TestMethod()]
        public void GetCatalogueItemMediaTest()
        {
            Catalogue target = new Catalogue();
            CatalogueItemIdentifier catalogueItemId = new CatalogueItemIdentifier(validCatalogueItemId);

            int mediaType = (int) MmsVideoFileTypes.WmVideo1000K;

            MediaTypeIdentifier mediaTypeId = new MediaTypeIdentifier(mediaType.ToString()); 
            ICatalogueItemMedia result = target.GetCatalogueItemMedia(catalogueItemId, mediaTypeId);
 
            Assert.IsNotNull(result);

            result.WriteXmlToConsole();
        }


        /// <summary>
        ///A test for GetClassificationItems
        ///</summary>
        [TestMethod()]
        public void GetRootClassificationItemsTest()
        {
            Catalogue target = new Catalogue();
            ClassificationCriteria criteria = new ClassificationCriteria();
            IClassificationItem[] items = target.GetClassificationItems(criteria);

            Assert.IsNotNull(items);

            items.WriteXmlToConsole();

            Assert.AreNotEqual(0, items.Length, "No category items returned");
            Assert.AreEqual(2, items.Length, "Unexpected number of category items returned (expected box office, catch up)");
        }

        [TestMethod()]
        public void GetBoxOfficeClassificationItemsTest()
        {
            Catalogue target = new Catalogue();
            ClassificationCriteria criteria = new ClassificationCriteria();
            //criteria.ClassificationItemId = new ClassificationItemIdentifier("2");
            criteria.ProductTypeId = new Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier(ProductTypes.BoxOffice);
            IClassificationItem[] items = target.GetClassificationItems(criteria);

            Assert.IsNotNull(items);

            items.WriteXmlToConsole();

            Assert.AreNotEqual(0, items.Length, "No category items returned");
            Assert.AreEqual(1, items.Length, "No category items returned");
            Assert.AreEqual("Movies", items[0].Name, "Unexcpected genre");
        }

        [TestMethod()]
        public void GetBoxOfficeMovieClassificationItemsTest()
        {
            Catalogue target = new Catalogue();
            ClassificationCriteria criteria = new ClassificationCriteria();
            criteria.ClassificationItemId = new ClassificationItemIdentifier("2");
            criteria.ProductTypeId = new Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier(ProductTypes.BoxOffice);
            IClassificationItem[] items = target.GetClassificationItems(criteria);

            Assert.IsNotNull(items);

            items.WriteXmlToConsole();

            Assert.AreNotEqual(0, items.Length, "No category items returned");
            Assert.AreEqual(8, items.Length, "Unexpected number category items returned");
            Assert.AreEqual("Animation", items[0].Name, "Unexpected genre");
        }


        [TestMethod()]
        public void GetExpiredCatalogueItemByIdTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();

            criteria.CatalogueItemIds = new CatalogueItemIdentifier[] { new CatalogueItemIdentifier("5310") };

            criteria.Paging.PageNumber = 1;
            criteria.Paging.ItemsPerPage = 2;
            criteria.SortOrder = SortOrders.ProgramTitleAsc;

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            result.WriteXmlToConsole();

            Assert.AreEqual(0, result.CatalogueItems.Length, "One item requested");
        }

        [TestMethod()]
        public void GetCatalogueItemByIdTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();

            criteria.CatalogueItemIds = new CatalogueItemIdentifier[] { new CatalogueItemIdentifier(validCatalogueItemId) };

            criteria.Paging.PageNumber = 1;
            criteria.Paging.ItemsPerPage = 2;
            criteria.SortOrder = SortOrders.ProgramTitleAsc;

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            result.WriteXmlToConsole();

            Assert.AreEqual(1, result.CatalogueItems.Length, "One item requested");
            Assert.AreEqual(validCatalogueItemId, result.CatalogueItems[0].CatalogueItemId.Value, "Incorrect item returned");
        }

        [TestMethod()]
        public void GetCatalogueItemByIdsTest()
        {
            string secondItemId = "10";
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();

            criteria.CatalogueItemIds = new CatalogueItemIdentifier[] { new CatalogueItemIdentifier(validCatalogueItemId), new CatalogueItemIdentifier(secondItemId) };

            criteria.Paging.PageNumber = 1;
            criteria.Paging.ItemsPerPage = 3;
            criteria.SortOrder = SortOrders.ProgramTitleAsc;

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            result.WriteXmlToConsole();

            //Assert.AreEqual(2, result.CatalogueItems.Length, "Two items requested"); What if there is only one record returned?
            Assert.AreEqual(validCatalogueItemId, result.CatalogueItems[0].CatalogueItemId.Value, "Incorrect item returned - verify ordinality");
            Assert.AreEqual(secondItemId, result.CatalogueItems[0].CatalogueItemId.Value, "Incorrect item returned - verify ordinality");
        }

        [TestMethod()]
        public void GetCatalogueItemForShowByProgramTitleTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();

            criteria.Paging.PageNumber = 1;
            criteria.Paging.ItemsPerPage = 10;
            criteria.SortOrder = SortOrders.ByShowProgramTitleAsc;

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            foreach (ICatalogueItem thisItem in result.CatalogueItems)
            {
                Console.WriteLine("{0}; Episode: {1}", thisItem.ProgramTitle, thisItem.VideoTitle);
            }

            Assert.AreNotEqual(0, result.CatalogueItems.Length, "No shows returned");
         
        }


        [TestMethod()]
        public void GetCatalogueItemForShowByVideoStartDateTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();

            criteria.Paging.PageNumber = 1;
            criteria.Paging.ItemsPerPage = 10;
            criteria.SortOrder = SortOrders.ByShowVideoStartDateDesc;

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            foreach (ICatalogueItem thisItem in result.CatalogueItems)
            {
                Console.WriteLine("{0}; Episode: {1}; Aired: {2}", thisItem.ProgramTitle, thisItem.VideoTitle, thisItem.Airdate);
            }

            Assert.AreNotEqual(0, result.CatalogueItems.Length, "No shows returned");

        }
        //ProgramTitleDesc

        [TestMethod(), ExpectedException(typeof(UnsupportedSortOrderException))]
        public void GetBoxOfficeCatalogueItemsInvalidSortOrderTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();
            criteria.Paging.ItemsPerPage = 10;
            criteria.Paging.PageNumber = 1;
            criteria.SortOrder = SortOrders.ProgramTitleDesc;
            criteria.ClassificationCriteria.ProductTypeId = new Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier(ProductTypes.BoxOffice);

            CatalogueResult result = target.GetCatalogueItems(criteria);
        }


        /// <summary>
        ///A test for GetCatalogueItems
        ///</summary>
        [TestMethod()]
        public void GetBoxOfficeCatalogueItemsTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();
            criteria.Paging.ItemsPerPage = 10;
            criteria.Paging.PageNumber = 1;
            criteria.SortOrder = SortOrders.ProgramTitleAsc;
            criteria.ClassificationCriteria.ProductTypeId = new Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier(ProductTypes.BoxOffice);

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            Console.WriteLine(result.TotalResultCount);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0} - {1}", result.CatalogueItems[i].ProgramTitle, result.CatalogueItems[i].VideoTitle);
            }

            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(ProductTypes.BoxOffice, result.CatalogueItems[i].ProductTypeId.Value, "Item does not match to requested product");
            }


            Console.WriteLine("Genres:");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0}", result.CatalogueItems[i].ProgramGenre);
            }
        }

        [TestMethod()]
        public void GetCatchUpCatalogueItemsTest()
        {
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();
            criteria.Paging.ItemsPerPage = 10;
            criteria.Paging.PageNumber = 1;
            criteria.SortOrder = SortOrders.ProgramTitleAsc;
            criteria.ClassificationCriteria.ProductTypeId = new Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier(ProductTypes.CatchUp);

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            Console.WriteLine(result.TotalResultCount);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0} - {1}", result.CatalogueItems[i].ProgramTitle, result.CatalogueItems[i].VideoTitle);
            }

            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(ProductTypes.CatchUp, result.CatalogueItems[i].ProductTypeId.Value, "Item does not match to requested product");
            }
        }

        [TestMethod()]
        public void GetBoxOfficeGenreCatalogueItemsTest()
        {
            string horrorMovies = "13";
            Catalogue target = new Catalogue();
            CatalogueCriteria criteria = new CatalogueCriteria();
            criteria.Paging.ItemsPerPage = 10;
            criteria.Paging.PageNumber = 1;
            criteria.SortOrder = SortOrders.ProgramTitleAsc;
            criteria.ClassificationCriteria.ProductTypeId = new Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier(ProductTypes.BoxOffice);
            criteria.ClassificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(horrorMovies);

            CatalogueResult result = target.GetCatalogueItems(criteria);

            Assert.IsNotNull(result);

            Console.WriteLine(result.TotalResultCount);

            Assert.AreNotEqual(0, result.TotalResultCount, "No items returned");

            for (int i = 0; i < 10; i++)
            {
                if (i >= result.CatalogueItems.Length)
                {
                    break;
                }
                Console.WriteLine("{0} - {1}", result.CatalogueItems[i].ProgramTitle, result.CatalogueItems[i].VideoTitle);
                Assert.AreEqual(ProductTypes.BoxOffice, result.CatalogueItems[i].ProductTypeId.Value, "Item does not match to requested product");
                Assert.AreEqual(horrorMovies, result.CatalogueItems[i].ClassificationItemId.Value, "Item does not match to requested classification");
            }
        }



        /// <summary>
        ///A test for GetChannels
        ///</summary>
        [TestMethod()]
        public void GetChannelsTest()
        {
            Catalogue target = new Catalogue();
            CatalogueItemIdentifier catalogueItemId = new CatalogueItemIdentifier(validCatalogueItemId);
            IChannel[] actual = target.GetChannels(catalogueItemId);

            actual.WriteXmlToConsole();

            Assert.AreNotEqual(0, actual.Length, "At least one item expected");
        }
    }

    internal static class TestExtensions
    {
        public static void WriteXmlToConsole(this Object result)
        {
            XmlWriter writer = XmlWriter.Create(Console.Out);
            DataContractSerializer ser = new DataContractSerializer(result.GetType());
            ser.WriteObject(writer, result);
            writer.Flush();
        }
    }
}
