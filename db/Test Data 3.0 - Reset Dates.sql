BEGIN TRAN


	-- Last Chance Items - Exp tomorrow
	UPDATE [dbo].[dt_Program]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() + 1, 120)
	WHERE [Abstract] LIKE '%Last Chance Test%'

	UPDATE [dbo].[dt_Video]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() + 1, 120),
		[ExpiryDate] = CONVERT(CHAR(10), GETDATE() + 1, 120)
	WHERE [Abstract] LIKE '%Last Chance Test%'
	
	-- Forth Coming Items - Valid tomorrow
	UPDATE [dbo].[dt_Program]
	SET [StartDate] = CONVERT(CHAR(10), GETDATE() + 7, 120)
	WHERE [Abstract] LIKE '%Forth Coming Test%'
	
	UPDATE [dbo].[dt_Video]
	SET [StartDate] = CONVERT(CHAR(10), GETDATE() + 7, 120),
		[AirDate] = CONVERT(CHAR(10), GETDATE() + 7, 120)
	WHERE [Abstract] LIKE '%Forth Coming Test%'

	-- Expired Items
	UPDATE [dbo].[dt_Program]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() - 1, 120)
	WHERE [Abstract] LIKE '%Expired Items%'

	UPDATE [dbo].[dt_Video]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() - 1, 120),
		[ExpiryDate] = CONVERT(CHAR(10), GETDATE() - 1, 120)
	WHERE [Abstract] LIKE '%Expired Items%'



ROLLBACK TRAN
--COMMIT TRAN