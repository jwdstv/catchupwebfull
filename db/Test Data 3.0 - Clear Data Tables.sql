	USE [DStvMedia2010]

	DELETE FROM [dbo].[dt_EditorialList]

	DELETE FROM [dbo].[dt_Person]

	DELETE FROM [dbo].[dt_Profession]

	DELETE FROM [dbo].[dt_VideoFile]

	DELETE FROM [dbo].[dt_Video]

	DELETE FROM [dbo].[dt_Program]
	
	
	DELETE FROM [dbo].[dt_DownloadLog]
	DELETE FROM [dbo].[dt_ErrorLog]
	DELETE FROM [dbo].[dt_Program_Rating]
	DELETE FROM [dbo].[dt_Rating]
	DELETE FROM [dbo].[dt_VideoLog]
	DELETE FROM [dbo].[dt_VideoMeta]
	DELETE FROM [dbo].[lt_Program_Tag]
	DELETE FROM [dbo].[lt_Video_Tag]
	DELETE FROM [dbo].[lt_Website_Channel]
	DELETE FROM [dbo].[lt_Website_VideoCategory]
	DELETE FROM [dbo].[tt_Video_Meta]
	DELETE FROM [dbo].[tt_Video_Meta_BU]
	DELETE FROM [dbo].[tt_Video_Meta_V2]
	DELETE FROM [dbo].[tt_Video_Meta_v3]
	
	DBCC CHECKIDENT ('lt_Product_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Program_Genre', RESEED, 0)
	DBCC CHECKIDENT ('lt_EditorialListItems', RESEED, 0)
	DBCC CHECKIDENT ('lt_Channel_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Product_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Person_Profession_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Person_Profession', RESEED, 0)
	DBCC CHECKIDENT ('lt_Video_VideoCategory', RESEED, 0)
	DBCC CHECKIDENT ('dt_Program', RESEED, 0)
	DBCC CHECKIDENT ('dt_Video', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoFile', RESEED, 0)
	DBCC CHECKIDENT ('dt_EditorialList', RESEED, 0)
	DBCC CHECKIDENT ('dt_Person', RESEED, 0)
	DBCC CHECKIDENT ('dt_Profession', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoCategory', RESEED, 0)

	DBCC CHECKIDENT ('dt_DownloadLog', RESEED, 0)
	DBCC CHECKIDENT ('dt_ErrorLog', RESEED, 0)
	DBCC CHECKIDENT ('dt_Program_Rating', RESEED, 0)
	DBCC CHECKIDENT ('dt_Rating', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoLog', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoMeta', RESEED, 0)
	DBCC CHECKIDENT ('lt_Program_Tag', RESEED, 0)
	DBCC CHECKIDENT ('lt_Video_VideoCategory', RESEED, 0)
	DBCC CHECKIDENT ('lt_Video_Tag', RESEED, 0)
	DBCC CHECKIDENT ('lt_Website_Channel', RESEED, 0)
	DBCC CHECKIDENT ('lt_Website_VideoCategory', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta_BU', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta_V2', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta_v3', RESEED, 0)
	
	
	SELECT * FROM [dbo].[lt_Channel_Program]

	SELECT * FROM [dbo].[lt_EditorialListItems]

	SELECT * FROM [dbo].[lt_Person_Profession]

	SELECT * FROM [dbo].[lt_Person_Profession_Program]

	SELECT * FROM [dbo].[lt_Product_Program]

	SELECT * FROM [dbo].[lt_Program_Genre]

	SELECT * FROM [dbo].[lt_Person_Profession_Program]

	SELECT * FROM [dbo].[lt_Product_Program]

	SELECT * FROM [dbo].[lt_Video_VideoCategory]
