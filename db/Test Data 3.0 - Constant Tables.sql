
	USE [DStvMedia2010]
	
	
--BEGIN TRAN

	DELETE FROM [dbo].[dt_AgeRestriction]
	DELETE FROM [dbo].[dt_Country]
	DELETE FROM [dbo].[dt_Language]
	DELETE FROM [dbo].[dt_Product]
	DELETE FROM [dbo].[dt_VideoFilePath]
	DELETE FROM [dbo].[dt_VideoFileType]
	DELETE FROM [dbo].[dt_Website]
	DELETE FROM [dbo].[dt_Status]
	DELETE FROM [dbo].[lt_Website_Product]


	SET IDENTITY_INSERT [dbo].[dt_AgeRestriction] ON
	
	
	
		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([Abbreviation], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([AgeRestrictionDescription], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_AgeRestriction]

	
		
		INSERT INTO [dbo].[dt_AgeRestriction]
				   ([ID]
				   ,[Abbreviation]
				   ,[AgeRestrictionDescription]
				   ,[StatusID])
			 VALUES (1, ' None', 'None', 1),
					(2, 'PG', 'Parental Guidance', 1),
					(3, '18 VSL', 'Not suitable for viewers under the age of 18, Violence, Sex and Language', 1),
					(4, '16 L', 'Not suitable for viewers under the age of 16, Language', 1),
					(5, '16 NL', 'Not suitable for viewers under the age of 16, Nudity and Language', 1),
					(6, 'None', 'No Age Restriction', 1),
					(7, '16 SL', 'Not suitable for viewers under the age of 16, Sex and Language', 1),
					(8, '13 S', 'Not suitable for viewers under the age of 13, Sex', 1),
					(9, 'FAM', 'Suitable for Family Viewing', 1),
					(10, 'PG 13', 'Parental Guidance, not suitable for viewers under the age of 13', 1),
					(11, '13 L', 'Not suitable for viewers under the age of 13, Language', 1),
					(12, '13 SL', 'Not suitable for viewers under the age of 13, Sex and Language', 1),
					(13, '13 V', 'Not suitable for viewers under the age of 13, Violence', 1),
					(14, '16 VSNL', 'Not suitable for viewers under the age of 16, Violence, Sex, Nudity and Language', 1),
					(15, '13 SN', 'Not suitable for viewers under the age of 13, Sex and Nudity', 1),
					(16, '18 VSLN', 'Not suitable for viewers under the age of 18, Violence, Sex, Language and Nudity', 1),
					(17, '16 V', 'Not suitable for viewers under the age of 16, Violence', 1),
					(18, '16 VL', 'Not suitable for viewers under the age of 16, Violence and Language', 1),
					(19, '16 VNL', 'Not suitable for viewers under the age of 16, Violence, Nudity and Language', 1),
					(20, '18', 'Not suitable for viewers under the age of 18', 1),
					(21, '13 NL', 'Not suitable for viewers under the age of 13, Language and Nudity', 1),
					(22, '13 VS', 'Not suitable for viewers under the age of 13, Violence and Sex', 1),
					(23, '13 VNL', 'Not suitable for viewers under the age of 13, Violence, Nudity and Language', 1),
					(24, '13 VL', 'Not suitable for viewers under the age of 13, Violence and Language', 1),
					(25, '16 VSN', 'Not suitable for viewers under the age of 16, Violence, Sex and Nudity', 1),
					(26, '18 SNL', 'Not suitable for viewers under the age of 18, Sex, Nudity and Language', 1),
					(27, '16 SNL', 'Not suitable for viewers under the age of 16, Sex, Nudity and Languauge', 1),
					(28, '16 S', 'Not suitable for viewers under the age of 16, Sex', 1),
					(29, '18 SN', 'Not suitable for viewers under the age of 18, Sex and Nudity', 1),
					(30, '18 SL', 'Not suitable for viewers under the age of 18, Sex and Language', 1),
					(31, '16 VSL', 'Not suitable for viewers under the age of 16, Violence, Sex and Language', 1),
					(32, '13 VSL', 'Not suitable for viewers under the age of 13, Violence, Sex and Language', 1),
					(33, 'R', 'Restricted', 1),
					(34, '16 N', 'Not suitable for viewers under the age of 16, Nudity', 1),
					(35, '13 VN', 'Not suitable for viewers under the age of 13, Violence and Nudity', 1),
					(36, '13 N', 'Not suitable for viewers under the age of 13, Nudity', 1),
					(37, '18 V', 'Not suitable for viewers under the age of 18, Violence', 1),
					(38, '16 VN', 'Not suitable for viewers under the age of 16, Violence and Nudity', 1),
					(39, 'PG 13 VL', 'Parental Guidance, not suitable for viewers under the age of 13, Violence and Language', 1),
					(40, 'PG 13 L', 'Parental Guidance, not suitable for viewers under the age of 13, Language', 1),
					(41, 'PG 13 SNL', 'Parental Guidance, not suitable for viewers under the age of 13, Sex, Nudity and Language', 1),
					(42, '16 VS', 'Not suitable for viewers under the age of 16, Violence and Sex', 1),
					(43, 'PG 13 V', 'Parental Guidance, not suitable for viewers under the age of 13, Violence', 1),
					(44, '18 VNL', 'Not suitable for viewers under the age of 18, Violence, Nudity and Language', 1),
					(45, 'R 18', 'Restricted, not suitable for viewers under the age of 18', 1),
					(46, '16', 'Not suitable for viewers under the age of 16', 1),
					(47, '13 SNL', 'Not suitable for viewers under the age of 13, Sex, Nudity and Language', 1),
					(48, '18 L', 'Not suitable for viewers under the age of 18, Language', 1),
					(49, 'PG 13 VN', 'Parental Guidance, not suitable for viewers under the age of 13, Violence and Nudity', 1),
					(50, 'PG 13 SL', 'Parental Guidance, not suitable for viewers under the age of 13, Sex and Language', 1),
					(51, '13 VSN', 'Not suitable for viewers under the age of 13, Violence, Sex and Nudity', 1),
					(52, '18 SVL', 'Not suitable for viewers under the age of 18, Sex, Violence and Language', 1),
					(53, '18 VL', 'Not suitable for viewers under the age of 18, Violence and Language', 1),
					(54, 'PG 13 NL', 'Parental Guidance, not suitable for viewers under the age of 13, Nudity and Lang', 1),
					(55, 'PG 13 N', 'Parental Guidance, not suitable for viewers under the age of 13, Nudity', 1),
					(56, '13', 'Not suitable for viewers under the age of 13', 1),
					(57, 'PG 13 VS', 'Parental Guidance, not suitable for viewers under the age of 13, Violence and Sex', 1),
					(58, 'PG 13 S', 'Parental Guidance, not suitable for viewers under the age of 13, Sex', 1),
					(59, '18 NL', 'Not suitable for viewers under the age of 18, Nudity and  Language', 1),
					(60, '16 SN', 'Not suitable for viewers under the age of 16, Sex and Nudity', 1),
					(61, 'PG 13 VSL', 'Parental Guidance, not suitable for viewers under the age of 13, Violence, Sex and Language', 1),
					(62, '21', 'Not suitable for viewers under the age of 21', 1),
					(64, 'R 21', 'Restricted, not suitable for viewers under the age of 21', 1),
					(65, 'R21 V', 'Restricted, not suitable for viewers under the age of 21, Violence', 1),
					(66, 'Not Rated', 'Not yet classified', 1),
					(67, 'PG VSNL', 'Parental Guidance, not suitable for viewers under the age of 13, Violence, Sex, ', 1)

	DECLARE @Max_AgeRestriction INT
	
	SET @Max_AgeRestriction = (SELECT MAX([ID]) FROM [dbo].[dt_AgeRestriction])
	SET @Max_AgeRestriction = ISNULL(@Max_AgeRestriction, 0)
	
	DBCC CHECKIDENT ('dt_AgeRestriction', RESEED, @Max_AgeRestriction)

	SET IDENTITY_INSERT [dbo].[dt_AgeRestriction] OFF

	-----------------------------------------------------------------------------------------

	SET IDENTITY_INSERT [dbo].[dt_Country] ON
	

		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([CountryName], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([FlagImageURL], '''','''''') + '''', 'NULL') + ', ' + ISNULL(CASE WHEN DefaultLanguageID IS NOT NULL THEN CAST(DefaultLanguageID AS VARCHAR) END, 'NULL') + ', ' + ISNULL(CASE WHEN DefaultFootprintID IS NOT NULL THEN CAST(DefaultFootprintID AS VARCHAR) END, 'NULL') + ', ' + ISNULL(CASE WHEN GMT IS NOT NULL THEN CAST(GMT AS VARCHAR) END, 'NULL') + ', ' + ISNULL('''' + REPLACE(DSN, '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE(NamiDSN, '''','''''') + '''', 'NULL') + ', ' + ISNULL(CASE WHEN ZIndex IS NOT NULL THEN CAST(ZIndex AS VARCHAR) END, 'NULL') + ', ' + CAST(StatusID AS VARCHAR)  + '),'
		--FROM [dbo].[dt_Country]

		INSERT INTO [dbo].[dt_Country]
				   ([ID]
				   ,[CountryName]
				   ,[FlagImageURL]
				   ,[DefaultLanguageID]
				   ,[DefaultFootprintID]
				   ,[GMT]
				   ,[DSN]
				   ,[NamiDSN]
				   ,[ZIndex]
				   ,[StatusID])
			 VALUES (1, 'Angola', 'angola_flag.gif', 3, 3, 3, 'Angola', NULL, NULL, 1),
					(2, 'Benin', 'benin_flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(3, 'Botswana', 'botswana_flag.gif', 1, 1, 1, 'Botswana', NULL, NULL, 1),
					(4, 'Burkina Faso', 'burkina_flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(5, 'Burundi', 'burindi_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(6, 'Cameroon', 'cameroon_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(7, 'Cape Verde', 'capeverde_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(8, 'Central African Rep', 'ca_logo.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(9, 'Chad', 'chad_logo.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(10, 'Comores', 'comores_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(11, 'Congo', 'congo_flag.gif', 3, 3, 3, 'DRC', 'COG', NULL, 1),
					(12, 'Cote d’Ivoire', 'cote__flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(13, 'DRC', 'drc_flag.gif', 3, 3, 3, 'DRC', 'COG', NULL, 1),
					(14, 'Equatorial Guinea', 'eg_flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(15, 'Eritrea', 'eritrea_Flag.gif', 2, 2, 2, 'DTH', 'DTH', NULL, 1),
					(16, 'Ethiopia', 'ethiopia_flag.gif', 2, 2, 2, 'Ethiopia', 'ETH', NULL, 1),
					(17, 'Gabon', 'gabon_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(18, 'Gambia', 'gambia_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(19, 'Ghana', 'ghana_flag.gif', 4, 4, 4, 'Ghana', NULL, NULL, 1),
					(20, 'Guinea', 'guinea_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(21, 'Guinea Bissau', 'gb_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(22, 'Kenya', 'kenya_flag.gif', 3, 3, 3, 'Kenya', NULL, NULL, 1),
					(23, 'Liberia', 'liberia_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(24, 'Madagascar', 'madagascar_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(25, 'Malawi', 'malawi_flag.gif', 1, 1, 1, 'Malawi', 'MWI', NULL, 1),
					(26, 'Mali', 'mali_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(27, 'Mauritius', 'mauritius_flag.gif', 3, 3, 3, 'Mauritius', 'DTH', NULL, 1),
					(28, 'Mayotte', 'MAYOTTE_fLAG.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(29, 'Mozambique', 'mozam_flag.gif', 1, 1, 1, 'Mozambique', NULL, 1, 1),
					(30, 'Namibia', 'nam_flag.gif', 1, 1, 1, 'Namibia', NULL, 1, 1),
					(31, 'Niger', 'niger_flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(32, 'Nigeria', 'nigeria_flag.gif', 4, 4, 4, 'Nigeria', NULL, NULL, 1),
					(33, 'Reunion', 'reunoin_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(34, 'Rwanda', 'rwanda_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(35, 'Sao Tome', 'saot_flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(36, 'Senegal', 'senegal_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(37, 'Seychelles', 'seychelles_flag.gif', 2, 2, 2, 'DTH', 'DTH', NULL, 1),
					(38, 'Sierra Leone', 'SL_Flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(39, 'Somalia', 'somalia_flag.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(40, 'South Africa', 'sa_flag.gif', 1, 1, 1, 'South_Africa', NULL, 1, 1),
					(41, 'St Helena', 'sthelena_flag.gif', 2, 2, 2, 'DTH', 'DTH', NULL, 1),
					(42, 'Sudan', 'sudan_flag.gif', 2, 2, 2, 'DTH', 'DTH', NULL, 1),
					(43, 'Swaziland', 'swazi_flag.gif', 1, 1, 1, 'Swaziland', NULL, NULL, 1),
					(44, 'Tanzania', 'tanz_flag.gif', 3, 3, 3, 'Tanzania', NULL, NULL, 1),
					(45, 'Togo', 'togo_flag.gif', 4, 4, 4, 'DTH', 'DTH', NULL, 1),
					(46, 'Uganda', 'uganda_flag.gif', 3, 3, 3, 'Uganda', NULL, NULL, 1),
					(47, 'Zanzibar', 'zanzibar.gif', 3, 3, 3, 'DTH', 'DTH', NULL, 1),
					(48, 'Zimbabwe', 'zim.gif', 1, 1, 1, 'Zimbabwe', NULL, NULL, 1),
					(49, 'Zambia', 'zambia.gif', 1, 1, 1, 'Zambia', NULL, NULL, 1)

	DECLARE @Max_Country INT
	
	SET @Max_Country = (SELECT MAX([ID]) FROM [dbo].[dt_Country])
	SET @Max_Country = ISNULL(@Max_Country, 0)
	
	DBCC CHECKIDENT ('dt_Country', RESEED, @Max_Country)

	SET IDENTITY_INSERT [dbo].[dt_Country] OFF

	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_Language] ON
	

		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([LanguageName], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_Language]


		INSERT INTO [dbo].[dt_Language]
				   ([ID]
				   ,[LanguageName]
				   ,[StatusID])
			 VALUES (3, 'Afrikaans', 1),
					(4, 'Arabic', 1),
					(5, 'Chinese', 1),
					(6, 'Dutch', 1),
					(7, 'English', 1),
					(8, 'French', 1),
					(9, 'German', 1),
					(10, 'Greek', 1),
					(11, 'Hindi', 1),
					(12, 'Indian', 1),
					(13, 'Italian', 1),
					(14, 'Portuguese', 1),
					(15, 'Tamil', 1),
					(16, 'Khosa', 1),
					(18, 'Zulu', 1),
					(19, 'Moré', 1),
					(20, 'Lingala', 1),
					(21, 'Swahili', 1),
					(22, 'Wolof', 1),
					(23, 'Créole', 1),
					(24, 'Bambara', 1),
					(25, 'Kabuverdianu', 1),
					(26, 'Malagasy', 1),
					(27, 'Luganda', 1),
					(28, 'Kaado', 1),
					(29, 'Djula', 1),
					(30, 'Kriolu', 1),
					(31, 'Sotho', 1)


	DECLARE @Max_Language INT
	
	SET @Max_Language = (SELECT MAX([ID]) FROM [dbo].[dt_Language])
	SET @Max_Language = ISNULL(@Max_Language, 0)
	
	DBCC CHECKIDENT ('dt_Language', RESEED, @Max_Language)

	SET IDENTITY_INSERT [dbo].[dt_Language] OFF
	
	
	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_Product] ON
	


		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([ProductName], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([ProductDescription], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([ProductIcon], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([MediumLogo], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([LargeLogo], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([CurrencyIdentifier], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([CreatedDate], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_Product]


		INSERT INTO [dbo].[dt_Product]
				   ([ID]
				   ,[ProductName]
				   ,[ProductDescription]
				   ,[ProductIcon]
				   ,[MediumLogo]
				   ,[LargeLogo]
				   ,[CurrencyIdentifier]
				   ,[CreatedDate]
				   ,[StatusID])
			 VALUES (1, 'AFL', 'AFL', NULL, NULL, NULL, NULL, 'Apr 13 2010  7:11PM', 0),
					(2, 'Catch Up', 'Catch Up', NULL, NULL, NULL, NULL, 'Apr 13 2010  7:11PM', 1),
					(3, 'M-Net Sales', 'M-Net Sales', NULL, NULL, NULL, NULL, 'Apr 13 2010  7:11PM', 0),
					(4, 'Open Time', 'Open Time', NULL, NULL, NULL, NULL, 'Apr 13 2010  7:12PM', 1),
					(5, 'Open Time Idols', 'Open Time Idols', NULL, NULL, NULL, NULL, 'Jul  9 2010  4:19PM', 1),
					(6, 'Open Time Big Brother', 'Open Time Big Brother', NULL, NULL, NULL, NULL, 'Jul  9 2010  4:20PM', 1),
					(7, 'Open Time Boer Soek n Vrou', 'Open Time Boer Soek n Vrou', NULL, NULL, NULL, NULL, 'Aug 31 2010  4:25PM', 1),
					(8, 'Open Time Naija Sings', 'Open Time Naija Sings', NULL, NULL, NULL, NULL, 'Aug 31 2010  4:26PM', 1),
					(9, 'OD CatchUp', 'OD CatchUp', NULL, NULL, NULL, NULL, 'Oct 11 2010  2:21PM', 1),
					(10, 'OD BoxOffice', 'OD BoxOffice', NULL, NULL, NULL, NULL, 'Oct 11 2010  2:21PM', 1),
					(11, 'OD OpenTime', 'OD OpenTime', NULL, NULL, NULL, NULL, 'Oct 11 2010  2:21PM', 1)

	DECLARE @Max_Product INT
	
	SET @Max_Product = (SELECT MAX([ID]) FROM [dbo].[dt_Product])
	SET @Max_Product = ISNULL(@Max_Product, 0)
	
	DBCC CHECKIDENT ('dt_Product', RESEED, @Max_Product)

	SET IDENTITY_INSERT [dbo].[dt_Product] OFF


	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_ProgramType] ON
	
	DELETE FROM [dbo].[dt_ProgramType]


		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([ProgramTypeName], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([RedirectURL], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([CreatedDate], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_ProgramType]


		INSERT INTO [dbo].[dt_ProgramType]
				   ([ID]
				   ,[ProgramTypeName]
				   ,[RedirectURL]
				   ,[CreatedDate]
				   ,[StatusID])
			 VALUES (1, 'Show Episode', '1', 'Jul 13 2009  2:59PM', 1),
					(2, 'Movie', '1', 'Jul 13 2009  2:59PM', 1),
					(3, 'Show Clip', '1', 'Jul 13 2009  2:59PM', 1),
					(4, 'Movie Clip', '1', 'Jul 13 2009  2:59PM', 1),
					(5, 'Feature Film', '1', 'Jul 13 2009  2:59PM', 1),
					(6, 'Movie Trailer', '1', 'Jul 13 2009  2:59PM', 1),
					(7, 'Show Trailer', '1', 'Jul 13 2009  4:09PM', 1)


	DECLARE @Max_ProgramType INT
	
	SET @Max_ProgramType = (SELECT MAX([ID]) FROM [dbo].[dt_ProgramType])
	SET @Max_ProgramType = ISNULL(@Max_ProgramType, 0)
	
	DBCC CHECKIDENT ('dt_ProgramType', RESEED, @Max_ProgramType)

	SET IDENTITY_INSERT [dbo].[dt_ProgramType] OFF


	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_Status] ON
	

		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([Status], '''','''''') + '''', 'NULL') + '),'
		--FROM [dbo].[dt_Status]


		INSERT INTO [dbo].[dt_Status]
				   ([ID]
				   ,[Status])
			 VALUES (1, 'Enabled'),
					(2, 'Disabled'),
					(3, 'Archived'),
					(4, 'Deleted')

	DECLARE @Max_Status INT
	
	SET @Max_Status = (SELECT MAX([ID]) FROM [dbo].[dt_Status])
	SET @Max_Status = ISNULL(@Max_Status, 0)
	
	DBCC CHECKIDENT ('dt_Status', RESEED, @Max_Status)

	SET IDENTITY_INSERT [dbo].[dt_Status] OFF


	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_VideoFilePath] ON
	

		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([VideoFilePathName], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([ServerPath], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([CreatedDate], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_VideoFilePath]


		INSERT INTO [dbo].[dt_VideoFilePath]
				   ([ID]
				   ,[VideoFilePathName]
				   ,[ServerPath]
				   ,[CreatedDate]
				   ,[StatusID])
			 VALUES (1, 'M-Net Streaming Server', 'http://mnetnmvideo.m-net.co.za/mnetvideo/flv/', 'Jun 30 2009 12:11PM', 1),
					(2, 'Akamai Streaming Long Format', 'rtmpte://cp73514.edgefcs.net/ondemand/longformat', 'Jun 30 2009 12:11PM', 1),
					(3, 'Akamai Streaming Short Format', 'rtmp://cp73514.edgefcs.net/ondemand/shortformat', 'Jun 30 2009 12:13PM', 1),
					(4, 'Akamai Streaming AFL', 'rtmp://cp73514.edgefcs.net/ondemand/EU/afl/', 'Jun 30 2009 12:13PM', 1),
					(5, 'Akamai WMV Download', 'http://mfile.akamai.com/14080/wmv/kuduclub.download.akamai.com/14080', 'Jun 30 2009 12:14PM', 1),
					(6, 'Test Short Format', 'rtmpte://cp77107.edgefcs.net/ondemand/shortformat', 'Jul 15 2009  1:50PM', 4),
					(7, 'Akamai EU Streaming Long Format', 'http://staging.dstv.com/videofiles', 'Sep 22 2009  6:29AM', 1),
					(8, 'Akamai EU Streaming Short Format', 'http://staging.dstv.com/videofiles/shortformat', 'Sep 22 2009  6:30AM', 1),
					(9, 'Miami Server', 'http://64.71.233.138', 'Oct  6 2009  9:53AM', 1),
					(10, 'DStvOD Download Server', 'http://196.22.172.100/', 'Apr  8 2010 12:00AM', 1),
					(11, 'Desigan Streaming Server', 'http://196.2.137.103/StreamTest', 'Apr 18 2010 10:07PM', 1),
					(12, 'M-Web Short Format Flash', 'rtmpe://vod.dstv.com/vod/media/Shortformat', 'Apr 20 2010  2:14PM', 1),
					(13, 'M-Web Long Format Flash', 'rtmpe://vod.dstv.com/vod/media/Longformat', 'Apr 20 2010  2:15PM', 1),
					(14, 'M-Web Long Format WMV', 'http://san.dstv.com/', 'Apr 21 2010 10:16AM', 1),
					(15, 'M-Web Short Format Flash Unprotected', 'http://san.dstv.com', 'Jul 12 2010  1:52PM', 1)


	DECLARE @Max_VideoFilePath INT
	
	SET @Max_VideoFilePath = (SELECT MAX([ID]) FROM [dbo].[dt_VideoFilePath])
	SET @Max_VideoFilePath = ISNULL(@Max_VideoFilePath, 0)
	
	DBCC CHECKIDENT ('dt_VideoFilePath', RESEED, @Max_VideoFilePath)

	SET IDENTITY_INSERT [dbo].[dt_VideoFilePath] OFF


	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_VideoFileType] ON
	

		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([FileExtension], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([Description], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_VideoFileType]


		INSERT INTO [dbo].[dt_VideoFileType]
				   ([ID]
				   ,[FileExtension]
				   ,[Description]
				   ,[StatusID])
			 VALUES (1, '.flv', 'Flash Video - 700K', 1),
					(2, '.3gp', '3GP - SD', 1),
					(3, '.wmv', 'WM Video - 700K', 1),
					(4, '.flv', 'Flash Video - 300K', 1),
					(5, '.wmv', 'WM Video - 300K', 1),
					(6, '.wmv', 'WM Video - 1000K', 1)


	DECLARE @Max_VideoFileType INT
	
	SET @Max_VideoFileType = (SELECT MAX([ID]) FROM [dbo].[dt_VideoFileType])
	SET @Max_VideoFileType = ISNULL(@Max_VideoFileType, 0)
	
	DBCC CHECKIDENT ('dt_VideoFileType', RESEED, @Max_VideoFileType)

	SET IDENTITY_INSERT [dbo].[dt_VideoFileType] OFF


	-----------------------------------------------------------------------------------------


	SET IDENTITY_INSERT [dbo].[dt_Website] ON
	

		--SELECT '(' + CAST([ID] AS VARCHAR) + ', ' + ISNULL('''' + REPLACE([WebsiteName], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([WebsiteURL], '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE([AuthenticationKey], '''','''''') + '''', 'NULL') + ', ' + ISNULL(CASE WHEN [VideoAccess] IS NOT NULL THEN CAST([VideoAccess] AS VARCHAR) END, 'NULL') + ', ' + CAST([StatusID] AS VARCHAR) + '),'
		--FROM [dbo].[dt_Website]


	INSERT INTO [dbo].[dt_Website]
			   ([ID]
			   ,[WebsiteName]
			   ,[WebsiteURL]
			   ,[AuthenticationKey]
			   ,[VideoAccess]
			   ,[StatusID])
		 VALUES (1, 'On Demand Catch Up', 'http://player.dstv.com', '2W18EC9C-81D5-40E9-90A5-4EDAZB639DXQ', 1, 1),
				(2, 'Vuzu', 'http://www.vuzu.tv', '0F18EC9C-84D2-40E9-90A5-4EDAAB619DFA', 1, 1),
				(3, 'KuduClub.com', 'http://www.kuduclub.com', 'VQ11EC9C-84D2-42E9-6075-48DAA9610DSA', 1, 1),
				(4, 'AFL', 'http://www.africanfilmlibrary.com', '0F18E39C-85D2-49E9-98A5-41DA1B6191F7', 1, 1),
				(5, 'DStv South Africa', 'http://www.dstv.com', '32d879d1-8479-4166-8b8f-2a128d9f72bd', 0, 1),
				(6, 'WS Test User', 'http://services.dstv.com', '0D18EC9E-84C2-40E9-90A5-4EDAAB617FFA', 0, 1),
				(7, 'M-NET', 'http://www.mnet.co.za', 'A00610FA-EC3C-4C7C-B597-462CE98E79CD', 0, 1),
				(8, 'MyWeek', 'http://myweek.co.za', 'DA8BA23E-56D1-4B40-8FA7-8A79B081FAF8', 0, 1),
				(9, 'MIH SWAT', 'http://www.mihswat.com', 'D32C5287-7B57-411F-9217-7BE1AAEA145E', 0, 1),
				(10, 'Martin Willis : Care', 'http://www.oilstone.com', '6F09CFF3-EA94-475F-9779-54F1F7DAF19D', 0, 1),
				(11, 'NM CMS', 'http://cms.multichoice.co.za', '889E6796-8192-41FF-A517-DA967E74D114', 0, 1),
				(12, 'DStv Flash EPG', 'http://guide.dstv.com', '68FBB3CC-7B43-4BB1-8F73-DCA365248CE2', 0, 1),
				(13, '24', 'http://www.24.com', '0B4CE3AE-BD39-40F5-BAE5-8C7C71162F30', 0, 1),
				(14, 'M-NET Africa', 'http://www.mnetafrica.com', '36D50A21-BB63-47B6-AA4D-D3E971AB18BD', 0, 1),
				(15, 'TNS', 'http://localhost', '6704B289-200A-4497-BCFE-848ECE704A2D', 0, 1),
				(16, 'DStv Mobile - Mobile', 'http://www.dstvmobile.co.za', '013B065B-1070-4D5E-B977-8E50FC794E75', 0, 1),
				(17, 'Orsome', 'http://watling.co.nz', 'AA4AB61D-7673-4DAB-906E-3C9B75223D3D', 0, 1),
				(18, 'iPhone Guide', 'http://www.multichoice.co.za', 'BFF934C7-547F-4538-A5D7-5E68B2926788', 0, 1),
				(19, 'DStv Mobile Africa', 'http://dstvmobileafrica.co.za', '51D6E2DA-3CAC-40F0-A62D-C27C65AF8212', 0, 1),
				(20, 'SuperSport New Media', 'http://www.supersport.co.za', 'F0457968-166E-4E00-B2EA-69F3306BC19D', 0, 1),
				(21, 'DStv Mobile - Website', 'http://www.dstvmobile.co.za', '0BDD0754-CC2A-46F8-8D76-A1CC9B247297', 0, 1),
				(22, 'M-NET Mobile', 'http://www.mnet.co.za', 'B304A6A9-A888-4D2D-BAC1-16AB74FD7587', 0, 1),
				(23, 'M-Net Sales', 'http://www.mnetsales.com', '354A0A86-532E-48C7-8391-77887B274DA6', 1, 1),
				(25, 'DStv CMS', 'http://www.dstv.com', 'F6A32F01-5594-402C-B885-3D54E69B09EE', 1, 1),
				(26, 'DStv Secure Account', 'https://secure.dstv.com/account', '405f9b03-1758-42fe-818d-ce8bcfd05304', NULL, 1),
				(33, 'DSTV On Demand', 'http://www.dstvondemand.com', 'EA654D83-ED2D-4447-9BDE-72EEDD27930F', 1, 1),
				(36, 'Idols 6', 'http://www.dstv.com/Idols', '32283367-9008-4D32-AA7F-64D4BA3892C2', 1, 1),
				(37, 'Big Brother Africa', 'http://www.dstv.com/BigBrotherAfrica', '0EADD8F5-1C84-411D-AA36-7145B2A01A35', 1, 1),
				(38, 'Irdeto Research User', 'http://www.irdeto.com', '59FEF2D6-818B-4831-95D1-857912E9CA75', 1, 1),
				(40, 'Naija Sings', 'http://', 'CE7781EA-0158-4723-B63A-D06765059C7E', 1, 1),
				(42, 'Boer Soek n Vrou', 'http://', 'F89B6706-95D7-4052-BC6B-F896906E3C3D', 1, 1),
				(43, 'OnDemandTest', 'http://ondemand.dstv.com', '035C9082-4FAB-4595-8B00-8856ACB505F8', 1, 1)
           

	DECLARE @Max_Website INT
	
	SET @Max_Website = (SELECT MAX([ID]) FROM [dbo].[dt_Website])
	SET @Max_Website = ISNULL(@Max_Website, 0)
	
	DBCC CHECKIDENT ('dt_Website', RESEED, @Max_Website)

	SET IDENTITY_INSERT [dbo].[dt_Website] OFF


	-----------------------------------------------------------------------------------------
		
	DBCC CHECKIDENT ('lt_Website_Product', RESEED, 0)
	
	INSERT INTO [DStvMedia2010].[dbo].[lt_Website_Product]
			   ([WebsiteID]
			   ,[ProductID])
		 VALUES (43, 9),
			   (43, 10)
			   
			   

--ROLLBACK TRAN
--COMMIT TRAN