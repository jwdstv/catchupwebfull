
	USE [DStvMedia2010]
	
	DBCC CHECKIDENT ('lt_Product_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Program_Genre', RESEED, 0)
	DBCC CHECKIDENT ('lt_EditorialListItems', RESEED, 0)
	DBCC CHECKIDENT ('lt_Channel_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Product_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Person_Profession_Program', RESEED, 0)
	DBCC CHECKIDENT ('lt_Person_Profession', RESEED, 0)
	DBCC CHECKIDENT ('lt_Video_VideoCategory', RESEED, 0)
	DBCC CHECKIDENT ('dt_Program', RESEED, 0)
	DBCC CHECKIDENT ('dt_Video', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoFile', RESEED, 0)
	DBCC CHECKIDENT ('dt_EditorialList', RESEED, 0)
	DBCC CHECKIDENT ('dt_Person', RESEED, 0)
	DBCC CHECKIDENT ('dt_Profession', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoCategory', RESEED, 0)

	DBCC CHECKIDENT ('dt_DownloadLog', RESEED, 0)
	DBCC CHECKIDENT ('dt_ErrorLog', RESEED, 0)
	DBCC CHECKIDENT ('dt_Program_Rating', RESEED, 0)
	DBCC CHECKIDENT ('dt_Rating', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoLog', RESEED, 0)
	DBCC CHECKIDENT ('dt_VideoMeta', RESEED, 0)
	DBCC CHECKIDENT ('lt_Program_Tag', RESEED, 0)
	DBCC CHECKIDENT ('lt_Video_VideoCategory', RESEED, 0)
	DBCC CHECKIDENT ('lt_Video_Tag', RESEED, 0)
	DBCC CHECKIDENT ('lt_Website_Channel', RESEED, 0)
	DBCC CHECKIDENT ('lt_Website_VideoCategory', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta_BU', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta_V2', RESEED, 0)
	DBCC CHECKIDENT ('tt_Video_Meta_v3', RESEED, 0)

--BEGIN TRAN

	-- NOT repopulated by script
	DELETE FROM [dbo].[dt_DownloadLog]
	DELETE FROM [dbo].[dt_ErrorLog]
	DELETE FROM [dbo].[dt_Program_Rating]
	DELETE FROM [dbo].[dt_Rating]
	DELETE FROM [dbo].[dt_VideoLog]
	DELETE FROM [dbo].[dt_VideoMeta]
	DELETE FROM [dbo].[lt_Program_Tag]
	DELETE FROM [dbo].[lt_Video_Tag]
	DELETE FROM [dbo].[lt_Website_Channel]
	DELETE FROM [dbo].[lt_Website_VideoCategory]
	DELETE FROM [dbo].[tt_Video_Meta]
	DELETE FROM [dbo].[tt_Video_Meta_BU]
	DELETE FROM [dbo].[tt_Video_Meta_V2]
	DELETE FROM [dbo].[tt_Video_Meta_v3]

	-- Repopulated by script
	DELETE FROM [dbo].[lt_Product_Program]
	DELETE FROM [dbo].[lt_Program_Genre]
	DELETE FROM [dbo].[lt_EditorialListItems]
	DELETE FROM [dbo].[lt_Channel_Program]
	DELETE FROM [dbo].[lt_Person_Profession_Program]
	DELETE FROM [dbo].[lt_Person_Profession]
	DELETE FROM [dbo].[lt_Video_VideoCategory]	
	DELETE FROM [dbo].[dt_VideoCategory]	
	DELETE FROM [dbo].[dt_VideoFile]
	DELETE FROM [dbo].[dt_Video]
	DELETE FROM [dbo].[dt_Program]
	DELETE FROM [dbo].[dt_EditorialList]
	DELETE FROM [dbo].[dt_Person]	
	DELETE FROM [dbo].[dt_Profession]	
	

	DECLARE @BoxOfficeID INT = (SELECT TOP 1 ID FROM [dbo].[dt_Product] WHERE ProductName LIKE '%boxoffice%')
	DECLARE @CatchUpID INT = (SELECT TOP 1 ID FROM [dbo].[dt_Product] WHERE ProductName LIKE '%catchup%')
	

	DECLARE @ProgramTbl TABLE 
			(ProgramName VARCHAR(150)
			,ProgramTypeID SMALLINT
			,ProductID INT
			)
			
	--ProgramTypeID 1 = Show; 2 = Movie
	INSERT INTO @ProgramTbl
			(ProgramName
			,ProgramTypeID
			,ProductID)
	VALUES ('Paranormal Activity 2', 2, @CatchUpID),
		('Jackass 3D', 2, @BoxOfficeID),
		('Red', 2, @BoxOfficeID),
		('Hereafter', 2, @CatchUpID),
		('The Social Network', 2, @BoxOfficeID),
		('Secretariat', 2, @BoxOfficeID),
		('Life as We Know It', 2, @BoxOfficeID),
		('Legend of the Guardians: The Owls of Ga Hoole', 2, @BoxOfficeID),
		('The Town', 2, @BoxOfficeID),
		('Easy A', 2, @BoxOfficeID),
		('The A-Team', 2, @CatchUpID),
		('The Hangover', 2, @CatchUpID),
		('Lost', 1, @CatchUpID),
		('House', 1, @CatchUpID),
		('CSI New York', 1, @CatchUpID),
		('America''s Got Talent', 1, @CatchUpID),
		('Keeping Up with the Kardashians', 1, @CatchUpID),
		('Carte Blanche', 1, @CatchUpID),
		('Idols', 1, @CatchUpID),
		('Chuck', 1, @CatchUpID),
		('Top Gear', 1, @CatchUpID),
		('Life', 1, @CatchUpID),
		('Two and a Half Men', 1, @CatchUpID),
		('Dexter', 1, @CatchUpID),
		('The Simpsons', 1, @CatchUpID),
		('Futurama', 1, @CatchUpID),
		('Conviction', 2, @BoxOfficeID),
		('Scream 4', 2, @BoxOfficeID),
		('Due Date', 2, @BoxOfficeID),
		('SAW 3D', 2, @BoxOfficeID),
		('Megamind', 2, @BoxOfficeID),
		('For Colored Girls', 2, @BoxOfficeID),
		('Freakonomics', 2, @BoxOfficeID),
		('Nowhere Boy', 2, @BoxOfficeID),
		('Enter the Void', 2, @BoxOfficeID),
		('Machete', 2, @BoxOfficeID),
		('Resident Evil: Afterlife', 2, @BoxOfficeID),
		('Piranha', 2, @BoxOfficeID),
		('How to Train Your Dragon', 2, @CatchUpID),
		('Splice', 2, @CatchUpID),
		('Predators', 2, @CatchUpID),
		('Frozen', 2, @CatchUpID),
		('Iron Man 2', 2, @CatchUpID),
		('Prince of Persia: The Sands of Time', 2, @CatchUpID),
		('Heroes', 1, @CatchUpID),
		('Bleach', 1, @CatchUpID),
		('Scrubs', 1, @CatchUpID),
		('Prison Break', 1, @CatchUpID),
		('Veronica Mars', 1, @CatchUpID),
		('Jericho', 1, @CatchUpID),
		('24', 1, @CatchUpID),
		('Unstoppable', 2, @BoxOfficeID),
		('Morning Glory', 2, @BoxOfficeID),
		('Sanctum', 2, @BoxOfficeID),
		('Toy Story 3', 2, @CatchUpID),
		('Killers', 2, @CatchUpID),
		('Bakgat 2', 2, @CatchUpID),
		('Weeds', 1, @CatchUpID),
		('Smallville', 1, @CatchUpID),
		('V', 1, @CatchUpID),
		('Nanny McPhee Returns', 2, @BoxOfficeID),
		('The Tillman Story', 2, @BoxOfficeID),
		('Wall Street: Money Never Sleeps', 2, @BoxOfficeID),
		('Grown Ups', 2, @CatchUpID),
		('Ramona and Beezus', 2, @CatchUpID),
		('Scott Pilgrim vs. the World', 2, @CatchUpID),
		('Desperate Housewives', 1, @CatchUpID),
		('Band of Brothers', 1, @CatchUpID),
		('Scrubs', 1, @CatchUpID),
		('Case 39', 2, @BoxOfficeID),
		('You Again', 2, @BoxOfficeID),
		('Let Me In', 2, @BoxOfficeID),
		('Death at a Funeral', 2, @CatchUpID),
		('She''s Out of My League', 2, @CatchUpID),
		('A Nightmare on Elm Street', 2, @CatchUpID),
		('Alias', 1, @CatchUpID),
		('4400', 1, @CatchUpID),
		('30 Rock', 1, @CatchUpID)

 
	---------------------- 3.0 ----------------------
	INSERT INTO [dbo].[dt_Program]
	  ([ProgramName]
      ,[ProgramTypeID]
      ,[Abstract]
      ,[ImageURL]
      ,[BillboardURL]
      ,[GenreID]
      ,[SubGenreID]
      ,[GeoGroupID]
      ,[WebsiteURL]
      ,[Keywords]
      ,[StatusID]
      ,[StartDate]
      ,[EndDate])
		SELECT ProgramName
			,ProgramTypeID
			,'This is the abstract for ' + ProgramName AS Abstract
			,'/content/onDemand/140x80_ThumbN.jpg' AS ImageURL
			,'/content/onDemand/show_vidPlayerHolder.jpg' AS BillboardURL
			,0 AS GenreID
			,0 AS SubGenreID
			,0 AS GeoGroupID
			,'http://www.imdb.com/title/' + ProgramName AS WebsiteURL
			,ProgramName AS Keywords
			,1 AS StatusID
			,GETDATE()-2 AS Startdate
			,GETDATE()+1000 AS EndDate
			FROM @ProgramTbl

	---------------------- 3.1 ----------------------
	INSERT INTO [dbo].[lt_Product_Program]
           ([ProductID]
           ,[ProgramID])
		SELECT DISTINCT PT.ProductID
			,P.ID
		FROM @ProgramTbl PT
		INNER JOIN [dbo].[dt_Program] P ON PT.ProgramName = P.ProgramName AND PT.ProgramTypeID = P.ProgramTypeID
		
	
	---------------------- 4.0 -----------------------
	DECLARE @VideoTypeTbl TABLE 
			(TitleSuffix VARCHAR(50)
			,AbstractPrefix VARCHAR(150)
			,VideoProgramType INT
			,Runtime INT
			,Price NUMERIC(10,2)
			,ProgramType VARCHAR(2)
			)
			
	DECLARE @AgeRestrictCount INT
	
	SET @AgeRestrictCount = (SELECT COUNT([ID]) FROM [dbo].[dt_AgeRestriction])
			
	INSERT INTO @VideoTypeTbl
			([TitleSuffix]
			,[AbstractPrefix]
			,[VideoProgramType]
			,[Runtime]
			,[Price]
			,[ProgramType]
			)
		VALUES ('- Show Episode 1', 'This is the abstract for Show Episode 1: ', 1, 5, 0, 1),
			('- Movie', 'This is the abstract for Movie: ', 2, 100, 30, 2),
			('- EPK Show Clip Episode 1', 'This is the abstract for EPK Show Clip: ', 3, 5, 0, 1),
			('- EPK Movie Clip', 'This is the abstract for EPK Movie Clip: ', 4, 5, 0, 2),
			('- Movie Trailer', 'This is the abstract for Movie Trailer: ', 6, 5, 0, 2),
			('- Show Trailer Episode 1', 'This is the abstract for Show Trailer: ', 7, 5, 0, 1)
		
	
	INSERT INTO [dbo].[dt_Video]
			   ([VideoTitle]
			   ,[Abstract]
			   ,[ImageURL]
			   ,[BillboardImageURL]
			   ,[EPGID]
			   ,[ProgramID]
			   ,[VideoProgramTypeID]
			   ,[AgeRestrictionID]
			   ,[AirDate]
			   ,[Runtime]
			   ,[Season]
			   ,[Episode]
			   ,[StartDate]
			   ,[EndDate]
			   ,[Keywords]
			   ,[CreatedDate]
			   ,[StatusID]
			   ,[ExpiryDate]
			   ,[RuntimeInMinutes]
			   ,[RentalPeriodInHours]
			   ,[Price]
			   ,[Geolocation]
			   ,[GroupID]
			   ,[CastCrewID])
	     
		SELECT P.[ProgramName] + VT.[TitleSuffix] AS Title
			,VT.[AbstractPrefix] + P.[ProgramName] AS Abstract
			,P.[ImageURL] AS ImageURL
			,P.[BillboardURL] AS BillboardURL
			,'0'  AS [EPGID]
			,P.ID AS [ProgramID]
			,VT.[VideoProgramType]
			,ISNULL(AR.[ID], 1) AS AgeRestrictionID
			,P.[StartDate] AS AirDate
			,'10:12' AS Runtime
			,CASE P.ProgramTypeID 
				WHEN 1 THEN (ABS(CHECKSUM(NewId())) % 4) + 1
				ELSE 0
			END AS Season
			,CASE P.ProgramTypeID 
				WHEN 1 THEN 1
				ELSE 0
			END AS Episode
			,P.[StartDate] AS Startdate
			,P.[EndDate] AS EndDate
			,P.[ProgramName] AS Keywords
			,GETDATE() AS CreatedDate
			,1 AS StatusID
			,P.[EndDate] AS ExpiryDate
			,VT.[Runtime]
			,48 AS RentalPeriodInHours
			,VT.[Price]
			,1 AS Geolocation
			,1 AS GroupId
			,0 AS CastCrewID
		FROM [dbo].[dt_Program] P
		INNER JOIN @VideoTypeTbl VT ON VT.[ProgramType] = P.[ProgramTypeID]
		INNER JOIN (SELECT ID, (ABS(CHECKSUM(NEWID())) % @AgeRestrictCount) + 1 RANDRN
					FROM [dbo].[dt_Program]) PRAND ON PRAND.[ID] = P.[ID]
		LEFT JOIN (SELECT ID, ROW_NUMBER() OVER (ORDER BY ID) RN
					FROM [dbo].[dt_AgeRestriction]) AR ON AR.[RN] = PRAND.[RANDRN]
		
		DECLARE @EpisodeCounter INT  = 1
		
		WHILE @EpisodeCounter < 5
		BEGIN
			INSERT INTO [dbo].[dt_Video]
					   ([VideoTitle]
					   ,[Abstract]
					   ,[ImageURL]
					   ,[BillboardImageURL]
					   ,[EPGID]
					   ,[ProgramID]
					   ,[VideoProgramTypeID]
					   ,[AgeRestrictionID]
					   ,[AirDate]
					   ,[Runtime]
					   ,[Season]
					   ,[Episode]
					   ,[StartDate]
					   ,[EndDate]
					   ,[Keywords]
					   ,[CreatedDate]
					   ,[StatusID]
					   ,[ExpiryDate]
					   ,[RuntimeInMinutes]
					   ,[RentalPeriodInHours]
					   ,[Price]
					   ,[Geolocation]
					   ,[GroupID]
					   ,[CastCrewID])

			SELECT REPLACE([VideoTitle], 'Episode ' + CAST(@EpisodeCounter AS VARCHAR), 'Episode ' + CAST(@EpisodeCounter + 1 AS VARCHAR))
				   ,REPLACE([Abstract], 'Episode ' + CAST(@EpisodeCounter AS VARCHAR), 'Episode ' + CAST(@EpisodeCounter + 1 AS VARCHAR))
				   ,[ImageURL]
				   ,[BillboardImageURL]
				   ,[EPGID]
				   ,[ProgramID]
				   ,[VideoProgramTypeID]
				   ,[AgeRestrictionID]
				   ,[AirDate]-1
				   ,[Runtime]
				   ,[Season]
				   ,@EpisodeCounter + 1
				   ,[StartDate]-1
				   ,[EndDate]
				   ,[Keywords]
				   ,[CreatedDate]
				   ,[StatusID]
				   ,[ExpiryDate]
				   ,[RuntimeInMinutes]
				   ,[RentalPeriodInHours]
				   ,[Price]
				   ,[Geolocation]
				   ,[GroupID]
				   ,[CastCrewID]
			FROM [dbo].[dt_Video]
			WHERE [Episode] = @EpisodeCounter
			AND [ProgramID] IN (SELECT TOP 3 [ProgramID] FROM [dbo].[dt_Video]
									WHERE [Episode] = @EpisodeCounter
									AND [VideoProgramTypeID] = 1
									ORDER BY NEWID())
			
			SET @EpisodeCounter = @EpisodeCounter + 1
		END
	
	---------------------- 5.0 -----------------------
	DECLARE @VideoFileTypeTbl TABLE 
			(VideoFilePathID SMALLINT
			,VideoFileTypeID TINYINT
			,FileExtension VARCHAR(20)
			)
			
	INSERT INTO @VideoFileTypeTbl
			([VideoFilePathID]
			,[VideoFileTypeID]
			,[FileExtension]
			)
	SELECT TOP 1 10, ID, FileExtension FROM [dbo].[dt_VideoFileType] WHERE [Description] = 'WM Video - 1000K'
	UNION ALL
	SELECT TOP 1 13, ID, FileExtension FROM [dbo].[dt_VideoFileType] WHERE [Description] = 'Flash Video - 300K'

	INSERT INTO [dbo].[dt_VideoFile]
           ([VideoID]
           ,[FileName]
           ,[VideoFilePathID]
           ,[VideoFileTypeID]
           ,[ProductId]
           ,[ManItemId]
           ,[CreatedDate]
           ,[StatusID]
           ,[VideoDimension]
           ,[SizeInKB])
		SELECT V.[ID] AS ID
				,'/'+ P.[ProgramName] + VFT.[FileExtension] AS FileName
				,VFT.VideoFilePathID AS VideoFilePathID
				,VFT.VideoFileTypeID AS VideoFileTypeID
				,PR_P.[ProductID] AS ProductId
				,'' AS ManItemId
				,GETDATE() AS CreatedDate
				,1 AS StatusID
				,'536x400' AS VideoDimension
				,'23454667' AS SizeInKB
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[dt_Video] V ON V.[ProgramID] = P.[ID]
		INNER JOIN [dbo].[lt_Product_Program] PR_P ON PR_P.[ProgramID] = P.[ID]
		CROSS JOIN @VideoFileTypeTbl VFT
		
	---------------------- 6.0 -----------------------
	DECLARE @MovieGenreTbl TABLE
			([RN] INT
			,[THE_ID] NUMERIC(10,0)
			,[THE_DESCRIPTION] VARCHAR(50)
			,[STH_ID] NUMERIC(10,0)
			,[STH_DESCRIPTIONS] VARCHAR(50)
			)
			
	DECLARE @OtherGenreTbl TABLE
			([RN] INT
			,[THE_ID] NUMERIC(10,0)
			,[THE_DESCRIPTION] VARCHAR(50)
			,[STH_ID] NUMERIC(10,0)
			,[STH_DESCRIPTIONS] VARCHAR(50)
			)
	
	DECLARE @SubGenreHorrorID INT = 13
	DECLARE @SubGenreMovieCount INT
	DECLARE @SubGenreCount INT
	
	INSERT INTO @MovieGenreTbl
			([RN]
			,[THE_ID]
			,[THE_DESCRIPTION]
			,[STH_ID]
			,[STH_DESCRIPTIONS]
			)
	SELECT TOP 7 ROW_NUMBER() OVER (ORDER BY NEWID()) + 1 RN
		  ,G.[THE_ID]
		  ,RTRIM(G.[THE_DESCRIPTION])
		  ,SG.[STH_ID]
		  ,RTRIM(SG.[STH_DESCRIPTIONS])
	  FROM [dbo].[DStv_Genre_View] G
	  INNER JOIN [dbo].[DStv_SubGenre_View] SG ON G.THE_ID = SG.STH_THE_ID
	  WHERE [THE_DESCRIPTION] = 'Movies'
	  AND SG.[STH_ID] <> @SubGenreHorrorID
	  
	UNION 
	
	SELECT 1
		  ,G.[THE_ID]
		  ,RTRIM(G.[THE_DESCRIPTION])
		  ,SG.[STH_ID]
		  ,RTRIM(SG.[STH_DESCRIPTIONS])
	  FROM [dbo].[DStv_Genre_View] G
	  INNER JOIN [dbo].[DStv_SubGenre_View] SG ON G.THE_ID = SG.STH_THE_ID
	  WHERE [THE_DESCRIPTION] = 'Movies'
	  AND SG.[STH_ID] = @SubGenreHorrorID

	
	INSERT INTO @OtherGenreTbl
			([RN]
			,[THE_ID]
			,[THE_DESCRIPTION]
			,[STH_ID]
			,[STH_DESCRIPTIONS]
			)
	SELECT ROW_NUMBER() OVER (ORDER BY [STH_ID]) RN
		  ,G.[THE_ID]
		  ,RTRIM(G.[THE_DESCRIPTION])
		  ,SG.[STH_ID]
		  ,RTRIM(SG.[STH_DESCRIPTIONS])
	  FROM [dbo].[DStv_Genre_View] G
	  INNER JOIN [dbo].[DStv_SubGenre_View] SG ON G.THE_ID = SG.STH_THE_ID
	  WHERE [THE_DESCRIPTION] IN ('Series','Shows')

	SET @SubGenreMovieCount = (SELECT COUNT([THE_ID]) FROM @MovieGenreTbl)
	SET @SubGenreCount = (SELECT COUNT([THE_ID]) FROM @OtherGenreTbl)

	
	INSERT INTO [dbo].[lt_Program_Genre]
			   ([ProgramID]
			   ,[GenreID]
			   ,[GenreName]
			   ,[SubGenreID]
			   ,[SubGenreName]
			   ,[ThemeID]
			   ,[CreatedDate])
		SELECT P.ID
				--Insert the SubGenreID for the GenreID and the GenreID for the ThemeID
				--This is a work around for the CMS
				,MGT.STH_ID
				,MGT.THE_DESCRIPTION
				,MGT.STH_ID
				,MGT.STH_DESCRIPTIONS
				,MGT.THE_ID
				,GETDATE()
		FROM [dbo].[dt_Program] P
		INNER JOIN (SELECT P2.ID, (ROW_NUMBER() OVER (ORDER BY P2.[ID]) % @SubGenreMovieCount) + 1 AS GRN FROM [dbo].[dt_Program] P2) P_RN ON P_RN.ID = P.ID
		INNER JOIN @MovieGenreTbl MGT ON P_RN.GRN = MGT.RN
		WHERE P.ProgramTypeID = 2
		
		UNION ALL
		
		SELECT P.ID
				--Insert the SubGenreID for the GenreID and the GenreID for the ThemeID
				--This is a work around for the CMS
				,OGT.STH_ID
				,OGT.THE_DESCRIPTION
				,OGT.STH_ID
				,OGT.STH_DESCRIPTIONS
				,OGT.THE_ID
				,GETDATE()
		FROM [dbo].[dt_Program] P
		INNER JOIN (SELECT P2.ID, (ROW_NUMBER() OVER (ORDER BY NewId()) % @SubGenreCount) + 1 AS GRN FROM [dbo].[dt_Program] P2) P_RN ON P_RN.ID = P.ID
		INNER JOIN @OtherGenreTbl OGT ON P_RN.GRN = OGT.RN
		WHERE P.ProgramTypeID = 1
		
		IF NOT EXISTS (SELECT ID FROM [dbo].[lt_Program_Genre] WHERE [SubGenreID] = @SubGenreHorrorID)
		BEGIN
			SET ROWCOUNT 1
			
			UPDATE PG
			SET PG.GenreID = MGT.STH_ID,
				PG.GenreName = MGT.THE_DESCRIPTION,
				PG.SubGenreID = MGT.STH_ID,
				PG.SubGenreName = MGT.STH_DESCRIPTIONS,
				PG.ThemeID = MGT.THE_ID
			FROM [dbo].[lt_Program_Genre] PG
			INNER JOIN [dbo].[dt_Program] P ON P.[ID] = PG.[ProgramID]
			INNER JOIN @MovieGenreTbl MGT ON MGT.[STH_ID] = @SubGenreHorrorID
			
			SET ROWCOUNT 0
		END

	
		---------------------- 7.0 -----------------------
		DECLARE @EdListTbl TABLE
				([Title] VARCHAR(255)
				,[DisplayTitle] VARCHAR(255)
				,[CreatedDate] DATETIME
				,[ProductID] INT
				,[ProgramTypeID] INT)
				

		INSERT INTO @EdListTbl
				   ([Title]
				   ,[DisplayTitle]
				   ,[CreatedDate]
				   ,[ProductID]
				   ,[ProgramTypeID])
			VALUES
				   ('Title - Top 5 BoxOffice Movies', 'Top 5 BoxOffice Movies', GETDATE(), @BoxOfficeID, 2),
				   ('Title - Top 5 CatchUp Movies', 'Top 5 CatchUp Movies', GETDATE(), @CatchUpID, 2),
				   ('Title - Top 5 Shows', 'Top 5 Shows', GETDATE(), @CatchUpID, 1),
				   ('Title - Top 5 New BoxOffice Movies', 'Top 5 New BoxOffice Movies', GETDATE(), @BoxOfficeID, 2),
				   ('Title - Top 5 New CatchUp Movies', 'Top 5 New CatchUp Movies', GETDATE(), @CatchUpID, 2),
				   ('Title - Top 5 New Shows', 'Top 5 New Shows', GETDATE(), @CatchUpID, 1),
				   ('Title - Todays Top 5 BoxOffice Movies', 'Todays Top 5 BoxOffice Movies', GETDATE(), @BoxOfficeID, 2),
				   ('Title - Todays Top 5 CatchUp Movies', 'Todays Top 5 CatchUp Movies', GETDATE(), @CatchUpID, 2),
				   ('Title - Todays Top 5 Shows', 'Todays Top 5 Shows', GETDATE(), @CatchUpID, 1),
				   ('Title - This Month''s Top 5 BoxOffice Movies', 'This Month''s Top 5 BoxOffice Movies', GETDATE(), @BoxOfficeID, 2),
				   ('Title - This Month''s Top 5 CatchUp Movies', 'This Month''s Top 5 CatchUp Movies', GETDATE(), @CatchUpID, 2),
				   ('Title - This Month''s Top 5 Shows', 'This Month''s Top 5 Shows', GETDATE(), @CatchUpID, 1)
			
									
		INSERT INTO [dbo].[dt_EditorialList]
				   ([Title]
				   ,[DisplayTitle]
				   ,[StatusID]
				   ,[CreatedDate])
			SELECT [Title]
				   ,[DisplayTitle]
				   ,1
				   ,[CreatedDate]
			FROM @EdListTbl
			

		--Program ProgramTypeID: 1 = Shows; 2 = Movies
		--EdListItems ItemTypeId: 1 = Video; 2 = Program
		 				   
		DECLARE @EdID INT
		
		SET @EdID = (SELECT MIN([ID]) FROM [dbo].[dt_EditorialList])
		
		WHILE ISNULL(@EdID, 0) > 0
		BEGIN
			INSERT INTO [dbo].[lt_EditorialListItems]
					   ([EditorialListID]
					   ,[ItemID]
					   ,[ItemTypeId]
					   ,[ItemRank]
					   ,[CreatedDate])
				SELECT TOP 5 @EdID
						,VTBL.VID
						,1
						,ROW_NUMBER() OVER (ORDER BY OrderID)
						,GETDATE()
				FROM [dbo].[dt_EditorialList] EL
				INNER JOIN @EdListTbl ETBL ON EL.[Title] = ETBL.[Title] AND EL.[DisplayTitle] = ETBL.[DisplayTitle] AND EL.[CreatedDate] = ETBL.[CreatedDate]
				INNER JOIN (SELECT V.[ID] VID, P.[ProgramTypeID], P_P.[ProductID], NewId() OrderID
							FROM [dbo].[dt_Program] P
							INNER JOIN [dbo].[lt_Product_Program] P_P ON P_P.ProgramID = P.ID
							INNER JOIN [dbo].[dt_Video] V ON P.[ID] = V.[ProgramID]
							WHERE P_P.ProductID = [ProductID]
							AND V.VideoProgramTypeID IN (1,2,5)) VTBL ON VTBL.[ProgramTypeID] = ETBL.[ProgramTypeID] AND VTBL.[ProductID] = ETBL.[ProductID]
				WHERE EL.[ID] = @EdID
				ORDER BY OrderID
			
			SET @EdID = (SELECT MIN([ID]) FROM [dbo].[dt_EditorialList] WHERE [ID] > @EdID)
		END
				
					   
		---------------------- 8.0 -----------------------
		
		--SELECT '(' + ISNULL('''' + REPLACE([ProfessionName], '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + ', ' + CAST([Rank] AS VARCHAR) + ', ' + CAST([IsCast] AS VARCHAR) + ', ' + CAST([IsCrew] AS VARCHAR) + '),'
		--FROM [dbo].[dt_Profession]
		
		INSERT INTO [dbo].[dt_Profession]
			   ([ProfessionName]
			   ,[StatusID]
			   ,[Rank]
			   ,[IsCast]
			   ,[IsCrew])
		VALUES ('Director', 1, 1, 0, 1),
				('Actor', 1, 5, 1, 0),
				('Producer', 1, 2, 0, 1),
				('Writer', 1, 4, 0, 1),
				('Photographer', 1, 7, 0, 1),
				('Guest Actor ', 1, 8, 1, 0),
				('Co-Producer', 1, 3, 0, 1),
				('Actress', 1, 6, 1, 0),
				('Guest Actress', 1, 9, 1, 0)
				
		--SELECT '(' + ISNULL('''' + REPLACE(FullName, '''','''''') + '''', 'NULL') + ', ' + CAST(ProfessionID AS VARCHAR) + ', ' + ISNULL('''' + REPLACE(Bio, '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE(Gender, '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE(BirthPlace, '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE(BirthDate, '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE(ImageURL, '''','''''') + '''', 'NULL') + ', ' + ISNULL('''' + REPLACE(AwardsText, '''','''''') + '''', 'NULL') + ', ''' + CONVERT(VARCHAR, CreatedDate, 120) + ''', ' + CAST(StatusID AS VARCHAR) + '),'
		--FROM [dbo].[dt_Person]

		INSERT INTO [dbo].[dt_Person]
			   ([FullName]
			   ,[ProfessionID]
			   ,[Bio]
			   ,[Gender]
			   ,[BirthPlace]
			   ,[BirthDate]
			   ,[ImageURL]
			   ,[AwardsText]
			   ,[CreatedDate]
			   ,[StatusID])
			VALUES ('Michael C Hall', 1, 'Michael C Hall plays Dexter Morgan with a perfect mix of charm and menace. For a man with a history of playing conflicted, secretive characters, his rise to the top has been remarkably conventional. <br />  <br />  Hall completed his MFA at New York University and began his acting career on stage. He starred in several plays, including Macbeth and Henry V, before landing a part in Sam Mendes� production of Cabaret in 1999. <br />  <br />  At the time, Mendes was working with writer Alan Ball on the film American Beauty. When Ball began casting his new TV series, Six Feet Under, Mendes recommended Hall for the role of the repressed David Fischer. Hall grasped his opportunity with both hands, earning himself an Emmy nomination for Outstanding Lead Actor in 2002. <br />  <br />  By the time Six Feet Under ended in 2005, Hall had established his leading man credentials, as well as his ability to play compelling characters leading double lives. He made his debut as Dexter Morgan in 2006 and has since gone on to receive three Golden Globe nominations, as well as an Emmy nomination for the role. <br />  <br />  Trivia: <br />  In 2008, Hall eloped with Jennifer Carpenter, who plays Dexter�s sister Deb. The couple were married on the Dec 31 2008, in California. <br />  ', 'M', '', '', '/MMS2/Content/Images/CastMichaelCHall.jpg', '', '2010-04-01 14:41:04', 4),
				('Eric Dane', 1, 'Born in San Francisco, California, Eric Dane caught the acting bug after appearing in a high school production of All My Sons. Shortly afterwards he relocated to Los Angeles where he landed small roles in television series like The Wonder Years, Saved by the Bell and Married with Children. In 2000 he landed a recurring role in Gideon''s Crossing and followed this with a two-season role in Charmed. <br />  <br />  Dane''s film credits include Feast, X-Men: The Last Stand and Open Water:Adrift. He will soon be seen in Marley and Me opposite Owen Wilson and Jennifer Aniston. <br />  <br />  He is married to actress/model Rebecca Gayheart. <br />  ', 'M', ' San Francisco, California, USA ', ' 9 November 1972', '/MMS2/Content/Images/CastEricDane.jpg', '', '2010-04-01 15:17:05', 1),
				('Hugh Laurie', 1, 'James Hugh Calum Laurie was born on June 11, 1959, and was the youngest of four children He represented England''s Youth Team at the 1977 World Championships where he finished fourth.He is the father of three children and has been married to his wife for 19 years. <br />  <br />  Laurie may not be a doctor in real life but he did get a good education, he completed his University degree at the prestigious Cambridge University. There he met and made friends with Emma Thompson and Stephen Fry and together they launched successful careers. Fry and Laurie worked together in the comedy skit show, A Bit of Fry and Laurie. He also acted alongside Rowan Atkinson, better known as Mr. Bean, in the hit comedy series Blackadder. <br />  <br />  In his role as the bad-tempered Dr. Gregory House in M-Net Series� House, he has earned great respect in the United States and has won several awards. <br />  It was recently reported that Laurie received a huge bonus , making him one of the highest paid actors on television. <br />  <br />  Laurie is is a fan of vintage cars and his dream is to travel cross-county through America in a vintage motorcycle before his 50th birthday, well he better hurry and prepare because that�s less than a year away! <br />  ', 'M', '', '', '/MMS2/Content/Images/CastHouse6.jpg', '', '2010-04-01 15:23:14', 1),
				('Derek Watts', 1, 'Derek Watts has been a journalist for more than 25 years, presenting on South African television since 1985. Derek has been an anchor and presenter on Carte Blanche since the programme''s inception in 1988.   ', 'M', '', '', '/MMS2/Content/Images/News_CarteBlanche_Derek.jpg', '', '2010-04-01 15:41:10', 1),
				('Anne Hathaway', 1, 'Anne Hathaway is an American actress who made her debut in the 1999 television series Get Real. After it was cancelled, she was cast as Mia Thermopolis in the Disney family comedy The Princess Diaries, from which her career gained momentum. Over the next three years, Hathaway continued to star in family films, reprising the role for its sequel, and appearing as the titular character in Ella Enchanted.<br />  <br />  Interested in other projects, Hathaway began a career transition with supporting roles in Havoc and Brokeback Mountain. She subsequently co-starred with Meryl Streep in The Devil Wears Prada.<br />  <br />  In 2008, she earned widespread critical acclaim for her lead role in the film Rachel Getting Married, for which she won numerous industry awards, and was nominated for the Academy Award for Best Actress. In 2010, she starred in the box office hits Valentine''s Day and Tim Burton''s Alice In Wonderland. <br />  <br />  Hathaway''s acting style has been compared to Judy Garland and Audrey Hepburn.<br />  ', 'F', '', '', '/MMS2/Content/Images/CastAnneHathaway.jpg', '', '2010-04-01 15:51:54', 1),
				('Anna Paquin', 1, 'Anna Paquin, who plays Sookie Stackhouse in True Blood, has taken an unconventional path to fame. Much like Sookie, Paquin is a determined individual who stands out and follows her own path. <br />  <br />  Paquin�s acting career began when she attended an open audition for The Piano at age 11. Despite not having any formal training, she impressed director Jane Campion so much that she was cast in the integral role of Flora McGrath, daughter of the main protagonist played by Holly Hunter. <br />  <br />  Campion obviously saw something special in Paquin, and it turns out that the Academy voters shared her opinion. In 1994, Paquin became the second youngest Oscar winner in history when she won Best Supporting Actress for her role in The Piano. She was only 11 years old. <br />  <br />  Predictably, the movie offers began to role in, but Paquin elected to keep a low profile. She remained largely under the radar until 2000 when she appeared in Bryan Singer�s blockbuster comicbook adaptation X-Men. Paquin played the role of Rogue, a tough but vulnerable mutant with the ability to absorb powers from others. She went on to reprise the role in X2: X-Men United and X-Men: The Last Stand. <br />  <br />  In 2007 Paquin was nominated for an Emmy, a Golden Globe and a Screen Actors� Guild Award for her role as Elaine Goodale in the TV movie Bury My Heart at Wounded Knee (screening on MM2 in Feb). She obviously enjoyed working on the small screen and followed this up with her role as Sookie Stackhouse in 2008. Once again Paquin proved to have the winning touch, as her portrayal of Sookie won her a Golden Globe for Best Actress in a Drama Series at the beginning of 2009. <br />  <br />  ', 'F', '', '', '/MMS2/Content/Images/CastAnnaPaquin.jpg', '', '2010-04-01 16:16:20', 1),
				('Christine Baranski ', 1, 'Christine Baranski is a two-time Tony, Emmy, Screen Actors Guild and American Comedy Award winner. Born in Buffalo, New York, she attended the famed Juilliard school. <br />  <br />  She made her Broadway debut in 1980 in <em>Hide &amp; Seek</em> after which she appeared in <em>The Real Thing</em>, <em>Rumors</em> and <em>Hurlyburly</em>. Flim credits include <em>Mama Mia</em>, <em>The Birdcage</em>, <em>Cruel Intentions</em> and <em>How the Grinch Stole Christmas</em>. <br />  <br />  One of her best-known television roles is that of Cybill Shepherd''s hard-drinking friend Maryanne in the show Cybill. She has also hosted Saturday Night Live and appeared Happy Family, Frasier, Ugly Betty and The Big Bang Theory. <br />  <br />  In addition to her Emmy for the comedy Cybill, Baranski received an American Comedy Award for Outstanding Supporting Actress in a Comedy, as well as a Screen Actors Guild Award for Outstanding Lead Actress in a Comedy. She also received four additional Emmy nominations, one of which was for guest starring in Frasier, and three Golden Globe nominations. <br />  <br />  A native of Buffalo, Baranski now lives with her husband and two children in Connecticut. <br />  ', 'F', ' Buffalo, New York, USA', ' 2 May 1952', '/MMS2/Content/Images/DStv/Cast/ChristineB_GoodWife_Cast.jpg', '�American Comedy Award: Funniest Supporting Female Performer in a TV Series <br />  for: "Cybill" (1995)<br />  Emmy: Outstanding Supporting Actress in a Comedy Series <br />  for: "Cybill" (1995) <br />  <br />  ', '2010-04-13 08:50:54', 1),
				('James Lafferty', 1, 'Growing up, James Lafferty had two great passions in life, acting and basketball. When in high school in his hometown Hemet, California, he managed to balance a lead spot on his high school basketball team with a rising acting career. During this time he landed a role in the independent movie <em>Boys on the Run</em>. <br />  <br />  After a stint on the show Emeril, he landed a dream gig in ESPN''s, A Season on the Brink, a show that allowed him to act while showcasing his basketball prowess in front of the camera. He also made guest appearances on series like Once and Again and Boston Public. <br />  <br />  Currently, Lafferty runs a production company called Swingman Productions, in addition to his work on One Tree Hill. He is also a diligent host of charity basketball events. <br />  ', 'M', ' Hemet, California, USA', ' 25 July 1985', '/MMS2/Content/Images/DStv/Cast/CastJamesLafferty.jpg', '�Nominated: Teen Choice Award Choice TV Chemistry <br />  for One Tree Hill (2003) <br />  Shared with: <br />  Chad Michael Murray <br />  <br />  Nominated: Teen Choice Award Choice Breakout TV Star - Male <br />  for One Tree Hill (2003)   ', '2010-04-13 09:57:23', 1),
				('Nina Dobrev', 1, 'Nina Dobrev developed a love for the arts at a young age. Aside from participating in international gymnastics as a teen she also did commercial modelling, which eventually led to film and television. <br />  <br />  Well known in her home Canada, Dobrev has appeared in the Canadian television movies <em>Away from Her</em> and <em>Fugitive Pieces</em>. She also appeared in the MTV movie musical <em>The American Mall</em> in which she played the lead character Ally. <br />  <br />  Other than The Vampire Diaries, Dobrev is well-known for her role in Degrassi: The Next Generation. <br />  ', 'F', ' Sofia, Bulgaria ', ' 9 January 1989', '/MMS2/Content/Images/DStv/Cast/CastNinaDebrov.jpg', '', '2010-04-13 10:09:22', 1),
				('Eliza Dushku', 1, 'Born to an Albanian American father and a Danish/English mother, Eliza Dushku came to the attention of casting agents when she was just 10 years old. <br />  <br />  She quickly landed the role of Alice in <em>That Night</em> opposite Juliette Lewis after which she starred alongside Robert De Niro and Leonardo DiCaprio in <em>This Boy''s Life</em>, a role that she credits with opening many doors for her. <br />  <br />  After taking time off from acting to graduate from high school, Dushku returned to the limelight in the hugely popular series Buffy the Vampire Slayer. Other television credits include Tru Calling, Angel, That 70''s Show and Ugly Betty. Her feature film credits include <em>Wrong Turn</em>, <em>Race the Sun</em>, <em>Bring It On</em> and <em>The New Guy</em>. <br />  <br />  She currently lives in Los Angeles and is the CEO of her production company Boston Diva. <br />  ', 'F', ' Boston, Massachusetts, USA ', ' 30 December 1980', '/MMS2/Content/Images/DStv/Cast/CastElizaDushku.jpg', 'Nominated: Saturn Award Best Actress in a Television Series <br />  for Tru Calling (2003) <br />  <br />  Nominated: Teen Choice Award Choice Breakout TV Star - Female <br />  for Tru Calling (2003) <br />  <br />  ', '2010-04-13 11:56:25', 1),
				('Simon Baker', 1, 'Simon Baker rose to fame in Australia after appearing in various soap operas and music videos. Upon relocating to Los Angeles with his family, Baker was immediately cast in the Academy Award-winning film <em>L.A. Confidential.</em> <br />  <br />  He has held supporting and lead roles in other feature films, nabbing roles in <em>Red Planet,</em> <em>The Affair of the Necklace</em>, <em>Land of the Dead</em>, <em>Book of Love</em>, <em>The Devil Wears Prada</em> and <em>Something New</em>. He has also earned various accolades from magazines for his good looks. <br />  <br />  Married to Rebecca Rigg since 1998, Baker has three children, named Stella, Claude, and Harry. <br />', 'M', ' Launceston, Tasmania, Australia ', ' 30 July 1969', '/MMS2/Content/Images/DStv/Cast/CastSimonBaker.jpg', 'Nominated: AFI Award Best Performance by an Actor in a Leading Role in a Telefeature or Mini-Series for Secret Men''s Business (1999) <br />  <br />  Nominated: Emmy Outstanding Lead Actor in a Drama Series for The Mentalist (2008)&nbsp;<br />  <br />  Nominated: Golden Globe Best Performance by an Actor in a Television Series - Drama for The Mentalist (2008) <br />  <br />', '2010-04-13 12:47:47', 1),
				('Vanessa Williams', 1, 'Vanessa Williams made history in 1983, when she became the first African American women to be crowned Miss America. After a scandal resulted in the loss of her title, the star began to focus on a singing career, releasing her first album on 1988. She has since earned multiple awards, including a GRAMMY, Academy Award and Golden Globe for her song "Colours of the Wind" which featured in the movie <em>Pocahontas</em>. <br />  <br />  She began her career as an actress on Broadway. Her feature film credits include <em>Under the Gun, Dance with Me, Johnson Family Vacation </em>and<em> Eraser</em>, amongst others. <br />  <br />  Television credits include appearances in The Boy who Loved Christmas and Don Quixote. But, it is her appearance as scheming Wilhelmina Slater on Ugly Better, that has earned her critical acclaim and an Emmy Nomination. <br />  <br />  Her most recent appearance was in <em>Hannah Montana: The Movie</em>, opposite teen sensation Miley Cyrus. <br />  ', 'F', ' Tarrytown, New York, USA ', ' 18 March 1963', '/MMS2/Content/Images/DStv/Cast/Cast_VanessaWilliams.jpg', 'Nominated: Emmy Outstanding Supporting Actress in a Comedy Series <br />  for Ugly Betty (2006) <br />  <br />  Won Image Award Outstanding Supporting Actress in a Comedy Series <br />  for Ugly Betty (2006) <br />  <br />  Nominated Teen Choice Award Choice TV Villain <br />  for Ugly Betty (2006) <br />  <br />  ', '2010-04-13 13:00:03', 1),
				('Joshua Jackson', 1, 'Most widely recognised and loved for his role as Pacey Witter, the fast-talking best friend of Dawson in the hit Dawson''s Creek, Joshua Jackson has been working in front of the camera for over 15 years. <br />  <br />  His recent works include the critically acclaimed movie <em>Bobby</em>, which was directed by Emilio Estevez. He also spent time on the stage in the comedy <em>A Life in the Theatre</em>, starring alongside Patrick Stewart. <br />  <br />  He made his feature film debut in the movie <em>Crooked Hearts</em>, which was followed by the popular <em>Mighty Ducks</em> trilogy. His credits include <em>Cruel Intentions</em>, <em>Gossip</em>, <em>Apt Pupil </em>and <em>The Skulls</em>, amongst others. <br />  ', 'M', ' Vancouver, British Columbia, Canada ', ' 11 June 1978', '/MMS2/Content/Images/DStv/Cast/CastJoshuaJackson.jpg', '�Nominated: Teen Choice Award Choice TV Actor Drama <br />  for Fringe (2008) <br />  <br />  Nominated: Teen Choice Award Choice TV Actor Drama/Action Adventure <br />  for Dawson''s Creek (1998) <br />  <br />  ', '2010-04-13 14:31:49', 1),
				('Jason Greer', 1, 'Most recently a continuity presenter for television channel SABC 3, he is also well-known for hosting and presenting various shows and events. These include: the SABC2 SAFTA Red Carpet Event, the Waterfall Mall Fashion Show, Mr and Miss Alberton (2007), Miss Rivonia (2007), Jo�burg Fashion Week, (2008) and the glamorous J&amp;B Met. <br />  <br />  DStv fans might remember him as the presenter of GO''s high-energy show The Loot. Jason also does his bit for charity, hosting, amongst others, the S.P.C.A charity golf day. <br />  <br />  Did you know? <br />  <br />  Jason is an accomplished trainer on AutoCAD, an architectural design programme. <br />  <br />  Make sure you tune in to M-Net�s All Access every Thursday night to see more of this talented young personality. <br />  <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastJasonGreer.jpg', '', '2010-04-13 14:35:51', 1),
				('Devi Sankaree', 1, 'Devi Sankaree began her career in journalism at the age of 21 when she secured a freelance radio position at the SABC, while completing a BA degree at the University of Natal in Durban. She quickly moved from presenting music shows and reading the news, to becoming a talk show host for national public broadcaster, Lotus FM. Devi�s daily radio chat show, which ran for nine years, was well known for its straightforward approach to tackling sensitive and controversial issues. <br />  <br />  Devi�s television break came in 1996 when she joined SABC 1�s Eastern Mosaic as a continuity presenter. While keeping her radio and television career on the boil, in 1998, Devi then began work as a weekly columnist for the Sunday Times, which she still manages to find time to do. A year later she joined the Sunday Times as their Features Editor in Durban. <br />  <br />  In January 2002, Devi joined the Carte Blanche team. Her more memorable stories have included interviews with Kenyan Nobel Laureate, Warangi Mathai, international financial guru Robert Kiyosaki and music icons Quincy Jones and Lionel Ritchie. <br />  <br />  ', 'F', '', '', '/MMS2/Content/Images/DStv/Cast/CastDeviSankaree.jpg', '', '2010-04-13 14:47:14', 1),
				('Bongani Bingwa', 1, 'Bongani Bingwa has worked on television on a variety of shows beginning in 1993 when he was still a student. <br />  <br />  He has hosted shows on business, entertainment and news. His career in journalism began on radio as a newsreader and talk show host. <br />  <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/Cast_BonganiCBMedical.jpg', '', '2010-04-13 14:58:56', 1),
				('Darren ''Wackhead'' Simpson', 1, 'Darren �Wackhead� Simpson has plied his particular brand of humour as part of Highveld Stereo''s Rude Awakening Team and at fun functions across the country � but his clever comic touches have never been more at home than they are on wacky series Clipz! <br />  <br />  Wackhead hails from Durbs but his quick wit and sometimes crude touch have played out in the nation�s seedy comedy clubs, theatres and many comedy concerts over the past few years. He was most recently seen on the box when he co-hosted M-Net''s Laugh Out Loud series alongside Jeremy Mansfield. <br />  <br />  Well known for his one-of-a-kind prank calls and the Celebrity Challenge, we reckon his love of all things funny makes him the perfect Clipz anchorman. <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/Cast_DarrenSimpson.jpg', '', '2010-04-13 15:07:20', 1),
				('Felicity Huffman', 1, 'Felecity Huffman�s character, Lynette Scavo, is the perfect embodiment of a Desperate Housewife. If there�s one thing that Huffman knows, its how to perfectly embody whatever role she is playing. <br />  <br />  Huffman studied drama at New York University�s prestigious Tisch School of the Arts. After graduating in 1998 she began her acting career with a series of small roles and appearances in films and TV shows such as The X Files, Law and Order and Chicago Hope. Her first recurring role came on critically-acclaimed producer Aaron Sorkin�s Sports Night, which ran from 1998 to 2000. <br />  <br />  When Sports Night ended, Huffman returned to her hard-working ways, appearing in shows such as The West Wing and Frasier. She hit the big time in 2004, when she was cast as Lynette in Desperate Housewives and also landed roles in the feature films <em>Raising Helen</em> with Kate Hudson and Joan Cusack and <em>Christmas with the Kranks </em>with Tim Allen and Jamie Lee Curtis. <br />  <br />  In 2005 Huffman�s hard work was rewarded when she won an Emmy for her portrayal of Lynette Scavo. She was nominated for a Golden Globe for the same role. The following year, her lead in the critical hit <em>Transamerica</em> won her a Golden Globe and earned her an Oscar nomination for Best Actress. <br />  <br />  Huffman�s latest film, <em>Phoebe in Wonderland</em> recently opened in America and she will next be seen on the big screen in Come Back to Sorrento opposite her husband and fellow Oscar-nominee William H Macy. <br />  <br />  <br />  ', 'F', ' Bedford, New York, USA ', ' 9 December 1962', '/MMS2/Content/Images/DStv/Cast/CastFelicityHuffman.jpg', 'Nominated: Oscar Best Performance by an Actress in a Leading Role <br />  for Transamerica (2005)<br />  <br />  Won: Emmy Outstanding Lead Actress in a Comedy Series <br />  for Desperate Housewives (2004) <br />  ', '2010-04-13 15:14:38', 1),
				('Chace Crawford', 1, 'Born in Texas, Chace Crawford attended Pepperdine University in Malibu where he studied broadcast journalism. <br />  <br />  After struggling to decide on a major, he was encouraged by his mother to pursue acting. He appeared in the 2006 feature film <em>The Covenant</em> and landed the role of Nate Archibald on Gossip Girl shortly after. In 2009 he was named Summer''s Hottest Bachelor by People magazine. <br />  <br />  He will next play a drug dealer in Joel Schumacher''s <em>Twelve</em> and has been tapped to play Ren McCormack in a remake of <em>Footloose</em>. <br />  ', 'M', ' Lubbock, Texas, USA ', ' 18 July 1985', '/MMS2/Content/Images/DStv/Cast/Cast_ChaceCrawford.jpg', 'Won: Teen Choice Award Choice TV Actor Drama <br />  for Gossip Girl (2007)<br />  <br />  Nominated: Teen Choice Award Choice TV Actor Drama <br />  for Gossip Girl (200 <br />  ', '2010-04-13 15:24:15', 1),
				('Lise Swart', 1, 'Lise Swart is �n Suid-Afrikaanse sangeres en aktrise wat veral bekend geword het as mede-aanbieder van die tweede seisoen van die musiekprogram Pitstop saam met Alvin Bruinders in 2006. Sy het ook Roer! op kykNET van 2004 tot 2005 aangebied. <br />  <br />  Lise is gebore in Kaapstad en het gematrikuleer aan die Ho�rskool Hottentots Holland in Somerset Wes in 1996. Sy het BA Drama by Stellenbosch Universiteit van 1998 tot 2000 studeer, waarna sy besluit het om ''n sangloopbaan te volg. <br />  <br />  Sy was ook al agtergrondsangeres vir verskeie van SA se top kunstenaars, insluitende Gert Vlok Nel, Laurinda Hofmeyer en Anton Goosen. Boonop het sy in vertonings soos �Om te Breyten - ''n produksie'' by die KKNK opgetree saam met Amanda Strydom, Piet Botha en Valiant Swart. <br />  <br />  Lise is ''n ervare reisiger en het van 2001 tot 2003 in Engeland gebly, waar sy ondervinding in verskeie velde opgedoen het, wat wissel van bestuursposisies tot assistent maatskaplike werker by ''n rehabilitasiesentrum vir dwelmverslawing. Gedurende haar reise het sy ook ondervinding opgedoen in die musiekindustrie en vertonings in Londen gedoen. <br />  <br />  Nadat sy in 2004 na Suid-Afrika teruggekeer het, het sy die Roer! aangebied vir twee seisoene. Daarna het sy die rol van aanbieder van Pitstop by haar ouer suster, Nina Swart, oorgeneem. Nina het bekend geword as Willemien in 7e Laan. <br />  <br />  Sy het ook die derde seisoen van die realiteits modelkompitisie, Revlon Supermodel saam met Michelle McLean aangebied. <br />  ', 'F', '', '', '/MMS2/Content/Images/DStv/Cast/CastLiseSwart.jpg', '', '2010-04-14 17:13:36', 1),
				('Neil Flynn', 1, 'Neil Flynn began acting in theatre productions while in high school. After college he pursued a career in live theatre and appeared on stage alongside Aidan Quinn and Brian Dennehy, among others.<br />  �<br />  He has appeared in movies like Home Alone III, The Fugitive and Chain Reaction. His television credits include Boston Public, NYPD Blue, That 70s Show and Ellen.<br />  �<br />  A sports fan, Flynn plays on a local softball team and also enjoys a weekly game of poker.   ', 'M', 'Chicago', '13 November, 1960', '/MMS2/Content/Images/CastNeilFlynn.jpg', '', '2010-04-14 17:26:42', 1),
				('Wikus Du Toit', 1, 'Wikus Du Toit wen onder andere twee FNB-Vita Toekennings vir sy spel in El Grande de Coca-Cola en play@risk. Hy ontvang ook in 1996 die deKat prys vir sy debuut op die KKNK met sy kabarert �Ses�. <br />  <br />  Wicus wen in 2005 die Nagtegaal/ KKNK Teksskryfkompetisie vir sy kabaret �10 gesprekke oor die man wat nie wou huil nie�, die eerste Afrikaanse musical �Antjie Somers� waarvan hy die mede-komposis was wen �n Fleur de Cap en hy wen ook die ATKV Crescendo Nasionale Sangkompetisie. <br />  <br />  Hy het kursusse bygewoon in Koormusiek aan die Universiteit van Graz in Oostenryk en ook in Sang aan NYU onder die meester van die musiekblyspel, John Kander wat verantwoordelik was vir werke soos Chicago en Cabaret. Hy�s genomineer vir twee Smeltkroes-pryse vir nuwe tekste by die Aardklop kunstefees en palm ook twee Aardvark nominasies in vir nuwe innoverende werk in play@risk en vir sy teks en regie van Elzab� Zietsman se vertoning smallchange. <br />  <br />  Wicus was al in meer as 50 verhoogproduksies, 10 TV-reekse en vier films. Hy het ook gedien op inniBos Kunstfees se keuringspaneel en was �n beoordelaar vir die KKNK se Kanna-pryse. Hy is ook �n SAMA-beoordeelaar. <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastWicusPreosstraat.jpg', '', '2010-04-14 17:29:23', 1),
				('Yvonne van den Bergh', 1, 'Yvonne van den Bergh behaal die graad BA Drama aan die Universiteit van Pretoria en word gou �n bekende gesig op plaaslike TV. As aktrise speel sy in reekse soos Jan Scholtz se Die 4de Kabinet en as aanbieder, op SABC2 se Pasella. Hierna spandeer sy twee jaar op die stel van 7de Laan, in die rol van Petra Malherbe Terblanche, vrou van Jan-Hendrik en die Laan se argitek. Hierna volg kykNET se ORION, gebaseer op die Deon Meyer topverkoper en sy tree ook op as mede-aanbieder van kykNET se SaterdagNag, �n kletsprogram oor bekendes se skandes, saam met Jacques du Preez en Deon Maas. <br />  <br />  Internasionale gehore leer haar ken in die vroulike hoofrol van Promised Land, gebaseer op Karel Schoman se Na die Geliefde Land. Sy het al opgetree in tientalle internasionale produkadvertensies, telkens in die Skandinawiese lande, Australi� ensovoorts. <br />  <br />  Yvonne werk gereeld as stemkunstenaar vir plaaslike radiodramas en advertensies. <br />  <br />  Wanneer sy nie voor die kamera�s werk nie, spandeer sy haar tyd by haar huis in JHB, waar sy, haar man en twee tienerdogters woon. Hier lees, oefen, kook en bak sy en bedryf �n spysenieringsbesigheid met die naam SMACK! Addictive Dining. <br />  ', 'F', '', '', '/MMS2/Content/Images/DStv/Cast/GlitteratiYvonneCast.jpg', '', '2010-04-14 17:33:34', 1),
				('Matthew Fox', 1, 'Matthew Fox got into acting through modelling, a job he fell into while attending Columbia University on a football scholarship. He studied economics and graduated with hopes of one day becoming a Wall Street Broker, but modelling jobs led to television commercials and acting jobs. He went on to study at The School for Film and Television in New York City for two years. <br />  <br />  After small acting parts on series and movies made for TV, Fox got his big break in 1994 when he was cast as Charlie, the sensitive older brother in a family of orphans on the M-Net Series drama, Party of Five. During his Party of Five years, the actor set many young girls� hearts on fire, appearing on teen magazines and posters. People magazine even named him as one of the 50 Most Beautiful People in the World in 1996. <br />  <br />  When Lost debuted in 2004 the actor once again joined the ranks of TV''s hottest hunks. The success of the series led to feature film roles, including We Are Marshall and Smokin'' Aces. <br />  <br />  For Lost, he originally auditioned for the role of Sawyer, but the show�s creator, J.J. Abrams, was so impressed with Fox�s reading of the lead character, Dr. Jack Shepard, that he offered him the part. <br />  <br />  Matthew married Margherita Ronch in 1991. They have two children. He and his family currently reside in Hawaii, near the set of Lost.   ', 'M', '14 July, 1966', 'Abington, Pennsylvan', '/MMS2/Content/Images/CastMatthewFox.jpg', 'He has received worldwide recognition for his role, including a 2006 Golden Globe Award nomination for Best Actor in a Drama. Fox won a Golden Satellite Award in 2005 and shared the 2006 Screen Actors Guild Award for Outstanding Performance by an Ensemble in a Drama Series with the Lost cast.   ', '2010-04-14 17:35:02', 1),
				('Francois Toerien', 1, 'Vanaf die begin van Julie is ou kykNET bekende terug op die kanaal met die blou stoel in die rol van aanbieder van DKNT. <br />  <br />  Dis reg ja, Francois Toerien het oorgeneem by Derrich Gardner as aanbieder van kykNET se gewilde musiekprogram. Francois is geen vreemdeling by die kanaal nie. kykNET kykers sal hom dalk onthou vir programme soos Klein maar Getrein Kniediep en Woordwoeker of selfs net as kontinu�teitsaanbieder op die kanaal. <br />  <br />  Francois Toerien is man van vele talente. Hy het lank reeds naam gemaak as vryskut-akteur, regisseur, vervaardiger, skepper van die sketskomedie toneelstuk, ''Die Francois Toerien Show'' en stemkunstenaar. En nou kuier hy elke aand in jou TV-kamer as hy jou die nuutste van die Top CD-verkopers van die week bring. <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastDKNTfRANCOIS.jpg', '', '2010-04-14 17:39:11', 1),
				('Mark-Paul Gosselaar', 1, 'Born in California, Mark-Paul Gosselaar first came to public notice in the role of Zack Morris in the hit television series Saved by the Bell.<br />  �<br />  His first feature film appearance was in 1998''s <em>Dead Man on Campus</em> which was followed by roles in Hyperion Bay, NYPD Blue and D.C. Other television credits include Hitched, Law and Order: Special Victims Unit and John from Cincinatti.<br />  �<br />  He has been married to Lisa Ann Russell since 1996.   ', 'M', 'March 1, 1974', 'Panorama City, Calif', '/MMS2/Content/Images/CastMarkPaulGosselaar.jpg', '', '2010-04-14 17:42:39', 1),
				('Marietta Theunissen', 1, 'Mari�tta Theunissen is �n heldersiende wat al �n bekende gesig op kykNET is, aangesien daar al vorige reekse van haar program Die Anderkant in 2004 en 2005 op die kanaal aangebied het. Sy is gebore in Windhoek, Namibi� met haar naelstring wat twee keer om haar nek gedraai was. Sy het vir twee weke geveg om haar lewe en, alhoewel sy oorleef het, was sy dikwels as kind baie siek. Dit het daartoe gelei dat sy meeste van die tyd in haar eie w�reld van fantasie en verbeelding geleef het. <br />  <br />  Marietta is gebore met ''n intense, natuurlike gawe waarmee sy gebeurtenisse in die toekoms kan sien. Sy het die afgelope 20 jaar deur professionele opleiding hierdie talent verder ontwikkel. Die gawe loop glo in die familie en dateer so ver terug as haar oer-oumagrootjie. <br />  <br />  Haar eerste deurbraak het in haar laat twintigs gekom toe haar ho�rskoolliefde in ''n ongeluk oorlede is. Hy het daarna met haar kontak gemaak en selfs vandag nog, wanneer sy twyfel, ontvang sy tekens en geskenke van hom af. <br />  <br />  Toe reeds het sy besluit om haarself heeltemal toe te wy aan hierdie nuwe rigting wat haar lewe ingeslaan het en het sy op alle vlakke - numerologie, astro-logie, aura, chakras en meer - begin studeer. <br />  <br />  Juis hierdie vaardighede help haar om kli�nte/pasi�nte te help om van blokkasies ontslae te raak of om vas te stel of die per-soon reeds die moeilikheid oorkom het. Wanneer sy nie tuis is nie, reis sy op versoek w�reldwyd om slypskole aan te bied en doen sy ook telefoniese berading en terapie. Sy was reeds verskeie kere ''n gasspreker vir radiogesprekke in Namibi� en het die afgelope twee jaar meer as 30 seminare en slypskole hier aangebied. <br />  <br />  Haar modus operandi v��r elke vertolking bly dieselfde. "Ek bid vir leiding voor elke vertolking en ek sluit elke sessie af met ''n gebed," vertel sy. Sy het ook haar eie rituele soos kerse brand en wierook om haar omgewing te suiwer. <br />  <br />  Ten opsigte van "sien", s� Marietta dat alles soos ''n fliek voor haar o� verbyflits en dat sy alles voel. "So, ek kan na willekeur myself verplaas in ander mense se gevoelens, omstandighede en omgewings, maar ek doen dit nie sonder om gevra te word of sonder toestemming nie." <br />  <br />  ', 'F', '', '', '/MMS2/Content/Images/DStv/Cast/CastTheunissen.jpg', '', '2010-04-14 17:43:13', 1),
				('John Henson', 1, 'Born in Connecticut, John Henson is a comedian, talk show host and actor. <br />  <br />  After studying acting at Boston University, he starred in various theatre productions including &lt;i&gt;Conduct Unbecoming&lt;/i&gt; and &lt;i&gt;The Greenhouse Effect&lt;/i&gt;. Previously the host of Talk Soup on E! Entertainment, he has also hosted several broadcast specials. <br />  <br />  He currently co-hosts Wipeout opposite John Anderson. <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastJohnHenson.jpg', '', '2010-04-14 17:46:45', 1),
				('Nico Panagio', 1, 'He is known to millions of local television viewers for his previous roles in several soaps and dramas, and as the host of popular local magazine shows. <br />  <br />  However, this rugged man is happiest when he has a chance to explore the great outdoors. Apart from surfing, horse riding and hiking, he is also an avid mountain biker and has qualified as and Advanced Diver. <br />  <br />  This, along with his training in body guarding and paramedics, and expertise in three martial arts disciplines, equip him superbly for the job of hosting Survivor. <br />  <br />  Helen Smit, M-Net�s Head of Local Content, says this season of Survivor South Africa puts a whole new spin on the familiar format. �The celebs are being hand-picked to ensure non-stop entertainment. They have fiery artistic spirits and Nico�s solid, calming presence will be the foil to keep them in check.� <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastSurvivorNico.jpg', '', '2010-04-14 17:51:28', 1),
				('Dylan Walsh', 1, 'Walsh spent the first ten years of his life travelling around the world because of the nature of his parents work. Most of these ten years were spent growing up in East Africa, India and Indonesia. <br />  <br />  When he returned to America he was very active in high school drama activities. After high school he attended the University of Virginia where he graduated with a degree in English. <br />  <br />  <br />  After graduating from university, Walsh decided to move to New York to act professionally. His first role was in a television movie called Soldier Boys. He then landed a role in the movie Loverboy and had a regular role on the television series Kate &amp; Allie. He continued to work in films, such as Betsy''s Wedding and playing Paul Newman''s son in Nobody''s Fool, as well as making television appearances in shows like the police drama Brooklyn South, The Twilight Zone and, Everwood. <br />  <br />  In 2003, Walsh landed the role of Sean McNamara on Nip/Tuck. He got the part after being approached by series creator Ryan Murphy in a coffee shop because Murphy remembered him from his role in Nobody''s Fool. <br />  <br />  During the filming of the third season of Nip/Tuck he landed a costarring role in The Lake House opposite Sandra Bullock and Keanu Reeves. <br />  <br />  Walsh�s latest film work is a part in the remake of The Stepfather. He will revive the role made famous by Terry O''Quinn. <br />  <br />  The actor who used to date Julia Roberts and was married to actress Melora Walters, is now married to actress Joanna Going. <br />  <br />  Walsh�s hobbies include collecting Rolex watches and outdated coins.   ', 'M', 'Los Angeles, California', 'November 17, 1963', '/MMS2/Content/Images/CastDylanWalsh.jpg', '', '2010-04-14 17:53:33', 1),
				('Christian Slater ', 1, 'Christian Slater made his television debut in the soap opera One Life to Live after which he appeared on Broadway opposite Dick Van Dyke in <em>The Music Man</em>.<br />  �<br />  Other Broadway credits include <em>Copperfield, Side Man</em> and <em>One Flew Over the Cuckoo''s Nest</em>. He has had several feature film roles, most notably in <em>Young Guns II</em>, <em>True Romance</em> and <em>Broken Arrow.</em><br />  �<br />  He currently divides his time between New York and Los Angeles.   ', 'M', 'New York City', 'August 18, 1969', '/MMS2/Content/Images/CastChristianSlater.jpg', '', '2010-04-15 13:42:00', 1),
				('Michael Imperioli', 1, 'Michael Imperioli was born in Mt. Vernon, New York, on March 26, 1966. His film work began in the late 1980s. An early part that brought him recognition was in Martin Scorsese''s Goodfellas, as Spider, a local kid who works for the gangsters and has a run-in with a psychopathic mob soldier played by Joe Pesci. He worked throughout the 1990s in the New York independent film industry, especially as a regular in Spike Lee''s movies, appearing in Jungle Fever, Malcolm X, Clockers, Girl 6 and Summer of Sam.<br />  <br />  In 1999 Imperioli was cast in The Sopranos as Christopher Moltisanti, a low-ranking soldier in the Soprano crime organization whose family connections to street boss Tony Soprano move him up the ladder in the organization. <br />  �<br />  Although most famous for his prominent part in "The Sopranos," Imperioli has worked on other television programs as well, including Law &amp; Order, New York Undercover and NYPD Blue.   ', 'M', 'Mount Vernon, New York', 'March 26, 1966', '/MMS2/Content/Images/CastMichaelImperioli.jpg', '', '2010-04-15 13:58:56', 1),
				('Denis Leary', 1, 'Denis was born to Irish immigrants Nora and John Leary. He attended Emerson College in Boston, dabbling in acting and writing. He was a founding member of Emerson''s Comedy Workshop. <br />  <br />  He worked as a stand-up comic in the twilight of the late-80''s comedy boom, and achieved some notoriety for his acerbic take on American life. His comedy CD No Cure For Cancer featured the hit novelty song, I''m An A**hole. He also appeared on the MTV game show Remote Control. <br />  <br />  Denis began appearing in movies, including <em>The Ref</em>, <em>Demolition Man</em>, and <em>Two If By Sea.</em> He voiced a character in the successful animated film <em>Ice Age</em>, and appears as the lead in the FX fireman hit, Rescue Me, for which he was nominated for a Golden Globe. <br />  ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastDenisLeary.jpg', '', '2010-04-16 07:13:04', 1),
				('Spencer Grammer', 1, 'Born in California, Spencer Grammer is the only child of actor Kelsey Grammar and dance instructor Doreen Alderman. <br />  <br />  She began acting early on in life, appearing on Cheers and children''s clothing commercials. Soon after these she got the role of Lucy in As the World Turns. Other credits include Sweetie Pie and Beautiful Ohio. <br />  <br />  She stars as Casey Cartwright in Greek. <br />  ', 'F', '', '', '/MMS2/Content/Images/DStv/Cast/Cast_SpencerGrammar.jpg', '', '2010-04-16 07:26:37', 1),
				('Hannah Rossouw (Lika Berning)', 1, 'Hannah Rossouw is Joeke en Hannes Rossouw se dogter en die sogenaamde erfgenaam van die Rossouw-ryk. Sy het met Oliver getrou kort na haar pa se dood, maar is sy werklik gelukkig? Of het sy net in haar pa se voetspore gevolg, net omdat haar broer dit nie gedoen het nie?   ', 'F', '', '', '/MMS2/Content/Images/DStv/Cast/BinnelandersCast1.jpg', '', '2010-04-19 09:51:43', 1),
				('Jaco Vermeulen', 1, 'Karakternaam: Marius Schoeman <br />  <br />  Verjaarsdag: 10 Maart <br />  <br />  Plek gebore: Roodepoort <br />  <br />  Som jouself op in drie woorde: Leef, Lag, Leer <br />  <br />  Hoekom hou jy van jou karakter? Ek hou daarvan om die karakter te speel, maar ek hou nie r�rig van hom as persoon nie, hy''s nogal nors <br />  <br />  Vir watter nasionale rugbyspan skree jy? Province <br />  <br />  Gunsteling kos: Lemon pepper mielie, mushroom pasta en skaaptjoppies. <br />  <br />  Gunsteling fliek: Te veel om ''n gunsteling te h� maar as enige van die twee CitySlickers op is, sal ek dit deurkyk, elke keer. <br />  <br />  Gunsteling vakansieplek: Thailand was fantasties, gaan verseker terug gaan. <br />  <br />  Gunsteling TV Program: Friends, Grey''s Anatomy <br />  <br />  Gunsteling akteur en aktrise: Steve Martin <br />  <br />  Gunsteling kar: Aston Martin Continental <br />  <br />  Gunsteling ''motto'': "Keep on keepin'' on"   ', 'M', '', '', '/MMS2/Content/Images/DStv/Cast/CastJacoVermeuling.jpg', '', '2010-04-20 11:30:13', 1),
				('Jim Carrey', 1, '', 'M', '', '', '', '', '2010-04-29 10:41:46', 1),
				('Bradley Cooper', 1, '', 'M', '', '', '', '', '2010-04-29 10:42:11', 1),
				('Zooey Deschanel', 1, '', 'M', '', '', '', '', '2010-04-29 10:43:29', 1),
				('Peyton Reed', 1, '', 'M', '', '', '', '', '2010-04-29 10:44:40', 1),
				('Kate Hudson', 1, '', 'M', '', '', '', '', '2010-04-29 10:55:47', 1),
				('Bryan Greenberg', 1, '', 'M', '', '', '', '', '2010-04-29 10:58:28', 1),
				('Gary Winick', 1, '', 'M', '', '', '', '', '2010-04-29 10:59:23', 1),
				('Jennifer Aniston', 1, '', 'F', '', '', '', '', '2010-05-03 11:45:56', 1),
				('Owen Wilson', 1, '', 'M', '', '', '', '', '2010-05-03 11:46:20', 1),
				('David Frankel', 1, '', 'M', '', '', '', '', '2010-05-03 11:47:44', 1),
				('Justin Long', 1, '', 'M', '', '', '', '', '2010-05-03 12:02:16', 1),
				('Ken Kwapis', 1, '', 'M', '', '', '', '', '2010-05-03 12:03:56', 1),
				('Tannishtha Chatterjee', 1, '', 'F', '', '', '', '', '2010-05-03 12:24:12', 1),
				('Satish Kaushik', 1, '', 'M', '', '', '', '', '2010-05-03 12:24:37', 1),
				('Sarah Gavron', 1, '', 'F', '', '', '', '', '2010-05-03 12:25:08', 1),
				('Heather Graham', 1, '', 'F', '', '', '', '', '2010-05-03 12:26:41', 1),
				('Tom Cavanagh', 1, '', 'M', '', '', '', '', '2010-05-03 12:27:16', 1),
				('Sue Kramer', 1, '', 'F', '', '', '', '', '2010-05-03 12:28:58', 1),
				('Angelina Jolie', 1, '', 'F', '', '', '', '', '2010-05-03 12:30:40', 1),
				('Ren�e Zellweger', 1, '', 'F', '', '', '', '', '2010-05-03 12:36:39', 1),
				('Harry Connick Jr.', 1, '', 'M', '', '', '', '', '2010-05-03 12:37:23', 1),
				('Jonas Elmer', 1, '', 'M', '', '', '', '', '2010-05-03 12:37:44', 1),
				('Clint Eastwood', 1, 'The Good, the Bad, The Ugly', 'M', '', '', '', '', '2010-10-01 14:50:40', 1),
				('Amitabh Bachhan', 1, 'Le Superstar', 'M', 'India', '1955', NULL, NULL, '2010-10-26 17:49:59', 1),
				('Stephen Spielberg', 1, '', 'M', '', '', '', '', '2010-05-03 12:37:23', 1),
				('Jerry Bruckheimer', 1, '', 'M', '', '', '', '', '2010-05-03 12:37:44', 1),
				('Quentin Tarantino', 1, '', 'M', '', '', '', '', '2010-05-03 12:37:23', 1),
				('George Lucas', 1, '', 'M', '', '', '', '', '2010-05-03 12:37:44', 1)

				
		DECLARE @ActorID INT = (SELECT TOP 1 [ID] FROM [dbo].[dt_Profession] WHERE [ProfessionName] = 'Actor')
		DECLARE @ActressID INT = (SELECT TOP 1 [ID] FROM [dbo].[dt_Profession] WHERE [ProfessionName] = 'Actress')
		DECLARE @DirectorID INT = (SELECT TOP 1 [ID] FROM [dbo].[dt_Profession] WHERE [ProfessionName] = 'Director')
		DECLARE @ProducerID INT = (SELECT TOP 1 [ID] FROM [dbo].[dt_Profession] WHERE [ProfessionName] = 'Producer')

		INSERT INTO [dbo].[lt_Person_Profession]
			   ([PersonID]
			   ,[ProfessionID])
			   
			SELECT [ID]
			   ,CASE [Gender]
					WHEN 'F' THEN @ActressID
					ELSE @ActorID
				END
			FROM [dbo].[dt_Person]
			
			UNION SELECT (SELECT [ID] FROM [dbo].[dt_Person] WHERE [FullName] = 'Michael C Hall') ID, @ProducerID
			UNION SELECT (SELECT [ID] FROM [dbo].[dt_Person] WHERE [FullName] = 'Jerry Bruckheimer') ID, @ProducerID
			UNION SELECT (SELECT [ID] FROM [dbo].[dt_Person] WHERE [FullName] = 'George Lucas') ID, @ProducerID
			UNION SELECT (SELECT [ID] FROM [dbo].[dt_Person] WHERE [FullName] = 'Clint Eastwood') ID, @DirectorID
			UNION SELECT (SELECT [ID] FROM [dbo].[dt_Person] WHERE [FullName] = 'Stephen Spielberg') ID, @DirectorID
			UNION SELECT (SELECT [ID] FROM [dbo].[dt_Person] WHERE [FullName] = 'Quentin Tarantino') ID, @DirectorID


		DECLARE @ProgID INT
		
		SET @ProgID = (SELECT MIN([ID]) FROM [dbo].[dt_Program])
		
		WHILE ISNULL(@ProgID, 0) > 0
		BEGIN
			INSERT INTO [dbo].[lt_Person_Profession_Program]
				   ([PersonProfessionID]
				   ,[ProgramID]
				   ,[CharacterName])
				   
				SELECT TOP 5 [Table_ID]
						,@ProgID
						,CASE 
							WHEN CHARINDEX(' ', P.[FullName]) > 0
								THEN SUBSTRING(P.[FullName], 0, CHARINDEX(' ', P.[FullName]))
							ELSE P.[FullName]
						END
					FROM [dbo].[lt_Person_Profession] P_P
					INNER JOIN [dbo].[dt_Person] P ON P_P.PersonID = P.ID
					WHERE P_P.[ProfessionID] NOT IN (@ProducerID, @DirectorID)
					ORDER BY NewId()
					
			INSERT INTO [dbo].[lt_Person_Profession_Program]
				   ([PersonProfessionID]
				   ,[ProgramID]
				   ,[CharacterName])
				
				SELECT TOP 1 [Table_ID]
						,@ProgID
						,CASE 
							WHEN CHARINDEX(' ', P.[FullName]) > 0
								THEN SUBSTRING(P.[FullName], 0, CHARINDEX(' ', P.[FullName]))
							ELSE P.[FullName]
						END
					FROM [dbo].[lt_Person_Profession] P_P
					INNER JOIN [dbo].[dt_Person] P ON P_P.PersonID = P.ID
					WHERE P_P.[ProfessionID] = @ProducerID
					ORDER BY NewId()
					
			INSERT INTO [dbo].[lt_Person_Profession_Program]
				   ([PersonProfessionID]
				   ,[ProgramID]
				   ,[CharacterName])
				
				SELECT TOP 1 [Table_ID]
						,@ProgID
						,CASE 
							WHEN CHARINDEX(' ', P.[FullName]) > 0
								THEN SUBSTRING(P.[FullName], 0, CHARINDEX(' ', P.[FullName]))
							ELSE P.[FullName]
						END
					FROM [dbo].[lt_Person_Profession] P_P
					INNER JOIN [dbo].[dt_Person] P ON P_P.PersonID = P.ID
					WHERE P_P.[ProfessionID] = @DirectorID
					ORDER BY NewId()
			
			SET @ProgID = (SELECT MIN([ID]) FROM [dbo].[dt_Program] WHERE [ID] > @ProgID)
		END
			   
			   
	---------------------- 9.0 -----------------------
	DECLARE @ChannelTbl AS TABLE (
			 [Id] INT
			,[ChannelName] VARCHAR(255)
			,[ChannelNumber] INT
			,[ChannelType] INT
			,[SmallLogo] VARCHAR(255)
			,[MediumLogo] VARCHAR(255)
			,[BigLogo] VARCHAR(255)
			,[RN] INT)
	
	INSERT INTO @ChannelTbl (
			 [Id]
			,[ChannelName]
			,[ChannelNumber]
			,[ChannelType]
			,[SmallLogo]
			,[MediumLogo]
			,[BigLogo]
			,[RN])
		SELECT [Id]
			  ,[ChannelName]
			  ,[ChannelNumber]
			  ,[ChannelType]
			  ,[SmallLogo]
			  ,[MediumLogo]
			  ,[BigLogo]
			  ,ROW_NUMBER() OVER (ORDER BY [Id])
		  FROM [dbo].[DStvChannels_Channel_view]
		 WHERE [Id] IN 
		(89, 93, 94, 95, 96, 97, 98, 102, 103)	
	
	DECLARE @ChannelCount INT = (SELECT COUNT([Id]) FROM @ChannelTbl)
	
	INSERT INTO [dbo].[lt_Channel_Program]
		   ([ChannelID]
		   ,[ProgramID])
		SELECT CT.Id
				,P.ID
		FROM [dbo].[dt_Program] P
		INNER JOIN (SELECT ID, (ROW_NUMBER() OVER (ORDER BY NewId()) % @ChannelCount) + 1 RN FROM [dbo].[dt_Program]) P2 ON P.ID = P2.ID
		INNER JOIN @ChannelTbl CT ON CT.RN = P2.RN

	---------------------- 10.0 -----------------------
	
	DECLARE @CountTbl TABLE (I INT)
	DECLARE @CategoryTbl TABLE (ID INT, CategoryName VARCHAR(255), CategoryParentID INT, RN INT)
	DECLARE @CategoryCount INT
	
	--SELECT '(' + ISNULL('''' + REPLACE([CategoryName], '''','''''') + '''', 'NULL') + ', ' + CAST(CategoryParentID AS VARCHAR) + ', ' + ISNULL('''' + REPLACE(CreatedDate, '''','''''') + '''', 'NULL') + ', ' + CAST(StatusID AS VARCHAR) + '),'
	--FROM [dbo].[dt_VideoCategory]
	
	INSERT INTO [dbo].[dt_VideoCategory]
		   ([CategoryName]
		   ,[CategoryParentID]
		   ,[CreatedDate]
		   ,[StatusID])
	VALUES ('Idols Auditions', 0, 'Jun 29 2010  4:39PM', 4),
			('Wooden mic', 0, 'Jun 29 2010  5:21PM', 4),
			('Big Brother Auditions', 0, 'Jun 30 2010  8:33PM', 1),
			('Idols Alumni', 0, 'Jul  9 2010 11:17AM', 4),
			('Idols Catch-Up', 0, 'Jul  9 2010 11:18AM', 4),
			('Idols Judges', 0, 'Jul  9 2010 11:18AM', 4),
			('Idols eXtra', 0, 'Jul  9 2010 11:18AM', 4),
			('Idols Advice', 0, 'Jul  9 2010 12:00PM', 4),
			('Test', 0, 'Jul 12 2010  4:17PM', 4),
			('Idols eXtra', 0, 'Jul 13 2010  3:21PM', 1),
			('Big Brother Test', 0, 'Jul 14 2010 10:31AM', 4),
			('Auditions', 0, 'Jul 14 2010  2:39PM', 1),
			('Tasks', 0, 'Jul 16 2010  2:46PM', 1),
			('Romance', 0, 'Jul 16 2010  2:47PM', 1),
			('Diary Room', 0, 'Jul 16 2010  2:47PM', 4),
			('Nominations', 0, 'Jul 16 2010  2:48PM', 4),
			('Evictions', 0, 'Jul 16 2010  2:48PM', 4),
			('Fun', 0, 'Jul 16 2010  2:48PM', 1),
			('Conflict', 0, 'Jul 16 2010  2:49PM', 4),
			('Profiles', 0, 'Jul 16 2010  2:49PM', 1),
			('Launch', 0, 'Jul 18 2010  9:21AM', 1),
			('Secrets', 0, 'Jul 18 2010  5:08PM', 1),
			('Highlights', 0, 'Jul 18 2010  6:02PM', 4),
			('Highlights', 0, 'Jul 18 2010  6:02PM', 1),
			('Nominations', 0, 'Jul 19 2010 11:04AM', 1),
			('Diary Room', 0, 'Jul 19 2010  3:43PM', 1),
			('Dirty Dancing', 0, 'Jul 20 2010 10:35AM', 1),
			('Conflict', 0, 'Jul 21 2010 11:24AM', 1),
			('Strike Action', 0, 'Jul 24 2010  8:40PM', 1),
			('Big Brother eXtra', 0, 'Aug  1 2010  9:46PM', 1),
			('Eviction Shows', 0, 'Aug  2 2010  7:41PM', 1),
			('The Barn', 0, 'Aug  4 2010  4:11PM', 1),
			('Wooden Mic ', 0, 'Aug 13 2010 12:08PM', 1),
			('Top 14', 0, 'Aug 18 2010 10:46AM', 1),
			('Boer Ekstra', 0, 'Aug 30 2010  7:02AM', 1),
			('Hoogtepunte', 0, 'Aug 30 2010  7:02AM', 1),
			('Top 10 Boere', 0, 'Aug 30 2010  7:03AM', 1),
			('Box Office', 0, 'Oct 11 2010  2:21PM', 1),
			('Catch Up', 0, 'Oct 11 2010  2:21PM', 1),
			('Open Time', 0, 'Oct 11 2010  2:21PM', 1)
	
	DECLARE @BoxOfficeCatID INT
	DECLARE @CatchUpCatID INT
	DECLARE @OpenTimeID INT
	
	SET @BoxOfficeCatID = (SELECT TOP 1 ID FROM [dbo].[dt_VideoCategory] WHERE [CategoryName] = 'Box Office') 
	SET @CatchUpCatID = (SELECT TOP 1 ID FROM [dbo].[dt_VideoCategory] WHERE [CategoryName] = 'Catch Up') 
	SET @OpenTimeID = (SELECT TOP 1 ID FROM [dbo].[dt_VideoCategory] WHERE [CategoryName] = 'Open Time') 
	
	INSERT INTO @CountTbl VALUES (1),(2),(3)
	
	
	INSERT INTO @CategoryTbl (ID, CategoryName, CategoryParentID, RN)
	SELECT	[ID]
			,[CategoryName]
			,[CategoryParentID]
			,ROW_NUMBER() OVER (ORDER BY ID, CategoryName, CategoryParentID)
	FROM [dbo].[dt_VideoCategory]
	WHERE [ID] IN (@BoxOfficeCatID,	@CatchUpCatID, @OpenTimeID)


	INSERT INTO [dbo].[dt_VideoCategory]
			   ([CategoryName]
			   ,[CategoryParentID]
			   ,[CreatedDate]
			   ,[StatusID])
	SELECT [CategoryName] + ' - Sub Category ' + CAST(I AS VARCHAR)
			,[ID]
			,GETDATE()
			,1
	FROM @CategoryTbl
	CROSS JOIN @CountTbl

	SET @CategoryCount = (SELECT COUNT(ID) FROM @CategoryTbl) 
	
	INSERT INTO @CategoryTbl (ID, CategoryName, CategoryParentID, RN)
	SELECT	[ID]
			,[CategoryName]
			,[CategoryParentID]
			,ROW_NUMBER() OVER (ORDER BY ID, CategoryName, CategoryParentID) + @CategoryCount
	FROM [dbo].[dt_VideoCategory]
	WHERE [CategoryParentID] > 0
	
	SET @CategoryCount = (SELECT COUNT(ID) FROM @CategoryTbl) 

	INSERT INTO [dbo].[lt_Video_VideoCategory]
			   ([VideoID]
			   ,[VideoCategoryID])
	SELECT DISTINCT V.[ID]
			,CT.ID
		FROM [dbo].[dt_Video] V
		INNER JOIN (SELECT ID, (ROW_NUMBER() OVER (ORDER BY NewId(), ID, VideoTitle) % @CategoryCount) + 1 RN FROM [dbo].[dt_Video]) V2 ON V.ID = V2.ID
		INNER JOIN @CategoryTbl CT ON CT.RN = V2.RN
			   
	---------------------- 10.0 -----------------------
	
	DECLARE @LastChance TABLE (ProgramID INT)
	DECLARE @ForthComing TABLE (ProgramID INT)
	DECLARE @ExpiredItems TABLE (ProgramID INT)	
	DECLARE @AlwaysExpiredID INT = 12
	DECLARE @AlwaysValidID INT = 3
	DECLARE @AlwaysValidID2 INT = 14
	DECLARE @FirstHorrorID INT
	
	
	SET @FirstHorrorID = (SELECT TOP 1 ID FROM [dbo].[lt_Program_Genre] PG WHERE [SubGenreID] = @SubGenreHorrorID)
	
	INSERT INTO @LastChance (ProgramID)
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @BoxOfficeID
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])

		UNION
		
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @CatchUpID AND P.[ProgramTypeID] = 1
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])
	
		UNION
		
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @CatchUpID AND P.[ProgramTypeID] = 2
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])
		
	
	INSERT INTO @ForthComing (ProgramID)
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @BoxOfficeID
		AND P.[ID] NOT IN (SELECT ProgramID FROM @LastChance)
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])

		UNION
		
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @CatchUpID AND P.[ProgramTypeID] = 1
		AND P.[ID] NOT IN (SELECT ProgramID FROM @LastChance)
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])
	
		UNION
		
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @CatchUpID AND P.[ProgramTypeID] = 2
		AND P.[ID] NOT IN (SELECT ProgramID FROM @LastChance)
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])
						
	
	INSERT INTO @ExpiredItems (ProgramID)
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @BoxOfficeID
		AND P.[ID] NOT IN (SELECT ProgramID FROM @LastChance)
		AND P.[ID] NOT IN (SELECT ProgramID FROM @ForthComing)
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])

		UNION
		
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @CatchUpID AND P.[ProgramTypeID] = 1
		AND P.[ID] NOT IN (SELECT ProgramID FROM @LastChance)
		AND P.[ID] NOT IN (SELECT ProgramID FROM @ForthComing)
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])
	
		UNION
		
		SELECT TOP 2 P.[ID]
		FROM [dbo].[dt_Program] P
		INNER JOIN [dbo].[lt_Product_Program] PP ON PP.ProgramID = P.ID
		WHERE PP.[ProductID] = @CatchUpID AND P.ProgramTypeID = 2
		AND P.[ID] NOT IN (SELECT ProgramID FROM @LastChance)
		AND P.[ID] NOT IN (SELECT ProgramID FROM @ForthComing)
		AND P.[ID] NOT IN (@AlwaysValidID, @AlwaysValidID2, @AlwaysExpiredID, @FirstHorrorID)
		AND P.[ID] NOT IN (SELECT ISNULL(V.ProgramID, ELI.ItemID)
								FROM [dbo].[lt_EditorialListItems] ELI
								LEFT JOIN [dbo].[dt_Video] V ON ELI.[ItemTypeId] = 1 AND ELI.[ItemID] = V.[ID])
							
			
	INSERT INTO @ExpiredItems (ProgramID)
		VALUES (@AlwaysExpiredID)
		
	-- Last Chance Items - Exp tomorrow
	UPDATE [dbo].[dt_Program]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() + 1, 120),
		[Abstract] = [Abstract] + '; Last Chance Test'
	WHERE [ID] IN (SELECT ProgramID FROM @LastChance)

	UPDATE [dbo].[dt_Video]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() + 1, 120),
		[ExpiryDate] = CONVERT(CHAR(10), GETDATE() + 1, 120),
		[Abstract] = [Abstract] + '; Last Chance Test'
	WHERE [ProgramID] IN (SELECT ProgramID FROM @LastChance)
	
	
	-- Forth Coming Items - Valid tomorrow
	UPDATE [dbo].[dt_Program]
	SET [StartDate] = CONVERT(CHAR(10), GETDATE() + 7, 120),
		[Abstract] = [Abstract] + '; Forth Coming Test'
	WHERE [ID] IN (SELECT ProgramID FROM @ForthComing)

	UPDATE [dbo].[dt_Video]
	SET [StartDate] = CONVERT(CHAR(10), GETDATE() + 7, 120),
		[AirDate] = CONVERT(CHAR(10), GETDATE() + 7, 120),
		[Abstract] = [Abstract] + '; Forth Coming Test'
	WHERE [ProgramID] IN (SELECT ProgramID FROM @ForthComing)


	-- Expired Items
	UPDATE [dbo].[dt_Program]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() - 1, 120),
		[Abstract] = [Abstract] + '; Expired Items'
	WHERE [ID] IN (SELECT ProgramID FROM @ExpiredItems)

	UPDATE [dbo].[dt_Video]
	SET [StartDate] = DATEADD(YEAR, -1, [StartDate]),
		[EndDate] = CONVERT(CHAR(10), GETDATE() - 1, 120),
		[ExpiryDate] = CONVERT(CHAR(10), GETDATE() - 1, 120),
		[Abstract] = [Abstract] + '; Expired Items'
	WHERE [ProgramID] IN (SELECT ProgramID FROM @ExpiredItems)


--ROLLBACK TRAN
--COMMIT TRAN


