USE [master]
GO
/****** Object:  Database [DStvMedia2010]    Script Date: 04/29/2011 16:58:25 ******/
CREATE DATABASE [DStvMedia2010] ON  PRIMARY 
( NAME = N'DStvMedia2010', FILENAME = N'D:\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\DStvMedia2010.mdf' , SIZE = 20480KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DStvMedia2010_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\DStvMedia2010_1.ldf' , SIZE = 321088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DStvMedia2010] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DStvMedia2010].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DStvMedia2010] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [DStvMedia2010] SET ANSI_NULLS OFF
GO
ALTER DATABASE [DStvMedia2010] SET ANSI_PADDING OFF
GO
ALTER DATABASE [DStvMedia2010] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [DStvMedia2010] SET ARITHABORT OFF
GO
ALTER DATABASE [DStvMedia2010] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [DStvMedia2010] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [DStvMedia2010] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [DStvMedia2010] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [DStvMedia2010] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [DStvMedia2010] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [DStvMedia2010] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [DStvMedia2010] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [DStvMedia2010] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [DStvMedia2010] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [DStvMedia2010] SET  DISABLE_BROKER
GO
ALTER DATABASE [DStvMedia2010] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [DStvMedia2010] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [DStvMedia2010] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [DStvMedia2010] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [DStvMedia2010] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [DStvMedia2010] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [DStvMedia2010] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [DStvMedia2010] SET  READ_WRITE
GO
ALTER DATABASE [DStvMedia2010] SET RECOVERY FULL
GO
ALTER DATABASE [DStvMedia2010] SET  MULTI_USER
GO
ALTER DATABASE [DStvMedia2010] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [DStvMedia2010] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'DStvMedia2010', N'ON'
GO
USE [DStvMedia2010]
GO
/****** Object:  User [\Everyone]    Script Date: 04/29/2011 16:58:25 ******/
CREATE USER [\Everyone] FOR LOGIN [\Everyone]
GO
/****** Object:  FullTextCatalog [dt_Video]    Script Date: 04/29/2011 16:58:25 ******/
CREATE FULLTEXT CATALOG [dt_Video]WITH ACCENT_SENSITIVITY = ON
AUTHORIZATION [dbo]
GO
/****** Object:  Table [dbo].[dt_Website]    Script Date: 04/29/2011 16:58:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Website](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[WebsiteName] [varchar](150) NOT NULL,
	[WebsiteURL] [varchar](500) NOT NULL,
	[AuthenticationKey] [varchar](255) NOT NULL,
	[VideoAccess] [bit] NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Website] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_VideoMeta]    Script Date: 04/29/2011 16:58:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_VideoMeta](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VideoID] [int] NOT NULL,
	[LanguageID] [smallint] NOT NULL,
	[SecondaryLanguageID] [smallint] NULL,
	[SubtitleLanguageID] [smallint] NULL,
	[YearOfRelease] [smallint] NULL,
	[AwardTextLong] [nvarchar](max) NULL,
	[AwardTextShort] [varchar](500) NULL,
	[Synopsis] [nvarchar](max) NULL,
	[CountryID] [smallint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_VideoMeta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_dt_VideoMeta] UNIQUE NONCLUSTERED 
(
	[VideoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_VideoLog]    Script Date: 04/29/2011 16:58:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dt_VideoLog](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VideoID] [int] NOT NULL,
	[WebsiteID] [smallint] NOT NULL,
	[ViewCount] [bigint] NOT NULL,
	[LastViewed] [datetime] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_VideoLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[dt_VideoLog_SelectAll_Mnet-Sales]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_VideoLog_Select
-- Author:	Arthur - DStv Online
-- alter date:	2010-02-08 12:13:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoLog table
-- ==========================================================================================
CREATE Procedure [dbo].[dt_VideoLog_SelectAll_Mnet-Sales]

	
AS
BEGIN
	select [ID]
      ,[VideoID]
      ,[WebsiteID]
      ,[ViewCount]
      ,CONVERT(datetime, [LastViewed], 110) as LastViewed
      , CONVERT(datetime, [CreatedDate], 110) as CreatedDate
      ,[StatusID]from [DStvMedia].[dbo].[dt_VideoLog]
      where [WebsiteID] = 23 
END
GO
/****** Object:  StoredProcedure [dbo].[dt_VideoLog_Select_Mnet-Sales]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_VideoLog_Select
-- Author:	Arthur - DStv Online
-- alter date:	2010-02-08 12:13:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoLog table
-- ==========================================================================================
CREATE Procedure [dbo].[dt_VideoLog_Select_Mnet-Sales]
@Date datetime 
	
AS
BEGIN
	select [ID]
      ,[VideoID]
      ,[WebsiteID]
      ,[ViewCount]
      ,[LastViewed]
      ,[CreatedDate]
      ,[StatusID]from [DStvMedia].[dbo].[dt_VideoLog]
      where [WebsiteID] = 23 and [CreatedDate] = @Date
END
GO
/****** Object:  Table [dbo].[dt_VideoFileType]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_VideoFileType](
	[ID] [tinyint] IDENTITY(1,1) NOT NULL,
	[FileExtension] [varchar](20) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_VideoFileType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_VideoFilePath]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_VideoFilePath](
	[ID] [smallint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VideoFilePathName] [varchar](80) NOT NULL,
	[ServerPath] [varchar](500) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_VideoFilePath] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_DownloadLog]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_DownloadLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](255) NOT NULL,
	[VideoFileID] [int] NOT NULL,
	[WebsiteID] [smallint] NOT NULL,
	[DateDownload] [datetime] NOT NULL,
 CONSTRAINT [PK_dt_DownloadLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Country]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Country](
	[ID] [smallint] IDENTITY(52,1) NOT NULL,
	[CountryName] [varchar](80) NOT NULL,
	[FlagImageURL] [varchar](500) NULL,
	[DefaultLanguageID] [smallint] NOT NULL,
	[DefaultFootprintID] [smallint] NULL,
	[GMT] [smallint] NOT NULL,
	[DSN] [varchar](50) NULL,
	[NamiDSN] [varchar](50) NULL,
	[ZIndex] [int] NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Channel]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Channel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChannelName] [varchar](255) NULL,
	[ChannelType] [int] NULL,
	[Language] [int] NULL,
	[ChannelNumber] [int] NULL,
	[Genre] [int] NULL,
	[GroupId] [int] NULL,
	[ShortName] [varchar](50) NULL,
	[SerID] [int] NULL,
	[EPGIS7] [varchar](50) NULL,
	[EPGW4] [varchar](50) NULL,
	[EPGIS10] [varchar](50) NULL,
	[EngDescription] [ntext] NULL,
	[PorDescription] [ntext] NULL,
	[FreDescription] [ntext] NULL,
	[SmallLogo] [varchar](255) NULL,
	[MediumLogo] [varchar](255) NULL,
	[BigLogo] [varchar](255) NULL,
	[URL] [varchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_AgeRestriction]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_AgeRestriction](
	[ID] [tinyint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Abbreviation] [varchar](40) NOT NULL,
	[AgeRestrictionDescription] [varchar](100) NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_AgeRestriction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_VideoCategory]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_VideoCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](255) NOT NULL,
	[CategoryParentID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_VideoCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Status]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Status](
	[ID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_dt_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Rating]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dt_Rating](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WebsiteID] [smallint] NOT NULL,
	[VideoID] [int] NOT NULL,
	[Rating] [float] NOT NULL,
	[RateCount] [int] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_Rating] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dt_ProgramType]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_ProgramType](
	[ID] [smallint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProgramTypeName] [varchar](100) NOT NULL,
	[RedirectURL] [varchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_ProgrammingType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Program_Rating]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dt_Program_Rating](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WebsiteID] [smallint] NOT NULL,
	[ProgramID] [int] NOT NULL,
	[Rating] [float] NOT NULL,
	[RateCount] [int] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_Program_Rating] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dt_Program]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Program](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramName] [varchar](150) NOT NULL,
	[ProgramTypeID] [smallint] NOT NULL,
	[Abstract] [varchar](500) NOT NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[BillboardURL] [varchar](500) NOT NULL,
	[GenreID] [smallint] NOT NULL,
	[SubGenreID] [smallint] NULL,
	[GeoGroupID] [int] NULL,
	[WebsiteURL] [varchar](500) NULL,
	[Keywords] [varchar](1000) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[OldID] [int] NULL,
	[OldSource] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_dt_Program] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1= Show; 2 = Movie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dt_Program', @level2type=N'COLUMN',@level2name=N'ProgramTypeID'
GO
/****** Object:  Table [dbo].[dt_Profession]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Profession](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProfessionName] [varchar](100) NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[Rank] [int] NOT NULL,
	[IsCast] [bit] NOT NULL,
	[IsCrew] [bit] NOT NULL,
 CONSTRAINT [PK_dt_Profession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Product]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](255) NOT NULL,
	[ProductDescription] [varchar](max) NOT NULL,
	[ProductIcon] [varchar](500) NULL,
	[MediumLogo] [varchar](500) NULL,
	[LargeLogo] [varchar](500) NULL,
	[CurrencyIdentifier] [nvarchar](15) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_Channel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Person]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Person](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FullName] [varchar](80) NOT NULL,
	[ProfessionID] [smallint] NOT NULL,
	[Bio] [nvarchar](max) NULL,
	[Gender] [char](1) NULL,
	[BirthPlace] [varchar](80) NULL,
	[BirthDate] [varchar](20) NULL,
	[ImageURL] [varchar](500) NULL,
	[AwardsText] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_Person] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_Language]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Language](
	[ID] [smallint] IDENTITY(17,1) NOT NULL,
	[LanguageName] [varchar](80) NOT NULL,
	[StatusID] [tinyint] NOT NULL,
 CONSTRAINT [PK_dt_Language] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dt_ErrorLog]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_ErrorLog](
	[ErrorId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Location] [varchar](500) NOT NULL,
	[ErrorDetails] [varchar](max) NOT NULL,
	[Resolved] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED 
(
	[ErrorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lt_Video_Tag]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lt_Video_Tag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VideoId] [int] NOT NULL,
	[Tag] [varchar](200) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_lt_Video_Tag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lt_Program_Tag]    Script Date: 04/29/2011 16:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lt_Program_Tag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[Tag] [varchar](200) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_lt_Program_Tag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[Array]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Array] ( @str varchar(5000) )

RETURNS @ReturnTable TABLE ( Value varchar(500) )

AS  

BEGIN 

DECLARE	@i int
DECLARE	@ThisString varchar(500)
DECLARE	@Array TABLE ( Value varchar(500) )

SET		@i = 1
SET		@ThisString = ''

WHILE		@i <= LEN( @str )
		BEGIN
		IF	SUBSTRING( @str, @i, 1 ) <> ','
			BEGIN
			SET @ThisString = @ThisString + SUBSTRING( @str, @i, 1 )
			END
		ELSE	-- we've reached the comma delimiter
			BEGIN
			--IF		LEN(RTRIM(LTRIM(@ThisString))) > 0
			--		BEGIN
					INSERT INTO	@Array
					SELECT	RTRIM(LTRIM(@ThisString))
			--		END
			SET		@ThisString = ''
			END
		SET @i = @i + 1
		END

-- This is done one last time for the last value in the string
--IF		LEN(RTRIM(LTRIM(@ThisString))) > 0
--		BEGIN
		INSERT INTO	@Array
		SELECT	RTRIM(LTRIM(@ThisString))
--		END

INSERT INTO	@ReturnTable
SELECT	Value
FROM		@Array

RETURN

END
GO
/****** Object:  Table [dbo].[lt_Website_Channel]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Website_Channel](
	[WebsiteID] [smallint] NOT NULL,
	[ChannelID] [int] NOT NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Website_Channel_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lt_Website_VideoCategory]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Website_VideoCategory](
	[WebsiteID] [int] NOT NULL,
	[VideoCategoryID] [int] NOT NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Website_VideoCategory_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ChannelType_Update]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ChannelType_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for update of dt_ChannelType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ChannelType_Update]
@ID int,
@TypeName varchar(50)
As
Begin
UPDATE    dt_ChannelType
SET              TypeName = @TypeName
WHERE     ID = @ID
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ChannelType_SelectList]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ChannelType_SelectList]
As
Begin
	Select 
		[ID],
		[TypeName]
	From dt_ChannelType where ([StatusID] = 1 or [StatusID] = NULL)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ChannelType_SelectAll]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ChannelType_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_ChannelType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ChannelType_SelectAll]
As
Begin
	Select 
		[ID],
		[TypeName]
	From dt_ChannelType where ([StatusID] = 1 or [StatusID] = NULL)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ChannelType_Insert]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ChannelType_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for inserting rows on dt_ChannelType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ChannelType_Insert]
@TypeName varchar(50)
As
Begin
INSERT INTO dt_ChannelType
                      (TypeName)
VALUES     (@TypeName)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ChannelType_DeleteRow]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ChannelType_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for deleting rows on dt_ChannelType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ChannelType_DeleteRow]
@ID int
As
Begin
UPDATE    dt_ChannelType
SET              StatusID = 4
WHERE     ID = @ID
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_Insert]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for inserting values to dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Genre_Insert]
	@GenreName varchar(80),
	@GenreParentID int
As
Begin
	Insert Into dt_Genre
		([GenreName],[GenreParentID])
	Values
		(@GenreName,@GenreParentID)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_DeleteRow]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Genre_DeleteRow]
	@ID smallint
As
Begin
	UPDATE    dt_Genre
SET              StatusID = 4
WHERE     (ID = @ID)


End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Footprint_Update]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Footprint_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for inserting values to dt_Update table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Footprint_Update]
	@FootprintName varchar(50),
	@ID int
As
Begin
	UPDATE    dt_Footprint
SET              FootprintName = @FootprintName
where ID = @ID
	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Footprint_SelectAll]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Footprint_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Footprint table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Footprint_SelectAll]
As
Begin
SELECT     ID, FootprintName
FROM         dt_Footprint
WHERE     (StatusID = 1) OR
                      (StatusID = NULL)
ORDER BY FootprintName
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Footprint_Insert]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Footprint_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for inserting values to dt_Footprint table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Footprint_Insert]
	@FootprintName varchar(50)
As
Begin
	INSERT INTO dt_Footprint
                      (FootprintName)
VALUES     (@FootprintName)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Footprint_DeleteRow]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Footprint_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Country table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Footprint_DeleteRow]
	@ID smallint
As
Begin
UPDATE    dt_Footprint
SET              StatusID = 4
WHERE     (ID = @ID)


End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_Update]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for updating dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Genre_Update]
	@ID smallint,
	@GenreName varchar(80),
	@GenreParentID int
As
Begin
	Update dt_Genre
	Set
		[GenreName] = @GenreName,
		[GenreParentID] = @GenreParentID
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_SelectParentList]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Genre_SelectParentList]
As
Begin
	Select 
		[ID],
		[GenreName],
		[CreatedDate]
	From dt_Genre where [ID] != 0 AND ([StatusID] = 1 or [StatusID] = NULL) and ([GenreParentID] = 0)
End
GO
/****** Object:  Table [dbo].[tt_Video_Meta_v3]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tt_Video_Meta_v3](
	[ttID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NOT NULL,
	[VideoTitle] [varchar](200) NOT NULL,
	[Keywords] [varchar](1000) NULL,
	[Abstract] [varchar](500) NOT NULL,
	[Runtime] [varchar](50) NULL,
	[AirDate] [datetime] NULL,
	[Season] [int] NULL,
	[Episode] [int] NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[PrimaryLanguage] [varchar](80) NULL,
	[YearOfRelease] [smallint] NULL,
	[Synopsis] [nvarchar](max) NULL,
	[CountryName] [varchar](80) NULL,
	[GenreName] [varchar](80) NULL,
	[ProgramID] [int] NULL,
	[ProgramTypeID] [smallint] NULL,
	[StartDate] [datetime] NULL,
	[LanguageID] [smallint] NULL,
	[CountryID] [smallint] NULL,
	[GenreID] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[AgeRestrictionAbbreviation] [varchar](40) NULL,
	[BillboardImageURL] [varchar](500) NULL,
 CONSTRAINT [PK_tt_Video_Meta_v3] PRIMARY KEY CLUSTERED 
(
	[ttID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tt_Video_Meta_V2]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tt_Video_Meta_V2](
	[ttID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NOT NULL,
	[VideoTitle] [varchar](200) NOT NULL,
	[Keywords] [varchar](1000) NULL,
	[Abstract] [varchar](500) NOT NULL,
	[Runtime] [varchar](50) NULL,
	[AirDate] [datetime] NULL,
	[Season] [int] NULL,
	[Episode] [int] NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[ServerPath] [varchar](500) NULL,
	[FileName] [varchar](756) NULL,
	[PrimaryLanguage] [varchar](80) NULL,
	[SecondaryLanguage] [varchar](80) NULL,
	[SubtitleLanguage] [varchar](80) NULL,
	[YearOfRelease] [smallint] NULL,
	[AwardTextLong] [nvarchar](max) NULL,
	[AwardTextShort] [varchar](500) NULL,
	[Synopsis] [nvarchar](max) NULL,
	[CountryName] [varchar](80) NULL,
	[GenreName] [varchar](80) NULL,
	[ProgramID] [int] NULL,
	[ProgramTypeID] [smallint] NULL,
	[VideoFileTypeID] [tinyint] NULL,
	[StatusID] [tinyint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[LanguageID] [smallint] NULL,
	[SecondaryLanguageID] [smallint] NULL,
	[SubtitleLanguageID] [smallint] NULL,
	[CountryID] [smallint] NULL,
	[GenreID] [smallint] NULL,
	[SubGenreID] [smallint] NULL,
	[SubGenreName] [varchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[AgeRestrictionID] [tinyint] NULL,
	[AgeRestrictionAbbreviation] [varchar](40) NULL,
	[AgeRestrictionDescription] [varchar](100) NULL,
	[BillboardImageURL] [varchar](500) NULL,
 CONSTRAINT [PK_tt_Video_Meta_V2] PRIMARY KEY CLUSTERED 
(
	[ttID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tt_Video_Meta_BU]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tt_Video_Meta_BU](
	[ttID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NOT NULL,
	[VideoTitle] [varchar](200) NOT NULL,
	[Keywords] [varchar](1000) NULL,
	[Abstract] [varchar](500) NOT NULL,
	[Runtime] [varchar](50) NULL,
	[AirDate] [datetime] NULL,
	[Season] [int] NULL,
	[Episode] [int] NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[ServerPath] [varchar](500) NULL,
	[FileName] [varchar](756) NULL,
	[Director] [varchar](80) NULL,
	[PrimaryLanguage] [varchar](80) NULL,
	[SecondaryLanguage] [varchar](80) NULL,
	[SubtitleLanguage] [varchar](80) NULL,
	[YearOfRelease] [smallint] NULL,
	[AwardTextLong] [nvarchar](max) NULL,
	[AwardTextShort] [varchar](500) NULL,
	[Synopsis] [nvarchar](max) NULL,
	[CountryName] [varchar](80) NULL,
	[GenreName] [varchar](80) NULL,
	[ProgramID] [int] NULL,
	[ProgramTypeID] [smallint] NULL,
	[VideoFileTypeID] [tinyint] NULL,
	[StatusID] [tinyint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[LanguageID] [smallint] NULL,
	[SecondaryLanguageID] [smallint] NULL,
	[SubtitleLanguageID] [smallint] NULL,
	[CountryID] [smallint] NULL,
	[GenreID] [smallint] NULL,
	[SubGenreID] [smallint] NULL,
	[SubGenreName] [varchar](80) NULL,
	[DirectorId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[AgeRestrictionID] [tinyint] NULL,
	[AgeRestrictionAbbreviation] [varchar](40) NULL,
	[AgeRestrictionDescription] [varchar](100) NULL,
	[BillboardImageURL] [varchar](500) NULL,
	[Price] [numeric](10, 2) NULL,
	[Geolocation] [numeric](3, 0) NULL,
	[RuntimeInMinutes] [numeric](3, 0) NULL,
	[RentalPeriodInHours] [numeric](3, 0) NULL,
	[SizeInKB] [numeric](18, 0) NULL,
	[VideoDimension] [nvarchar](15) NULL,
 CONSTRAINT [PK_tt_Video_Meta_BU] PRIMARY KEY CLUSTERED 
(
	[ttID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tt_Video_Meta]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tt_Video_Meta](
	[ttID] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NOT NULL,
	[VideoTitle] [varchar](200) NOT NULL,
	[Keywords] [varchar](1000) NULL,
	[Abstract] [varchar](500) NOT NULL,
	[Runtime] [varchar](50) NULL,
	[AirDate] [datetime] NULL,
	[Season] [int] NULL,
	[Episode] [int] NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[ServerPath] [varchar](500) NULL,
	[FileName] [varchar](756) NULL,
	[Director] [varchar](80) NULL,
	[PrimaryLanguage] [varchar](80) NULL,
	[SecondaryLanguage] [varchar](80) NULL,
	[SubtitleLanguage] [varchar](80) NULL,
	[YearOfRelease] [smallint] NULL,
	[AwardTextLong] [nvarchar](max) NULL,
	[AwardTextShort] [varchar](500) NULL,
	[Synopsis] [nvarchar](max) NULL,
	[CountryName] [varchar](80) NULL,
	[GenreName] [varchar](80) NULL,
	[ProgramID] [int] NULL,
	[ProgramTypeID] [smallint] NULL,
	[VideoFileTypeID] [tinyint] NULL,
	[StatusID] [tinyint] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[LanguageID] [smallint] NULL,
	[SecondaryLanguageID] [smallint] NULL,
	[SubtitleLanguageID] [smallint] NULL,
	[CountryID] [smallint] NULL,
	[GenreID] [smallint] NULL,
	[SubGenreID] [smallint] NULL,
	[SubGenreName] [varchar](80) NULL,
	[DirectorId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[AgeRestrictionID] [tinyint] NULL,
	[AgeRestrictionAbbreviation] [varchar](40) NULL,
	[AgeRestrictionDescription] [varchar](100) NULL,
	[BillboardImageURL] [varchar](500) NULL,
	[Price] [numeric](10, 2) NULL,
	[Geolocation] [numeric](3, 0) NULL,
	[RuntimeInMinutes] [numeric](3, 0) NULL,
	[RentalPeriodInHours] [numeric](3, 0) NULL,
	[CastCrewID] [int] NULL,
	[SizeInKB] [numeric](18, 0) NULL,
	[VideoDimension] [nvarchar](15) NULL,
 CONSTRAINT [PK_tt_Video_Meta] PRIMARY KEY CLUSTERED 
(
	[ttID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Footprint_Select]    Script Date: 04/29/2011 16:58:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ws_dt_Footprint_Select]
                
AS
BEGIN
                SELECT 
       [ID]
      ,[FootprintName]
     FROM [dbo].[dt_Footprint] with(nolock) where [StatusID] = 1 or [StatusID] = NULL 
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_SubGenre_SelectAllByGenre]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_SubGenre_SelectAllByGenre]
@GenreID smallint
As
Begin
	Select 
		[ID],
		[GenreName],
		[GenreParentID],
		[CreatedDate]
	From dt_Genre where ([StatusID] = 1 or [StatusID] = NULL)
	and GenreParentID = @GenreID
End
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Language_Select]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ws_dt_Language_Select]

AS
BEGIN
    SELECT
      [Id]
      ,[LanguageName] 
      
    FROM [dbo].[dt_Language] with(nolock) where [StatusID] = 1 or [StatusID] = NULL 
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Genre_Select]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_Genre_Select
-- Author:	Desigan - DStv Online
-- alter date:	7/12/2009 11:28:00 PM
-- Description:	This stored procedure is intended for selecting single/all rows from dt_Genre table

-- Entity Name:	mms_dt_Genre_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Genre table

-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_Genre_Select]
	@AuthenticationKey varchar(255),
	@GenreID	int = 0,
	@GenreParentID int = 0
	,@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website WITH(NOLOCK)
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		SELECT		ID, GenreName
		FROM        dt_Genre WITH(NOLOCK)
		WHERE		(StatusID = 1 OR StatusID = NULL)
		AND			(Id = @GenreId OR @GenreId = 0)
		AND			(ID <> 0)
		AND			(GenreParentID = @GenreParentID)
		ORDER BY	GenreName
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoWithMeta_Select_bup]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan
-- alter date: 07/15/2009
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoWithMeta_Select_bup] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey  varchar(255),
	@VideoID int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
			-- Insert statements for procedure here
			SELECT     l.LanguageName AS PrimaryLanguage, sl.LanguageName AS SecondaryLanguage, sul.LanguageName AS SubtitleLanguage, vm.YearOfRelease, 
							  vm.AwardTextLong, vm.AwardTextShort, dt_Country.CountryName, vm.Synopsis
			FROM         dt_VideoMeta AS vm WITH(NOLOCK) LEFT OUTER JOIN
								  dt_Country ON vm.CountryID = dt_Country.ID LEFT OUTER JOIN
								  dt_Language AS sul WITH(NOLOCK) ON vm.SubtitleLanguageID = sul.ID LEFT OUTER JOIN
								  dt_Language AS sl WITH(NOLOCK) ON vm.SubtitleLanguageID = sl.ID LEFT OUTER JOIN
								  dt_Language AS l WITH(NOLOCK) ON vm.LanguageID = l.ID AND vm.LanguageID = l.ID
			WHERE     (vm.StatusID = 1) AND (vm.VideoID = @VideoID OR @VideoID = 0)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoMeta_Select]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan
-- alter date: 07/15/2009
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoMeta_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey  varchar(255),
	@VideoID int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
			-- Insert statements for procedure here
			SELECT		vm.LanguageID AS PrimaryLanguageID
						, l.LanguageName AS PrimaryLanguage
						, vm.SecondaryLanguageID
						, sl.LanguageName AS SecondaryLanguage
						, vm.SubtitleLanguageID
						, sul.LanguageName AS SubtitleLanguage
						, vm.YearOfRelease
						, vm.AwardTextLong
						, vm.AwardTextShort
						, vm.CountryID
						, dt_Country.CountryName
						, vm.Synopsis
			FROM         dt_VideoMeta AS vm WITH(NOLOCK) LEFT OUTER JOIN
								  dt_Country ON vm.CountryID = dt_Country.ID LEFT OUTER JOIN
								  dt_Language AS sul WITH(NOLOCK) ON vm.SubtitleLanguageID = sul.ID LEFT OUTER JOIN
								  dt_Language AS sl WITH(NOLOCK) ON vm.SecondaryLanguageID = sl.ID LEFT OUTER JOIN
								  dt_Language AS l WITH(NOLOCK) ON vm.LanguageID = l.ID AND vm.LanguageID = l.ID
			WHERE     (vm.StatusID = 1) AND (vm.VideoID = @VideoID OR @VideoID = 0)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoLog_Select]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_VideoLog_Select
-- Author:	Desigan Royan - DStv Online
-- alter date:	7/13/2009 12:13:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoLog table
-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_VideoLog_Select]
	@AuthenticationKey varchar(255),
	@VideoId int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		SELECT		VideoID, SUM(ViewCount) AS VideoCount
		FROM        dt_VideoLog WITH(NOLOCK)
		WHERE		([StatusID] = 1 OR [StatusID] = NULL)
		AND			WebSiteID = 
						(
							SELECT     ID
							FROM         dt_Website WITH(NOLOCK)
							WHERE     AuthenticationKey = @AuthenticationKey
						)
		AND			(VideoID = @VideoID OR @VideoId = 0)
		GROUP BY VideoID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoLog_Insert]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entity Name:	ws_dt_VideoLog_Insert
-- Author:		Desigan Royan
-- alter date: 07/13/2009 17:22 pm
-- Description:	This stored procedure is intended for inserting rows into the dt_VideoLog table
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoLog_Insert] 
	@AuthenticationKey  varchar(255), 
	@VideoId int = 0,
	@IsAuthenticated bit OUTPUT,
	@ReturnStatus tinyint OUTPUT
	
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @IsAuthenticated = 0
	SET @ReturnStatus = 0

	DECLARE @ItemId int, @WebsiteID int

	SET @WebsiteID =(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
	)

	IF (@WebsiteID > 0)
	BEGIN
		SET @IsAuthenticated = 1

		DECLARE @StartDate datetime, @EndDate datetime

		SET @StartDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 00:00' AS datetime) 
		SET @EndDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 23:59' AS datetime) 

		SET @ItemId =(
			SELECT	ID FROM	dt_VideoLog WITH(NOLOCK)
			WHERE	(WebSiteId = @WebSiteId)
			AND		(VideoID = @VideoId)
			AND		(LastViewed BETWEEN @StartDate AND @EndDate)
		)

		IF (@ItemId > 0)
			BEGIN
				SET @ReturnStatus = 2
				UPDATE  dt_VideoLog
				SET		ViewCount = ViewCount + 1
						,LastViewed = GETDATE()
				WHERE	(ID = @ItemId)
				
			END
		ELSE
			BEGIN
				SET @ReturnStatus = 1
				INSERT INTO dt_VideoLog
							(VideoID, WebsiteID, ViewCount,StatusID)
				VALUES		(@VideoId, @WebSiteId,1,1)
			END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Rating_Select]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_Rating_Select
-- Author:	Desigan Royan - DStv Online
-- alter date:	7/13/2009 12:13:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Rating table

-- Entity Name:	mms_dt_Rating_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:20:12 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Rating table
-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_Rating_Select]
	@AuthenticationKey varchar(255),
	@VideoId int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website WITH(NOLOCK)
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		SELECT     VideoID, ROUND(AVG(Rating), 0) AS Rating
		FROM    dt_Rating WITH (NOLOCK)
		
		WHERE		([StatusID] = 1 OR [StatusID] = NULL)
		AND			WebSiteID = 
						(
							SELECT     ID
							FROM         dt_Website WITH(NOLOCK)
							WHERE     AuthenticationKey = @AuthenticationKey
						)
		AND			(VideoID = @VideoID OR @VideoId = 0)
		GROUP BY VideoID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Rating_Insert]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entity Name:	ws_dt_Rating_Insert
-- Author:		Desigan Royan
-- alter date: 07/13/2009 12:27 pm
-- Description:	This stored procedure is intended for inserting rows into the dt_Rating table
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Rating_Insert] 
	@AuthenticationKey  varchar(255), 
	@VideoId int = 0,
	@Rating tinyint = 0,
	@IsAuthenticated bit OUTPUT--,
	--@ReturnStatus tinyint OUTPUT
	
AS
BEGIN
	SET NOCOUNT ON;

	SET @IsAuthenticated = 0
	--SET @ReturnStatus = 0

	DECLARE @ItemId int, @WebsiteID int

	SET @WebsiteID =(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
	)

	IF (@WebsiteID > 0)
	BEGIN
		SET @IsAuthenticated = 1

		DECLARE @StartDate datetime, @EndDate datetime
		
		SET @StartDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 00:00' AS datetime) 
		SET @EndDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 23:59' AS datetime) 

		SET @ItemId =(
			SELECT	ID FROM	dt_Rating WITH(NOLOCK)
			WHERE	(WebSiteId = @WebSiteId)
			AND		(VideoID = @VideoId)
			AND LastModified BETWEEN @StartDate AND @EndDate
		 )

		IF (@ItemId > 0)
			BEGIN
				--SET @ReturnStatus = 2
				UPDATE	dt_Rating
				SET     Rating = ((Rating*RateCount)+@Rating)/(RateCount+1),
						RateCount = RateCount +1
				WHERE	(ID = @ItemId)
			END
		ELSE
			BEGIN
				--SET @ReturnStatus = 1
				INSERT INTO dt_Rating
								  (WebsiteID, VideoID, StatusID, Rating)
				VALUES     (@WebSiteId, @VideoId, 1, @Rating)
			END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Program_Rating_SelectAllBySiteVideo]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_Rating_SelectAll
-- Author:	Jacques du Preez - DStv Online
-- alter date:	2009-10-13 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Rating table
-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_Program_Rating_SelectAllBySiteVideo]
@AuthenticationKey varchar(255),
@ProgramID int
As
Begin
	Select 
		[ID],
		[ProgramID],
		[Rating],
		[LastModified]
	From dt_Program_Rating where [StatusID] = 1 or [StatusID] = NULL 
	and WebSiteID = (select ID from dt_WebSite where dt_WebSite.AuthenticationKey = @AuthenticationKey)
	and ProgramID = @ProgramID
End
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Program_Rating_Select]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_Program_Rating_Select
-- Author:	Jacques du Preez - DStv Online
-- alter date:	2009-10-12 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Rating table
-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_Program_Rating_Select]
	@AuthenticationKey varchar(255),
	@ProgramId int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website WITH(NOLOCK)
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		SELECT     ProgramID, ROUND(AVG(Rating), 0) AS Rating
		FROM    dt_Program_Rating WITH (NOLOCK)
		
		WHERE		([StatusID] = 1 OR [StatusID] = NULL)
		AND			WebSiteID = 
						(
							SELECT     ID
							FROM         dt_Website WITH(NOLOCK)
							WHERE     AuthenticationKey = @AuthenticationKey
						)
		AND			(ProgramID = @ProgramID OR @ProgramId = 0)
		GROUP BY ProgramID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Program_Rating_Insert]    Script Date: 04/29/2011 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entity Name:	ws_dt_Rating_Insert
-- Author:		Desigan Royan
-- alter date: 07/13/2009 12:27 pm
-- Description:	This stored procedure is intended for inserting rows into the dt_Rating table
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Program_Rating_Insert] 
	@AuthenticationKey  varchar(255), 
	@ProgramId int = 0,
	@Rating tinyint = 0,
	@IsAuthenticated bit OUTPUT--,
	--@ReturnStatus tinyint OUTPUT
	
AS
BEGIN
	SET NOCOUNT ON;

	SET @IsAuthenticated = 0
	--SET @ReturnStatus = 0

	DECLARE @ItemId int, @WebsiteID int

	SET @WebsiteID =(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
	)

	IF (@WebsiteID > 0)
	BEGIN
		SET @IsAuthenticated = 1

		DECLARE @StartDate datetime, @EndDate datetime
		
		SET @StartDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 00:00' AS datetime) 
		SET @EndDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 23:59' AS datetime) 

		SET @ItemId =(
			SELECT	ID FROM	dt_Program_Rating WITH(NOLOCK)
			WHERE	(WebSiteId = @WebSiteId)
			AND		(ProgramID = @ProgramId)
			AND LastModified BETWEEN @StartDate AND @EndDate
		 )

		IF (@ItemId > 0)
			BEGIN
				--SET @ReturnStatus = 2
				UPDATE	dt_Program_Rating
				SET     Rating = ((Rating*RateCount)+@Rating)/(RateCount+1),
						RateCount = RateCount +1
				WHERE	(ID = @ItemId)
			END
		ELSE
			BEGIN
				--SET @ReturnStatus = 1
				INSERT INTO dt_Program_Rating
								  (WebsiteID, ProgramID, StatusID, Rating)
				VALUES     (@WebSiteId, @ProgramId, 1, @Rating)
			END
	END
END
GO
/****** Object:  View [dbo].[vw_TempVideoMeta]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_TempVideoMeta]
AS
SELECT DISTINCT 
                      ID, VideoTitle, Keywords, Abstract, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, FileName, Director, PrimaryLanguage, SecondaryLanguage, 
                      SubtitleLanguage, YearOfRelease, AwardTextLong, AwardTextShort, Synopsis, CountryName, GenreName, ProgramID, ProgramTypeID, VideoFileTypeID, StatusID, 
                      StartDate, EndDate, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, DirectorId, SubGenreName, CreatedDate, 
                      AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription, BillboardImageURL
                      ,Price,
								  Geolocation, 
								  RuntimeInMinutes, 
								  RentalPeriodInHours,
								  CastCrewID,
								  SizeInKB, 
								  VideoDimension
FROM         dbo.tt_Video_Meta
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tt_Video_Meta"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 218
               Right = 285
            End
            DisplayFlags = 280
            TopColumn = 31
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_TempVideoMeta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_TempVideoMeta'
GO
/****** Object:  View [dbo].[Video_ViewCount]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Video_ViewCount]
AS
SELECT     WebsiteID, VideoID, ROUND(AVG(ViewCount), 0) AS ViewCount
FROM         dbo.dt_VideoLog
GROUP BY WebsiteID, VideoID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dt_VideoLog"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_ViewCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_ViewCount'
GO
/****** Object:  View [dbo].[Video_Rating]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Video_Rating]
AS
SELECT     WebsiteID, VideoID, ROUND(AVG(Rating), 0) AS Rating
FROM         dbo.dt_Rating
GROUP BY WebsiteID, VideoID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dt_Rating"
            Begin Extent = 
               Top = 78
               Left = 359
               Bottom = 322
               Right = 569
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_Rating'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_Rating'
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Country_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ws_dt_Country_Select] 
      
AS
BEGIN
SELECT     dt_Country.ID, dt_Country.CountryName, dt_Country.FlagImageURL AS CountryImage, dt_Country.GMT, dt_Country.DefaultLanguageID, dt_Country.DSN, 
                      dt_Country.NamiDSN AS NamitechDSN, dt_Country.DefaultFootprintID, dt_Footprint.FootprintName AS DefaultFootprintName, 
                      dt_Language.LanguageName AS DefaultLanguageName
FROM         dt_Country WITH (nolock) INNER JOIN
                      dt_Footprint ON dt_Country.DefaultFootprintID = dt_Footprint.ID INNER JOIN
                      dt_Language ON dt_Country.DefaultLanguageID = dt_Language.ID
WHERE     (dt_Country.StatusID = 1) OR
                      (dt_Country.StatusID = NULL)
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Country_DStv_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ws_dt_Country_DStv_Select] 
      
AS
BEGIN
SELECT     dt_Country.ID, dt_Country.CountryName, dt_Country.FlagImageURL AS CountryImage, dt_Country.GMT, dt_Country.DefaultLanguageID, dt_Country.DSN, 
                      dt_Country.NamiDSN AS NamitechDSN, dt_Country.DefaultFootprintID, dt_Language.LanguageName AS DefaultLanguageName, 
                      dt_Footprint.FootprintName AS DefaultFootprintName
FROM         dt_Country WITH (nolock) INNER JOIN
                      dt_Footprint ON dt_Country.DefaultFootprintID = dt_Footprint.ID INNER JOIN
                      dt_Language ON dt_Country.DefaultLanguageID = dt_Language.ID
WHERE     (dt_Country.DefaultFootprintID IS NOT NULL) AND (dt_Country.StatusID = 1 OR
                      dt_Country.StatusID = NULL)
END
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_Update]
	@Id int,
	@ProductName varchar(255),
	@ProductDescription varchar(max),
	@StatusID tinyint
As
Begin
	UPDATE    dt_Product
	SET              
		ProductName = @ProductName,
		ProductDescription = @ProductDescription,
		StatusID = @StatusID
WHERE dt_Product.ID = @Id

End
GO
/****** Object:  StoredProcedure [dbo].[site_ErrorLog_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan
-- alter date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[site_ErrorLog_Insert] 
	-- Add the parameters for the stored procedure here
	@Location varchar(500), 
	@ErrorDetails varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dt_ErrorLog
                      (Location, ErrorDetails)
	VALUES     (@Location,@ErrorDetails)
END
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_Insert]
	@ProductName varchar(255),
	@ProductDescription varchar(max),
	@StatusID tinyint
As
Begin
	Insert Into dt_Product
		(ProductName, ProductDescription, CreatedDate, StatusID)
	Values
		(@ProductName, @ProductDescription, GETDATE(), @StatusID)
		
		
Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_Delete]
	@Id int
As
Begin
	UPDATE    dt_Product
	SET 
		StatusID = 0
WHERE dt_Product.ID = @Id

End
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Website_VideoCategory_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_lt_Website_VideoCategory_Insert]
	@WebsiteID int,
	@VideoCategoryID int
As
Begin
	Insert Into lt_Website_VideoCategory
		([WebsiteID],[VideoCategoryID])
	Values
		(@WebsiteID,@VideoCategoryID)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Website_VideoCategory_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_lt_Website_VideoCategory_Delete]
	@VideoCategoryID int

As
Begin

	BEGIN 
		DELETE from lt_Website_VideoCategory where VideoCategoryID = @VideoCategoryID
	END
End
GO
/****** Object:  View [dbo].[Program_Rating]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Program_Rating]
AS
SELECT     WebsiteID, ProgramID, ROUND(AVG(Rating), 0) AS Rating
FROM         dbo.dt_Program_Rating
GROUP BY WebsiteID, ProgramID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dt_Program_Rating"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Program_Rating'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Program_Rating'
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Video_Tag_insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 16-04-2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[mms_lt_Video_Tag_insert] 
	-- Add the parameters for the stored procedure here
	@VideoId int = 0, 
	@Keywords varchar(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM [lt_Video_Tag] WHERE VideoId = @VideoId
	
	IF LEN(@Keywords) > 0
	BEGIN
	INSERT INTO [lt_Video_Tag]
           ([VideoId]
           ,[Tag]
           ,[CreatedDate])
	SELECT @VideoId, Value, GETDATE() from array(@Keywords)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Program_Tag_insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 16-04-2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[mms_lt_Program_Tag_insert] 
	-- Add the parameters for the stored procedure here
	@ProgramId int = 0, 
	@Keywords varchar(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM [lt_Program_Tag] WHERE ProgramId = @ProgramId
	IF LEN(@Keywords) > 0
	BEGIN
	INSERT INTO [lt_Program_Tag]
           ([ProgramId]
           ,[Tag]
           ,[CreatedDate])
	SELECT @ProgramId, Value, GETDATE() from array(@Keywords)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoCategory_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoCategory_Update
-- Author:	Desigan Royan - DStv Online
-- alter date:	29/06/2010
-- Description:	This stored procedure is intended for updating dt_VideoCategory table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoCategory_Update]
	@ID int,
	@CategoryName varchar(255)
As
Begin
	Update dt_VideoCategory
	Set
		[CategoryName] = @CategoryName
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoMeta_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoMeta_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:01:49 PM
-- Description:	This stored procedure is intended for inserting values to dt_VideoMeta table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoMeta_Insert]
	@VideoID int,
	@LanguageID smallint,
	@SecondaryLanguageID smallint,
	@SubtitleLanguageID smallint,
	@YearOfRelease smallint = NULL,
	@AwardTextLong nvarchar(MAX) = NULL,
	@AwardTextShort varchar(500) = NULL,
	@CountryID smallint = NULL,
	@Synopsis nvarchar(max)
As
Begin

	IF (@SecondaryLanguageID = 0 or @SecondaryLanguageID = NULL or (@SecondaryLanguageID =''))
	BEGIN
		SET @SecondaryLanguageID = NULL
	END
	
	IF ((@SubtitleLanguageID = 0) or (@SubtitleLanguageID = NULL) or (@SubtitleLanguageID =''))
	BEGIN
		SET @SubtitleLanguageID = NULL
	END
	
	Insert Into dt_VideoMeta
		([VideoID],[LanguageID],[SecondaryLanguageID],[SubtitleLanguageID],[YearOfRelease],[AwardTextLong],[AwardTextShort],[CountryID],[Synopsis])
	Values
		(@VideoID,@LanguageID,@SecondaryLanguageID,@SubtitleLanguageID,@YearOfRelease,@AwardTextLong,@AwardTextShort,@CountryID,@Synopsis)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoMeta_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoMeta_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:01:49 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_VideoMeta table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoMeta_DeleteRow]
	@ID int
As
Begin
	update dt_VideoMeta
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoLog_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Entity Name:	ws_dt_VideoLog_Insert
-- Author:		Desigan Royan
-- alter date: 07/13/2009 17:22 pm
-- Description:	This stored procedure is intended for inserting rows into the dt_VideoLog table
-- =============================================
CREATE PROCEDURE [dbo].[mms_dt_VideoLog_Insert] 
	@AuthenticationKey  varchar(255), 
	@VideoId int = 0	
AS
BEGIN
	SET NOCOUNT ON;
	

	DECLARE @ItemId int, @WebsiteID int, @IsAuthenticated bit
	SET @IsAuthenticated = 0

	SET @WebsiteID =(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
	)

	IF (@WebsiteID > 0)
	BEGIN
		SET @IsAuthenticated = 1

		DECLARE @StartDate datetime, @EndDate datetime

		SET @StartDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 00:00' AS datetime) 
		SET @EndDate = CAST(CONVERT(nvarchar(30), GETDATE(), 111) + ' 23:59' AS datetime) 

		SET @ItemId =(
			SELECT	ID FROM	dt_VideoLog WITH(NOLOCK)
			WHERE	(WebSiteId = @WebSiteId)
			AND		(VideoID = @VideoId)
			AND		(LastViewed BETWEEN @StartDate AND @EndDate)
		)

		IF (@ItemId > 0)
			BEGIN
				UPDATE  dt_VideoLog
				SET		ViewCount = ViewCount + 1
						,LastViewed = GETDATE()
				WHERE	(ID = @ItemId)
				
			END
		ELSE
			BEGIN
				INSERT INTO dt_VideoLog
							(VideoID, WebsiteID, ViewCount,StatusID)
				VALUES		(@VideoId, @WebSiteId,1,1)
			END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFileType_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFileType_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:45:20 PM
-- Description:	This stored procedure is intended for updating dt_VideoFileType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFileType_Update]
	@ID tinyint,
	@FileExtension varchar(20),
	@Description varchar(100)
As
Begin
	Update dt_VideoFileType
	Set
		[FileExtension] = @FileExtension,
		[Description] = @Description
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFileType_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFileType_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:45:20 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoFileType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFileType_SelectAll]
As
Begin
	Select 
		[ID],
		[FileExtension],
		[Description]
	From dt_VideoFileType where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFileType_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFileType_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:45:20 PM
-- Description:	This stored procedure is intended for inserting values to dt_VideoFileType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFileType_Insert]
	@FileExtension varchar(20),
	@Description varchar(100)
As
Begin
	Insert Into dt_VideoFileType
		([FileExtension],[Description])
	Values
		(@FileExtension,@Description)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFileType_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFileType_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:45:20 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_VideoFileType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFileType_DeleteRow]
	@ID tinyint
As
Begin
	update dt_VideoFileType
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFilePath_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFilePath_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:40:55 PM
-- Description:	This stored procedure is intended for updating dt_VideoFilePath table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFilePath_Update]
	@ID smallint,
	@VideoFilePathName varchar(80),
	@ServerPath varchar(500)
As
Begin
	Update dt_VideoFilePath
	Set
		[VideoFilePathName] = @VideoFilePathName,
		[ServerPath] = @ServerPath
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFilePath_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFilePath_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:40:55 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoFilePath table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFilePath_SelectAll]
As
Begin
	Select 
		[ID],
		[VideoFilePathName],
		[ServerPath],
		[CreatedDate]
	From dt_VideoFilePath where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFilePath_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFilePath_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:40:55 PM
-- Description:	This stored procedure is intended for inserting values to dt_VideoFilePath table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFilePath_Insert]
	@VideoFilePathName varchar(80),
	@ServerPath varchar(500)
As
Begin
	Insert Into dt_VideoFilePath
		([VideoFilePathName],[ServerPath])
	Values
		(@VideoFilePathName,@ServerPath)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFilePath_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFilePath_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:40:55 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_VideoFilePath table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFilePath_DeleteRow]
	@ID smallint
As
Begin
	update dt_VideoFilePath
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Website_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Website_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:03:41 PM
-- Description:	This stored procedure is intended for updating dt_Website table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Website_Update]
	@ID smallint,
	@WebsiteName varchar(150),
	@WebsiteURL varchar(500),
	@AuthenticationKey varchar(255)
As
Begin
	Update dt_Website
	Set
		[WebsiteName] = @WebsiteName,
		[WebsiteURL] = @WebsiteURL,
		[AuthenticationKey] = @AuthenticationKey
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_WebSite_SelectByCategoryID]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	[mms_dt_Channel_SelectAllByProgram]
-- Author:	Desigan Royan - DStv Online
-- alter date:	12/04/2010 12:13:54 PM

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_WebSite_SelectByCategoryID]
@ID int
As
Begin
SELECT     lt_Website_VideoCategory.WebsiteID, lt_Website_VideoCategory.VideoCategoryID, dt_Website.WebsiteName
FROM         lt_Website_VideoCategory LEFT OUTER JOIN
                      dt_Website ON lt_Website_VideoCategory.WebsiteID = dt_Website.ID
WHERE     (lt_Website_VideoCategory.VideoCategoryID = @ID)

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Website_SelectAllByVideoCategory]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	[mms_dt_Channel_SelectAllByProgram]
-- Author:	Desigan Royan - DStv Online
-- alter date:	12/04/2010 12:13:54 PM

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Website_SelectAllByVideoCategory]
@VideoCategoryID int
As
Begin

SELECT     w.WebsiteName, w.ID
FROM         dt_Website AS w INNER JOIN
                      lt_Website_VideoCategory AS wvc ON w.ID = wvc.WebsiteID
WHERE     (wvc.VideoCategoryID = @VideoCategoryID)

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Website_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Website_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:03:41 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Website table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Website_SelectAll]
As
Begin
	Select 
		[ID],
		[WebsiteName],
		[WebsiteURL],
		[AuthenticationKey]
	From dt_Website where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Website_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Website_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:03:41 PM
-- Description:	This stored procedure is intended for inserting values to dt_Website table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Website_Insert]
	@WebsiteName varchar(150),
	@WebsiteURL varchar(500),
	@AuthenticationKey varchar(255),
	@VideoAccess bit = 1
As
Begin
	Insert Into dt_Website
		([WebsiteName],[WebsiteURL],[AuthenticationKey],[VideoAccess])
	Values
		(@WebsiteName,@WebsiteURL,@AuthenticationKey,@VideoAccess)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Website_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Website_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:03:41 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Website table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Website_DeleteRow]
	@ID smallint
As
Begin
	update dt_Website
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoMeta_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoMeta_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:01:49 PM
-- Description:	This stored procedure is intended for updating dt_VideoMeta table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoMeta_Update]
	@ID int,
	@LanguageID smallint,
	@SecondaryLanguageID smallint,
	@SubtitleLanguageID smallint,
	@YearOfRelease smallint,
	@AwardTextLong nvarchar(MAX),
	@AwardTextShort varchar(500),
	@CountryID smallint,
	@Synopsis nvarchar(max)
As
Begin

	IF (@SecondaryLanguageID = 0 or @SecondaryLanguageID = NULL or (@SecondaryLanguageID =''))
	BEGIN
		SET @SecondaryLanguageID = NULL
	END
	
	IF ((@SubtitleLanguageID = 0) or (@SubtitleLanguageID = NULL) or (@SubtitleLanguageID =''))
	BEGIN
		SET @SubtitleLanguageID = NULL
	END

	Update dt_VideoMeta
	Set
		[LanguageID] = @LanguageID,
		[SecondaryLanguageID] = @SecondaryLanguageID,
		[SubtitleLanguageID] = @SubtitleLanguageID,
		[YearOfRelease] = @YearOfRelease,
		[AwardTextLong] = @AwardTextLong,
		[AwardTextShort] = @AwardTextShort,
		[CountryID] = @CountryID,
		[Synopsis] = @Synopsis
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoCategory_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoCategory_SelectAl
-- Author:	Desigan Royan - DStv Online
-- alter date:	29/06/2010
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoCategory_SelectAll]
AS
BEGIN
SELECT     [ID], CategoryName, CreatedDate
FROM         dt_VideoCategory
WHERE     (StatusID = 1)
ORDER BY CategoryName

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoCategory_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoCategory_SelectAl
-- Author:	Desigan Royan - DStv Online
-- alter date:	29/06/2010
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoCategory_Select]
@Search nvarchar(50),
@WebSiteID int
AS
BEGIN
SELECT   Distinct  dt_VideoCategory.CategoryName, dt_VideoCategory.CreatedDate, dt_VideoCategory.ID--, lt_Website_VideoCategory.WebsiteID
                      --, lt_Website_VideoCategory.VideoCategoryID, dt_Website.WebsiteName, dt_Website.AuthenticationKey
FROM         dt_VideoCategory INNER JOIN
                      lt_Website_VideoCategory ON dt_VideoCategory.ID = lt_Website_VideoCategory.VideoCategoryID INNER JOIN
                      dt_Website ON lt_Website_VideoCategory.WebsiteID = dt_Website.ID
WHERE     (dt_VideoCategory.StatusID = 1)
AND
(dt_VideoCategory.CategoryName like '%'+ @Search +'%' OR dt_Website.WebsiteName like '%'+ @Search +'%' OR @Search like '') AND
(WebsiteID = @WebSiteID OR @WebSiteID = 0)
ORDER BY CategoryName, CreatedDate, ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoCategory_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoCategory_Insert
-- Author:	Desigan Royan - DStv Online
-- alter date:	29/06/2010
-- Description:	This stored procedure is intended for inserting values to dt_VideoCategory table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoCategory_Insert]
	@CategoryName varchar(255)
As
Begin
	
	INSERT INTO dt_VideoCategory
						  (CategoryName)
	VALUES     (@CategoryName)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoCategory_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoCategory_DeleteRow
-- Author:	Desigan Royan - DStv Online
-- alter date:	29/06/2010
-- Description:	This stored procedure is intended for deleting a specific row from dt_VideoCategory table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoCategory_DeleteRow]
	@ID int
As
Begin
	update dt_VideoCategory
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_SelectListById]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Program table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Program_SelectListById] -- [dbo].[mms_dt_Program_SelectListById] 51
@Id int
As
Begin
	Select 
		[ID],
		[ProgramName],
		[CreatedDate]
	From dt_Program where [ID] = @Id AND [StatusID] = 1 or [StatusID] = NULL 
	
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_SelectList]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Program table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Program_SelectList]
As
Begin
	Select 
		[ID],
		[ProgramName],
		[CreatedDate]
	From dt_Program where [StatusID] = 1 or [StatusID] = NULL 
	ORDER BY ProgramName ASC
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Status_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Status_Select]	
As
Begin
	SELECT     ID, [Status]
	FROM         dt_Status
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ProgramType_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ProgramType_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:19:23 PM
-- Description:	This stored procedure is intended for updating dt_ProgramType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ProgramType_Update]
	@ID smallint,
	@ProgramTypeName varchar(100),
	@RedirectURL varchar(500)
As
Begin
	Update dt_ProgramType
	Set
		[ProgramTypeName] = @ProgramTypeName,
		[RedirectURL] = @RedirectURL
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ProgramType_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ProgramType_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:19:23 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_ProgramType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ProgramType_SelectAll]
As
Begin
	Select 
		[ID],
		[ProgramTypeName],
		[RedirectURL],
		[CreatedDate]
	From dt_ProgramType where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ProgramType_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ProgramType_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:19:23 PM
-- Description:	This stored procedure is intended for inserting values to dt_ProgramType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ProgramType_Insert]
	@ProgramTypeName varchar(100),
	@RedirectURL varchar(500) = NULL
As
Begin
	Insert Into dt_ProgramType
		([ProgramTypeName],[RedirectURL])
	Values
		(@ProgramTypeName,@RedirectURL)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_ProgramType_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_ProgramType_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:19:23 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_ProgramType table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_ProgramType_DeleteRow]
	@ID smallint
As
Begin
	Update dt_ProgramType
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for updating dt_Program table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Program_Update]
	@ID int,
	@ProgramName varchar(150),
	@ProgramTypeID int,
	@Abstract varchar(500),
	@ImageURL varchar(500),
	@BillboardURL varchar(500),
	--@GenreID smallint,
	--@SubGenreID smallint = NULL,
	@GeoGroupID int,
	@WebsiteURL varchar(500),
	@Keywords varchar(1000),
	@StartDate datetime,
	@EndDate datetime
As
--Begin
--IF (@SubGenreID = null or @SubGenreID = 0)
Begin
	Update dt_Program
	Set
		[ProgramName] = @ProgramName,
		[ProgramTypeID] = @ProgramTypeID,
		[Abstract] = @Abstract,
		[ImageURL] = @ImageURL,
		[BillboardURL] = @BillboardURL,
		--[GenreID] = @GenreID,
		[GeoGroupID] = @GeoGroupID,
	    [WebsiteURL] = @WebsiteURL,
	    [Keywords] = @Keywords,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate
	Where		
		[ID] = @ID
END
--exec [mms_dt_Program_Update] '30 Rock', 1, 'tina fey', 62, 1, ''

--ELSE 
--BEGIN
--Update dt_Program
--	Set
--		[ProgramName] = @ProgramName,
--		[ProgramTypeID] = @ProgramTypeID,
--		[Abstract] = @Abstract,
--		[ImageURL] = @ImageURL,
--		[BillboardURL] = @BillboardURL,
--		--[GenreID] = @GenreID,
--		--[SubGenreID] = @SubGenreID,
--		[GeoGroupID] = @GeoGroupID,
--	    [WebsiteURL] = @WebsiteURL,
--	    [Keywords] = @Keywords
--	Where		
--		[ID] = @ID

------ Code by Sameer Kulkarni: 29OCT2010, Friday
		
----Update lt_Program_Genre
----	Set
----		[ProgramID] = @ID,		
----		[SubGenreID] = @GenreID	
----	Where		
----		[ID] = @ID
--	END
--End



--EXEC mms_lt_Program_Tag_insert @ID, @Keywords
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Product_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Product_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for deleting from dt_Product table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Product_DeleteRow]
@ID smallint
As
Begin
UPDATE    dt_Product
SET              StatusID = 4
WHERE     (ID = @ID)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Person_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Person_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:13:54 PM
-- Description:	This stored procedure is intended for updating dt_Person table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Person_Update]
	@ID int,
	@FullName varchar(80),
--	@ProfessionID smallint,
	@Bio varchar(max),
	@Awards nvarchar(500), 
	@Gender char(1),
	@BirthPlace varchar(80),
	@BirthDate varchar(20),
	@ImageURL varchar(500)
As
Begin
UPDATE    dt_Person
SET              FullName = @FullName, Bio = @Bio, AwardsText = @Awards, BirthPlace = @BirthPlace, BirthDate = @BirthDate, Gender = @Gender, 
                      ImageURL = @ImageURL
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Product_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Product_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Product table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Product_SelectAll]
AS
BEGIN
	SELECT     ID, ProductName, ProductDescription as LongDescription, ProductDescription, ProductIcon, MediumLogo, LargeLogo, CreatedDate, StatusID
	FROM         dt_Product
	--WHERE StatusID = 1
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Profession_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Profession_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:16:53 PM
-- Description:	This stored procedure is intended for updating dt_Profession table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Profession_Update]
	@ID smallint,
	@ProfessionName varchar(100),
	@Rank int
As
Begin
	Update dt_Profession
	Set
		[ProfessionName] = @ProfessionName, [Rank] = @Rank
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Profession_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Profession_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:16:53 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Profession table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Profession_SelectAll]
As
Begin
	Select 
		[ID],
		[ProfessionName],
		[Rank]
	From dt_Profession where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Profession_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Profession_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:16:53 PM
-- Description:	This stored procedure is intended for inserting values to dt_Profession table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Profession_Insert]
	@ProfessionName varchar(100),
	@Rank int
As
Begin
	Insert Into dt_Profession
		([ProfessionName],[Rank])
	Values
		(@ProfessionName,@Rank)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Profession_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Profession_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:16:53 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Profession table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Profession_DeleteRow]
	@ID smallint
As
Begin
	update dt_Profession
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Person_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Person_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:13:54 PM
-- Description:	This stored procedure is intended for inserting values to dt_Person table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Person_Insert]
	@FullName varchar(80),
--	@ProfessionID smallint,
	@Bio varchar(max),
	@Awards nvarchar(500), 
	@Gender char(1),
	@BirthPlace varchar(80),
	@BirthDate varchar(20),
	@ImageURL varchar(500)
As
Begin
	Insert Into dt_Person
		([FullName],[Bio],[AwardsText],[BirthPlace],[BirthDate],[Gender],[ImageURL])
	Values
		(@FullName,@Bio,@Awards,@BirthPlace,@BirthDate,@Gender,@ImageURL)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Person_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Person_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:13:54 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Person table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Person_DeleteRow]
	@ID int
As
Begin
	update dt_Person
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Language_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Language_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:12:30 PM
-- Description:	This stored procedure is intended for updating dt_Language table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Language_Update]
	@ID smallint,
	@LanguageName varchar(80)
As
Begin
	Update dt_Language
	Set
		[LanguageName] = @LanguageName
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Language_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Language_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:12:30 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Language table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Language_SelectAll]
As
Begin
	Select 
		[ID],
		[LanguageName]
	From dt_Language where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Language_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Language_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:12:30 PM
-- Description:	This stored procedure is intended for inserting values to dt_Language table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Language_Insert]
	@LanguageName varchar(80)
As
Begin
	Insert Into dt_Language
		([LanguageName])
	Values
		(@LanguageName)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Language_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Language_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:12:30 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Language table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Language_DeleteRow]
	@ID smallint
As
Begin
	update dt_Language
	SET  StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Genre_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:11:22 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Genre table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Genre_SelectAll]
As
Begin
	--Select 
	--	[ID],
	--	[GenreName],
	--	[GenreParentID],
	--	[CreatedDate]
	--From dt_Genre where [StatusID] = 1 or [StatusID] = NULL 
	
	--SELECT [DStvOnlineEPG].[dbo].[THE_THEME].[THE_ID] ID
 --     ,[DStvOnlineEPG].[dbo].[THE_THEME].[THE_DESCRIPTION] GenreName
	--FROM [DStvOnlineEPG].[dbo].[THE_THEME]
	
	SELECT [DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_ID] ID
      ,[DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_THE_ID]
      ,[DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_CODE]
      ,RTRIM([DStvOnlineEPG].[dbo].[THE_THEME].[THE_DESCRIPTION]) + ' | ' + [DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_DESCRIPTIONS] GenreName
    FROM [DStvOnlineEPG].[dbo].[STH_SUB_THEME] inner join [DStvOnlineEPG].[dbo].THE_THEME 
    ON [DStvOnlineEPG].[dbo].[STH_SUB_THEME].STH_THE_ID = [DStvOnlineEPG].[dbo].THE_THEME.THE_ID
	ORDER BY [DStvOnlineEPG].[dbo].[THE_THEME].[THE_DESCRIPTION]
End


	--SELECT [DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_ID] ID
 --     ,[DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_THE_ID]
 --     ,[DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_CODE]
 --     ,RTRIM([DStvOnlineEPG].[dbo].[THE_THEME].[THE_DESCRIPTION]) + ' - ' + [DStvOnlineEPG].[dbo].[STH_SUB_THEME].[STH_DESCRIPTIONS] GenreName
 -- FROM [DStvOnlineEPG].[dbo].[STH_SUB_THEME] inner join [DStvOnlineEPG].[dbo].THE_THEME 
 -- ON [DStvOnlineEPG].[dbo].[STH_SUB_THEME].STH_THE_ID = [DStvOnlineEPG].[dbo].THE_THEME.THE_ID
GO
/****** Object:  Table [dbo].[lt_Person_Profession]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Person_Profession](
	[PersonID] [int] NULL,
	[ProfessionID] [int] NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Person_Profession_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Channel_SelectAll]
AS
BEGIN
SELECT     dt_Channel.ChannelName, dt_Channel.ChannelNumber, dt_Channel.ShortName, dt_Channel.LanguageID, dt_Channel.ChannelTypeID, dt_Channel.GenreID, dt_Channel.SerID, 
                      dt_Channel.ChannelDescription, dt_Channel.ChannelDescriptionPor, dt_Channel.ChannelDescriptionFre, dt_Channel.SmallLogo, dt_Channel.MediumLogo, 
                      dt_Channel.LargeLogo, dt_Channel.WebsiteURL, dt_Channel.CreatedDate, dt_Channel.ID, dt_ChannelType.TypeName, dt_Genre.GenreName, 
                      dt_Language.LanguageName
FROM         dt_Channel INNER JOIN
                      dt_Language ON dt_Channel.LanguageID = dt_Language.ID INNER JOIN
                      dt_Genre ON dt_Channel.GenreID = dt_Genre.ID INNER JOIN
                      dt_ChannelType ON dt_Channel.ChannelTypeID = dt_ChannelType.ID
                      WHERE (dt_Channel.StatusID = 1 or dt_Channel.StatusID = NULL)
ORDER BY ChannelName ASC

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Channel_Insert]
@ChannelName varchar(255),
@ChannelNumber int,
@ShortName varchar(50),
@LanguageID smallint,
@ChannelTypeID smallint,
@GenreID smallint,
@SerID int,
@ChannelDescription varchar(max),
@ChannelDescriptionPor varchar(max),
@ChannelDescriptionFre varchar(max),
@SmallLogo varchar(500),
@MediumLogo varchar(500),
@LargeLogo varchar(500),
@WebsiteURL varchar(500)
As
Begin
INSERT INTO dt_Channel
                      (ChannelName, ChannelNumber, ShortName, LanguageID, ChannelTypeID, GenreID, SerID, ChannelDescription, ChannelDescriptionPor, ChannelDescriptionFre, 
                      SmallLogo, MediumLogo, LargeLogo, WebsiteURL)
VALUES     (@ChannelName,@ChannelNumber,@ShortName,@LanguageID,@ChannelTypeID,@GenreID,@SerID,@ChannelDescription,@ChannelDescriptionPor,@ChannelDescriptionFre,@SmallLogo,@MediumLogo,@LargeLogo,@WebsiteURL)

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================

CREATE Procedure [dbo].[mms_dt_Channel_DeleteRow]
@ID int
As
Begin
UPDATE    dt_Channel
SET              StatusID = 4
WHERE     (ID = @ID)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_AgeRestriction_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for updating dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_AgeRestriction_Update]
	@ID tinyint,
	@Abbreviation varchar(40),
	@AgeRestrictionDescription varchar(80)
As
Begin
	Update dt_AgeRestriction
	Set
		[Abbreviation] = @Abbreviation,
		[AgeRestrictionDescription] = @AgeRestrictionDescription
	Where		
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_AgeRestriction_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_AgeRestriction_SelectAll]
As
Begin
	Select 
		[ID],
		[Abbreviation],
		[AgeRestrictionDescription]
	From dt_AgeRestriction where [StatusID] = 1 or [StatusID] = NULL 
ORDER BY Abbreviation asc

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_AgeRestriction_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for inserting values to dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_AgeRestriction_Insert]
	@Abbreviation varchar(40),
	@AgeRestrictionDescription varchar(80) = NULL
As
Begin
	Insert Into dt_AgeRestriction
		([Abbreviation],[AgeRestrictionDescription])
	Values
		(@Abbreviation,@AgeRestrictionDescription)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_AgeRestriction_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_AgeRestriction table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_AgeRestriction_DeleteRow]
	@ID tinyint
As
Begin
UPDATE    dt_AgeRestriction
SET              StatusID = 4
WHERE     (ID = @ID)

End
GO
/****** Object:  View [dbo].[DStv_SubGenre_View]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[DStv_SubGenre_View]
AS
SELECT     TOP (1000) STH_ID, STH_THE_ID, STH_DESCRIPTIONS
FROM         DStvOnlineEPG.dbo.STH_SUB_THEME
GO
/****** Object:  View [dbo].[DStv_Genre_View]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[DStv_Genre_View]
AS
SELECT     TOP (1000) THE_ID, THE_DESCRIPTION
FROM         DStvOnlineEPG.dbo.THE_THEME
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "THE_THEME (DStvOnlineEPG.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2955
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStv_Genre_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStv_Genre_View'
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_AgeRestriction_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:03:08 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_AgeRestriction table
-- ==========================================================================================

CREATE Procedure [dbo].[mms_dt_Channel_Update]
@ID int,
@ChannelName varchar(255),
@ChannelNumber int,
@ShortName varchar(50),
@LanguageID smallint,
@ChannelTypeID smallint,
@GenreID smallint,
@SerID int,
@ChannelDescription varchar(max),
@ChannelDescriptionPor varchar(max),
@ChannelDescriptionFre varchar(max),
@SmallLogo varchar(500),
@MediumLogo varchar(500),
@LargeLogo varchar(500),
@WebsiteURL varchar(500)
As
Begin
UPDATE    dt_Channel
SET              ChannelName = @ChannelName, ChannelNumber = @ChannelNumber, ShortName = @ShortName, LanguageID = @LanguageID, ChannelTypeID = @ChannelTypeID, 
                      GenreID = @GenreID, SerID = @SerID, ChannelDescription = @ChannelDescription, ChannelDescriptionPor = @ChannelDescriptionPor, 
                      ChannelDescriptionFre = @ChannelDescriptionFre, SmallLogo = @SmallLogo, MediumLogo = @MediumLogo, LargeLogo = @LargeLogo, WebsiteURL = @WebsiteURL
WHERE dt_Channel.ID = @ID
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Country_SelectList]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Country_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Country table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Country_SelectList]
As
Begin
SELECT     ID, CountryName, ZIndex
FROM         dt_Country
WHERE     (StatusID = 1) OR
                      (StatusID = NULL)
ORDER BY CountryName
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Country_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Country_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Country table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Country_SelectAll]
As
Begin
SELECT     ID, CountryName, ZIndex, FlagImageURL, DefaultLanguageID, DefaultFootprintID, DSN, GMT, NamiDSN
FROM         dt_Country
WHERE     (StatusID = 1) OR
                      (StatusID = NULL)
ORDER BY CountryName
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Country_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Country_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for inserting values to dt_Country table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Country_Insert]
	@CountryName varchar(80), 
	@DefaultLanguageID smallint, 
	@FlagImageURL varchar(500), 
	@DefaultFootprintID tinyint, 
	@GMT smallint, 
	@DSN varchar(50), 
	@NamiDSN varchar(50),
	@ZIndex int = NULL
As
Begin
	INSERT INTO dt_Country
                      (CountryName, ZIndex, DefaultLanguageID, FlagImageURL, DefaultFootprintID, GMT, DSN, NamiDSN)
VALUES     (@CountryName,@ZIndex,@DefaultLanguageID,@FlagImageURL,@DefaultFootprintID,@GMT,@DSN,@NamiDSN)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Country_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Country_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:07:00 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Country table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Country_DeleteRow]
	@ID smallint
As
Begin
UPDATE    dt_Country
SET              StatusID = 4
WHERE     (ID = @ID)


End
GO
/****** Object:  Table [dbo].[lt_Website_Product]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Website_Product](
	[WebsiteID] [smallint] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Website_Product_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lt_Channel_Program]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Channel_Program](
	[ChannelID] [int] NOT NULL,
	[ProgramID] [int] NOT NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Channel_Program_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lt_Program_Genre]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Program_Genre](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramID] [int] NOT NULL,
	[GenreID] [int] NULL,
	[GenreName] [nvarchar](50) NULL,
	[SubGenreID] [int] NULL,
	[SubGenreName] [nvarchar](50) NULL,
	[ThemeID] [int] NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lt_Product_Program]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Product_Program](
	[ProductID] [int] NOT NULL,
	[ProgramID] [int] NOT NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Product_Program_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dt_EditorialList]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_EditorialList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[DisplayTitle] [varchar](255) NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dt_EditorialList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dt_EditorialList', @level2type=N'COLUMN',@level2name=N'StatusID'
GO
/****** Object:  Table [dbo].[dt_Video]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_Video](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VideoTitle] [varchar](200) NOT NULL,
	[Abstract] [varchar](500) NOT NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[BillboardImageURL] [varchar](500) NULL,
	[EPGID] [varchar](255) NULL,
	[ProgramID] [int] NOT NULL,
	[VideoProgramTypeID] [smallint] NOT NULL,
	[AgeRestrictionID] [tinyint] NOT NULL,
	[AirDate] [datetime] NULL,
	[Runtime] [varchar](50) NULL,
	[Season] [int] NULL,
	[Episode] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Keywords] [varchar](1000) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[OldId] [int] NULL,
	[OldSource] [int] NULL,
	[ExpiryDate] [datetime] NULL,
	[RuntimeInMinutes] [numeric](3, 0) NULL,
	[RentalPeriodInHours] [numeric](3, 0) NULL,
	[Price] [numeric](10, 2) NULL,
	[Geolocation] [numeric](3, 0) NULL,
	[GroupID] [numeric](3, 0) NULL,
	[CastCrewID] [int] NULL,
	[IBSProductID] [varchar](50) NULL,
	[AssetId] [varchar](50) NULL,
	[GeoGroupID] [int] NULL,
 CONSTRAINT [PK_dt_Video] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[DStvProgram_SubGenre_View]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DStvProgram_SubGenre_View]
AS
SELECT     STH_ID, STH_THE_ID, STH_CODE, STH_DESCRIPTIONS
FROM         DStvOnlineEPG.dbo.STH_SUB_THEME
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "STH_SUB_THEME (DStvOnlineEPG.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStvProgram_SubGenre_View'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStvProgram_SubGenre_View'
GO
/****** Object:  View [dbo].[DStvProgram_Genre_view]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DStvProgram_Genre_view]
AS
SELECT     DStvOnlineEPG.dbo.STH_SUB_THEME.STH_ID AS ID, DStvOnlineEPG.dbo.STH_SUB_THEME.STH_THE_ID, DStvOnlineEPG.dbo.STH_SUB_THEME.STH_CODE, 
                      RTRIM(DStvOnlineEPG.dbo.THE_THEME.THE_DESCRIPTION) + ' - ' + DStvOnlineEPG.dbo.STH_SUB_THEME.STH_DESCRIPTIONS AS GenreName
FROM         DStvOnlineEPG.dbo.STH_SUB_THEME INNER JOIN
                      DStvOnlineEPG.dbo.THE_THEME ON DStvOnlineEPG.dbo.STH_SUB_THEME.STH_THE_ID = DStvOnlineEPG.dbo.THE_THEME.THE_ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "STH_SUB_THEME (DStvOnlineEPG.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "THE_THEME (DStvOnlineEPG.dbo)"
            Begin Extent = 
               Top = 6
               Left = 264
               Bottom = 95
               Right = 446
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStvProgram_Genre_view'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStvProgram_Genre_view'
GO
/****** Object:  Table [dbo].[dt_VideoFile]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dt_VideoFile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VideoID] [int] NOT NULL,
	[FileName] [varchar](255) NOT NULL,
	[VideoFilePathID] [smallint] NOT NULL,
	[VideoFileTypeID] [tinyint] NOT NULL,
	[ProductId] [int] NULL,
	[ManItemId] [varchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[VideoDimension] [nvarchar](15) NULL,
	[SizeInKB] [nvarchar](18) NULL,
 CONSTRAINT [PK_dt_VideoFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[DStvChannels_Channel_view]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DStvChannels_Channel_view]
AS
SELECT DISTINCT Id, ChannelName, ChannelNumber, ChannelType, SmallLogo, MediumLogo, BigLogo
FROM         DSTVChannels.dbo.Channel
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Channel (DSTVChannels.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 235
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 9
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStvChannels_Channel_view'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DStvChannels_Channel_view'
GO
/****** Object:  Table [dbo].[lt_Person_Profession_Program]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lt_Person_Profession_Program](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonProfessionID] [int] NOT NULL,
	[ProgramID] [int] NOT NULL,
	[CharacterName] [varchar](200) NULL,
 CONSTRAINT [PK_lt_Person_Profession_Video] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lt_EditorialListItems]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_EditorialListItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EditorialListID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemTypeId] [smallint] NOT NULL,
	[ItemRank] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_lt_EditorialListItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1= Video; 2= Program;' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lt_EditorialListItems', @level2type=N'COLUMN',@level2name=N'ItemTypeId'
GO
/****** Object:  Table [dbo].[lt_Video_VideoCategory]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lt_Video_VideoCategory](
	[VideoID] [int] NOT NULL,
	[VideoCategoryID] [int] NOT NULL,
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [lt_Video_VideoCategory_PK_Table] PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialList_Video_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialList_Video_Select]

As
Begin

SELECT     /*ProgramName + ' - ' + */VideoTitle + ' - {'+CONVERT(varchar,dt_Video.ID) +'}'  as DisplayTitle, dt_Video.ID,  dt_Video.VideoTitle, dt_Video.ExpiryDate, dt_Video.ProgramID, dt_Program.ProgramName, dt_Video.StatusID, dt_Status.Status
FROM         dt_Video INNER JOIN
                      dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
                      dt_Status ON dt_Video.StatusID = dt_Status.ID

Where dt_Video.StatusID = 1
Order by DisplayTitle
	
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialList_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialList_Update]
	@ID int,
	@Title varchar(255),
	@DisplayTitle varchar(255),
	@StatusId tinyint = 1
As
Begin
	Update dt_EditorialList
	Set
		[Title] = @Title,DisplayTitle = @DisplayTitle, StatusID = @StatusId, CreatedDate = GETDATE()
		
		Where ID = @ID
	
		

	--Declare @ReferenceID int
	--SET @ReferenceID = @@IDENTITY
	

	--EXEC mms_lt_Video_Tag_insert @@IDENTITY, @Keywords

	
	--Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialList_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialList_Select]	
@Search varchar(255) = '',
@StatusID int = 0
As
Begin
	SELECT     dt_EditorialList.ID, dt_EditorialList.Title, dt_EditorialList.DisplayTitle, dt_EditorialList.StatusID, dt_Status.Status, dt_EditorialList.CreatedDate
FROM         dt_EditorialList INNER JOIN
                      dt_Status ON dt_EditorialList.StatusID = dt_Status.ID 
                      
                      Where  (StatusID = @StatusID OR @StatusID = 0) AND (Title like '%'+ @Search +'%' OR DisplayTitle like '%'+ @Search +'%' OR @Search = '' )
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialList_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialList_Insert]
	@Title varchar(255),
	@DisplayTitle varchar(255),
	@StatusId tinyint = 1
As
Begin
	Insert Into dt_EditorialList
		([Title],DisplayTitle, StatusID, CreatedDate)
	Values
		(@Title, @DisplayTitle, @StatusId, GETDATE())

	--Declare @ReferenceID int
	--SET @ReferenceID = @@IDENTITY
	

	--EXEC mms_lt_Video_Tag_insert @@IDENTITY, @Keywords

	
	--Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialList_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_EditorialList_DeleteRow
-- Author:	Desmond Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to delte a record on the editorial list table 
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialList_Delete]
	@ID int
As
Begin
	DELETE     
	FROM         dt_EditorialList
	WHERE ID = @ID
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_SelectByProgram]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	[mms_dt_Genre_SelectByProgram]
-- Author:	Sameer Kulkarni - DStv Online
-- alter date:	28/10/2010 16:56:57

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Genre_SelectByProgram]
@ProgramID int
As
Begin
SELECT     gv.GenreName, gv.Id
FROM         lt_Program_Genre AS pg INNER JOIN
                      dt_Program AS p ON pg.ProgramID = p.ID INNER JOIN
                      DStvProgram_Genre_view AS gv ON pg.GenreID = gv.ID
WHERE     (pg.ProgramID = @ProgramID)
End
--exec [mms_dt_Genre_SelectByProgram] 326
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_Program_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Genre_Program_Insert]--[mms_dt_Genre_Program_Insert] 286, 124, 'Movies', 'Comedy'
	@ProgramID int,
	@GenreID int,
	@GenreName nvarchar(50),
	@SubGenre nvarchar(50)
As
Begin
	declare @IdOfSubGenre int
	
	set @IdOfSubGenre = (select top 1 STH_ID from dbo.DStvProgram_SubGenre_View where STH_DESCRIPTIONS = @SubGenre)

	declare @ThemeID int = (select top 1 THE_ID from dbo.DStv_Genre_View where THE_DESCRIPTION = @GenreName)

	Insert Into lt_Program_Genre
		([GenreID],[ProgramID], [GenreName], [SubGenreName], [SubGenreID], [ThemeID], CreatedDate)
	Values
		(@GenreID,@ProgramID, @GenreName, @SubGenre, @IdOfSubGenre, @ThemeId, GETDATE())

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Genre_Program_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Genre_Program_Delete]
	@ProgramID int

As
Begin

	BEGIN 
		DELETE from lt_Program_Genre where ProgramID = @ProgramID
	END
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for inserting values to dt_Program table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Program_Insert]
	@ProgramName varchar(150) = '',
	@ProgramTypeID int = 1,
	@Abstract varchar(500) = '',
	@ImageURL varchar(500) = '',
	@BillboardURL varchar(500) = '',
	@GenreID smallint = 0,
	--@SubGenreID smallint,
	@GeoGroupID int,
	@WebsiteURL varchar(500),
	@Keywords varchar(1000),
	@StartDate datetime,
	@EndDate datetime
As
Begin

	Insert Into dt_Program
		([ProgramName],[ProgramTypeID],[Abstract],[ImageURL],[BillboardURL],[GenreID],[GeoGroupID],[WebsiteURL],[Keywords], [StartDate], [EndDate])
	Values
		(@ProgramName,@ProgramTypeID,@Abstract,@ImageURL, @BillboardURL, @GenreID, @GeoGroupID, @WebsiteURL, @Keywords, @StartDate, @EndDate)

	Declare @ReferenceID int
	SET @ReferenceID = @@IDENTITY


	EXEC mms_lt_Program_Tag_insert @@IDENTITY, @Keywords

	
	Return @ReferenceID

End


--ALTER Procedure [dbo].[mms_dt_Program_Insert]
--	@ProgramName varchar(150),
--	@ProgramTypeID int,
--	@Abstract varchar(500),
--	@ImageURL varchar(500),
--	@BillboardURL varchar(500),
--	@GenreID smallint,
--	@SubGenreID smallint,
--	@GeoGroupID int,
--	@WebsiteURL varchar(500),
--	@Keywords varchar(1000)
--As
--Begin

--	IF (@SubGenreID = 0 or @SubGenreID = NULL or (@SubGenreID =''))
--	BEGIN
--		SET @SubGenreID = NULL
--	END

--	Insert Into dt_Program
--		([ProgramName],[ProgramTypeID],[Abstract],[ImageURL],[BillboardURL],[GenreID],[SubGenreID],[GeoGroupID],[WebsiteURL],[Keywords])
--	Values
--		(@ProgramName,@ProgramTypeID,@Abstract,@ImageURL, @BillboardURL, @GenreID, @SubGenreID, @GeoGroupID, @WebsiteURL, @Keywords)

--	Declare @ReferenceID int
--	SET @ReferenceID = @@IDENTITY


--	EXEC mms_lt_Program_Tag_insert @@IDENTITY, @Keywords

	
--	Return @ReferenceID

--End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Program table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Program_DeleteRow]
	@ID int
As
Begin
	Update dt_Program
	SET              StatusID = 4
	Where
		[ID] = @ID
	EXEC mms_lt_Program_Tag_insert @ID, ''
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Channel_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Program_Channel_Insert]
	@ProgramID int,
	@ChannelID int
As
Begin
	Insert Into lt_Channel_Program
		([ChannelID],[ProgramID])
	Values
		(@ChannelID,@ProgramID)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Channel_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Program_Channel_Delete]
	@ProgramID int

As
Begin

	BEGIN 
		DELETE from lt_Channel_Program where ProgramID = @ProgramID
	END
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Product_SelectAllByProgram]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	[mms_dt_Channel_SelectAllByProgram]
-- Author:	Desigan Royan - DStv Online
-- alter date:	12/04/2010 12:13:54 PM

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Product_SelectAllByProgram]
@ProgramID int
As
Begin
SELECT     pro.ProductName, pro.Id
FROM         dt_Program AS p INNER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID INNER JOIN
                      dt_Product AS pro ON pp.ProductID = pro.ID
WHERE     (pp.ProgramID = @ProgramID)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Product_Program_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Product_Program_Insert]
	@ProgramID int,
	@ProductID int
As
Begin
	Insert Into lt_Product_Program
		([ProductID],[ProgramID])
	Values
		(@ProductID,@ProgramID)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Product_Program_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Product_Program_Delete]
	@ProgramID int

As
Begin

	BEGIN 
		DELETE from lt_Product_Program where ProgramID = @ProgramID
	END
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Person_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Person_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:13:54 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Person table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Person_SelectAll]
As
Begin
SELECT		p.ID
			, p.FullName
			, (select top 1 ProfessionID from lt_Person_Profession inner join dt_Profession on lt_Person_Profession.ProfessionID = dt_Profession.ID WHERE lt_Person_Profession.PersonID = p.ID ORDER BY dt_Profession.[Rank]) AS ProfessionID
			, p.CreatedDate
			, (select top 1 ProfessionName from dt_Profession inner join lt_Person_Profession on lt_Person_Profession.ProfessionID = dt_Profession.ID WHERE lt_Person_Profession.PersonID = p.ID ORDER BY dt_Profession.[Rank]) AS ProfessionName
			, p.Bio
			, p.Gender
			, p.BirthDate
			, p.BirthPlace
			, p.ImageURL
			, p.AwardsText
FROM         dt_Person AS p WITH(NOLOCK) 
WHERE     (p.StatusID = 1) OR
                      (p.StatusID = NULL)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Person_SelectList]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Person_SelectList
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:13:54 PM
-- Description:	This stored procedure is intended for selecting a list from dt_Person table
-- Altered: Date - 04/10/2010, By: Sameer Kulkarni
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Person_SelectList]
As
Begin
--SELECT		p.ID
--			, p.FullName
--			, (select top 1 ProfessionID from lt_Person_Profession left join dt_Profession on lt_Person_Profession.ProfessionID = dt_Profession.ID WHERE 
--			  lt_Person_Profession.PersonID = p.ID ORDER BY dt_Profession.[Rank]) AS ProfessionID
--			--, p.CreatedDated t
--			, (select top 1 ProfessionName from dt_Profession inner join lt_Person_Profession on lt_Person_Profession.ProfessionID = dt_Profession.ID WHERE 
--			   lt_Person_Profession.PersonID = p.ID ORDER BY dt_Profession.[Rank]) AS ProfessionName
--			, (p.FullName + ' - ' + (select top 1 ProfessionName from dt_Profession inner join lt_Person_Profession on lt_Person_Profession.ProfessionID = dt_Profession.ID WHERE 
--			   lt_Person_Profession.PersonID = p.ID ORDER BY dt_Profession.[Rank])) AS PersonProfession
			
--FROM         dt_Person AS p WITH(NOLOCK) 
--WHERE     ((p.StatusID = 1) OR
--                      (p.StatusID = NULL))
--              --and        p.fullname like '%clint%'
--order by ProfessionName

SELECT DISTINCT p.FullName AS Expr1, pro.ProfessionName, p.FullName + ' - ' + pro.ProfessionName as PersonProfession, pp.Table_ID as ID
FROM dt_Person AS p INNER JOIN
     lt_Person_Profession AS pp ON p.ID = pp.PersonID INNER JOIN
     dt_Profession AS pro ON pp.ProfessionID = pro.ID
WHERE ((p.StatusID = 1) OR
      (p.StatusID = NULL))
ORDER BY p.FullName


--SELECT	A.Table_ID AS PersonProfessionID
--		, a.PersonID
--		, B.FullName AS FullName
--		, a.ProfessionID
--		, C.ProfessionName AS ProfessionName
--		,  B.FullName + ' - ' + C.ProfessionName AS PersonProfession
--	FROM dt_Person B
--	INNER JOIN lt_Person_Profession A  ON A.PersonID = B.ID
--	INNER JOIN dt_Profession C ON A.ProfessionID  = C.ID
--	order by b.fullname
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_SelectListByWebsiteID]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Program_SelectListByWebsiteID]
@WebsiteID int,
@SortBy int = 1
As
Begin
if(@SortBy = 1)
BEGIN
SELECT     dt_Program.ID, dt_Program.ProgramName, dt_Program.CreatedDate
FROM         dt_Program INNER JOIN
                      lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
                      lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
                      dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
WHERE     (dt_Website.ID = @WebsiteID) AND (dt_Program.StatusID = 1) OR
                      (dt_Program.StatusID = NULL)
                      ORDER BY ProgramName ASC
END
ELSE IF(@SortBy = 2)
BEGIN
SELECT     dt_Program.ID, dt_Program.ProgramName, dt_Program.CreatedDate
FROM         dt_Program INNER JOIN
                      lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
                      lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
                      dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
WHERE     (dt_Website.ID = @WebsiteID) AND (dt_Program.StatusID = 1) OR
                      (dt_Program.StatusID = NULL)
                      ORDER BY dt_Program.CreatedDate DESC
END                    
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Program_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Program table
-- ==========================================================================================

CREATE Procedure [dbo].[mms_dt_Program_SelectAll] --[mms_dt_Program_SelectAll] 0,0,0
	@ChannelID int = 0,
	@ProductID	int = 0,
	@ProgramID	int = 0
AS
BEGIN --[mms_dt_Program_SelectAll] 0,0,119
	SELECT DISTINCT 
                      p.ID, p.ProgramName, p.ProgramTypeID, p.Abstract, p.ImageURL, p.BillboardURL, p.SubGenreID, p.WebsiteURL, p.GeoGroupID, p.CreatedDate, p.StartDate, 
                      p.EndDate, pg.GenreName, p.Keywords, pp.ProductID, pg.ProgramID, pg.GenreID
FROM         dt_Program AS p LEFT OUTER JOIN
                      lt_Program_Genre AS g ON p.GenreID = g.ThemeID LEFT OUTER JOIN
                      lt_Channel_Program AS cp ON p.ID = cp.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre AS pg ON p.ID = pg.ProgramID
	WHERE     (p.StatusID = 1 OR p.StatusID = NULL)
	AND		(cp.ChannelID = @ChannelID OR @ChannelID = 0)
	AND		(pp.ProductID = @ProductID OR @ProductID = 0)
	ORDER BY p.ProgramName ASC
END


--ALTER Procedure [dbo].[mms_dt_Program_SelectAll] --[mms_dt_Program_SelectAll] 0,0,0
--	@ChannelID int = 0,
--	@ProductID	int = 0,
--	@ProgramID	int = 0
--AS
--BEGIN
--	SELECT     p.ID, p.ProgramName, p.ProgramTypeID, p.Abstract, p.ImageURL, p.BillboardURL, p.GenreID, 
--	           p.SubGenreID, p.WebsiteURL, p.GeoGroupID, p.CreatedDate, 
--                      g.GenreName, p.Keywords, pp.ProductID, pg.ProgramID, pg.GenreID
--	FROM         dt_Program AS p LEFT OUTER JOIN
--                      dt_Genre AS g ON p.GenreID = g.ID LEFT OUTER JOIN
--                      lt_Channel_Program AS cp ON p.ID = cp.ProgramID LEFT OUTER JOIN
--                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
--                      lt_Program_Genre as pg on p.ID = pg.ProgramID
--	WHERE     (p.StatusID = 1 OR p.StatusID = NULL)
--	AND		(cp.ChannelID = @ChannelID OR @ChannelID = 0)
--	AND		(pp.ProductID = @ProductID OR @ProductID = 0)
--	ORDER BY p.ProgramName ASC
--END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Video_Update]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:22:43 PM
-- Description:	This stored procedure is intended for updating dt_Video table
-- ==========================================================================================
-- ==============Modified=======================
-- Author:		Desmond Nzuza
-- Date Modified: 2010-July-14
-- Description: Added a method to remove 
--				extra "/CMS" charaters on 
--				v.ImageURL, v.BillboardImageURL
-- =============================================
CREATE Procedure [dbo].[mms_dt_Video_Update]
	@ID int,
	@VideoTitle varchar(200),
	@Abstract varchar(500),
	@ImageURL varchar(500),
	@EPGID varchar(255),
	@ProgramID int,
	@VideoProgramTypeID smallint,
	@AgeRestrictionID tinyint,
	@AirDate datetime = NULL,
	@Runtime varchar(50),
	@Season int,
	@Episode int,
	@StartDate varchar(10),
	@StartTime varchar(8),
	@EndDate varchar(10),
	@EndTime varchar(8),
	@Keywords varchar(1000),
	@BillboardImageURL varchar(500),
	@ExpiryDate datetime = NULL,
	@RuntimeInMinutes numeric(3,0) = NULL,
	@RentalPeriodInHours numeric(3,0) = NULL,
    @Price numeric(10,2) = 0, 
    @Geolocation numeric(3,0) = NULL,
    @CastCrewID int = null,
    @IBSProductID varchar(50),
	@AssetId varchar(50),
	@GeoGroupID int = 0
As
Begin
	set @Price = ISNULL(@Price,0)
	Update dt_Video
	Set
		[VideoTitle] = @VideoTitle,
		[Abstract] = @Abstract,
		[ImageURL] = REPLACE(@ImageURL,'/cms/',''),--@ImageURL,
		[EPGID] = @EPGID,
		[ProgramID] = @ProgramID,
		[VideoProgramTypeID] = @VideoProgramTypeID,
		[AgeRestrictionID] = @AgeRestrictionID,
		[AirDate] = @AirDate,
		[Runtime] = @Runtime,
		[Season] = @Season,
		[Episode] = @Episode,
		[StartDate] = Convert(datetime,@StartDate + ' ' + @StartTime, 20),
		[EndDate] = Convert(datetime,@EndDate + ' ' + @EndTime, 20),
		[Keywords] = @Keywords,
		[BillboardImageURL] = REPLACE(@BillboardImageURL,'/cms/',''),--@BillboardImageURL,
		[ExpiryDate] = @ExpiryDate,
		[RuntimeInMinutes] = @RuntimeInMinutes,
		[RentalPeriodInHours] = @RentalPeriodInHours,
		[Price] =  @Price,--dt_Video.Price,
		[Geolocation] = @Geolocation,--dt_Video.Geolocation
		[CastCrewID] = @CastCrewID,
		[IBSProductID] = @IBSProductID,
		[AssetId] = @AssetId,
		[GeoGroupID] = @GeoGroupID
	Where		
		[ID] = @ID
		
	EXEC mms_lt_Video_Tag_insert @ID, @Keywords


End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Video_SelectByProgram]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Video_SelectByProgram]
@ProgramID int
AS
BEGIN
SELECT     dt_Video.ID, dt_Video.VideoTitle
FROM dt_Video
WHERE     (dt_Video.ProgramID = @ProgramID) AND (dt_Video.StatusID = 1) OR
                      (dt_Video.StatusID = NULL)
ORDER BY dt_Video.VideoTitle                  
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Video_SelectByChannel]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_dt_Video_SelectByChannel]
@ChannelID int,
@SortBy int = 1
AS
BEGIN
IF(@SortBy = 1)
BEGIN
SELECT     dt_Video.ID, dt_Video.VideoTitle
FROM         dt_Video INNER JOIN
                      dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
                      lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID
WHERE     (lt_Channel_Program.ChannelID = @ChannelID) AND (dt_Video.StatusID = 1) OR
                      (dt_Video.StatusID = NULL)
ORDER BY dt_Video.VideoTitle                      
END
ELSE IF(@SortBy = 2)
BEGIN
SELECT     dt_Video.ID, dt_Video.VideoTitle
FROM         dt_Video INNER JOIN
                      dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
                      lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID
WHERE     (lt_Channel_Program.ChannelID = @ChannelID) AND (dt_Video.StatusID = 1) OR
                      (dt_Video.StatusID = NULL)
ORDER BY dt_Video.CreatedDate DESC                      
END
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoMeta_SelectAll]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoMeta_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 1:01:49 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoMeta table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoMeta_SelectAll]
As
Begin
SELECT     dt_VideoMeta.ID, dt_VideoMeta.VideoID, dt_VideoMeta.LanguageID, dt_VideoMeta.SecondaryLanguageID, dt_VideoMeta.SubtitleLanguageID, 
                      dt_VideoMeta.YearOfRelease, dt_VideoMeta.AwardTextLong, dt_VideoMeta.Synopsis, dt_VideoMeta.AwardTextShort, dt_VideoMeta.CountryID, 
                      dt_VideoMeta.CreatedDate, dt_Video.VideoTitle AS VideoName, dt_Video.ProgramID
FROM         dt_VideoMeta INNER JOIN
                      dt_Video ON dt_VideoMeta.VideoID = dt_Video.ID
WHERE     (dt_VideoMeta.StatusID = 1) OR
                      (dt_VideoMeta.StatusID = NULL)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jacques du Preez
-- alter date: 2009/10/15
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[mms_lt_Person_Profession_Insert]
	-- Add the parameters for the stored procedure here
	@PersonID int,
	@ProfessionID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		INSERT INTO lt_Person_Profession (PersonID, ProfessionID)
		VALUES (@PersonID, @ProfessionID)
END
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jacques du Preez
-- alter date: 2009/10/15
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[mms_lt_Person_Profession_Delete]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		DELETE FROM lt_Person_Profession WHERE PersonID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoLog_SelectAll_MnetSales]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_VideoLog_Select
-- Author:	Arthur - DStv Online
-- alter date:	2010-02-08 12:13:00 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoLog table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoLog_SelectAll_MnetSales]

	
AS
BEGIN
	select [dt_VideoLog].[ID]
      ,[dt_VideoLog].[VideoID]
      ,[dt_VideoLog].[WebsiteID]
      ,[dt_VideoLog].[ViewCount]
      ,[dt_VideoLog].[LastViewed]
      ,CONVERT(VARCHAR(10), [dt_VideoLog].[CreatedDate], 111) AS [CreatedDate] 
      ,[dt_VideoLog].[StatusID]
      ,dt_Video.VideoTitle AS VideoName
      from [DStvMedia].[dbo].[dt_VideoLog]  INNER JOIN
                      dt_Video ON [dt_VideoLog].VideoID = dt_Video.ID
      where [WebsiteID] = 23
      -- and [dt_VideoLog].[CreatedDate] > '2010/01/04' and [dt_VideoLog].[CreatedDate] < '2010/02/05'
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Video_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:22:43 PM
-- Description:	This stored procedure is intended for inserting values to dt_Video table
-- ==========================================================================================
-- ==============Modified=======================
-- Author:		Desmond Nzuza
-- Date Modified: 2010-July-14
-- Description: Added a method to remove 
--				extra "/CMS" charaters on 
--				v.ImageURL, v.BillboardImageURL
-- =============================================
CREATE Procedure [dbo].[mms_dt_Video_Insert]
	@VideoTitle varchar(200)='',
	@Abstract varchar(500)='',
	@ImageURL varchar(500)='',
	@EPGID varchar(255) = NULL,
	@ProgramID int,
	@VideoProgramTypeID smallint = 1,
	@AgeRestrictionID tinyint = 1,
	@AirDate datetime = NULL,
	@Runtime varchar(50) = NULL,
	@Season int = NULL,
	@Episode int = NULL,
	@StartDate datetime,
	@StartTime datetime,
	@EndDate varchar(10),
	@EndTime varchar(8),
	@Keywords varchar(1000) = NULL,
	@BillboardImageURL varchar(500) = '',
	@ExpiryDate datetime = NULL,
	@RuntimeInMinutes numeric(3,0) = NULL,
	@RentalPeriodInHours numeric(3,0) = NULL,
	@Price numeric(10,2) = 0,
	@Geolocation numeric(3,0) = NULL,
	@CastCrewID int = null,
	@IBSProductID varchar(50),
	@AssetId varchar(50),
	@GeoGroupID int = 0
As
Begin
	Insert Into dt_Video
		([VideoTitle],[Abstract],[ImageURL],[EPGID],[ProgramID],[VideoProgramTypeID],[AgeRestrictionID],[AirDate],[Runtime],[Season],[Episode],[StartDate],[EndDate],[Keywords],[BillboardImageURL],[ExpiryDate], [RuntimeInMinutes], [RentalPeriodInHours], [Price], [Geolocation],[CastCrewID], [IBSProductID], [AssetId], [GeoGroupID])
	Values
		(@VideoTitle,@Abstract,REPLACE(@ImageURL,'/cms/',''),@EPGID,@ProgramID,@VideoProgramTypeID,@AgeRestrictionID,@AirDate,@Runtime,@Season,@Episode,Convert(datetime,@StartDate + ' ' + @StartTime, 20),Convert(datetime,@EndDate + ' ' + @EndTime, 20),@Keywords,REPLACE(@BillboardImageURL,'/cms/',''), @ExpiryDate, @RuntimeInMinutes, @RentalPeriodInHours, @Price, @Geolocation,@CastCrewID, @IBSProductID, @AssetId, @GeoGroupID)

	Declare @ReferenceID int
	SET @ReferenceID = @@IDENTITY
	

	EXEC mms_lt_Video_Tag_insert @@IDENTITY, @Keywords

	
	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Video_DeleteRow]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:22:43 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_Video table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Video_DeleteRow]
	@ID int
As
Begin
	update dt_Video
	SET              StatusID = 4
	Where
		[ID] = @ID
	EXEC mms_lt_Video_Tag_insert @@IDENTITY, ''
End
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jacques du Preez
-- alter date: 2009/10/15
-- Description:	Gets the professions for the specified person
-- =============================================
CREATE PROCEDURE [dbo].[mms_lt_Person_Profession_Select]
	-- Add the parameters for the stored procedure here
	@PersonID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT ProfessionID
		FROM lt_Person_Profession WITH(NOLOCK) 
		WHERE PersonID = @PersonID
END
GO
/****** Object:  StoredProcedure [dbo].[TestPaging]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[TestPaging]
@PageSize int,
@PageNumber int,
@OrderBy varchar(50)
as 
Begin
select q1.*
from
(
SELECT ROW_NUMBER() OVER (ORDER BY @OrderBy) as [RowNumber]
		,[ID]
      ,[VideoTitle]
      ,[Abstract]
      ,[ImageURL]
      ,[BillboardImageURL]
      ,[EPGID]
      ,[ProgramID]
      ,[VideoProgramTypeID]
      ,[AgeRestrictionID]
      ,[AirDate]
      ,[Runtime]
      ,[Season]
      ,[Episode]
      ,[StartDate]
      ,[EndDate]
      ,[Keywords]
      ,[CreatedDate]
      ,[StatusID]
      ,[OldId]
      ,[OldSource]
      ,[ExpiryDate]
      ,[RuntimeInMinutes]
      ,[RentalPeriodInHours]
      ,[Price]
      ,[Geolocation]
      ,[GroupID]
      ,[CastCrewID]
  FROM [dbo].[dt_Video] 
  ) as q1 
  where q1.RowNumber > @PageNumber * @PageSize
  and q1.RowNumber < (@PageNumber * @PageSize) + @PageSize
  END
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_WebSite_Insert]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Product_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Product table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_WebSite_Insert] -- mms_Ondemand_Product_WebSite_GetByProductID 12
	@productId int,
	@websiteId int
	
AS
BEGIN

	INSERT INTO [dbo].[lt_Website_Product]
           (
				ProductID, WebsiteID	
			)
     VALUES
           (
				@productId, @websiteId			
			)

Select @@IDENTITY as ID
                      
                      
End
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_WebSite_GetByProductID]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Product_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Product table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_WebSite_GetByProductID] -- mms_Ondemand_Product_WebSite_GetByProductID '', 0, -1
	@productId int
AS
BEGIN
	SELECT DISTINCT 
                      lt_Website_Product.ProductID, lt_Website_Product.WebsiteID, dt_Website.ID, dt_Website.WebsiteName, dt_Website.WebsiteURL, dt_Website.AuthenticationKey, 
                      dt_Website.VideoAccess, dt_Website.StatusID
FROM         lt_Website_Product RIGHT OUTER JOIN
                      dt_Website ON lt_Website_Product.WebsiteID = dt_Website.ID
                      
                      WHERE
                      (lt_Website_Product.ProductID = @productId)
                      
                      
End
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_WebSite_delete]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Product_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Product table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_WebSite_delete] -- mms_Ondemand_Product_WebSite_GetByProductID 12
	@productId int
	
AS
BEGIN

DELETE FROM 	[dbo].[lt_Website_Product]
           WHERE				[ProductID] = @productId
                      
                      
End
GO
/****** Object:  View [dbo].[Program_Video_Count]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Program_Video_Count]
AS
SELECT     TOP (100) PERCENT v.ProgramID, v.ID AS VideoId, SUM(vl.ViewCount) AS ViewCount
FROM         dbo.dt_Video AS v INNER JOIN
                      dbo.dt_VideoLog AS vl ON v.ID = vl.VideoID
GROUP BY v.ID, v.ProgramID, vl.VideoID
ORDER BY vl.VideoID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 141
               Bottom = 121
               Right = 327
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "vl"
            Begin Extent = 
               Top = 5
               Left = 391
               Bottom = 120
               Right = 547
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Program_Video_Count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Program_Video_Count'
GO
/****** Object:  StoredProcedure [dbo].[mms_Ondemand_Product_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Product_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Product table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_Ondemand_Product_Select] -- mms_Ondemand_Product_Select '', 0, -1
	@Search	nvarchar(250),
	@WebsiteId int,
	@StatusID int
AS
BEGIN
	SELECT DISTINCT 
                      dt_Product.ID, dt_Product.ProductName, dt_Product.ProductDescription, dt_Product.ProductIcon, dt_Product.MediumLogo, dt_Product.LargeLogo, 
                      dt_Product.CurrencyIdentifier, dt_Product.CreatedDate, dt_Product.StatusID
FROM         dt_Product LEFT OUTER JOIN
                      lt_Website_Product ON dt_Product.ID = lt_Website_Product.ProductID
                      
                      WHERE
                      (lt_Website_Product.WebsiteID = @WebsiteId OR @WebsiteId = 0)
                      AND
                      (dt_Product.ProductName like '%'+ @Search +'%' OR dt_Product.ProductDescription like '%'+ @Search +'%' OR @Search like '')
                      AND
                      (dt_Product.StatusID = @StatusID OR @StatusID = -1)
                      
                      
End
GO
/****** Object:  StoredProcedure [dbo].[ws_bo_GetCatalogueItems]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chris de Kock
-- alter date: 28/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_bo_GetCatalogueItems] 
	-- Add the parameters for the stored procedure here
	--@CatalogueItemIds varchar(8000)	 = '',
	
	@PageNumber int = 0,
	@PageSize int = 20,
	@OrderBy varchar(100) --= 'ID'
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @startID int = 2000;
set @startID = (@PageNumber - 1) * @PageSize;


With VideoEntities As 
(
    Select ROW_NUMBER() Over (Order By
         CASE WHEN @orderBy = 'ID' THEN ID
              WHEN @orderBy = 'airdate' THEN airdate
              ELSE ID
              END
    ) As Row,
       *
    From dbo.dt_Video
)
Select p.Abstract, v.AirDate, v.ID CatalogueItemId, v.Runtime DurationDescription, v.Episode EpisodeNumber, v.VideoTitle,
v.ExpiryDate, v.Keywords + ', ' + p.Keywords Keywords, v.ImageURL MediaImageUri, '' ProductTypeId, p.ID ProgramId, p.ProgramName ProgramTitle, 29 RentalAmount, 
36 RentalPeriodInHours, null RunTimeInMinutes, v.Season SeasonNumber, v.StartDate StartDate, v.Abstract Synopsis, v.VideoProgramTypeID VideoProgramTypeId
From VideoEntities v
left join dbo.dt_Program p on p.ID = v.ProgramID
Inner Join dbo.dt_Video t0 on v.ID = t0.id
Where v.Row Between @startID + 1 AND (@startID + @PageSize)
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_EPGGenre_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	ws_dt_EPGGenre_Select
-- Author:	Chris de Kock - DStv Online
-- alter date:	13/10/2010 9:50:00 AM
-- Description:	This stored procedure is intended for selecting single/all rows from EPG Genres 
-- ==========================================================================================
CREATE Procedure [dbo].[ws_dt_EPGGenre_Select]
	@AuthenticationKey varchar(255),
	@ProductID	int = 0,
	@ParentID int = null,
	@VideoProgramTypeID varchar(max) = '0'
AS
BEGIN

Declare  @SiteId int

SELECT	@SiteId = ID
FROM	dt_Website  WITH(NOLOCK)
Where AuthenticationKey = @AuthenticationKey
		
IF(@ParentID is null)
	BEGIN
		SELECT DISTINCT 
             lt.ThemeID AS ID, NULL AS ParentID, RTRIM(lt.GenreName) AS Description, COUNT(DISTINCT v.ID) AS VideoCount, COUNT(DISTINCT lt.SubGenreID) 
                      AS ChildCount
		FROM         lt_Program_Genre AS lt WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON v.ProgramID = lt.ProgramID INNER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON lt.ProgramID = pp.ProgramID INNER JOIN
                      lt_Website_Product AS wp WITH (NOLOCK) ON pp.ProductID = wp.ProductID INNER JOIN
                      dt_Program AS p WITH (NOLOCK) ON v.ProgramID = p.ID 
                      INNER JOIN lt_Channel_Program AS cp ON p.ID = cp.ProgramID
		WHERE     (pp.ProductID = @ProductID)
		AND      (wp.WebsiteID = @SiteId)
		AND (GETDATE() BETWEEN v.StartDate AND v.ExpiryDate)
		AND (GETDATE() BETWEEN p.StartDate AND p.EndDate)
		AND (v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramTypeID)) OR (0 in (select * from dbo.Array(@VideoProgramTypeID))))
		AND v.StatusID = 1
		GROUP BY lt.ThemeId, lt.GenreName, wp.WebsiteID
	END
ELSE
	BEGIN
		SELECT DISTINCT lt.SubGenreID AS ID, lt.ThemeID AS ParentID, lt.SubGenreName AS Description, COUNT(DISTINCT v.ID) AS videoCount, 0 AS ChildCount
		FROM         lt_Program_Genre AS lt WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON v.ProgramID = lt.ProgramID INNER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON lt.ProgramID = pp.ProgramID INNER JOIN
                      lt_Website_Product AS wp WITH (NOLOCK) ON pp.ProductID = wp.ProductID INNER JOIN
                      dt_Program AS p WITH (NOLOCK) ON v.ProgramID = p.ID 
                      INNER JOIN lt_Channel_Program AS cp ON p.ID = cp.ProgramID
		WHERE pp.ProductID = @ProductID 
		AND      (wp.WebsiteID = @SiteID)
		AND		(lt.ThemeId = @ParentID)
		AND (GETDATE() BETWEEN v.StartDate AND v.ExpiryDate)
		AND (GETDATE() BETWEEN p.StartDate AND p.EndDate)
		AND (v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramTypeID)) OR (0 in (select * from dbo.Array(@VideoProgramTypeID))))
		AND v.StatusID = 1
		GROUP BY lt.ThemeId, lt.SubGenreID, lt.SubGenreName
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_EditorialList_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 09/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_EditorialList_Select] 
	-- Add the parameters for the stored procedure here
	@EditorialListID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     ID, Title, DisplayTitle
	FROM         dt_EditorialList
	WHERE     (ID = @EditorialListID OR @EditorialListID = 0)
	AND			(StatusID = 1)
	ORDER BY CreatedDate
	
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Product_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ws_dt_Product_Select]
                @AuthenticationKey varchar(255),
                @ProductID	int = 0
AS
BEGIN
  SELECT p.[Id]
      , rtrim(p.[ProductName]) as [Description]
      , null as ParentID
      , COUNT(distinct lt.Table_ID) as ChildCount
      , count(v.id) as VideoCount 
  FROM [dbo].[dt_Product] AS p with(nolock) 
	Inner Join lt_Product_Program as lt with(nolock) On lt.ProductID = p.Id 
	inner join lt_Website_Product wp on wp.ProductID = p.ID
	inner join dt_Website w on w.ID = wp.WebsiteID
	inner join dt_Video v on v.ProgramID = lt.ProgramID
	where w.AuthenticationKey = @AuthenticationKey
	and GETDATE() between v.StartDate and v.ExpiryDate and v.StatusID = 1
  group by 
       p.[Id]
      ,p.[ProductName]
     
END
GO
/****** Object:  StoredProcedure [dbo].[ws_bo_GetCatalogueItem]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chris de Kock
-- alter date: 28/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_bo_GetCatalogueItem] 
	-- Add the parameters for the stored procedure here
	@catalogueItemId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select p.Abstract, v.AirDate, v.ID CatalogueItemId, v.Runtime DurationDescription, v.Episode EpisodeNumber, v.VideoTitle,
v.ExpiryDate, v.Keywords + ', ' + p.Keywords Keywords, v.ImageURL MediaImageUri, '' ProductTypeId, p.ID ProgramId, p.ProgramName ProgramTitle, 29 RentalAmount, 
36 RentalPeriodInHours, null RunTimeInMinutes, v.Season SeasonNumber, v.StartDate StartDate, v.Abstract Synopsis, v.VideoProgramTypeID VideoProgramTypeId
from dbo.dt_Video v
left join dbo.dt_Program p on p.ID = v.ProgramID
where v.id = @catalogueItemId
order by ProgramTitle, Season, Episode
	
END
GO
/****** Object:  StoredProcedure [dbo].[ws_RSSProgram_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jacques du Preez
-- alter date: 2009-10-07
-- Description:	Get the program RSS feed details
-- =============================================
CREATE PROCEDURE [dbo].[ws_RSSProgram_Select] 
	-- Add the parameters for the stored procedure here
	@ProgramID int = 0,
	@VideoID int = 0,
	@VideoProgramTypeID int = 0,
	@VideoFileTypeID int,
	@AuthenticationKey varchar(255),
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET @IsAuthenticated = 0
	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1


		SELECT	TOP 50 v.ID,
			v.VideoTitle,
			v.Abstract,
			v.StartDate,
			'http://mms.dstv.com' + v.ImageURL AS ImageURL,
			vp.ServerPath + '' + vf.[FileName] AS VideoPath
		FROM	dt_Video AS v WITH(NOLOCK) INNER JOIN
			dt_VideoFile AS vf WITH(NOLOCK) ON v.ID = vf.VideoID INNER JOIN
			dt_VideoFilePath AS vp WITH(NOLOCK) ON vf.VideoFilePathID = vp.ID
		WHERE	((v.ProgramID = @ProgramID) OR (v.ID = @VideoID))
			AND ((v.VideoProgramTypeID = @VideoProgramTypeID) OR (@VideoProgramTypeID = 0))
			AND vf.VideoFileTypeID = @VideoFileTypeID
			AND getdate() BETWEEN v.StartDate AND v.EndDate
			AND v.StatusID = 1
		ORDER BY StartDate DESC
	END
		
END
GO
/****** Object:  StoredProcedure [dbo].[ws_PersonDetails_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan
-- alter date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_PersonDetails_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ChannelId int = 0,
	@PersonTypeId int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @IsAuthenticated = 0

	DECLARE @SiteId int
	SELECT @SiteId = ID
					FROM         dt_Website
					WHERE     (StatusID = 1) 
					AND (AuthenticationKey = @AuthenticationKey)
					AND (VideoAccess = 1)
				

	IF (@SiteId > 0)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		SELECT   DISTINCT  pe.ID, pe.FullName
		FROM         lt_Website_Channel AS w INNER JOIN
							  lt_Channel_Program AS c ON w.ChannelID = c.ChannelID INNER JOIN
							  lt_Person_Profession_Program AS lppp WITH(NOLOCK) ON lppp.ProgramID = c.ProgramID INNER JOIN
							  lt_Person_Profession AS lpp WITH(NOLOCK) ON lpp.Table_ID = lppp.PersonProfessionID INNER JOIN
							  dt_Person AS pe WITH(NOLOCK) ON pe.ID = lpp.PersonID
		WHERE     (w.WebsiteID = @SiteId) 
		AND (w.ChannelID = @ChannelId OR @ChannelId = 0) 
		AND (pe.ProfessionID = @PersonTypeId)
		ORDER BY FullName ASC
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_lt_Program_Person_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jacques du Preez
-- alter date: 2009-07-28
-- Description:	Gets the Related program info tied to the person specified
-- =============================================
CREATE PROCEDURE [dbo].[ws_lt_Program_Person_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255), 
	@PersonID int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END

	IF (@IsAuthenticated = 1)
	BEGIN
		SELECT	lppp.ProgramID
				,p.ProgramName
				,p.Abstract
				,p.ImageURL
				,p.BillboardURL
		FROM	lt_Person_Profession_Program AS lppp WITH(NOLOCK) INNER JOIN
				dt_Program AS p WITH(NOLOCK) ON lppp.ProgramID = p.ID and p.StatusID = 1 INNER JOIN
				lt_Person_Profession AS lpp WITH(NOLOCK) ON lpp.Table_ID = lppp.PersonProfessionID and p.StatusID = 1
		WHERE	lpp.PersonID = @PersonID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_lt_EditorialListItems_Select]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chris de Kock
-- alter date: 28/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_lt_EditorialListItems_Select] 
	-- Add the parameters for the stored procedure here
	@EditorialListTitle varchar(8000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT dt_EditorialList.ID, dt_EditorialList.Title, dt_EditorialList.DisplayTitle, lt_EditorialListItems.ItemID, lt_EditorialListItems.ItemRank
	FROM         dt_EditorialList INNER JOIN
						  lt_EditorialListItems ON lt_EditorialListItems.EditorialListID = dt_EditorialList.ID
	WHERE     (dt_EditorialList.Title = @EditorialListTitle) AND (dt_EditorialList.StatusID = 1)
	ORDER BY lt_EditorialListItems.ItemRank
		
END
GO
/****** Object:  StoredProcedure [dbo].[ws_lt_EditorialListItems]    Script Date: 04/29/2011 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 09/09/10
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_lt_EditorialListItems] 
	-- Add the parameters for the stored procedure here
	@EditorialListID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT    DISTINCT ItemID, ItemTypeId, ItemRank
	FROM         lt_EditorialListItems
	WHERE     (EditorialListID = @EditorialListID)
	ORDER BY ItemRank ASC--, CreatedDate DESC
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoWithMeta_Select_V2_bup]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoWithMeta_Select_V2_bup] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 0,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@PrimaryLanguageId int = 0,
	@SecondaryLanguageId int = 0,
	@SubtitleLanguageId int = 0,
	@CountryId	int = 0,
	@GenreId	int = 0,
	@KeyWords	varchar(100)='',
	@DirectorId  int = 0,
	@IsAuthenticated bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;

	Declare @DateStamp datetime
	Declare @Interval int

	SET @DateStamp = (SELECT TOP 1 CreatedDate FROM tt_Video_Meta_V2 ORDER BY CreatedDate DESC)
 
	SET @DateStamp = ISNULL(@DateStamp, getdate()-1)

	SET @Interval = 15

	--SELECT CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) - @Interval

	IF (CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) > @Interval)
	BEGIN
		
		TRUNCATE TABLE tt_Video_Meta_V2

		INSERT INTO tt_Video_Meta_V2
		SELECT     v.ID, ltrim(v.VideoTitle) AS VideoTitle, v.Keywords, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) 
                      THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) ELSE vfp.ServerPath END AS ServerPath, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) 
                      - (charindex('ondemand', vfp.serverpath) + 7)) + '/' + vf.[FileName]) ELSE vf.[FileName] END AS FileName, 
                      l3.LanguageName AS PrimaryLanguage, l2.LanguageName AS SecondaryLanguage, l1.LanguageName AS SubtitleLanguage, vm.YearOfRelease, 
                      vm.AwardTextLong, vm.AwardTextShort, vm.Synopsis, c.CountryName, g.GenreName, p.ID AS ProgramID, p.ProgramTypeID, vf.VideoFileTypeID, 
                      p.StatusID, v.StartDate, v.EndDate, vm.LanguageID, vm.SecondaryLanguageID, vm.SubtitleLanguageID, vm.CountryID, p.GenreID, p.SubGenreID, 
                      g2.GenreName AS SubGenreName, GETDATE() AS Expr1, v.AgeRestrictionID, ag.Abbreviation AS AgeRestrictionAbbreviation, ag.AgeRestrictionDescription, v.BillboardImageURL
		FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID RIGHT OUTER JOIN
                      dt_VideoMeta AS vm WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON vm.VideoID = v.ID INNER JOIN
                      dt_Program AS p WITH (NOLOCK) ON v.ProgramID = p.ID ON vf.VideoID = v.ID LEFT OUTER JOIN
                      dt_Genre AS g2 WITH (NOLOCK) ON p.SubGenreID = g2.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Country AS c WITH (NOLOCK) ON vm.CountryID = c.ID LEFT OUTER JOIN
                      dt_Language AS l3 WITH (NOLOCK) ON vm.LanguageID = l3.ID LEFT OUTER JOIN
                      dt_Language AS l2 WITH (NOLOCK) ON vm.SecondaryLanguageID = l2.ID LEFT OUTER JOIN
                      dt_Language AS l1 WITH (NOLOCK) ON vm.SubtitleLanguageID = l1.ID INNER JOIN
					  dt_AgeRestriction AS ag WITH (NOLOCK) ON  v.AgeRestrictionID = ag.ID

--		INSERT INTO tt_Video_Meta
--		SELECT     v.ID, v.VideoTitle, v.Keywords, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) 
--                      THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) ELSE vfp.ServerPath END AS ServerPath, 
--                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) 
--                      - (charindex('ondemand', vfp.serverpath) + 7)) + '/' + vf.[FileName]) ELSE vf.[FileName] END AS FileName, per.FullName AS Director, 
--                      l3.LanguageName AS PrimaryLanguage, l2.LanguageName AS SecondaryLanguage, l1.LanguageName AS SubtitleLanguage, vm.YearOfRelease, 
--                      vm.AwardTextLong, vm.AwardTextShort, vm.Synopsis, c.CountryName, g.GenreName, p.ID AS ProgramID, p.ProgramTypeID, vf.VideoFileTypeID, 
--                      p.StatusID, v.StartDate, v.EndDate, vm.LanguageID, vm.SecondaryLanguageID, vm.SubtitleLanguageID, vm.CountryID, p.GenreID, p.SubGenreID, 
--                      g2.GenreName AS SubGenreName, per.ID AS DirectorId, getdate()
--		FROM         dbo.dt_Program AS p WITH (NOLOCK) INNER JOIN
--                      dbo.dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
--                      dbo.dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
--                      dbo.dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID INNER JOIN
--                      dbo.dt_VideoMeta AS vm WITH (NOLOCK) ON v.ID = vm.VideoID ON p.ID = v.ProgramID LEFT OUTER JOIN
--                      dbo.dt_Genre AS g2 WITH (NOLOCK) ON p.SubGenreID = g2.ID LEFT OUTER JOIN
--                      dbo.lt_Program_Person AS pro WITH (NOLOCK) INNER JOIN
--                      dbo.dt_Person AS per WITH (NOLOCK) ON pro.PersonID = per.ID ON v.ProgramID = pro.ProgramID AND per.ProfessionID = 1 LEFT OUTER JOIN
--                      dbo.dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
--                      dbo.dt_Country AS c WITH (NOLOCK) ON vm.CountryID = c.ID LEFT OUTER JOIN
--                      dbo.dt_Language AS l3 WITH (NOLOCK) ON vm.LanguageID = l3.ID LEFT OUTER JOIN
--                      dbo.dt_Language AS l2  WITH (NOLOCK)ON vm.SecondaryLanguageID = l2.ID LEFT OUTER JOIN
--                      dbo.dt_Language AS l1 WITH (NOLOCK) ON vm.SubtitleLanguageID = l1.ID
	END


	SET @IsAuthenticated = 0
	DECLARE @SiteId int
	SELECT @SiteId = ID
					FROM         dt_Website
					WHERE     (StatusID = 1) 
					AND (AuthenticationKey = @AuthenticationKey)
					AND (VideoAccess = 1)
				

	IF (@SiteId > 0)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@SortyBy =1)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
						     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, NULL AS Director, v.PrimaryLanguage, 
							  v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, 
							  v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, NULL AS DirectorID, vr.Rating, v.ProgramId,
							 v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL
						FROM         tt_Video_Meta_V2 AS v WITH (NOLOCK) LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
											  lt_Program_Person AS lpp on v.ProgramID = lpp.ProgramID INNER JOIN
											  lt_Person_Profession AS pp ON lpp.PersonID = pp.PersonID AND pp.ProfessionID = 1 INNER JOIN
											  dt_Person AS per ON lpp.PersonID = per.ID
											  
						
						WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
						--AND			(CONTAINS(VideoTitle, @KeyWords) OR CONTAINS(keywords, @KeyWords) OR CONTAINS(Director, @KeyWords) OR @KeyWords = '')
						AND			(lpp.PersonId =  @DirectorId OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR per.Fullname LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
										
					)
					
					SELECT     Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, FileName, ServerPath, NULL AS Director, PrimaryLanguage, SecondaryLanguage, 
										  SubtitleLanguage, YearOfRelease, AwardTextLong, AwardTextShort, Synopsis, CountryName, GenreName, LanguageID, SecondaryLanguageID, 
										  SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription, BillboardImageURL, (SELECT     COUNT(Id) FROM          Records) AS TotalRows
					FROM         Records
					WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY LTRIM(VideoTitle)

		END

		IF (@SortyBy =2)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
							 v.ID AS Id
							, v.VideoTitle
							, v.Abstract
							, v.Runtime
							, v.AirDate
							, v.Season
							, v.Episode
							, v.ImageURL
							, v.ServerPath
							, v.[FileName]
							, v.StartDate
							, NULL AS Director
							, v.PrimaryLanguage
							, v.SecondaryLanguage
							, v.SubtitleLanguage
							, v.YearOfRelease
							, v.AwardTextLong
							, v.AwardTextShort
							, v.Synopsis
							, v.CountryName
							, v.GenreName
							, v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, NULL AS DirectorId, vr.Rating, v.ProgramId
							, v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription
							, v.BillboardImageURL
						FROM        tt_Video_Meta_V2 AS v WITH (NOLOCK) LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
											  lt_Program_Person AS lpp on v.ProgramID = lpp.ProgramID INNER JOIN
											  lt_Person_Profession AS pp ON lpp.PersonID = pp.PersonID AND pp.ProfessionID = 1 INNER JOIN
											  dt_Person AS per ON lpp.PersonID = per.ID
						WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0  OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
						AND			(lpp.PersonID =  @DirectorId OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR per.Fullname LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					)
					
					SELECT		Id
								, VideoTitle
								, Abstract
								, Runtime
								, AirDate
								, Season
								, Episode
								, ImageURL
								, [FileName]
								, ServerPath
								, Director
								, PrimaryLanguage
								, SecondaryLanguage
								, SubtitleLanguage
								, YearOfRelease
								, AwardTextLong
								, AwardTextShort
								, Synopsis
								, CountryName
								, GenreName
								, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId 
								, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
								, BillboardImageURL
								,(SELECT COUNT(Id) FROM Records) AS TotalRows, 1 as [Rating]
					FROM        Records
					WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
							 v.ID AS Id
							, v.VideoTitle
							, v.Abstract
							, v.Runtime
							, v.AirDate
							, v.Season
							, v.Episode
							, v.ImageURL
							, v.ServerPath
							, v.[FileName]
							, NULL AS Director
							, v.PrimaryLanguage
							, v.SecondaryLanguage
							, v.SubtitleLanguage
							, v.YearOfRelease
							, v.AwardTextLong
							, v.AwardTextShort
							, v.Synopsis
							, v.CountryName
							, v.GenreName
							, v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, NULL AS DirectorId, vr.Rating, v.ProgramId
							, v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription
							, v.BillboardImageURL
						FROM        tt_Video_Meta_V2 AS v WITH (NOLOCK) LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
											  lt_Program_Person AS lpp on v.ProgramID = lpp.ProgramID INNER JOIN
											  lt_Person_Profession AS pp ON lpp.PersonID = pp.PersonID AND pp.ProfessionID = 1 INNER JOIN
											  dt_Person AS per ON lpp.PersonID = per.ID
						WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '')
						AND			(lpp.PersonID =  @DirectorId OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR per.Fullname LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					)
					
					SELECT		Id
								, VideoTitle
								, Abstract
								, Runtime
								, AirDate
								, Season
								, Episode
								, ImageURL
								, ServerPath
								, [FileName]
								, Director
								, PrimaryLanguage
								, SecondaryLanguage
								, SubtitleLanguage
								, YearOfRelease
								, AwardTextLong
								, AwardTextShort
								, Synopsis
								, CountryName
								, GenreName
								, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId
								, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
								, BillboardImageURL
								, (SELECT COUNT(Id) FROM Records) AS TotalRows
					FROM        Records
					WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY	Rating DESC
		END
		IF (@SortyBy =4)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, v.VideoTitle DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, NULL AS Director, v.PrimaryLanguage, 
                      v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, 
                      v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, NULL AS DirectorId, 
                      vl.ViewCount, vr.Rating, v.ProgramId, v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL
				FROM         tt_Video_Meta_V2 AS v WITH (NOLOCK) LEFT OUTER JOIN
                      Video_ViewCount AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
											  lt_Program_Person AS lpp on v.ProgramID = lpp.ProgramID INNER JOIN
											  lt_Person_Profession AS pp ON lpp.PersonID = pp.PersonID AND pp.ProfessionID = 1 INNER JOIN
											  dt_Person AS per ON lpp.PersonID = per.ID
				WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
				AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
				AND			(vl.WebsiteID = @SiteId OR vl.WebsiteID is null)
				--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
				AND			(lpp.PersonID =  @DirectorId OR @DirectorId = 0) 
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR per.Fullname LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

					)
					
				SELECT		Id
							, VideoTitle
							, Abstract
							, Runtime
							, AirDate
							, Season
							, Episode
							, ImageURL
							, ServerPath
							, [FileName]
							, Director
							, PrimaryLanguage
							, SecondaryLanguage
							, SubtitleLanguage
							, YearOfRelease
							, AwardTextLong
							, AwardTextShort
							, Synopsis
							, CountryName
							, GenreName
							, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId
							, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
							, BillboardImageURL
							, (SELECT COUNT(Id) FROM Records) AS TotalRows, 1 as [Rating]
				FROM        Records
				WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
				ORDER BY	ViewCount DESC, VideoTitle DESC
				END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoWithMeta_Select_V2]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoWithMeta_Select_V2] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 0,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@PrimaryLanguageId int = 0,
	@SecondaryLanguageId int = 0,
	@SubtitleLanguageId int = 0,
	@CountryId	int = 0,
	@GenreId	int = 0,
	@KeyWords	varchar(100)='',
	@DirectorId  int = 0,
	@IsAuthenticated bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;

	Declare @DateStamp datetime
	Declare @Interval int

	SET @DateStamp = (SELECT TOP 1 CreatedDate FROM tt_Video_Meta ORDER BY CreatedDate DESC)
 
	SET @DateStamp = ISNULL(@DateStamp, getdate()-1)

	SET @Interval = 15

	--SELECT CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) - @Interval

	IF (CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) > @Interval)
	BEGIN
		
		TRUNCATE TABLE tt_Video_Meta

		INSERT INTO tt_Video_Meta
		
		SELECT     v.ID, LTRIM(v.VideoTitle) AS VideoTitle, v.Keywords, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', 
                      vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + '/' + vf.[FileName]) ELSE vf.[FileName] END AS [FileName],'', 
                      l3.LanguageName AS PrimaryLanguage, l2.LanguageName AS SecondaryLanguage, l1.LanguageName AS SubtitleLanguage, vm.YearOfRelease, 
                      vm.AwardTextLong, vm.AwardTextShort, vm.Synopsis, c.CountryName, g.GenreName, p.ID AS ProgramID, p.ProgramTypeID, vf.VideoFileTypeID, 
                      p.StatusID, v.StartDate, v.EndDate, vm.LanguageID, vm.SecondaryLanguageID, vm.SubtitleLanguageID, vm.CountryID, p.GenreID, p.SubGenreID, 
                      g2.GenreName AS SubGenreName, 0,GETDATE() AS [CreatedDate], v.AgeRestrictionID, ag.Abbreviation AS AgeRestrictionAbbreviation, 
                      ag.AgeRestrictionDescription, v.BillboardImageURL
		FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID RIGHT OUTER JOIN
							  dt_VideoMeta AS vm WITH (NOLOCK) INNER JOIN
							  dt_Video AS v WITH (NOLOCK) ON vm.VideoID = v.ID INNER JOIN
							  dt_Program AS p WITH (NOLOCK) ON v.ProgramID = p.ID ON vf.VideoID = v.ID LEFT OUTER JOIN
							  dt_Genre AS g2 WITH (NOLOCK) ON p.SubGenreID = g2.ID INNER JOIN
							  dt_AgeRestriction AS ag WITH (NOLOCK) ON v.AgeRestrictionID = ag.ID LEFT OUTER JOIN
							  dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							  dt_Country AS c WITH (NOLOCK) ON vm.CountryID = c.ID LEFT OUTER JOIN
							  dt_Language AS l3 WITH (NOLOCK) ON vm.LanguageID = l3.ID LEFT OUTER JOIN
							  dt_Language AS l2 WITH (NOLOCK) ON vm.SecondaryLanguageID = l2.ID LEFT OUTER JOIN
							  dt_Language AS l1 WITH (NOLOCK) ON vm.SubtitleLanguageID = l1.ID
		
		
	END


	SET @IsAuthenticated = 0
	DECLARE @SiteId int
	SELECT @SiteId = ID
					FROM         dt_Website
					WHERE     (StatusID = 1) 
					AND (AuthenticationKey = @AuthenticationKey)
					AND (VideoAccess = 1)
				

	IF (@SiteId > 0)
	BEGIN
		SET @IsAuthenticated = 1
	END
	
	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@SortyBy =1)
		BEGIN

					WITH Records AS
					(
						--SELECT     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.Director, v.PrimaryLanguage, 
      --                v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, v.LanguageID, 
      --                v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramID, v.AgeRestrictionID, 
      --                v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL, PrPe.PersonID
						--FROM         Video_Rating AS vr RIGHT OUTER JOIN
      --                vw_TempVideoMeta AS v WITH (NOLOCK) ON vr.VideoID = v.ID LEFT OUTER JOIN
      --                lt_Person_Profession AS PeP INNER JOIN
      --                lt_Program_Person AS PrPe ON PeP.PersonID = PrPe.PersonID AND PeP.ProfessionID = 1 ON v.ProgramID = PrPe.ProgramID
						
						
						SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
						     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.Director, v.PrimaryLanguage, 
											  v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, v.LanguageID, 
											  v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramID, v.AgeRestrictionID, 
											  v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL, WC.WebsiteID
						FROM         Video_Rating AS vr RIGHT OUTER JOIN
											  vw_TempVideoMeta AS v WITH (NOLOCK) INNER JOIN
											  lt_Website_Channel AS WC INNER JOIN
											  lt_Channel_Program AS CP ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID ON vr.VideoID = v.ID
						WHERE     (WC.WebsiteID = @SiteId)
						
						AND			(v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
						--AND			(CONTAINS(VideoTitle, @KeyWords) OR CONTAINS(keywords, @KeyWords) OR CONTAINS(Director, @KeyWords) OR @KeyWords = '')
						AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
							
							
							--SELECT     P1.ProgramID, P1.PersonID
							--FROM         lt_Person_Profession AS P2 INNER JOIN
							--					  lt_Program_Person AS P1 ON P2.PersonID = P1.PersonID AND P2.ProfessionID = 1			
					)
					
					SELECT     Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, FileName, ServerPath, Director, PrimaryLanguage, SecondaryLanguage, 
										  SubtitleLanguage, YearOfRelease, AwardTextLong, AwardTextShort, Synopsis, CountryName, GenreName, LanguageID, SecondaryLanguageID, 
										  SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription, BillboardImageURL, (SELECT     COUNT(Id) FROM          Records) AS TotalRows
					FROM         Records
					WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY LTRIM(VideoTitle)

		END

		IF (@SortyBy =2)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
						     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.StartDate, v.Director, v.PrimaryLanguage, 
											  v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, v.LanguageID, 
											  v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramID, v.AgeRestrictionID, 
											  v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL
						FROM         Video_Rating AS vr RIGHT OUTER JOIN
											  vw_TempVideoMeta AS v WITH (NOLOCK) INNER JOIN
											  lt_Website_Channel AS WC INNER JOIN
											  lt_Channel_Program AS CP ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID ON vr.VideoID = v.ID
						WHERE     (WC.WebsiteID = @SiteId)
						AND     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0  OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
						AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					)
					
					SELECT		Id
								, VideoTitle
								, Abstract
								, Runtime
								, AirDate
								, Season
								, Episode
								, ImageURL
								, [FileName]
								, ServerPath
								, Director
								, PrimaryLanguage
								, SecondaryLanguage
								, SubtitleLanguage
								, YearOfRelease
								, AwardTextLong
								, AwardTextShort
								, Synopsis
								, CountryName
								, GenreName
								, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId 
								, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
								, BillboardImageURL
								,(SELECT COUNT(Id) FROM Records) AS TotalRows, 1 as [Rating]
					FROM        Records
					WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
						     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.Director, v.PrimaryLanguage, 
							  v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, v.LanguageID, 
							  v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramID, v.AgeRestrictionID, 
							  v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL
						FROM         Video_Rating AS vr RIGHT OUTER JOIN
											  vw_TempVideoMeta AS v WITH (NOLOCK) INNER JOIN
											  lt_Website_Channel AS WC INNER JOIN
											  lt_Channel_Program AS CP ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID ON vr.VideoID = v.ID
						WHERE     (WC.WebsiteID = @SiteId)
						AND     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '')
						AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					)
					
					SELECT		Id
								, VideoTitle
								, Abstract
								, Runtime
								, AirDate
								, Season
								, Episode
								, ImageURL
								, ServerPath
								, [FileName]
								, Director
								, PrimaryLanguage
								, SecondaryLanguage
								, SubtitleLanguage
								, YearOfRelease
								, AwardTextLong
								, AwardTextShort
								, Synopsis
								, CountryName
								, GenreName
								, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId
								, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
								, BillboardImageURL
								, (SELECT COUNT(Id) FROM Records) AS TotalRows
					FROM        Records
					WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY	Rating DESC
		END
		IF (@SortyBy =4)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, v.VideoTitle DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.Director, v.PrimaryLanguage, 
                      v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, v.LanguageID, 
                      v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vl.ViewCount, vr.Rating, v.ProgramID, 
                      v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL, WC.WebsiteID
				FROM         Video_Rating AS vr RIGHT OUTER JOIN
									  vw_TempVideoMeta AS v WITH (NOLOCK) INNER JOIN
									  lt_Website_Channel AS WC INNER JOIN
									  lt_Channel_Program AS CP ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID LEFT OUTER JOIN
									  Video_ViewCount AS vl ON v.ID = vl.VideoID ON vr.VideoID = v.ID
				WHERE     (WC.WebsiteID = @SiteId)
				AND     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
				AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
				AND			(vl.WebsiteID = @SiteId OR vl.WebsiteID is null)
				--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
				AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0) 
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

					)
					
				SELECT		Id
							, VideoTitle
							, Abstract
							, Runtime
							, AirDate
							, Season
							, Episode
							, ImageURL
							, ServerPath
							, [FileName]
							, Director
							, PrimaryLanguage
							, SecondaryLanguage
							, SubtitleLanguage
							, YearOfRelease
							, AwardTextLong
							, AwardTextShort
							, Synopsis
							, CountryName
							, GenreName
							, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId
							, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
							, BillboardImageURL
							, (SELECT COUNT(Id) FROM Records) AS TotalRows, 1 as [Rating]
				FROM        Records
				WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
				ORDER BY	ViewCount DESC, VideoTitle DESC
				END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoWithMeta_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoWithMeta_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 0,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@PrimaryLanguageId int = 0,
	@SecondaryLanguageId int = 0,
	@SubtitleLanguageId int = 0,
	@CountryId	int = 0,
	@GenreId	int = 0,
	@KeyWords	varchar(100)='',
	@DirectorId  int = 0,
	@IsAuthenticated bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;



	SET @IsAuthenticated = 0
	DECLARE @SiteId int
	
	SELECT @SiteId = ID 
	FROM         dt_Website
	WHERE		(StatusID = 1) 
	AND			(AuthenticationKey = @AuthenticationKey)
	AND			(VideoAccess = 1)
	
	IF (@SiteId > 0)
	BEGIN
	
	
		Declare @DateStamp datetime
		Declare @Interval int

		SET @DateStamp = (SELECT TOP 1 CreatedDate FROM tt_Video_Meta_V3 ORDER BY CreatedDate DESC)
	 
		SET @DateStamp = ISNULL(@DateStamp, getdate()-1)

		SET @Interval = 15

		--SELECT CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) - @Interval

		IF (CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) > @Interval)
		BEGIN
		
			--- Insert Video With Meta Data
			TRUNCATE TABLE tt_Video_Meta_V3

			INSERT INTO tt_Video_Meta_V3
			
			SELECT     v.ID, LTRIM(v.VideoTitle) AS VideoTitle, v.Keywords, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, l3.LanguageName AS PrimaryLanguage, 
						  vm.YearOfRelease, vm.Synopsis, c.CountryName, g.GenreName, p.ID AS ProgramID, p.ProgramTypeID, v.StartDate,
						  vm.LanguageID, vm.CountryID, p.GenreID, GETDATE() AS CreatedDate, ag.Abbreviation AS AgeRestrictionAbbreviation, v.BillboardImageURL
			FROM         dt_VideoMeta AS vm WITH (NOLOCK) INNER JOIN
								  dt_Video AS v WITH (NOLOCK) ON vm.VideoID = v.ID INNER JOIN
								  dt_Program AS p WITH (NOLOCK) ON v.ProgramID = p.ID INNER JOIN
								  dt_AgeRestriction AS ag WITH (NOLOCK) ON v.AgeRestrictionID = ag.ID LEFT OUTER JOIN
								  dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
								  dt_Country AS c WITH (NOLOCK) ON vm.CountryID = c.ID LEFT OUTER JOIN
								  dt_Language AS l3 WITH (NOLOCK) ON vm.LanguageID = l3.ID
			WHERE     (p.StatusID = 1)
			AND Getdate() between v.StartDate AND v.EndDate
			
			--- Insert Rating of 0 for new items for a specific Website
			--DECLARE @SiteId int
			--SET @SiteId = 23
			
			INSERT INTO dt_Rating (WebsiteID, VideoID, Rating, RateCount)
				SELECT     @SiteId AS WebsiteID, V.ID AS VideoID, 0 AS Rating, 0 AS RateCount
				FROM		dt_Video AS V WITH (NOLOCK) INNER JOIN
							dt_Program AS P WITH (NOLOCK) ON V.ProgramID = P.ID INNER JOIN
							lt_Channel_Program AS CP WITH (NOLOCK) ON P.ID = CP.ProgramID INNER JOIN
							lt_Website_Channel AS WC WITH (NOLOCK) ON CP.ChannelID = WC.ChannelID
				WHERE     (P.StatusID = 1) 
				AND (GETDATE() BETWEEN V.StartDate AND V.EndDate) 
				AND (WC.WebsiteID = @SiteId)
				AND V.ID NOT IN 
					(	
						SELECT     VideoID
						FROM         dt_Rating
						WHERE     (WebsiteID = @SiteId)				  
					)
					
					
				--DECLARE @SiteId int
				--SET @SiteId = 23

				INSERT INTO dt_VideoLog (VideoID, WebsiteID, ViewCount)
				SELECT     dt_Video.ID AS VideoID, @SiteId AS WebsiteID, 0 ViewCount
				FROM         dt_Video INNER JOIN
									  dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
									  lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
									  lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
				WHERE     (dt_Program.StatusID = 1) AND (GETDATE() BETWEEN dt_Video.StartDate AND dt_Video.EndDate)
				AND lt_Website_Channel.WebsiteID = @SiteId	  
					AND dt_Video.ID NOT IN (	
					SELECT      VideoID
				FROM         dt_VideoLog
				Where WebsiteID = @SiteId					  
								)	
		END
		
		SET @IsAuthenticated = 1
	END
	
	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@SortyBy = 1)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
							v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.PrimaryLanguage, v.YearOfRelease, v.CountryName, 
							v.GenreName, v.LanguageID, v.CountryID, v.GenreID, v.ProgramID, v.AgeRestrictionAbbreviation, v.BillboardImageURL, vr.Rating
				FROM		tt_Video_Meta_v3 AS v WITH (NOLOCK) INNER JOIN
							lt_Website_Channel AS WC WITH (NOLOCK) INNER JOIN
							lt_Channel_Program AS CP WITH (NOLOCK) ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID INNER JOIN
							Video_Rating AS vr WITH (NOLOCK) ON v.ID = vr.VideoID AND (vr.WebsiteID = @SiteId)
				WHERE		(WC.WebsiteID = @SiteId) 
				--AND			(v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
				AND			(v.ID in (SELECT VideoID FROM dt_VideoFile WHERE VideoFileTypeID = @VideoFileTypeId) OR @VideoFileTypeId = 0)
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0) 
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%'  OR @KeyWords = '') -- OR Director LIKE '%'+@KeyWords+'%'		
				
				
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
			)
			
			SELECT		Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, PrimaryLanguage, YearOfRelease, CountryName, GenreName, LanguageID,  
						CountryID, GenreID, Rating, ProgramId, AgeRestrictionAbbreviation, BillboardImageURL, (SELECT COUNT(Id) FROM Records) AS TotalRows
			-- 			,AwardTextLong, AwardTextShort, FileName, ServerPath, Director, , SecondaryLanguage, SubtitleLanguage, Synopsis, SecondaryLanguageID, SubtitleLanguageID, SubGenreID, SubGenreName, DirectorId, AgeRestrictionID, AgeRestrictionDescription
			FROM         Records
			WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			--ORDER BY LTRIM(VideoTitle)
			ORDER BY VideoTitle

		END

		IF (@SortyBy = 2)
		BEGIN
		
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC, v.VideoTitle ASC) AS RowNum,
							v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.PrimaryLanguage, v.YearOfRelease, v.CountryName, 
							v.GenreName, v.LanguageID, v.CountryID, v.GenreID, v.ProgramID, v.AgeRestrictionAbbreviation, v.BillboardImageURL, vr.Rating, v.StartDate
				FROM		tt_Video_Meta_v3 AS v WITH (NOLOCK) INNER JOIN
							lt_Website_Channel AS WC WITH (NOLOCK) INNER JOIN
							lt_Channel_Program AS CP WITH (NOLOCK) ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID INNER JOIN
							Video_Rating AS vr WITH (NOLOCK) ON v.ID = vr.VideoID AND (vr.WebsiteID = @SiteId)
				WHERE		(WC.WebsiteID = @SiteId) 
				AND			(v.ID in (SELECT VideoID FROM dt_VideoFile WHERE VideoFileTypeID = @VideoFileTypeId) OR @VideoFileTypeId = 0) 
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0) 
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR @KeyWords = '')-- OR Director LIKE '%'+@KeyWords+'%'		
			)
			
			SELECT		Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, PrimaryLanguage, YearOfRelease, CountryName, GenreName, LanguageID,  
						CountryID, GenreID, Rating, ProgramId, AgeRestrictionAbbreviation, BillboardImageURL, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM         Records
			WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC, VideoTitle ASC
			
		END	

		IF (@SortyBy = 3)
		BEGIN
		
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
							v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.PrimaryLanguage, v.YearOfRelease, v.CountryName, 
							v.GenreName, v.LanguageID, v.CountryID, v.GenreID, v.ProgramID, v.AgeRestrictionAbbreviation, v.BillboardImageURL, vr.Rating
				FROM		tt_Video_Meta_v3 AS v WITH (NOLOCK) INNER JOIN
							lt_Website_Channel AS WC WITH (NOLOCK) INNER JOIN
							lt_Channel_Program AS CP WITH (NOLOCK) ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID INNER JOIN
							Video_Rating AS vr WITH (NOLOCK) ON v.ID = vr.VideoID AND (vr.WebsiteID = @SiteId)
				WHERE		(WC.WebsiteID = @SiteId) 
				AND			(v.ID in (SELECT VideoID FROM dt_VideoFile WHERE VideoFileTypeID = @VideoFileTypeId) OR @VideoFileTypeId = 0) 
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0)
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR @KeyWords = '')-- OR Director LIKE '%'+@KeyWords+'%'
			)
			
			SELECT		Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, PrimaryLanguage, YearOfRelease, CountryName, GenreName, LanguageID,  
						CountryID, GenreID, Rating, ProgramId, AgeRestrictionAbbreviation, BillboardImageURL, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM         Records
			WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY Rating DESC
			
		END	
		
		IF (@SortyBy = 4)
		BEGIN

			WITH Records AS
			(
				
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, v.VideoTitle ASC) AS RowNum,
							v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.PrimaryLanguage, v.YearOfRelease, v.CountryName, v.GenreName, 
							v.LanguageID, v.CountryID, v.GenreID, v.ProgramID, v.AgeRestrictionAbbreviation, v.BillboardImageURL, vr.Rating, vl.ViewCount
				FROM         tt_Video_Meta_v3 AS v WITH (NOLOCK) INNER JOIN
									  lt_Website_Channel AS WC WITH (NOLOCK) INNER JOIN
									  lt_Channel_Program AS CP WITH (NOLOCK) ON WC.ChannelID = CP.ChannelID ON v.ProgramID = CP.ProgramID INNER JOIN
									  Video_Rating AS vr WITH (NOLOCK) ON v.ID = vr.VideoID AND vr.WebsiteID = @SiteId INNER JOIN
									  Video_ViewCount AS vl WITH (NOLOCK) ON v.ID = vl.VideoID AND vl.WebsiteID = @SiteId
				WHERE     (WC.WebsiteID = @SiteId)
				AND			(v.ID in (SELECT VideoID FROM dt_VideoFile WHERE VideoFileTypeID = @VideoFileTypeId) OR @VideoFileTypeId = 0) 
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(v.ProgramId in (SELECT  ProgramID FROM lt_Program_Person WHERE PersonID = @DirectorId) OR @DirectorId = 0)
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR @KeyWords = '')-- OR Director LIKE '%'+@KeyWords+'%'
			)
			SELECT		Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, PrimaryLanguage, YearOfRelease, CountryName, GenreName, LanguageID,  
						CountryID, GenreID, Rating, ProgramId, AgeRestrictionAbbreviation, BillboardImageURL, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM         Records
			WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ViewCount DESC, VideoTitle ASC
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoFile_SelectByManItemId]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoFile_SelectByManItemId]
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ManItemId varchar(50) = '',
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SET @IsAuthenticated = 0
	
	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1
		
			SELECT      vf.ID AS VideoFileId, vf.VideoID,  v.VideoTitle, 
								   CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
							  ELSE vfp.ServerPath END + CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
							  + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FilePath
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , v.CastCrewID
                    , vf.SizeInKB
                    , vf.VideoDimension							  
			FROM         dt_VideoFile AS vf INNER JOIN
								  dt_VideoFilePath AS vfp ON vf.VideoFilePathID = vfp.ID INNER JOIN
								  dt_Video AS v ON vf.VideoID = v.ID AND v.StatusID = 1
			WHERE     (vf.ManItemId = @ManItemId) AND (GETDATE() BETWEEN v.StartDate AND v.EndDate)

	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoFile_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan
-- alter date: 02/09/2010
-- Description:	Get Video Files by VideoId and FileType
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_VideoFile_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoFileTypeId smallint = 0,
	@ProductId int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @IsAuthenticated = 0
	
	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT   distinct  vf.VideoID, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, vf.VideoFileTypeID, vf.ManItemId, 
                      vf.ProductId
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , v.CastCrewID
                    , vf.SizeInKB
                    , vf.VideoDimension                      
		FROM         dt_VideoFile AS vf INNER JOIN
                      dt_VideoFilePath AS vfp ON vf.VideoFilePathID = vfp.ID INNER JOIN
                      dt_Video AS v ON vf.VideoID = v.ID AND v.StatusID = 1 
		WHERE      (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0) 
		AND			(vf.ProductId = @ProductId OR @ProductId = 0)
		AND			(getdate() BETWEEN v.StartDate AND v.ExpiryDate)
		AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
		AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_VideoCategory_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Jacques du Preez
-- Date Modified: 2009-09-25
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Desigan Royan
-- Date Modified: 2010-04-15
-- =============================================
-- ==============Modified=======================
-- Author:		Desigan Royan
-- Date Modified: 2010-04-28
-- =============================================
-- ==============Modified=======================
-- Author:		3 of the horseman
-- Date Modified: 2010-10-04
-- =============================================

CREATE PROCEDURE [dbo].[ws_dt_VideoCategory_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@VideoId int = 0,
	@CategoryParentID int = null,
	@IsAuthenticated bit OUTPUT
	
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SiteId int
	SET @IsAuthenticated = 0

if (@CategoryParentID is not null)
begin
	IF EXISTS(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website WITH(NOLOCK)
		Where AuthenticationKey = @AuthenticationKey
	END

	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@VideoId > 0)
		BEGIN
			SELECT     vc.ID, vc.CategoryName, vc.CategoryParentID
			FROM         dt_VideoCategory AS vc WITH (NOLOCK) INNER JOIN
								  lt_Website_VideoCategory AS wvc WITH (NOLOCK) ON vc.ID = wvc.VideoCategoryID INNER JOIN
								  lt_Video_VideoCategory AS vvc WITH (NOLOCK) ON vc.ID = vvc.VideoCategoryID
			WHERE     (vc.StatusID = 1) AND (wvc.WebsiteID = @SiteId) AND (vvc.VideoID = @VideoID)
			ORDER BY vc.CategoryName
		END
		ELSE
		BEGIN
			SELECT     vc.ID, vc.CategoryName, vc.CategoryParentID
			FROM         dt_VideoCategory AS vc WITH(NOLOCK) INNER JOIN
								  lt_Website_VideoCategory AS wvc WITH(NOLOCK) ON vc.ID = wvc.VideoCategoryID
			WHERE     (vc.StatusID = 1) AND (wvc.WebsiteID = @SiteId)
			ORDER BY vc.CategoryName
		END
	END
	end
	else
	begin
	IF EXISTS(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website WITH(NOLOCK)
		Where AuthenticationKey = @AuthenticationKey
	END

	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@VideoId > 0)
		BEGIN
			SELECT     vc.ID, vc.CategoryName, vc.CategoryParentID
			FROM         dt_VideoCategory AS vc WITH (NOLOCK) INNER JOIN
								  lt_Website_VideoCategory AS wvc WITH (NOLOCK) ON vc.ID = wvc.VideoCategoryID INNER JOIN
								  lt_Video_VideoCategory AS vvc WITH (NOLOCK) ON vc.ID = vvc.VideoCategoryID
			WHERE     (vc.StatusID = 1) AND (wvc.WebsiteID = @SiteId) AND (vvc.VideoID = @VideoID) 
			AND	CategoryParentID = @CategoryParentID
			ORDER BY vc.CategoryName
		END
		ELSE
		BEGIN
			SELECT     vc.ID, vc.CategoryName, vc.CategoryParentID
			FROM         dt_VideoCategory AS vc WITH(NOLOCK) INNER JOIN
								  lt_Website_VideoCategory AS wvc WITH(NOLOCK) ON vc.ID = wvc.VideoCategoryID
			WHERE     (vc.StatusID = 1) AND (wvc.WebsiteID = @SiteId)
			AND	CategoryParentID = @CategoryParentID
			ORDER BY vc.CategoryName
		END
	END
	end
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Video_Select_mms3]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- Create date: 2000/02/04
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Video_Select_mms3]
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType varchar(max) = '0',
	@VideoFileTypeId int = 0,
	@SortBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@KeyWords	varchar(100) = '',
	@CountryCode	varchar(10) = '', 
	@ChannelId  int = 0,
	@ProductId	int = 0,
	@GenreID  int = 0,
	@SubGenreID INT = 0,
	@VideoCategoryID  int = 0,
	@ScheduleDate DateTime = NULL,
	@IsAuthenticated bit OUTPUT,
	@IsRestricted bit OUTPUT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SiteId int, @Vid int

	SET @IsAuthenticated = 0
	SET @IsRestricted = 0
	SET @Vid = 0
	
	-- Get SiteId based on the AuthenticationKey parameter defaulted to 0
	SET @SiteId = ISNULL((
		SELECT	ID
		FROM	dt_Website  WITH(NOLOCK)
		Where AuthenticationKey = @AuthenticationKey
		AND (VideoAccess = 1)
	),0)
	
	IF @SiteId > 0
	BEGIN
		SET @IsAuthenticated = 1
		IF @ScheduleDate IS NULL SET @ScheduleDate = GetDate()
		
		-- Sets the Video Id, if only 1 Video is parsed to the Stored Proc, 
		-- Consider using a new sortby for better performance
		IF (CHARINDEX(',', @VideoId) = 0)
			BEGIN
				SET @Vid = CAST(@VideoID AS INT)
			END
	
	-- Query for a single Item only
	IF (@Vid > 0)
	BEGIN
		SELECT TOP 1 v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle
		, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, null AS [FileName], NULL AS ServerPath
		, v.BillboardImageURL, NULL AS Rating, ar.Abbreviation AS AgeRestriction, ppGenre.ThemeID AS [GenreID], ppGenre.GenreName
		, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName, v.VideoProgramTypeID, 1 AS TotalRows, pp.ProductID AS Product, v.ExpiryDate
		, v.Price, v.Geolocation, v.RuntimeInMinutes, v.RentalPeriodInHours, v.CastCrewID, v.Keywords, v.IBSProductID, v.AssetId
		FROM	dt_Program AS p WITH (NOLOCK) INNER JOIN
				dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
				lt_Product_Program AS pp ON p.ID = pp.ProgramID  LEFT OUTER JOIN
				dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
				lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
		WHERE	(pp.ProductID = @ProductId OR @ProductId = 0)
		AND		(v.Id  = @Vid)
		AND		(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
		AND		(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
		AND		(v.StatusID = 1 OR v.StatusID = NULL)
		AND		(
					v.ProgramId in
					(
						SELECT     p.ID
						FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
											  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
											  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
						WHERE     (wp.WebsiteID = @SiteId)
						AND		(p.StatusID = 1 OR p.StatusID = NULL)
						AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
					)
				)
	END
	ELSE
	BEGIN
		
		IF (@ProgramId = '' OR @ProgramId IS NULL) SET @ProgramId = '0'
		IF (@ProgramId <> '0')
		BEGIN
			DECLARE @ProgramIds table (ID int IDENTITY(1,1) primary key, ProgramId int NOT NULL,unique clustered (ID))
			Insert Into @ProgramIds(ProgramId)
			select Value from dbo.Array(@ProgramId)
		END
		
		IF (@VideoId = '' OR @VideoId IS NULL) SET @VideoId = '0'
		IF (@VideoId <> '0')
		BEGIN
			DECLARE @VideoIds table (ID int IDENTITY(1,1) primary key, VideoId int NOT NULL,unique clustered (ID))
			Insert Into @VideoIds ( VideoId)
			select Value from dbo.Array(@VideoID)
		END
		
		
		
		IF (@VideoCategoryID > 0)
		BEGIN
			DECLARE @VideoIds_VideoCategory table (ID int IDENTITY(1,1) primary key, VideoId int NOT NULL,unique clustered (ID))
			Insert Into @VideoIds_VideoCategory (VideoId)
			SELECT     DISTINCT VideoID
			FROM         lt_Video_VideoCategory WITH(NOLOCK)
			WHERE		(VideoCategoryID = @VideoCategoryID)
		END

		IF (@VideoFileTypeId > 0)
		BEGIN
			DECLARE @VideoIds_dt_VideoFile table (ID int IDENTITY(1,1) primary key, VideoId int NOT NULL,unique clustered (ID))
			Insert Into @VideoIds_dt_VideoFile (VideoId)
			SELECT  distinct   VideoID
			FROM         dt_VideoFile  WITH(NOLOCK)
			WHERE		(VideoFileTypeID = @VideoFileTypeId)
		END

		
		IF (@SortBy IN (1,2, 11))
		BEGIN
		--Changed the row number to be based on the program name if sort order is 11 (Lyle)		

			WITH Records AS
			(
			SELECT TOP 1000
			
			--CASE WHEN @SortBy = 1 THEN LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) END 
			--,
			CASE @SortBy 
			
			WHEN  1 THEN  (ROW_NUMBER() OVER( ORDER BY LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), ''))))
			WHEN  2 THEN  (ROW_NUMBER() OVER( ORDER BY v.Startdate DESC, v.VideoTitle)) 
			WHEN  11 THEN  (ROW_NUMBER() OVER( ORDER BY ProgramName))
			END AS RowNum
			--
			
			 ,
		     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, --vr.Rating, 
                  NULL AS ServerPath, null AS FileName, ar.Abbreviation AS AgeRestriction, 
                 ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName,
                   v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate,
                  v.Price,
				  v.Geolocation,
				  v.RuntimeInMinutes,
				  v.RentalPeriodInHours,
				  v.CastCrewID,
				  v.Keywords,
				  v.IBSProductID,
				  v.AssetId,
				  v.StartDate
			FROM	dt_Program AS p WITH (NOLOCK) INNER JOIN
					dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
					lt_Product_Program AS pp ON p.ID = pp.ProgramID  LEFT OUTER JOIN
					dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
					lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
			WHERE			(pp.ProductID = @ProductId OR @ProductId = 0)
			AND			((v.Id  in (SELECT VideoId FROM @VideoIds)) OR (@VideoId = '0'))
			AND			((v.ProgramID  in (SELECT ProgramId FROM @ProgramIds)) OR (@ProgramId = '0'))
			AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
			AND			((v.ProgramId  in (select Value from dbo.Array(@ProgramId)) OR (0 in (select Value from dbo.Array(@ProgramId)))))
			AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
			AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
			AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
			AND			(v.StatusID = 1 OR v.StatusID = NULL)
			----AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
			AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
			AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
			AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
			AND			(
							v.ProgramId in
							(
								SELECT     p.ID
								FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
													  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
													  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
								WHERE     (wp.WebsiteID = @SiteId)
								AND		(p.StatusID = 1 OR p.StatusID = NULL)
								AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
							)
						)
			AND			(
							v.ProgramId in
							(
								SELECT     cp.ProgramID
								FROM         lt_Channel_Program AS cp WITH (NOLOCK)
								WHERE     (cp.ChannelID = @ChannelId)
								OR @ChannelId = 0
							)
			)
			AND			(
					v.ID in
					(
					SELECT	VideoID
					FROM	@VideoIds_dt_VideoFile
					)
					OR @VideoFileTypeId = 0
				)
			AND			(
							v.ID in
							(
								SELECT     VideoID
								FROM        @VideoIds_VideoCategory
							)
							OR @VideoCategoryID = 0
						)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId,
			StartDate
			FROM        Records r
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			--ORDER BY	VideoTitle
			ORDER BY	
			CASE WHEN @SortBy = 1
			THEN VideoTitle END,
			CASE WHEN @SortBy = 2
			THEN StartDate END DESC,
			CASE WHEN @SortBy = 11
			THEN ProgramName END
		END
	END
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Video_Select_bup_2]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Jacques du Preez
-- Date Modified: 2009-09-25
-- =============================================

CREATE PROCEDURE [dbo].[ws_dt_Video_Select_bup_2] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 1,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@KeyWords	varchar(100) = '',
	@CountryCode	varchar(10) = '', 
	@IsAuthenticated bit OUTPUT,
	@IsRestricted bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SiteId int, @ProgId int, @Vid int
	SET @IsAuthenticated = 0
	SET @IsRestricted = 0
	SET @ProgId = 0
	SET @Vid = 0

	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website
		Where AuthenticationKey = @AuthenticationKey
		
		IF (CHARINDEX(',', @VideoId) = 0)
			BEGIN
				SET @ProgId = (SELECT ISNULL(ProgramID, 0) AS ProgramID  FROM  dt_Video WHERE Id  = CAST(@VideoID AS INT))
				SET @ProgId = ISNULL(@ProgId, 0)
				SET @Vid = CAST(@VideoID AS INT)
			END

		--SELECT @ProgId AS ProGra, CHARINDEX(',', @VideoId) As Pos

		IF (@ProgId = 0)
			BEGIN
				IF (CHARINDEX(',', @ProgramId) = 0)
				BEGIN
					SET @ProgId = CAST(@ProgramId AS INT)
				END
			END
		IF (@ProgId > 0)
		BEGIN
			IF NOT EXISTS(
				SELECT     TOP (1) gc.GeoIPCountryCode, gc.GeoGroupID, p.ID
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
									  [DStvUtilities].[dbo].lt_GeoGroup_GeoCountry AS gc WITH (NOLOCK) ON p.GeoGroupID = gc.GeoGroupID
				WHERE     (gc.GeoIPCountryCode = @CountryCode) 
				--AND (gc.GeoGroupID <> 2) 
				AND (p.ID = @ProgId)
			)
			BEGIN
				SET @IsRestricted = 1
			END
		END
	END

	IF (@IsAuthenticated = 1)
	--IF (@IsAuthenticated = 1 AND @IsRestricted = 0)
	BEGIN
		IF (@SortyBy =1)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, ar.Abbreviation AS AgeRestriction, 
                      p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension                      
FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) LEFT OUTER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	VideoTitle
		END

		IF (@SortyBy =2)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, v.StartDate, 
                      ar.Abbreviation AS AgeRestriction, p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension                      
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, ar.Abbreviation AS AgeRestriction, 
                      p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension                      
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	Rating DESC
		
		END
	IF (@SortyBy =4)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, v.VideoTitle DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, vl.ViewCount, 
                      ar.Abbreviation AS AgeRestriction, p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension                      
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_ViewCount AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ViewCount DESC, VideoTitle DESC
		
		END
	IF (@SortyBy =5) --Related
		BEGIN
			IF @Vid > 0
			BEGIN

				WITH Records AS
				(
					SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate) AS RowNum,
					     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, vl.ViewCount, 
                      ar.Abbreviation AS AgeRestriction, v.StartDate, p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension                      
					FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
                      lt_Video_Tag ON vf.VideoID = lt_Video_Tag.VideoId ON v.ID = vf.VideoID LEFT OUTER JOIN
                      dt_VideoLog AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID

					WHERE     lt_Video_Tag.Tag IN
											  (SELECT     Keywords
												FROM          dt_Video
												WHERE      (ID = @Vid))


					AND			v.ID != @Vid
					
					AND     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
					--AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
					AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
					AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
					AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
					AND			(v.StatusID = 1 OR v.StatusID = NULL)
					AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
					AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					AND			(
									v.Id in 
									(	
										SELECT	dt_Video.ID
										FROM    dt_Video INNER JOIN
											dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
											lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
											lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
										WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
											(dt_Video.StatusID = NULL)
									)
								)
					AND			(
									v.ProgramId in
									(
										SELECT	dt_Program.ID
										FROM	dt_Program INNER JOIN
											lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
											lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
											dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
										WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
											(dt_Program.StatusID = NULL)
									)
								)
				)
				
				SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
				FROM        Records
				WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
				ORDER BY	StartDate DESC
			END
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Video_Select_bup]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Jacques du Preez
-- Date Modified: 2009-09-25
-- =============================================

CREATE PROCEDURE [dbo].[ws_dt_Video_Select_bup] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 1,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@KeyWords	varchar(100) = '',
	@CountryCode	varchar(10) = '', 
	@IsAuthenticated bit OUTPUT,
	@IsRestricted bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SiteId int, @ProgId int, @Vid int
	SET @IsAuthenticated = 0
	SET @IsRestricted = 0
	SET @ProgId = 0
	SET @Vid = 0

	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website
		Where AuthenticationKey = @AuthenticationKey
		
		IF (CHARINDEX(',', @VideoId) = 0)
			BEGIN
				SET @ProgId = (SELECT ISNULL(ProgramID, 0) AS ProgramID  FROM  dt_Video WHERE Id  = CAST(@VideoID AS INT))
				SET @ProgId = ISNULL(@ProgId, 0)
				SET @Vid = CAST(@VideoID AS INT)
			END

		--SELECT @ProgId AS ProGra, CHARINDEX(',', @VideoId) As Pos

		IF (@ProgId = 0)
			BEGIN
				IF (CHARINDEX(',', @ProgramId) = 0)
				BEGIN
					SET @ProgId = CAST(@ProgramId AS INT)
				END
			END
		IF (@ProgId > 0)
		BEGIN
			IF NOT EXISTS(
				SELECT     TOP (1) gc.GeoIPCountryCode, gc.GeoGroupID, p.ID
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
									  [DStvUtilities].[dbo].lt_GeoGroup_GeoCountry AS gc WITH (NOLOCK) ON p.GeoGroupID = gc.GeoGroupID
				WHERE     (gc.GeoIPCountryCode = @CountryCode) 
				--AND (gc.GeoGroupID <> 2) 
				AND (p.ID = @ProgId)
			)
			BEGIN
				SET @IsRestricted = 1
			END
		END
	END

	IF (@IsAuthenticated = 1)
	--IF (@IsAuthenticated = 1 AND @IsRestricted = 0)
	BEGIN
		IF (@SortyBy =1)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
					 v.ID AS Id
					, v.VideoTitle
					, v.Abstract
					, v.ProgramID
					, p.ProgramName
					, v.Runtime
					, v.AirDate
					, v.Season
					, v.Episode
					, v.ImageURL
					, v.BillboardImageURL
					, vr.Rating
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9))
						else vfp.ServerPath
					end as ServerPath
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7) ) +  vf.[FileName])
						else vf.[FileName]
					end AS [FileName]
					, ar.Abbreviation AS AgeRestriction
					, p.GenreID
					, g.GenreName
					, p.SubGenreID AS SubGenreID
					, sg.GenreName AS SubGenreName
					, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							 dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
							 dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
							 Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
							 dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID INNER JOIN
							 dt_Program AS p WITH(NOLOCK) ON v.ProgramID = p.ID LEFT OUTER JOIN
							 dt_Genre AS g WITH(NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							 dt_Genre AS sg WITH(NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	VideoTitle
		END

		IF (@SortyBy =2)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
					 v.ID AS Id
					, v.VideoTitle
					, v.Abstract
					, v.ProgramID
					, p.ProgramName
					, v.Runtime
					, v.AirDate
					, v.Season
					, v.Episode
					, v.ImageURL
					, v.BillboardImageURL
					, vr.Rating
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9))
						else vfp.ServerPath
					end as ServerPath
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7) ) +  vf.[FileName])
						else vf.[FileName]
					end AS [FileName]
					, v.StartDate
					, ar.Abbreviation AS AgeRestriction
					, p.GenreID
					, g.GenreName
					, p.SubGenreID AS SubGenreID
					, sg.GenreName AS SubGenreName
					, v.VideoProgramTypeID
		
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							 dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
							 dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
							 Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
							 dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID INNER JOIN
							 dt_Program AS p WITH(NOLOCK) ON v.ProgramID = p.ID LEFT OUTER JOIN
							 dt_Genre AS g WITH(NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							 dt_Genre AS sg WITH(NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
					 v.ID AS Id
					, v.VideoTitle
					, v.Abstract
					, v.ProgramID
					, p.ProgramName
					, v.Runtime
					, v.AirDate
					, v.Season
					, v.Episode
					, v.ImageURL
					, v.BillboardImageURL
					, vr.Rating
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension					
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9))
						else vfp.ServerPath
					end as ServerPath
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7) ) +  vf.[FileName])
						else vf.[FileName]
					end AS [FileName]
					, ar.Abbreviation AS AgeRestriction
					, p.GenreID
					, g.GenreName
					, p.SubGenreID AS SubGenreID
					, sg.GenreName AS SubGenreName
					, v.VideoProgramTypeID
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							 dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
							 dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
							 Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
							 dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID INNER JOIN
							 dt_Program AS p WITH(NOLOCK) ON v.ProgramID = p.ID LEFT OUTER JOIN
							 dt_Genre AS g WITH(NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							 dt_Genre AS sg WITH(NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	Rating DESC
		
		END
	IF (@SortyBy =4)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, v.VideoTitle DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', 
                      vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, 
                      vl.ViewCount, ar.Abbreviation AS AgeRestriction
                      , p.GenreID
					, g.GenreName
					, p.SubGenreID AS SubGenreID
					, sg.GenreName AS SubGenreName
					, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension					
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
									  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
									  dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
									  Video_ViewCount AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
									  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
							 dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID INNER JOIN
							 dt_Program AS p WITH(NOLOCK) ON v.ProgramID = p.ID LEFT OUTER JOIN
							 dt_Genre AS g WITH(NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							 dt_Genre AS sg WITH(NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ViewCount DESC, VideoTitle DESC
		
		END
	IF (@SortyBy =5) --Related
		BEGIN
			IF @Vid > 0
			BEGIN

				WITH Records AS
				(
					SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate) AS RowNum,
						 v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
						  CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
						  ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', 
						  vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, 
						  vl.ViewCount, ar.Abbreviation AS AgeRestriction, v.StartDate
						  , p.GenreID
					, g.GenreName
					, p.SubGenreID AS SubGenreID
					, sg.GenreName AS SubGenreName
					, v.VideoProgramTypeID
					, v.Price
                    , v.Geolocation
                    , v.RuntimeInMinutes
                    , v.RentalPeriodInHours
                    , vf.SizeInKB
                    , vf.VideoDimension					
					FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
										  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
										  dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID INNER JOIN
										  lt_Video_Tag ON vf.VideoID = lt_Video_Tag.VideoId LEFT OUTER JOIN
										  dt_VideoLog AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
										  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
										  dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID INNER JOIN
							 dt_Program AS p WITH(NOLOCK) ON v.ProgramID = p.ID LEFT OUTER JOIN
							 dt_Genre AS g WITH(NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							 dt_Genre AS sg WITH(NOLOCK) ON p.SubGenreID = g.ID

					WHERE     lt_Video_Tag.Tag IN
											  (SELECT     Keywords
												FROM          dt_Video
												WHERE      (ID = @Vid))


					AND			v.ID != @Vid
					
					AND     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
					--AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
					AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
					AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
					AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
					AND			(v.StatusID = 1 OR v.StatusID = NULL)
					AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
					AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					AND			(
									v.Id in 
									(	
										SELECT	dt_Video.ID
										FROM    dt_Video INNER JOIN
											dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
											lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
											lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
										WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
											(dt_Video.StatusID = NULL)
									)
								)
					AND			(
									v.ProgramId in
									(
										SELECT	dt_Program.ID
										FROM	dt_Program INNER JOIN
											lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
											lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
											dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
										WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
											(dt_Program.StatusID = NULL)
									)
								)
				)
				
				SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
				FROM        Records
				WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
				ORDER BY	StartDate DESC
			END
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Video_Select_All]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Jacques du Preez
-- Date Modified: 2009-09-25
-- =============================================

CREATE PROCEDURE [dbo].[ws_dt_Video_Select_All] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 1,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@KeyWords	varchar(100) = '',
	@CountryCode	varchar(10) = '', 
	@IsAuthenticated bit OUTPUT,
	@IsRestricted bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SiteId int, @ProgId int, @Vid int
	SET @IsAuthenticated = 0
	SET @IsRestricted = 0
	SET @ProgId = 0
	SET @Vid = 0

	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website
		Where AuthenticationKey = @AuthenticationKey
		
		IF (CHARINDEX(',', @VideoId) = 0)
			BEGIN
				SET @ProgId = (SELECT ISNULL(ProgramID, 0) AS ProgramID  FROM  dt_Video WHERE Id  = CAST(@VideoID AS INT))
				SET @ProgId = ISNULL(@ProgId, 0)
				SET @Vid = CAST(@VideoID AS INT)
			END

		--SELECT @ProgId AS ProGra, CHARINDEX(',', @VideoId) As Pos

		IF (@ProgId = 0)
			BEGIN
				IF (CHARINDEX(',', @ProgramId) = 0)
				BEGIN
					SET @ProgId = CAST(@ProgramId AS INT)
				END
			END
		IF (@ProgId > 0)
		BEGIN
			IF NOT EXISTS(
				SELECT     TOP (1) gc.GeoIPCountryCode, gc.GeoGroupID, p.ID
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
									  [DStvUtilities].[dbo].lt_GeoGroup_GeoCountry AS gc WITH (NOLOCK) ON p.GeoGroupID = gc.GeoGroupID
				WHERE     (gc.GeoIPCountryCode = @CountryCode) 
				--AND (gc.GeoGroupID <> 2) 
				AND (p.ID = @ProgId)
			)
			BEGIN
				SET @IsRestricted = 1
			END
		END
	END

	IF (@IsAuthenticated = 1)
	--IF (@IsAuthenticated = 1 AND @IsRestricted = 0)
	BEGIN
		IF (@SortyBy =1)
		BEGIN
			SELECT v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                  CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                  ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                  + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, ar.Abbreviation AS AgeRestriction, 
                  p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      vf.SizeInKB,
                      vf.VideoDimension                  
			FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                  dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                  dt_VideoFile AS vf WITH (NOLOCK) LEFT OUTER JOIN
                  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                  dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                  dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                  dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
			WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
			AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
			AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
			AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
			AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
			AND			(v.StatusID = 1 OR v.StatusID = NULL)
			AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
			AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

			AND			(
							v.Id in 
							(	
								SELECT	dt_Video.ID
								FROM    dt_Video INNER JOIN
									dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
									lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
									lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
								WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
									(dt_Video.StatusID = NULL)
							)
						)
			AND			(
							v.ProgramId in
							(
								SELECT	dt_Program.ID
								FROM	dt_Program INNER JOIN
									lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
									lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
									dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
								WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
									(dt_Program.StatusID = NULL)
							)
						)
			ORDER BY	VideoTitle
			--SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			--FROM        Records
			--WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			--ORDER BY	VideoTitle
		END

		IF (@SortyBy =2)
		BEGIN
			SELECT v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                  CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                  ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                  + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, v.StartDate, 
                  ar.Abbreviation AS AgeRestriction, p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      vf.SizeInKB,
                      vf.VideoDimension                  
			FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                  dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                  dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                  Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                  dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                  dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                  dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
			WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
			AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
			AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
			AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
			AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
			AND			(v.StatusID = 1 OR v.StatusID = NULL)
			AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
			AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
			
			AND			(
							v.Id in 
							(	
								SELECT	dt_Video.ID
								FROM    dt_Video INNER JOIN
									dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
									lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
									lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
								WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
									(dt_Video.StatusID = NULL)
							)
						)
			AND			(
							v.ProgramId in
							(
								SELECT	dt_Program.ID
								FROM	dt_Program INNER JOIN
									lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
									lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
									dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
								WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
									(dt_Program.StatusID = NULL)
							)
						)
			ORDER BY	StartDate DESC
			--SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
				
		END	

		IF (@SortyBy =3)
		BEGIN
				SELECT v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, ar.Abbreviation AS AgeRestriction, 
                      p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      vf.SizeInKB,
                      vf.VideoDimension                      
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			
			--SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			ORDER BY	Rating DESC
		
		END
	IF (@SortyBy =4)
		BEGIN
				SELECT v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, vl.ViewCount, 
                      ar.Abbreviation AS AgeRestriction, p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      vf.SizeInKB,
                      vf.VideoDimension                      
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_ViewCount AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			--SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
			ORDER BY	ViewCount DESC, VideoTitle DESC
		
		END
	IF (@SortyBy =5) --Related
		BEGIN
			IF @Vid > 0
			BEGIN
					SELECT v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) 
                      + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, vl.ViewCount, 
                      ar.Abbreviation AS AgeRestriction, v.StartDate, p.GenreID, g.GenreName, p.SubGenreID, sg.GenreName AS SubGenreName, v.VideoProgramTypeID,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      vf.SizeInKB,
                      vf.VideoDimension                      
					FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
                      lt_Video_Tag ON vf.VideoID = lt_Video_Tag.VideoId ON v.ID = vf.VideoID LEFT OUTER JOIN
                      dt_VideoLog AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID

					WHERE     lt_Video_Tag.Tag IN
											  (SELECT     Keywords
												FROM          dt_Video
												WHERE      (ID = @Vid))


					AND			v.ID != @Vid
					
					AND     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR vf.VideoFileTypeID IS NUll)
					--AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
					AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
					AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
					AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
					AND			(v.StatusID = 1 OR v.StatusID = NULL)
					AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
					AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					AND			(
									v.Id in 
									(	
										SELECT	dt_Video.ID
										FROM    dt_Video INNER JOIN
											dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
											lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
											lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
										WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
											(dt_Video.StatusID = NULL)
									)
								)
					AND			(
									v.ProgramId in
									(
										SELECT	dt_Program.ID
										FROM	dt_Program INNER JOIN
											lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
											lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
											dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
										WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
											(dt_Program.StatusID = NULL)
									)
								)
				--SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows
				ORDER BY	StartDate DESC
			END
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Video_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery1.sql|7|0|C:\Users\desigan.royan\AppData\Local\Temp\~vs7360.sql
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Jacques du Preez
-- Date Modified: 2009-09-25
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Desigan Royan
-- Date Modified: 2010-04-15
-- =============================================
-- ==============Modified=======================
-- Author:		Desigan Royan
-- Date Modified: 2010-04-28
-- =============================================
-- ==============Modified=======================
-- Author:		Desmond Nzuza
-- Date Modified: 2010-July-14
-- Description: Added a method to remove 
--				extra "/CMS" charaters on 
--				v.ImageURL, v.BillboardImageURL
--Update: Removed the Replace() code as it was uneccesary
-- =============================================
-- ==============Modified=======================
-- Author:		Sameer Kulkarni
-- Date Modified: 2010-Sep-27
-- Description: Added new parameters:
--				Price, 
--              Geolocation, 
--              RuntimeInMinutes, 
--              RentalPeriodInHours,
--Update: Removed SizeinKB, VideoDimensions. Not
--		  required here. Sameer (30/9/2010)
-- =============================================
-- ==============Modified=======================
-- Author:		Chris de Kock
-- Date Modified: 2010-Oct-11
-- Description: 
-- Added new parameter: Keywords
-- Added "TOP 1000" to all selects to limit resultset
-- =============================================
-- ==============Modified=======================
-- Author:		Desigan Royan
-- Date Modified: 2010-Oct-11
-- Description: 
-- Updated Product Filter
-- Added "TOP 1000" to all selects to limit resultset
-- =============================================

CREATE PROCEDURE [dbo].[ws_dt_Video_Select] -- [dbo].[ws_dt_Video_Select] '	2W18EC9C-81D5-40E9-90A5-4EDAZB639DXQ', 0,0,'0',4,2,4,1,'','',0,2,0,0,0,'',1,1
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType varchar(max) = '0',
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@KeyWords	varchar(100) = '',
	@CountryCode	varchar(10) = '', 
	@ChannelId  int = 0,
	@ProductId	int = 0,
	@GenreID  int = 0,
	@SubGenreID INT = 0,
	@VideoCategoryID  int = 0,
	@ScheduleDate DateTime = NULL,
	@IsAuthenticated bit OUTPUT,
	@IsRestricted bit OUTPUT
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SiteId int, @ProgId int, @Vid int

	SET @IsAuthenticated = 0
	SET @IsRestricted = 0
	SET @ProgId = 0
	SET @Vid = 0
	
	IF EXISTS(
		SELECT ID
		FROM         dt_Website  WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website  WITH(NOLOCK)
		Where AuthenticationKey = @AuthenticationKey
		
		IF (CHARINDEX(',', @VideoId) = 0)
			BEGIN
				SET @ProgId = (SELECT ISNULL(ProgramID, 0) AS ProgramID  FROM  dt_Video WITH(NOLOCK) WHERE Id  = CAST(@VideoID AS INT))
				SET @ProgId = ISNULL(@ProgId, 0)
				SET @Vid = CAST(@VideoID AS INT)
			END

		--SELECT @ProgId AS ProGra, CHARINDEX(',', @VideoId) As Pos

		IF (@ProgId = 0)
			BEGIN
				IF (CHARINDEX(',', @ProgramId) = 0)
				BEGIN
					SET @ProgId = CAST(@ProgramId AS INT)
				END
			END
		IF (@ProgId > 0 AND LEN(@CountryCode) > 0)
		BEGIN
			IF NOT EXISTS(
				SELECT     TOP (1) gc.GeoIPCountryCode, gc.GeoGroupID, p.ID
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
									  [DStvUtilities].[dbo].lt_GeoGroup_GeoCountry AS gc WITH (NOLOCK) ON p.GeoGroupID = gc.GeoGroupID
				WHERE     (gc.GeoIPCountryCode = @CountryCode) 
				--AND (gc.GeoGroupID <> 2) 
				AND (p.ID = @ProgId)
			)
			BEGIN
				SET @IsRestricted = 1
			END
		END
	END

	IF (@IsAuthenticated = 1)
	--IF (@IsAuthenticated = 1 AND @IsRestricted = 0)
	BEGIN
		IF @ScheduleDate IS NULL SET @ScheduleDate = GetDate()
		
		--IF (@SortyBy = 1) -- Use this query for sort order 11 as well (Lyle)
		IF (@SortyBy IN (1, 11))
		BEGIN

			WITH Records AS
			(
				--SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), ''))) AS RowNum,
				--Changed the row number to be based on the program name if sort order is 11 (Lyle)			
				SELECT TOP 1000 ROW_NUMBER() OVER( ORDER BY (CASE @SortyBy WHEN 1 THEN LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), ''))  WHEN 11 THEN ProgramName END)) AS RowNum,
			     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, --vr.Rating, 
                      NULL AS ServerPath, null AS FileName, ar.Abbreviation AS AgeRestriction, 
                     ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName,
                       v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate,
                      v.Price,
					  v.Geolocation,
					  v.RuntimeInMinutes,
					  v.RentalPeriodInHours,
					  v.CastCrewID,
					  v.Keywords,
					  v.IBSProductID,
					  v.AssetId
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID  LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				WHERE     --(vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0) AND	
						(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				----AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records r
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			--ORDER BY	VideoTitle
			ORDER BY	(CASE @SortyBy WHEN 1 THEN VideoTitle WHEN 11 THEN ProgramName END) -- Cater for sort order 11 as well (Lyle)
		END

		IF (@SortyBy =2)
		BEGIN
			WITH Records AS
			(
				SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY v.StartDate DESC, v.VideoTitle) AS RowNum,
				     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL  AS Rating, 
                      NULL AS ServerPath, NULL AS FileName, v.StartDate, 
                      ar.Abbreviation AS AgeRestriction, ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName, v.VideoProgramTypeID, pp.ProductID AS Product, 
                      v.ExpiryDate,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      v.Keywords,
						v.IBSProductID,
						v.AssetId
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      --dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      --dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				--WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				WHERE			(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId,
			StartDate
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN
			WITH Records AS
			(
				SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
				     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      NULL AS ServerPath, NULL AS FileName, ar.Abbreviation AS AgeRestriction, 
                      ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName, v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      v.Keywords,
						v.IBSProductID,
						v.AssetId
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      --dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      --dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr WITH (NOLOCK) ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				--WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				WHERE			(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	Rating DESC
		
		END
		
		IF (@SortyBy =4)
		BEGIN
			WITH Records AS
			(
				SELECT Distinct TOP 1000 ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), ''))) AS RowNum,
			         v.ID AS Id, LTRIM(REPLACE(v.VideoTitle, SUBSTRING(v.VideoTitle, CHARINDEX('[', v.VideoTitle), CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, 
                      v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, NULL AS ServerPath, NULL 
                      AS FileName, vl.ViewCount, ar.Abbreviation AS AgeRestriction, ppGenre.ThemeID AS GenreID, ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName, 
                      v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate, v.Price, v.Geolocation, v.RuntimeInMinutes, v.RentalPeriodInHours, v.CastCrewID, v.Keywords, 
                      v.IBSProductID, v.AssetId, v.StartDate
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Video_ViewCount AS vl WITH (NOLOCK) ON v.ID = vl.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre AS ppGenre WITH (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				--WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				WHERE			(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
				AND 
						(
						vl.WebsiteID = @SiteId
						)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ViewCount DESC, StartDate DESC
		
		END
		
		IF (@SortyBy =5) --Related
		BEGIN
			IF @Vid > 0
			BEGIN
				Declare @tmpKeywords varchar(1000)
				SET @tmpKeywords = (SELECT Keywords FROM dt_Video WITH(NOLOCK) WHERE ID = @Vid);
				
				WITH Records AS
				(
					SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY v.StartDate) AS RowNum,
					     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, 
                      NULL AS ServerPath, NULL AS FileName, vl.ViewCount, 
                      ar.Abbreviation AS AgeRestriction, v.StartDate, ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName, v.VideoProgramTypeID, 
                      pp.ProductID AS Product, v.ExpiryDate,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      v.Keywords,
			v.IBSProductID,
			v.AssetId                     
					FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
                      lt_Video_Tag ON vf.VideoID = lt_Video_Tag.VideoId ON v.ID = vf.VideoID LEFT OUTER JOIN
                      dt_VideoLog AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID

					WHERE     lt_Video_Tag.Tag IN
											(Select Distinct value from dbo.Array(@tmpKeywords))
					AND			v.ID != @Vid
					AND			(pp.ProductID = @ProductId OR @ProductId = 0)
					AND     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
					AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
					AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
					AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
					AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
					AND			(v.StatusID = 1 OR v.StatusID = NULL)
					AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
					AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
					AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
				)
				
				SELECT		Distinct Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate, StartDate
				,Price,
				Geolocation,
				RuntimeInMinutes,
				RentalPeriodInHours,
				CastCrewID,
				Keywords,
			IBSProductID,
			AssetId
				FROM        Records
				WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
				ORDER BY	StartDate DESC
			END
		END
		
		IF (@SortyBy =6) -- Last Chance
		BEGIN

			WITH Records AS
			(
				SELECT  TOP 1000 ROW_NUMBER() OVER(ORDER BY v.ExpiryDate) AS RowNum,
				      v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, 
                      NULL AS ServerPath, NULL AS FileName, ar.Abbreviation AS AgeRestriction, 
                      ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName, v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      v.Keywords,
			v.IBSProductID,
			v.AssetId                     
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      --dt_VideoFile AS vf WITH (NOLOCK) LEFT OUTER JOIN
                      --dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      Video_Rating AS vr WITH (NOLOCK) ON v.ID = vr.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				--WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				WHERE			(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(GETDATE() BETWEEN p.StartDate AND p.EndDate)
				AND			(DATEDIFF(hh,v.ExpiryDate, @ScheduleDate +1) < 24 AND DATEDIFF(hh,v.ExpiryDate, @ScheduleDate +1) > 0) 
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ExpiryDate
		END

		IF (@SortyBy = 7) -- Ordinal
		BEGIN
			return null
			
		END
		
		IF (@SortyBy = 8)
		
		BEGIN
			--Test
			WITH Records AS
			(
				--SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), ''))) AS RowNum,
				--Row number should be based on Program Name (Lyle)
				SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY p.ProgramName) AS RowNum,
			     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, --vr.Rating, 
                      NULL AS ServerPath, null AS FileName, ar.Abbreviation AS AgeRestriction, 
                      ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName,
                       v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate,
                      v.Price,
					  v.Geolocation,
					  v.RuntimeInMinutes,
					  v.RentalPeriodInHours,
					  v.CastCrewID,
					  v.Keywords,
			v.IBSProductID,
			v.AssetId
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID  LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				WHERE     --(vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0) AND	
						(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			
				--Select * from dbo.dt_Video V
				AND V.ID in (
				Select VideoId from (
				select distinct V.ProgramId, 
				(select top 1 V.ID from dbo.dt_Video V where ProgramID = P.Id AND @ScheduleDate between Startdate AND expirydate Order by StartDate DESC, Season DESC,  Episode DESC) VideoId 
				from dbo.dt_Video V 
				join dbo.dt_Program P on P.ID = V.ProgramID
				where ProgramTypeID = 1 
				) ValidVideos
				Where VideoId is not null
				) 
			)		
						
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records r
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ProgramName
		END
		
		IF (@SortyBy = 9)
		BEGIN
			--Test
			WITH Records AS
			(
				SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), ''))) AS RowNum,
			     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL AS Rating, --vr.Rating, 
                      NULL AS ServerPath, null AS FileName, ar.Abbreviation AS AgeRestriction, 
                      ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName,
                       v.VideoProgramTypeID, pp.ProductID AS Product, v.ExpiryDate,
                      v.Price,
					  v.Geolocation,
					  v.RuntimeInMinutes,
					  v.RentalPeriodInHours,
					  v.CastCrewID,
					  v.Keywords,
					  v.StartDate,
						v.IBSProductID,
						v.AssetId
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID  LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
                      --dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
                      --dt_Genre AS sg WITH (NOLOCK) ON p.SubGenreID = g.ID
				WHERE     --(vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0) AND	
						(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(@ScheduleDate BETWEEN v.StartDate AND v.ExpiryDate)
				AND			(@ScheduleDate BETWEEN p.StartDate AND p.EndDate)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			


				--Select * from dbo.dt_Video V
				AND V.ID in (
				Select VideoId from (
				select distinct V.ProgramId, 
				(select top 1 V.ID from dbo.dt_Video V where ProgramID = P.Id AND @ScheduleDate between Startdate AND expirydate Order by StartDate DESC, Season DESC,  Episode DESC) VideoId 
				from dbo.dt_Video V 
				join dbo.dt_Program P on P.ID = V.ProgramID
				where ProgramTypeID = 1 
				) ValidVideos
				Where VideoId is not null
				) 
			)		
						
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate, StartDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records r
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	Startdate DESC
		END
		
		IF (@SortyBy = 10) -- FortComming
		BEGIN
			WITH Records AS
			(
				SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
				     v.ID AS Id, LTRIM(Replace(v.VideoTitle, SUBSTRING ( v.VideoTitle ,CHARINDEX('[', v.VideoTitle) , CHARINDEX(']', v.VideoTitle)), '')) AS VideoTitle, v.Abstract, v.ProgramID, p.ProgramName, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, NULL  AS Rating, 
                      NULL AS ServerPath, NULL AS FileName, v.StartDate, 
                      ar.Abbreviation AS AgeRestriction, ppGenre.ThemeID AS [GenreID], ppGenre.GenreName, ppGenre.SubGenreID, ppGenre.SubGenreName AS SubGenreName, v.VideoProgramTypeID, pp.ProductID AS Product, 
                      v.ExpiryDate,
                      v.Price,
                      v.Geolocation,
                      v.RuntimeInMinutes,
                      v.RentalPeriodInHours,
                      v.CastCrewID,
                      v.Keywords,
			v.IBSProductID,
			v.AssetId
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
                      dt_Video AS v WITH (NOLOCK) ON p.ID = v.ProgramID LEFT OUTER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      --dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      --dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID ON v.ID = vf.VideoID LEFT OUTER JOIN
                      dt_AgeRestriction AS ar WITH (NOLOCK) ON v.AgeRestrictionID = ar.ID LEFT OUTER JOIN
                      lt_Program_Genre as ppGenre with (NOLOCK) ON v.ProgramID = ppGenre.ProgramID
				--WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				WHERE			(pp.ProductID = @ProductId OR @ProductId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(v.StartDate between CONVERT(CHAR(10), DATEADD(DAY, 1, GETDATE()), 120) AND CONVERT(CHAR(10), DATEADD(DAY, 31, GETDATE()), 120))
				--AND			(p.StartDate BETWEEN GETDATE() AND GETDATE()+30)
				AND			(v.VideoProgramTypeID  in (select * from dbo.Array(@VideoProgramType)) OR (0 in (select * from dbo.Array(@VideoProgramType))))
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				--AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(ppGenre.ThemeId = @GenreID OR @GenreID = 0)
				AND			(ppGenre.subGenreID = @SubGenreID OR @SubGenreID = 0)
				AND			(
								v.ProgramId in
								(
									SELECT     p.ID
									FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
														  lt_Product_Program AS pp WITH (NOLOCK) ON wp.ProductID = pp.ProductID INNER JOIN
														  dt_Program AS p WITH (NOLOCK) ON pp.ProgramID = p.ID
									WHERE     (wp.WebsiteID = @SiteId)
									AND		(p.StatusID = 1 OR p.StatusID = NULL)
									AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
								)
							)
					AND			(
						v.ProgramId in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(
						v.ID in
						(
						SELECT  distinct   VideoID
						FROM         dt_VideoFile  WITH(NOLOCK)
						WHERE		(VideoFileTypeID = @VideoFileTypeId)
						)
						OR @VideoFileTypeId = 0
					)
				AND			(
								v.ID in
								(
								SELECT     VideoID
								FROM         lt_Video_VideoCategory WITH(NOLOCK)
								WHERE		(VideoCategoryID = @VideoCategoryID)
								)
								
								OR @VideoCategoryID = 0
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, ProgramName, Runtime, StartDate AS AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, AgeRestriction, GenreID, GenreName, SubGenreID, SubGenreName, VideoProgramTypeID, (SELECT COUNT(Id) FROM Records) AS TotalRows, Product, ExpiryDate
			,Price,
			Geolocation,
			RuntimeInMinutes,
			RentalPeriodInHours,
			CastCrewID,
			Keywords,
			IBSProductID,
			AssetId
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate
		END	
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Video_Search]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Video_Search] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 0,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@PrimaryLanguageId int = 0,
	@SecondaryLanguageId int = 0,
	@SubtitleLanguageId int = 0,
	@CountryId	int = 0,
	@GenreId	int = 0,
	@KeyWords	varchar(100)='',
	@DirectorId  int = 0,
	@IsAuthenticated bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;

	Declare @DateStamp datetime
	Declare @Interval int

	SET @DateStamp = (SELECT TOP 1 CreatedDate FROM tt_Video_Meta ORDER BY CreatedDate DESC)
 
	SET @DateStamp = ISNULL(@DateStamp, getdate()-1)

	SET @Interval = 15

	--SELECT CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) - @Interval

	IF (CAST(DATEDIFF(MINUTE, @DateStamp, GETDATE()) AS INT) > @Interval)
	BEGIN
		
		TRUNCATE TABLE tt_Video_Meta

		INSERT INTO	tt_Video_Meta
		(
		[ID]
      ,[VideoTitle]
      ,[Keywords]
      ,[Abstract]
      ,[Runtime]
      ,[AirDate]
      ,[Season]
      ,[Episode]
      ,[ImageURL]
      ,[ServerPath]
      ,[FileName]
      ,[Director]
      ,[PrimaryLanguage]
      ,[SecondaryLanguage]
      ,[SubtitleLanguage]
      ,[YearOfRelease]
      ,[AwardTextLong]
      ,[AwardTextShort]
      ,[Synopsis]
      ,[CountryName]
      ,[GenreName]
      ,[ProgramID]
      ,[ProgramTypeID]
      ,[VideoFileTypeID]
      ,[StatusID]
      ,[StartDate]
      ,[EndDate]
      ,[LanguageID]
      ,[SecondaryLanguageID]
      ,[SubtitleLanguageID]
      ,[CountryID]
      ,[GenreID]
      ,[SubGenreID]
      ,[SubGenreName]
      ,[DirectorId]
      ,[CreatedDate]
      ,[AgeRestrictionID]
      ,[AgeRestrictionAbbreviation]
      ,[AgeRestrictionDescription]
      ,[BillboardImageURL]
      ,[Price]
      ,[Geolocation]
      ,[RuntimeInMinutes]     
      ,[RentalPeriodInHours]
      ,[CastCrewID]
      ,[SizeInKB]
      ,[VideoDimension]
		)
		SELECT   v.ID as [ID]
				, LTRIM(v.VideoTitle) AS VideoTitle
				, v.Keywords AS Keywords
				, v.Abstract as Abstract
				, v.Runtime as Runtime
				, v.AirDate
				, v.Season
				, v.Episode
				, v.ImageURL
				, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) ELSE vfp.ServerPath END AS ServerPath
				, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + '/' + vf.[FileName]) ELSE vf.[FileName] END AS [FileName]
				,'' as [Director]
				, l3.LanguageName AS PrimaryLanguage
				, l2.LanguageName AS SecondaryLanguage
				, l1.LanguageName AS SubtitleLanguage
				, vm.YearOfRelease
				, vm.AwardTextLong
				, vm.AwardTextShort
				, vm.Synopsis
				, c.CountryName
				, g.GenreName
				, p.ID AS ProgramID
				, p.ProgramTypeID
				, vf.VideoFileTypeID
				, p.StatusID
				, v.StartDate
				, v.EndDate
				, vm.LanguageID
				, vm.SecondaryLanguageID
				, vm.SubtitleLanguageID
				, vm.CountryID
				, p.GenreID
				, p.SubGenreID
				, g2.GenreName AS SubGenreName
				, 0 as DirectorID
				,GETDATE() AS [CreatedDate]
				, v.AgeRestrictionID
				, ag.Abbreviation AS AgeRestrictionAbbreviation
				, ag.AgeRestrictionDescription
				, v.BillboardImageURL
				, v.Price
				, v.Geolocation
				, v.RuntimeInMinutes
				, v.RentalPeriodInHours
				, v.CastCrewID
				, vf.SizeInKB
				, vf.VideoDimension		
		FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID RIGHT OUTER JOIN
							  dt_VideoMeta AS vm WITH (NOLOCK) INNER JOIN
							  dt_Video AS v WITH (NOLOCK) ON vm.VideoID = v.ID INNER JOIN
							  dt_Program AS p WITH (NOLOCK) ON v.ProgramID = p.ID ON vf.VideoID = v.ID LEFT OUTER JOIN
							  dt_Genre AS g2 WITH (NOLOCK) ON p.SubGenreID = g2.ID INNER JOIN
							  dt_AgeRestriction AS ag WITH (NOLOCK) ON v.AgeRestrictionID = ag.ID LEFT OUTER JOIN
							  dt_Genre AS g WITH (NOLOCK) ON p.GenreID = g.ID LEFT OUTER JOIN
							  dt_Country AS c WITH (NOLOCK) ON vm.CountryID = c.ID LEFT OUTER JOIN
							  dt_Language AS l3 WITH (NOLOCK) ON vm.LanguageID = l3.ID LEFT OUTER JOIN
							  dt_Language AS l2 WITH (NOLOCK) ON vm.SecondaryLanguageID = l2.ID LEFT OUTER JOIN
							  dt_Language AS l1 WITH (NOLOCK) ON vm.SubtitleLanguageID = l1.ID
	END


	SET @IsAuthenticated = 0
	DECLARE @SiteId int
	SELECT @SiteId = ID
					FROM         dt_Website
					WHERE     (StatusID = 1) 
					AND (AuthenticationKey = @AuthenticationKey)
					AND (VideoAccess = 1)
				

	IF (@SiteId > 0)
	BEGIN
		SET @IsAuthenticated = 1
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@SortyBy =1)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
						     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.Director, v.PrimaryLanguage, 
							  v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, 
							  v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramId,
							 v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL
							 ,v.Price,
							  v.Geolocation, 
							  v.RuntimeInMinutes, 
							  v.RentalPeriodInHours,
							  v.CastCrewID,
							  v.SizeInKB, 
							  v.VideoDimension
						FROM         tt_Video_Meta AS v WITH (NOLOCK) LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID
						
						WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
						--AND			(CONTAINS(VideoTitle, @KeyWords) OR CONTAINS(keywords, @KeyWords) OR CONTAINS(Director, @KeyWords) OR @KeyWords = '')
						AND			(v.DirectorId =  @DirectorId OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
										
					)
					
					SELECT     Id, VideoTitle, Abstract, Runtime, AirDate, Season, Episode, ImageURL, FileName, ServerPath, Director, PrimaryLanguage, SecondaryLanguage, 
										  SubtitleLanguage, YearOfRelease, AwardTextLong, AwardTextShort, Synopsis, CountryName, GenreName, LanguageID, SecondaryLanguageID, 
										  SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription, BillboardImageURL, (SELECT     COUNT(Id) FROM          Records) AS TotalRows
										  ,Price,
										  Geolocation, 
										  RuntimeInMinutes, 
										  RentalPeriodInHours,
										  CastCrewID,
										  SizeInKB, 
										  VideoDimension
					FROM         Records
					WHERE     (RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY LTRIM(VideoTitle)

		END

		IF (@SortyBy =2)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
							 v.ID AS Id
							, v.VideoTitle
							, v.Abstract
							, v.Runtime
							, v.AirDate
							, v.Season
							, v.Episode
							, v.ImageURL
							, v.ServerPath
							, v.[FileName]
							, v.StartDate
							, v.Director
							, v.PrimaryLanguage
							, v.SecondaryLanguage
							, v.SubtitleLanguage
							, v.YearOfRelease
							, v.AwardTextLong
							, v.AwardTextShort
							, v.Synopsis
							, v.CountryName
							, v.GenreName
							, v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramId
							, v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription
							, v.BillboardImageURL
							,v.Price,
							  v.Geolocation, 
							  v.RuntimeInMinutes, 
							  v.RentalPeriodInHours,
							  v.CastCrewID,
							  v.SizeInKB, 
							  v.VideoDimension
						FROM        tt_Video_Meta AS v WITH (NOLOCK) LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID
						WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0  OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
						AND			(v.DirectorId =  @DirectorId OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					)
					
					SELECT		Id
								, VideoTitle
								, Abstract
								, Runtime
								, AirDate
								, Season
								, Episode
								, ImageURL
								, [FileName]
								, ServerPath
								, Director
								, PrimaryLanguage
								, SecondaryLanguage
								, SubtitleLanguage
								, YearOfRelease
								, AwardTextLong
								, AwardTextShort
								, Synopsis
								, CountryName
								, GenreName
								, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId 
								, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
								, BillboardImageURL
								,Price,
								  Geolocation, 
								  RuntimeInMinutes, 
								  RentalPeriodInHours,
								  CastCrewID,
								  SizeInKB, 
								  VideoDimension
								,(SELECT COUNT(Id) FROM Records) AS TotalRows, 1 as [Rating]
					FROM        Records
					WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN

					WITH Records AS
					(
						SELECT ROW_NUMBER() OVER(ORDER BY vr.Rating DESC) AS RowNum,
							 v.ID AS Id
							, v.VideoTitle
							, v.Abstract
							, v.Runtime
							, v.AirDate
							, v.Season
							, v.Episode
							, v.ImageURL
							, v.ServerPath
							, v.[FileName]
							, v.Director
							, v.PrimaryLanguage
							, v.SecondaryLanguage
							, v.SubtitleLanguage
							, v.YearOfRelease
							, v.AwardTextLong
							, v.AwardTextShort
							, v.Synopsis
							, v.CountryName
							, v.GenreName
							, v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, vr.Rating, v.ProgramId
							, v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription
							, v.BillboardImageURL
							,v.Price,
							  v.Geolocation, 
							  v.RuntimeInMinutes, 
							  v.RentalPeriodInHours,
							  v.CastCrewID,
							  v.SizeInKB, 
							  v.VideoDimension
						FROM        tt_Video_Meta AS v WITH (NOLOCK) LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID
						WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
						AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
						AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
						AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
						AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
						AND			(v.StatusID = 1 OR v.StatusID = NULL)
						AND			(v.GenreID = @GenreId OR @GenreId = 0)
						AND			(v.CountryID = @CountryId OR @CountryId = 0)
						AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
						AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
						AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
						--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '')
						AND			(v.DirectorId =  @DirectorId OR @DirectorId = 0) 
						AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
					)
					
					SELECT		Id
								, VideoTitle
								, Abstract
								, Runtime
								, AirDate
								, Season
								, Episode
								, ImageURL
								, ServerPath
								, [FileName]
								, Director
								, PrimaryLanguage
								, SecondaryLanguage
								, SubtitleLanguage
								, YearOfRelease
								, AwardTextLong
								, AwardTextShort
								, Synopsis
								, CountryName
								, GenreName
								, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId
								, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
								, BillboardImageURL
								,Price,
								  Geolocation, 
								  RuntimeInMinutes, 
								  RentalPeriodInHours,
								  CastCrewID,
								  SizeInKB, 
								  VideoDimension
								, (SELECT COUNT(Id) FROM Records) AS TotalRows
					FROM        Records
					WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
					ORDER BY	Rating DESC
		END
		IF (@SortyBy =4)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount DESC, v.VideoTitle DESC) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.ServerPath, v.FileName, v.Director, v.PrimaryLanguage, 
                      v.SecondaryLanguage, v.SubtitleLanguage, v.YearOfRelease, v.AwardTextLong, v.AwardTextShort, v.Synopsis, v.CountryName, v.GenreName, 
                      v.LanguageID, v.SecondaryLanguageID, v.SubtitleLanguageID, v.CountryID, v.GenreID, v.SubGenreID, v.SubGenreName, v.DirectorId, 
                      vl.ViewCount, vr.Rating, v.ProgramId, v.AgeRestrictionID, v.AgeRestrictionAbbreviation, v.AgeRestrictionDescription, v.BillboardImageURL
                      ,v.Price,
								  v.Geolocation, 
								  v.RuntimeInMinutes, 
								  v.RentalPeriodInHours,
								  v.CastCrewID,
								  v.SizeInKB, 
								  v.VideoDimension
				FROM         vw_TempVideoMeta AS v WITH (NOLOCK) LEFT OUTER JOIN
                      Video_ViewCount AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
											  Video_Rating AS vr ON v.ID = vr.VideoID
				WHERE     (v.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0 OR v.VideoFileTypeID IS NULL)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.ProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(v.GenreID = @GenreId OR @GenreId = 0)
				AND			(v.CountryID = @CountryId OR @CountryId = 0)
				AND			(v.LanguageID = @PrimaryLanguageId OR @PrimaryLanguageId = 0)
				AND			(v.SecondaryLanguageID = @SecondaryLanguageId OR @SecondaryLanguageId = 0)
				AND			(v.SubtitleLanguageID = @SubtitleLanguageId OR @SubtitleLanguageId = 0)
				AND			(vl.WebsiteID = @SiteId OR vl.WebsiteID is null)
				--AND			(FREETEXT(VideoTitle, @KeyWords) OR FREETEXT(keywords, @KeyWords) OR FREETEXT(Director, @KeyWords) OR @KeyWords = '') 
				AND			(v.DirectorId =  @DirectorId OR @DirectorId = 0) 
				AND			(VideoTitle LIKE '%'+@KeyWords+'%' OR keywords LIKE '%'+@KeyWords+'%' OR Synopsis LIKE '%'+@KeyWords+'%' OR Director LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

					)
					
				SELECT		Id
							, VideoTitle
							, Abstract
							, Runtime
							, AirDate
							, Season
							, Episode
							, ImageURL
							, ServerPath
							, [FileName]
							, Director
							, PrimaryLanguage
							, SecondaryLanguage
							, SubtitleLanguage
							, YearOfRelease
							, AwardTextLong
							, AwardTextShort
							, Synopsis
							, CountryName
							, GenreName
							, LanguageID, SecondaryLanguageID, SubtitleLanguageID, CountryID, GenreID, SubGenreID, SubGenreName, DirectorId, Rating, ProgramId
							, AgeRestrictionID, AgeRestrictionAbbreviation, AgeRestrictionDescription
							, BillboardImageURL
							,Price,
							  Geolocation, 
							  RuntimeInMinutes, 
							  RentalPeriodInHours,
							  CastCrewID,
							  SizeInKB, 
							  VideoDimension
							, (SELECT COUNT(Id) FROM Records) AS TotalRows, 1 as [Rating]
				FROM        Records
				WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
				ORDER BY	ViewCount DESC, VideoTitle DESC
				END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Program_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- Create date: 13/07/2009
-- Description:	
-- =============================================
-- =================Modified====================
-- Author:		Jacques du Preez
-- Create date: 2009-09-25
-- Description:	
-- =============================================
-- =================Modified====================
-- Author:		Chris de Kock
-- Create date: 2010-10-11
-- Description:	Added Parameter Keywords
-- =============================================
-- =================Modified====================
-- Author:		Sameer Kulkarni
-- Create date: 2010-11-02
-- Description:	Added 'ChannelID' Parameter
-- =============================================
-- =================Modified====================
-- Author:		Lyle Eckstein
-- Create date: 2010-11-02
-- Description:	Removed default for schedule start
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Program_Select] -- [ws_dt_Program_Select] '035C9082-4FAB-4595-8B00-8856ACB505F8', '0', 0, ,0, 0, 1, 1, 1, '', 0, 0
	-- Add the parameters for the stored procedure here
	@AuthenticationKey  varchar(255),
	@ProgramId varchar(max) = '0', 
	@ProgramType int = 0,
	@GenreId	int = 0,
	@SubGenreId	int = 0,
	--@CountryCode	char(10) = '', 
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@KeyWords	varchar(100) = '',
	@ChannelId  int = 0,
	@ProductId	int = 0,
	@ScheduleDate DateTime = NULL,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @SiteId int
	SET @IsAuthenticated = 0
	
	IF @ScheduleDate IS NULL SET @ScheduleDate = GetDate()
	
	IF EXISTS(
				SELECT ID
				FROM         dt_Website WITH(NOLOCK)
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
		SELECT	@SiteId = ID
		FROM	dt_Website
		Where AuthenticationKey = @AuthenticationKey
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		
		CREATE TABLE #ProgramIDList(ID int)
		DECLARE @spot SMALLINT, @str VARCHAR(10), @tempIDList VARCHAR(max)  
		SET @tempIDList = @ProgramID
		WHILE @ProgramID <> ''  
		BEGIN  
			SET @spot = CHARINDEX(',', @ProgramID)  
			IF @spot>0  
            BEGIN  
                SET @str = LEFT(@ProgramID, @spot-1)  
                SET @ProgramID = RIGHT(@ProgramID, LEN(@ProgramID)-@spot)  
            END  
			ELSE  
            BEGIN  
                SET @str = @ProgramID  
                SET @ProgramID = ''  
            END  
			INSERT INTO #ProgramIDList SELECT @str 
		END  

		IF (@SortyBy =1)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY p.ProgramName) AS RowNum,
						 p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, null AS Rating, pp.ProductID AS Product, p.Keywords
				FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
							lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
							 --LEFT OUTER JOIN dt_Video as v on v.ProgramID = p.ID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList ) OR @tempIDList = '0')
				AND			(p.ProgramTypeId = @ProgramType OR @ProgramType = 0)
				AND			(ppg.GenreID = @GenreId OR @GenreId = 0)
				AND			(ppg.SubGenreID = @SubGenreId OR @SubGenreId = 0)
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
									
				AND			(p.ProgramName LIKE '%'+@KeyWords+'%' OR p.Abstract LIKE '%'+@KeyWords+'%' OR p.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
				AND			(@ScheduleDate between p.StartDate and p.EndDate)
				
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating, Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ProgramName
		END

		IF (@SortyBy =2)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY p.CreatedDate DESC) AS RowNum,
				     p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, p.CreatedDate, pr.Rating, pp.ProductID AS Product, p.Keywords
				FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Program_Rating AS pr WITH (NOLOCK) ON p.ID = pr.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList) OR @tempIDList = '0')
				AND			(@ScheduleDate between p.StartDate and p.EndDate)
				AND			(p.ProgramTypeId = @ProgramType OR @ProgramType = 0)
				AND			(ppg.GenreID = @GenreId OR @GenreId = 0)
				AND			(ppg.SubGenreID = @SubGenreId OR @SubGenreId = 0)
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(p.ProgramName LIKE '%'+@KeyWords+'%' OR p.Abstract LIKE '%'+@KeyWords+'%' OR p.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(pr.WebsiteID = @SiteId OR pr.WebsiteID IS NULL)
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating, Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	CreatedDate DESC
		END

		IF (@SortyBy =3)
		BEGIN
			
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY pr.Rating DESC) AS RowNum,
				     p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, p.CreatedDate, pvc.ViewCount, pr.Rating, pp.ProductID AS Product, p.Keywords
FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
                      lt_Product_Program AS pp WITH (NOLOCK) ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Program_Video_Count AS pvc WITH (NOLOCK) ON p.ID = pvc.ProgramID LEFT OUTER JOIN
                      Program_Rating AS pr WITH (NOLOCK) ON p.ID = pr.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList) OR @tempIDList = '0')
				AND			(p.ProgramTypeId = @ProgramType OR @ProgramType = 0)
				AND			(ppg.GenreID = @GenreId OR @GenreId = 0)
				AND			(ppg.SubGenreID = @SubGenreId OR @SubGenreId = 0)
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(p.ProgramName LIKE '%'+@KeyWords+'%' OR p.Abstract LIKE '%'+@KeyWords+'%' OR p.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				AND			(pr.WebsiteID = @SiteId OR pr.WebsiteID IS NULL)
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
				AND			(@ScheduleDate between p.StartDate and p.EndDate)
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating, Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	Rating DESC, ProgramName

		END
		IF (@SortyBy =4)
		BEGIN
			
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY pvc.ViewCount DESC) AS RowNum,
				     p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, p.CreatedDate, pvc.ViewCount, pr.Rating, pp.ProductID AS Product, p.Keywords
				FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Program_Video_Count AS pvc WITH (NOLOCK) ON p.ID = pvc.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList) OR @tempIDList = '0')
				AND			(p.ProgramTypeId = @ProgramType OR @ProgramType = 0)
				AND			(ppg.GenreID = @GenreId OR @GenreId = 0)
				AND			(ppg.SubGenreID = @SubGenreId OR @SubGenreId = 0)
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(p.ProgramName LIKE '%'+@KeyWords+'%' OR p.Abstract LIKE '%'+@KeyWords+'%' OR p.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				--AND			(pr.WebsiteID = @SiteId OR pr.WebsiteID IS NULL)
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
				AND			(@ScheduleDate between p.StartDate and p.EndDate)
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating, Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	ViewCount DESC, ProgramName

		END
		IF (@SortyBy =5)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY p.StartDate DESC) AS RowNum,
				     p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, p.CreatedDate, pvc.ViewCount, NULL AS Rating, pp.ProductID AS Product, p.Keywords,
				     p.StartDate
				FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Program_Video_Count AS pvc WITH (NOLOCK) ON p.ID = pvc.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList) OR @tempIDList = '0')
				AND			(p.ProgramTypeId = @ProgramType OR @ProgramType = 0)
				AND			(ppg.GenreID = @GenreId OR @GenreId = 0)
				AND			(ppg.SubGenreID = @SubGenreId OR @SubGenreId = 0)
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(p.ProgramName LIKE '%'+@KeyWords+'%' OR p.Abstract LIKE '%'+@KeyWords+'%' OR p.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
				AND			(@ScheduleDate between p.StartDate and p.EndDate)
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating,  Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC, ProgramName
		END
		
		IF (@SortyBy =6)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY p.StartDate DESC) AS RowNum,
				     p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, p.CreatedDate, pvc.ViewCount, NULL AS Rating, pp.ProductID AS Product, p.Keywords, StartDate
				FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Program_Video_Count AS pvc WITH (NOLOCK) ON p.ID = pvc.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList) OR @tempIDList = '0')
				AND			(p.ProgramTypeId = @ProgramType OR @ProgramType = 0)
				AND			(ppg.GenreID = @GenreId OR @GenreId = 0)
				AND			(ppg.SubGenreID = @SubGenreId OR @SubGenreId = 0)
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(p.ProgramName LIKE '%'+@KeyWords+'%' OR p.Abstract LIKE '%'+@KeyWords+'%' OR p.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
				AND			(@ScheduleDate between p.StartDate and p.EndDate)
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating,  Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC, ProgramName
		END
		
		IF (@SortyBy = 10) -- This is intended to ignore Program dates
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY p.StartDate DESC) AS RowNum,
				     p.ID AS Id, p.ProgramName, p.Abstract, p.ImageURL, p.BillboardURL, p.WebsiteURL, p.CreatedDate, pvc.ViewCount, NULL AS Rating, pp.ProductID AS Product, p.Keywords, StartDate
				FROM         dt_Program AS p WITH (NOLOCK) LEFT OUTER JOIN
                      lt_Product_Program AS pp ON p.ID = pp.ProgramID LEFT OUTER JOIN
                      Program_Video_Count AS pvc WITH (NOLOCK) ON p.ID = pvc.ProgramID LEFT OUTER JOIN
                      lt_Program_Genre as ppg with (NOLOCK) ON p.ID = ppg.ProgramID
				WHERE		(p.Id  in (SELECT ID FROM #ProgramIDList) OR @tempIDList = '0')
				AND			(p.StatusID = 1 OR p.StatusID = NULL)
				AND			(
									p.Id in
									(
										
										SELECT     p.ID
										FROM         lt_Website_Product AS wp WITH (NOLOCK) INNER JOIN
															  lt_Product_Program AS pp ON wp.ProductID = pp.ProductID INNER JOIN
															  dt_Program AS p ON pp.ProgramID = p.ID
										WHERE     (wp.WebsiteID = @SiteId)
										AND		(p.StatusID = 1 OR p.StatusID = NULL)
										AND		(wp.ProductID = @ProductId OR @ProductId = 0) 
									)
								)
				AND			(
						p.ID in
						(
							SELECT     cp.ProgramID
							FROM         lt_Channel_Program AS cp WITH (NOLOCK)
							WHERE     (cp.ChannelID = @ChannelId)
							OR @ChannelId = 0
						)
					)
				AND			(pp.ProductId = @ProductId OR @ProductId = 0)
			)
			
			SELECT		Id, ProgramName, Abstract, ImageURL, BillboardURL, WebsiteURL, (SELECT COUNT(id) FROM Records) AS TotalRows, @PageSize AS PageSize, Rating,  Product, Keywords
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC, ProgramName
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Person_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jacques du Preez
-- alter date: 2009-07-28
-- Description:	Gets the person meta data
-- =============================================
-- =============================================
-- Author:		Chris de Kock
-- alter date: 2010-10-11
-- Description:	Added IsCast and IsCrew fields
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Person_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255), 
	@PersonID int = 0,
	@ProgramID int = 0,
	@ExclusionId int = 0,
	@ProfessionID int = 0,
	@IsAuthenticated bit OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET @IsAuthenticated = 0

	IF EXISTS(
				SELECT ID
				FROM         dt_Website
				WHERE     (StatusID = 1) 
				AND (AuthenticationKey = @AuthenticationKey)
	)
	BEGIN
		SET @IsAuthenticated = 1
	END

	IF (@IsAuthenticated = 1)
	BEGIN

		DECLARE @ArrIds varchar(255)
		
		SELECT @ArrIds = COALESCE(@ArrIds + ', ', '') + CAST(ProgramID AS varchar(10) )
		FROM  lt_Person_Profession_Program AS lpp INNER JOIN
              lt_Person_Profession AS pp ON lpp.PersonProfessionID = pp.Table_ID INNER JOIN
              dt_Person AS p ON p.ID = pp.PersonID
		WHERE     (p.ID = @PersonID)
		

		IF (@ProgramID > 0)
		BEGIN
			SELECT	 p.ID
					,p.[Fullname]
					,p.professionid
					,pr.[ProfessionName]
					,pr.[ID] AS [ProfessionID]
					,pr.[Rank]
					,p.[ImageURL]
					,CASE(ISNULL(p.[Gender],'m'))
						WHEN 'm' THEN 'Male'
						WHEN 'f' THEN 'Female'
						ELSE null
					END as [Gender]
					,p.[Birthdate]
					,p.[Birthplace]
					,p.[Bio] AS [Biography]
					,p.[AwardsText]
					,@ArrIds AS [ArrPrograms]
					, pr.IsCast
					, pr.IsCrew
			FROM	dt_Person AS p WITH(NOLOCK) INNER JOIN
					lt_Person_Profession AS pp WITH(NOLOCK) ON p.ID = pp.PersonID INNER JOIN
					lt_Person_Profession_Program AS lpp WITH(NOLOCK) ON pp.Table_ID = lpp.PersonProfessionID AND lpp.ProgramID = @ProgramID INNER JOIN
					dt_Profession AS pr WITH(NOLOCK) ON pp.ProfessionID = pr.ID 
					 --AND pp.ProfessionID = pr.ID
					WHERE (p.ID = @PersonID OR @PersonID = 0) AND (p.StatusID = 1)
					AND (p.ID <> @ExclusionId)
					AND (pr.ID = @ProfessionID OR @ProfessionID = 0)
					ORDER BY pr.[Rank]
		END
		ELSE
		BEGIN
			SELECT	DISTINCT p.ID
					,p.[Fullname]
					,pr.[ProfessionName]
					,pr.[ID] AS [ProfessionID]
					,pr.[Rank]
					,p.[ImageURL]
					,CASE(ISNULL(p.[Gender],'m'))
						WHEN 'm' THEN 'Male'
						WHEN 'f' THEN 'Female'
						ELSE null
					END as [Gender]
					,p.[Birthdate]
					,p.[Birthplace]
					,p.[Bio] AS [Biography]
					,p.[AwardsText]
					,@ArrIds AS [ArrPrograms]
					, pr.IsCast
					, pr.IsCrew
			FROM	dt_Person AS p WITH(NOLOCK) INNER JOIN
					lt_Person_Profession AS pp WITH(NOLOCK) ON p.ID = pp.PersonID INNER JOIN
					dt_Profession AS pr WITH(NOLOCK) ON pp.ProfessionID = pr.ID
					WHERE (p.ID = @PersonID OR @PersonID = 0) AND (p.StatusID = 1)
					AND (p.ID <> @ExclusionId)
					AND (pr.ID = @ProfessionID OR @ProfessionID = 0)
					ORDER BY pr.[Rank]
		END
	END

END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_DownloadLog_Insert]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan
-- alter date: 05/03/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_DownloadLog_Insert]
	@AuthenticationKey  varchar(255), 
	@VideoFileID int = 0,
	@UserID varchar(255) = '',
	@ManItemID varchar(50),
	@IsAuthenticated bit OUTPUT,
	@ReturnStatus tinyint OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @IsAuthenticated = 0
	SET @ReturnStatus = 0

	DECLARE @ItemId int, @WebsiteID int

	SET @WebsiteID =(
		SELECT ID
		FROM         dt_Website WITH(NOLOCK)
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
	)

	IF (@WebsiteID > 0)
	BEGIN
		
		SET @IsAuthenticated = 1
		
		SET @ItemId = @VideoFileID
		
		IF (@ItemId < 1 AND LEN(@ManItemID) > 0)
		BEGIN
			SET @ItemId = 
			(
				SELECT TOP (1) ID FROM dt_VideoFile WITH(NOLOCK) WHERE (ManItemId = @ManItemID)
			)
		END
		IF (@ItemId > 0)
		BEGIN
		-- Insert statements for procedure here
			INSERT INTO [dbo].[dt_DownloadLog]
				   ([UserID]
				   ,[VideoFileID]
				   ,[WebsiteID]
				   ,[DateDownload])
			 VALUES
				   (@UserID
				   ,@ItemId
				   ,@WebsiteID
				   ,GetDate())
				   END
				   
			SET @ReturnStatus = 1
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[ws_dt_Channels_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:      Desigan Royan
-- alter date: 2010-04-12
-- Description: Returns the Channels by website
-- =============================================

-- Edited:      Jodi Adams
-- alter date: 2010-09-11
-- Description: Alter to use only the lt_Channel_Program and DStvChannels_Channel_view objects
-- =============================================
CREATE PROCEDURE [dbo].[ws_dt_Channels_Select] 
	@AuthenticationKey varchar(255),
	@SortBy int = 1,
	@ProgramID int = 0,
	@IsAuthenticated bit OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @SiteId int
	SET @IsAuthenticated = 0
	
	
	SELECT	@SiteId = ID
	FROM	dt_Website  WITH(NOLOCK)
	Where AuthenticationKey = @AuthenticationKey
	AND (VideoAccess = 1)
	
	IF (@SiteId IS NOT NULL)
	BEGIN
		SET @IsAuthenticated = 1
		SELECT	@SiteId = ID
		FROM	dt_Website
		Where AuthenticationKey = @AuthenticationKey
		
		IF (@SortBy = 1)
			BEGIN
			SELECT DISTINCT cv.Id, cv.ChannelName, cv.ChannelNumber, LTRIM(cv.BigLogo) AS Logo
			FROM         lt_Channel_Program AS cp INNER JOIN
                      DStvChannels_Channel_view AS cv ON cp.ChannelID = cv.Id
                      
			WHERE     (cp.ProgramID = @ProgramID OR @ProgramID = 0)			
			ORDER BY ChannelName
		END
		
		IF (@SortBy = 2)
			BEGIN
			SELECT DISTINCT cv.Id, cv.ChannelName, cv.ChannelNumber, LTRIM(cv.BigLogo) AS Logo
			FROM         lt_Channel_Program AS cp INNER JOIN
                      DStvChannels_Channel_view AS cv ON cp.ChannelID = cv.Id
                      
			WHERE     (cp.ProgramID = @ProgramID OR @ProgramID = 0)
			ORDER BY ChannelNumber
		END
	
		
	END
	
END

/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_SelectAll_WebChannel]    Script Date: 04/12/2010 17:27:09 ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[ws_bo_GetClassificationItems]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chris de Kock
-- alter date: 28/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_bo_GetClassificationItems] 
	-- Add the parameters for the stored procedure here
	@EditorialListTitle varchar(8000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     dt_EditorialList.ID, dt_EditorialList.Title, dt_EditorialList.DisplayTitle, lt_EditorialListItems.ItemID
	FROM         dt_EditorialList
	INNER JOIN lt_EditorialListItems on lt_EditorialListItems.EditorialListID = dt_EditorialList.ID
	WHERE     (dt_EditorialList.Title = @EditorialListTitle)
	AND			(StatusID = 1)
	ORDER BY dt_EditorialList.CreatedDate
	
END
GO
/****** Object:  StoredProcedure [dbo].[ws_bo_GetCatalogueItemMedia]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chris de Kock
-- alter date: 28/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_bo_GetCatalogueItemMedia] 
	-- Add the parameters for the stored procedure here
	@EditorialListTitle varchar(8000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     dt_EditorialList.ID, dt_EditorialList.Title, dt_EditorialList.DisplayTitle, lt_EditorialListItems.ItemID
	FROM         dt_EditorialList
	INNER JOIN lt_EditorialListItems on lt_EditorialListItems.EditorialListID = dt_EditorialList.ID
	WHERE     (dt_EditorialList.Title = @EditorialListTitle)
	AND			(StatusID = 1)
	ORDER BY dt_EditorialList.CreatedDate
	
END
GO
/****** Object:  StoredProcedure [dbo].[ws_bo_GetCatalogueItemCastAndCrew]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Chris de Kock
-- alter date: 28/09/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ws_bo_GetCatalogueItemCastAndCrew] 
	-- Add the parameters for the stored procedure here
	@EditorialListTitle varchar(8000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     dt_EditorialList.ID, dt_EditorialList.Title, dt_EditorialList.DisplayTitle, lt_EditorialListItems.ItemID
	FROM         dt_EditorialList
	INNER JOIN lt_EditorialListItems on lt_EditorialListItems.EditorialListID = dt_EditorialList.ID
	WHERE     (dt_EditorialList.Title = @EditorialListTitle)
	AND			(StatusID = 1)
	ORDER BY dt_EditorialList.CreatedDate
	
END
GO
/****** Object:  View [dbo].[Video_Meta]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Video_Meta]
AS
SELECT     v.ID, v.VideoTitle, v.Abstract, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) 
                      THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) ELSE vfp.ServerPath END AS ServerPath, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) 
                      - (charindex('ondemand', vfp.serverpath) + 7)) + '/' + vf.[FileName]) ELSE vf.[FileName] END AS FileName, per.FullName AS Director, 
                      l3.LanguageName AS PrimaryLanguage, l2.LanguageName AS SecondaryLanguage, l1.LanguageName AS SubtitleLanguage, vm.YearOfRelease, 
                      vm.AwardTextLong, vm.AwardTextShort, vm.Synopsis, c.CountryName, pg.GenreName, p.ID AS ProgramID, p.ProgramTypeID, vf.VideoFileTypeID, 
                      p.StatusID, v.StartDate, v.EndDate, vm.LanguageID, vm.SecondaryLanguageID, vm.SubtitleLanguageID, vm.CountryID, pg.GenreID, pg.SubGenreID, 
                      pg.GenreName AS SubGenreName, per.ID AS DirectorId
FROM         dbo.dt_Program AS p INNER JOIN
                      dbo.dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
                      dbo.dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
                      dbo.dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID INNER JOIN
                      dbo.dt_VideoMeta AS vm ON v.ID = vm.VideoID ON p.ID = v.ProgramID LEFT OUTER JOIN
                      dbo.lt_Program_Genre AS pg ON pg.ProgramID = p.ID LEFT OUTER JOIN
                      dbo.lt_Person_Profession_Program AS pro ON pro.ProgramID = p.ID INNER JOIN
                      dbo.lt_Person_Profession AS ppro ON ppro.Table_ID = pro.PersonProfessionID INNER JOIN
                      dbo.dt_Person AS per ON per.ID = ppro.PersonID LEFT OUTER JOIN
                      dbo.dt_Country AS c ON vm.CountryID = c.ID LEFT OUTER JOIN
                      dbo.dt_Language AS l3 ON vm.LanguageID = l3.ID LEFT OUTER JOIN
                      dbo.dt_Language AS l2 ON vm.SecondaryLanguageID = l2.ID LEFT OUTER JOIN
                      dbo.dt_Language AS l1 ON vm.SubtitleLanguageID = l1.ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[31] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 310
               Left = 237
               Bottom = 425
               Right = 393
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "vf"
            Begin Extent = 
               Top = 6
               Left = 232
               Bottom = 121
               Right = 390
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "vfp"
            Begin Extent = 
               Top = 40
               Left = 447
               Bottom = 155
               Right = 619
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 638
               Bottom = 121
               Right = 820
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "vm"
            Begin Extent = 
               Top = 19
               Left = 894
               Bottom = 201
               Right = 1084
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g2"
            Begin Extent = 
               Top = 491
               Left = 517
               Bottom = 606
               Right = 670
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pro"
            Begin Extent = 
               Top = 145
               Left = 218
               Bottom = 230
               Right = 370
            End
            DisplayFlags = 280
            TopColumn = 0
    ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_Meta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     End
         Begin Table = "per"
            Begin Extent = 
               Top = 203
               Left = 447
               Bottom = 318
               Right = 599
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 365
               Left = 490
               Bottom = 480
               Right = 643
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 126
               Left = 609
               Bottom = 241
               Right = 783
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l3"
            Begin Extent = 
               Top = 130
               Left = 1127
               Bottom = 230
               Right = 1282
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l2"
            Begin Extent = 
               Top = 323
               Left = 615
               Bottom = 423
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "l1"
            Begin Extent = 
               Top = 261
               Left = 1117
               Bottom = 361
               Right = 1272
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 28
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 2490
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_Meta'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Video_Meta'
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Video_VideoCategory_Insert]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_lt_Video_VideoCategory_Insert]
	@VideoID int,
	@VideoCategoryID int
As
Begin
	Insert Into lt_Video_VideoCategory
		([VideoID],[VideoCategoryID])
	Values
		(@VideoID,@VideoCategoryID)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Video_VideoCategory_Delete]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mms_lt_Video_VideoCategory_Delete]
	@VideoID int

As
Begin

	BEGIN 
		DELETE from lt_Video_VideoCategory where VideoID = @VideoID
	END
End
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Program_Update]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Procedure Name: mms_lt_Person_Profession_Program_Update
Creation Date: 28Sep2010
Created By: Sameer Kulkarni
*/

CREATE procedure [dbo].[mms_lt_Person_Profession_Program_Update]
	@ID int,
	@PersonProfessionID int,
	@ProgramID int,
	@CharacterName varchar(200)
as
update dbo.lt_Person_Profession_Program
set
	PersonProfessionID = @PersonProfessionID,
	ProgramID = @ProgramID,
	CharacterName = @CharacterName
where
	ID = @ID
-- To Test:
-- exec mms_lt_Person_Profession_Program_Update 1, 1, 'Jack'
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Program_Read]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Procedure Name: mms_lt_Person_Profession_Program_Read
Creation Date: 28Sep2010
Created By: Sameer Kulkarni
*/

CREATE procedure [dbo].[mms_lt_Person_Profession_Program_Read]
	@ID int
as
select ID, PersonProfessionID, ProgramID, CharacterName from dbo.lt_Person_Profession_Program
where
	ID = @ID
	
-- To Test:
-- exec mms_lt_Person_Profession_Program_Read 1
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Program_Insert]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Procedure Name: mms_lt_Person_Profession_Program
Creation Date: 28Sep2010
Created By: Sameer Kulkarni
*/

CREATE procedure [dbo].[mms_lt_Person_Profession_Program_Insert]
	@PersonProfessionID int,
	@ProgramID int,
	@CharacterName varchar(200)
as
insert into dbo.lt_Person_Profession_Program(PersonProfessionID, ProgramID, CharacterName)
values
(
	@PersonProfessionID,
	@ProgramID,
	@CharacterName
)

-- To Test:
-- exec mms_lt_Person_Profession_Program 1, 1, 'Jack'
GO
/****** Object:  StoredProcedure [dbo].[mms_lt_Person_Profession_Program_Delete]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Procedure Name: mms_lt_Person_Profession_Program_Delete
Creation Date: 28Sep2010
Created By: Sameer Kulkarni
*/

CREATE procedure [dbo].[mms_lt_Person_Profession_Program_Delete]
	@ID int
as
delete from dbo.lt_Person_Profession_Program
where
	ID = @ID
-- To Test:
-- exec mms_lt_Person_Profession_Program_Delete 1
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoCategory_SelectAllByVideo]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	[mms_dt_Channel_SelectAllByProgram]
-- Author:	Desigan Royan - DStv Online
-- alter date:	12/04/2010 12:13:54 PM

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoCategory_SelectAllByVideo]
@VideoID int
As
Begin
SELECT     vc.ID, vc.CategoryName
FROM         dt_VideoCategory AS vc INNER JOIN
                      lt_Video_VideoCategory AS lvvc ON vc.ID = lvvc.VideoCategoryID
WHERE     (lvvc.VideoID = @VideoID)

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFile_Update]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFile_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:24:07 PM
-- Description:	This stored procedure is intended for updating dt_VideoFile table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFile_Update]
	@VideoID int,
	@FileName varchar(255),
	@VideoFilePathID smallint,
	@VideoFileTypeID tinyint,
	@ManItemId varchar(50),
	@ProductId int = 2,
	@VideoDimension nvarchar(15) = '',
	@SizeInKB numeric(18,0) = 0
AS
BEGIN

IF EXISTS(SELECT 1 FROM dt_VideoFile WHERE VideoID = @VideoID AND [VideoFileTypeID] = @VideoFileTypeID)
	UPDATE dt_VideoFile
	SET
		[FileName] = @FileName,
		[VideoFilePathID] = @VideoFilePathID,
		[ManItemId]  = @ManItemId,
		[ProductId] = @ProductId,
		[VideoDimension] = @VideoDimension,
		[SizeInKB] = @SizeInKB
	WHERE		
	[VideoID] = @VideoID AND [VideoFileTypeID] = @VideoFileTypeID
ELSE
	INSERT INTO dt_VideoFile
		([VideoID],[FileName],[VideoFilePathID],[VideoFileTypeID],[ManItemId], [ProductId], [VideoDimension], [SizeInKB])
	VALUES
		(@VideoID,@FileName,@VideoFilePathID,@VideoFileTypeID,@ManItemId, @ProductId, @VideoDimension, @SizeInKB)
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFile_SelectAll]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFile_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:24:07 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_VideoFile table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFile_SelectAll]
As
Begin
	Select 
		[ID],
		[VideoID],
		[FileName],
		[VideoFilePathID],
		[VideoFileTypeID],
		[CreatedDate],
		[VideoDimension],
		[SizeInKB]		
	From dt_VideoFile where [StatusID] = 1 or [StatusID] = NULL 
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFile_Insert]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFile_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:24:07 PM
-- Description:	This stored procedure is intended for inserting values to dt_VideoFile table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFile_Insert]
	@VideoID int,
	@FileName varchar(255),
	@VideoFilePathID smallint,
	@VideoFileTypeID tinyint,
	@ManItemID varchar(50),
	@ProductId int = 2,
	@VideoDimension nvarchar(15),
	@SizeInKB numeric(18,0)
As
Begin
	Insert Into dt_VideoFile
		([VideoID],[FileName],[VideoFilePathID],[VideoFileTypeID],[ManItemId],ProductId,[VideoDimension], [SizeInKB])
	Values
		(@VideoID,@FileName,@VideoFilePathID,@VideoFileTypeID,@ManItemID, @ProductId, @VideoDimension, @SizeInKB)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFile_DeleteRow]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFile_DeleteRow
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:24:07 PM
-- Description:	This stored procedure is intended for deleting a specific row from dt_VideoFile table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFile_DeleteRow]
	@ID int
As
Begin
	Update dt_VideoFile
	SET              StatusID = 4
	Where
		[ID] = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_VideoFile_delete]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_VideoFile_Update
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:24:07 PM
-- Description:	This stored procedure is intended for updating dt_VideoFile table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_VideoFile_delete]
	@VideoID int,
	@VideoFileTypeID tinyint
AS
BEGIN

IF EXISTS(SELECT 1 FROM dt_VideoFile WHERE VideoID = @VideoID AND [VideoFileTypeID] = @VideoFileTypeID)
	DELETE FROM dt_VideoFile
	WHERE [VideoID] = @VideoID AND [VideoFileTypeID] = @VideoFileTypeID
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Video_SelectAll]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_SelectAll
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:22:43 PM
-- Description:	This stored procedure is intended for selecting all rows from dt_Video table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Video_SelectAll]
AS
BEGIN
SELECT dt_Video.ID, dt_Video.VideoTitle, dt_Video.Abstract, dt_Video.ImageURL, dt_Video.BillboardImageURL, dt_Video.EPGID, dt_Video.ProgramID, dt_Video.VideoProgramTypeID, 
                      dt_Video.AgeRestrictionID, dt_Video.AirDate, dt_Video.Runtime, dt_Video.Season, dt_Video.Episode, dt_Video.StartDate, CONVERT(varchar, [dt_Video].[StartDate], 108) as StartTime, dt_Video.EndDate, CONVERT(varchar, [dt_Video].[EndDate], 108) as EndTime, dt_Video.Keywords, 
                      dt_Video.CreatedDate, dt_Program.ProgramName, dt_ProgramType.ProgramTypeName, (select dt_VideoFile.[FileName] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 4) AS VideoFileNameFLVLow,
                      (select top 1 dt_VideoFile.[FileName] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 3) AS VideoFileNameWMVHigh,
                      (select top 1 dt_VideoFile.[FileName] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 5) AS VideoFileNameWMVLow,
                      (select top 1 dt_VideoFile.[VideoFilePathID] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 4) AS VideoFileServerIDFLVLow,
                      (select top 1 dt_VideoFile.[VideoFilePathID] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 5) AS VideoFileServerIDWMVLow,
                      (select top 1 dt_VideoFile.[ManItemId] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 5) AS VideoManItemIdLowWMV,
                      (select top 1 dt_VideoFile.[VideoFilePathID] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 3) AS VideoFileServerIDWMVHigh,
                      (select top 1 dt_VideoFile.[ManItemId] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 3) AS VideoManItemIdHighWMV,
                      (select top 1 dt_VideoFile.[ManItemId] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 4) AS VideoManItemIdHighFLV,
                      -- Mobile Files
                      (select top 1 dt_VideoFile.[FileName] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 2) AS VideoFileNameMobileLow,
                      (select top 1 dt_VideoFile.[VideoFilePathID] from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 2) AS VideoFileServerIDMobileLow,
                      -- End Mobile Files
                      -- File_Product
                      (select top 1 ISNULL(dt_VideoFile.[ProductId], 0) from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 4) AS VideoProductIDFLVHigh,
                      (select top 1 ISNULL(dt_VideoFile.[ProductId], 0) from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 3) AS VideoProductIdHighWMV,
                      (select top 1 ISNULL(dt_VideoFile.[ProductId], 0) from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 5) AS VideoProductIdLowWMV,
                      (select top 1 ISNULL(dt_VideoFile.[ProductId], 0) from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 2) AS VideoProductIdMobileLow,
                      -- End File_Product
                      --start Size in KB
                       (select top 1 dt_VideoFile.SizeInKB from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 2) AS VideoSizeInKbMobileLow,
                      (select top 1 dt_VideoFile.SizeInKB from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 3) AS VideoSizeInKbWMVHigh,
                      (select top 1 dt_VideoFile.SizeInKB from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 4) AS VideoSizeInKbFLVHigh,
                      (select top 1 dt_VideoFile.SizeInKB from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 5) AS VideoSizeInKbWMVLow,
                      --End Size in KB
                      
                       --start Dimension
                       (select top 1 dt_VideoFile.VideoDimension from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 2) AS VideoDimensionMobileLow,
                      (select top 1 dt_VideoFile.VideoDimension from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 3) AS VideoDimensionWMVHigh,
                      (select top 1 dt_VideoFile.VideoDimension from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 4) AS VideoDimensionFLVHigh,
                      (select top 1 dt_VideoFile.VideoDimension from dt_VideoFile where VideoID = dt_Video.ID and VideoFileTypeID = 5) AS VideoDimensionWMVLow
                      --End Dimension
                      ,dt_Video.ExpiryDate, dt_Video.RuntimeInMinutes, dt_Video.RentalPeriodInHours, dt_Video.Price, dt_Video.Geolocation, dt_Video.CastCrewID, dt_Video.IBSProductID, dt_Video.AssetId, dt_Video.GeoGroupID
FROM         dt_Video INNER JOIN
                      dt_ProgramType ON dt_Video.VideoProgramTypeID = dt_ProgramType.ID INNER JOIN
                      dt_Program ON dt_Video.ProgramID = dt_Program.ID
WHERE                (dt_Video.StatusID = 1) OR
                     (dt_Video.StatusID = NULL)
END
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Person_Update]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================
-- Entity Name:	mms_dt_Program_Person_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for inserting values to dt_Program_Person table
-- ===============================================================================================
--ALTER Procedure [dbo].[mms_dt_Program_Person_Update]
--	@ProgramID int,
--	@PersonID int
--As
--Begin
--	BEGIN
--	Insert Into lt_Program_Person
--		([ProgramID],[PersonID])
--	Values
--		(@ProgramID,@PersonID)
--	END
--End

CREATE Procedure [dbo].[mms_dt_Program_Person_Update]
	@ProgramID int,
	@PersonProfessionID int
As
Begin
	BEGIN
	Insert Into lt_Person_Profession_Program
		([ProgramID],[PersonProfessionID])
	Values
		(@ProgramID,@PersonProfessionID)
	END
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Person_Insert]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================
-- Entity Name:	mms_dt_Program_Person_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for inserting values to dt_Program_Person table
-- ===============================================================================================

CREATE Procedure [dbo].[mms_dt_Program_Person_Insert]
	@ProgramID int,
	@PersonProfessionID int
As
Begin
	Insert Into lt_Person_Profession_Program
		([ProgramID],[PersonProfessionID])
	Values
		(@ProgramID,@PersonProfessionID)

	Declare @ReferenceID int
	Select @ReferenceID = @@IDENTITY

	Return @ReferenceID
End


--ALTER Procedure [dbo].[mms_dt_Program_Person_Insert]
--	@ProgramID int,
--	@PersonID int
--As
--Begin
--	Insert Into lt_Program_Person
--		([ProgramID],[PersonID])
--	Values
--		(@ProgramID,@PersonID)

--	Declare @ReferenceID int
--	Select @ReferenceID = @@IDENTITY

--	Return @ReferenceID
--End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Program_Person_Delete]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================
-- Entity Name:	mms_dt_Program_Person_Insert
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:17:50 PM
-- Description:	This stored procedure is intended for inserting values to dt_Program_Person table
-- ===============================================================================================

CREATE Procedure [dbo].[mms_dt_Program_Person_Delete]
	@ProgramID int

As
Begin

	BEGIN 
	Delete from lt_Person_Profession_Program where ProgramID = @ProgramID
	END
End

-- exec [mms_dt_Program_Person_Delete] 294
--ALTER Procedure [dbo].[mms_dt_Program_Person_Delete]
--	@ProgramID int

--As
--Begin

--	BEGIN 
--	Delete from lt_Program_Person where ProgramID = @ProgramID
--	END
--End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Person_SelectAllByProgram]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Person_SelectAllByProgram
-- Author:	Danie Joubert - DStv Online
-- alter date:	7/10/2009 12:13:54 PM

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Person_SelectAllByProgram]
@ProgramID int = 0
As
Begin
--SELECT     dt_Person.ID, dt_Person.FullName, dt_Person.ProfessionID, dt_Person.CreatedDate, dt_Profession.ProfessionName, dt_Person.Bio, dt_Person.Gender, 
--                      dt_Person.BirthDate, dt_Person.BirthPlace, dt_Person.ImageURL, dt_Person.AwardsText
--FROM         dt_Person INNER JOIN
--                      dt_Profession ON dt_Person.ProfessionID = dt_Profession.ID INNER JOIN
--                     lt_Program_Person ON dt_Person.ID = lt_Program_Person.PersonID
--WHERE     ((dt_Person.StatusID = 1) OR
--                      (dt_Person.StatusID = NULL)) AND lt_Program_Person.ProgramID = @ProgramID

SELECT     pp.Table_ID as ID, p.FullName + ' - ' + pf.ProfessionName AS 'PersonProfession'
FROM         lt_Person_Profession_Program AS ppp INNER JOIN
                      lt_Person_Profession AS pp ON ppp.PersonProfessionID = pp.Table_ID INNER JOIN
                      dt_Profession AS pf ON pp.ProfessionID = pf.ID INNER JOIN
                      dt_Person AS p ON pp.PersonID = p.ID
where ppp.ProgramID = @ProgramID
or @ProgramID     = 0

End
--exec [mms_dt_Person_SelectAllByProgram] 294
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialListItem_Update]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list item table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialListItem_Update]
	@ID int,
	@EditorialListID int,
	@ItemID int,--Id of the video
	@ItemTypeId smallint = 1,
	@ItemRank int--Order of Items
As
Begin
	Update lt_EditorialListItems
	Set
		[EditorialListID] = @EditorialListID, ItemID = @ItemID, ItemTypeId = @ItemTypeId, ItemRank = @ItemRank , CreatedDate = GETDATE()
		
		Where ID = @ID

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialListItem_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialListItem_Select]	
@Search varchar(255) = '',
@EditorialListID int = 0
As
Begin
SELECT     lt_EditorialListItems.ID, lt_EditorialListItems.EditorialListID, lt_EditorialListItems.ItemID, lt_EditorialListItems.ItemTypeId, lt_EditorialListItems.ItemRank, 
                      lt_EditorialListItems.CreatedDate, dt_EditorialList.Title, dt_Video.VideoTitle
FROM         lt_EditorialListItems INNER JOIN
                      dt_EditorialList ON lt_EditorialListItems.EditorialListID = dt_EditorialList.ID INNER JOIN
                      dt_Video ON lt_EditorialListItems.ItemID = dt_Video.ID
                      
                      Where  (EditorialListID = @EditorialListID OR @EditorialListID = 0) AND (Title like '%'+ @Search +'%' OR ItemID like '%'+ @Search +'%' OR dt_Video.VideoTitle like '%'+ @Search +'%' OR @Search = '' )
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialListItem_Insert]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_Video_Insert
-- Author:	Desmod Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to add a record to the editorial list table
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialListItem_Insert]
	@EditorialListID int,
	@ItemID int,--Id of the video
	@ItemTypeId smallint = 1,
	@ItemRank int--Order of Items	
As
Begin
	Insert Into lt_EditorialListItems
		(EditorialListID, ItemID, ItemTypeId, ItemRank, CreatedDate)
	Values
		(@EditorialListID, @ItemID, @ItemTypeId, @ItemRank, GETDATE())

End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_EditorialListItem_Delete]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	mms_dt_EditorialList_DeleteRow
-- Author:	Desmond Nzuza - DStv Online
-- alter date:	2010 - Sep - 16
-- Description:	Used to delte a record on the editorial list Items table 
-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_EditorialListItem_Delete]
	@ID int
As
Begin
	DELETE     
	FROM         lt_EditorialListItems
	WHERE ID = @ID
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_SelectAllByProgram]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	[mms_dt_Channel_SelectAllByProgram]
-- Author:	Desigan Royan - DStv Online
-- alter date:	12/04/2010 12:13:54 PM

-- ==========================================================================================
CREATE Procedure [dbo].[mms_dt_Channel_SelectAllByProgram]
@ProgramID int
As
Begin
SELECT     c.ChannelName, c.Id
FROM         lt_Channel_Program AS cp INNER JOIN
                      dt_Program AS p ON cp.ProgramID = p.ID INNER JOIN
                      DStvChannels_Channel_view AS c ON cp.ChannelID = c.Id
WHERE     (cp.ProgramID = @ProgramID)
End
GO
/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_SelectAll_WebChannel]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[mms_dt_Channel_SelectAll_WebChannel]
	
AS
BEGIN
	SELECT     ID, ChannelName, ChannelNumber, BigLogo--, Genre AS GenreId, ChannelType
	FROM         DStvChannels_Channel_view 
	where		ChannelType = 4
	Order BY ChannelName
END





/****** Object:  StoredProcedure [dbo].[mms_dt_Channel_SelectAllByProgram]    Script Date: 04/12/2010 17:31:51 ******/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[Delete_ws_dt_Video_Select]    Script Date: 04/29/2011 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Desigan Royan
-- alter date: 07/14/2009
-- Description:	
-- =============================================
-- =============================================
-- ==============Modified=======================
-- Author:		Jacques du Preez
-- Date Modified: 2009-09-25
-- =============================================

CREATE PROCEDURE [dbo].[Delete_ws_dt_Video_Select] 
	-- Add the parameters for the stored procedure here
	@AuthenticationKey varchar(255),
	@ProgramId varchar(max) = '0', 
	@VideoId varchar(max) = '0',
	@VideoProgramType smallint = 1,
	@VideoFileTypeId int = 0,
	@SortyBy	int = 1,
	@PageSize	int = 1,
	@PageNumber int = 1,
	@KeyWords	varchar(100) = '',
	@CountryCode	varchar(10) = '', 
	@IsAuthenticated bit OUTPUT
	--,@UserAccess bit OUTPUT
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SiteId int, @ProgId int
	SET @IsAuthenticated = 0
	SET @ProgId = 0

	IF EXISTS(
		SELECT ID
		FROM         dt_Website
		WHERE     (StatusID = 1) 
		AND (AuthenticationKey = @AuthenticationKey)
		AND (VideoAccess = 1)
	)
	BEGIN
		SET @IsAuthenticated = 1

		SELECT	@SiteId = ID
		FROM	dt_Website
		Where AuthenticationKey = @AuthenticationKey
		
		IF (CHARINDEX(',', @VideoId) = 0)
			BEGIN
				SET @ProgId = (SELECT ProgramID  FROM  dt_Video WHERE	Id  = CAST(@VideoID AS INT))
			END
		IF (@ProgId = 0)
			BEGIN
				IF (CHARINDEX(',', @ProgramId) = 0)
				BEGIN
					SET @ProgId = CAST(@ProgramId AS INT)
				END
			END
			
		IF (@ProgId > 0)
		BEGIN
			IF EXISTS(
				SELECT     TOP (1) gc.GeoIPCountryCode, gc.GeoGroupID, p.ID
				FROM         dt_Program AS p WITH (NOLOCK) INNER JOIN
									  [DStvUtilities].[dbo].lt_GeoGroup_GeoCountry AS gc WITH (NOLOCK) ON p.GeoGroupID = gc.GeoGroupID
				WHERE     (gc.GeoIPCountryCode = @CountryCode) 
				AND (gc.GeoGroupID <> 2) 
				AND (p.ID = @ProgId)
			)
			BEGIN
				SET @IsAuthenticated = 0
			END
		END
	END


	IF (@IsAuthenticated = 1)
	BEGIN
		IF (@SortyBy =1)
		BEGIN

			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
					 v.ID AS Id
					, v.VideoTitle
					, v.Abstract
					, v.ProgramID
					, v.Runtime
					, v.AirDate
					, v.Season
					, v.Episode
					, v.ImageURL
					, v.BillboardImageURL
					, vr.Rating
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9))
						else vfp.ServerPath
					end as ServerPath
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7) ) +  vf.[FileName])
						else vf.[FileName]
					end AS [FileName]
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							 dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
							 dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
							 Video_Rating AS vr ON v.ID = vr.VideoID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (select * from dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (select * from dbo.Array(@ProgramId)) or (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 

				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	VideoTitle
		END

		IF (@SortyBy =2)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.StartDate DESC) AS RowNum,
					 v.ID AS Id
					, v.VideoTitle
					, v.Abstract
					, v.ProgramID
					, v.Runtime
					, v.AirDate
					, v.Season
					, v.Episode
					, v.ImageURL
					, v.BillboardImageURL
					, vr.Rating
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9))
						else vfp.ServerPath
					end as ServerPath
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7) ) +  vf.[FileName])
						else vf.[FileName]
					end AS [FileName]
					, v.StartDate
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							 dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
							 dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
							 Video_Rating AS vr ON v.ID = vr.VideoID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, Runtime, AirDate, Season, Episode, ImageURL, [FileName], ServerPath, BillboardImageURL, Rating, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	StartDate DESC
		END	

		IF (@SortyBy =3)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY v.VideoTitle) AS RowNum,
					 v.ID AS Id
					, v.VideoTitle
					, v.Abstract
					, v.ProgramID
					, v.Runtime
					, v.AirDate
					, v.Season
					, v.Episode
					, v.ImageURL
					, v.BillboardImageURL
					, vr.Rating
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9))
						else vfp.ServerPath
					end as ServerPath
					, case
						when(charindex('rtmp', vfp.ServerPath) > 0) then (substring(vfp.ServerPath, charindex('ondemand', vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7) ) +  vf.[FileName])
						else vf.[FileName]
					end AS [FileName]
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
							 dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
							 dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
							 Video_Rating AS vr ON v.ID = vr.VideoID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	VideoTitle
		
		END
	IF (@SortyBy =4)
		BEGIN
			WITH Records AS
			(
				SELECT ROW_NUMBER() OVER(ORDER BY vl.ViewCount) AS RowNum,
				     v.ID AS Id, v.VideoTitle, v.Abstract, v.ProgramID, v.Runtime, v.AirDate, v.Season, v.Episode, v.ImageURL, v.BillboardImageURL, vr.Rating, 
                      CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, 0, charindex('ondemand', vfp.serverpath) + 9)) 
                      ELSE vfp.ServerPath END AS ServerPath, CASE WHEN (charindex('rtmp', vfp.ServerPath) > 0) THEN (substring(vfp.ServerPath, charindex('ondemand', 
                      vfp.serverpath) + 9, len(vfp.ServerPath) - (charindex('ondemand', vfp.serverpath) + 7)) + vf.[FileName]) ELSE vf.[FileName] END AS FileName, 
                      vl.ViewCount
				FROM         dt_VideoFile AS vf WITH (NOLOCK) INNER JOIN
									  dt_VideoFilePath AS vfp WITH (NOLOCK) ON vf.VideoFilePathID = vfp.ID INNER JOIN
									  dt_Video AS v WITH (NOLOCK) ON vf.VideoID = v.ID LEFT OUTER JOIN
									  dt_VideoLog AS vl ON v.ID = vl.VideoID LEFT OUTER JOIN
									  Video_Rating AS vr ON v.ID = vr.VideoID
				WHERE     (vf.VideoFileTypeID = @VideoFileTypeId OR @VideoFileTypeId = 0)
				AND			(v.Id  in (SELECT * FROM dbo.Array(@VideoID)) OR (0 in (select * from dbo.Array(@VideoID))))
				AND			(v.ProgramId  in (SELECT * FROM dbo.Array(@ProgramID)) OR (0 in (select * from dbo.Array(@ProgramId))))
				AND			(GETDATE() BETWEEN v.StartDate AND v.EndDate)
				AND			(v.VideoProgramTypeID = @VideoProgramType OR @VideoProgramType = 0)
				AND			(v.StatusID = 1 OR v.StatusID = NULL)
				AND			(vr.WebsiteID = @SiteId OR vr.WebsiteID IS NULL)
				AND			(v.VideoTitle LIKE '%'+@KeyWords+'%' OR v.Abstract LIKE '%'+@KeyWords+'%' OR v.Keywords LIKE '%'+@KeyWords+'%' OR @KeyWords = '') 
				
				AND			(
								v.Id in 
								(	
									SELECT	dt_Video.ID
									FROM    dt_Video INNER JOIN
										dt_Program ON dt_Video.ProgramID = dt_Program.ID INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID
									WHERE     (lt_Website_Channel.WebsiteID = @SiteID) AND (dt_Video.StatusID = 1) OR
										(dt_Video.StatusID = NULL)
								)
							)
				AND			(
								v.ProgramId in
								(
									SELECT	dt_Program.ID
									FROM	dt_Program INNER JOIN
										lt_Channel_Program ON dt_Program.ID = lt_Channel_Program.ProgramID INNER JOIN
										lt_Website_Channel ON lt_Channel_Program.ChannelID = lt_Website_Channel.ChannelID INNER JOIN
										dt_Website ON lt_Website_Channel.WebsiteID = dt_Website.ID
									WHERE     (dt_Website.ID = @SiteID) AND (dt_Program.StatusID = 1) OR
										(dt_Program.StatusID = NULL)
								)
							)
			)
			
			SELECT		Id, VideoTitle, Abstract, ProgramID, Runtime, AirDate, Season, Episode, ImageURL, ServerPath, [FileName], BillboardImageURL, Rating, (SELECT COUNT(Id) FROM Records) AS TotalRows
			FROM        Records
			WHERE		(RowNum BETWEEN (@PageNumber - 1) * @PageSize + 1 AND @PageNumber * @PageSize)
			ORDER BY	VideoTitle
		
		END
	END
END
GO
/****** Object:  Default [DF_dt_Website_StatusID]    Script Date: 04/29/2011 16:58:27 ******/
ALTER TABLE [dbo].[dt_Website] ADD  CONSTRAINT [DF_dt_Website_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_VideoMeta_CreatedDate]    Script Date: 04/29/2011 16:58:27 ******/
ALTER TABLE [dbo].[dt_VideoMeta] ADD  CONSTRAINT [DF_dt_VideoMeta_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_VideoMeta_StatusID]    Script Date: 04/29/2011 16:58:27 ******/
ALTER TABLE [dbo].[dt_VideoMeta] ADD  CONSTRAINT [DF_dt_VideoMeta_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_VideoLog_LastViewed]    Script Date: 04/29/2011 16:58:27 ******/
ALTER TABLE [dbo].[dt_VideoLog] ADD  CONSTRAINT [DF_dt_VideoLog_LastViewed]  DEFAULT (getdate()) FOR [LastViewed]
GO
/****** Object:  Default [DF_dt_VideoLog_CreatedDate]    Script Date: 04/29/2011 16:58:27 ******/
ALTER TABLE [dbo].[dt_VideoLog] ADD  CONSTRAINT [DF_dt_VideoLog_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_VideoLog_StatusID]    Script Date: 04/29/2011 16:58:27 ******/
ALTER TABLE [dbo].[dt_VideoLog] ADD  CONSTRAINT [DF_dt_VideoLog_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_VideoFileType_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_VideoFileType] ADD  CONSTRAINT [DF_dt_VideoFileType_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_VideoFilePath_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_VideoFilePath] ADD  CONSTRAINT [DF_dt_VideoFilePath_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_VideoFilePath_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_VideoFilePath] ADD  CONSTRAINT [DF_dt_VideoFilePath_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_DownloadLog_DateDownload]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_DownloadLog] ADD  CONSTRAINT [DF_dt_DownloadLog_DateDownload]  DEFAULT (getdate()) FOR [DateDownload]
GO
/****** Object:  Default [DF_dt_Country_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Country] ADD  CONSTRAINT [DF_dt_Country_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_AgeRestriction_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_AgeRestriction] ADD  CONSTRAINT [DF_dt_AgeRestriction_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_VideoCategory_CategoryParentID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_VideoCategory] ADD  CONSTRAINT [DF_dt_VideoCategory_CategoryParentID]  DEFAULT ((0)) FOR [CategoryParentID]
GO
/****** Object:  Default [DF_dt_VideoCategory_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_VideoCategory] ADD  CONSTRAINT [DF_dt_VideoCategory_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_VideoCategory_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_VideoCategory] ADD  CONSTRAINT [DF_dt_VideoCategory_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_Rating_RateCount]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Rating] ADD  CONSTRAINT [DF_dt_Rating_RateCount]  DEFAULT ((1)) FOR [RateCount]
GO
/****** Object:  Default [DF_dt_Rating_DateCreated]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Rating] ADD  CONSTRAINT [DF_dt_Rating_DateCreated]  DEFAULT (getdate()) FOR [LastModified]
GO
/****** Object:  Default [DF_dt_Rating_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Rating] ADD  CONSTRAINT [DF_dt_Rating_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_ProgrammingType_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_ProgramType] ADD  CONSTRAINT [DF_dt_ProgrammingType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_ProgrammingType_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_ProgramType] ADD  CONSTRAINT [DF_dt_ProgrammingType_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_Program_Rating_WebsiteID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program_Rating] ADD  CONSTRAINT [DF_dt_Program_Rating_WebsiteID]  DEFAULT ((1)) FOR [WebsiteID]
GO
/****** Object:  Default [DF_dt_Program_Rating_ProgramID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program_Rating] ADD  CONSTRAINT [DF_dt_Program_Rating_ProgramID]  DEFAULT ((1)) FOR [ProgramID]
GO
/****** Object:  Default [DF_dt_Program_Rating_RateCount]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program_Rating] ADD  CONSTRAINT [DF_dt_Program_Rating_RateCount]  DEFAULT ((1)) FOR [RateCount]
GO
/****** Object:  Default [DF_dt_Program_Rating_DateCreated]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program_Rating] ADD  CONSTRAINT [DF_dt_Program_Rating_DateCreated]  DEFAULT (getdate()) FOR [LastModified]
GO
/****** Object:  Default [DF_dt_Program_Rating_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program_Rating] ADD  CONSTRAINT [DF_dt_Program_Rating_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_Program_OldType]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program] ADD  CONSTRAINT [DF_dt_Program_OldType]  DEFAULT ((1)) FOR [ProgramTypeID]
GO
/****** Object:  Default [DF_dt_Program_GenreID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program] ADD  CONSTRAINT [DF_dt_Program_GenreID]  DEFAULT ((0)) FOR [GenreID]
GO
/****** Object:  Default [DF_dt_Program_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program] ADD  CONSTRAINT [DF_dt_Program_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_Program_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Program] ADD  CONSTRAINT [DF_dt_Program_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_Profession_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Profession] ADD  CONSTRAINT [DF_dt_Profession_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_Profession_Rank]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Profession] ADD  CONSTRAINT [DF_dt_Profession_Rank]  DEFAULT ((1)) FOR [Rank]
GO
/****** Object:  Default [DF_dt_Profession_IsCast]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Profession] ADD  CONSTRAINT [DF_dt_Profession_IsCast]  DEFAULT ((0)) FOR [IsCast]
GO
/****** Object:  Default [DF_dt_Profession_IsCrew]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Profession] ADD  CONSTRAINT [DF_dt_Profession_IsCrew]  DEFAULT ((0)) FOR [IsCrew]
GO
/****** Object:  Default [DF_dt_Product_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Product] ADD  CONSTRAINT [DF_dt_Product_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_Person_ProfessionID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Person] ADD  CONSTRAINT [DF_dt_Person_ProfessionID]  DEFAULT ((1)) FOR [ProfessionID]
GO
/****** Object:  Default [DF_dt_Person_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Person] ADD  CONSTRAINT [DF_dt_Person_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_Person_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Person] ADD  CONSTRAINT [DF_dt_Person_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_Language_StatusID]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_Language] ADD  CONSTRAINT [DF_dt_Language_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_ErrorLog_Resolved]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_ErrorLog] ADD  CONSTRAINT [DF_dt_ErrorLog_Resolved]  DEFAULT ((0)) FOR [Resolved]
GO
/****** Object:  Default [DF_ErrorLog_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[dt_ErrorLog] ADD  CONSTRAINT [DF_ErrorLog_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_lt_Video_Tag_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[lt_Video_Tag] ADD  CONSTRAINT [DF_lt_Video_Tag_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_lt_Program_Tag_CreatedDate]    Script Date: 04/29/2011 16:58:29 ******/
ALTER TABLE [dbo].[lt_Program_Tag] ADD  CONSTRAINT [DF_lt_Program_Tag_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tt_Video_Meta_v3_CreadetDate]    Script Date: 04/29/2011 16:58:31 ******/
ALTER TABLE [dbo].[tt_Video_Meta_v3] ADD  CONSTRAINT [DF_tt_Video_Meta_v3_CreadetDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tt_Video_Meta_V2_CreadetDate]    Script Date: 04/29/2011 16:58:31 ******/
ALTER TABLE [dbo].[tt_Video_Meta_V2] ADD  CONSTRAINT [DF_tt_Video_Meta_V2_CreadetDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tt_Video_Meta_CreadetDate_BU]    Script Date: 04/29/2011 16:58:31 ******/
ALTER TABLE [dbo].[tt_Video_Meta_BU] ADD  CONSTRAINT [DF_tt_Video_Meta_CreadetDate_BU]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tt_Video_Meta_CreadetDate]    Script Date: 04/29/2011 16:58:31 ******/
ALTER TABLE [dbo].[tt_Video_Meta] ADD  CONSTRAINT [DF_tt_Video_Meta_CreadetDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_EditorialList_StatusID]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_EditorialList] ADD  CONSTRAINT [DF_dt_EditorialList_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_EditorialList_CreatedDate]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_EditorialList] ADD  CONSTRAINT [DF_dt_EditorialList_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_Video_ProgramID]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_ProgramID]  DEFAULT ((0)) FOR [ProgramID]
GO
/****** Object:  Default [DF_dt_Video_VideoProgramTypeID]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_VideoProgramTypeID]  DEFAULT ((1)) FOR [VideoProgramTypeID]
GO
/****** Object:  Default [DF_dt_Video_AgeRestrictionID]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_AgeRestrictionID]  DEFAULT ((1)) FOR [AgeRestrictionID]
GO
/****** Object:  Default [DF_dt_Video_StartDate]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_StartDate]  DEFAULT (getdate()) FOR [StartDate]
GO
/****** Object:  Default [DF_dt_Video_EndDate]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_EndDate]  DEFAULT (getdate()) FOR [EndDate]
GO
/****** Object:  Default [DF_dt_Video_CreatedDate]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_Video_StatusID]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video] ADD  CONSTRAINT [DF_dt_Video_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_dt_VideoFile_ProductId]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_VideoFile] ADD  CONSTRAINT [DF_dt_VideoFile_ProductId]  DEFAULT ((1)) FOR [ProductId]
GO
/****** Object:  Default [DF_dt_VideoFile_CreatedDate]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_VideoFile] ADD  CONSTRAINT [DF_dt_VideoFile_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_dt_VideoFile_StatusID]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_VideoFile] ADD  CONSTRAINT [DF_dt_VideoFile_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF_lt_EditorialListItems_ItemTypeId]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_EditorialListItems] ADD  CONSTRAINT [DF_lt_EditorialListItems_ItemTypeId]  DEFAULT ((1)) FOR [ItemTypeId]
GO
/****** Object:  Default [DF_lt_EditorialListItems_Rank]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_EditorialListItems] ADD  CONSTRAINT [DF_lt_EditorialListItems_Rank]  DEFAULT ((100)) FOR [ItemRank]
GO
/****** Object:  Default [DF_lt_EditorialListItems_CreatedDate]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_EditorialListItems] ADD  CONSTRAINT [DF_lt_EditorialListItems_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  ForeignKey [FK_lt_Person_Profession_dt_Person]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Person_Profession]  WITH CHECK ADD  CONSTRAINT [FK_lt_Person_Profession_dt_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[dt_Person] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Person_Profession] CHECK CONSTRAINT [FK_lt_Person_Profession_dt_Person]
GO
/****** Object:  ForeignKey [FK_lt_Person_Profession_dt_Profession]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Person_Profession]  WITH CHECK ADD  CONSTRAINT [FK_lt_Person_Profession_dt_Profession] FOREIGN KEY([ProfessionID])
REFERENCES [dbo].[dt_Profession] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Person_Profession] CHECK CONSTRAINT [FK_lt_Person_Profession_dt_Profession]
GO
/****** Object:  ForeignKey [FK_lt_Website_Product_dt_Product]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Website_Product]  WITH CHECK ADD  CONSTRAINT [FK_lt_Website_Product_dt_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[dt_Product] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Website_Product] CHECK CONSTRAINT [FK_lt_Website_Product_dt_Product]
GO
/****** Object:  ForeignKey [FK_lt_Website_Product_dt_Website]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Website_Product]  WITH CHECK ADD  CONSTRAINT [FK_lt_Website_Product_dt_Website] FOREIGN KEY([WebsiteID])
REFERENCES [dbo].[dt_Website] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Website_Product] CHECK CONSTRAINT [FK_lt_Website_Product_dt_Website]
GO
/****** Object:  ForeignKey [FK_lt_Channel_Program_dt_Program]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Channel_Program]  WITH CHECK ADD  CONSTRAINT [FK_lt_Channel_Program_dt_Program] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[dt_Program] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Channel_Program] CHECK CONSTRAINT [FK_lt_Channel_Program_dt_Program]
GO
/****** Object:  ForeignKey [FK_lt_Program_Genre_dt_Program]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Program_Genre]  WITH CHECK ADD  CONSTRAINT [FK_lt_Program_Genre_dt_Program] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[dt_Program] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Program_Genre] CHECK CONSTRAINT [FK_lt_Program_Genre_dt_Program]
GO
/****** Object:  ForeignKey [FK_lt_Product_Program_dt_Product]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Product_Program]  WITH CHECK ADD  CONSTRAINT [FK_lt_Product_Program_dt_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[dt_Product] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Product_Program] CHECK CONSTRAINT [FK_lt_Product_Program_dt_Product]
GO
/****** Object:  ForeignKey [FK_lt_Product_Program_dt_Program]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Product_Program]  WITH CHECK ADD  CONSTRAINT [FK_lt_Product_Program_dt_Program] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[dt_Program] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Product_Program] CHECK CONSTRAINT [FK_lt_Product_Program_dt_Program]
GO
/****** Object:  ForeignKey [FK_dt_EditorialList_dt_Status]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_EditorialList]  WITH CHECK ADD  CONSTRAINT [FK_dt_EditorialList_dt_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[dt_Status] ([ID])
GO
ALTER TABLE [dbo].[dt_EditorialList] CHECK CONSTRAINT [FK_dt_EditorialList_dt_Status]
GO
/****** Object:  ForeignKey [FK_dt_Video_dt_Program]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video]  WITH CHECK ADD  CONSTRAINT [FK_dt_Video_dt_Program] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[dt_Program] ([ID])
GO
ALTER TABLE [dbo].[dt_Video] CHECK CONSTRAINT [FK_dt_Video_dt_Program]
GO
/****** Object:  ForeignKey [FK_dt_Video_dt_Video]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_Video]  WITH CHECK ADD  CONSTRAINT [FK_dt_Video_dt_Video] FOREIGN KEY([ID])
REFERENCES [dbo].[dt_Video] ([ID])
GO
ALTER TABLE [dbo].[dt_Video] CHECK CONSTRAINT [FK_dt_Video_dt_Video]
GO
/****** Object:  ForeignKey [FK_dt_VideoFile_dt_Video]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[dt_VideoFile]  WITH CHECK ADD  CONSTRAINT [FK_dt_VideoFile_dt_Video] FOREIGN KEY([VideoID])
REFERENCES [dbo].[dt_Video] ([ID])
GO
ALTER TABLE [dbo].[dt_VideoFile] CHECK CONSTRAINT [FK_dt_VideoFile_dt_Video]
GO
/****** Object:  ForeignKey [FK_lt_Person_Profession_Program_dt_Program]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Person_Profession_Program]  WITH CHECK ADD  CONSTRAINT [FK_lt_Person_Profession_Program_dt_Program] FOREIGN KEY([ProgramID])
REFERENCES [dbo].[dt_Program] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Person_Profession_Program] CHECK CONSTRAINT [FK_lt_Person_Profession_Program_dt_Program]
GO
/****** Object:  ForeignKey [FK_lt_Person_Profession_Program_lt_Person_Profession]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Person_Profession_Program]  WITH NOCHECK ADD  CONSTRAINT [FK_lt_Person_Profession_Program_lt_Person_Profession] FOREIGN KEY([PersonProfessionID])
REFERENCES [dbo].[lt_Person_Profession] ([Table_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Person_Profession_Program] NOCHECK CONSTRAINT [FK_lt_Person_Profession_Program_lt_Person_Profession]
GO
/****** Object:  ForeignKey [FK_lt_EditorialListItems_dt_EditorialList]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_EditorialListItems]  WITH CHECK ADD  CONSTRAINT [FK_lt_EditorialListItems_dt_EditorialList] FOREIGN KEY([EditorialListID])
REFERENCES [dbo].[dt_EditorialList] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_EditorialListItems] CHECK CONSTRAINT [FK_lt_EditorialListItems_dt_EditorialList]
GO
/****** Object:  ForeignKey [FK_lt_Video_VideoCategory_dt_Video]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Video_VideoCategory]  WITH CHECK ADD  CONSTRAINT [FK_lt_Video_VideoCategory_dt_Video] FOREIGN KEY([VideoID])
REFERENCES [dbo].[dt_Video] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Video_VideoCategory] CHECK CONSTRAINT [FK_lt_Video_VideoCategory_dt_Video]
GO
/****** Object:  ForeignKey [FK_lt_Video_VideoCategory_dt_VideoCategory]    Script Date: 04/29/2011 16:58:34 ******/
ALTER TABLE [dbo].[lt_Video_VideoCategory]  WITH CHECK ADD  CONSTRAINT [FK_lt_Video_VideoCategory_dt_VideoCategory] FOREIGN KEY([VideoCategoryID])
REFERENCES [dbo].[dt_VideoCategory] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lt_Video_VideoCategory] CHECK CONSTRAINT [FK_lt_Video_VideoCategory_dt_VideoCategory]
GO
