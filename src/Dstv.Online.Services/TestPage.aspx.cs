﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.PdfGenerator;

namespace Dstv.Online.Services
{
    public partial class TestPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //byte[] documentBytes;

            //InvoiceService serv = new InvoiceService();

            //serv.Generate(81182835, 44676790, new List<string>(), new DateTime(1990, 1, 1), DateTime.Now, out documentBytes);

            InvoiceGenerator gen = new InvoiceGenerator(81182835, 44676790, new List<string>(), new DateTime(1990, 1, 1), DateTime.Now);

            gen.Download("test.pdf");
        }
    }
}