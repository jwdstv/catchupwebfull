﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Dstv.Online.PdfGenerator;
using Dstv.Online.Contracts;

#endregion

namespace Dstv.Online.Services
{
    public class InvoiceService : IInvoiceService
    {
        /// <summary>
        /// Generate customer invoice
        /// </summary>
        /// <param name="customerId">uint The connect user Id for the customer for which the invoice should be generated</param>
        /// <param name="accountNumber">uint The IBS account number for the customer for which the invoice should be generated</param>
        /// <param name="transactionCodes">List<string> The IBS transaction codes for the transactions to be displayed on the generated invoice</string></param>
        /// <param name="startDate">DateTime the transaction start date</param>
        /// <param name="endDate">DateTime the transaction end date</param>
        /// <param name="documentBytes">byte[] containing the pdf document bytes</param>
        /// <returns>ServiceResult indicating the result</returns>
        public ServiceResult Generate
            (
                uint customerId,
                uint accountNumber,
                List<string> transactionCodes,
                DateTime startDate,
                DateTime endDate,
                out byte[] documentBytes
            )
        {
            ServiceResult result = ServiceResult.CreateBlank();

            documentBytes = null;

            try
            {
                using (InvoiceGenerator gen = new InvoiceGenerator
                    (
                        customerId,            // Customer ID
                        accountNumber,         // Acount Number
                        transactionCodes,      // Filters
                        startDate,             // Period Start
                        endDate                // Period End
                    ))
                {
                    documentBytes = gen.Generate();
                }

                result.Success  = true;
                result.ErrorMsg = "";
            }
            catch (Exception ex)
            {
                result.Success  = false;
                result.ErrorMsg = ex.Message.ToString();
            }

            return result;
        }
    }
}
