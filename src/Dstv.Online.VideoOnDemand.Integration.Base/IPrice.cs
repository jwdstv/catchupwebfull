﻿using System;
using Dstv.Online.VideoOnDemand.Globalization;

namespace Dstv.Online.VideoOnDemand {
    internal interface ICurrencyAmount {
        CurrencyIdentifier CurrencyId { get; }
        Decimal Value { get; }
    }
}
