﻿using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Content {
    public interface IContentManagement {
        IPlaylist GetPlaylist(PlaylistIdentifier playlistId);
    }
}
