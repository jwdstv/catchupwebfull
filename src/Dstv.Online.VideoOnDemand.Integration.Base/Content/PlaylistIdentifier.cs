﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Content {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public class PlaylistIdentifier : AbstractIdentifier<string> {
        public PlaylistIdentifier(string id) : base(id){ }

        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return this.Value;
        }
    }
}
