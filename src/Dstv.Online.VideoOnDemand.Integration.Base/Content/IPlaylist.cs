﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Content 
{
    public interface IPlaylist
    {
        string Name { get; }
        CatalogueItemIdentifier[] CatalogueItemIds { get; }
    }
}
