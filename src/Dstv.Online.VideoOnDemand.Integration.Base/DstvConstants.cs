﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand {
    public static class DstvConstants {
        public const string DstvNamespace = "www.dstv.co.za";


    }

    public static class CacheConstants {
        public const string CachePrefix = "VOD";
        public const int AuthKeyLength = 8;
        public const int MaxCacheKeyLength = 250;

    }

}
