﻿
namespace Dstv.Online.VideoOnDemand
{
	public enum SortOrders
	{
		LastChance,
		ExpiryDateDesc,
		ProgramTitleAsc, // Video title
        ProgramTitleDesc, // Video title
		StartDateAsc,
		StartDateDesc,
		StartDateAscFilterCurrent,
        LatestReleases,
        ForthcomingReleases,
		StartDateDescFilterFuture,
		Ordinal,
		MostPopular,
		ByShowProgramTitleAsc, // Returns only the show (not all the videos)
		ByShowVideoStartDateDesc,
        ByShowProgramTitleAllVideosAsc // Returns all the videos for the show
	}
}
