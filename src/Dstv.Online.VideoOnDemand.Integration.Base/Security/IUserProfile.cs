﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Transactions;

namespace Dstv.Online.VideoOnDemand.Security
{
    public interface IUserProfile
    {
        IProfileInformation GetCurrentUserProfile();
        IProfileInformation GetUserProfile(string userId);
    }
    public interface IProfileInformation
    {
        CustomerIdentifier CustomerId { get; }
        bool HasWallet { get; }
        CurrencyAmount WalletBalance { get; }
        string UserName { get; }
        string Email { get; }
        bool IsSmartCardLinked { get; }
        bool IsPremiumDstvSubscriber { get; }
        bool IsLinkedToISP { get; }
    }
}
