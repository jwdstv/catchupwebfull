﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Collections;

namespace Dstv.Online.VideoOnDemand {
	[Serializable]
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public abstract class AbstractIdentifier<T>  {
        private T id = default(T);
        protected AbstractIdentifier(T id) {
            this.id = id;
        }
        protected AbstractIdentifier(string id) {
            this.id = this.parse(id);
        }

        [DataMember]
        public T Value {
            get { return id; }
            set { throw new ApplicationException("Value is read only");  }
        }
        public override string ToString() {
            string stringValue = createStringValue();
            return stringValue;
        }
        abstract protected T parse(string id);
        abstract protected string createStringValue();

        public override bool Equals(object obj) {
            AbstractIdentifier<T> abstractIdentifier = obj as AbstractIdentifier<T>;
            if (abstractIdentifier != null) {
                return Value.Equals(abstractIdentifier.Value);
            }
            return createStringValue().Equals(obj.ToString());
        }
        public override int GetHashCode() {
            return createStringValue().GetHashCode();
        }
    }
}
