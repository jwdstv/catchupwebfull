﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    [Serializable]
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(CatalogueResult))]
    public class CatalogueResult : ICatalogueResult {
        #region ICatalogueResult Members
        public CatalogueResult(int results, int page, ICatalogueItem[] items) {
            this.totalResultCount = results;
            this.currentPage = page;
            this.catalogueItems = items;
        }
        private int totalResultCount = -1;
        [DataMember]
        public int TotalResultCount {
            get { return totalResultCount; }
			  set { totalResultCount = value;  }
        }

        private int currentPage = -1;
        [DataMember]
        public int CurrentPage {
            get { return currentPage; }
            set { }
        }

        private ICatalogueItem[] catalogueItems = null;
        [DataMember]
        public ICatalogueItem[] CatalogueItems {
            get { return catalogueItems; }
			  set { catalogueItems = value;  }
        }

        #endregion
    }
}
