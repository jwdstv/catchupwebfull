﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue
{
	public enum VideoProgramType
	{
		Unknown = 0,
		ShowEpisode = 1,
		Movie = 2,
		ShowClip = 3,
		MovieEPK = 4,
		FeatureFilm = 5,
		MovieTrailer = 6,
		ShowTrailer = 7,
	}
}
