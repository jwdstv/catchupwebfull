﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Transactions;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    internal interface IClassificationCriteria {
        ClassificationItemIdentifier ClassificationItemId {
            get;
        }
        CatalogueItemClassificationTypes ClassificationType {
            get;
        }

        ProductTypeIdentifier ProductTypeId {
            get;
        }

        ClassificationItemIdentifier ParentClassificationId {
            get;
        }

        TerritoryIdentifier TerritoryId {
            get;
        }
    }
}
