﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
namespace Dstv.Online.VideoOnDemand.MediaCatalogue 
{
     internal interface ICatalogueResult
     {
         int TotalResultCount { get; }
         int CurrentPage { get; }
         ICatalogueItem[] CatalogueItems { get; }
    }
}
