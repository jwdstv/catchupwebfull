﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    public interface ICatalogueItemMedia {
        MediaTypeIdentifier MediaType { get; }
        string FileName { get; }
        string ServerPath { get; }
        long? FileSizeInBytes { get; }
        string ManId { get; }

        string Dimensions { get; }
    }
}
