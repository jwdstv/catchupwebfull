﻿using System;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    public interface ICatalogueItemMedia {
        MediaTypeIdentifier MediaTypeId { get; }
        string ServerPath { get; }
        decimal FileSizeInMb { get; }
        string FilePath { get; }
    }
}
