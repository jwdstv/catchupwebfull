﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Globalization;
using System.Runtime.Serialization;
using System.Threading;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
	
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(CatalogueCriteria))]
    public class CatalogueCriteria : ICatalogueCriteria{
        #region ICatalogueCriteria Members
        private ClassificationCriteria classificationCriteria = new ClassificationCriteria();
        [DataMember]
        public ClassificationCriteria ClassificationCriteria {
            get {
                return classificationCriteria;
            }
            set {
                classificationCriteria = value;
            }
        }

        private PagingCriteria pagingCriteria = new PagingCriteria();
        [DataMember]
        public PagingCriteria Paging {
            get { return pagingCriteria; }
            set { pagingCriteria = value; }
        }

        private SortOrders sortOrder = SortOrders.ProgramTitleAsc;
        [DataMember]
        public SortOrders SortOrder {
            get { return sortOrder; }
            set { sortOrder = value; }
        }

        private List<CatalogueItemIdentifier> catalogueItems = new List<CatalogueItemIdentifier>();
        [DataMember]
        public CatalogueItemIdentifier[] CatalogueItemIds {
            get { return catalogueItems.ToArray(); }
            set { 
                catalogueItems.Clear();
                catalogueItems.AddRange(value);
            }
        }

        #endregion

        #region ICatalogueCriteria Members


        #endregion
    }
}
