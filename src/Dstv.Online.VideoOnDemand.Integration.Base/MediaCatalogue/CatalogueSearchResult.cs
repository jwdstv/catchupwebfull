﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    [Serializable]
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(CatalogueResult))]
    public class CatalogueSearchResult : ICatalogueSearchResult {
        #region ICatalogueResult Members
        public CatalogueSearchResult(int results, int page, CatalogueItemIdentifier[] items) {
            this.totalResultCount = results;
            this.currentPage = page;
            this.catalogueItems = items;
        }
        private int totalResultCount = -1;
        [DataMember]
        public int TotalResultCount {
            get { return totalResultCount; }
            set { }
        }

        private int currentPage = -1;
        [DataMember]
        public int CurrentPage {
            get { return currentPage; }
            set { }
        }

        private CatalogueItemIdentifier[] catalogueItems = null;
        [DataMember]
        public CatalogueItemIdentifier[] CatalogueItemIds {
            get { return catalogueItems; }
            set { }
        }

        #endregion
    }
}
