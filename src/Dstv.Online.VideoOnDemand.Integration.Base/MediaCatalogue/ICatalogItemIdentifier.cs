﻿
namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
     internal interface ICatalogueItemIdentifier {
         string Value { get; }
    }
}
