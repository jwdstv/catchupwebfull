﻿using System;
using Dstv.Online.VideoOnDemand.Transactions;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
     public interface ICatalogueItem {
        CatalogueItemIdentifier CatalogueItemId { get; }
        System.DateTime? Airdate { get; }
        DateTime? ExpiryDate { get; }
        CurrencyAmount RentalAmount { get; }
        string Abstract { get; }
         string ProgramAbstract { get; }
        int RunTimeInMinutes { get; }
        int RentalPeriodInHours { get; }
        ProductTypeIdentifier ProductTypeId { get; }
        //ClassificationItemIdentifier ClassificationId { get; }
        string VideoTitle { get; }
        string Keywords { get; }
        string DurationDescription { get; }
        int? EpisodeNumber { get; }
        string EpisodeTitle { get; }
        string ProgramTitle { get; }
        int? ProgramId { get; }
        string ProgramKeywords { get; }
        string VideoKeywords { get; }
        int? SeasonNumber { get; }
        DateTime? StartDate { get; }
        int? VideoProgramTypeId { get; }
		VideoProgramType VideoProgramType { get; }

        string MediaThumbnailUri { get; }

        String Synopsis { get; }

        /// <summary>
        /// 18 VSL
        /// </summary>
        string AgeRestriction { get; }

        ClassificationItemIdentifier ClassificationItemId { get; }

        bool? IsRestricted { get; }

        string MediaImageUri { get; }
        /// <summary>
        /// Popularity sort order
        /// </summary>
        int? Rating { get; }

        //ClassificationItemIdentifier SecondaryClassificationItemId { get; }

        string ProgramGenre
        {
            get;
        }

        string ProgramSubGenre
        {
            get;
        }

        string ProgramType
        {
            get;
        }

		string ProgramBillboardUri { get; }

        string IBSProductId { get; }

        string AssetId { get; }

        int ProductId { get; }

    }
}
