﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Globalization;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.Transactions;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {

	[DataContract(Namespace = DstvConstants.DstvNamespace)]
	[KnownType(typeof(ClassificationCriteria))]
	public class ClassificationCriteria : IClassificationCriteria
	{
		#region IClassificationCriteria Members

		private ClassificationItemIdentifier classificationItemId;
		[DataMember]
		public ClassificationItemIdentifier ClassificationItemId
		{
			get { return classificationItemId; }
			set { classificationItemId = value; }
		}

		private string classificationItemName;
		[DataMember]
		public string ClassificationItemName
		{
			get { return classificationItemName; }
			set { classificationItemName = value; }
		}

		private CatalogueItemClassificationTypes classificationType;
		[DataMember]
		public CatalogueItemClassificationTypes ClassificationType
		{
			get { return classificationType; }
			set { classificationType = value; }
		}

		private ProductTypeIdentifier productTypeId;
		[DataMember]
		public ProductTypeIdentifier ProductTypeId
		{
			get { return productTypeId; }
			set { productTypeId = value; }
		}

		private TerritoryIdentifier territoryId;
		[DataMember]
		public TerritoryIdentifier TerritoryId
		{
			get { return territoryId; }
			set { territoryId = value; }
		}

        private ClassificationItemIdentifier parentClassificationId;
        [DataMember]
        public ClassificationItemIdentifier ParentClassificationId
        {
            get { return parentClassificationId; }
            set { parentClassificationId = value; }
        }
		#endregion

		#region IClassificationCriteria Members

		private List<VideoProgramType> videoProgramTypes = new List<VideoProgramType>();
		[DataMember]
		public VideoProgramType[] VideoProgramTypes
		{
			get { return videoProgramTypes.ToArray(); }
			set
			{
				videoProgramTypes.Clear();
				videoProgramTypes.AddRange(value);
			}
		}

		#endregion


		#region public void SetVideoProgramTypesFromCsv ( string videoProgramTypesCsvString )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoProgramTypesCsvString"></param>
		public void SetVideoProgramTypesFromCsv ( string videoProgramTypesCsvString )
		{
			videoProgramTypes.Clear();
			string[] videoTypesSplit = videoProgramTypesCsvString.Split(',');

			foreach (string videoType in videoTypesSplit)
			{
				// Framework 4
				//VideoProgramType videoTypeEnum;
				//if (Enum.TryParse<VideoProgramType>(videoType, out videoTypeEnum))
				//{
				//   videoProgramTypes.Add(videoTypeEnum);
				//}
				try
				{
					videoProgramTypes.Add((VideoProgramType)Enum.Parse(typeof(VideoProgramType), videoType));
				}
				catch
				{
				}
			}
		}
		#endregion


		#region public string GetVideoProgramTypesAsCsv ()
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetVideoProgramTypesAsCsv ()
		{
			// Framework 4
			//return string.Join<VideoProgramType>(",", videoProgramTypes);
			string videoProgramTypesAsCsv = "";
			foreach (VideoProgramType videoType in videoProgramTypes)
			{
				videoProgramTypesAsCsv += string.Format("{0},", videoType);
			}

			return videoProgramTypesAsCsv.TrimEnd(',');
		}
		#endregion

	}
}
