﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue
{
    public enum VideoReleases
    {
        LastChance,
        NewRelease,
        UpcomingRelease,
    }
}
