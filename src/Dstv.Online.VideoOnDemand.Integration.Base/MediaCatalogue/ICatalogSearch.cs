﻿using System.Collections.Generic;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    public interface ICatalogueSearch {
        CatalogueSearchResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm, List<int> productIdList);

        CatalogueSearchResult SearchCatalogForRelated(CatalogueCriteria criteria, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList);
    }

}
