﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue
{
	[DataContract(Namespace = DstvConstants.DstvNamespace)]
	[KnownType(typeof(CatalogueClassificationResult))]
    [Serializable]
	public class CatalogueClassificationResult
	{
		private List<ClassificationResultItem> _classificationHierarchy = new List<ClassificationResultItem>();
		public List<ClassificationResultItem> ClassificationHierarchy
		{
			get { return _classificationHierarchy; }
		}

		public void AddHierarchyItem ( ClassificationResultItem classificationResultItem )
		{
			_classificationHierarchy.Add(classificationResultItem);
		}

		public void AddHierarchyItem ( IClassificationItem classificationItem )
		{
			_classificationHierarchy.Add(new ClassificationResultItem(classificationItem) );
		}
	}

	public class ClassificationResultItem
	{
		private IClassificationItem _classificationItem;
		public IClassificationItem ClassificationItem
		{
			get { return _classificationItem; }
			set { _classificationItem = value; }
		}

		private List<ClassificationResultItem> _subItems = new List<ClassificationResultItem>();
		public List<ClassificationResultItem> SubItems
		{
			get { return _subItems; }
		}

		public ClassificationResultItem ( IClassificationItem classificationItem )
		{
			_classificationItem = classificationItem;
		}

		public void AddSubItem ( ClassificationResultItem subItem )
		{
			_subItems.Add(subItem);
		}

		public void AddSubItems ( IClassificationItem[] subItems )
		{
			_subItems.Clear();
			for (int i=0; i < subItems.Length; i++)
			{
				_subItems.Add(new ClassificationResultItem(subItems[i]));
			}
		}
	}
}
