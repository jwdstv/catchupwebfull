﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    internal interface IGenreIdentifier {
        string Value {
            get;
        }
    }
}
