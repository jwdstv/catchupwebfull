﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    [Serializable]
    [DataContract(Namespace = DstvConstants.DstvNamespace)]   
    [KnownType(typeof(CatalogueItemIdentifier))]
    public class CatalogueItemIdentifier : AbstractIdentifier<string> {
        public CatalogueItemIdentifier(string id) : base(id) { }

        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return this.Value;
        }
    }
}
