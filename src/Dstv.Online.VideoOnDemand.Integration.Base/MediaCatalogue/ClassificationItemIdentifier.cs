﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
	
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(ClassificationItemIdentifier))]
    public class ClassificationItemIdentifier : AbstractIdentifier<string> {
        public ClassificationItemIdentifier(string id) : base(id) { }

        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return Value;
        }
    }
}
