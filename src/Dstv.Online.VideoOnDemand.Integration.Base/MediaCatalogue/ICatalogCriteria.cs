﻿using Dstv.Online.VideoOnDemand.Globalization;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue
{
    internal interface ICatalogueCriteria {
        PagingCriteria Paging { get; }
        SortOrders SortOrder { get; }
        CatalogueItemIdentifier[] CatalogueItemIds { get; }

        ClassificationCriteria ClassificationCriteria { get; } 
    }
}
