﻿
using System.Runtime.Serialization;
namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(MediaTypeIdentifier))]
    public class MediaTypeIdentifier : AbstractIdentifier<string> {
        public MediaTypeIdentifier(string id) : base(id) { }
        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return this.Value;
        }
    }

}
