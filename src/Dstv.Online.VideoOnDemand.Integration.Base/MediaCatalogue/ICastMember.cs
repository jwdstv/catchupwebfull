﻿
using System.Data;
namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    public interface ICastMember {
        string FullName { get; }
        string Role { get; }
        bool IsCast { get; }
        bool IsCrew { get; }
    }
}
