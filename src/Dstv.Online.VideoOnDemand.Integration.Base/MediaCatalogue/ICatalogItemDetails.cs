﻿
namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    public interface ICatalogueItemDetails  {
        ICatalogueItem[] CatalogueItems { get; }
    }
}
