﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand
{
    public interface IChannel
    {
        ChannelItemIdentifier Id
        {
            get;
        }

        string Name
        {
            get;
        }

        string ImageFilename
        {
            get;
        }
    }
}
