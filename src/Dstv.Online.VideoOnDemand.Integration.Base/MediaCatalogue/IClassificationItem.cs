﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
    public interface IClassificationItem {
        ClassificationItemIdentifier ClassificationItemId { get; }
       // CatalogueItemClassificationTypes ClassificationType { get; }
        bool HasAdditionalClassification { get; }
        string Name { get; }
        int TotalNumberOfCatalogueItems { get; }
        ClassificationItemIdentifier ParentClassificationItemId { get; }
    }
}
