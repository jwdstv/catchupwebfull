﻿
using Dstv.Online.VideoOnDemand.Transactions;

namespace Dstv.Online.VideoOnDemand.MediaCatalogue {
     public interface ICatalogue {
        CatalogueResult GetCatalogueItems(CatalogueCriteria criteria);
        CatalogueResult GetCatalogueItem(CatalogueItemIdentifier catalogueItemId, bool getRelatedMedia);
        IClassificationItem[] GetClassificationItems(ClassificationCriteria criteria);

        ICatalogueItemMedia GetCatalogueItemMedia(CatalogueItemIdentifier catalogueItemId, MediaTypeIdentifier mediaTypeId);

        ICastMember[] GetCatalogueItemCastAndCrew(CatalogueItemIdentifier catalogueItemId);

        IChannel[] GetChannels(CatalogueItemIdentifier catalogueItemId);

        void InsertCatalogueItemPlayHit(int videoID);

    }
}
