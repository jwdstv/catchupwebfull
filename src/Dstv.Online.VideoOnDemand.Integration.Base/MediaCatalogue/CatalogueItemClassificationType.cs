﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand
{
	public enum CatalogueItemClassificationTypes
	{
		ProgramType,
		TvShows,
		TvChannels,
		Categories,
		All,
		Genre,
		ProductType,
		Unknown = 0
	}
}
