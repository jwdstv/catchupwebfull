﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand
{
    public class Channel : IChannel
    {
        #region IChannel Members

        public ChannelItemIdentifier Id { get; set; }

        public string Name { get; set; }

        public string ImageFilename { get; set; }

        #endregion
    }
}
