﻿using System;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Transactions {
    internal interface IPurchaseDetails {
        CustomerIdentifier CustomerId { get; }
        DateTime PurchaseDateTime { get; }
        string Description { get; }
        CurrencyAmount Amount { get; }
        TransactionReference RequestReference { get; }
        CatalogueItemIdentifier CatalogItemId { get; }
    }
}
