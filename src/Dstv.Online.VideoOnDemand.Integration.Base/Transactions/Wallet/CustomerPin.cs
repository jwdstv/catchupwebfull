﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Transactions.Wallet {

    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public class CustomerPin : ICustomerPin {
        #region ICustomerWalletPin Members
        private String pinValue;
        public CustomerPin(String pin) {
            pinValue = pin;
        }
        [DataMember]
        public String PinValue {
            get { return pinValue; }
            set { }
        }

        #endregion
    }
}
