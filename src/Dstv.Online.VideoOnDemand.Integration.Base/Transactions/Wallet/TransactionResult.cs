﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Transactions {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(TransactionResult))]
    public class TransactionResult : ITransactionResult {
        public TransactionResult(OperationResults outcome, string details, TransactionReference requestRef, TransactionReference resultRef) {
            this.result = outcome;
            this.details = details;
            this.requestReference = requestRef;
            this.resultReference = resultRef;
        }
        #region IWalletTransactionResult Members

        private OperationResults result = OperationResults.Indeterminate;
        [DataMember]
        public OperationResults Result {
            get { return result; }
            set { }
        }


        private string details = null;
        [DataMember]
        public string Details {
            get {return details; }
            set { }
        }

        private TransactionReference requestReference = null;
        [DataMember]
        public TransactionReference RequestReference {
            get { return requestReference; }
            set { }
        }

        private TransactionReference resultReference = null;
        [DataMember]
        public TransactionReference ResultReference {
            get { return resultReference; }
            set { }
        }

        #endregion
    }
}
