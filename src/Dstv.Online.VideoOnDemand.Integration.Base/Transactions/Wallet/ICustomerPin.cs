﻿using System.Security;

namespace Dstv.Online.VideoOnDemand.Transactions.Wallet {
    internal interface ICustomerPin {
        string PinValue { get; }
    }
}
