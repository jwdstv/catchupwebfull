﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Transactions.Wallet
{
	public abstract class DstvWallet : IDstvWallet
	{

		//public abstract CustomerIdentifier GetCurrentCustomerId();
		//public abstract CurrencyAmount GetBalance();

		protected abstract void performRentalTransaction ( PurchaseDetails purchaseDetails, CustomerPin pin );

		public TransactionResult Purchase ( PurchaseDetails purchaseDetails, CustomerPin pin )
		{
            //// Simulate time to process.
            //Random random = new Random();
            //System.Threading.Thread.Sleep(random.Next(3000, 15000));

			validationOutcome outcome = validatePurchase(purchaseDetails);
			if (!outcome.passedValidation)
			{
				return outcome.result;
			}
			try
			{
				performRentalTransaction(purchaseDetails, pin);
			}
			catch (Exception ex)
			{
				return createExceptionResult(purchaseDetails, ex);
			}
			return createSuccessResult(purchaseDetails);
		}
		//protected abstract CurrencyAmount GetBalance ();
		private validationOutcome validatePurchase ( IPurchaseDetails purchaseDetails )
		{
			//CurrencyAmount balance = GetBalance();
			validationOutcome outcome = new validationOutcome();
			if (purchaseDetails == null)
			{
				outcome.passedValidation = false;
			}
			if (purchaseDetails.CustomerId == null)
			{
				outcome.passedValidation = false;
			}
			if (purchaseDetails.CatalogItemId == null)
			{
				outcome.passedValidation = false;
			}
			if (purchaseDetails.Amount == null)
			{
				outcome.passedValidation = false;
			}
			if (purchaseDetails.RequestReference == null)
			{
				outcome.passedValidation = false;
			}
            //if (balance == null)
            //{
            //    outcome.passedValidation = false;
            //}
			if (!outcome.passedValidation)
			{
				outcome.result = createValidationFailedResult(purchaseDetails);
				return outcome;
			}
            //if (purchaseDetails.Amount.Value > GetBalance().Value)
            //{
            //    outcome.result = createInsufficientFundsResult(purchaseDetails);
            //    outcome.passedValidation = false;
            //}
			return outcome;
		}


		const string validationFailedMessage = "Validation failed";
		private TransactionResult createValidationFailedResult ( IPurchaseDetails purchaseDetails )
		{
			return new TransactionResult(
				 OperationResults.Failure,
				 validationFailedMessage,
				 purchaseDetails.RequestReference,
				 new TransactionReference(Guid.NewGuid().ToString()));

		}
		const string insufficientFundsMessage = "Insufficient Funds";
		private TransactionResult createInsufficientFundsResult ( IPurchaseDetails purchaseDetails )
		{
			return new TransactionResult(
				 OperationResults.Failure,
				 insufficientFundsMessage,
				 purchaseDetails.RequestReference,
				 new TransactionReference(Guid.NewGuid().ToString()));
		}
		private TransactionResult createExceptionResult ( IPurchaseDetails purchaseDetails, Exception ex )
		{
			return new TransactionResult(OperationResults.Failure, ex.Message, purchaseDetails.RequestReference, new TransactionReference(GetIBSTxRefNo()));
		}
		private TransactionResult createSuccessResult ( IPurchaseDetails purchaseDetails )
		{
			return new TransactionResult(OperationResults.Success, string.Empty, purchaseDetails.RequestReference, new TransactionReference(GetIBSTxRefNo()));
		}

		protected class validationOutcome
		{
			internal bool passedValidation = true;
			internal TransactionResult result;
		}


		private string GetIBSTxRefNo ()
		{
			return string.Format("IBS:{0}{1}", DateTime.Now.Millisecond, Guid.NewGuid().ToString("N").Substring(0, 4)).ToUpper();
		}

    }

}
