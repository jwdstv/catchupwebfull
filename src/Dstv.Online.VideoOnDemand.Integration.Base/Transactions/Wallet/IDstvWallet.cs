﻿
using System;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Transactions.Wallet {
    
    internal interface IDstvWallet {
        TransactionResult Purchase(PurchaseDetails purchaseDetails, CustomerPin pin);

        //CustomerIdentifier GetCurrentCustomerId();
        //CurrencyAmount GetBalance();
    }
}
