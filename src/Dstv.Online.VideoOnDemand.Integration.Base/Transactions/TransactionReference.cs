﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Transactions {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public class TransactionReference : AbstractIdentifier<string> {
        public TransactionReference(string reference) : base(reference) {
        }

        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return this.Value;
        }
    }
}
