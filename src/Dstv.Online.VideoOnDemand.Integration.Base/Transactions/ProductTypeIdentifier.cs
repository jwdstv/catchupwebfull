﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Transactions {
	
    [DataContract(Namespace = DstvConstants.DstvNamespace)] 
    [KnownType(typeof(ProductTypeIdentifier))]
    public class ProductTypeIdentifier : AbstractIdentifier<ProductTypes> {
        public ProductTypeIdentifier(string id) : base(id) { }
        public ProductTypeIdentifier(ProductTypes id) : base(id) { }

        protected override ProductTypes parse(string id) {
            return (ProductTypes) Enum.Parse(typeof(ProductTypes), id);
        }

        protected override string createStringValue() {
            return this.Value.ToString();
        }
    }
}
