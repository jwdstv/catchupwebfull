﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Transactions {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    //[KnownType(typeof(CurrencyAmount))]
    [KnownType(typeof(CustomerIdentifier))]
    [KnownType(typeof(TransactionReference))]
    [KnownType(typeof(CatalogueItemIdentifier))]
    public class PurchaseDetails : IPurchaseDetails {
        public PurchaseDetails(CustomerIdentifier custId, CatalogueItemIdentifier catalogItemId, DateTime when, CurrencyAmount amt, string reference, string desc) {
            this.customerId = custId;
            this.catalogItemId = catalogItemId;
            purchaseDateTime = when;
            this.amount = amt;
            this.requestReference = new TransactionReference(reference);
            this.description = desc;
        }
        #region IPurchaseDetails Members

        private CustomerIdentifier customerId = null;
        [DataMember]
        public CustomerIdentifier CustomerId {
            get { return customerId; }
            set { }
        }

        private DateTime purchaseDateTime;
        [DataMember]
        public DateTime PurchaseDateTime {
            get { return purchaseDateTime; }
            set { }
        }

        private string description = null;
        [DataMember]
        public string Description {
            get { return description; }
            set { }
        }

        private CurrencyAmount amount = null;
        [DataMember]
        public CurrencyAmount Amount {
            get { return amount; }
            set { }
        }

        private TransactionReference requestReference = null;
        [DataMember]
        public TransactionReference RequestReference {
            get { return requestReference; }
            set { }
        }


        private CatalogueItemIdentifier catalogItemId = null;
        [DataMember]
        public CatalogueItemIdentifier CatalogItemId {
            get { return catalogItemId; }
            set { }
        }

        #endregion
    }
}
