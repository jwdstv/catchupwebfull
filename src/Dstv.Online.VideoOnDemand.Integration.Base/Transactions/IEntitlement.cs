﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Transactions
{
    public interface IEntitlement
    {
        //TransactionResult GenerateEntitlement(CustomerIdentifier connectUserId, CatalogueItemIdentifier catalogueItemId);
        TransactionResult AuthorizeCatchupPackages(CustomerIdentifier connectUserId);
        TransactionResult DeauthorizeCatchupPackages(CustomerIdentifier connectUserId);
        TransactionResult GenerateVideoEntitlement(CustomerIdentifier connectUserId, CatalogueItemIdentifier catalogueItemId);
    }
}
