﻿
namespace Dstv.Online.VideoOnDemand.Transactions {
    public interface IProductType {
        ProductTypeIdentifier ProductTypeId { get; }
        string Name { get; }
    }
}
