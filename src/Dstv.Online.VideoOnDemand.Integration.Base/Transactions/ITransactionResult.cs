﻿
namespace Dstv.Online.VideoOnDemand.Transactions {
    internal interface ITransactionResult {
        OperationResults Result { get; }
        string Details { get; }
        TransactionReference RequestReference { get; }
        TransactionReference ResultReference { get; }
    }

}
