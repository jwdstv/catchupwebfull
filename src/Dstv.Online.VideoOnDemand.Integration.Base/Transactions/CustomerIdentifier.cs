﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Transactions {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public class CustomerIdentifier : AbstractIdentifier<string> {
        public CustomerIdentifier(string id) : base(id) { }

        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return this.Value;
        }
    }
}
