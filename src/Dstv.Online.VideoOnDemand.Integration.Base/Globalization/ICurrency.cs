﻿
namespace Dstv.Online.VideoOnDemand.Globalization {
    public interface ICurrency {
        //ITerritory TerritoryId { get; }
        CurrencyIdentifier CurrencyId { get; }
        string Description { get; }
        string Code { get; }
        string Symbol { get; }
    }
}
