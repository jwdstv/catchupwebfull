﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Globalization {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(Territory))]
    public class Territory :  ITerritory{
        public Territory(string territory) {
            value = territory;
        }
        #region ITerritory Members
        private string value;
        [DataMember]
        public string Value {
            get { return value; }
            set { }
        }

        #endregion
        public override string ToString() {
            return value;
        }
    }
}
