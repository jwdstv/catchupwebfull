﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Globalization {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(CurrencyIdentifier))]
    public class CurrencyIdentifier : AbstractIdentifier<string> {
        public CurrencyIdentifier(string id) : base(id) { }
        protected override string parse(string id) {
            return id;
        }

        protected override string createStringValue() {
            return Value;
        }
    }
}
