﻿
namespace Dstv.Online.VideoOnDemand.Globalization {
    public interface ITerritory {
        TerritoryIdentifier TerritoryId { get; }
        string Name { get; }
    }
}
