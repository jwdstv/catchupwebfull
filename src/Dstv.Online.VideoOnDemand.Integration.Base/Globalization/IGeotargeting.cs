﻿
namespace Dstv.Online.VideoOnDemand.Globalization {
    public interface IGeotargeting {
        ITerritory ResolveUserTerritory();
    }

}
