﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand {

    [Serializable]
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public class PagingCriteria : IPagingCriteria {
        internal PagingCriteria() {
        }
        #region IPagingCriteria Members
        private int pageNumber = -1;
        [DataMember]
        public int PageNumber {
            get { return pageNumber; }
            set { pageNumber = value; }
        }

        private int itemsOnPage = -1;
        [DataMember]
        public int ItemsPerPage {
            get { return itemsOnPage; }
            set { itemsOnPage = value; }
        }

        #endregion
    }
}
