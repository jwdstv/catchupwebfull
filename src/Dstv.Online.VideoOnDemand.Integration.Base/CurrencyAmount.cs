﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Globalization;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand {
    [Serializable]
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    [KnownType(typeof(CurrencyAmount))]
    public class CurrencyAmount : ICurrencyAmount {
        CurrencyIdentifier currencyId = null;
        decimal value;
        public CurrencyAmount(CurrencyIdentifier currencyId, decimal value) {
            this.currencyId = currencyId;
            this.value = value;
        }
        #region IPrice Members

        [DataMember]
        public CurrencyIdentifier CurrencyId {
            get { return currencyId; }
            set { }
        }

        [DataMember]
        public decimal Value {
            get { return value; }
            set { }
        }

        #endregion

        public override bool Equals(object obj) {
            CurrencyAmount currencyAmount = obj as CurrencyAmount;
            if (currencyAmount != null) {
                return currencyAmount.value.Equals(this.value);
            }
            decimal target = Convert.ToDecimal(obj);
            return Value.Equals(target);
        }
        public override int GetHashCode() {
            return value.GetHashCode();
        }
    }
}
