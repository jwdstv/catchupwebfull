﻿
namespace Dstv.Online.VideoOnDemand {
    internal interface IPagingCriteria {
        int PageNumber { get; }
        int ItemsPerPage { get; }
    }
}
