﻿using Dstv.Online.VideoOnDemand.Content;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Player;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand.Security;
namespace Dstv.Online.VideoOnDemand {
    public interface IIntegrationBroker {
        ICatalogue Catalogue { get; }
        ICatalogueSearch CatalogueSearch { get; }
        IContentManagement ContentManagement { get; }
        IGeotargeting Geotargeting { get; }
        DstvWallet DstvWallet { get; }
        IUserProfile UserProfile { get; }
        IEntitlement Entitlement { get; }
        
    }
}
