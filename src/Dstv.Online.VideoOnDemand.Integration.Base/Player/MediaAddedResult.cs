﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Player {
    [DataContract(Namespace = DstvConstants.DstvNamespace)]
    public class MediaAddedResult : IMediaAddedResult {
        public MediaAddedResult(OperationResults result) {
            this.result = result;
        }
        #region IMediaAddedResult Members

        private OperationResults result = OperationResults.Indeterminate;
        [DataMember]
        public OperationResults Result {
            get { return result; }
            set { }
        }

        #endregion
    }
}
