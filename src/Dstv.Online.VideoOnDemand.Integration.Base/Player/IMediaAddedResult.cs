﻿
namespace Dstv.Online.VideoOnDemand.Player {
    internal interface IMediaAddedResult {
        OperationResults Result { get; }
    }
}
