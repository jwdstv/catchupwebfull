﻿using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Transactions;

namespace Dstv.Online.VideoOnDemand.Player {
    public interface IDesktopPlayer {
        MediaAddedResult AddMedia(CustomerIdentifier customerId, CatalogueItemIdentifier mediaId);
        bool IsDesktopPlayerInstalled();
    }


}
