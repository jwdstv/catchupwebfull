﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerifyAccount.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.VerifyAccount" %>

<%@ Register src="Controls/Verifier.ascx" tagname="Verifier" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <uc1:Verifier ID="Verifier1" runat="server" />
    </form>
</body>
</html>--%>

<%@ Page Title="VerifyAccount" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master" AutoEventWireup="true" CodeBehind="VerifyAccount.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.VerifyAccount" %>
<%@ Register Src="~/Controls/Verifier.ascx" TagPrefix="uc1" TagName="Verifier" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
Verify Account
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <uc1:Verifier ID="Verifier1" runat="server" DefaultTab="catchup" />
</asp:Content>
