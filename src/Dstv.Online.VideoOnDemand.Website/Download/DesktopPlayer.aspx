﻿<%@ Page Title="Desktop Player" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master"
    AutoEventWireup="true" CodeBehind="DesktopPlayer.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Download.DesktopPlayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <script src="../Scripts/jquery.browser.js" type="text/javascript"></script>
    <script src="../Scripts/systemRequirements.js" type="text/javascript"></script>
    <script src="../Scripts/Site.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
   <!-- 3 COLUMN LAYOUT START -->
   <div class="block980_bg">
   <div class="hdrWht24">DESKTOP PLAYER</div>
   </div>

   <div class="cols3">
      <div>
         <div class="block300">
            <div class="floatLeft">
               <h6>CatchUp on popular shows and sport whenever you like</h6>
               <div class="floatLeft"><br/>
                  Download the On Demand Desktop Player to view your 
favourite TV shows, sports and movies with CatchUp</div>
               <div class="floatLeft"><br/>
                  <img src="/app_themes/onDemand/images/download_screen.png" height="217" alt=""/><br/>
                  <img src="/app_themes/onDemand/images/desktopplayer.png" width="300" height="207" alt=""/></div>
            </div>
         </div>
         <!--  <button value="submit" class="submitBtn"><span>Install</span></button>-->
         <div class="block300">
            <div class="floatLeft"> 
            <iframe scrolling="no" src="<%= ConfigurationManager.AppSettings["DownloadPlayerLink"] %>"
                            width="300" height="85" frameborder="0" marginheight="0" marginwidth="0" style="overflow: hidden;">
                        </iframe>

            <%--<img src="/app_themes/onDemand/images/DesktopInstall.jpg" width="300" height="74" alt=""/> --%>
            
            </div>
            <div class="padding20px"></div>
            <div class="greyBgTxt">
                        <div class="floatLeft">
                            <!-- BEGIN SYSTEM DIAGNOSTICS -->
                            <h3>
                                Is my computer ready?</h3>
                            <br />
                            <br />
                            <br />
                            <div class="dwnlMainTbl">
                                <div class="reqs">
                                    <img id="imgOS" src="/app_themes/onDemand/images/icon-pass.gif" alt=" " align="absmiddle" />Operating
                                    system</div>
                                <div class="reqsUser">
                                    <div id="divOS">
                                        &lt;Operating system&gt;</div>

                                         <div id="divFailOS_WinVer" class="errorTxt" style="display: none;">
                                        DStv On Demand Desktop is only available on Windows XP (Service Pack 2) and higher.
                                        </div>
                                        <div id="divFailOS_MacVer" class="errorTxt" style="display: none;">
                                        DStv On Demand Desktop is only available on Mac OS X 10.6.4 and higher.
                                        </div>

                                    <div id="divFailOS_NonWin" class="errorTxt" style="display: none;">
                                        DStv On Demand Desktop is only available on Windows XP (Service Pack 2) and higher.
                                        You cannot currently download videos to Apple Macs or other operating systems. However,
                                        you may still stream videos if you have Adobe Flash installed.</div>
                                    <div id="divFailOS_x64" class="errorTxt" style="display: none;">
                                        DStv On Demand Desktop is optimised for 32-bit operationg system. Please let us
                                        know via our Beta response about any issues you encounter on 64-bit systems.</div>
                                </div>
                            </div>
                            <div class="hr_dotline1">
                            </div>
                            <div class="dwnlMainTbl">
                                <div class="reqs">
                                    <asp:Image ID="imgCountry" runat="server" ImageUrl="~/App_Themes/onDemand/images/icon-pass.gif"
                                        ImageAlign="AbsMiddle" AlternateText=" " />Country</div>
                                <div class="reqsUser">
                                    <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                                    <div id="divFailCountry" runat="server" class="errorTxt" style="display: none;">
                                        DStv On Demand Desktop is not available for this country.</div>
                                </div>
                            </div>
                           <%-- <div id="divMediaMain" class="dwnlMainTbl">
                                <div class="hr_dotline1">
                                </div>
                                <div class="reqs">
                                    <img id="imgMedia" src="/app_themes/onDemand/images/icon-pass.gif" alt=" " align="absmiddle" />Media
                                    Player</div>
                                <div class="reqsUser">
                                    <div id="divMedia">
                                        &lt;Media Player&gt;
                                    </div>
                                    <div id="divFailMedia" style="display: none;">
                                        DStv On Demand requires Windows Media Player V11 and above. <a href="http://windows.microsoft.com/en-ZA/windows/downloads/windows-media-player"
                                            target="_blank">Click here</a> to download the latest version and <a href="http://drmlicense.one.microsoft.com/Indivsite/en/indivit.asp?force=1"
                                                target="_blank">click here</a> for the latest security patch.
                                    </div>
                                    <div id="divPassMedia" style="display: none;">
                                        <a href="http://drmlicense.one.microsoft.com/Indivsite/en/indivit.asp?force=1" target="_blank">
                                            Click here</a> for the latest security patch.
                                    </div>
                                </div>
                            </div>--%>
                            <div class="hr_dotline1">
                            </div>
                            <div class="dwnlMainTbl">
                                <div class="reqs">
                                    <img id="imgBrowser" src="/app_themes/onDemand/images/icon-pass.gif" alt=" " align="absmiddle" />Web
                                    browser</div>
                                <div class="reqsUser">
                                    <div id="divBrowser">
                                        &lt;Browser&gt;</div>
                                    <div id="divFailBrowserIE" class="errorTxt" style="display: none;">
                                        DStv On Demand requires Internet Explorer v7 or higher. <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx"
                                            target="_blank">Click here</a> to download the latest version.</div>
                                </div>
                            </div>
                            <%--<div id="divActiveXMain" class="dwnlMainTbl">
                                <div class="hr_dotline1">
                                </div>
                                <div class="reqs">
                                    <img id="imgActiveX" src="/app_themes/onDemand/images/icon-pass.gif" alt=" " align="absmiddle" />Active
                                    X</div>
                                <div class="reqsUser">
                                    <div id="divActiveX">
                                        &lt;Status&gt;</div>
                                    <div id="divFailActiveX" class="errorTxt" style="display: none;">
                                        DStv On Demand requires Active X to be enabled.</div>
                                </div>
                            </div>--%>
                            <div class="hr_dotline1">
                            </div>
                            <div class="dwnlMainTbl">
                                <div class="reqs">
                                    <img id="imgCookie" src="/app_themes/onDemand/images/icon-pass.gif" alt=" " align="absmiddle" />Cookies</div>
                                <div class="reqsUser">
                                    <div id="divCookie">
                                        &lt;Status&gt;</div>
                                    <div id="divFailCookie" class="errorTxt" style="display: none;">
                                        DStv On Demand requires cookies to be enabled.</div>
                                </div>
                            </div>
                            <div class="hr_dotline1">
                            </div>
                            <div class="dwnlMainTbl">
                                <div class="reqs">
                                    <img id="imgJS" src="/app_themes/onDemand/images/icon-pass.gif" alt=" " align="absmiddle" />Javascript</div>
                                <div class="reqsUser">
                                    <div id="divJS">
                                        &lt;Status&gt;</div>
                                    <div id="divFailJS" class="errorTxt" style="display: none;">
                                        DStv On Demand requires Javascript to be enabled.</div>
                                </div>
                            </div>
                            <!-- END SYSTEM DIAGNOSTICS -->
                        </div>
                    </div>
         </div>
      </div>
      <div class="block300">
         <h5>On Demand Desktop Player requires:</h5>
         <div class="greyHdrLine"></div>
         <div class="floatLeft">
              <h3>Windows:</h3>
              <br/>
              <br/>
              <ul class="basList">
                 <li>Operating System: Windows 7,Windows Vista; Windows XP Service Pack 2</li>
                 <li>Intel Core™ Duo 1.83GHz or faster processor</li>
                 <li>512MB of RAM</li>
                 <br/>
              </ul>
         </div>
         <div class="padding10px"></div>
         <div class="floatLeft">
              <h3>Mac OS:</h3>
              <br/>
              <br/>
              <ul class="basList">
                 <li>Operating System: Apple Mac OS X 10.6.4 or above</li>
                 <li>Intel Core™ Duo 1.83GHz or faster processor</li>
                 <li>512MB of RAM</li>
                 <br/>
              </ul>
         </div>

         <div class="padding10px"></div>
         <div class="floatLeft">
              <h3>BROWSER</h3>
              <br/>
              <br/>
              <ul class="basList">
                <li>Internet Explorer 7 +</li>
                <li>Safari 5 +</li>
                <li>Chrome 11 +</li>
                <li>Firefox 4 +</li>
                 <br/>
              </ul>
         </div>

         <div class="padding10px"></div>
         <div class="floatLeft">
         		<h3>General:</h3>
              <br/>
              <br/>
              <ul class="basList">
                 <li>Minimum 3GB free disk space for installation and media storage</li>
                 <li>Uncapped ADSL with connection speed of at least 1024kbps</li>
                 <li>Administrator rights for the computer to install the player</li>
                 <li>Screen resolution of at least 1280 x 1024</li>
                 <%--<li>The latest Windows Media Player and Windows Media Player Security Patches</li>--%>
                 <li>Silverlight 4 runtime</li>
                 <br/>
              </ul>
         </div>
         
      </div>
    
   </div>
   <!-- 3 COLUMN LAYOUT END -->
    <script type="text/javascript">
        var failOS = false;

        //Operating System
        $('#divOS').text(OSName);
        if (!(OSWinXPHigher || is_macosx10_6_4Up)) {
            if (!is_macosx10_6_4Up) {
                failOS = true;
                $('#divFailOS_MacVer').css('display', '');
            }
            else if (!OSWinXPHigher) {
                failOS = true;
                $('#divFailOS_WinVer').css('display', '');
            }
        }

        if (failOS) {
            $('#imgOS').attr('src', $('#imgOS').attr('src').replace(/pass\.gif$/i, 'fail.gif'));
        }

        //Media Player
//        if ($.browser.name == 'msie') {
//            if (!i_mediaplayer) {
//                $('#divMedia').text('Not Detected');
//                $('#divFailMedia').css('display', '');
//                $('#imgMedia').attr('src', $('#imgMedia').attr('src').replace(/pass\.gif$/i, 'fail.gif'));
//            }
//            else {
//                $('#divMedia').text('Windows Media Player v' + split[0]);
//                $('#divPassMedia').css('display', '');
//            }
//        }
//        else {
//            $('#divMediaMain').css('display', 'none');
//        }

        //Web Browser
        if ($.browser.name == 'msie') {
            $('#divBrowser').text(navigator.appName + ' ' + $.browser.version);
        }
        else {
            $('#divBrowser').text($.browser.name[0].toUpperCase() + $.browser.name.substr(1, $.browser.name.length - 1));
        }

        if (navigator.appName != 'Netscape' && ($.browser.name != 'msie' || $.browser.version < 7)) {
            $('#divFailBrowser').css('display', '');
            $('#imgBrowser').attr('src', $('#imgBrowser').attr('src').replace(/pass\.gif$/i, 'fail.gif'));
        }


        //ActiveX
//        if ($.browser.name == 'msie') {
//            if (!i_activex) {
//                $('#divActiveX').text('Disabled');
//                $('#divFailActiveX').css('display', '');
//                $('#imgActiveX').attr('src', $('#imgActiveX').attr('src').replace(/pass\.gif$/i, 'fail.gif'));
//            }
//            else {
//                $('#divActiveX').text('Enabled');
//            }
//        }
//        else {
//            $('#divActiveXMain').css('display', 'none');
//        }

        //Cookies
        if (!is_cookie) {
            $('#divCookie').text('Disabled');
            $('#divFailCookie').css('display', '');
            $('#imgCookie').attr('src', $('#imgCookie').attr('src').replace(/pass\.gif$/i, 'fail.gif'));
        }
        else {
            $('#divCookie').text('Enabled');
        }

        //Javascript
        if (!is_cookie) {
            $('#divJS').text('Disabled');
            $('#divFailJS').css('display', '');
            $('#imgJS').attr('src', $('#imgJS').attr('src').replace(/pass\.gif$/i, 'fail.gif'));
        }
        else {
            $('#divJS').text('Enabled');
        }
    </script>
</asp:Content>
