﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Globalization;

namespace Dstv.Online.VideoOnDemand.Website.Download
{
	public partial class DesktopPlayer : System.Web.UI.Page
	{
        //public string DownloadLink;
        //public bool CanDownload;

        protected void Page_Load(object sender, EventArgs e)
        {
            //lblCountry.Text = ClassLibrary.WebsiteSettings.SupportedTerritories.TerritoryName();
            if (!ClassLibrary.WebsiteSettings.SupportedTerritories.IsUserInSupportedTerritory(ClassLibrary.SiteEnums.ProductType.OnDemand))
            {
               //TODO:add code to show that the users country is not supported.

                //imgCountry.ImageUrl = System.Text.RegularExpressions.Regex.Replace(imgCountry.ImageUrl, @"pass\.gif$", "fail.gif", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //divFailCountry.Style["display"] = "";
            }
            else
            {
                ITerritory territory = IntegrationBroker.Instance.Geotargeting.ResolveUserTerritory();

                lblCountry.Text = territory.Name.ToString();
            }
            ////check if the user is logged in before doing anything("magic" -Chris de Kock 19 October 2010) added by Oliver 
            ////-------------------------------------------------------------------------------
            //bool isLoggedIn = false;
            //if (SessionManager.GetSession("LogedInUser") != null)
            //{
            //    if (SessionManager.GetSession("LogedInUser").ToLower() == "true")
            //        isLoggedIn = true;
            //}

            //if (isLoggedIn)
            //{
            //    ImageButton1.Visible = true;
            //    LblLogin.Visible = false;
            //    IrdetoAPIWrapper.IrdetoAuthorize();
            //}
            //else
            //{
            //    ImageButton1.Visible = false;
            //    LblLogin.Visible = true;
            //}
            //-------------------------------------------------------------------------------


        }



        //[System.Web.Services.WebMethod()]
        //public static bool CanPlayContent(string _contenType, string _genreName, string _videoTitle, string _connectID)
        //{
        //    try
        //    {
        //        bool result = false;

        //        #region Auth Service Code (Redundant)
                

        //        #endregion

        //        if (SessionManager.GetSession("SmartCardNumber") == null || SessionManager.GetSession("MWebPartnerID") == null)
        //            return false;

        //        result = Convert.ToBoolean(SessionManager.GetSession("SmartCardNumber").ToString().Length > 0);

        //        if (result)
        //        {
        //            DstvoConnectWrapper.ConnectWrapper connectWrapper = new DstvoConnectWrapper.ConnectWrapper();
        //            ConnectRemoteImpl.Services.ClientISPAuthenticateResult clientISPAuthenticateResult = connectWrapper.AuthoriseISPUser(SessionManager.GetSession("MWebPartnerID"), HttpContext.Current.Request.UserHostAddress);
        //            result = clientISPAuthenticateResult.Status;
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        General.LogError("NewOnDemand.Download.Default", ex.ToString());
        //        return false;
        //    }


        //}

        protected void btnInstall_Click(object sender, ImageClickEventArgs e)
        {
            //if (!chbTAndC.Checked)
            //{
            //    ValidationError.Display("You need to accept the terms and conditions in order to download this file");
            //}
            //else
            //{

            //}
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("DStvOnDemandDesktop.exe");
        }
	}
}