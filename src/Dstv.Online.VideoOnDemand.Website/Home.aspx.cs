﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website
{
	public partial class Home : BasePage
	{

        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteHomePage"]) && HideBoxOffice)
                {
                    if (ConfigurationManager.AppSettings["SiteHomePage"].ToLower() != Request.ServerVariables["SCRIPT_NAME"].ToLower())
                    {
                        
                        Response.Redirect(ConfigurationManager.AppSettings["SiteHomePage"], false);
                        //Response.Redirect("http://test");
                    }
                }
            }
            catch (Exception ex) {

                throw;
            }
 
            
        }

        protected void Page_Load ( object sender, EventArgs e )
		{

		}

	}
}