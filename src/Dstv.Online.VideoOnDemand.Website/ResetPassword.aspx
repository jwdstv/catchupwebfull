﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.ResetPassword" %>

<%@ Register src="Controls/PasswordReset.ascx" tagname="PasswordReset" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <uc1:PasswordReset ID="PasswordReset1" runat="server" />
    </form>
</body>
</html>
--%>

<%@ Page Title="VerifyAccount" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.ResetPassword" %>
<%@ Register Src="~/Controls/PasswordReset.ascx" TagPrefix="uc1" TagName="PasswordReset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="block980_bg">
        <div class="hdrWht24">
            PASSWORD RESET
        </div>
    </div>
    <div class="block960">
        <uc1:PasswordReset ID="PasswordReset1" runat="server" />
    </div>
</asp:Content>