﻿<%@ Page Title="Site Map" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master"
    AutoEventWireup="true" CodeBehind="SiteMap.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.SiteMap" %>

<%@ Register Src="~/UserControls/Video/SiteMapVideoHierarchyB.ascx" TagName="SiteMapVideoHierarchy"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Shared/LightTreeView.ascx" TagName="TreeView" TagPrefix="wuc" %>
<%@ OutputCache VaryByParam="none" Duration="600" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="block980_bg">
        <div class="hdrWht24">
            SITE MAP</div>
    </div>
    <div class="block960">
        <table cellspacing="5" style="margin: 0 auto; width: 100%">
            <tr style="width: 100%">
                <td valign="top" style="width: 32%">
                    <h5>
                        On Demand</h5>
                    <br />
                    <br />
                    <a href="/">Home</a><br />
                    <wuc:TreeView ID="tvHome" runat="server" />
                </td>
                <%
                    if (!HideBoxOffice)
                    {
                %>
                <td valign="top" style="width: 32%">
                    <h5>
                        BoxOffice</h5>
                    <br />
                    <br />
                    <wuc:SiteMapVideoHierarchy ID="wucSiteMapVideoHierarchyBoxOffice" runat="server"
                        ClassificationConfigFile="VideoFilterSettings.xml">
                        <VideoFilter ProductType="BoxOffice" VideoTypes="Movie" />
                        <PageLinks VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
                    </wuc:SiteMapVideoHierarchy>
                </td>
                <%
                    } 
                %>
                <td valign="top" style="width: 32%">
                    <h5>
                        CatchUp</h5>
                    <br />
                    <br />
                    <wuc:SiteMapVideoHierarchy ID="wucSiteMapVideoHierarchyCatchUp" runat="server" ClassificationConfigFile="VideoFilterSettings.xml">
                        <VideoFilter ProductType="CatchUp" VideoTypes="Movie,ShowEpisode" />
                        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
                    </wuc:SiteMapVideoHierarchy>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
