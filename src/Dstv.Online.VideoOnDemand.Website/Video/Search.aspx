﻿<%@ Page Title="Video Search Results" Language="C#" MasterPageFile="~/MasterPages/TitleAndVerticalSplitContentWithLeftNav.master"
    AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Video.Search" %>

<%@ Register Src="~/UserControls/Video/VideoGallery.ascx" TagName="VideoGallery"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoFilterList.ascx" TagName="VideoFilterList"
    TagPrefix="wuc" %>
<asp:Content ID="contentTitle" ContentPlaceHolderID="cphTitle" runat="server">
Video Search Results
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">
<wuc:VideoFilterList ID="wucVifeoFilterList" runat="server" ConfigFile="VideoFilterSettings.xml" />
</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="cphRightContent" runat="server">
    <div class="block700top">
        <h4 class="home">
            <strong>Search Results for </strong>
            <img src="/app_themes/onDemand/images/arrow_hdr_grey.gif" width="8" height="14" />
            <asp:Literal ID="litSearchTerm" runat="server">{Search Term}</asp:Literal>
        </h4>
        <span class="grey16">(<asp:Literal ID="litVideoCount" runat="server">{Video Count}</asp:Literal>)</span>
    </div>
    <wuc:VideoGallery ID="wucVideoGallery" runat="server" ItemsPerPage="16" ItemsPerRow="4"
        RuntimeDisplay="Duration">
        <VideoFilter ProductType="OnDemand" VideoTypes="ShowEpisode,Movie,ShowClip,MovieClip,FeatureFilm,MovieTrailer,ShowTrailer"
            SortFilter="SearchResultOrder" />
        <PageLinks VideoDetailUrlForBoxOffice="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
        VideoDetailUrlForCatchUp="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    ShowDetailUrlForCatchUp="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
        VideoDetailUrlForOpenTime="/OpenTime/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrlForOpenTime="/OpenTime/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
</wuc:VideoGallery>
<%--<div id="divNoItems" runat="server" visible="false">
<div class="padding20px">
No videos were found matching your search term, <asp:Literal ID="litSearchTerm2" runat="server"></asp:Literal>. <br /><br />
Please try a different search term or use the 'Filter Video' menu on the left to browse the On Demand videos. 
</div>
</div>--%>
</asp:Content>
