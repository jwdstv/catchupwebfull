﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.Video
{
	public partial class Search : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
            wucVifeoFilterList.HeaderFilter = "FILTER VIDEO";
           
		}

		protected void Page_PreRender ( object sender, EventArgs e )
		{
			if (!string.IsNullOrEmpty(wucVideoGallery.VideoGalleryTitle))
			{
				litSearchTerm.Text = HttpUtility.HtmlEncode(wucVideoGallery.VideoGalleryTitle);
                
				//litSearchTerm2.Text = litSearchTerm.Text;
			}
            //else if (wucVideoGallery.TotalItems > 0)
            //{
            //    litSearchTerm.Text = "not specified: all videos returned";
            //}

            //if (wucVideoGallery.TotalItems > 0)
            //{
            //    wucVideoGallery.Visible = true;
            //    divNoItems.Visible = false;
            //}
            //else
            //{
            //    wucVideoGallery.Visible = false;
            //    divNoItems.Visible = true;
            //}

			litVideoCount.Text = wucVideoGallery.TotalItems.ToString();
		}
	}
}