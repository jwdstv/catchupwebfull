﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo.Util;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Players
{
	public partial class DStvOnlineFlashPlayer : System.Web.UI.UserControl
	{
		private string _videoId = "unknown";
        private string _width = "640";
        private string _height = "360";
        private string _playFeedUrl;

		/// <summary>
		/// 
		/// </summary>
		public string VideoId
		{
			get { return _videoId; }
			set { _videoId = value; }
		}

        public string Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public string Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public string PlayFeedUrl
        {
            //get { return _playFeedUrl; }
            //set { _playFeedUrl = value; }
            get 
            {
                string feedUrl = string.Empty;
                try
                {
                    feedUrl = ConfigValues.GetURL(ConfigurationManager.AppSettings["PlayerFeedURL"]);
                }
                catch (Exception ex)
                {
                    //LogError
                    ClassLibrary.ErrorLogManager.LogError(string.Format("Unable to get feed URL from Web.config. Error: {0}", ex));
                }

                return feedUrl;
            }
        }


		private string _videoProgramType = "unknown";
		/// <summary>
		/// 
		/// </summary>
		public string VideoProgramType
		{
			get { return _videoProgramType; }
			set { _videoProgramType = value; }
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
            
		}

		public void LoadVideo ( string videoId, string videoProgramType, string playFeedUrl, string width, string height)
		{
			_videoId = videoId;            
            //set dimentions
            _width = width;
            _height = height;
            _playFeedUrl = playFeedUrl;
			_videoProgramType = videoProgramType;

			this.Visible = true;
		}

		protected void Page_PreRender ( object sender, EventArgs e )
		{
			//litVideoId.Text = _videoId;
			//litVideoProgramType.Text = _videoProgramType;
		}
	}
}