﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Players
{
	public partial class FlashPlayer : System.Web.UI.UserControl
	{
        /// <summary>
        /// The media item that should be played
        /// </summary>
        private DataContract.ICatalogueItemMedia _mediaItemToPlay = null;

        protected void Page_Load(object sender, EventArgs e) {
        }

        public void LoadVideo(DataContract.ICatalogueItemMedia mediaItemToPlay)
        {
            _mediaItemToPlay = mediaItemToPlay;
            // Do stuff here.

            // finally display control.
            this.Visible = true;
        }

        public int IA
        {
            get
            {
                int _ia = 0;

                if (Request.QueryString["ia"] != null)
                    _ia = Convert.ToInt32(Request.QueryString["ia"]);

                return _ia;
            }
        }
	}
}