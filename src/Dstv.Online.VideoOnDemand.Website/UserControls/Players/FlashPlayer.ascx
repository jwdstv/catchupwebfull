﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlashPlayer.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Players.FlashPlayer" %>
<script type="text/javascript">

    var flashvars = { viewwidth: 536, viewheight: 400, base_url: "/Services/Player/player.ashx?", id: "" + GetQueryString("vid") + "&ia=<%=this.IA%>" };
    var params = { menu: "false", scale: "noScale", allowFullscreen: "true", allowScriptAccess: "always", bgcolor: "#000000" };
    var attributes = { id: "flashcontent" };

    swfobject.embedSWF
        (
            "/Flash/dstv_videoplayer.swf?noCache=" + new Date().getTime(),
            "altContent",
            "536",
            "400",
            "9.0.246",
            "/Flash/expressInstall.swf",
            flashvars,
            params,
            attributes
        );

</script>
