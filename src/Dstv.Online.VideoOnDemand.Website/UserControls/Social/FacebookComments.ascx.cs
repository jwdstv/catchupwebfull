﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;


namespace Dstv.Online.VideoOnDemand.Website.UserControls.Social
{
	public partial class FacebookComments : System.Web.UI.UserControl
	{
        protected void Page_Load(object sender, EventArgs e)
		{
            lblFb.Text = FBSettings.GetIframe();
		}


        private FacebookSettings _fBSettings = new FacebookSettings();

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public FacebookSettings FBSettings
        {
            get { return _fBSettings; }
            set { _fBSettings = value; }
        }
	}
}