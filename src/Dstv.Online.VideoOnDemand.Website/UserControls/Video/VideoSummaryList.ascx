﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoSummaryList.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoSummaryList" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileSummary" Src="~/UserControls/Video/VideoTileSummary.ascx" %>
<div class="block300">
    <h5><asp:Literal ID="litProductTitle" runat="server"></asp:Literal></h5>
    <div class="greyHdrLine"></div>
    <asp:Repeater ID="rptTiles" runat="server">
        <ItemTemplate>
            <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" VideoItem="<%#Container.DataItem %>" SummaryDisplayType="VideoLayoutTextRight" />
        </ItemTemplate>
        <SeparatorTemplate>
            <br />
        </SeparatorTemplate>
    </asp:Repeater>
</div>
