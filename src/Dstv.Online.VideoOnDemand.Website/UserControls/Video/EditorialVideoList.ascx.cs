﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using System.ComponentModel;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
    [DataBindingHandler("System.Web.UI.Design.TextDataBindingHandler, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    public partial class EditorialVideoList : VideoControlBase
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
				PopulateControl();
		}

		#region private void PopulateControl ()
		/// <summary>
		/// 
		/// </summary>
		private void PopulateControl ()
		{
			Modules.VideoCatalogue.EditorialPlaylist editorialPlaylist = DataAccess.VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.List, ListId, VideoFilter);

			if (editorialPlaylist != null)
			{
				ListTitle = editorialPlaylist.PlaylistName;
				PopulateVideoList(editorialPlaylist.CatalogueItemSummaryList.CatalogueItems);
			}
			else
			{
				_itemCount = 0;
			}

			this.Visible = _itemCount > 0;
		}
		#endregion


		#region public void PopulateVideoList ( List<DataContract.VideoItemSummary> videoList )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoList"></param>
        public void PopulateVideoList(DataContract.ICatalogueItem[] videoList)
		{
            if (videoList != null && videoList.Length > 0)
			{
                _itemCount = videoList.Length;

                rptVideoList.DataSource = videoList;
				rptVideoList.DataBind();
			}
			else
			{
				_itemCount = 0;
			}
		}
		#endregion


		#region protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
		/// <summary>
		/// Required for populating the video and show detail Url properties in the VideoTile control object.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				object videoTileObject = e.Item.FindControl("wucVideoTileList");
				if (videoTileObject != null)
				{
					VideoTileList videoTile = (VideoTileList)videoTileObject;
					videoTile.SetUrls(PageLinks);
				}
			}
		}
		#endregion
	}
}