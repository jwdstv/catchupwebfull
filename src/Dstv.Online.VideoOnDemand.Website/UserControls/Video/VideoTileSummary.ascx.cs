﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class VideoTileSummary : VideoTileBase
	{

		#region SummaryDisplayType
		private SiteEnums.VideoSummaryDisplay _summaryDisplayType = SiteEnums.VideoSummaryDisplay.VideoLayoutTextBottom;
		/// <summary>
		/// 
		/// </summary>
		public SiteEnums.VideoSummaryDisplay SummaryDisplayType
		{
			get { return _summaryDisplayType; }
			set { _summaryDisplayType = value; }
		}
		#endregion


		#region PropertyDisplayCount
		private int _propertyDisplayCount = 0;
		/// <summary>
		/// 
		/// </summary>
		public int PropertyDisplayCount
		{
			get { return _propertyDisplayCount; }
		}
		#endregion


		#region IncludeFullAbstractTooltip
		private bool _includeFullAbstractTooltip = false;
		/// <summary>
		/// 
		/// </summary>
		public bool IncludeFullAbstractTooltip
		{
			get { return _includeFullAbstractTooltip; }
			set { _includeFullAbstractTooltip = value; }
		}
		#endregion


		protected void Page_Load ( object sender, EventArgs e )
		{
		}


		protected void Page_PreRender ( object sender, EventArgs e )
		{
			PopulateControl();
		}


		#region private void PopulateControl ()
		/// <summary>
		/// 
		/// </summary>
		private void PopulateControl ()
		{
			divVideoTextRight.Visible = false;
			divVideoTextBottom.Visible = false;
			divVideoTextBottom2.Visible = false;
			divEpisodeLayout.Visible = false;
			divShowLayout.Visible = false;

			switch (_summaryDisplayType)
			{
				case SiteEnums.VideoSummaryDisplay.EpisodeLayout:
					DisplayEpisodeLayout();
					break;
				case SiteEnums.VideoSummaryDisplay.ShowLayout:
					DisplayShowLayout();
					break;
				case SiteEnums.VideoSummaryDisplay.VideoLayoutTextBottom:
					DisplayVideoTextBottom();
					break;
				case SiteEnums.VideoSummaryDisplay.VideoLayoutTextBottom2:
					DisplayVideoTextBottom2();
					break;
				case SiteEnums.VideoSummaryDisplay.VideoLayoutTextRight:
					DisplayVideoTextRight();
					break;
			}
		}
		#endregion


		private void DisplayEpisodeLayout ()
		{
			divEpisodeLayout.Visible = true;

            // Set abstract.
            if (_includeFullAbstractTooltip && VideoAbstractTrimmed.Length < VideoAbstract.Length)
            {
                litDoHoverEp.Visible = false;
                divToolTipEp.Visible = true;
            }
            else
            {
                litDoHoverEp.Visible = true;
                litDoHoverEp.Text = VideoAbstractTrimmed;
                divToolTipEp.Visible = false;
            }
		}


		private void DisplayShowLayout ()
		{
			divShowLayout.Visible = true;
		}

		private void DisplayVideoTextRight ()
		{
			divVideoTextRight.Visible = true;

			if (VideoProductType == ProductTypes.BoxOffice)
			{
				trAirDateR.Visible = false;
				trEpisodeTitleR.Visible = false;
				_propertyDisplayCount = 3;

				if (IsForthcomingRelease)
				{
					trAvailableDateR.Visible = true;
					trExpiryDateR.Visible = false;
				}
				else
				{
					trAvailableDateR.Visible = false;
					trExpiryDateR.Visible = true;
				}
			}
			else
			{
				_propertyDisplayCount = 6;
				if (!IsEpisode)
				{
					trEpisodeTitleR.Visible = false;
					_propertyDisplayCount--;
				}

				if (!HasAirDate)
				{
					trAirDateR.Visible = false;
					_propertyDisplayCount--;
				}

				
				trExpiryDateR.Visible = false;
				_propertyDisplayCount--;

				// CatchUp / OpenTime do NOT have forthcoming releases
				trAvailableDateR.Visible = false;
				_propertyDisplayCount--;

			}

			// Set abstract.
			if (_includeFullAbstractTooltip && VideoAbstractTrimmed.Length < VideoAbstract.Length)
			{
				litDoHoverR.Visible = false;
				divTooltipR.Visible = true;
			}
			else
			{
				litDoHoverR.Visible = true;
				litDoHoverR.Text = VideoAbstractTrimmed;
				divTooltipR.Visible = false;
			}
		}

		private void DisplayVideoTextBottom ()
		{
			divVideoTextBottom.Visible = true;

			if (VideoProductType == ProductTypes.BoxOffice)
			{
				trAirDateB.Visible = false;
				trEpisodeTitleB.Visible = false;
				trAbstractB.Visible = false;

				if (IsForthcomingRelease)
				{
					trAvailableDateB.Visible = true;
					trExpiryDateB.Visible = false;
				}
				else
				{
					trAvailableDateB.Visible = false;
					trExpiryDateB.Visible = true;
				}
			}
			else
			{
				trEpisodeTitleB.Visible = IsEpisode;
				trAirDateB.Visible = HasAirDate;

				// Not as per spec.
				trExpiryDateB.Visible = VideoProductType == ProductTypes.CatchUp && HasExpiryDate;

				// CatchUp / OpenTime do NOT forthcoming releases
				trAvailableDateB.Visible = false;

				// Set abstract.
				if (_includeFullAbstractTooltip && VideoAbstractTrimmed.Length < VideoAbstract.Length)
				{
					litDoHoverB.Visible = false;
					divTooltipB.Visible = true;
				}
				else
				{
					litDoHoverB.Visible = true;
					litDoHoverB.Text = VideoAbstractTrimmed;
					divTooltipB.Visible = false;
				}
			}
		}

		private void DisplayVideoTextBottom2 ()
		{
			divVideoTextBottom2.Visible = true;

			if (VideoProductType == ProductTypes.BoxOffice)
			{
				trAirDateB2.Visible = false;
				trEpisodeTitleB2.Visible = false;
				// trAbstractB2.Visible = false;

				if (IsForthcomingRelease)
				{
					trAvailableDateB2.Visible = true;
					trExpiryDateB2.Visible = false;
				}
				else
				{
					trAvailableDateB2.Visible = false;
					trExpiryDateB2.Visible = true;
				}
			}
			else
			{
				trEpisodeTitleB2.Visible = IsEpisode;
				trAirDateB2.Visible = HasAirDate;

				// Not as per spec.
				trExpiryDateB2.Visible = VideoProductType == ProductTypes.CatchUp && HasExpiryDate;

				// CatchUp / OpenTime do NOT have forthcoming
				trAvailableDateB2.Visible = false;

				// Set abstract.
				if (_includeFullAbstractTooltip && VideoAbstractTrimmed.Length < VideoAbstract.Length)
				{
					litDoHoverB2.Visible = false;
					divTooltipB2.Visible = true;
				}
				else
				{
					litDoHoverB2.Visible = true;
					litDoHoverB2.Text = VideoAbstractTrimmed;
					divTooltipB2.Visible = false;
				}
			}
		}

	}
}