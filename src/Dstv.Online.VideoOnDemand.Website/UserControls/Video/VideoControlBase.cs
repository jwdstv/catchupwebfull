﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using System.ComponentModel;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{

    #region public class VideoControlBasePageLinks
    /// <summary>
    /// 
    /// </summary>
    public class VideoControlBasePageLinks
    {
        private string _videoDetailUrl = "";
        private string _videoDetailUrlForBoxOffice = "";
        private string _videoDetailUrlForCatchUp = "";
        private string _videoDetailUrlForOpenTime = "";
        private string _showDetailUrl = "";
        private string _showDetailUrlForBoxOffice = "";
        private string _showDetailUrlForCatchUp = "";
        private string _showDetailUrlForOpenTime = "";
        private string _moreUrl = "";

        #region public string VideoDetailUrl
        /// <summary>
        /// 
        /// </summary>
        public string VideoDetailUrl
        {
            get { return _videoDetailUrl; }
            set { _videoDetailUrl = value; }
        }
        #endregion


        #region public string ShowDetailUrl
        /// <summary>
        /// 
        /// </summary>
        public string ShowDetailUrl
        {
            get { return _showDetailUrl; }
            set { _showDetailUrl = value; }
        }
        #endregion


        #region public string VideoDetailUrlForBoxOffice
        /// <summary>
        /// 
        /// </summary>
        public string VideoDetailUrlForBoxOffice
        {
            get { return _videoDetailUrlForBoxOffice; }
            set { _videoDetailUrlForBoxOffice = value; }
        }
        #endregion


        #region public string ShowDetailUrlForBoxOffice
        /// <summary>
        /// 
        /// </summary>
        public string ShowDetailUrlForBoxOffice
        {
            get { return _showDetailUrlForBoxOffice; }
            set { _showDetailUrlForBoxOffice = value; }
        }
        #endregion


        #region public string VideoDetailUrlForCatchUp
        /// <summary>
        /// 
        /// </summary>
        public string VideoDetailUrlForCatchUp
        {
            get { return _videoDetailUrlForCatchUp; }
            set { _videoDetailUrlForCatchUp = value; }
        }
        #endregion


        #region public string ShowDetailUrlForCatchUp
        /// <summary>
        /// 
        /// </summary>
        public string ShowDetailUrlForCatchUp
        {
            get { return _showDetailUrlForCatchUp; }
            set { _showDetailUrlForCatchUp = value; }
        }
        #endregion


        #region public string VideoDetailUrlForOpenTime
        /// <summary>
        /// 
        /// </summary>
        public string VideoDetailUrlForOpenTime
        {
            get { return _videoDetailUrlForOpenTime; }
            set { _videoDetailUrlForOpenTime = value; }
        }
        #endregion


        #region public string ShowDetailUrlForOpenTime
        /// <summary>
        /// 
        /// </summary>
        public string ShowDetailUrlForOpenTime
        {
            get { return _showDetailUrlForOpenTime; }
            set { _showDetailUrlForOpenTime = value; }
        }
        #endregion


        #region public string MoreUrl
        /// <summary>
        /// 
        /// </summary>
        public string MoreUrl
        {
            get { return _moreUrl; }
            set { _moreUrl = value; }
        }
        #endregion


        public string GetFormattedVideoDetailUrl(DataContract.ICatalogueItem catalogueItem)
        {
            string url;
            if (catalogueItem.ProductTypeId.Value == ProductTypes.BoxOffice)
            {
                url = string.IsNullOrEmpty(_videoDetailUrlForBoxOffice) ? _videoDetailUrl : _videoDetailUrlForBoxOffice;
            }
            else if (catalogueItem.ProductTypeId.Value == ProductTypes.CatchUp)
            {
                url = string.IsNullOrEmpty(_videoDetailUrlForCatchUp) ? _videoDetailUrl : _videoDetailUrlForCatchUp;
            }
            else if (catalogueItem.ProductTypeId.Value == ProductTypes.OpenTime)
            {
                url = string.IsNullOrEmpty(_videoDetailUrlForOpenTime) ? _videoDetailUrl : _videoDetailUrlForOpenTime;
            }
            else
            {
                return "";
            }
            return url.
                Replace(SiteConstants.FormatIdentifiers.EpisodeNumber, HttpUtility.UrlEncode(catalogueItem.EpisodeNumber.HasValue ? catalogueItem.EpisodeNumber.Value.ToString() : "")).
                Replace(SiteConstants.FormatIdentifiers.EpisodeTitle, HttpUtility.UrlEncode(catalogueItem.VideoTitle)).
                Replace(SiteConstants.FormatIdentifiers.ProductType, catalogueItem.ProductTypeId.Value.ToString()).
                Replace(SiteConstants.FormatIdentifiers.ProgramId, catalogueItem.ProgramId.HasValue ? catalogueItem.ProgramId.Value.ToString() : "").
                Replace(SiteConstants.FormatIdentifiers.ProgramTitle, HttpUtility.UrlEncode(catalogueItem.VideoTitle)).
                Replace(SiteConstants.FormatIdentifiers.SeasonNumber, HttpUtility.UrlEncode(catalogueItem.SeasonNumber.HasValue ? catalogueItem.SeasonNumber.Value.ToString() : "")).
                Replace(SiteConstants.FormatIdentifiers.VideoId, HttpUtility.UrlEncode(catalogueItem.CatalogueItemId.Value)).
                Replace(SiteConstants.FormatIdentifiers.VideoTitle, HttpUtility.UrlEncode(catalogueItem.VideoTitle)).
                Replace(SiteConstants.FormatIdentifiers.Id, HttpUtility.UrlEncode(catalogueItem.CatalogueItemId.Value));
        }
    }
    #endregion


    public class VideoControlBase : System.Web.UI.UserControl
    {
        #region Properties

        #region ListTitle
        private string _listTitle = "";
        /// <summary>
        /// 
        /// </summary>
        public string ListTitle
        {
            get { return _listTitle; }
            set { _listTitle = value; }
        }
        #endregion


        #region ListId
        private string _listId = "";
        /// <summary>
        /// 
        /// </summary>
        [Browsable(true)]
        [Bindable(true, BindingDirection.OneWay)]
        public string ListId
        {
            get { return _listId; }
            set { _listId = value; }
        }
        #endregion


        #region RuntimeDisplay
        private SiteEnums.VideoRuntimeDisplay _runtimeDisplay = SiteEnums.VideoRuntimeDisplay.RuntimeInMinutes;
        /// <summary>
        /// 
        /// </summary>
        public SiteEnums.VideoRuntimeDisplay RuntimeDisplay
        {
            get { return _runtimeDisplay; }
            set { _runtimeDisplay = value; }
        }
        #endregion


        #region VideoFilter
        private DataAccess.VideoFilter _videoFilter = new DataAccess.VideoFilter();
        /// <summary>
        /// 
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataAccess.VideoFilter VideoFilter
        {
            get { return _videoFilter; }
            set { _videoFilter = value; }
        }
        #endregion


        #region PageLinks
        private VideoControlBasePageLinks _pageLinks = new VideoControlBasePageLinks();
        /// <summary>
        /// 
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public VideoControlBasePageLinks PageLinks
        {
            get { return _pageLinks; }
            set { _pageLinks = value; }
        }
        #endregion


        #region ItemCount
        internal int _itemCount = 0;
        /// <summary>
        /// 
        /// </summary>
        public int ItemCount
        {
            get { return _itemCount; }
        }
        #endregion

        #endregion
        #region public string MoreUrlFormatted
        /// <summary>
        /// 
        /// </summary>
        public string MoreUrlFormatted
        {
            get
            {
                return GetMoreUrlFormatted();
            }

        }

        private bool showMoreLink = true;
        /// <summary>
        /// Indicates if the More link should be shown
        /// </summary>
        public bool ShowMoreLink
        {
            get
            {
                return showMoreLink;
            }
            set
            {
                showMoreLink = value;
            }
        }

        private string GetMoreUrlFormatted()
        {
            string formattedMoreUrl = SiteUtils.Url.SEO.GetSEOFriendlyUrl(_pageLinks.MoreUrl);

            formattedMoreUrl = formattedMoreUrl.
                Replace(SiteConstants.FormatIdentifiers.ListId, HttpUtility.UrlEncode(ListId)).
                Replace(SiteConstants.FormatIdentifiers.SortFilter, VideoFilter.SortFilter.ToString()).
                Replace(SiteConstants.FormatIdentifiers.ListTitle, HttpUtility.UrlEncode(ListTitle)).
                Replace(SiteConstants.FormatIdentifiers.ClassificationName, HttpUtility.UrlEncode(ListTitle)).
                Replace(SiteConstants.FormatIdentifiers.ClassificationId, HttpUtility.UrlEncode(VideoFilter.ClassificationId)).
                Replace(SiteConstants.FormatIdentifiers.ClassificationType, SiteEnums.VideoClassificationType.Unknown.ToString());

            return formattedMoreUrl;
        }
        #endregion


        #region public string DisplayNameForProductType
        /// <summary>
        /// 
        /// </summary>
        public string DisplayNameForProductType
        {
            get { return SiteConstants.DisplayNames.GetDisplayNameForSite(_videoFilter.ProductType); }
        }
        #endregion


        #region public string DateHeaderForProductType
        /// <summary>
        /// 
        /// </summary>
        public string DateHeaderForProductType
        {
            get { return _videoFilter.ProductType == ProductTypes.BoxOffice || _videoFilter.ProductType == ProductTypes.CatchUp ? "Available until" : "Aired on"; }
        }
        #endregion

    }
}