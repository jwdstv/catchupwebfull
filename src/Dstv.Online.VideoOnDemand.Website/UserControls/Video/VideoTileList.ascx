﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoTileList.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoTileList" %>
<a href="<%=VideoDetailPageUrl%>" class="listItem">
<div class="iconArrow-<%=ProductTypeCssName%>"></div>
<div class="listDesc-ext" title="<%=VideoTooltip%>"><strong><%=VideoFullTitle%></strong></div>
<div class="expDate-ext"><%=VideoListDate%></div>
</a>