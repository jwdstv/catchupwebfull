﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using System.Collections.Specialized;
using Dstv.Online.VideoOnDemand.Integration;


namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class VideoGallery : VideoControlBase
	{

		#region Control Properties

		private SiteEnums.VideoGalleryDisplayType _displayType = SiteEnums.VideoGalleryDisplayType.VideoCategory;
		private string _searchTerm = "";
		private SiteEnums.ProductType _productType;
		
		private int _itemsPerPage = 16;
		/// <summary>
		/// Items per page
		/// </summary>
		public int ItemsPerPage
		{
			get { return _itemsPerPage; }
			set { _itemsPerPage = value; }
		}

		private int _itemsPerRow = 4;

		public int ItemsPerRow
		{
			get { return _itemsPerRow; }
			set { _itemsPerRow = value; }
		}

		private int _totalItems = 0;
		public int TotalItems
		{
			get { return _totalItems; }
		}

		
		private int _pageNumber = 1;
		/// <summary>
		/// The current page number
		/// </summary>
		public int PageNumber
		{
			get { return _pageNumber; }
			set { _pageNumber = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoGalleryTitle
		{
			// Set VideoControlBase ListTitle ??
			get
			{
				if (_displayType == SiteEnums.VideoGalleryDisplayType.VideoEditorial)
				{
					return ListTitle;
				}
				else if (_displayType == SiteEnums.VideoGalleryDisplayType.VideoSearch)
				{
					return _searchTerm;
				}
				else if (_displayType == SiteEnums.VideoGalleryDisplayType.VideoShows)
				{
					return SiteConstants.Urls.ParameterValues.Shows;
				}
				else
				{
					if (VideoFilter.CatalogueCriteria.ClassificationCriteria != null && !string.IsNullOrEmpty(VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemName))
					{
						return VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemName;
					}
					else if (VideoFilter.CatalogueCriteria.ClassificationCriteria != null && VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId != null)
					{
						return "";
					}
					else
					{
						// return SiteContstants.DisplayNames.GetDisplayNameForVideoSortOrder(VideoFilter.SortFilter);
						return string.Format("All {0}", SiteConstants.DisplayNames.GetDisplayNameForSite(VideoFilter.ProductType));
					}
				}
			}
		}

        private string _noResultsReturned = string.Empty;
        public string NoResultsReturned
        {
            get
            {
                return _noResultsReturned;
            }
            set { _noResultsReturned = value; }
        }        

        public string PageUrl { get; set; }

        public string SearchString { get; set; }

		#endregion


		#region protected void Page_Load ( object sender, EventArgs e )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Load ( object sender, EventArgs e )
		{

            this.PageUrl = this.Page.Request.RawUrl;

            if (!string.IsNullOrEmpty(this.PageUrl))
            {
                if (this.PageUrl.Contains("Search.aspx"))
                {
                    this.NoResultsReturned = string.Format("{0}", "No videos were found matching your search term. Please try a different search term or use the 'Filter Video' menu on the left to browse the On Demand videos.");
                }
                else
                {
                    this.NoResultsReturned = "No videos were found in the selected category. Please select a different category..";
                }
            }

			// MMS requires this with classification i.e. classification is not unique to a product type.
			_productType = SiteUtils.Request.GetProductTypeFromRequest();

			// Read VideoFilter catalogue criteria from Url.
			_searchTerm = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.SearchTerm, "").Trim();
			string editorialId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.EditorialListId, null);
			string classificationId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ClassificationId, "");
            string parentClassificationId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ParentClassificationId, "");

			if (!string.IsNullOrEmpty(editorialId))
			{
				_displayType = SiteEnums.VideoGalleryDisplayType.VideoEditorial;
				ListId = editorialId;
			}
			else if (!string.IsNullOrEmpty(_searchTerm))
			{
				_displayType = SiteEnums.VideoGalleryDisplayType.VideoSearch;
			}
			else if (classificationId.Equals(SiteConstants.Urls.ParameterValues.Shows, StringComparison.OrdinalIgnoreCase))
			{
				_displayType = SiteEnums.VideoGalleryDisplayType.VideoShows;

				if (VideoFilter.CatalogueCriteria.ClassificationCriteria == null)
				{
					VideoFilter.CatalogueCriteria.ClassificationCriteria = new DataContract.ClassificationCriteria();
				}
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = CatalogueItemClassificationTypes.ProductType;
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ProductTypeId = new Transactions.ProductTypeIdentifier(SiteEnums.Mapper.MapSiteProductTypeToBaseProductType(_productType));
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemName = SiteConstants.Urls.ParameterValues.Shows;
			}
			else
			{
				_displayType = SiteEnums.VideoGalleryDisplayType.VideoCategory;

				if (VideoFilter.CatalogueCriteria.ClassificationCriteria == null)
				{
					VideoFilter.CatalogueCriteria.ClassificationCriteria = new DataContract.ClassificationCriteria();
				}

				SiteEnums.VideoClassificationType classificationType = SiteEnums.VideoClassificationType.ProgramType_All;
				if (!string.IsNullOrEmpty(classificationId)
					&& Enum.TryParse<SiteEnums.VideoClassificationType>(SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ClassificationType, ""), out classificationType))
				{
					VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId = new DataContract.ClassificationItemIdentifier(classificationId);
					VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = SiteEnums.Mapper.MapVideoClassificationTypeToBaseCatalogueItemClassificationType(classificationType);

                    if (!string.IsNullOrWhiteSpace(parentClassificationId))
                    {
                        VideoFilter.CatalogueCriteria.ClassificationCriteria.ParentClassificationId = new DataContract.ClassificationItemIdentifier(parentClassificationId);
                    }
				}
				else
				{
					// TODO: Use All for ProductType as unspecified default.
					// TODO: What is ClassificationItemId for productType.
					// VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId = new DataContract.ClassificationItemIdentifier(VideoFilter.ProductType.ToString());
					VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = CatalogueItemClassificationTypes.ProductType;
				}

				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemName = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ClassificationName, "");

				VideoFilter.CatalogueCriteria.ClassificationCriteria.ProductTypeId = new Transactions.ProductTypeIdentifier(SiteEnums.Mapper.MapSiteProductTypeToBaseProductType(_productType));
			}

			if (!IsPostBack)
			{
				// Read PageNumber and SortOrder from Url.
				PageNumber = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.PageNumber, 1);
				VideoFilter.SortFilter = SiteUtils.Request.GetQueryStringParameterValue<SiteEnums.VideoSortOrder>(SiteConstants.Urls.ParameterNames.SortFilter, SiteEnums.VideoSortOrder.LatestReleases);

				PopulateSortOrderList();

				PopulateControl();
			}
		}
		#endregion
     
		#region private void CopyUrlCatalogueCriteriaProperties ( DataContract.CatalogueCriteria urlCatalogueCriteria )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="urlCatalogueCriteria"></param>
		private void CopyUrlCatalogueCriteriaProperties ( DataContract.CatalogueCriteria urlCatalogueCriteria )
		{
			if (urlCatalogueCriteria != null)
			{
				VideoFilter.CatalogueCriteria.SortOrder = urlCatalogueCriteria.SortOrder;

				// Classification Criteria
				if (urlCatalogueCriteria.ClassificationCriteria != null)
				{
					if (VideoFilter.CatalogueCriteria.ClassificationCriteria == null)
					{
						VideoFilter.CatalogueCriteria.ClassificationCriteria = new DataContract.ClassificationCriteria();
					}

					if (urlCatalogueCriteria.ClassificationCriteria.ClassificationItemId != null)
					{
						VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId = new DataContract.ClassificationItemIdentifier(urlCatalogueCriteria.ClassificationCriteria.ClassificationItemId.Value);
					}

					if (urlCatalogueCriteria.ClassificationCriteria.ClassificationItemName != null)
					{
						VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemName = urlCatalogueCriteria.ClassificationCriteria.ClassificationItemName;
					}

					if (urlCatalogueCriteria.ClassificationCriteria.ParentClassificationId != null)
					{
						VideoFilter.CatalogueCriteria.ClassificationCriteria.ParentClassificationId = new DataContract.ClassificationItemIdentifier(urlCatalogueCriteria.ClassificationCriteria.ParentClassificationId.Value);
					}

					VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = urlCatalogueCriteria.ClassificationCriteria.ClassificationType;

					// Want ProductType. Territory and VideoProgramTypes to remain from control.
				}
				else if (urlCatalogueCriteria.ClassificationCriteria == null)
				{
					VideoFilter.CatalogueCriteria.ClassificationCriteria = null;
				}

				// Catalogue Item Ids
				if (urlCatalogueCriteria.CatalogueItemIds != null)
				{
					VideoFilter.CatalogueCriteria.CatalogueItemIds = urlCatalogueCriteria.CatalogueItemIds;
				}

				// Want paging and sort order to remain from control.
			}
		}
		#endregion


		#region protected void rptVideoList_ItemDataBound ( object sender, RepeaterItemEventArgs e )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void rptVideoList_ItemDataBound ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Separator)
			{
				// Set vertical or horizontal seperator based on current index/item count in repeater.
				System.Web.UI.HtmlControls.HtmlGenericControl divVerticalSeparator = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divVerticalSeparator");
				System.Web.UI.HtmlControls.HtmlGenericControl divHorizontalSeparator = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divHorizontalSeparator");

				if (rptVideoList.Items.Count % _itemsPerRow == 0)
				{
					divHorizontalSeparator.Visible = true;
					divVerticalSeparator.Visible = false;
				}
				else if (rptVideoList.Items.Count % _itemsPerRow != 0)
				{
					divHorizontalSeparator.Visible = false;
					divVerticalSeparator.Visible = true;
				}
				else
				{
					divHorizontalSeparator.Visible = false;
					divVerticalSeparator.Visible = false;
				}
			}
			else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// Populate video and show detaul Urls on Video Summary Tile.
				object videoTileObject = e.Item.FindControl("wucVideoTileSummary");
				if (videoTileObject != null)
				{
					VideoTileSummary videoTile = (VideoTileSummary)videoTileObject;
                    if (VideoFilter.SortFilter == SiteEnums.VideoSortOrder.ForthcomingReleases)
                    {
                        string forthcoming = string.Format("&{0}=1", SiteConstants.Urls.ParameterNames.IsForthcoming);

                        if (!PageLinks.VideoDetailUrl.ToUpper().Contains(forthcoming))
                        {
                            PageLinks.VideoDetailUrl += forthcoming;
                            //make sure that the metadata displayed is "Available on" instead of "Available until"
                            videoTile.IsForthcomingRelease = true;
                        }
                    }
					videoTile.SetUrls(PageLinks);
					videoTile.SummaryDisplayType = _displayType == SiteEnums.VideoGalleryDisplayType.VideoShows ? SiteEnums.VideoSummaryDisplay.ShowLayout : SiteEnums.VideoSummaryDisplay.VideoLayoutTextBottom2;
					videoTile.VideoItem = (DataContract.ICatalogueItem)e.Item.DataItem;
				}
			}
		}
		#endregion


		#region protected void ddlListOrder_SelectedIndexChanged ( object sender, EventArgs e )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ddlListOrder_SelectedIndexChanged ( object sender, EventArgs e )
		{
			// TODO: Set new order and populate the control - populate at 1st page ??
			if (IsPostBack)
			{
				PageNumber = 1;
				SetSortOrderFromList();
				PopulateControl();
			}
		}
		#endregion


		#region protected void wucPagerControl_SelectedPageIndexChanged ( object sender, UserControls.Navigation.PagingControlEventArgs e )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void wucPagerControl_SelectedPageIndexChanged ( object sender, UserControls.Navigation.PagingControlEventArgs e )
		{
			if (IsPostBack && sender is UserControls.Navigation.PagingControl)
			{
				PageNumber = e.SelectedPageNumber;
				SetSortOrderFromList();
				PopulateControl();
			}
		}
		#endregion


		#region private void SetSortOrderFromList ()
		/// <summary>
		/// 
		/// </summary>
		private void SetSortOrderFromList ()
		{
			SiteEnums.VideoSortOrder sortOrder = SiteEnums.VideoSortOrder.LatestReleases;
			Enum.TryParse<SiteEnums.VideoSortOrder>(selListOrder.Items[selListOrder.SelectedIndex].Value, out sortOrder);
			VideoFilter.SortFilter = sortOrder;
		}
		#endregion


		#region private void PopulateSortOrderList ()
		/// <summary>
		/// 
		/// </summary>
		private void PopulateSortOrderList ()
		{
			selListOrder.Items.Clear();
			List<SiteEnums.VideoSortOrder> selectSortOrders = new List<SiteEnums.VideoSortOrder>();
			
			switch (_displayType)
			{
				case SiteEnums.VideoGalleryDisplayType.VideoCategory:
				case SiteEnums.VideoGalleryDisplayType.VideoEditorial:
					selectSortOrders.Add(SiteEnums.VideoSortOrder.LatestReleases);
					selectSortOrders.Add(SiteEnums.VideoSortOrder.LastChance);
					if (_productType == SiteEnums.ProductType.BoxOffice)
					{
						selectSortOrders.Add(SiteEnums.VideoSortOrder.ForthcomingReleases);
					}
					else
					{
						selectSortOrders.Add(SiteEnums.VideoSortOrder.MostPopular);
					}
					//selectSortOrders.Add(SiteEnums.VideoSortOrder.Title); 
					selectSortOrders.Add(SiteEnums.VideoSortOrder.ProgramName); 
					break;
				case SiteEnums.VideoGalleryDisplayType.VideoSearch:
					selectSortOrders.Add(SiteEnums.VideoSortOrder.SearchResultOrder);
					selectSortOrders.Add(SiteEnums.VideoSortOrder.LatestReleases);
					selectSortOrders.Add(SiteEnums.VideoSortOrder.LastChance);
					// Add most popular ?? Box Office videos in results ??
					selectSortOrders.Add(SiteEnums.VideoSortOrder.MostPopular);
                    //selectSortOrders.Add(SiteEnums.VideoSortOrder.Title); 
                    selectSortOrders.Add(SiteEnums.VideoSortOrder.ProgramName);
                    break;
				case SiteEnums.VideoGalleryDisplayType.VideoShows:
                    //selectSortOrders.Add(SiteEnums.VideoSortOrder.Title); 
                    selectSortOrders.Add(SiteEnums.VideoSortOrder.ProgramName);
                    selectSortOrders.Add(SiteEnums.VideoSortOrder.ShowsByLatestVideo);
					break;
			}
			
			foreach (SiteEnums.VideoSortOrder selectSortOrder in selectSortOrders)
			{
				NameValueCollection urlParams = new NameValueCollection();
				urlParams.Add(SiteConstants.Urls.ParameterNames.SortFilter, selectSortOrder.ToString());
				urlParams.Add(SiteConstants.Urls.ParameterNames.PageNumber, "1");

				ListItem selListOrderOption = new ListItem(
					SiteConstants.DisplayNames.GetShortDisplayNameForVideoSortOrder(selectSortOrder),
					SiteUtils.Request.AddParametersToUri(Request.Url, urlParams).PathAndQuery
					);

				if (selectSortOrder == VideoFilter.SortFilter)
				{
					selListOrderOption.Selected = true;
				}

				selListOrder.Items.Add(selListOrderOption);
			}
		}
		#endregion


		#region private void PopulateControl ()
		/// <summary>
		/// 
		/// </summary>
		private void PopulateControl ()
		{
			// TODO: get filter and sort criteria from Url.
			// Is sort order on criteria - probably as latest releases (Latest), Last Chance (LastChance), Forthcoming link from home page.

			VideoFilter.CatalogueCriteria.Paging.ItemsPerPage = ItemsPerPage;
			VideoFilter.CatalogueCriteria.Paging.PageNumber = PageNumber;

			DataContract.CatalogueResult videoResult = null;
			_itemCount = 0;

			if (_displayType == SiteEnums.VideoGalleryDisplayType.VideoEditorial)
			{
				Modules.VideoCatalogue.EditorialPlaylist editorialPlaylist = DataAccess.VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.Gallery, ListId, VideoFilter);

				if (editorialPlaylist != null)
				{
					ListTitle = editorialPlaylist.PlaylistName;
					videoResult = editorialPlaylist.CatalogueItemSummaryList;
				}
			}
			else if (_displayType == SiteEnums.VideoGalleryDisplayType.VideoSearch)
			{
                videoResult = DataAccess.VideoData.GetVideoItemsForSearchTerm(SiteEnums.VideoDataType.Search, VideoFilter, _searchTerm, SiteConstants.GetProductIdsForProduct(_productType));
			}
			else
			{
				videoResult = DataAccess.VideoData.GetVideoItems(SiteEnums.VideoDataType.List, VideoFilter);
			}

			if (videoResult != null)
			{
				PopulateVideoList(videoResult.CatalogueItems);
				_itemCount = videoResult.CatalogueItems.Length;
				_totalItems = videoResult.TotalResultCount;
			}
			
            //removed by Jodi Adams in order to fix the no videos returned bug
			//this.Visible = _itemCount > 0;

			SetPagingControlProperties();
		}
		#endregion


		#region private void PopulateVideoList ( DataContract.ICatalogueItem[] videoList )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoList"></param>
		private void PopulateVideoList ( DataContract.ICatalogueItem[] videoList )
		{
			if (videoList != null && videoList.Length > 0)
			{
				rptVideoList.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
				rptVideoList.DataSource = videoList;
				rptVideoList.DataBind();

				rptVideoList.Visible = true;

				//selListOrder.Disabled = false;
                divNoItems.Visible = false;
			}
			else
			{
				rptVideoList.Visible = false;
                wucPagingControlTop.Visible = false;
                wucPagingControlBottom.Visible = false;
				//selListOrder.Disabled = true;
                divNoItems.Visible = true;
			}
		}
		#endregion


		#region private void SetPagingControlProperties ()
		/// <summary>
		/// 
		/// </summary>
		private void SetPagingControlProperties ()
		{
			if (_itemCount > 0)
			{
                wucPagingControlTop.NumberOfItems = TotalItems;
                wucPagingControlTop.NumberOfItemsPerPage = ItemsPerPage;
                wucPagingControlTop.PopulateControl(PageNumber);

				wucPagingControlBottom.NumberOfItems = TotalItems;
				wucPagingControlBottom.NumberOfItemsPerPage = ItemsPerPage;
				wucPagingControlBottom.PopulateControl(PageNumber);
			}
			else
			{
                wucPagingControlTop.Visible = false;
				wucPagingControlBottom.Visible = false;
			}
		}
		#endregion


	}
}