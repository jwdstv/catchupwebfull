﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;


namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class VideoSummary : VideoControlBase
	{

		private bool _isLoaded = false;

		#region SummaryDisplayType
		private SiteEnums.VideoSummaryDisplay _summaryDisplayType = SiteEnums.VideoSummaryDisplay.Unknown;
		/// <summary>
		/// 
		/// </summary>
		public SiteEnums.VideoSummaryDisplay SummaryDisplayType
		{
			get { return _summaryDisplayType; }
			set { _summaryDisplayType = value; }
		}
		#endregion

		
		#region IsEditorial
		private bool _isEditorial = false;
		/// <summary>
		/// 
		/// </summary>
		public bool IsEditorial
		{
			get { return _isEditorial; }
			set { _isEditorial = value; }
		}
		#endregion


		#region IncludeFullAbstractTooltip
		private bool _includeFullAbstractTooltip = false;
		/// <summary>
		/// 
		/// </summary>
		public bool IncludeFullAbstractTooltip
		{
			get { return _includeFullAbstractTooltip; }
			set { _includeFullAbstractTooltip = value; }
		}
		#endregion


		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack && Visible && !_isLoaded)
			{
				PopulateControl();
			}
		}


		#region protected void PopulateControl ()
		/// <summary>
		/// 
		/// </summary>
		protected void PopulateControl ()
		{
			if (_isEditorial)
			{
				Modules.VideoCatalogue.EditorialPlaylist editorialPlaylist = DataAccess.VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.List, ListId, VideoFilter);

				if (editorialPlaylist != null)
				{
					ListTitle = editorialPlaylist.PlaylistName;
					PopulateVideoList(editorialPlaylist.CatalogueItemSummaryList.CatalogueItems);
				}
				else
				{
					_itemCount = 0;
				}
			}
			else
			{
				DataContract.CatalogueResult videoResult = DataAccess.VideoData.GetVideoItems(SiteEnums.VideoDataType.Summary, VideoFilter);

				if (videoResult != null)
				{
					PopulateVideoList(videoResult.CatalogueItems);
				}
				else
				{
					_itemCount = 0;
				}
			}

			if (_itemCount <= 0)
				Visible = false;
		}
		#endregion


		#region public void PopulateVideoList ( List<DataContract.VideoItemSummary> videoList )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoList"></param>
		public void PopulateVideoList ( DataContract.ICatalogueItem[] videoList )
		{

			if (videoList != null && videoList.Length > 0)
			{
				// With forthcoming we need forthcoming videos to display.
				if (VideoFilter.SortFilter == SiteEnums.VideoSortOrder.ForthcomingReleases)
				{
					List<DataContract.ICatalogueItem> videoListToFilter = videoList.ToList<DataContract.ICatalogueItem>();
                    //List<DataContract.ICatalogueItem> videoListForthcoming = videoListToFilter.FindAll(delegate( DataContract.ICatalogueItem catalogueItem ) { return catalogueItem.StartDate.HasValue && catalogueItem.StartDate.Value > DateTime.Now; });

                    if (videoListToFilter.Count > 0)
					{
                        rptVideoList.DataSource = videoListToFilter;
						rptVideoList.DataBind();

                        _itemCount = videoListToFilter.Count;
					}
					else
					{
						_itemCount = 0;
					}
				}
				else
				{
					rptVideoList.DataSource = videoList;
					rptVideoList.DataBind();

					_itemCount = videoList.Length;
				}
			}
			else
			{
				_itemCount = 0;
			}

			_isLoaded = true;
		}
		#endregion


		#region protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
		/// <summary>
		/// Required for populating the video and show detail Url properties in the VideoTile control object.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				object videoTileObject = e.Item.FindControl("wucVideoTileSummary");
				if (videoTileObject != null)
				{
					VideoTileSummary videoTile = (VideoTileSummary)videoTileObject;
                    if (VideoFilter.SortFilter == SiteEnums.VideoSortOrder.ForthcomingReleases)
                    {
                        string fc = string.Format("&{0}=1", SiteConstants.Urls.ParameterNames.IsForthcoming);
                        if (!PageLinks.VideoDetailUrl.ToUpper().Contains(fc.ToUpper()))
                            PageLinks.VideoDetailUrl += string.Format("&{0}=1", SiteConstants.Urls.ParameterNames.IsForthcoming);

                        //make sure that the metadata displayed is "Available on" instead of "Available until"
                        videoTile.IsForthcomingRelease = true;

                    }
					videoTile.SetUrls(PageLinks);
					videoTile.IncludeFullAbstractTooltip = _includeFullAbstractTooltip;
					if (_summaryDisplayType != SiteEnums.VideoSummaryDisplay.Unknown)
					{
						videoTile.SummaryDisplayType = _summaryDisplayType;
					}

				}
			}
		}
		#endregion
	}
}