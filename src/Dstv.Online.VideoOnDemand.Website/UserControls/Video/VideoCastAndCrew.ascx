﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoCastAndCrew.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoCastAndCrew" %>

<h4>Cast and Crew</h4>
<div class="greyHdrLine"></div>
<div class="floatLeft">
    <table width="100%" border="0" cellspacing="5" cellpadding="0">
        <tr>
        <td valign="top">
			<asp:Repeater ID="rptCastandCrewCell1" runat="server">
			<ItemTemplate>
			<span class="crewName"><%#DataBinder.GetPropertyValue(Container.DataItem, "FullName")%></span> (<%#DataBinder.GetPropertyValue(Container.DataItem, "Role")%>)<br />
			</ItemTemplate>
			<AlternatingItemTemplate>
			</AlternatingItemTemplate>
			</asp:Repeater>
        </td>
        <td valign="top">
        <asp:Repeater ID="rptCastandCrewCell2" runat="server">
			<ItemTemplate>
			</ItemTemplate>
			<AlternatingItemTemplate>
			<span class="crewName"><%#DataBinder.GetPropertyValue(Container.DataItem, "FullName")%></span> (<%#DataBinder.GetPropertyValue(Container.DataItem, "Role")%>)<br />
			</AlternatingItemTemplate>
			</asp:Repeater>
        </td>
        </tr>
    </table>
</div>

<div class="padding20px"></div>
