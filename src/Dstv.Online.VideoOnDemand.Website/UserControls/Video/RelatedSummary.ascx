﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedSummary.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.RelatedSummary" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileSummary" Src="~/UserControls/Video/VideoTileSummary.ascx" %>
<h4><%=ListTitle%></h4>
<div class="moreBtn" style='display:<%=ShowMoreLink? "block": "none" %>;'>
    <a href="<%=MoreUrlFormatted%>">
        <img src="/app_themes/onDemand/images/arrow_blue.gif" alt="" width="9" height="10" />
        <%=ShowMoreLink ? "More " + ListTitle : ""%>   </a></div>
<div class="greyHdrLine"></div>
<div class="OD_item_sep" style="height:10px;">&nbsp;</div>
<asp:Repeater ID="rptVideoList" runat="server" OnItemCreated="rptVideoList_ItemCreated">
    <ItemTemplate>
        <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" SummaryDisplayType="VideoLayoutTextBottom" IncludeFullAbstractTooltip="true" VideoItem="<%#Container.DataItem%>" />
        <div class="OD_item_sep">&nbsp;</div>
    </ItemTemplate>
</asp:Repeater>
