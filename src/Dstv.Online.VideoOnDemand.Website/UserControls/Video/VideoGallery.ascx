﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoGallery.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoGallery" %>
<%@ Register Src="~/UserControls/Navigation/PagingControlWithLinks.ascx" TagName="PagingControl"
    TagPrefix="wuc" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileSummary" Src="~/UserControls/Video/VideoTileSummary.ascx" %>

<script type="text/javascript">
    function GalleryListOrderChanged() {
        var selListOrder = document.getElementById('selListOrder');
        if (selListOrder != null) {
            document.location.href = selListOrder.options[selListOrder.selectedIndex].value;
        }
    }
</script>


<div class="vidFilter">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="140">
                List videos in this order
            </td>
            <td>
                <select id="selListOrder" onchange="javascript:GalleryListOrderChanged();" runat="server"
                    clientidmode="Static">
                </select>
            </td>
            <td align="right" valign="middle">
                <wuc:PagingControl ID="wucPagingControlTop" runat="server" />
            </td>
        </tr>
    </table>
</div>

<asp:Repeater ID="rptVideoList" runat="server" OnItemDataBound="rptVideoList_ItemDataBound">
    <ItemTemplate>
        <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" SummaryDisplayType="VideoLayoutTextBottom2"
            IncludeFullAbstractTooltip="true" />
    </ItemTemplate>
    <SeparatorTemplate>
        <div class="OD_item_sep" id="divVerticalSeparator" runat="server" clientidmode="Static">
        </div>
        <div class="padding5px" id="divHorizontalSeparator" runat="server" clientidmode="Static">
        </div>
    </SeparatorTemplate>
</asp:Repeater>
<div class="noVideoMessage" id="divNoItems" runat="server" clientidmode="Static"
    style="display: inline; position: relative;">
    <%=NoResultsReturned %>
</div>
<div class="greyHdrLine">
</div>
<div class="vidFilter">
<wuc:PagingControl ID="wucPagingControlBottom" runat="server" />
</div>
