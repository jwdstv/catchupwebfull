﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoFilterList.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoFilterList" %>
<div class="hdrFilter"><img src="/app_themes/onDemand/images/arrows_filterHdr_down.png" width="10" height="12" /> <%= HeaderFilter %></div>
<script type="text/javascript">
    (function ($) {
        $.QueryString = (function (a) {
            if (a == "") return {};
            var b = {};
            for (var i = 0; i < a.length; ++i) {
                var p = a[i].split('=');
                if (p.length != 2) continue;
                b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            }
            return b;
        })(window.location.search.substr(1).split('&'))
    })(jQuery);

    $(document).ready(function () {
        $('.AccordionPanel').removeClass('AccordionPanel_SubItemOpen');
        $('#mainMenuCatchUp').addClass('AccordionPanel_SubItemTabHover');

        if ($.QueryString["ParentId"]) {
            $('.AccordionPanelTab_SubItem').removeClass('AccordionPanel_SubItemTabHover');
            $('.AccordionPanelTab_SubItem').mouseout(function () {
                $('.AccordionPanelTab_SubItem').removeClass('AccordionPanel_SubItemTabHover');
            });
            $('.AccordionPanelTab_SubItem').removeClass('titi');

            $('#' + $.QueryString["ParentId"]).addClass('titi');
            $('#' + $.QueryString["ParentId"]).addClass('AccordionPanel_SubItemTabHover');

            Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
                $('#' + $.QueryString["ParentId"]).addClass('AccordionPanel_SubItemTabHover');
            };
            $('.AccordionPanelTab_SubItem').mouseout(function () {
                $('#' + $.QueryString["ParentId"]).addClass('AccordionPanel_SubItemTabHover');
            });
        }
    });

    Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
        $('#mainMenuCatchUp').addClass('AccordionPanel_SubItemTabHover');
        $('.AccordionPanelTab_SubItem').removeClass('AccordionPanel_SubItemTabHover');
    };

    function doClick2() {
        Spry.Widget.Accordion.prototype.onPanelTabClick = function (e, panel) {
            if (panel != this.currentPanel) {
                this.openPanel(panel);
                $('.AccordionPanel').removeClass('AccordionPanel_SubItemOpen');
                $('.AccordionPanel').mouseout(function () {
                    $('#mainMenuCatchUp').addClass('AccordionPanel_SubItemTabHover');
                });
            } else {
                this.closePanel();
                $('.AccordionPanel').mouseout(function () {
                    $('#mainMenuCatchUp').removeClass('AccordionPanel_SubItemTabHover');
                });
            }
        }
    }

    function doClick(obj) {
        $('.AccordionPanelTab_SubItem').removeClass('AccordionPanel_SubItemTabHover');
        $('.AccordionPanelTab_SubItem').mouseout(function () {
            $('.AccordionPanelTab_SubItem').removeClass('AccordionPanel_SubItemTabHover');
        });

        if ($('#' + obj.id).hasClass('titi')) {
            $('#' + obj.id).removeClass('titi');

            Spry.Widget.Accordion.prototype.onPanelTabClick = function (e, panel) {
                if (panel != this.currentPanel) {
                    this.openPanel(panel);
                } else {
                    this.closePanel();
                    Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
                        $('.AccordionPanelTab_SubItem').removeClass('AccordionPanel_SubItemTabHover');
                    };
                }
            }
        } else {
            $('.AccordionPanelTab_SubItem').removeClass('titi');

            $('#' + obj.id).addClass('titi');
            $('#' + obj.id).addClass('AccordionPanel_SubItemTabHover');

            Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
                $('#' + obj.id).addClass('AccordionPanel_SubItemTabHover');
            };
            $('.AccordionPanelTab_SubItem').mouseout(function () {
                $('#' + obj.id).addClass('AccordionPanel_SubItemTabHover');
            });
        }
    }
</script>
<div id="filterAccordion" class="Accordion" tabindex="0">
	<asp:Repeater ID="rptSection" runat="server" onitemdatabound="rptSection_ItemDataBound">
	<ItemTemplate>
        <div class="AccordionPanel AccordionPanelClosed">
			<div class="AccordionPanelTab" id="mainMenuCatchUp" onclick="doClick2();"><%#DataBinder.GetPropertyValue(Container.DataItem, "Name")%></div>
			<div class="AccordionPanelContent">
			    <ul>

			    <asp:Repeater ID="rptItemsOnlyTop" runat="server" ClientIDMode="Static">
			    <ItemTemplate>
				    <li><a href="<%#DataBinder.GetPropertyValue(Container.DataItem, "FormattedUrl")%>" class="<%# IsItemSelected(Container.DataItem) ? "up" : "down" %>"><strong><%#GetDisplayTitle(Container.DataItem)%></strong></a></li>
			    </ItemTemplate>
			    </asp:Repeater>

                <li>
					    <div id="AccordionSubMenu" class="Accordion" runat="server" clientidmode="Static">
			    <asp:Repeater ID="rptItemsWithSubItems" runat="server" ClientIDMode="Static" onitemdatabound="rptSection_rptItemsWithSubItems_ItemDataBound">
			    <ItemTemplate>
                  <div class="AccordionPanelSubItem">
						<div class="AccordionPanelTab_SubItem" onclick="doClick(this);" id="<%#DataBinder.GetPropertyValue(Container.DataItem, "Id")%>"><%#DataBinder.GetPropertyValue(Container.DataItem, "Name")%></div>
						<div class="AccordionPanelContent_SubItem">
							<ul>
							<asp:Repeater ID="rptSubItems" runat="server" ClientIDMode="Static" onitemdatabound="rptSection_rptItemsWithSubItems_rptSubItems_ItemDataBound">
							<ItemTemplate>
								<li><a href="<%#DataBinder.GetPropertyValue(Container.DataItem, "FormattedUrl")%>" class="<%# IsItemSelected(Container.DataItem) ? "up" : "down" %>"><%#GetDisplayTitle(Container.DataItem)%></a></li>
							</ItemTemplate>
							</asp:Repeater>
							</ul>
						</div>
					    </div>
			    </ItemTemplate>
			    </asp:Repeater>
                   </div>
				    </li>

				<asp:Repeater ID="rptItemsOnlyBottom" runat="server" ClientIDMode="Static">
			    <ItemTemplate>
				    <li><a href="<%#DataBinder.GetPropertyValue(Container.DataItem, "FormattedUrl")%>" class="<%# IsItemSelected(Container.DataItem) ? "up" : "down" %>"><strong><%#GetDisplayTitle(Container.DataItem)%></strong></a></li>
			    </ItemTemplate>
			    </asp:Repeater>

			    </ul>
            </div>
        </div>
	</ItemTemplate>
	</asp:Repeater>
</div>

<script type="text/javascript"> 
<!--
var filterAccordion = new Spry.Widget.Accordion("filterAccordion", { useFixedPanelHeights: false, defaultPanel: <%=DefaultSectionIndex%> });
//-->
</script>
