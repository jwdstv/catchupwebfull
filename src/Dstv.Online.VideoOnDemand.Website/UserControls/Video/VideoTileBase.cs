﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public class VideoTileBase : System.Web.UI.UserControl
	{


		#region Control Properties

		#region PageLinks
		private VideoControlBasePageLinks _pageLinks = new VideoControlBasePageLinks();
		/// <summary>
		/// 
		/// </summary>
		public VideoControlBasePageLinks PageLinks
		{
			get { return _pageLinks; }
			set { _pageLinks = value; }
		}
		#endregion


		#region RuntimeDisplay
		private SiteEnums.VideoRuntimeDisplay _runtimeDisplay = SiteEnums.VideoRuntimeDisplay.Duration;
		/// <summary>
		/// 
		/// </summary>
		public SiteEnums.VideoRuntimeDisplay RuntimeDisplay
		{
			get { return _runtimeDisplay; }
			set { _runtimeDisplay = value; }
		}
		#endregion

		#endregion



		#region VideoItem
		private DataContract.ICatalogueItem _videoItem;
		/// <summary>
		/// 
		/// </summary>
		public DataContract.ICatalogueItem VideoItem
		{
			get { return _videoItem; }
			set { _videoItem = value; }
		}
		#endregion


		/// <summary>
		/// 
		/// </summary>
		public string ProductTypeCssName
		{
			get
			{
				// TODO: get based on productType/productTypeId
				switch (VideoProductType)
				{
					case ProductTypes.BoxOffice:
						return "boxOffice";
					case ProductTypes.CatchUp:
						return "catchUp";
					case ProductTypes.OpenTime:
						return "openTime";
					default:
						return "";
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ProductTypes VideoProductType
		{
			get
			{
				return _videoItem.ProductTypeId.Value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int VideoProgramId
		{
			get
			{
				return _videoItem.ProgramId.HasValue ? _videoItem.ProgramId.Value : -1;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoId
		{
			get
			{
				return _videoItem.CatalogueItemId.Value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoRuntimeDisplay
		{
			get
			{
				return _runtimeDisplay == SiteEnums.VideoRuntimeDisplay.RuntimeInMinutes ? string.Format("{0}:00", _videoItem.RunTimeInMinutes) : _videoItem.DurationDescription;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoThumbnailImgUrl
		{
			get
			{
				return _videoItem.MediaThumbnailUri;
			}
		}

		public string VideoDetailPageUrl
		{
			get 
			{
				return GetFormattedVideoDetailUrl();
			}
		}

		public string VideoShowPageUrl
		{
			get
			{
				return IsEpisode ? GetFormattedShowDetailUrl() : VideoThumbnailPageUrl;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoThumbnailPageUrl
		{
			get
			{
				return GetFormattedVideoDetailUrl();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoTitlePageUrl
		{
			get
			{
				return IsEpisode ? GetFormattedShowDetailUrl() : VideoThumbnailPageUrl;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoTitle
		{
			get
			{
                if (_videoItem != null)
                    return string.IsNullOrEmpty(_videoItem.ProgramTitle) ? _videoItem.VideoTitle : _videoItem.ProgramTitle;
                else return string.Empty;
				// TODO: get based on VideoTitle/ProgramTitle (?) property
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoFullTitle
		{
			get
			{
				if (IsEpisode)
				{
					if (IsSeason)
					{
						return SiteConstants.Display.Video.FullTitleWithEpisodeAndSeasonNumberFormatString.
							Replace(SiteConstants.FormatIdentifiers.VideoTitle, VideoTitle).
							Replace(SiteConstants.FormatIdentifiers.EpisodeNumber, VideoEpisodeNumber).
							Replace(SiteConstants.FormatIdentifiers.SeasonNumber, VideoSeasonNumber);
					}
					else
					{
						return SiteConstants.Display.Video.FullTitleWithEpisodeFormatString.
							Replace(SiteConstants.FormatIdentifiers.VideoTitle, VideoTitle).
							Replace(SiteConstants.FormatIdentifiers.EpisodeNumber, VideoEpisodeNumber);
					}
				}
				else
				{
					return VideoTitle;
				}
			}
		}

		

		/// <summary>
		/// 
		/// </summary>
		public bool IsEpisode
		{
			get
			{
				return _videoItem.EpisodeNumber.HasValue && _videoItem.EpisodeNumber.Value > 0;
				// TODO: determine if episode (i.e. series etc). ProgramTitlle != EpisodeTitle (?)
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoEpisodeNumber
		{
			get
			{
				return IsEpisode ? _videoItem.EpisodeNumber.Value.ToString() : "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoEpisodeTitle
		{
			get
			{
				if (IsEpisode)
				{
					if (IsSeason)
					{
						return SiteConstants.Display.Video.EpisodeTitleWithSeasonNumberFormatString.
							Replace(SiteConstants.FormatIdentifiers.EpisodeNumber, VideoEpisodeNumber).
							Replace(SiteConstants.FormatIdentifiers.SeasonNumber, VideoSeasonNumber);
					}
					else
					{
						return SiteConstants.Display.Video.EpisodeTitleFormatString.
							Replace(SiteConstants.FormatIdentifiers.EpisodeNumber, VideoEpisodeNumber);
					}
				}
				else
				{
					return VideoTitle;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsSeason
		{
			get
			{
				return _videoItem.SeasonNumber.HasValue && _videoItem.SeasonNumber.Value > 0;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoSeasonNumber
		{
			get
			{
				return IsSeason ? _videoItem.SeasonNumber.Value.ToString() : "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool HasStartDate
		{
			get
			{
				return _videoItem.StartDate.HasValue && _videoItem.StartDate.Value > SiteConstants.Display.Video.MinDateValue;
				// TODO: return based on productType and ExpiryDate item property.
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool HasExpiryDate
		{
			get
			{
				return _videoItem.ExpiryDate.HasValue && _videoItem.ExpiryDate.Value > SiteConstants.Display.Video.MinDateValue;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool HasAirDate
		{
			get
			{
				return _videoItem.Airdate.HasValue && _videoItem.Airdate.Value > SiteConstants.Display.Video.MinDateValue;
			}
		}

		/// <summary>
		/// 
		/// </summary>
        public bool IsForthcomingRelease
        {
            //get
            //{
            //    return HasStartDate ? _videoItem.StartDate.Value > DateTime.Now : false;
            //}
            get;
            set;
        }

		/// <summary>
		/// 
		/// </summary>
		public string VideoStartDate
		{
			get
			{
				return HasStartDate ? _videoItem.StartDate.Value.ToString(SiteConstants.Display.Video.StartDateDisplayFormat) : "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoExpiryDate
		{
			get
			{
                return HasExpiryDate ? _videoItem.ExpiryDate.Value.ToString(SiteConstants.Display.Video.ExpiryDateDisplayFormat) : "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoAirDate
		{
			get
			{
                return HasAirDate ? _videoItem.Airdate.Value.ToString(SiteConstants.Display.Video.AirDateDisplayFormat) : "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoListDate
		{
			get
			{
				return HasExpiryDate ? VideoExpiryDate : VideoAirDate;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoAbstractTrimmed
		{
			get
			{
                string abs = string.Empty;
                if (_videoItem != null && (!string.IsNullOrEmpty(_videoItem.Abstract)))//Check for null values
                {
                    abs = SiteUtils.TrimString(_videoItem.Abstract, SiteConstants.Display.Video.AbstractTrimLength, " ...");
                }
                return abs;
			}
		}

		public string VideoAbstract
		{
			get
			{
				return _videoItem.Abstract;
			}
		}

	    public string ProgramAbstract
	    {
	        get { return _videoItem.ProgramAbstract; }
	    }

		public string VideoAgeRestriction
		{
			get { return _videoItem.AgeRestriction; }
		}

		public string ProgramGenre
		{
			get { return string.IsNullOrEmpty(_videoItem.ProgramGenre) ? "" : _videoItem.ProgramGenre; }
		}

		public string ProgramShowInfo
		{
			get
			{
				if (IsSeason && _videoItem.SeasonNumber.Value > 0)
				{
					return string.Format("Season {0} / {1}", VideoSeasonNumber, ProgramGenre);
				}
				else
				{
					return ProgramGenre;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string VideoTooltip
		{
            get
            {
                if (HttpContext.Current.IsDebuggingEnabled)
                {
                    return string.Format("ProgramId: {0}\nVideoId: {1}\nStartDate: {2}\nAirDate: {3}\nExpiryDate: {4}\nIsForthcomingRelease: {5}\nProgramTitle: {6}\nVideoTitle: {7}\n Tags: {8}",
                        VideoProgramId,
                        VideoId,
                        _videoItem.StartDate.HasValue ? _videoItem.StartDate.Value.ToString("yyyy-MM-dd HH:mm") : "-",
                        _videoItem.Airdate.HasValue ? _videoItem.Airdate.Value.ToString("yyyy-MM-dd HH:mm") : "-",
                        _videoItem.ExpiryDate.HasValue ? _videoItem.ExpiryDate.Value.ToString("yyyy-MM-dd HH:mm") : "-",
                        IsForthcomingRelease,
                        string.IsNullOrEmpty(_videoItem.ProgramTitle) ? "" : _videoItem.ProgramTitle,
                        string.IsNullOrEmpty(_videoItem.VideoTitle) ? "" : _videoItem.VideoTitle,
                        _videoItem.Keywords
                        );
                }
                else
                {
                    return string.Format("Click to view {0}", VideoFullTitle);
                }
            }
        }


		#region Control Methods

		#region public void SetUrls ( VideoControlBasePageLinks sourcePageLinks )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sourcePageLinks"></param>
		public void SetUrls ( VideoControlBasePageLinks sourcePageLinks )
		{
			_pageLinks.VideoDetailUrlForBoxOffice = string.IsNullOrEmpty(sourcePageLinks.VideoDetailUrlForBoxOffice) ? sourcePageLinks.VideoDetailUrl : sourcePageLinks.VideoDetailUrlForBoxOffice;
			_pageLinks.VideoDetailUrlForCatchUp = string.IsNullOrEmpty(sourcePageLinks.VideoDetailUrlForCatchUp) ? sourcePageLinks.VideoDetailUrl : sourcePageLinks.VideoDetailUrlForCatchUp;
			_pageLinks.VideoDetailUrlForOpenTime = string.IsNullOrEmpty(sourcePageLinks.VideoDetailUrlForOpenTime) ? sourcePageLinks.VideoDetailUrl : sourcePageLinks.VideoDetailUrlForOpenTime;

			_pageLinks.ShowDetailUrlForBoxOffice = string.IsNullOrEmpty(sourcePageLinks.ShowDetailUrlForBoxOffice) ? sourcePageLinks.ShowDetailUrl : sourcePageLinks.ShowDetailUrlForBoxOffice;
			_pageLinks.ShowDetailUrlForCatchUp = string.IsNullOrEmpty(sourcePageLinks.ShowDetailUrlForCatchUp) ? sourcePageLinks.ShowDetailUrl : sourcePageLinks.ShowDetailUrlForCatchUp;
			_pageLinks.ShowDetailUrlForOpenTime = string.IsNullOrEmpty(sourcePageLinks.ShowDetailUrlForOpenTime) ? sourcePageLinks.ShowDetailUrl : sourcePageLinks.ShowDetailUrlForOpenTime;
		}
		#endregion

		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		private string GetFormattedVideoDetailUrl ()
		{
			switch (VideoProductType)
			{
				case ProductTypes.BoxOffice:
					return GetFormattedUrl(string.IsNullOrEmpty(_pageLinks.VideoDetailUrlForBoxOffice) ? _pageLinks.VideoDetailUrl : _pageLinks.VideoDetailUrlForBoxOffice);
				case ProductTypes.CatchUp:
					return GetFormattedUrl(string.IsNullOrEmpty(_pageLinks.VideoDetailUrlForCatchUp) ? _pageLinks.VideoDetailUrl : _pageLinks.VideoDetailUrlForCatchUp);
				case ProductTypes.OpenTime:
					return GetFormattedUrl(string.IsNullOrEmpty(_pageLinks.VideoDetailUrlForOpenTime) ? _pageLinks.VideoDetailUrl : _pageLinks.VideoDetailUrlForOpenTime);
				default:
					return "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		private string GetFormattedShowDetailUrl ()
		{
			switch (VideoProductType)
			{
				case ProductTypes.BoxOffice:
					return GetFormattedUrl(string.IsNullOrEmpty(_pageLinks.ShowDetailUrlForBoxOffice) ? _pageLinks.ShowDetailUrl : _pageLinks.ShowDetailUrlForBoxOffice);
				case ProductTypes.CatchUp:
					return GetFormattedUrl(string.IsNullOrEmpty(_pageLinks.ShowDetailUrlForCatchUp) ? _pageLinks.ShowDetailUrl : _pageLinks.ShowDetailUrlForCatchUp);
				case ProductTypes.OpenTime:
					return GetFormattedUrl(string.IsNullOrEmpty(_pageLinks.ShowDetailUrlForOpenTime) ? _pageLinks.ShowDetailUrl : _pageLinks.ShowDetailUrlForOpenTime);
				default:
					return "";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		internal string GetFormattedUrl ( string url )
		{
			string seoUrl = SiteUtils.Url.SEO.GetSEOFriendlyUrl(url);

			return seoUrl.
				Replace(SiteConstants.FormatIdentifiers.EpisodeNumber, HttpUtility.UrlEncode(VideoEpisodeNumber)).
				Replace(SiteConstants.FormatIdentifiers.EpisodeTitle, HttpUtility.UrlEncode(VideoEpisodeTitle)).
				Replace(SiteConstants.FormatIdentifiers.ProductType, VideoProductType.ToString()).
				Replace(SiteConstants.FormatIdentifiers.ProgramId, VideoProgramId.ToString()).
				Replace(SiteConstants.FormatIdentifiers.ProgramTitle, HttpUtility.UrlEncode(VideoTitle)).
				Replace(SiteConstants.FormatIdentifiers.SeasonNumber, HttpUtility.UrlEncode(VideoSeasonNumber)).
				Replace(SiteConstants.FormatIdentifiers.VideoId, HttpUtility.UrlEncode(VideoId)).
				Replace(SiteConstants.FormatIdentifiers.VideoTitle, HttpUtility.UrlEncode(VideoTitle)).
				Replace(SiteConstants.FormatIdentifiers.Id, HttpUtility.UrlEncode(VideoId));
		}

		#endregion

	}
}
