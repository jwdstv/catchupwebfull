﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoSummary.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoSummary" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileSummary" Src="~/UserControls/Video/VideoTileSummary.ascx" %>
<%@ OutputCache Duration="600" VaryByParam="*"%>
<h4 class="home">
    <%=ListTitle%></h4>
<div class="moreBtn2" style='display: <%=ShowMoreLink? "block": "none" %>'>
    <a href="<%=MoreUrlFormatted%>"><%=ShowMoreLink ? "... more " + ListTitle : ""%></a></div>
<div class="greyHdrLine">
</div>
<div class="padding5px" style="height: 10px;">
</div>

<asp:Repeater ID="rptVideoList" runat="server" OnItemCreated="rptVideoList_ItemCreated">
    <ItemTemplate>
        <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" SummaryDisplayType="VideoLayoutTextBottom"
            VideoItem="<%#Container.DataItem%>" />
    </ItemTemplate>
    <SeparatorTemplate>
        <div class="OD_item_sep">
        </div>
    </SeparatorTemplate>
</asp:Repeater>
