﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditorialVideoList.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.EditorialVideoList" %>
<%@ OutputCache Duration="600" VaryByParam="none" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileList" Src="~/UserControls/Video/VideoTileList.ascx" %>
<h5><%=ListTitle%></h5>
<div class="greyHdrLine">
</div>
<div class="topList">
    <div class="expTextHdr-ext">
        <asp:Literal ID="litProductDateHeader" runat="server">Available until</asp:Literal></div>
    <asp:Repeater ID="rptVideoList" runat="server" OnItemCreated="rptVideoList_ItemCreated">
        <HeaderTemplate>
            <ul class="topList">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <wuc:VideoTileList ID="wucVideoTileList" runat="server" VideoItem="<%#Container.DataItem%>" />
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div class="padding10px">
</div>
<div class="padding20px">
</div>
