﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoDetailConsole.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoDetailConsole" %>
<%@ Import Namespace="Dstv.Online.VideoOnDemand.Website.ClassLibrary" %>
<%@ Register Src="~/UserControls/BoxOffice/PayDialogue.ascx" TagName="PayDialogue"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Shared/ErrorDialogue.ascx" TagName="ErrorDialogue"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Players/DStvOnlineFlashPlayer.ascx" TagName="FlashPlayer"
    TagPrefix="wuc" %>

<script type="text/javascript">
	var _streamingMedia = <%=IsCurrentlyPlayingMovie ? "true" : "false"%>;
	var _isCurrentlyRented = <%=IsCurrentlyRented ? "true" : "false"%>;

	if (_isCurrentlyRented)
	{
		$(document).ready(
			function () 
			{
                var minLeft = <%=JavaScriptRentalMinutesLeft%>;

				if (minLeft > 0)
				{
                    var clientExpiryDate = new Date();
                    clientExpiryDate.setMinutes(clientExpiryDate.getMinutes() + minLeft);

					$('#divTimeLeft').countdown({ until: clientExpiryDate, expiryText: 'expired', timezone: +2, compact: true, format: 'HM', layout: '{hnn}hrs, {mnn}mins', onExpiry: ExpireVideo, tickInterval: 60 });
				}
			}
		);
	}

    function ExpireVideo() {
        // Reload page so that video is no longer available. What if currently streaming ??
		  if (!_streamingMedia)
		  {
			document.location.href = document.location.href;
		  }
    }

    function ImageLoadFail ( img ) {img.style.display = 'none';}

    function callFromPlayer(param) {

        if (param.call.toLowerCase() == 'play_video') {
            //Call method to increment play counter in MMS for this video
            //vod_cs_async_url is created by a call that the Player control makes to a ControlBase function

            $.ajax({
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                url: "VideoDetail.aspx/InsertPlayHit",
                data: "{'videoId': '" + GetQueryString("VideoId") + "'}",
                success: function (data) {
                },
                error: function () {
                }
            });
        }
    }
</script>
<div class="vidPlay_bg">
    <div class="" id="divNoVideo" runat="server" clientidmode="Static">
        <div class="box140"></div>
        <div class="vidPlayer1 white18"><br /><br /><br />The requested video could not be loaded.</div>
        <div class="box140"></div>
    </div>
    
    <div id="divVideoConsole" runat="server" clientidmode="Static">
        <div class="box140">
            <div class="white22">
                            <asp:HyperLink ID="hlTitle" runat="server" Text="" CssClass="CuShowTitle"></asp:HyperLink><asp:Literal
                                ID="litTitle" runat="Server"></asp:Literal></div>
            <div class="white14px" id="divEpisodeTitle" runat="server" clientidmode="Static"><strong><asp:Literal ID="litEpisodeTitle" runat="server"></asp:Literal></strong><br /><br /></div>
            <div class="floatLeft"><asp:Literal ID="litAbstract" runat="Server"></asp:Literal></div>
            <div class="padding5px"></div>
            <div class="hr_blckONwht"></div>
            
            <div class="floatLeft" id="divVideoProperties" runat="server" clientidmode="Static">
                <div class="padding5px"></div>
                <table border="0" width="100%" class="vidDetails_sml">
                    <tr>
                        <td>Age Restriction:</td>
                        <td><asp:Literal ID="litAgeRestriction" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Duration:</td>
                        <td><asp:Literal ID="litRuntime" runat="server"></asp:Literal></td>
                    </tr>
                    <tr id="trDateAired" runat="server" clientidmode="Static">
                        <td>Date Aired:</td>
                        <td><asp:Literal ID="litAiredOn" runat="server"></asp:Literal></td>
                    </tr>
                    <tr id="trAvailableTill" runat="server" clientidmode="Static">
                        <td>Available Till:</td>
                        <td><asp:Literal ID="litAvailableTill" runat="server"></asp:Literal></td>
                    </tr>
                </table>
            </div>
        </div>
        <%--<div class="vidPlayer1">
		<asp:UpdatePanel id="pnlPlay" runat="server">
		<ContentTemplate>
			<wuc:FlashPlayer ID="wucFlashPlayer" runat="server" />

			<asp:Image ID="imgBillboard" runat="server" width="660" height="380" />
		</ContentTemplate>
		</asp:UpdatePanel>
    </div>--%>
    
    
        <asp:UpdatePanel ID="pnlPlay" runat="server">
            <ContentTemplate>
                <wuc:FlashPlayer ID="wucFlashPlayer" runat="server" />
                <div class="vidPlayer1" runat="server" id="imgDiv"><asp:Image ID="imgBillboard" class="vidPlayerImage" runat="server" /></div>
                <%--<div id="imgDiv" runat="server"></div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <div class="box140">
                        <!-- TITLE -->
                        <div class="floatLeft">
                        	<img src="/app_themes/onDemand/images/hdr-boxOffice.png" alt="Box Office" id="imgTitleBO" runat="server" clientidmode="Static" />
                            <img src="/app_themes/onDemand/images/hdr-catchUp.png" alt="Catch Up" id="imgTitleCU" runat="server" clientidmode="Static" />
                        </div>
                        <div class="hr_blckONwht"></div>
                        <!-- Channel -->
                        <div class="floatLeft" id="divChannel" runat="server" clientidmode="Static">
                            <asp:Image ID="imgChannel" runat="server" ClientIDMode="Static" onerror="javascript:ImageLoadFail(this);" />
                            <asp:HyperLink ID="hlChannel" runat="server" Target="_blank">
                            <asp:Image ID="imgChannelLink" runat="server" ClientIDMode="Static" /></asp:HyperLink>
                            <div class="padding10px"></div><br />
                           <%-- <strong>Watch it on
                                <asp:Literal ID="litChannelName" runat="server"></asp:Literal></strong>--%>
                                <%--watch it on channel removed as per story DCCU088 -OM--%>
                            
                        </div>
                        <!-- CATCH UP: not logged in control bar -->
                        <div id="divCatchUpNotLoggedIn" runat="server" clientidmode="Static">
                        <div class="hr_blckONwht"></div>
                            <div class="floatLeft" id="divAvailableToWatch" runat="server" clientidmode="Static">
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td valign="top"><img src="/app_themes/onDemand/images/<%=PlayIcon%>" width="24" height="27" alt="Watch Now. Catchup" /></td>
                                        <td><%-- <a href="javascript:ShowLoginDialogue();"><strong>Watch Now</strong></a>--%><%--Removed this to allow for lifghtbox--%>
                                            <strong><span class="link-<%=ProductTypeCssName%>"><asp:HyperLink ID="hlPlayVideo" runat="server" ClientIDMode="Static">Watch Now</asp:HyperLink></span></strong>
                                            <asp:Literal ID="litEpisodeTitle2" runat="server"><br />({0})</asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- CATCH UP: not available -->
                        <div class="floatLeft" id="divCatchUpNotAvailable" runat="server" clientidmode="Static">
                        <div class="hr_blckONwht"></div>
                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                <tr>
                                    <td><img src="/app_themes/onDemand/images/<%=PlayIcon%>" width="24" height="27" alt="Forthcoming Release" /></td>
                                    <td>Available to view on <strong><asp:Literal ID="litAvailableOn" runat="server"></asp:Literal></strong></td>
                                </tr>
                            </table>
                        </div>
                        <!-- CATCH UP: Logged in control bar (valid) -->
                        <div id="divCatchUpLoggedInAndValid" runat="server" clientidmode="Static">
                        <div class="hr_blckONwht"></div>
                            <div class="padding10px"></div>
                            <div class="white18">
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td valign="top"><img src="/app_themes/onDemand/images/icon_playBlue.png" alt="Watch Now.. CatchUp" width="24" height="27" /></td>
                                        <td><span class="link-<%=ProductTypeCssName%>"><asp:HyperLink ID="hlPlayCatchUpVideo" runat="server" ClientIDMode="Static">Watch Now</asp:HyperLink></span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- CATCH UP: Logged in control bar (not valid) -->
                        <div id="divCatchUpLoggedInAndNotValid" runat="server" clientidmode="Static" class="vidDetails_sml">
                            <div class="hr_blckONwht"></div>
                            <div class="padding10px"></div>
                            <asp:Literal ID="litNotSubscriber" runat="Server">In order to watch this video you need to be a DStv Premium subscriber.</asp:Literal>
                            <%--<br />
                            <br />
                            Update you DStv online profile <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>Account/About.aspx">
                                here</a>--%>
                        </div>
                        <!-- SHOW -->
                        <div id="divCatchUpShowPlay" runat="server" clientidmode="Static">
                            <div class="hr_blckONwht"></div>
                            <div class="padding10px"></div>
                            <div class="floatLeft">
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td valign="top"><img src="/app_themes/onDemand/images/icon_playBlue.png" alt="Watch Now.........Catchup" width="24" height="27" /></td>
                                        <td>
                                            <span class="link-<%=ProductTypeCssName%>">
                                            <asp:HyperLink ID="hlPlayShowVideo" runat="server" ClientIDMode="Static"><strong>View Featured Video</strong></asp:HyperLink></span><asp:Literal
                                                ID="litEpisodeTitleForShow" runat="server"><br />({0})</asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- BOX OFFICE: not rented control bar -->
                        <div id="divBoxOfficeNotRented" runat="server" clientidmode="Static">
                            <div class="hr_blckONwht"></div>
                            <div class="promoPrice"><asp:Literal ID="litCost" runat="server"></asp:Literal></div>
                            <div id="divBoxOfficeAvailableToRent" runat="server" clientidmode="Static">
                                <div class="floatLeft">
                                    <strong>
                                        <asp:Literal ID="litPlayTypes" runat="server">Stream or download</asp:Literal><br />
                                        and keep for
                                        <asp:Literal ID="litRentalPeriod" runat="server"></asp:Literal>
                                        hours</strong></div>
                                <div class="hr_blckONwht"></div>
                                <div class="white18">
                                    <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                        <tr>
                                            <td><img src="/app_themes/onDemand/images/icon_rentNow.png" alt="" width="19" height="19" align="absmiddle" /></td>
                                            <td>
                                                <span class="link-<%=ProductTypeCssName%>"><asp:HyperLink ID="hlBoxOfficeNotRented" runat="server">Rent Now</asp:HyperLink></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="divBoxOfficeNotAvailableToRent" runat="server" clientidmode="Static">
                                <div class="hr_blckONwht"></div>
                                <div class="white12px">
                                    <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                        <tr>
                                            <td><img src="/app_themes/onDemand/images/<%=PlayIcon%>" width="24" height="27" alt="Forthcoming Release" /></td>
                                            <td>Available to rent on <strong><asp:Literal ID="litAvailableOnBoxOffice" runat="server"></asp:Literal></strong></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- BOX OFFICE: Rented control bar -->
                        <div id="divBoxOfficeRented" runat="server" clientidmode="Static">
                            <div class="hr_blckONwht"></div>
                            <div id="divExpiryTicker" runat="server" clientidmode="Static">
                                <div class="promoTime" id="divTimeLeft" runat="server" clientidmode="Static">35hrs, 55min</div>
                                <div class="floatLeft">left to watch this video</div>
                            </div>
                            <div class="padding10px"></div>
                            <div class="white18">
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td valign="top">
                                            <img src="/app_themes/onDemand/images/<%=PlayIcon%>" alt="Watch Now.. Box Office"
                                                width="19" height="18" align="absmiddle" />
                                        </td>
                                        <td>
                                            <span class="link-<%=ProductTypeCssName%>">
                                            <asp:HyperLink ID="hlPlayRentedVideo" runat="server" ClientIDMode="Static">Watch Now</asp:HyperLink></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- BOX OFFICE and CATCH UP: Download Video -->
                        <div id="divDownloadVideo" runat="server" clientidmode="Static">
                            <div class="hr_blckONwht"></div>
                            <div class="white18">
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td valign="top">
                                            <img src="/app_themes/onDemand/images/icon_whiteDownload.png" alt="" width="19" height="16"
                                                align="absmiddle" />
                                        </td>
                                        <td>
                                            <span class="link-<%=ProductTypeCssName%>">
                                             <asp:LinkButton ID="lbDownloadVideo"  ClientIDMode="Static" runat="server" 
                                                onclick="lbDownloadVideo_Click">Download</asp:LinkButton>
                                            </span><br /><span
                                                class="rated-rating">(<asp:Literal ID="litDownloadSize" runat="server"></asp:Literal>&nbsp;MB)</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- BOX OFFICE and CATCH UP: video trailer and EPK -->
                        <div id="divFreeMedia" runat="server" clientidmode="Static">
                            <div class="hr_blckONwht"></div>
                            <div class="floatLeft">
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr id="trWatchTrailer" runat="server" clientidmode="Static">
                                        <td valign="top">
                                            <img src="/app_themes/onDemand/images/icon_playYellow.png" alt="" width="19" height="20" />
                                        </td>
                                        <td>
                                            <strong>
                                                <span class="link-<%=ProductTypeCssName%>">
                                                <asp:HyperLink ID="hlPlayTrailer" runat="server" ClientIDMode="Static">Watch the Trailer</asp:HyperLink></span></strong>
                                            <asp:Literal ID="litTrailerPlaying" runat="server" Visible="false"><br />(Now playing)</asp:Literal>
                                        </td>
                                    </tr>
                                    <tr id="trWatchEPK" runat="server" clientidmode="Static">
                                        <td valign="top">
                                            <img src="/app_themes/onDemand/images/icon_playYellow.png" alt="" width="19" height="20" />
                                        </td>
                                        <td>
                                            <strong>
                                                <span class="link-<%=ProductTypeCssName%>"><asp:HyperLink ID="hlPlayEPK" runat="server" ClientIDMode="Static">Watch the EPK</asp:HyperLink></span></strong>
                                            <asp:Literal ID="litEPKPlaying" runat="server" Visible="false"><br />(Now playing)</asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
    </div>
     <wuc:ErrorDialogue ID="wucErrorDialogue" runat="Server" Visible="False"></wuc:ErrorDialogue>
    <a href="#" style="display: none; visibility: hidden;" onclick="return false" id="lnkDummy"
        runat="server"></a>
    <%--<wuc:PayDialogue ID="wucPaymentdDlg" runat="server" TargetControlID="lnkDummy" ClientIDMode="Static"
        ShowDialogOnLoad="false" LoadViaUrlParameter="false" />--%>
        
</div>

<%--  pod Up Div--%>
<div id="Div1" style='position: absolute; left: -2000px;'>
    <div id="LightboxDivConsole" style='position: absolute;'>
        <div class="lb_global_wrapper">
            <div class="lb_errBg">
                <div class="lb_errMsg1" id="LightBoxHeader" runat="server">
                </div>
                <br />
                <div class="lb_errPoptxt" id="LightBoxMessage" runat="server">
                    <%=VideoDisplayMessage %>
                </div>
                <div class="errTxt" id="LightBoxLinks" runat="server">
                    <br />
                    <%--<a href="#">Try again</a> | <a href="#">Back</a> | <a href="#">DStv Home</a>--%>
                </div>
              </div>
        </div>
    </div>
</div>