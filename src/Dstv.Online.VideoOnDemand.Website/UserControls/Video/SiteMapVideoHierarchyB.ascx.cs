﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.UserControls.Shared.LightTreeView;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
    public partial class SiteMapVideoHierarchyB : LightTreeViewControl
    {
        private DataAccess.VideoClassificationForSection _productClassification = null;

        #region ClassificationConfigFile
        private string _classificationConfigFile = "";
        /// <summary>
        /// 
        /// </summary>
        public string ClassificationConfigFile
        {
            get { return _classificationConfigFile; }
            set { _classificationConfigFile = value; }
        }
        #endregion

        LightTreeView ltv = null;

        protected override void OnPreRender(EventArgs e)
        {
            if (ltv != null)
            {
                litTv.Text = ltv.ToString();
                RegisterScripts();
            }
            base.OnPreRender(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            List<DataAccess.VideoClassificationForSection> productClassificationList = DataAccess.VideoData.GetVideoClassificationHierarchy(SiteEnums.VideoDataType.ClassificationList, _classificationConfigFile);
            foreach (DataAccess.VideoClassificationForSection section in productClassificationList)
            {
                if (section.ProductType == VideoFilter.ProductType)
                {
                    _productClassification = section;
                    break;
                }
            }

            if (_productClassification == null)
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;

                PopulateTreeview();
            }
        }

        private void PopulateTreeview()
        {
            List<DataAccess.VideoClassificationItem> classificationItems = _productClassification.GetFirstLevelItems(true, true, true, false);

           

            foreach (DataAccess.VideoClassificationItem classificationItem in classificationItems)
            {
                Node treeNode = GetTreeViewNode(classificationItem);
                if (treeNode != null)
                {
                    if(ltv == null)
                    {
                        ltv = new LightTreeView(this.ClientID);
                    }
                    else
                    {
                         ltv.Nodes.Add(treeNode);
                    }
                }
            }
        }

        private Node GetTreeViewNode(DataAccess.VideoClassificationItem classificationItem)
        {
            Node treeNode = new Node(classificationItem.Name, classificationItem.FormattedUrl);

            if (classificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Link || classificationItem.ClassificationType == SiteEnums.VideoClassificationType.ProgramType_All)
            {
                return treeNode;
            }
            else if (classificationItem.SubItems.Count > 0)
            {
                foreach (DataAccess.VideoClassificationItem subItem in classificationItem.SubItems)
                {
                    Node subTreeNode = GetTreeViewNode(subItem);
                    if (subTreeNode != null)
                    {
                        treeNode.AddChild(subTreeNode);
                    }
                }
            }
            else if (classificationItem.VideoCount > 0)
            {
                AddVideoItemsToNode(classificationItem, treeNode);
            }
            else
            {
                return null;
            }

            return treeNode;
        }

        private void AddVideoItemsToNode(DataAccess.VideoClassificationItem videoClassificationItem, Node treeNode)
        {
            DataContract.CatalogueResult videoResult = null;

            if (videoClassificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Editorial)
            {
                Modules.VideoCatalogue.EditorialPlaylist editorialPlaylist = DataAccess.VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.Gallery, videoClassificationItem.Id, VideoFilter);

                if (editorialPlaylist != null)
                {
                    videoResult = editorialPlaylist.CatalogueItemSummaryList;
                }
            }
            else
            {
                if (VideoFilter.CatalogueCriteria.ClassificationCriteria == null)
                {
                    VideoFilter.CatalogueCriteria.ClassificationCriteria = new DataContract.ClassificationCriteria();
                }
                VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId = new DataContract.ClassificationItemIdentifier(videoClassificationItem.Id);
                VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = SiteEnums.Mapper.MapVideoClassificationTypeToBaseCatalogueItemClassificationType(videoClassificationItem.ClassificationType);

                videoResult = DataAccess.VideoData.GetVideoItems(SiteEnums.VideoDataType.List, VideoFilter);
            }

            VideoFilter.CatalogueCriteria.Paging.ItemsPerPage = -1;
            VideoFilter.CatalogueCriteria.Paging.PageNumber = 0;

            if (videoResult != null && videoResult.CatalogueItems.Length > 0)
            {
                foreach (DataContract.ICatalogueItem catalogueItem in videoResult.CatalogueItems)
                {
                    Node videoNode = new Node(catalogueItem.VideoTitle, PageLinks.GetFormattedVideoDetailUrl(catalogueItem));

                    //videoNode.ImageUrl = string.Format("/app_themes/onDemand/images/icon_arrowSml-{0}.png", catalogueItem.ProductTypeId.Value.ToString());

                    videoNode.ClassAttribute = "Play" + catalogueItem.ProductTypeId.Value;
                    treeNode.AddChild(videoNode);
                }
            }
        }
    }
}