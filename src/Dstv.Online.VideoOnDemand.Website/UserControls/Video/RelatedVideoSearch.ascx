﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedVideoSearch.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.RelatedVideoSearch" %>
<%@ Register Src="~/UserControls/Video/RelatedSummary.ascx" TagName="VideoSummary" TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoSummaryList.ascx" TagName="VideoList" TagPrefix="wuc" %>
<wuc:VideoList ID="wucVideoList" runat="server" />
<wuc:VideoSummary ID="wucVideoSummary" runat="server" ShowMoreLink="false" IncludeFullAbstractTooltip="true" />