﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class LatestProduct : VideoControlBase
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack)
			{
				PopulateControl();
			}
		}

		#region private void PopulateControl ()
		private void PopulateControl ()
		{
			// Set properties on tile control.
			wucVideoTileSummary.SetUrls(PageLinks);
			wucVideoTileSummary.RuntimeDisplay = RuntimeDisplay;

			// Set headings/titles.
			litProductTitle.Text = DisplayNameForProductType;
			litListTitle.Text = string.IsNullOrEmpty(ListTitle) ? "Top 5" : ListTitle;
			litProductDateHeader.Text = DateHeaderForProductType;

			// DataContract.VideoCatalogue videoCatalogue = new DataContract.VideoCatalogue();
			DataContract.CatalogueResult videoList = DataAccess.VideoData.GetVideoItems(SiteEnums.VideoDataType.List, VideoFilter);
			if (videoList != null && videoList.CatalogueItems.Length > 0)
			{
				_itemCount = videoList.CatalogueItems.Length;

				Random random = new Random();
				int randomIndex = random.Next(0, videoList.CatalogueItems.Length);
				wucVideoTileSummary.VideoItem = videoList.CatalogueItems[randomIndex];

				rptVideoList.DataSource = videoList.CatalogueItems;
				rptVideoList.DataBind();
			}
			else
			{
				_itemCount = 0;
			}

			this.Visible = _itemCount > 0;
		}
		#endregion


		#region protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
		/// <summary>
		/// Required for populating the video and show detail Url properties in the VideoTile control object.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				object videoTileObject = e.Item.FindControl("wucVideoTileList");
				if (videoTileObject != null)
				{
					VideoTileList videoTile = (VideoTileList)videoTileObject;
					videoTile.SetUrls(PageLinks);
				}
			}
		}
		#endregion

	}
}
