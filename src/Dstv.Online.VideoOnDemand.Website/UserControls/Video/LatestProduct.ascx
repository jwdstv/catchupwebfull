﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestProduct.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.LatestProduct" %>
<%@ OutputCache Duration="600" VaryByParam="none" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileSummary" Src="~/UserControls/Video/VideoTileSummary.ascx" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileList" Src="~/UserControls/Video/VideoTileList.ascx" %>
<div class="block450">
    <h5>
        Latest
        <asp:Literal ID="litProductTitle" runat="server"></asp:Literal></h5>
    <div class="greyHdrLine"></div>
    <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" SummaryDisplayType="VideoLayoutTextRight" 
                          IncludeFullAbstractTooltip="true"  />
    <div class="padding20px">
    </div>
    <h5>
        <asp:Literal ID="litListTitle" runat="server"></asp:Literal></h5>
    <div class="greyHdrLine">
    </div>
    <div class="topList450">
        <div class="expTextHdr-ext">
            <asp:Literal ID="litProductDateHeader" runat="server">Available until</asp:Literal></div>
        <asp:Repeater ID="rptVideoList" runat="server" OnItemCreated="rptVideoList_ItemCreated">
            <HeaderTemplate>
                <ul class="topList450">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <wuc:VideoTileList ID="wucVideoTileList" runat="server" VideoItem="<%#Container.DataItem%>" />
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div class="padding10px">
    </div>
    <div class="moreBtn">
        <a href="<%=MoreUrlFormatted%>">... more</a></div>
</div>
