﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoEpisodesSummary.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoEpisodesSummary" %>
<%@ Register TagPrefix="wuc" TagName="VideoTileSummary" Src="~/UserControls/Video/VideoTileSummary.ascx" %>
<h4>
    <%=ListTitle%></h4>
<div class="greyHdrLine">
</div>
<div class="divLeft">
    <asp:Repeater ID="rptVideoShown" runat="server" OnItemCreated="rptVideoList_ItemCreated">
        <ItemTemplate>
            <div class="episodeListCont">
            <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" SummaryDisplayType="EpisodeLayout"
                VideoItem="<%#Container.DataItem%>" IncludeFullAbstractTooltip="true" />
            </div>
        </ItemTemplate>
        <SeparatorTemplate>
            <div class="hr_dotline">
            </div>
        </SeparatorTemplate>
    </asp:Repeater>
    <div id="divShowAll" runat="server" clientidmode="Static" class="moreBtn3">
        <a class="" onclick="return showAllEpisodes();">show all videos</a>
    </div>
    <div id="secondary" style="display: none;">
        <asp:Repeater ID="rptVideoHidden" runat="server" OnItemCreated="rptVideoList_ItemCreated">
            <ItemTemplate>
                <div class="episodeListCont">
                <wuc:VideoTileSummary ID="wucVideoTileSummary" runat="server" SummaryDisplayType="EpisodeLayout"
                    VideoItem="<%#Container.DataItem%>" IncludeFullAbstractTooltip="true" />
                </div>
            </ItemTemplate>
            <SeparatorTemplate>
                <div class="hr_dotline">
                </div>
            </SeparatorTemplate>
        </asp:Repeater>
    </div>
</div>
<script type="text/javascript">
    function showAllEpisodes() {
        var showAllLink = document.getElementById('divShowAll');
        var hiddenDiv = document.getElementById('secondary');

        hiddenDiv.style.display = "";
        showAllLink.style.display = "none";
    }
</script>
