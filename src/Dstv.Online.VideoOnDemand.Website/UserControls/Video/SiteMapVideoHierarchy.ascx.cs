﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class SiteMapVideoHierarchy : VideoControlBase
	{

		private DataAccess.VideoClassificationForSection _productClassification = null;

		#region ClassificationConfigFile
		private string _classificationConfigFile = "";
		/// <summary>
		/// 
		/// </summary>
		public string ClassificationConfigFile
		{
			get { return _classificationConfigFile; }
			set { _classificationConfigFile = value; }
		}
		#endregion


		protected void Page_Load ( object sender, EventArgs e )
		{
			List<DataAccess.VideoClassificationForSection> productClassificationList = DataAccess.VideoData.GetVideoClassificationHierarchy(SiteEnums.VideoDataType.ClassificationList, _classificationConfigFile);
			foreach (DataAccess.VideoClassificationForSection section in productClassificationList)
			{
				if (section.ProductType == VideoFilter.ProductType)
				{
					_productClassification = section;
					break;
				}
			}

			if (_productClassification == null)
			{
				this.Visible = false;
			}
			else
			{
				this.Visible = true;

				PopulateTreeview();
			}
		}

		private void PopulateTreeview ()
		{
			List<DataAccess.VideoClassificationItem> classificationItems = _productClassification.GetFirstLevelItems(true, true, true, false);

			foreach (DataAccess.VideoClassificationItem classificationItem in classificationItems)
			{
				TreeNode treeNode = GetTreeViewNode(classificationItem);
				if (treeNode != null)
				{
					treeVideoHierarchy.Nodes.Add(treeNode);
				}
			}
		}

		private TreeNode GetTreeViewNode ( DataAccess.VideoClassificationItem classificationItem )
		{
			TreeNode treeNode = new TreeNode(classificationItem.Name);
			treeNode.PopulateOnDemand = false;
			treeNode.NavigateUrl = classificationItem.FormattedUrl;
			if (classificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Link || classificationItem.ClassificationType == SiteEnums.VideoClassificationType.ProgramType_All)
			{
				return treeNode;
			}
			else if (classificationItem.SubItems.Count > 0)
			{
				foreach (DataAccess.VideoClassificationItem subItem in classificationItem.SubItems)
				{
					TreeNode subTreeNode = GetTreeViewNode(subItem);
					subTreeNode.PopulateOnDemand = false;
					if (subTreeNode != null)
					{
						treeNode.ChildNodes.Add(subTreeNode);
					}
				}
			}
			else if (classificationItem.VideoCount > 0)
			{
				AddVideoItemsToNode(classificationItem, treeNode);
				treeNode.Expanded = false;
			}
			else
			{
				return null;
			}

			return treeNode;
		}

		private void AddVideoItemsToNode ( DataAccess.VideoClassificationItem videoClassificationItem, TreeNode treeNode )
		{
			DataContract.CatalogueResult videoResult = null;

			if (videoClassificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Editorial)
			{
				Modules.VideoCatalogue.EditorialPlaylist editorialPlaylist = DataAccess.VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.Gallery, videoClassificationItem.Id, VideoFilter);

				if (editorialPlaylist != null)
				{
					videoResult = editorialPlaylist.CatalogueItemSummaryList;
				}
			}
			else
			{
				if (VideoFilter.CatalogueCriteria.ClassificationCriteria == null)
				{
					VideoFilter.CatalogueCriteria.ClassificationCriteria = new DataContract.ClassificationCriteria();
				}
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId = new DataContract.ClassificationItemIdentifier(videoClassificationItem.Id);
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = SiteEnums.Mapper.MapVideoClassificationTypeToBaseCatalogueItemClassificationType(videoClassificationItem.ClassificationType);

				videoResult = DataAccess.VideoData.GetVideoItems(SiteEnums.VideoDataType.List, VideoFilter);
			}

			VideoFilter.CatalogueCriteria.Paging.ItemsPerPage = -1;
			VideoFilter.CatalogueCriteria.Paging.PageNumber = 0;

			if (videoResult != null && videoResult.CatalogueItems.Length > 0)
			{
				foreach (DataContract.ICatalogueItem catalogueItem in videoResult.CatalogueItems)
				{
					TreeNode videoNode = new TreeNode(catalogueItem.VideoTitle);
					// TODO: resolve once style is finalised. May use videoTile control.
					videoNode.NavigateUrl = PageLinks.GetFormattedVideoDetailUrl(catalogueItem);
					videoNode.ImageUrl = string.Format("/app_themes/onDemand/images/icon_arrowSml-{0}.png", catalogueItem.ProductTypeId.Value.ToString());
					treeNode.ChildNodes.Add(videoNode);
				}
			}
		}

		/*
		private void RenderVideoHierarchy ()
		{
			// litTitle.Text = string.Format("{0} Videos", SiteContstants.DisplayNames.GetDisplayNameForSite(_productClassification.ProductType));

			List<DataAccess.VideoClassificationItem> classificationItems = _productClassification.GetFirstLevelItems(true, true, true, false);

			classificationItems.RemoveAll(delegate( DataAccess.VideoClassificationItem item ) { return item.ClassificationType == SiteEnums.VideoClassificationType.ProgramType_All; });

			rptClassificationItems.DataSource = classificationItems;
			rptClassificationItems.DataBind();
		}

		protected void rptClassificationItems_ItemDataBound ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Repeater rptClassificationSubItems = GetRepeaterSubControl(e.Item, "rptClassificationSubItems");
				Repeater rptClassificationVideoItems = GetRepeaterSubControl(e.Item, "rptClassificationVideoItems");
				DataAccess.VideoClassificationItem classificationItem = (DataAccess.VideoClassificationItem)e.Item.DataItem;
				if (classificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Link)
				{
					if (rptClassificationSubItems != null)
					{
						rptClassificationSubItems.Visible = false;
					}
					if (rptClassificationVideoItems != null)
					{
						rptClassificationVideoItems.Visible = false;
					}
				}
				else
				{
					if (classificationItem.SubItems.Count > 0)
					{
						// Has classification sub items.
						if (rptClassificationSubItems != null)
						{
							rptClassificationSubItems.Visible = true;

							rptClassificationSubItems.DataSource = classificationItem.SubItems;
							rptClassificationSubItems.DataBind();
						}
						if (rptClassificationVideoItems != null)
						{
							rptClassificationVideoItems.Visible = false;
						}
					}
					else
					{
						// Has video items.
						PopulateVideoItems(classificationItem, rptClassificationVideoItems);

						if (rptClassificationSubItems != null)
						{
							rptClassificationSubItems.Visible = false;
						}
					}
				}
			}
		}

		protected void rptClassificationSubItems_ItemDataBound ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Repeater rptClassificationVideoItems = GetRepeaterSubControl(e.Item, "rptClassificationVideoItems");
				DataAccess.VideoClassificationItem classificationItem = (DataAccess.VideoClassificationItem)e.Item.DataItem;
				if (classificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Link || classificationItem.SubItems.Count > 0) // no 3rd level at the moment.
				{
					if (rptClassificationVideoItems != null)
					{
						rptClassificationVideoItems.Visible = false;
					}
				}
				else
				{
					// Has video items.
					PopulateVideoItems(classificationItem, rptClassificationVideoItems);
				}
			}
		}

		protected void rptClassificationVideoItems_ItemCreated ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				object videoTileObject = e.Item.FindControl("wucVideoTileList");
				if (videoTileObject != null)
				{
					VideoTileList videoTile = (VideoTileList)videoTileObject;
					videoTile.SetUrls(PageLinks);
				}
			}
		}

		private bool PopulateVideoItems ( DataAccess.VideoClassificationItem videoClassificationItem, Repeater videoItemRepeater )
		{
			if (videoClassificationItem == null || videoItemRepeater == null)
			{
				return false;
			}

			DataContract.CatalogueResult videoResult = null;

			if (videoClassificationItem.ClassificationType == SiteEnums.VideoClassificationType.Item_Editorial)
			{
				Modules.VideoCatalogue.EditorialPlaylist editorialPlaylist = DataAccess.VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.Gallery, videoClassificationItem.Id, VideoFilter);

				if (editorialPlaylist != null)
				{
					videoResult = editorialPlaylist.CatalogueItemSummaryList;
				}
			}
			else
			{
				if (VideoFilter.CatalogueCriteria.ClassificationCriteria == null)
				{
					VideoFilter.CatalogueCriteria.ClassificationCriteria = new DataContract.ClassificationCriteria();
				}
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationItemId = new DataContract.ClassificationItemIdentifier(videoClassificationItem.Id);
				VideoFilter.CatalogueCriteria.ClassificationCriteria.ClassificationType = SiteEnums.Mapper.MapVideoClassificationTypeToBaseCatalogueItemClassificationType(videoClassificationItem.ClassificationType);

				videoResult = DataAccess.VideoData.GetVideoItems(SiteEnums.VideoDataType.List, VideoFilter);
			}

			VideoFilter.CatalogueCriteria.Paging.ItemsPerPage = -1;
			VideoFilter.CatalogueCriteria.Paging.PageNumber = 0;

			if (videoResult != null && videoResult.CatalogueItems.Length > 0)
			{
				videoItemRepeater.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
				videoItemRepeater.DataSource = videoResult.CatalogueItems;
				videoItemRepeater.DataBind();

				return true;
			}
			else
			{
				videoItemRepeater.Visible = false;

				return false;
			}
		}

		private Repeater GetRepeaterSubControl ( RepeaterItem repeaterParent, string repeaterId )
		{
			var repeaterObj = repeaterParent.FindControl(repeaterId);
			if (repeaterObj == null)
			{
				return null;
			}
			else
			{
				return (Repeater)repeaterObj;
			}
		}
		*/
	}
}