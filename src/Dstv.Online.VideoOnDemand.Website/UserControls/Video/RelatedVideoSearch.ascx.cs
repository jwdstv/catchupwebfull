﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class RelatedVideoSearch : VideoControlBase
	{

		#region Public Properties

		
		#region ListingDisplayType
        /// <summary>
		/// The Layout of the related videos (summary or List)
		/// </summary>
		private SiteEnums.VideoListingDisplay _displayLayout = SiteEnums.VideoListingDisplay.HorizontalSummary;
		/// <summary>
		/// 
		/// </summary>
		public SiteEnums.VideoListingDisplay DisplayLayout
		{
			get { return _displayLayout; }
			set { _displayLayout = value; }
		}
		#endregion

		/// <summary>
		/// Search Term for finding related videos
		/// </summary>
		public string SearchTerm { get; set; }

        public Dstv.Online.VideoOnDemand.MediaCatalogue.ICatalogueItem VideoItem { get; set; }

		/// <summary>
		/// Program name not to include in the related video results
		/// </summary>
		public string ExcludedProgramName { get; set; }

		/// <summary>
		/// Video Id's not to include in the related video results
		/// </summary>
        public string VideoExclusionList { get; set; }

		/// <summary>
		/// Number of video items to display
		/// </summary>
		public int VideoCount { get; set; }

		/// <summary>
		/// Title of the list
		/// </summary>
		public new string ListTitle { get; set; }

		#endregion


		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!string.IsNullOrWhiteSpace(SearchTerm))
			{
				PopulateControl();
			}
		}


		#region protected void PopulateControl ()
		/// <summary>
		/// 
		/// </summary>
		protected void PopulateControl ()
		{
            int searchResultLimit = VideoCount;

            // Dont set a page size limit on the video criteria 
            // MMS treats ordinal as alphabetical first and then orders the results in the sequence which the IDs originally sent
            // If you limit the page size, the result set returned will be incorrect
            VideoFilter.VideoCount = 0;

			// find videos that match search term
            CatalogueResult searchResult = DataAccess.VideoData.GetRelatedVideoItems(SiteEnums.VideoDataType.Summary, VideoFilter, int.Parse(VideoItem.CatalogueItemId.Value), VideoItem.Keywords, VideoItem.ProgramId.Value, VideoItem.ProgramGenre, VideoItem.ProgramSubGenre, new List<int>(new int[] { VideoItem.ProductId }));
			List<ICatalogueItem> filteredResults = new List<ICatalogueItem>();
			int counter = 0;

			foreach (ICatalogueItem item in searchResult.CatalogueItems)
			{
                filteredResults.Add(item);
                counter++;

				if (counter >= VideoCount)  // limit video tiles
				{
					break;
				}
			}

			if (filteredResults.Count > 0)
			{
				if (_displayLayout == SiteEnums.VideoListingDisplay.VerticalList)
				{
					wucVideoList.ListName = ListTitle;
					wucVideoList.VideoItems = filteredResults;
					wucVideoList.Visible = true;
					wucVideoSummary.Visible = false;
				}
				else
				{
					wucVideoSummary.ListTitle = ListTitle;
					wucVideoSummary.SummaryDisplayType = _displayLayout == SiteEnums.VideoListingDisplay.HorizontalSummary ? SiteEnums.VideoSummaryDisplay.VideoLayoutTextBottom : SiteEnums.VideoSummaryDisplay.VideoLayoutTextRight;
					wucVideoSummary.Visible = true;
					wucVideoSummary.PageLinks = this.PageLinks;
					wucVideoSummary.PopulateVideoList(filteredResults.ToArray());
					wucVideoList.Visible = false;
				}
			}
			else
			{
				Visible = false;
			}
		}
	}

		#endregion

}
