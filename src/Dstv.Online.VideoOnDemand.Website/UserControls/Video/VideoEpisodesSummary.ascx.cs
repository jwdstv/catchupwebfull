﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
    public partial class VideoEpisodesSummary : VideoControlBase
    {
        #region public properties
        /// <summary>
        /// The currently visible series video Id
        /// </summary>
        public string VideoId { get; set; }

        /// <summary>
        /// Indicates whether to exclude the video matching VideoId from the list of videos (series episodes) displayed.
        /// </summary>
        public bool ExcludeVideoIdFromDisplay { get; set; }

        /// <summary>
        /// Video Program (Result)
        /// </summary>
        public ProgramResult VideoProgram { get; set; }

        /// <summary>
        /// The number of video tiles to initially display
        /// </summary>
        public int VideoCount { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
                PopulateControl();
        }

        #region public void PopulateVideoList ( List<DataContract.VideoItemSummary> videoList )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="videoList"></param>
        public void PopulateVideoList(DataContract.ICatalogueItem[] videoList)
        {
            // No video items?
            if (videoList.Length == 0)
            {
                Visible = false;
                return;
            }

            // Only the first {VideoCount} video items are shown, the rest are hidden in the second div
            if (videoList.Length <= VideoCount)
            {
                rptVideoShown.DataSource = videoList;
                rptVideoShown.DataBind();

                divShowAll.Visible = false;
            }
            else
            {
                ICatalogueItem[] visibleItems = new ICatalogueItem[VideoCount];
                ICatalogueItem[] hiddenItems = new ICatalogueItem[videoList.Length - VideoCount];

                // Visible videos
                for (int k = 0; k < VideoCount; k++)
                {
                    visibleItems[k] = videoList[k];
                }

                // Hidden videos
                for (int k = VideoCount; k < videoList.Length; k++)
                {
                    hiddenItems[k - VideoCount] = videoList[k];
                }

                // Bind data to the repeaters
                rptVideoShown.DataSource = visibleItems;
                rptVideoHidden.DataSource = hiddenItems;

                rptVideoShown.DataBind();
                rptVideoHidden.DataBind();
            }
        }
        #endregion


        protected new void PopulateControl()
        {
            if (VideoProgram == null && !string.IsNullOrEmpty(VideoId))
            {
                VideoProgram = DataAccess.VideoData.GetVideoDetailsForProgram(VideoId, VideoProgramType.ShowEpisode);
            }

            if (VideoProgram != null)
            {
                List<ICatalogueItem> catalogueItems = new List<ICatalogueItem>();
                if (ExcludeVideoIdFromDisplay && !string.IsNullOrEmpty(VideoId))
                {
                    catalogueItems.AddRange(VideoProgram.VideoItems.FindAll(delegate(VideoDetailItem videoItem) { return videoItem.CatalogueItem.CatalogueItemId.Value != VideoId; }).Select(item => item.CatalogueItem));
                }
                else
                {
                    catalogueItems.AddRange(VideoProgram.VideoItems.Select(item => item.CatalogueItem));
                }

                if (catalogueItems.Count > 0)
                {
                    // Sort by Aired Date desc.
                    catalogueItems.Sort(
                        (item1, item2) =>
                        (item2.Airdate.HasValue ? item2.Airdate.Value : DateTime.MinValue).CompareTo(
                            item1.Airdate.HasValue ? item1.Airdate.Value : DateTime.MinValue)
                        );

                    PopulateVideoList(catalogueItems.ToArray());
                }
                else
                {
                    Visible = false;
                }
            }
            else
            {
                Visible = false;
            }
        }


        #region protected void rptVideoList_ItemCreated ( object sender, RepeaterItemEventArgs e )
        /// <summary>
        /// Required for populating the video and show detail Url properties in the VideoTile control object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptVideoList_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                object videoTileObject = e.Item.FindControl("wucVideoTileSummary");
                if (videoTileObject != null)
                {
                    VideoTileSummary videoTile = (VideoTileSummary)videoTileObject;
                    videoTile.SetUrls(PageLinks);
                }
            }
        }
        #endregion

    }
}