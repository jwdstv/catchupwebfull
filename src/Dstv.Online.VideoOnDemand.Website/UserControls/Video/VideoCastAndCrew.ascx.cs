﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
	public partial class VideoCastAndCrew : System.Web.UI.UserControl
	{
		private string _videoId = "";
		/// <summary>
		/// 
		/// </summary>
		public string VideoId
		{
			get { return _videoId; }
			set { _videoId = value; }
		}

		private ProgramResult _programDetail = null;
		/// <summary>
		/// 
		/// </summary>
		public ProgramResult ProgramDetail
		{
			get { return _programDetail; }
			set { _programDetail = value; }
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
				if (_programDetail == null && !string.IsNullOrEmpty(_videoId))
				{
					_programDetail = _programDetail = DataAccess.VideoData.GetVideoDetailsForProgram(_videoId, DataContract.VideoProgramType.Unknown);
				}

				if (_programDetail != null)
				{
					PopulateControl(_programDetail.CastAndCrew);
				}
				else
				{
					this.Visible = false;
				}
		}

		public void PopulateControl ( List<DataContract.ICastMember> castAndCrewList )
		{
			if (castAndCrewList != null && castAndCrewList.Count > 0)
			{
				// Set Cast and Crew (Design has a split table)
				rptCastandCrewCell1.DataSource = castAndCrewList;
				rptCastandCrewCell1.DataBind();

				rptCastandCrewCell2.DataSource = castAndCrewList;
				rptCastandCrewCell2.DataBind();

				this.Visible = true;
			}
			else
			{
				this.Visible = false;
			}
		}
	}
}