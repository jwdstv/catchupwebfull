﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoTileSummary.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoTileSummary" %>
<div class="latestItem-ext" id="divVideoTextRight" runat="server" clientidmode="Static">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 114px;" valign="top" rowspan="<%=PropertyDisplayCount%>">
                <div class="thumb-<%=ProductTypeCssName%>">
                    <div class="vid-time">
                        <%=VideoRuntimeDisplay%></div>
                    <a href="<%=VideoThumbnailPageUrl%>" title="<%=VideoTooltip%>"><span></span>
                        <img class="vid-thumb" src="<%=VideoThumbnailImgUrl%>" /></a>
                </div>
            </td>
            <td class="title">
                <a href="<%=VideoTitlePageUrl%>">
                    <%=VideoTitle%></a>
            </td>
        </tr>
        <tr id="trEpisodeTitleR" runat="server">
            <td class="episode">
                <%=VideoEpisodeTitle%>
            </td>
        </tr>
        <tr id="trAirDateR" runat="server">
            <td class="time">
                Aired on
                <%=VideoAirDate%>
                |
                <%=VideoAgeRestriction%>
            </td>
        </tr>
        <tr id="trExpiryDateR" runat="server">
            <td class="time">
                Available until
                <%=VideoExpiryDate%>
            </td>
        </tr>
        <tr id="trAvailableDateR" runat="server">
            <td class="time">
                Available on
                <%=VideoStartDate%>
            </td>
        </tr>
        <tr>
            <td class="info">
                <asp:Literal ID="litDoHoverR" runat="Server"></asp:Literal>
                <div id="divTooltipR" runat="server" visible="false">
                    <div onmouseover="showMore('<%=divTooltipR.ClientID+VideoId%>')" onmouseout="showLess('<%=divTooltipR.ClientID+VideoId%>')"
                        style="display: none;" class="item_moreOL_R-ext" id="more_<%=divTooltipR.ClientID+VideoId%>">
                        <span class="browntxt-ext">
                            <%=VideoAbstract%></span></div>
                    <div onmouseout="showLess('<%=divTooltipR.ClientID+VideoId%>')" onmouseover="showMore('<%=divTooltipR.ClientID+VideoId%>')">
                        <span class="browntxt">
                            <%=VideoAbstractTrimmed%></span></div>
                </div>
            </td>
        </tr>
    </table>
</div>
<div id="divEpisodeLayout" class="floatLeft" runat="server" clientidmode="Static">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 114px;" valign="top" rowspan="4">
                <div class="thumb-<%=ProductTypeCssName%>">
                    <div class="vid-time">
                        <%=VideoRuntimeDisplay%></div>
                    <a href="<%=VideoDetailPageUrl%>" title="<%=VideoTooltip%>"><span></span>
                        <img alt="<%=VideoTitle %>" class="vid-thumb" src="<%=VideoThumbnailImgUrl%>" /></a>
                </div>
            </td>
            <td class="title">
                <a href="<%=VideoDetailPageUrl%>">Episode
                    <%=VideoEpisodeNumber%>, S<%=VideoSeasonNumber%></a>
            </td>
        </tr>
        <tr id="tr2" runat="server">
            <td colspan="2" class="time">
                Aired on
                <%=VideoAirDate%>
                |
                <%=VideoAgeRestriction%>
            </td>
        </tr>
        <tr id="tr3" runat="server">
            <td colspan="2" class="time">
                Available until
                <%=VideoExpiryDate%>
            </td>
        </tr>
        <tr id="trAbstractEp" runat="server">
            <td class="info" colspan="2">
                <asp:Literal ID="litDoHoverEp" runat="Server"></asp:Literal>
                <div id="divToolTipEp" runat="server" visible="false">
                    <div onmouseover="showMore('<%=divToolTipEp.ClientID+VideoId%>')" onmouseout="showLess('<%=divToolTipEp.ClientID+VideoId%>')"
                        style="display: none;" class="OD_item_sep_H-ext" id="more_<%=divToolTipEp.ClientID+VideoId%>">
                        <span class="browntxt-ext">
                            <%=VideoAbstract%></span></div>
                    <div onmouseout="showLess('<%=divToolTipEp.ClientID+VideoId%>')" onmouseover="showMore('<%=divToolTipEp.ClientID+VideoId%>')">
                        <span class="browntxt">
                            <%=VideoAbstractTrimmed%></span></div>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="OD_item" id="divVideoTextBottom" runat="server" clientidmode="Static">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="text-align: left">
                    <div class="thumb-<%=ProductTypeCssName%>">
                        <div class="vid-time">
                            <%=VideoRuntimeDisplay%></div>
                        <a href="<%=VideoThumbnailPageUrl%>" title="<%=VideoTooltip%>"><span></span>
                            <img class="vid-thumb" src="<%=VideoThumbnailImgUrl%>" /></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <a href="<%=VideoTitlePageUrl%>">
                        <%=VideoTitle%></a>
                </td>
            </tr>
            <tr id="trEpisodeTitleB" runat="server">
                <td class="episode">
                    <%=VideoEpisodeTitle%>
                </td>
            </tr>
            <tr id="trAirDateB" runat="server">
                <td class="time">
                    Aired on
                    <%=VideoAirDate%>
                    |
                    <%=VideoAgeRestriction%>
                </td>
            </tr>
            <tr id="trExpiryDateB" runat="server">
                <td class="time">
                    Available until
                    <%=VideoExpiryDate%>
                </td>
            </tr>
            <tr id="trAvailableDateB" runat="server">
                <td class="time">
                    Available on
                    <%=VideoStartDate%>
                </td>
            </tr>
            <tr id="trAbstractB" runat="server">
                <td class="info">
                    <asp:Literal ID="litDoHoverB" runat="server"></asp:Literal>
                    <div id="divTooltipB" runat="server" visible="false">
                        <div onmouseover="showMore('<%=divTooltipB.ClientID+VideoId%>')" onmouseout="showLess('<%=divTooltipB.ClientID+VideoId%>')"
                            style="display: none;" class="item_moreOL-ext" id="more_<%=divTooltipB.ClientID+VideoId%>">
                            <span class="browntxt-ext">
                                <%=VideoAbstract%></span></div>
                        <div onmouseout="showLess('<%=divTooltipB.ClientID+VideoId%>')" onmouseover="showMore('<%=divTooltipB.ClientID+VideoId%>')">
                            <span class="browntxt">
                                <%=VideoAbstractTrimmed%></span></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="OD_item2" id="divVideoTextBottom2" runat="server" clientidmode="Static">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="text-align: left">
                    <div class="thumb-<%=ProductTypeCssName%>">
                        <div class="vid-time">
                            <%=VideoRuntimeDisplay%></div>
                        <a href="<%=VideoThumbnailPageUrl%>" title="<%=VideoTooltip%>"><span></span>
                            <img class="vid-thumb" src="<%=VideoThumbnailImgUrl%>" /></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <a href="<%=VideoTitlePageUrl%>">
                        <%=VideoTitle%></a>
                </td>
            </tr>
            <tr id="trEpisodeTitleB2" runat="server">
                <td class="episode">
                    <%=VideoEpisodeTitle%>
                </td>
            </tr>
            <tr id="trAirDateB2" runat="server">
                <td class="time">
                    Aired on
                    <%=VideoAirDate%>
                    |
                    <%=VideoAgeRestriction%>
                </td>
            </tr>
            <tr id="trExpiryDateB2" runat="server">
                <td class="time">
                    Available until
                    <%=VideoExpiryDate%>
                </td>
            </tr>
            <tr id="trAvailableDateB2" runat="server">
                <td class="time">
                    Available on
                    <%=VideoStartDate%>
                </td>
            </tr>
            <tr id="trAbstractB2" runat="server">
                <td class="info">
                    <asp:Literal ID="litDoHoverB2" runat="Server"></asp:Literal>
                    <div id="divTooltipB2" runat="server" visible="false">
                        <div onmouseover="showMore('<%=divTooltipB2.ClientID+VideoId%>')" onmouseout="showLess('<%=divTooltipB2.ClientID+VideoId%>')"
                            style="display: none;" class="item_moreOL-ext" id="more_<%=divTooltipB2.ClientID+VideoId%>">
                            <span class="browntxt-ext">
                                <%=VideoAbstract%></span></div>
                        <div onmouseout="showLess('<%=divTooltipB2.ClientID+VideoId%>')" onmouseover="showMore('<%=divTooltipB2.ClientID+VideoId%>')">
                            <span class="browntxt">
                                <%=VideoAbstractTrimmed%></span></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="OD_item2" id="divShowLayout" runat="server" clientidmode="Static">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="text-align: left">
                    <div class="thumb-show">
                        <a href="<%=VideoTitlePageUrl%>" title="<%=VideoTooltip%>"><span></span>
                            <img class="vid-thumb" src="<%=VideoThumbnailImgUrl%>" /></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <a href="<%=VideoTitlePageUrl%>">
                        <%=VideoTitle%></a>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <%=ProgramShowInfo%>
                </td>
            </tr>
        </tbody>
    </table>
</div>
