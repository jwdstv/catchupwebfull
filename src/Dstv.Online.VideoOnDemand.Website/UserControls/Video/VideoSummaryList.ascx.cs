﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
    public partial class VideoSummaryList : System.Web.UI.UserControl
    {

        /// <summary>
        /// The text to display at the top of the control (the list name)
        /// </summary>
        public string ListName { get; set; }

        /// <summary>
        /// The Video Items to display
        /// </summary>
        public List<ICatalogueItem> VideoItems { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (VideoItems != null && VideoItems.Count > 0)
                {
                    litProductTitle.Text = ListName;
                    rptTiles.DataSource = VideoItems;
                    rptTiles.DataBind();
                }
                else
                {
                    Visible = false;
                }
            }
        }
    }
}