﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using SiteData = Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Transactions;
using System.Text;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data.ResponseTypes;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo;
using DStvConnect.WebUI.Controls;
using System.Configuration;
using System.Net;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{

    public partial class VideoDetailConsole : UserControls.Video.VideoTileBase
    {

        private VideoDetailItem _trailerDetail = null;
        private VideoDetailItem _epkDetail = null;
        private VideoDetailItem _videoDetail = null;

        public string VideoDisplayMessage { get; set; }

        /// <summary>
        /// checks if the user can watch the video or not.
        /// </summary>
        /// <returns></returns>
        public bool UserCanViewVideo()
        {
            VideoDisplayMessage = string.Empty;
            bool userCanViewVideo = true;
            PlayCheck playCheck = new PlayCheck();
            string msg = string.Empty;

            if (VideoProductType == ProductTypes.BoxOffice)
            {
                //userCanViewVideo = BoxOfficeVideoValidate(out msg);
                userCanViewVideo = playCheck.CanBuildFeedBoxOffice(VideoItem.CatalogueItemId.Value, out msg);
            }
            else if (VideoProductType == ProductTypes.CatchUp)
            {
                userCanViewVideo = playCheck.CanBuildFeedCatchUp(VideoItem.CatalogueItemId.Value, out msg);
            }

            if (!userCanViewVideo)
            {
                divDownloadVideo.Visible = false;

                if (!string.IsNullOrEmpty(msg))//Show Message
                {
                    VideoDisplayMessage = msg;
                    litNotSubscriber.Text = msg;
                    runjQueryCode(string.Format("{0}{1}{2}", "jQuery.lightbox(", "\"#LightboxDivConsole\",", "{modal: true});"));
                }
            }

            return userCanViewVideo;
        }


        #region ConsoleDisplayType
        /// <summary>
        /// 
        /// </summary>
        private SiteEnums.VideoConsoleDisplay _consoleDisplayType = SiteEnums.VideoConsoleDisplay.Video;
        /// <summary>
        /// 
        /// </summary>
        public SiteEnums.VideoConsoleDisplay ConsoleDisplayType
        {
            get { return _consoleDisplayType; }
            set { _consoleDisplayType = value; }
        }
        #endregion


        #region MediaDisplayAction

        private SiteEnums.MediaDisplayActionType _mediaDisplayAction = SiteEnums.MediaDisplayActionType.Unknown;// set default to play video. it was previously Unknown
        /// <summary>
        /// 
        /// </summary>
        public SiteEnums.MediaDisplayActionType MediaDisplayAction
        {
            get { return _mediaDisplayAction; }
            set { _mediaDisplayAction = value; }
        }
        #endregion


        #region ProgramDetail
        private ProgramResult _programDetail = null;
        /// <summary>
        /// 
        /// </summary>
        public ProgramResult ProgramDetail
        {
            get { return _programDetail; }
            set { _programDetail = value; }
        }
        #endregion


        #region IsCurrentlyRented
        private bool _isRented = false;
        /// <summary>
        /// 
        /// </summary>
        public bool IsCurrentlyRented
        {
            get { return _isRented; }
        }
        #endregion


        #region TopUpUrl
        private string _topUpUrl = "";
        /// <summary>
        /// 
        /// </summary>
        public string TopUpUrl
        {
            get { return _topUpUrl; }
            set { _topUpUrl = value; }
        }
        #endregion


        #region IsCurrentlyPlayingMovie
        /// <summary>
        /// 
        /// </summary>
        public bool IsCurrentlyPlayingMovie
        {
            get { return _mediaDisplayAction == SiteEnums.MediaDisplayActionType.PlayVideo; }
        }
        #endregion


        #region JavaScriptRentalExpiryDate
        private DateTime? _rentalExpiryDate = null;
        /// <summary>
        /// 
        /// </summary>
        public string JavaScriptRentalExpiryDate
        {
            get
            {
                return SiteUtils.JavaScript.GetNewDateString(_rentalExpiryDate, true);
            }
        }

        public int JavaScriptRentalMinutesLeft
        {
            get
            {
                if (_rentalExpiryDate.HasValue && _rentalExpiryDate > DateTime.Now)
                {
                    return (int)(_rentalExpiryDate.Value - DateTime.Now).TotalMinutes;
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion


        #region PlayIcon
        /// <summary>
        /// 
        /// </summary>
        public string PlayIcon
        {
            get { return _programDetail != null ? (VideoProductType == ProductTypes.BoxOffice ? "icon_greenPlay.png" : "icon_playBlue.png") : "icon_playBlue.png"; }
        }
        #endregion


        #region TitleIcon
        /// <summary>
        /// 
        /// </summary>
        public string TitleIcon
        {
            get { return VideoProductType == ProductTypes.BoxOffice ? "hdr-boxOffice.png" : "hdr-catchUp.png"; }
        }
        #endregion


        private string getjQueryCode(string jsCodetoRun)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            sb.AppendLine(jsCodetoRun);
            sb.AppendLine(" });");

            return sb.ToString();
        }

        private void runjQueryCode(string jsCodetoRun)
        {

            ScriptManager requestSM = ScriptManager.GetCurrent(this.Page);
            //System.Web.UI.ClientScriptManager ClientScript = new ClientScriptManager

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(Page),
                                                   Guid.NewGuid().ToString(),
                                                   getjQueryCode(jsCodetoRun),
                                                   true);

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (_programDetail != null && _programDetail.PrimaryVideoItem != null && _programDetail.PrimaryVideoItem.CatalogueItem != null)
            {
                VideoItem = _programDetail.PrimaryVideoItem.CatalogueItem;

                divNoVideo.Visible = false;
                divVideoConsole.Visible = true;

                SetTrailerDetail();

                SetLeftConsole();

                SetRightConsole();

                SetTrailerConsole();

                SetMediaAction();
            }
            else
            {
                divNoVideo.Visible = true;
                divVideoConsole.Visible = false;
            }
        }


        #region private void SetTrailerDetail ()
        /// <summary>
        /// 
        /// </summary>
        private void SetTrailerDetail()
        {
            // Currently only support Trailers (no EPK in MMS and no media type to identify it).
            List<VideoDetailItem> trailerMedia = _programDetail.GetVideoDetailItems(new DataContract.VideoProgramType[] { DataContract.VideoProgramType.MovieTrailer }, DataContract.MediaTypeEnum.Flash);
            if (trailerMedia != null && trailerMedia.Count > 0)
            {
                _trailerDetail = trailerMedia[0];
            }

            List<VideoDetailItem> epkMedia = _programDetail.GetVideoDetailItems(new DataContract.VideoProgramType[] { DataContract.VideoProgramType.MovieEPK }, DataContract.MediaTypeEnum.Flash);
            if (epkMedia != null && epkMedia.Count > 0)
            {
                _epkDetail = epkMedia[0];
            }
        }
        #endregion


        #region private void SetLeftConsole ()
        /// <summary>
        /// 
        /// </summary>
        private void SetLeftConsole()
        {
            // Set video detail (left console)
            litTitle.Text = _programDetail.ProgramTitle;
            if (IsEpisode && _consoleDisplayType == SiteEnums.VideoConsoleDisplay.Video)
            {
                if (VideoProductType == ProductTypes.CatchUp)
                {
                    litTitle.Text = "";
                    hlTitle.Text = _programDetail.ProgramTitle;
                    hlTitle.NavigateUrl = VideoShowPageUrl;
                }

                litEpisodeTitle.Text = VideoEpisodeTitle;
            }
            else
            {
                divEpisodeTitle.Visible = false;
            }


            if (_consoleDisplayType == SiteEnums.VideoConsoleDisplay.Show)
            {
                litTitle.Text = string.Format("{0}, S{1}", _programDetail.ProgramTitle, VideoSeasonNumber);
                divVideoProperties.Visible = false;
                litAbstract.Text = ProgramAbstract;
            }
            else
            {
                divVideoProperties.Visible = true;

                litAbstract.Text = VideoAbstract;
                litAgeRestriction.Text = VideoItem.AgeRestriction;
                litRuntime.Text = VideoRuntimeDisplay;
                if (VideoProductType == ProductTypes.CatchUp)
                {
                    litAiredOn.Text = VideoAirDate;
                    trDateAired.Visible = true;
                }
                else
                {
                    trDateAired.Visible = false;
                }
                litAvailableTill.Text = VideoExpiryDate;

                if (IsForthcomingRelease)
                {
                    trAvailableTill.Visible = false;
                }
            }
        }
        #endregion


        private void SetRightConsole()
        {
            // Title Image
            imgTitleBO.Visible = false;
            imgTitleCU.Visible = false;
            if (VideoProductType == ProductTypes.BoxOffice)
            {
                imgTitleBO.Visible = true;
            }
            else if (VideoProductType == ProductTypes.CatchUp)
            {
                imgTitleCU.Visible = true;
            }

            SetVideoRentalConsole();

            SetVideoWatchConsole();
        }


        #region private void SetVideoRentalConsole ()
        /// <summary>
        /// 
        /// </summary>
        private void SetVideoRentalConsole()
        {
            _isRented = false; 

            if (_consoleDisplayType == SiteEnums.VideoConsoleDisplay.Video && VideoProductType == ProductTypes.BoxOffice)
            {
                //wucPaymentdDlg.Visible = false;
                divBoxOfficeAvailableToRent.Visible = false;
                divBoxOfficeNotAvailableToRent.Visible = false;
                divBoxOfficeRented.Visible = false;
                divBoxOfficeNotRented.Visible = false;
                divDownloadVideo.Visible = false;
                divChannel.Visible = false;

                hlBoxOfficeNotRented.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PayForVideo);

                if (Page.User.Identity.IsAuthenticated)
                {
                    string customerId = SiteData.DstvUser.UserProfile.CustomerId.Value;
                    RentalResult rentalResult;
                    Rental rental = new Rental();

                    // Check whether user has this item rented and it's not expired.
                    rentalResult = rental.GetVideoRentalItemByUserIdAndItemId(customerId, VideoItem.CatalogueItemId.Value);

                    if (rentalResult.TotalResults == 0 || rentalResult.Rentals.Count == 0 || rentalResult.Rentals[0].HasRentalExpired)
                    {
                        string sessionName = string.Format("AssetId_{0}", VideoItem.AssetId);
                        object objConnectPurchaseResult = Session[sessionName];
                        // If the session variable exists and is not false, Connect has successfully deducted the amount from the wallet.
                        // We need to add a record in the DB for the rental
                        // Remove the session variable and display a message after this record has been successfully added
                        if (objConnectPurchaseResult != null
                            && objConnectPurchaseResult.ToString().ToUpper() != bool.FalseString.ToUpper())
                        {
                            TransactionResponse transResponse = DstvUser.RentVideo(VideoItem.CatalogueItemId.Value, VideoItem.RentalAmount.Value, null, objConnectPurchaseResult.ToString());

                            if (transResponse != null && transResponse.Success)
                            {
                                Session.Remove(sessionName);

                                rentalResult = rental.GetVideoRentalItemByUserIdAndItemId(customerId, VideoItem.CatalogueItemId.Value);

                                _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;

                                VideoDisplayMessage = string.Format("Payment successful, remaining balance {0}", DstvUser.WalletBalance);
                                runjQueryCode("jQuery.lightbox('#LightboxDivConsole','{modal: true});");
                            }
                        }
                    }


                    if (rentalResult.TotalResults > 0 && rentalResult.Rentals.Count > 0 && !rentalResult.Rentals[0].HasRentalExpired)
                    {
                        _isRented = true;

                        _rentalExpiryDate = rentalResult.Rentals[0].ExpiryDate;

                        TimeSpan? ts = _rentalExpiryDate - DateTime.Now;

                        divTimeLeft.InnerText = string.Format("{0}hrs, {1}min", (int)(ts.Value.TotalMinutes / 60), (int)(ts.Value.TotalMinutes % 60));
                    }
                }

                //// Set payment dialogue settings.
                //wucPaymentdDlg.ShowDialogOnLoad = false;
                //wucPaymentdDlg.CatalogItemId = VideoItem.CatalogueItemId;
                //wucPaymentdDlg.CatalogueItemTitle = VideoTitle;
                //wucPaymentdDlg.UrlParameterName = SiteConstants.Urls.ParameterNames.PayDialogue;
                //wucPaymentdDlg.TopUpUrl = GetUrlWithRedirect(_topUpUrl, true);
                //wucPaymentdDlg.PaymentSuccessfulRedirectUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.DisplayVideoImage);
                //// TODO: payment dialog item cost must be of type CurrencyAmount
                //wucPaymentdDlg.CatalogueItemCost = VideoItem.RentalAmount.Value;

                // Check if payment required.
                if (_isRented)
                {
                    //wucPaymentdDlg.Visible = false;

                    divBoxOfficeNotRented.Visible = false;
                    // Set currently rented actions (play/download and expiry ticker).
                    if (_rentalExpiryDate.HasValue)
                    {
                        divExpiryTicker.Visible = true;
                    }
                    else
                    {
                        divExpiryTicker.Visible = false;
                    }

                    hlPlayRentedVideo.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PlayVideo);

                    SetDownloadVideo();

                    divBoxOfficeRented.Visible = true;
                }
                else
                {
                    //wucPaymentdDlg.ShowDialogOnLoad = _mediaDisplayAction == SiteEnums.MediaDisplayActionType.PayForVideo;

                    divBoxOfficeRented.Visible = false;
                    // Set rent actions (rent now).
                    // TODO: RentalAmount.CurrentId to return valid currency symbol. Ideally RentalAmount has ToCurrencyString() that returns currency symbol and value i.e. R 29.99, $ 9.99 etc.
                    litCost.Text = string.Format("{0} {1}", "R", VideoItem.RentalAmount.Value);
                    litRentalPeriod.Text = VideoItem.RentalPeriodInHours.ToString();

                    if (IsForthcomingRelease)
                    {
                        divBoxOfficeAvailableToRent.Visible = false;
                        divBoxOfficeNotAvailableToRent.Visible = true;
                        litAvailableOnBoxOffice.Text = VideoStartDate;
                    }
                    else
                    {
                        divBoxOfficeNotAvailableToRent.Visible = false;
                        divBoxOfficeAvailableToRent.Visible = true;
                    }

                    divBoxOfficeNotRented.Visible = true;
                }
            }
            else
            {
                // Currently do not display Show for BoxOffice.

                //wucPaymentdDlg.Visible = false;
                divBoxOfficeRented.Visible = false;
                divBoxOfficeNotRented.Visible = false;
                divDownloadVideo.Visible = false;
            }
        }
        #endregion


        #region private void SetVideoWatchConsole ()
        /// <summary>
        /// 
        /// </summary>
        private void SetVideoWatchConsole()
        {
            if (VideoProductType == ProductTypes.CatchUp)
            {
                divCatchUpNotAvailable.Visible = false;
                divCatchUpNotLoggedIn.Visible = false;
                divCatchUpLoggedInAndValid.Visible = false;
                divCatchUpLoggedInAndNotValid.Visible = false;
                divCatchUpShowPlay.Visible = false;
                divDownloadVideo.Visible = false;
                divChannel.Visible = false;

                SetChannel();

                if (_consoleDisplayType == SiteEnums.VideoConsoleDisplay.Show)
                {
                    litEpisodeTitleForShow.Text = string.Format(litEpisodeTitleForShow.Text, VideoEpisodeTitle);
                    hlPlayShowVideo.NavigateUrl = VideoDetailPageUrl;
                    divCatchUpShowPlay.Visible = true;
                }
                else
                {
                    if (DstvUser.IsAuthenticated)
                    {
                        if (WebsiteSettings.DisablePremiumSubsCheck || DstvUser.IsPremiumDstvSubscriber)
                        {
                            //divCatchUpLoggedInAndValid.Visible = true; //RSS: DCCU-046
                            hlPlayCatchUpVideo.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PlayVideo);
                            SetDownloadVideo();
                        }
                        else
                        {
                            litNotSubscriber.Visible = true;
                            divCatchUpLoggedInAndNotValid.Visible = true;
                        }
                    }
                    else
                    {
                        divCatchUpNotLoggedIn.Visible = true;

                        if (IsEpisode)
                        {
                            litEpisodeTitle2.Text = string.Format(litEpisodeTitle2.Text, VideoEpisodeTitle);
                        }
                        else
                        {
                            litEpisodeTitle2.Visible = false;
                        }

                        if (IsForthcomingRelease)
                        {
                            divCatchUpNotAvailable.Visible = true;
                            divAvailableToWatch.Visible = false;
                        }
                        else
                        {
                            divCatchUpNotAvailable.Visible = false;
                            divAvailableToWatch.Visible = true;
                        }
                    }
                }
            }
            else
            {
                divCatchUpNotAvailable.Visible = false;
                divCatchUpLoggedInAndValid.Visible = false;
                divCatchUpLoggedInAndNotValid.Visible = false;
                divCatchUpNotLoggedIn.Visible = false;
                divCatchUpShowPlay.Visible = false;
            }
        }
        #endregion


        #region private void SetChannel ()
        /// <summary>
        /// 
        /// </summary>
        private void SetChannel()
        {
            ProgramChannel channel = _programDetail.GetFirstValidChannel();
            if (channel != null)
            {
                imgChannel.Visible = false;
                hlChannel.Visible = false;

                if (true) //TODO: Define the logic which is currently hardcoded as true! Don't have redirect url from MMS. // (string.IsNullOrEmpty(channel.RedirectUrl)) 
                {
                    imgChannel.ImageUrl = string.Format("{0}{1}", WebsiteSettings.AppSettings.ChannelImagePath, channel.ImageFilename);
                    imgChannel.ToolTip = channel.Name;
                    imgChannel.Visible = true;
                }
                else
                {
                    imgChannelLink.ImageUrl = channel.ImageFilename;
                    imgChannelLink.ToolTip = channel.Name;
                    // hlChannel.NavigateUrl = channel.RedirectUrl;
                    hlChannel.Visible = true;
                }

                //litChannelName.Text = channel.Name;
                //Puneet.Visible = false;
                divChannel.Visible = true;
            }
        }
        #endregion


        #region private void SetDownloadVideo ()
        /// <summary>
        /// 
        /// </summary>
        private void SetDownloadVideo()
        {
            DataContract.MediaTypeEnum[] downloadableTypes = { DataContract.MediaTypeEnum.Download, DataContract.MediaTypeEnum.Download700 };
            List<DataContract.ICatalogueItemMedia> downloadMediaItems = _programDetail.PrimaryVideoItem.GetMediaFiles(downloadableTypes);
            if (downloadMediaItems.Count == 0) 
            {
                divDownloadVideo.Visible = false;
            }
            else
            {
                divDownloadVideo.Visible = true;
                litDownloadSize.Text = ((long)downloadMediaItems[0].FileSizeInBytes / (1024 * 1024)).ToString();                
            }
        }
        #endregion


        #region private void SetTrailerConsole ()
        /// <summary>
        /// 
        /// </summary>
        private void SetTrailerConsole()
        {
            if (_consoleDisplayType == SiteEnums.VideoConsoleDisplay.Show || (_trailerDetail == null && _epkDetail == null))
            {
                divFreeMedia.Visible = false;

                if (VideoItem != null)
                {
                    hlPlayVideo.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PlayVideo);
                }
                else
                {
                    trWatchEPK.Visible = false;
                    trWatchTrailer.Visible = false;
                }
            }
            else
            {
                divFreeMedia.Visible = true;
                if (_trailerDetail != null)
                {
                    trWatchTrailer.Visible = true;
                    hlPlayTrailer.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PlayTrailer);
                }
                else
                {
                    trWatchTrailer.Visible = false;
                }

                if (_epkDetail != null)
                {
                    trWatchEPK.Visible = true;
                    hlPlayEPK.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PlayEPK);
                }
                else
                {
                    trWatchEPK.Visible = false;
                }

                if (VideoItem != null)
                {
                    hlPlayVideo.NavigateUrl = GetActionUrl(SiteEnums.MediaDisplayActionType.PlayVideo);
                }
                else
                {
                    trWatchEPK.Visible = false;
                    trWatchTrailer.Visible = false;
                }
            }
        }
        #endregion


        #region private void SetMediaAction ()
        /// <summary>
        /// 
        /// </summary>
        private void SetMediaAction()
        {
            imgBillboard.Visible = false;
            wucFlashPlayer.Visible = false;
            litTrailerPlaying.Visible = false;
            imgDiv.Visible = false;


            if (IsForthcomingRelease) //Extra check to prevent user hacking the querystring to download or play forthcoming video
            {
                _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
            }

            if (_consoleDisplayType == SiteEnums.VideoConsoleDisplay.Show)
            {
                SetMediaImage(VideoItem.ProgramBillboardUri);
            }
            else
            {
                if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.Unknown ||
                    _mediaDisplayAction == SiteEnums.MediaDisplayActionType.DisplayTrailerImage ||
                    _mediaDisplayAction == SiteEnums.MediaDisplayActionType.DisplayEPKImage)
                {
                    if (_trailerDetail != null && _trailerDetail.CatalogueItem != null && _mediaDisplayAction == SiteEnums.MediaDisplayActionType.DisplayTrailerImage)
                    {
                        SetMediaImage(_trailerDetail.CatalogueItem.MediaImageUri);
                    }
                    else if (_epkDetail != null && _epkDetail.CatalogueItem != null && _mediaDisplayAction == SiteEnums.MediaDisplayActionType.DisplayEPKImage)
                    {
                        SetMediaImage(_epkDetail.CatalogueItem.MediaImageUri);
                    }
                    else
                    {
                        _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
                    }
                }

                if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.DisplayVideoImage)
                {
                    SetMediaImage(VideoItem.MediaImageUri);
                }
                else if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.PlayTrailer)
                {
                    if (_trailerDetail != null && _trailerDetail.CatalogueItem != null)
                    {
                        // TODO: Set properties on FlashPlayer control (such as video id) and load.
                        string width;
                        string height;
                        SetWidthAndHeightFromDimensions(_trailerDetail.CatalogueItem.CatalogueItemId.Value, out width, out height);
                        wucFlashPlayer.LoadVideo(_trailerDetail.CatalogueItem.CatalogueItemId.Value, "MediaDisplayActionType.PlayTrailer", ClassLibrary.FlashVideo.Util.ConfigValues.PlayerFeedURL, width, height);
                        litTrailerPlaying.Visible = true;
                    }
                    else
                    {
                        _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
                        SetMediaAction();
                        return;
                    }
                }
                else if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.PlayEPK)
                {
                    if (_epkDetail != null && _epkDetail.CatalogueItem != null)
                    {
                        // TODO: Set properties on FlashPlayer control (such as video id) and load.

                        //string dimension = _epkDetail.CatalogueItem.Dimentions;
                        string width;
                        string height;
                        SetWidthAndHeightFromDimensions(_epkDetail.CatalogueItem.CatalogueItemId.Value, out width, out height);
                        wucFlashPlayer.LoadVideo(_epkDetail.CatalogueItem.CatalogueItemId.Value, "MediaDisplayActionType.PlayEPK", ClassLibrary.FlashVideo.Util.ConfigValues.PlayerFeedURL, width, height);
                        litEPKPlaying.Visible = true;
                    }
                    else
                    {
                        _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
                        SetMediaAction();
                        return;
                    }
                }
                else if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.PlayVideo || _mediaDisplayAction == SiteEnums.MediaDisplayActionType.DownloadVideo)
                {
                    bool canWath = UserCanViewVideo();

                    if (VideoItem != null && canWath)
                    {
                        //string dimension = _epkDetail.CatalogueItem.Dimentions;
                        string width;
                        string height;
                        SetWidthAndHeightFromDimensions(VideoItem.CatalogueItemId.Value, out width, out height);
                        wucFlashPlayer.LoadVideo(VideoItem.CatalogueItemId.Value, "MediaDisplayActionType.PlayVideo", ClassLibrary.FlashVideo.Util.ConfigValues.PlayerFeedURL, width, height);

                        if (VideoProductType == ProductTypes.BoxOffice)
                        {
                            // Check that it's rented? Double check?
                            if (_rentalExpiryDate.HasValue && _rentalExpiryDate.Value > DateTime.Now)
                            {
                                wucErrorDialogue.ShowErrorDialogue(
                                    "Video Rental Expired",
                                    string.Format("Video: {0}", VideoTitle),
                                    "Your rental period has expired."
                                    );

                                _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
                                SetMediaAction();
                                return;
                            }
                        }

                        if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.DownloadVideo)
                        {
                            string downloadLink = System.Configuration.ConfigurationManager.AppSettings["DownloadPlayerLink"];
                            string downloadParams = System.Configuration.ConfigurationManager.AppSettings["DownloadVideoParams"];

                            downloadParams = downloadParams.Replace(SiteConstants.FormatIdentifiers.VideoId, Server.UrlEncode(EncryptionHelper.Encrypt(this.VideoId)));

                            downloadLink += downloadParams;
                            runjQueryCode(string.Format("jQuery.lightbox('{0}', {{modal: true, iframe: true, width: 540, height: 150}});", downloadLink));
                        }

                    }
                    else
                    {
                        _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
                        SetMediaAction();
                        return;
                    }
                }
                else if (_mediaDisplayAction == SiteEnums.MediaDisplayActionType.PayForVideo)
                {
                    if (!DstvUser.IsAuthenticated)//Show Message
                    {
                        VideoDisplayMessage = SiteConstants.Display.Message.GetLoginHTML();
                        runjQueryCode("jQuery.lightbox('#LightboxDivConsole', {modal: true});");
                    }
                    else
                    {
                        string customerId = SiteData.DstvUser.UserProfile.CustomerId.Value;
                        Rental rental = new Rental();
                        RentalResult rentalResult;

                        rentalResult = rental.GetVideoRentalItemByUserIdAndItemId(customerId, VideoItem.CatalogueItemId.Value);

                        if (rentalResult.TotalResults > 0 && rentalResult.Rentals.Count > 0 && !rentalResult.Rentals[0].HasRentalExpired)
                        {
                            _mediaDisplayAction = SiteEnums.MediaDisplayActionType.PlayVideo;
                            SetMediaAction();
                            return;
                        }
                        else
                        {
                            string height = "380";

                            if (!DstvUser.HasWallet || DstvUser.WalletBalance.Value < VideoItem.RentalAmount.Value)
                            {
                                height = "230";
                            }
                            runjQueryCode(string.Format("jQuery.lightbox('../Payment.aspx?{0}={1}',{{iframe: true, width: 605, height: {2}}});", SiteConstants.Urls.ParameterNames.VideoId, SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, null), height));
                        }
                    }

                    _mediaDisplayAction = SiteEnums.MediaDisplayActionType.DisplayVideoImage;
                    SetMediaAction();
                    return;
                }
            }
        }

        /// <summary>
        /// Sets the width and height from dimensions.
        /// </summary>
        /// <param name="videoID">The video ID.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        private void SetWidthAndHeightFromDimensions(string vid, out string width, out string height)
        {
            DataContract.CatalogueItemIdentifier videoID = new DataContract.CatalogueItemIdentifier(vid);

            var videoMeta = VideoCatalogue.GetVideoMedia(videoID, MediaCatalogue.MediaTypeEnum.Flash);
            string dimensions = videoMeta.Dimensions;
            width = "536";
            height = "400";

            if (dimensions != null && dimensions.ToLower().Contains("x"))
            {
                string[] diList = dimensions.Split('x');

                if (diList.Length > 1)
                {
                    width = diList[0];
                    height = diList[1];
                }
            }

        }


        #endregion


        #region private void SetMediaImage ( string mediaImageUri )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediaImageUri"></param>
        private void SetMediaImage(string mediaImageUri)
        {
            string imageServerPath = Dstv.Online.VideoOnDemand.Integration.Extensions.GetImageServerPathSetting();
            string displayImage;

            if (string.IsNullOrWhiteSpace(imageServerPath) || System.Text.RegularExpressions.Regex.IsMatch(mediaImageUri, @"^(HTTP|\.)", System.Text.RegularExpressions.RegexOptions.IgnoreCase) || mediaImageUri.Contains(imageServerPath))
            {
                //imgBillboard.ImageUrl = mediaImageUri;
                displayImage = mediaImageUri;
            }
            else
            {
                //imgBillboard.ImageUrl = string.Format("{0}{1}", imageServerPath, mediaImageUri);
                displayImage = string.Format("{0}{1}", imageServerPath, mediaImageUri); ;
            }

            if (doesImageExistRemotely(displayImage, "image/jpeg"))
            {
                imgBillboard.ImageUrl = displayImage;
            }
            else
            {
                imgBillboard.ImageUrl = "~/app_themes/onDemand/images/OnDemand_Generic_Showpage_image.jpg";
            }

            imgBillboard.Visible = true;
            imgDiv.Visible = true;
        }
        #endregion


        #region private string GetActionUrl ( SiteEnums.MediaDisplayActionType actionType )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionType"></param>
        /// <returns></returns>
        private string GetActionUrl(SiteEnums.MediaDisplayActionType actionType)
        {
            string actionUrl = Request.Url.AbsolutePath;
            string actionQueryString = "";
            for (int i = 0; i < Request.QueryString.Count; i++)
            {
                if (Request.QueryString.Keys[i] != SiteConstants.Urls.ParameterNames.VideoDetailAction && Request.QueryString.Keys[i] != SiteConstants.Urls.ParameterNames.PayDialogue)
                {
                    actionQueryString += string.Format("{0}={1}&", Request.QueryString.Keys[i], Request.QueryString[i]);
                }
            }

            actionUrl = string.Format("{0}?{1}{2}=", actionUrl, actionQueryString, SiteConstants.Urls.ParameterNames.VideoDetailAction);

            if (actionType == SiteEnums.MediaDisplayActionType.PlayTrailer)
            {
                if (_trailerDetail != null)
                {
                    DataContract.ICatalogueItemMedia mediaItem = _trailerDetail.GetMediaFile(DataContract.MediaTypeEnum.Flash);
                    if (mediaItem != null)
                    {
                        actionUrl += string.Format("{0},{1}", SiteConstants.Urls.ParameterValues.PlayTrailer, mediaItem.ManId);
                    }
                }
            }
            else if (actionType == SiteEnums.MediaDisplayActionType.PlayEPK)
            {
                if (_epkDetail != null)
                {
                    DataContract.ICatalogueItemMedia mediaItem = _epkDetail.GetMediaFile(DataContract.MediaTypeEnum.Flash);
                    if (mediaItem != null)
                    {
                        actionUrl += string.Format("{0},{1}", SiteConstants.Urls.ParameterValues.PlayEPK, mediaItem.ManId);
                    }
                }
            }
            else if (actionType == SiteEnums.MediaDisplayActionType.PlayVideo)
            {
                DataContract.ICatalogueItemMedia mediaItem = _programDetail.PrimaryVideoItem.GetMediaFile(DataContract.MediaTypeEnum.Flash);
                if (mediaItem != null)
                {
                    actionUrl += string.Format("{0},{1}", SiteConstants.Urls.ParameterValues.PlayMovie, mediaItem.ManId);
                }
            }
            else if (actionType == SiteEnums.MediaDisplayActionType.DownloadVideo)
            {
                //DataContract.ICatalogueItemMedia mediaItem = _programDetail.PrimaryVideoItem.GetMediaFile(DataContract.MediaTypeEnum.Download);
                DataContract.MediaTypeEnum[] downloadableTypes = { DataContract.MediaTypeEnum.Download, DataContract.MediaTypeEnum.Download700 };
                List<DataContract.ICatalogueItemMedia> downloadMediaItems = _programDetail.PrimaryVideoItem.GetMediaFiles(downloadableTypes);
                if (downloadMediaItems.Count > 0)
                {
                    actionUrl += string.Format("{0},{1}", SiteConstants.Urls.ParameterValues.DownloadMovie, downloadMediaItems[0].ManId);
                    //actionUrl = "javascript:LaunchDesktopPlayer();";
                }

                #region Old Code - don't remove just yet
                /*
				// Check if user has Desktop Player.
				if (DStvUser.HasDesktopPlayer)
				{
					actionUrl = "javascript:LaunchDesktopPlayer();";
					//DataContract.ICatalogueItemMedia mediaItem = _programDetail.PrimaryVideoItem.GetMediaFile(DataContract.MediaTypeEnum.Download);
					//if (mediaItem != null)
					//{
					//   actionUrl += string.Format("{0},{1}", SiteConstants.Urls.ParameterValues.DownloadMovie, mediaItem.ManId);
					//}
				}
				else
				{
					// Set Url to Desktop Player Page.
					actionUrl = GetUrlWithRedirect(_desktopPlayerDownloadUrl, false);
				}
				*/
                #endregion
            }
            else if (actionType == SiteEnums.MediaDisplayActionType.PayForVideo)
            {
                actionUrl += SiteConstants.Urls.ParameterValues.PayForMovie;
            }
            return actionUrl;
        }
        #endregion


        #region private string GetUrlWithRedirect ( string url, bool includePayParameter )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="includePayParameter"></param>
        /// <returns></returns>
        private string GetUrlWithRedirect(string url, bool includePayParameter)
        {
            string redirectUrl = Request.Url.PathAndQuery;
            if (includePayParameter)
            {
                if (Request.QueryString[SiteConstants.Urls.ParameterNames.PayDialogue] == null)
                {
                    redirectUrl = string.Format("{0}{1}{2}=1", redirectUrl, Request.QueryString.Count > 0 ? "&" : "?", SiteConstants.Urls.ParameterNames.PayDialogue);
                }
                else if (Request.QueryString[SiteConstants.Urls.ParameterNames.PayDialogue].ToString() != "1")
                {
                    redirectUrl = redirectUrl.Replace(SiteConstants.Urls.ParameterNames.PayDialogue, string.Format("{0}=1&Z", SiteConstants.Urls.ParameterNames.PayDialogue));
                }
            }

            return string.Format("{0}?{1}", url, SiteUtils.Url.InternalRedirector.GetRedirectUrlParamForQueryString(redirectUrl));
        }
        #endregion


        protected void lbDownloadVideo_Click(object sender, EventArgs e)
        {
            try
            {
                string connectId = string.Empty;
                //Check if connectId is set for the session.            
                if (SessionManager.GetSession("ConnectId") == null || SessionManager.GetSession("ConnectId") == string.Empty)
                {
                    //set connectId fo the session
                    SessionManager.SetSession("ConnectId", DstvUser.UserDStvPrincipal.DStvConnectID);
                    connectId = SessionManager.GetSession("ConnectId");
                }
                else
                {
                    connectId = SessionManager.GetSession("ConnectId");
                }

                //Make injection service call
                InjectionService.InjectionServiceClient injectionServiceClient = new InjectionService.InjectionServiceClient();
                string catalogueItemId = string.Empty;
                if (_programDetail.PrimaryVideoItem.CatalogueItem != null)
                {
                    catalogueItemId = _programDetail.PrimaryVideoItem.CatalogueItem.CatalogueItemId.ToString();
                }
                string AuthKey = ConfigurationManager.AppSettings["DesktopPlayerAuthKey"].ToString();

                bool isMediaInjected = injectionServiceClient.InjectMediaItem(AuthKey,EncryptionHelper.Encrypt(connectId), catalogueItemId);
                if (isMediaInjected)
                {
                    //Load LightBox
                   // VideoDisplayMessage = "The media item has been added to your download queue. Don't have a player? Download your player <a href=\"" + Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath + "Download/DesktopPlayer.aspx?retunUrl=" + Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath + "closeparent.aspx?hl=en&tab=nw&lightbox[iframe]=true&lightbox[width]=740&lightbox[height]=465\" id=\"DownloadLightbox\">here</a>.";
                    VideoDisplayMessage = "Don’t have the new DStv Desktop Player installed? Please <a href=\"" + Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath + "Download/DesktopPlayer.aspx?retunUrl=" + Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath + "closeparent.aspx?hl=en&tab=nw&lightbox[iframe]=true&lightbox[width]=740&lightbox[height]=465\" id=\"DownloadLightbox\">Click here</a> to download the new DStv Desktop Player for free.<br /><br />If you already have the new DStv Desktop Player installed and you’ve clicked on the <i><b>Download</b></i> button the video has been added to your download queue and will start downloading once you open the new DStv Desktop Player.";
                    //triger lightbox with an iframe
                    runjQueryCode(string.Format("{0}{1}{2}", "jQuery.lightbox(", "\"#LightboxDivConsole\",", "{modal: true});"));
                }
                else
                {
                    //Load Error LightBox
                    VideoDisplayMessage = "The media could not be added to your queue.";
                    //triger lightbox with an iframe
                    runjQueryCode(string.Format("{0}{1}{2}", "jQuery.lightbox(", "\"#LightboxDivConsole\",", "{modal: true});"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*** START Check Image Exist BY Titi ***/
        public bool doesImageExistRemotely(string uriToImage, string mimeType)
        {
            if (!string.IsNullOrWhiteSpace(uriToImage))
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriToImage);
                    request.Method = "HEAD";

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK && response.ContentType == mimeType)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        /*** END Check Image Exist BY Titi ***/
    }
}