﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Video
{
    public partial class VideoFilterList : System.Web.UI.UserControl
    {

        private bool _userLoggedIn = false;
        private int _currentSectionIdx = 0;
        private int _currentItemIdx = 0;
        private string _classificationId = "";
        private string _parentClassificationId = "";
        private int _defaultSubMenuIndex = -1;
        private SiteEnums.VideoClassificationType _classificationType = SiteEnums.VideoClassificationType.Unknown;
        private DataAccess.VideoClassificationForSite _productClassificationList = null;
        private List<string> _accordionSubMenuIds = new List<string>();


        #region DefaultSectionIndex
        private int _defaultSectionIndex = -1;
        /// <summary>
        /// 
        /// </summary>
        public int DefaultSectionIndex
        {
            get { return _defaultSectionIndex; }
        }
        #endregion


        #region SelectedItemName
        private string _selectedItemName = "";
        /// <summary>
        /// 
        /// </summary>
        public string SelectedItemName
        {
            get { return _selectedItemName; }
        }
        #endregion

        private SiteEnums.ProductType SiteProductType
        {
            get { return SiteUtils.Request.GetProductTypeFromRequest(); }
        }

        #region ExpandedProductOnLoad
        private string _expandedProductOnLoad;
        /// <summary>
        /// 
        /// </summary>
        public string ExpandedProductOnLoad
        {
            get { return _expandedProductOnLoad; }
            set { _expandedProductOnLoad = value; }
        }
        #endregion


        #region ConfigFile
        private string _configFile = "";
        /// <summary>
        /// 
        /// </summary>
        public string ConfigFile
        {
            get { return _configFile; }
            set { _configFile = value; }
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            // Read Url parameters - catalogue criteria to set the current expanded section and selected item
            _classificationId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ClassificationId, null);
            if (string.IsNullOrEmpty(_classificationId))
            {
                _classificationId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.EditorialListId, null);
            }
            Enum.TryParse<SiteEnums.VideoClassificationType>(SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ClassificationType, ""), out _classificationType);

            _userLoggedIn = Page.User.Identity.IsAuthenticated;

            _productClassificationList = DataAccess.VideoData.GetVideoClassificationHierarchy(SiteEnums.VideoDataType.ClassificationList, _configFile);

            _parentClassificationId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.ParentClassificationId, null);

            SetDefaultSectionIndex();

            rptSection.DataSource = _productClassificationList;
            rptSection.DataBind();

        }

        /// <summary>
        /// Section Data Binding
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptSection_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataAccess.VideoClassificationForSection classificationForSection = (DataAccess.VideoClassificationForSection)e.Item.DataItem;

                _currentItemIdx = 0;

                // 1. Items at first level with videos (Catalogue items) - top part (by order)
                Repeater rptItemsOnlyTop = GetRepeaterSubControl(e.Item, "rptItemsOnlyTop");
                if (rptItemsOnlyTop != null)
                {
                    rptItemsOnlyTop.DataSource = classificationForSection.GetFirstLevelItems(true, false, false, _userLoggedIn);
                    rptItemsOnlyTop.DataBind();
                }

                // 2. Items at first level with sub items (Classification items) - middle part (by order)
                //		Restriction of SpryAccordion - sticking with what design provided.
                bool displaySubMenu = false;
                System.Web.UI.HtmlControls.HtmlGenericControl accordionSubMenu = GetGenericControl(e.Item, "AccordionSubMenu");
                if (accordionSubMenu != null)
                {
                    Repeater rptItemsWithSubItems = GetRepeaterSubControl(e.Item, "rptItemsWithSubItems");
                    if (rptItemsWithSubItems != null)
                    {
                        List<DataAccess.VideoClassificationItem> items = classificationForSection.GetFirstLevelItems(false, false, true, _userLoggedIn);
                        if (items.Count > 0)
                        {
                            // Get sub menu default.
                            bool setDefaultPanel = false;
                            if (!string.IsNullOrEmpty(_classificationId) && _defaultSubMenuIndex < 0)
                            {
                                for (int i = 0; i < items.Count; i++)
                                {
                                    if (IsItemExpanded(items[i]))
                                    {
                                        _defaultSubMenuIndex = i;
                                        setDefaultPanel = true;
                                        break;
                                    }
                                }
                            }

                            rptItemsWithSubItems.DataSource = items;
                            rptItemsWithSubItems.DataBind();

                            displaySubMenu = true;

                            string menuId = string.Format("accordionSubMenu_{0}", classificationForSection.ProductType);
                            accordionSubMenu.ID = menuId;

                            // Register Accordion Sub Menu with section (ProductType) code.
                            Page.ClientScript.RegisterStartupScript(
                                this.GetType(),
                                menuId,
                                string.Format("var {0} = new Spry.Widget.Accordion(\"{0}\", {{ useFixedPanelHeights: false, defaultPanel: {1} }});", menuId, setDefaultPanel ? _defaultSubMenuIndex : -1),
                                true
                                );
                        }
                    }

                    if (!displaySubMenu)
                    {
                        accordionSubMenu.Visible = false;
                    }
                }

                // 3. Items at first level with videos (Catalogue items) - bottom part (by order)
                Repeater rptItemsOnlyBottom = GetRepeaterSubControl(e.Item, "rptItemsOnlyBottom");
                if (rptItemsOnlyBottom != null)
                {
                    rptItemsOnlyBottom.DataSource = classificationForSection.GetFirstLevelItems(false, true, false, _userLoggedIn);
                    rptItemsOnlyBottom.DataBind();
                }

                _currentSectionIdx++;
            }
        }


        /// <summary>
        /// Sections First Level Databinding for Classification Items with Sub Items - styling dictates seperate lists/databinding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptSection_rptItemsWithSubItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataAccess.VideoClassificationItem classificationItem = (DataAccess.VideoClassificationItem)e.Item.DataItem;

                // 1. Sub items at second level with videos.
                Repeater rptSubItems = GetRepeaterSubControl(e.Item, "rptSubItems");
                

                if (rptSubItems != null)
                {
                    rptSubItems.DataSource = classificationItem.SubItems;
                    rptSubItems.DataBind();
                }
            }
        }

        /// <summary>
        /// Sections Second Level Databinding for Classification Sub Items with Videos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptSection_rptItemsWithSubItems_rptSubItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        private Repeater GetRepeaterSubControl(RepeaterItem repeaterParent, string repeaterId)
        {
            var repeaterObj = repeaterParent.FindControl(repeaterId);
            if (repeaterObj == null)
            {
                return null;
            }
            else
            {
                return (Repeater)repeaterObj;
            }
        }

        private System.Web.UI.HtmlControls.HtmlGenericControl GetGenericControl(RepeaterItem repeaterParent, string controlId)
        {
            var controlObj = repeaterParent.FindControl(controlId);
            if (controlObj == null)
            {
                return null;
            }
            else
            {
                return (System.Web.UI.HtmlControls.HtmlGenericControl)controlObj;
            }
        }

        private void SetDefaultSectionIndex()
        {
            _defaultSectionIndex = -1;

            // First check provided classification Id as it may not be in Product Classification set by ExpandedProductOnLoad
            if (!string.IsNullOrEmpty(_classificationId))
            {
                for (int i = 0; i < _productClassificationList.Count; i++)
                {
                    if (_productClassificationList[i].ContainsItemsOrSubItem(_classificationId, SiteProductType, _parentClassificationId))
                    {
                        _defaultSectionIndex = i;
                        break;
                    }
                }
            }

            if (_defaultSectionIndex < 0)
            {
                ProductTypes productTypeToExpand;
                if (Enum.TryParse<ProductTypes>(_expandedProductOnLoad, out productTypeToExpand))
                {
                    for (int i = 0; i < _productClassificationList.Count; i++)
                    {
                        if (_productClassificationList[i].ProductType == productTypeToExpand)
                        {
                            _defaultSectionIndex = i;
                            break;
                        }
                    }
                }
            }
        }

        public string GetDisplayTitle(object dataItem)
        {
            DataAccess.VideoClassificationItem videoClassificationItem = GetVideoClassificationItemFromObject(dataItem);
            if (videoClassificationItem != null)
            {
                if (videoClassificationItem.VideoCount > 0)
                {
                    return string.Format("{0} ({1})", videoClassificationItem.Name, videoClassificationItem.VideoCount);
                }
                else
                {
                    return videoClassificationItem.Name;
                }
            }
            else
            {
                return "unknown";
            }
        }

        public bool IsItemExpanded(object dataItem)
        {
            DataAccess.VideoClassificationItem videoClassificationItem = GetVideoClassificationItemFromObject(dataItem);
            if (videoClassificationItem != null)
            {
                return (IsItemExpanded(videoClassificationItem));
            }
            else
            {
                return false;
            }
        }

        public bool IsItemExpanded(DataAccess.VideoClassificationItem dataItem)
        {
            try
            {
                if (!string.IsNullOrEmpty(_classificationId) && dataItem.SubItems.Count > 0 && dataItem.ContainsSubItem(_classificationId, SiteProductType, _parentClassificationId) && _defaultSectionIndex == _currentSectionIdx)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool IsItemSelected(object itemObject)
        {
            try
            {
                DataAccess.VideoClassificationItem item = (DataAccess.VideoClassificationItem)itemObject;
                if (item != null)
                {
                    if (
                        (string.IsNullOrEmpty(_classificationId) && _defaultSectionIndex == _currentSectionIdx && item.ClassificationType == SiteEnums.VideoClassificationType.ProgramType_All) // this will select 'All' for all three product types !!
                        ||
                        (!string.IsNullOrEmpty(_classificationId) && item.Id.Equals(_classificationId, StringComparison.OrdinalIgnoreCase) && item.SiteProductType == SiteProductType && item.ParentId.Equals(_parentClassificationId, StringComparison.OrdinalIgnoreCase))
                        ||
                        (!string.IsNullOrEmpty(_classificationId) && item.Name.Equals(_classificationId, StringComparison.OrdinalIgnoreCase))
                        )
                    {
                        _selectedItemName = item.Name;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }


        private DataAccess.VideoClassificationItem GetVideoClassificationItemFromObject(object dataItem)
        {
            if (dataItem != null)
            {
                try
                {
                    return (DataAccess.VideoClassificationItem)dataItem;
                }
                catch
                {
                }
            }

            return null;
        }

        public string HeaderFilter { get; set; }
    }
}