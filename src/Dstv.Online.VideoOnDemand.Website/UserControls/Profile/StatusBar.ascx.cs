﻿
#region References

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.MasterPages;
using Dstv.Online.VideoOnDemand.Security;
using System.Web.Security;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Profile
{
    public partial class StatusBar : UserControls.Shared.JsRenderingControl
    {
		 protected override void OnInit ( EventArgs e )
		 {
			 base.OnInit(e);

			 // Check source/user territory (Cache in session??)
			 if (!WebsiteSettings.SupportedTerritories.IsUserInSupportedTerritory(Request.Url) && !WebsiteSettings.SupportedBrowsers.IsCrawler)
			 {
				 if (Request.RawUrl.IndexOf(SiteUtils.GetAbsoluteUrlPath(SiteConstants.Urls.Constants.SupportedCountriesPage), StringComparison.OrdinalIgnoreCase) < 0)
				 {
					 Response.Redirect(SiteConstants.Urls.Constants.SupportedCountriesPage, true);
				 }
			 }
		 }

		  //protected void Page_Init(object sender, EventArgs e) 
		  //{
		  //    // Check source/user territory (Cache in session??)
		  //    if (!WebsiteSettings.SupportedTerritories.IsUserInSupportedTerritory(Request.Url))
		  //    {
		  //        if (Request.RawUrl.IndexOf(SiteUtils.GetAbsoluteUrlPath(SiteConstants.Urls.Constants.SupportedBrowsersPage), StringComparison.OrdinalIgnoreCase) < 0)
		  //        {
		  //            Response.Redirect(SiteConstants.Urls.Constants.SupportedBrowsersPage, true);
		  //        }
		  //    }
		  //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DstvUser.IsAuthenticated)
            {
                tblLoggedIn.Visible = true;
                tblNotLoggedIn.Visible = false;
                lblUserName.Text = Page.User.Identity.Name;

					 if (DstvUser.HasWallet)
                {
                    hlSetUpPrePaid.Visible = false;
                    lnkTopUp.Visible = true;
                    lnkTopUp.NavigateUrl = string.Format("{0}?{1}", lnkTopUp.NavigateUrl, SiteUtils.Url.InternalRedirector.GetRedirectUrlParamForQueryStringForCurrentRequest());
                    // TODO: CurrencyAmount to provide currency formatted string.
                    lblPrePaidBalance.Text = string.Format("{0:c}", DstvUser.WalletBalance.Value);
                }
                else
                {
                    hlSetUpPrePaid.Visible = true;
                    lnkTopUp.Visible = false;
                    lblPrePaidBalance.Text = "NA";
                }

					 connectLogin.Visible = false;
            }
            else
            {
                // Enable / Disable modal dialog
                // mpLogin.Enabled = (Request.IsSecureConnection ? true : false);

                if (Request.QueryString["Login"] != null)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "showLoginDialogue", "$(window).load(ShowLoginDialogue());", true);

                //mpLogin.Show();

                tblLoggedIn.Visible = false;
                tblNotLoggedIn.Visible = true;
            }
        }


        /// <summary>
        /// Login to the system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkLogin_Click(object sender, EventArgs e)
        {
            // TODO: user controls should not be referencing master page!!
            SiteUtils.Url.RedirectAndShowLogin();
        }


        /// <summary>
        /// Topup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkTopUp_Click(object sender, EventArgs e)
        {
            // TODO: user controls should not be referencing master page!!
            SiteUtils.Url.RedirectToTopUp();
        }

		  protected void lnkLogout_Click ( object sender, EventArgs e )
		  {
			  FormsAuthentication.SignOut();
			  // Parameters to remove to prevent pop up of login when logging out.
			  string[] urlParams = new string[] { SiteConstants.Urls.ParameterNames.PayDialogue, SiteConstants.Urls.ParameterNames.VideoDetailAction };
			  
			  Response.Redirect(SiteUtils.Request.RemoveParametersFromUri(Request.Url, urlParams).OriginalString);
		  }

    }
}