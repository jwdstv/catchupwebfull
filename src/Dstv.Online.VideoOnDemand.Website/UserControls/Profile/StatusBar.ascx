﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatusBar.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Profile.StatusBar" %>
<%@ Register src="~/Controls/Login.ascx" TagName="Connect"  TagPrefix="DStv" %>
<script type="text/javascript">

    function ShowLoginDialogue() {
        //$find('mpLogin').show();
        $('#connectLogin').modal();
        // return false;
    }
</script>
<table cellpadding="1" cellspacing="0" border="0" id="tblNotLoggedIn" runat="server">
    <tr>
        <td class="logregitem">
            <a class="smlLink" ID="lnkLogin" runat="server" onclick="ShowLoginDialogue();">Login</a>
        </td>
        <td class="blueSep">
            |
        </td>
        <td class="logregitem">
            <a href="/Profile/RecoverPassword.aspx" class="smlLink" title="Click to recover forgotten password">
                Forgot Password?</a>
        </td>
        <td class="blueSep">
            |
        </td>
        <td class="logregitem">
            <a href="/SignUp.aspx" class="smlLink" title="Click to register/sign up with DStv Online">
                Register</a>
        </td>
    </tr>
</table>
<table cellpadding="1" cellspacing="0" border="0" id="tblLoggedIn" runat="server">
    <tr>
        <td class="logregitem">
            You are logged in as
            <asp:Label ID="lblUserName" runat="server" CssClass="userName"></asp:Label>
        </td>
        <td class="blueSep">
            |
        </td>
        <td class="logregitem">
            <a href="/Account/Account.aspx" class="smlLink" title="Click to view and update your DStv Online profile">
                My Profile</a>
        </td>
        <td class="blueSep">
            |
        </td>
        <td class="logregitem">
            <asp:LinkButton ID="lnkLogout" class="smlLink" runat="server" onclick="lnkLogout_Click">Logout</asp:LinkButton>
        </td>
        <td class="blueSep">
            |
        </td>
        <td class="logregitem">
            Pre-paid Balance:
            <asp:Label ID="lblPrePaidBalance" runat="server"></asp:Label>
        </td>
        <td style="padding-left: 3px;">
            (<asp:HyperLink ID="lnkTopUp" runat="server" NavigateUrl="/Profile/TopUp.aspx" Text="Top Up" ToolTip="Click here to top up your pre-paid balance" /><asp:HyperLink ID="hlSetUpPrePaid" runat="server" NavigateUrl="javascript:SetUpPrePaid();" ToolTip="Click here to set up a pre-paid facility on your profile">Set Up</asp:HyperLink>)
        </td>
    </tr>
</table>
<div id="connectLogin" style="display:none" >
<!-- LOGIN BEGIN -->
    <DStv:Connect ID="connectLogin" runat="server" />
</div>
<!-- DSTV CONNECT END -->
