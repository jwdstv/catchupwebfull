﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using Dstv.Online.VideoOnDemand.Integration;
using Dstv.Online.VideoOnDemand.MemCached;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Advert
{
    public partial class LargeBanner : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string result = GetAdData();
            string adText = string.Empty;

            if (!string.IsNullOrEmpty(result))
            {
                adText = result;
            }

            lblAd.Text = adText;

        }

        /// <summary>
        /// Uses the current URL to get the ad tag data from SQL
        /// </summary>
        /// <returns>string representation of the ad tag</returns>
        private string GetAdData()
        {
            string url = HttpContext.Current.Request.Url.AbsolutePath;// ServerVariables["SCRIPT_NAME"].Trim().ToString();
            string cacheKey = string.Format("{0}_LargeBanner_GetAdData_{1}_{2}", CacheConstants.CachePrefix, AdvertSize ?? string.Empty, url).CacheRemoveInvalidChars();

            string adTag = string.Empty;

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                adTag = MemCacheManager.Get<string>(cacheKey);
            }
            else
            {
                List<Dstv.Online.VideoOnDemand.Data.AdvertDTO> advertItems = Dstv.Online.VideoOnDemand.Data.AdvertDb.GetAdTagByPageNameAndSize(url, AdvertSize);

                if (advertItems != null)
                {
                    if (advertItems.Count > 0)
                    {



                        adTag = Server.HtmlDecode(advertItems[0].Tag);
                        if (string.IsNullOrEmpty(adTag.Trim()))
                        {
                            string image = advertItems[0].DefaultImageURL;
                            string adUrl = advertItems[0].LinkURL;
                            if (!string.IsNullOrEmpty(image) && !string.IsNullOrEmpty(adUrl))
                            {
                                adTag = "<a href=\"" + adUrl + "\" target=\"_blank\"><img src=\"" + image + "\" border=\"0\" /></a>";
                            }
                        }
                    }

                    //If not tag is supplied
                    //Use Default Image and Link URL
                }

                adTag = System.Text.RegularExpressions.Regex.Replace(adTag, @"^\s*\<br\s*\/?\>", "");

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, adTag);
                }
            }

            return adTag;
        }

        #region Public Members

        public string AdvertSize
        { get; set; }        

        #endregion
    }
}