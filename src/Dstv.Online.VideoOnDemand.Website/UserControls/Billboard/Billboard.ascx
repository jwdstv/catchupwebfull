﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Billboard.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Billboard.billboard" %>
<%--this controld displys a billboard Item based on a product ID
Desmond Nzuza
29 - Sep - 2010
--%>

<div class="dstvflash">
<div id="altContent">
		<div style="color:#000000; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:9px; font-weight:bold; width:112px; margin:0px auto; background-color:#FFFFFF; padding:4px">Please Upgrade<br/>Your Version of Flash
		<div><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" style="padding-top:4px" border="0" width="112" height="33" /></a></div>
        </div>
	</div>
    <script type="text/javascript">
        //alert("<%= Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>feeds/billboard.aspx?productid=<%= GetProductId() %>");
        swfobject.embedSWF("http://core.dstv.com/billboard/flash/DStvBillboard.swf", "altContent", "660", "327", "9", "http://core.dstv.com/flash/expressInstall.swf",
        { rootStr: "<%= Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>feeds/billboard.aspx?productid=<%= GetProductId() %>", color: "<%= GetFontColor() %>", colorB : "525252" },        
		{ menu: "false", allowScriptAccess: "always", wmode: "transparent" });
	</script> 
</div>
