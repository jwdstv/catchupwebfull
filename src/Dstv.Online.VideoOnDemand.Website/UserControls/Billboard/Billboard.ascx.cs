﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Billboard
{
    public partial class billboard : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            
            

        }

        public string GetProductId()
        {  
            //Get productID From Web.config
            //get current product
            ClassLibrary.SiteEnums.ProductType product = ClassLibrary.SiteUtils.Request.GetProductTypeFromRequest();
            string productName = product.ToString();

           

            try
            {
                ProductId = Convert.ToInt32(ConfigurationManager.AppSettings["BillBoard" + product.ToString()]);
            }
            catch(Exception ex) 
            {
                //LogError
                ClassLibrary.ErrorLogManager.LogError(string.Format("Unable to get ProductID from Web.config. Error: {0}", ex));
            }
            bool isBoxOffice = false;

            if (product == ClassLibrary.SiteEnums.ProductType.BoxOffice)
                isBoxOffice = true;

            string prod = ProductId.ToString() + "," + isBoxOffice.ToString();

            return prod;

            //return ProductId.ToString();
        }

        public string CheckIfBoxOffice()
        {
            bool isBoxOffice = false;

            //Check Current Product Type
            ClassLibrary.SiteEnums.ProductType product = ClassLibrary.SiteUtils.Request.GetProductTypeFromRequest();
            if (product == ClassLibrary.SiteEnums.ProductType.BoxOffice)
                isBoxOffice = true;


            return isBoxOffice.ToString();
        }


        public string GetFontColor()
        {
            if (String.IsNullOrEmpty(FontColor))
                FontColor = "ff0000";            
            return FontColor;
        }

        #region Public Members

        public int ProductId
        { get; set; }
        //ID if the Show Custom Categories
        public string FontColor
        { get; set; }

        #endregion
    }
}