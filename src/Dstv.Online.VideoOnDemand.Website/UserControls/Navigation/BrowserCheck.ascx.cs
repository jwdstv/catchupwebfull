﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
	public partial class BrowserCheck : System.Web.UI.UserControl
	{
		private WebsiteSettings.SupportedBrowsers _supportedBrowsers;
		private bool _isSupportedBrowsersPage = false;

		private string _supportedBrowsersPageUrl = "/Other/SupportedBrowsers.aspx";

		public string SupportedBrowsersPageUrlWithRedirect
		{
			get { return string.Format("{0}?{1}", _supportedBrowsersPageUrl, SiteUtils.Url.InternalRedirector.GetRedirectUrlParamForQueryStringForCurrentRequest()); }
		}

		protected void Page_Init ( object sender, EventArgs e )
		{
			_isSupportedBrowsersPage = Request.RawUrl.IndexOf(_supportedBrowsersPageUrl, StringComparison.OrdinalIgnoreCase) >= 0;

			_supportedBrowsers = new WebsiteSettings.SupportedBrowsers();
			if (
				(_supportedBrowsers.SupportedState == SiteEnums.SupportedBrowserState.Unknown || _supportedBrowsers.SupportedState == SiteEnums.SupportedBrowserState.UnsupportedBrowser)
				&&
				!_isSupportedBrowsersPage
				)
			{
				Response.Redirect(string.Format("{0}?{1}", _supportedBrowsersPageUrl, SiteUtils.Url.InternalRedirector.GetRedirectUrlParamForQueryStringForCurrentRequest()), true);
			}
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
			this.Visible = !_isSupportedBrowsersPage && _supportedBrowsers.SupportedState == SiteEnums.SupportedBrowserState.UnsupportedBrowserAndUserAccepted;
		}
	}
}