﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedChannels.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.FeaturedChannels" %>

<script type="text/javascript">

    function MM_preloadImages() { //v3.0
        var d = document; if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; } 
        }
    }

    function MM_swapImgRestore() { //v3.0
        var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
    }

    function MM_findObj(n, d) { //v4.01
        var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n); return x;
    }

    function MM_swapImage() { //v3.0
        var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2); i += 3)
            if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
    }


</script>

<table>
    <tr>
        <td>
            <h5>
                Featured Channels</h5>
            <div class="greyHdrLine" />
        </td>
    </tr>
    <tr>
        <td>
            <table width="300" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <div class="featChnls">
                            <asp:Repeater ID="repChannels" runat="server">
                                <ItemTemplate>
                                    <div class="chnl_icons">
                                        <a href="<%# ((Dstv.Online.VideoOnDemand.Website.ClassLibrary.Channel)Container.DataItem).RedirectUrl %>">
                                            <img id="imgChannel<%# ((Dstv.Online.VideoOnDemand.Website.ClassLibrary.Channel)Container.DataItem).MMSId %>" src="<%# ((Dstv.Online.VideoOnDemand.Website.ClassLibrary.Channel)Container.DataItem).DisplayImage %>"
                                                width="100" height="55" alt="<%# ((Dstv.Online.VideoOnDemand.Website.ClassLibrary.Channel)Container.DataItem).Name %>" 
                                                onmouseover="MM_swapImage('imgChannel<%# ((Dstv.Online.VideoOnDemand.Website.ClassLibrary.Channel)Container.DataItem).MMSId %>','','<%# ((Dstv.Online.VideoOnDemand.Website.ClassLibrary.Channel)Container.DataItem).HoverImage %>',1)" onmouseout="MM_swapImgRestore()" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Hyperlink ID="lnkMore" Text="...more" runat="server" />
        </td>
    </tr>
</table>
