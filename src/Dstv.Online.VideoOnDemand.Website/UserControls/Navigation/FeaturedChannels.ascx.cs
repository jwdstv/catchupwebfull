﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using System.Text;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
    public partial class FeaturedChannels : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.RegisterStartupScript("PreLoadImages", "<script>" + GetPreloadCommand() + "</script>");

            // More button
            this.lnkMore.NavigateUrl = this.MoreUrl;

            // Load the featured channels
            repChannels.DataSource = WebSettings.GetSettings.FeaturedProgramChannels; 
            repChannels.DataBind();
        }

        public string GetPreloadCommand()
        {
            StringBuilder sb = new StringBuilder();

            if (WebSettings.GetSettings.FeaturedProgramChannels.Count > 0)
            {
                sb.Append("MM_preloadImages(");
                sb.Append("'" + WebSettings.GetSettings.FeaturedProgramChannels[0].HoverImage + "'");

                for (int i = 1; i < WebSettings.GetSettings.FeaturedProgramChannels.Count; i++)
                {
                    sb.Append(",");
                    sb.Append("'" + WebSettings.GetSettings.FeaturedProgramChannels[i].HoverImage + "'");
                }

                sb.Append(");");
            }

            return sb.ToString();
        }

        public string MoreUrl
        {
            get;
            set;
        }
    }
}