﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrowserCheck.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.BrowserCheck" %>
<div class="footer_content">
<p>
* please note that the web browser you are using is not supported by the DStv On Demand website. Click <a href="<%=SupportedBrowsersPageUrlWithRedirect%>">here</a> for more information.
</p>
</div>