﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
	public class PagingControlEventArgs : EventArgs
	{
		public int SelectedPageNumber;

		public PagingControlEventArgs ( int selectedPageNumber )
		{
			SelectedPageNumber = selectedPageNumber;
		}
	}

	public partial class PagingControl : System.Web.UI.UserControl
	{
		private const string CssClass_SelectedPageLink = "page_up";
		private const string CssClass_NotSelectedPageLink = "page_down";

		private LinkButton[] _pageLinks;

		/// <summary>
		/// Total number of items.
		/// </summary>
		private int _numberOfItems = 0;

		public int NumberOfItems
		{
			get { return _numberOfItems; }
			set { _numberOfItems = value; }
		}

		/// <summary>
		/// Number of items per page
		/// </summary>
		private int _numberOfItemsPerPage = 5;

		public int NumberOfItemsPerPage
		{
			get { return _numberOfItemsPerPage; }
			set { _numberOfItemsPerPage = value; }
		}

		/// <summary>
		/// The currently selected page number
		/// </summary>
		// private int _selectedPageNumber = 1;

		public int SelectedPageNumber
		{
			get { return ViewState["_selectedPageNumber"] == null ? 1 : Convert.ToInt32(ViewState["_selectedPageNumber"]); }
			set { ViewState["_selectedPageNumber"] = value; }
		}

		/// <summary>
		/// The number of pages in this collection
		/// </summary>
		private int _numberOfPages = 0;

		public int NumberOfPages
		{
			get { return _numberOfPages; }
		}

		public int NumberOfLinksToDisplay
		{
			get { return _pageLinks == null ? 0 : _pageLinks.Length; }
		}

		/// <summary>
		/// Selected Page Number changed event.
		/// </summary>
		public event EventHandler<PagingControlEventArgs> SelectedPageNumberChanged;


		protected void Page_init ( object sender, EventArgs e )
		{
			_pageLinks = new LinkButton[] { hlPageIdx1, hlPageIdx2, hlPageIdx3, hlPageIdx4, hlPageIdx5 };
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack && SelectedPageNumberChanged == null)
			{
				PopulateControl(1);
			}
		}

		protected void hlPage_Command ( object sender, CommandEventArgs e )
		{
			int newPageNumber = 1;

			if (e.CommandName == "ScrollLeft")
			{
				newPageNumber = GetScrollLeftIndexForCurrentPage() + 1 - NumberOfLinksToDisplay;
			}
			else if (e.CommandName == "ScrollRight")
			{
				newPageNumber = GetScrollLeftIndexForCurrentPage() + 1 + NumberOfLinksToDisplay;
			}
			else if (e.CommandName == "PageSelect")
			{
				newPageNumber = Convert.ToInt32(e.CommandArgument);
			}

			if (SelectedPageNumberChanged != null)
			{
				SelectedPageNumberChanged(this, new PagingControlEventArgs(newPageNumber));
			}
			else
			{
				PopulateControl(newPageNumber);
			}
		}

		public void PopulateControl ( int newPageNumber )
		{
			SelectedPageNumber = newPageNumber;

			_numberOfPages = Math.Max((int)Math.Ceiling((float)_numberOfItems / (float)_numberOfItemsPerPage), 1); // There should be atleast one page

			int leftIndex = GetScrollLeftIndexForCurrentPage();

			bool displayLeftScroll = false;
			if (leftIndex > 0 && _numberOfItems > _numberOfItemsPerPage)
			{
				displayLeftScroll = true;
			}

			bool displayRightScroll = false;
			if (_numberOfItems > _numberOfItemsPerPage && (_numberOfPages > (leftIndex + 1 + _pageLinks.Length)))
			{
				displayRightScroll = true;
			}

			litCurrentPage.Text = SelectedPageNumber.ToString();
			litPageCount.Text = _numberOfPages.ToString();

			hlPageScrollLeft.Visible = displayLeftScroll;
			litLeftDots.Visible = displayLeftScroll;

			int pageNumber;

			for (int i = leftIndex; i < (leftIndex + NumberOfLinksToDisplay); i++)
			{
				pageNumber = i + 1;
				if ((i* _numberOfItemsPerPage) < _numberOfItems)
				{
					_pageLinks[i - leftIndex].Visible = true;
					_pageLinks[i - leftIndex].CssClass = pageNumber == SelectedPageNumber ? CssClass_SelectedPageLink : CssClass_NotSelectedPageLink;
					_pageLinks[i - leftIndex].Text = pageNumber.ToString();
					_pageLinks[i - leftIndex].ToolTip = pageNumber == SelectedPageNumber ? "Selected Page" : string.Format("Click to view page {0}", pageNumber);
					_pageLinks[i - leftIndex].CommandArgument = pageNumber.ToString();
				}
				else
				{
					_pageLinks[i - leftIndex].Visible = false;
				}
			}

			hlPageScrollRight.Visible = displayRightScroll;
			litRightDots.Visible = displayRightScroll;
		}

		private int GetScrollLeftIndexForCurrentPage ()
		{
			return ((SelectedPageNumber - 1) / NumberOfLinksToDisplay) * NumberOfLinksToDisplay;
		}
	}
}