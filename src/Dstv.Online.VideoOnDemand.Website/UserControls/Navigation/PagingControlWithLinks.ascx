﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingControlWithLinks.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.PagingControlWithLinks" %>
<div class="paging-ext"> Page <asp:Literal ID="litCurrentPage" runat="server"></asp:Literal> of <asp:Literal ID="litPageCount" runat="server"></asp:Literal>&nbsp;&nbsp; | 
	<asp:HyperLink ID="hlPageScrollLeft" runat="server" ClientIDMode="Static" CssClass="page_down">&lt;</asp:HyperLink>
	<asp:Literal ID="litLeftDots" runat="server" ClientIDMode="Static">...</asp:Literal>
	<asp:HyperLink ID="hlPageIdx1" runat="server" ClientIDMode="Static" CssClass="page_up">1</asp:HyperLink>
	<asp:HyperLink ID="hlPageIdx2" runat="server" ClientIDMode="Static" CssClass="page_down">2</asp:HyperLink>
	<asp:HyperLink ID="hlPageIdx3" runat="server" ClientIDMode="Static" CssClass="page_down">3</asp:HyperLink>
	<asp:HyperLink ID="hlPageIdx4" runat="server" ClientIDMode="Static" CssClass="page_down">4</asp:HyperLink>
	<asp:HyperLink ID="hlPageIdx5" runat="server" ClientIDMode="Static" CssClass="page_down">5</asp:HyperLink>
	<asp:Literal ID="litRightDots" runat="server" ClientIDMode="Static">...</asp:Literal>
	<asp:HyperLink ID="hlPageScrollRight" runat="server" ClientIDMode="Static" CssClass="page_down">&gt;</asp:HyperLink>
</div>