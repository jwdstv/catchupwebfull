﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
	public partial class PagingControlWithLinks : System.Web.UI.UserControl
	{
		private const string CssClass_SelectedPageLink = "page_up";
		private const string CssClass_NotSelectedPageLink = "page_down";

		private HyperLink[] _pageLinks;


		#region NumberOfItems
		private int _numberOfItems = 0;
		/// <summary>
		/// Total number of items.
		/// </summary>
		public int NumberOfItems
		{
			get { return _numberOfItems; }
			set { _numberOfItems = value; }
		}
		#endregion


		#region NumberOfItemsPerPage
		private int _numberOfItemsPerPage = 5;
		/// <summary>
		/// Number of items per page
		/// </summary>
		public int NumberOfItemsPerPage
		{
			get { return _numberOfItemsPerPage; }
			set { _numberOfItemsPerPage = value; }
		}
		#endregion


		#region SelectedPageNumber
		private int _selectedPageNumber = 1;
		/// <summary>
		/// The currently selected page number
		/// </summary>
		public int SelectedPageNumber
		{
			get { return _selectedPageNumber; }
			set { _selectedPageNumber = value; }
		}
		#endregion


		#region NumberOfPages
		private int _numberOfPages = 0;
		/// <summary>
		/// The number of pages in this collection
		/// </summary>
		public int NumberOfPages
		{
			get { return _numberOfPages; }
		}
		#endregion


		#region NumberOfLinksToDisplay
		/// <summary>
		/// 
		/// </summary>
		public int NumberOfLinksToDisplay
		{
			get { return _pageLinks == null ? 0 : _pageLinks.Length; }
		}
		#endregion


		protected void Page_Init ( object sender, EventArgs e )
		{
			_selectedPageNumber = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.PageNumber, 1);

			_pageLinks = new HyperLink[] { hlPageIdx1, hlPageIdx2, hlPageIdx3, hlPageIdx4, hlPageIdx5 };
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
			// PopulateControl();
		}

		public void PopulateControl ( int newPageNumber )
		{
			_selectedPageNumber = newPageNumber;

			_numberOfPages = Math.Max((int)Math.Ceiling((float)_numberOfItems / (float)_numberOfItemsPerPage), 1); // There should be atleast one page

			int leftIndex = GetScrollLeftIndexForCurrentPage();

			bool displayLeftScroll = leftIndex > 0 && _numberOfItems > _numberOfItemsPerPage;

			bool displayRightScroll = _numberOfItems > _numberOfItemsPerPage && (_numberOfPages >= (leftIndex + 1 + _pageLinks.Length));

			litCurrentPage.Text = SelectedPageNumber.ToString();
			litPageCount.Text = _numberOfPages.ToString();

			int pageNumber = 1;

			if (displayLeftScroll)
			{
				pageNumber = leftIndex - NumberOfLinksToDisplay + 1;
				hlPageScrollLeft.ToolTip = string.Format("Scroll down to pages {0} to {1}", pageNumber, pageNumber + NumberOfLinksToDisplay - 1);
				hlPageScrollLeft.NavigateUrl = SiteUtils.Request.AddParameterToRequest(Request, SiteConstants.Urls.ParameterNames.PageNumber, pageNumber.ToString()).PathAndQuery;
			}
			hlPageScrollLeft.Visible = displayLeftScroll;
			litLeftDots.Visible = displayLeftScroll;

			for (int i = leftIndex; i < (leftIndex + NumberOfLinksToDisplay); i++)
			{
				pageNumber = i + 1;
				if ((i * _numberOfItemsPerPage) < _numberOfItems)
				{
					_pageLinks[i - leftIndex].Visible = true;
					_pageLinks[i - leftIndex].CssClass = pageNumber == SelectedPageNumber ? CssClass_SelectedPageLink : CssClass_NotSelectedPageLink;
					_pageLinks[i - leftIndex].Text = pageNumber.ToString();
					_pageLinks[i - leftIndex].ToolTip = pageNumber == SelectedPageNumber ? "Selected Page" : string.Format("Click to view page {0}", pageNumber);
					// Lets set Request QueryString page number.
					_pageLinks[i - leftIndex].NavigateUrl = SiteUtils.Request.AddParameterToRequest(Request, SiteConstants.Urls.ParameterNames.PageNumber, pageNumber.ToString()).PathAndQuery;
				}
				else
				{
					_pageLinks[i - leftIndex].Visible = false;
				}
			}

			if (displayRightScroll)
			{
				pageNumber++;
				hlPageScrollRight.ToolTip = string.Format("Scroll up to pages {0} to {1}", pageNumber, pageNumber + NumberOfLinksToDisplay - 1);
				hlPageScrollRight.NavigateUrl = SiteUtils.Request.AddParameterToRequest(Request, SiteConstants.Urls.ParameterNames.PageNumber, pageNumber.ToString()).PathAndQuery;
			}
			hlPageScrollRight.Visible = displayRightScroll;
			litRightDots.Visible = displayRightScroll;
		}

		private int GetScrollLeftIndexForCurrentPage ()
		{
			return ((SelectedPageNumber - 1) / NumberOfLinksToDisplay) * NumberOfLinksToDisplay;
		}
	}
}