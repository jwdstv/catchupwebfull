﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadcrumbAndTitle.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.BreadcrumbAndTitle" %>

<div class="breadcrumbs">
<asp:Repeater ID="rptBreadcrumb" runat="server">
<ItemTemplate>
<a href="<%# DataBinder.Eval(Container.DataItem, "NavigateUrl") %>"><strong><%# DataBinder.Eval(Container.DataItem, "DisplayName") %></strong></a>
</ItemTemplate>
</asp:Repeater>
<strong><asp:Literal ID="litBreadcrumbCurrent" runat="server"></asp:Literal></strong>
</div>