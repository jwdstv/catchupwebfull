﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagingControl.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.PagingControl" %>
<div class="paging-ext"> Page <asp:Literal ID="litCurrentPage" runat="server"></asp:Literal> of <asp:Literal ID="litPageCount" runat="server"></asp:Literal>&nbsp;&nbsp; | 
	<asp:LinkButton ID="hlPageScrollLeft" runat="server" ClientIDMode="Static" CssClass="page_down" CommandName="ScrollLeft" oncommand="hlPage_Command">&lt;</asp:LinkButton>
	<asp:Literal ID="litLeftDots" runat="server" ClientIDMode="Static">...</asp:Literal>
	<asp:LinkButton ID="hlPageIdx1" runat="server" ClientIDMode="Static" CssClass="page_up" CommandName="PageSelect" CommandArgument="1" OnCommand="hlPage_Command">1</asp:LinkButton>
	<asp:LinkButton ID="hlPageIdx2" runat="server" ClientIDMode="Static" CssClass="page_down" CommandName="PageSelect" CommandArgument="1" OnCommand="hlPage_Command">2</asp:LinkButton>
	<asp:LinkButton ID="hlPageIdx3" runat="server" ClientIDMode="Static" CssClass="page_down" CommandName="PageSelect" CommandArgument="1" OnCommand="hlPage_Command">3</asp:LinkButton>
	<asp:LinkButton ID="hlPageIdx4" runat="server" ClientIDMode="Static" CssClass="page_down" CommandName="PageSelect" CommandArgument="1" OnCommand="hlPage_Command">4</asp:LinkButton>
	<asp:LinkButton ID="hlPageIdx5" runat="server" ClientIDMode="Static" CssClass="page_down" CommandName="PageSelect" CommandArgument="1" OnCommand="hlPage_Command">5</asp:LinkButton>
	<asp:Literal ID="litRightDots" runat="server" ClientIDMode="Static">...</asp:Literal>
	<asp:LinkButton ID="hlPageScrollRight" runat="server" ClientIDMode="Static" CssClass="page_down" CommandName="ScrollRight" oncommand="hlPage_Command">&gt;</asp:LinkButton>
</div>
