﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiChoiceFooter.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.MultiChoiceFooter" %>
<div id="footer_ctrlFooter_mfooter_za">
	
		<div class="footer_content">
			<div class="logo">
			   <a target="_blank" href="http://www.multichoice.co.za"><img border="0" src="/app_themes/onDemand/images/logo_multiChoice.png" alt="MultiChoice"></a></div>
			<div class="copy">
				&copy;
			2011
				MultiChoice (PTY) LTD. All rights reserved.<br>
					<a target="_blank" href="http://www.multichoice.co.za" target="_blank">MultiChoice Website</a> | 
					<a target="_blank" href="http://www.dstv.com/Pages/TermsConditions/">Terms &amp; Conditions</a> | 
					<a target="_blank" href="http://www.dstv.com/Pages/PrivacyPolicy/">Privacy Policy</a> | 
					<a target="_blank" href="http://dstv.com/Pages/Copyright/">Copyright</a> | 
					<a target="_blank" href="http://dstv.com/Pages/Glossary/">Glossary</a> | 
					<a target="_blank" href="http://dstv.com/Pages/DStv-i/">DStv-i</a> | 
					<a target="_blank" href="http://dstv.com/Pages/EmailScam/">Email Scam</a> | 
					<a target="_blank" href="http://www.opa.co.za/">OPA</a> |
					<a href="/SiteMap.aspx" title="On Demand Site Map" id="aSiteMapLink" runat="server" clientidmode="Static">Site Map</a> |
					<a href="/Help/ContactUs.aspx" id="aContactUsLink" runat="server" clientidmode="Static"> Contact us</a> | 
					<a target="_blank" href="http://www.careers24.com/jobs/mih-internet-africa-jobs/em-mih-internet-africa/">Careers</a>
			</div>
		</div>
	
</div>