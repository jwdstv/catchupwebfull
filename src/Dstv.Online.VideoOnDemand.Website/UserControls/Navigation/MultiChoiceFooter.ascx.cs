﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
	public partial class MultiChoiceFooter : System.Web.UI.UserControl
	{
		private bool _disabledForOnDemand = false;

		public bool DisabledForOnDemand
		{
			get { return _disabledForOnDemand; }
			set { _disabledForOnDemand = value; }
		}
		
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (DisabledForOnDemand)
			{
				aContactUsLink.Disabled = true;
				//aHelpLink.Disabled = true;
				aSiteMapLink.Disabled = true;
			}
		}
	}
}