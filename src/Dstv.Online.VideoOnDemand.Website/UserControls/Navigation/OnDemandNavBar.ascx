﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnDemandNavBar.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Navigation.OnDemandNavBar" %>
<script type="text/javascript">
    function DoSearch() {
        var searchTerm = $.trim($('#txtSearchTerm').val());
        if (searchTerm.length > 0) {
            document.location = '<%=SearchPageUrl%>?SortFilter=SearchResultOrder&Page=1&searchTerm=' + searchTerm;
        }
        else {
            alert("Please enter a word or term to search for.");
            $('#txtSearchTerm').focus();
        }
    }

    function SearchKeyPress(e) {
        var evt = e ? e : event;
        if (evt.keyCode == 13) {
            DoSearch();
            e.returnValue = false;
            e.cancel = true;
            return false;
        }
    }

</script>

<ul id="MenuBar" class="MenuBarHorizontal">
   <li><a id="liHome" runat="server" href="/Home.aspx" class="nav" title="On Demand Home">HOME</a></li>
   <li id="liBrowseBoxOffice" runat="server" clientidmode="Static"><a href="/BoxOffice/Browse.aspx" class="nav" title="Browse On Demand Video Catalogue">BROWSE</a></li>
   <li id="liBrowseCatchUp" runat="server" clientidmode="Static"><a href="/CatchUp/Browse.aspx" class="nav" title="Browse On Demand Video Catalogue">BROWSE</a></li>
   <%
       if (!HideBoxOffice){
        %>
   <li><a href="/BoxOffice/Home.aspx" class="nav-boxOffice" title="BoxOffice Home">BOXOFFICE</a></li>
   <li><a href="/CatchUp/Home.aspx" class="nav-catchUp" title="CatchUp Home">CATCHUP</a></li>
   <%
       } %>
    <li><a href="/Download/DesktopPlayer.aspx" class="nav" title="On Demand Desktop Player">DESKTOP PLAYER</a></li>
    <li><a href="/CatchUp/FindOutMore.aspx" class="nav" title="On Demand Find Out More">FIND OUT MORE</a></li>
    <li><a href="/Help/FAQ.aspx" class="nav" title="On Demand Frequently Asked Questions">FAQ</a></li>
    <li><a href="/Help/ContactUs.aspx" class="nav" title="On Demand Contact Us">CONTACT US</a></li>
    <li id="liMyRentalsLink" runat="server" clientidmode="Static"><a href="/BoxOffice/MyRentals.aspx" class="nav-boxOffice" title="My Rentals">MY RENTALS (<asp:literal id="litMyRentalsActiveCount" runat="server"></asp:literal>)</a></li>
</ul>
<div class="search">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td>Search</td>
      <td align="right" valign="top"><input id="txtSearchTerm" type="text" class="searchInput" onkeypress="javascript:SearchKeyPress(event);"  /></td>
      <td width="22">
         <a href="javascript:DoSearch();"><img src="<%=SiteRoot %>app_themes/onDemand/images/btn_nav_search.png" style="border:none;" alt="search" /></a>
      </td>
   </tr>
   </table>
</div>