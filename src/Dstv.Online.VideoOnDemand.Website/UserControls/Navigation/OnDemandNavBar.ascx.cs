﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
	public partial class OnDemandNavBar : BaseControl
	{
		private string _searchPageUrl = "~/Video/SearchVideo.aspx";

		public string SearchPageUrl
		{
			get { return SiteUtils.GetAbsoluteUrlPath(_searchPageUrl); }
			set { _searchPageUrl = value; }
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
            try
            {
                SiteEnums.ProductType product = SiteUtils.Request.GetProductTypeFromRequest();

                SetBrowseLink(product);
                if (HideBoxOffice) //RSS: no rentals if no BoxOffice
                {
                    liMyRentalsLink.Visible = false;
                }
                else
                {
                    SetMyRentalsLink();
                }

                //Remove the custom page link home behavior
                //liHome.HRef = SiteConstants.Urls.GetHomePageUrl(product).Replace("~/", "/");
            }
            catch { }//TODO: REmove try catch
		}

        private void SetBrowseLink(SiteEnums.ProductType product)
		{
			if (product == SiteEnums.ProductType.CatchUp)
			{
				liBrowseBoxOffice.Visible = false;
				liBrowseCatchUp.Visible = true;
			}
			else
			{
                if (HideBoxOffice)
                {
                    liBrowseBoxOffice.Visible = false;
				    liBrowseCatchUp.Visible = true;
                }
                else { 
                    liBrowseBoxOffice.Visible = true;
                    liBrowseCatchUp.Visible = false;
                }
			}
		}

		private void SetMyRentalsLink ()
		{
			int activeRentalCount = 0;

			if (DstvUser.IsAuthenticated)
			{
				activeRentalCount = Rentals.GetActiveVideoRentalCountForCurrentUser();
			}

			if (activeRentalCount > 0)
			{
				litMyRentalsActiveCount.Text = activeRentalCount.ToString();
				liMyRentalsLink.Visible = true;
			}
			else
			{
				liMyRentalsLink.Visible = false;
			}
		}
	}
}