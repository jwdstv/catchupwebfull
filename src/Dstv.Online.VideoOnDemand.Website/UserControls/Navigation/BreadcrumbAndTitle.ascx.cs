﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using System.Web.UI.HtmlControls;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Navigation
{
	public partial class BreadcrumbAndTitle : System.Web.UI.UserControl
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            SiteBreadcrumb.BreadcrumbList breadcrumbList = new SiteBreadcrumb.BreadcrumbList(this.Request.RawUrl, Page.Title);
            rptBreadcrumb.DataSource = breadcrumbList;
            rptBreadcrumb.DataBind();

            litBreadcrumbCurrent.Text = breadcrumbList.CurrentPageBreadcrumb;

            Page.Title = breadcrumbList.GetPageTitle();
            HtmlMeta descriptionMetaTag = new HtmlMeta();
            descriptionMetaTag.Name = "description";
            descriptionMetaTag.Content = breadcrumbList.GetPageMetaDescription();            
            Page.Master.Page.Header.Controls.Add(descriptionMetaTag);

            Page.Title = breadcrumbList.GetPageTitle();
            HtmlMeta keywordMetaTag = new HtmlMeta();
            keywordMetaTag.Name = "keywords";
            keywordMetaTag.Content = breadcrumbList.GetPageMetaKeywords();
            Page.Master.Page.Header.Controls.Add(keywordMetaTag);
        }
	}
}