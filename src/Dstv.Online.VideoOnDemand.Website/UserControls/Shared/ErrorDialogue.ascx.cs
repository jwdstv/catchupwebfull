﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Shared
{
	public partial class ErrorDialogue : System.Web.UI.UserControl
	{

		public string DialogueTitle { get; set; }

		public string MessageTitle { get; set; }

		public string Message { get; set; }

		public string MessageDetail { get; set; }

		protected void Page_Load ( object sender, EventArgs e )
		{

		}

		public void ShowErrorDialogue ( string dialogueTitle, string messageTitle, string message )
		{
			this.Visible = true;

			DialogueTitle = dialogueTitle;
			MessageTitle = messageTitle;
			Message = message;

			Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowError", "ShowErrorDialogue();", true);
		}

		public void ShowErrorDialogue ( string dialogueTitle, string messageTitle, string message, string messageDetail )
		{
			this.Visible = true;

			DialogueTitle = dialogueTitle;
			MessageTitle = ReplaceLineBreaks(messageTitle);
			Message = ReplaceLineBreaks(message);
			MessageDetail = ReplaceLineBreaks(messageDetail);

			Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowError", "ShowErrorDialogue();", true);
		}

		private string ReplaceLineBreaks ( string str )
		{
			return str.Replace("\r\n", "<br/>");
		}
	}
}