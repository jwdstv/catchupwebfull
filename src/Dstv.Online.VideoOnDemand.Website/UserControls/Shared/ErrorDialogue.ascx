﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorDialogue.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Shared.ErrorDialogue" %>
<script type="text/javascript">
    function ShowErrorDialogue() {
        $('#divErrorDialogue').modal();
    }
    
    function HideErrorDialogue() {
        $.modal.close();
        return false;
    }
</script>
<div id="divErrorDialogue" style="display:none;">
<div class="form">
    <div class="formHead">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="20">
                </td>
                <td>
                    <img src="/app_themes/onDemand/images/dstvconnect.png" alt="" border="0" />
                </td>
                <td width="15">
                </td>
                <td>
                    <img src="/app_themes/onDemand/images/pr_headsep.gif" alt="" border="0" />
                </td>
                <td width="15">
                </td>
                <td class="lbformHeadTxt">
                    <%=DialogueTitle%>
                </td>
            </tr>
        </table>
    </div>
     <div class="lbformItemsPAY">
        <span class="formHeadTxt"><%=MessageTitle%></span>
        <br />
        <br />
        <img src="/app_themes/onDemand/images/icon_excl.gif" alt="Error" style="border-width: 0px;vertical-align:text-top" /> <span class="lbHeadBlue"><%=Message%> </span>
        <br />
        <br />
        <% // <span class="lbHeadBlue"> < % =MessageDetail % > </span>%>
        <br />
        <br />
        <br />
        <br />
        <input type="image" id="btnOk" src="/app_themes/onDemand/images/btn_ok.gif" alt="Ok" onclick="javascript:return HideErrorDialogue();" style="border-width: 0px;" />
        <br />
        <br />
    </div>
</div>
</div>