﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using Dstv.Online.VideoOnDemand.Website.UserControls.Video;
using System.Xml;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Shared.LightTreeView
{
    public partial class LightTreeViewControl : VideoControlBase
    {

        #region Once off Scripts and styles

        // The idea here is for this control to be self-contained.  
        // The style & javascript should generally not change 

        #region styles
        private string styles = @"
<style type=""text/css"">
.TreeView {font: Verdana; line-height: 20px; cursor: pointer; font-style: normal;}
.TreeView LI {padding: 0 0 0 18px; float: left; width: 100%; list-style: none;}
.TreeView, .TreeView ul {margin: 0; padding: 0;}
LI.Expanded {background: url(/App_Themes/OnDemand/images/minus.gif) no-repeat left top;}
LI.Expanded ul {display: block;}
LI.Collapsed {background: url(/App_Themes/OnDemand/images/plus.gif) no-repeat left top;}
LI.Collapsed ul {display: none;}
LI.PlayCatchUp {background: url(/app_themes/onDemand/images/icon_arrowSml-CatchUp.png) no-repeat left top;}
LI.PlayCatchUp ul {display: block;}
LI.PlayBoxOffice {background: url(/app_themes/onDemand/images/icon_arrowSml-BoxOffice.png) no-repeat left top;}
LI.PlayBoxOffice ul {display: block;}
</style>";

        #endregion

        #region javascript
        private string javascript = @"
<script type=""text/javascript"" language=""javascript"">
Array.prototype.indexOf = IndexOf;
function ToggleClass(element, firstClass, secondClass, event) {
event.cancelBubble = true;
var classes = element.className.split("" "");
var firstClassIndex = classes.indexOf(firstClass);
var secondClassIndex = classes.indexOf(secondClass);
if (firstClassIndex == -1 && secondClassIndex == -1) {
classes[classes.length] = firstClass;
}
else if (firstClassIndex != -1) { classes[firstClassIndex] = secondClass; }
else { classes[secondClassIndex] = firstClass; }
element.className = classes.join("" "");
}
function IndexOf(item) {
for (var i = 0; i < this.length; i++) {
if (this[i] == item) {
return i;
}
} return -1;
}
function ToggleNodeStateHandler(event) { ToggleClass(this, ""Collapsed"", ""Expanded"", (event == null) ? window.event : event); }
function PreventBubbleHandler(event) { if (!event) event = window.event; event.cancelBubble = true; }
function SetupTreeView(elementId) {
var tree = document.getElementById(elementId); var treeElements = tree.getElementsByTagName(""li"");
for (var i = 0; i < treeElements.length; i++) {
if (treeElements[i].getElementsByTagName(""ul"").length > 0) { treeElements[i].onclick = ToggleNodeStateHandler; }
else { treeElements[i].onclick = PreventBubbleHandler; }
}
}
</script>";
        #endregion

        protected void RegisterScripts()
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LightTreeViewJavascript", javascript);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "LightTreeViewStyle", styles);
        }


        #endregion

        public string TreeViewClientId
        {
            get
            {
                return this.ClientID + "_TreeView";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterScripts();
        }

        public LightTreeView TreeView = null;

        protected override void OnPreRender(EventArgs e)
        {
            if (TreeView != null)
            {
                litTv.Text = TreeView.ToString();
                RegisterScripts();
            }
            base.OnPreRender(e);
        }


    }


    [Serializable]
    [XmlRoot(ElementName = "div")]
    public class LightTreeView
    {
        protected LightTreeView() { } // for serialisation

        private string divIdentifier = "TreeView";
        [XmlAttribute(AttributeName = "id")]
        public string ClientId
        {
            get
            {
                return divIdentifier;
            }
            set { }
        }

        private ParentNodeCollection nodes = new ParentNodeCollection();
        [XmlElement(ElementName = "ul")]
        public ParentNodeCollection Nodes
        {
            get
            {
                return nodes;
            }
            set { }
        }

        public LightTreeView(string TreeViewClientId)
        {
            divIdentifier = TreeViewClientId + "_TreeView";
        }

        public override string ToString()
        {
            return Serialise(this);
        }

        protected string Serialise(LightTreeView instance)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(LightTreeView));
            XmlSerializerNamespaces xmlnsEmpty = new XmlSerializerNamespaces();
            xmlnsEmpty.Add("", "");

            StringBuilder stringB = new StringBuilder();

            XmlWriterSettings settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, CloseOutput = false, Encoding = Encoding.ASCII };

            using (XmlWriter wrter = XmlWriter.Create(stringB, settings))
            {
                serializer.Serialize(wrter, instance, xmlnsEmpty);
                return stringB.ToString();
            }
        }
    }


    public class ParentNodeCollection
    {
        private List<Node> nodes = null;

        private string nodeClass = "TreeView";
        [XmlAttribute(AttributeName = "class")]
        public string NodeClass
        {
            get
            {
                return nodeClass;
            }
            set { }
        }

        [XmlElement(ElementName = "li")]
        public List<Node> Nodes
        {
            get
            {
                return nodes;
            }
            set { }
        }

        private void AddNode(Node node)
        {
            if (nodes == null)
                nodes = new List<Node>();
            nodes.Add(node);
        }

        public void Add(Node node)
        {
            AddNode(node);
        }

        public void Add(string value)
        {
            AddNode(new Node(value));
        }

        public void Add(string value, string navigationUrl)
        {
            AddNode(new Node(value, navigationUrl));
        }

        internal ParentNodeCollection() { }
    }


    public class ChildNodes
    {
        private List<Node> nodes = new List<Node>();

        private string nodeClass = null;
        [XmlAttribute(AttributeName = "class")]
        public string NodeClass
        {
            get
            {
                return nodeClass;
            }

            set { }
        }

        [XmlElement(ElementName = "li")]
        public List<Node> Nodes
        {
            get
            {
                return nodes;
            }
            set { }
        }

        internal ChildNodes() { }
    }

    public class Node
    {
        #region public ALink Link
        protected ALink _a;
        [XmlElement(ElementName = "a")]
        public ALink Link
        {
            get
            {
                return _a;
            }
            set
            {
                _a = value;
            }
        }
        #endregion

        #region bool IsExpanded
        [XmlIgnore]
        protected bool _isExpanded = false;

        [XmlIgnore]
        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
            }
        }
        #endregion

        private ChildNodes children = null;
        [XmlElement(ElementName = "ul")]
        public ChildNodes Children
        {
            get
            {
                return children;
            }
            set { }
        }

        private void AddChildNode(Node node)
        {
            if (children == null)
                children = new ChildNodes();

            children.Nodes.Add(node);
        }

        public void AddChild(Node node)
        {
            AddChildNode(node);
        }

        public void AddChild(string value)
        {
            AddChildNode(new Node(value));
        }

        public void AddChild(string value, string navigationUrl)
        {
            AddChildNode(new Node(value, navigationUrl));
        }

        private string overriddenClassAttribute = null;
        [XmlAttribute(AttributeName = "class")]
        public string ClassAttribute
        {
            get
            {
                if (children != null && overriddenClassAttribute == null)
                {
                    return IsExpanded ? "Expanded" : "Collapsed";
                }
                if (overriddenClassAttribute != null)
                {
                    return overriddenClassAttribute;
                }
                return null;
            }
            set
            {
                overriddenClassAttribute = value;
            }
        }
        protected Node() { }

        public Node(string value)
        {
            _a = new ALink(value);
        }

        public Node(string value, string navigationUrl)
        {
            _a = new ALink(value, navigationUrl);
        }

    }

    public class ALink
    {
        [XmlAttribute(AttributeName = "href")]
        public string Url = "";

        [XmlText]
        public string Value;

        public ALink(string value, string url)
        {
            Url = url;
            Value = value;
        }

        public ALink(string value)
        {
            Value = value;
            Url = null;
        }

        protected ALink() { }
    }

}