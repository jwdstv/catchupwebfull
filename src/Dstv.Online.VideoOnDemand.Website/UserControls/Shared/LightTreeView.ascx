﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LightTreeView.ascx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Shared.LightTreeView.LightTreeViewControl" EnableViewState="false" %>
<%@ OutputCache Duration="600" VaryByParam="*" %>

<asp:Literal ID="litTv" runat="server"></asp:Literal>
<script type="text/javascript">SetupTreeView("<%= TreeViewClientId %>");</script>