﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Shared
{
	public class JsRenderingControl : System.Web.UI.UserControl
	{
		protected void Page_Init ( object sender, EventArgs e )
		{
			if (!Page.ClientScript.IsClientScriptIncludeRegistered("Rendering"))
			{
				Page.ClientScript.RegisterClientScriptInclude("Rendering", "/scripts/rendering.js");
			}
		}
		
	}
}