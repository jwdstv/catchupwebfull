﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagerControl.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice.PagerControl" %>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            Page <asp:Label ID="lblPageNumber" runat="server">1</asp:Label> of <asp:Label ID="lblTotalPages" runat="server">?</asp:Label>&nbsp;&nbsp; | 
        </td>
        <td>
            &nbsp; &nbsp;
        </td>
        <td>
            <asp:Button ID="btnBack" runat="server" Text="&lt;" OnClick="Pager_Click" CssClass="pagerButton" />
        </td>
        <td>
            &nbsp;&nbsp;
        </td>
        <td>
            <asp:Button ID="btnFirst" runat="server" Text="1" CommandArgument="1" OnClick="Pager_Click" CssClass="pagerButton" />
            <asp:Label ID="lblFirst" Text="..." runat="server" />
        </td>
        <td>
            <asp:Repeater ID="repPages" runat="server">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Button ID="btnLink" Text="<%# ((Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice.PagerPage)Container.DataItem).PageNumber %>"
                        CommandArgument="<%# ((Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice.PagerPage)Container.DataItem).Link %>"
                        Enabled="<%# ((Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice.PagerPage)Container.DataItem).Enabled %>"
                        runat="server" OnClick="Pager_Click" CssClass="pagerButton" />
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
        </td>
        <td>
            <asp:Label ID="lblLast" Text="..." runat="server" />
            <asp:Button ID="btnLast" runat="server" OnClick="Pager_Click" CssClass="pagerButton" />
        </td>
        <td>
            &nbsp;&nbsp;
        </td>
        <td>
            <asp:Button ID="btnNext" runat="server" Text="&gt;" OnClick="Pager_Click" CssClass="pagerButton" />
        </td>
    </tr>
</table>
