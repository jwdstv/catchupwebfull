﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayDialogue.ascx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.UserControls.Profile.PayDialogue" %>
<script type="text/javascript">
	$(document).ready(
		function () {
           $('[id^="txtPinPart"]').keypress(ProcessPinKey);
		});

    function ShowPaymentModal() 
    {
        jQuery.lightbox('#<%=TargetControlID %>');
    }

    function DoRentNow()
    {
        window.location = window.location + '&<%=UrlParameterName %>=1';
    }

	function ProcessPinKey(e)
	{
		if (e.keyCode >= 48 && e.keyCode <= 57)
		{
			if (e.srcElement.id == 'txtPinPart1')
			{
				window.setTimeout("$('#txtPinPart2').focus().select();", 50);
			}
			else if (e.srcElement.id == 'txtPinPart2')
			{
				window.setTimeout("$('#txtPinPart3').focus().select();", 50);
			}
			else if (e.srcElement.id == 'txtPinPart3')
			{
				window.setTimeout("$('#txtPinPart4').focus().select();", 50);
			}
		}
		else
		{
			return false;
		}
	}

	function ShowLoginDialogue ()
	{
        jQuery.lightbox("#LightboxDivConsole");
	}

	function HidePaymentDialogue ()
	{
		$.LightBoxObject.close();
        return false;
	}

	 function ShowPaymentDialogue() 
    {
        var isAuthenticated = <%= this.Page.User.Identity.IsAuthenticated.ToString().ToLower() %>;
		  var loadViaUrlParameter = <%= this.LoadViaUrlParameter.ToString().ToLower() %>;

        if (isAuthenticated)
        {
				try
				{
					SetPaymentState('Load');
				}
				catch (e)
				{
				}

           ShowPaymentModal();

				$('#txtPinPart1').focus();
        }
        else
        {
				if (redirectUrl.indexOf("?") != -1) {
					redirectUrl = redirectUrl + "&<%=UrlParameterName%>=1";
				}
				else {
					redirectUrl = redirectUrl + "?<%=UrlParameterName%>=1";
				}

            ShowLoginDialogue();
        }
    }

	 function ClearPaymentPin ()
	 {
		$('#txtPinPart1').val('');
		$('#txtPinPart2').val('');
		$('#txtPinPart3').val('');
		$('#txtPinPart4').val('');
	 }

	 function SetPaymentState ( paymentState, message, refNo )
	 {
		var divInputPIN = document.getElementById('divInputPIN');
		divInputPIN.disabled = true;
		var chkPaymentTandC = document.getElementById('chkPaymentTandC');
		chkPaymentTandC.disabled = true;
		var btnSubmit = document.getElementById('btnSubmit');
		btnSubmit.disabled = true;
		var btnCancelPay = document.getElementById('btnCancelPay');
		btnCancelPay.disabled = true;
		var divProcessing = document.getElementById('divProcessing');
		divProcessing.style.display = 'none';
		var divProcessingResultError = document.getElementById('divProcessingResultError');
		divProcessingResultError.style.display = 'none';
		var divProcessingResultSuccess = document.getElementById('divProcessingResultSuccess');
		divProcessingResultSuccess.style.display = 'none';
		var divInvalidPIN = document.getElementById('divInvalidPIN');
		divInvalidPIN.style.display = 'none';

		var spErrorMessage = document.getElementById('spErrorMessage');
		var spErrorRefNo = document.getElementById('spErrorRefNo');

		if (paymentState == 'Load')
		{
			ClearPaymentPin();
			divInputPIN.disabled = false;
			btnSubmit.disabled = false;
			btnCancelPay.disabled = false;
			chkPaymentTandC.disabled = false;
		}
		else if (paymentState == 'Processing')
		{
			divProcessing.style.display = 'inline';
		}
		else if (paymentState == 'ProcessedWithError')
		{
			divInputPIN.disabled = false;
			btnSubmit.disabled = false;
			btnCancelPay.disabled = false;
			chkPaymentTandC.disabled = false;
			divProcessingResultError.style.display = 'inline';
			if (message == 'NotAuthenticated')
			{
				spErrorMessage.innerHTML = 'You are not logged into the website. Please login to the website before renting the video.';
				spErrorRefNo.innerHTML = refNo;
			}
			else if (message.indexOf('SystemError') >= 0)
			{
				spErrorMessage.innerHTML = 'A system error occurred';
				spErrorRefNo.innerHTML = refNo;
				if (message.indexOf('DEBUG') >= 0)
				{
					spErrorRefNo.innerHTML += message;
				}
			}
			else if (message.toLowerCase().indexOf('invalid pin') >= 0)
			{
				divInvalidPIN.style.display = 'inline';
				spErrorMessage.innerHTML = 'Invalid PIN entered';
				spErrorRefNo.innerHTML = 'NA';
			}
			else
			{
				spErrorMessage.innerHTML = message;
				spErrorRefNo.innerHTML = refNo;
			}	
		}
		else if (paymentState == 'ProcessedSuccess')
		{
			divProcessingResultSuccess.style.display = 'inline';
			window.setTimeout('PaymentFinished()', 10000);
		}
		
	 }

    function SubmitPayment() 
    {
        var pinPart1 = $('#txtPinPart1').val();
        var pinPart2 = $('#txtPinPart2').val();
        var pinPart3 = $('#txtPinPart3').val();
        var pinPart4 = $('#txtPinPart4').val();

        if (pinPart1 != "" && pinPart2 != "" && pinPart3 != "" && pinPart4 != "") 
        {
				if (!$('#chkPaymentTandC').attr('checked'))
			  {
				alert('Please accept the DStv Online terms and conditions before continuing with your rental.');
				return;
			  }

				SetPaymentState('Processing');

            var customerPin = pinPart1 + pinPart2 + pinPart3 + pinPart4;
            var catalogItemId = '<%= this.CatalogItemIdValue %>';
            var catalogueItemCost = <%= this.CatalogueItemCost %>;

				var serviceUrl = "<%=ServiceUrl%>";
				var proxy = new ServiceProxy(serviceUrl);

            proxy.invoke(
					"/SubmitVideoRentalPayment", 
					{ "_catalogItemId": catalogItemId, "_catalogItemCost": catalogueItemCost, "_customerPin": customerPin },
               SubmitPayment_OnSucceeded,
					SubmitPayment_OnFailed
               );
        }
    }

	 function SubmitPayment_OnSucceeded(result)
	 {
		var json = JSON.parse(result, true);

      if (json.Success) {
            SetPaymentState('ProcessedSuccess');
      }
      else {
			SetPaymentState('ProcessedWithError', json.ErrorMsg, json.ReferenceNo);
      }
	 }

	function SubmitPayment_OnFailed(error)
	{
		SetPaymentState('ProcessedWithError', 'ClientError:' + error.message, "NA");
	}

	 function PaymentFinished ()
	 {
			var url = '<%=PaymentSuccessfulRedirectUrl%>';
			if (url == null || url.length == 0)
			{
				url = window.location.href;

				url = url.replace("?<%=UrlParameterName%>=1", "");
				url = url.replace("&<%=UrlParameterName%>=1", "");
			}
         window.location = url;
	 }
</script>
<div id="pnlPay" style="display: none;">
    <!-- LOGIN BEGIN -->
    <div class="form">
        <div class="formHead">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td width="20">
                    </td>
                    <td>
                        <img src="/app_themes/onDemand/images/dstvconnect.png" alt="" border="0" />
                    </td>
                    <td width="15">
                    </td>
                    <td>
                        <img src="/app_themes/onDemand/images/pr_headsep.gif" alt="" border="0" />
                    </td>
                    <td width="15">
                    </td>
                    <td class="lbformHeadTxt">
                        Video Rental Payment Details
                    </td>
                </tr>
            </table>
        </div>
        <div class="lbformItemsPAY">
            <span class="formHeadTxt">
                <%=CatalogueItemTitle%></span><br />
            <br />
            <span class="lbHeadBlue">This video costs <span class="greenTXT">
                <asp:Label ID="lblCost" runat="server" Text="<%# this.CatalogueItemCostFormatted %>" />
            </span>to rent and watch.<br />
                <br />
            </span>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblPay" runat="server"
                clientidmode="Static">
                <tr>
                    <td class="lbHeadBlue">
                        Your pre-paid balance, currently <strong>
                            <asp:Label ID="lblBalance" runat="server" Text="<%# this.BalanceFormatted %>" /></strong>,
                        will be debited with this amount.<br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="formHeadTxt">
                        Please enter your pre-paid PIN to rent and watch this video online.<br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="inputBoxesPIN" id="divInputPIN">
                            <asp:TextBox ID="txtPinPart1" ClientIDMode="Static" runat="server" Width="25" MaxLength="1" />
                            <asp:RequiredFieldValidator ID="reqPinPart1" ControlToValidate="txtPinPart1" ErrorMessage="*"
                                runat="server" ValidationGroup="PaymentGroup" Display="Dynamic" />
                            &nbsp;
                            <asp:TextBox ID="txtPinPart2" ClientIDMode="Static" runat="server" Width="25" MaxLength="1" />
                            <asp:RequiredFieldValidator ID="reqPinPart2" ControlToValidate="txtPinPart2" ErrorMessage="*"
                                runat="server" ValidationGroup="PaymentGroup" Display="Dynamic" />
                            &nbsp;
                            <asp:TextBox ID="txtPinPart3" ClientIDMode="Static" runat="server" Width="25" MaxLength="1" />
                            <asp:RequiredFieldValidator ID="reqPinPart3" ControlToValidate="txtPinPart3" ErrorMessage="*"
                                runat="server" ValidationGroup="PaymentGroup" Display="Dynamic" />
                            &nbsp;
                            <asp:TextBox ID="txtPinPart4" ClientIDMode="Static" runat="server" Width="25" MaxLength="1" />
                            <asp:RequiredFieldValidator ID="reqPinPart4" ControlToValidate="txtPinPart4" ErrorMessage="*"
                                runat="server" ValidationGroup="PaymentGroup" Display="Dynamic" />
                        </div>
                        <div class="payErrorHead" id="divInvalidPIN" style="display: none; float: left; padding-left: 5px;
                            padding-top: 10px;">
                            * PIN provided is invalid</div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="padding-top: 15px; padding-bottom: 10px; padding-left: 5px;">
                        <input type="checkbox" id="chkPaymentTandC" />&nbsp;I accept the DStv Online <a href="/Help/TermsAndConditions.aspx"
                            target="_blank">terms and conditions</a>.
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="padding-top: 10px;">
                        <div id="divActionButtons">
                            <asp:ImageButton ID="btnSubmit" runat="server" AlternateText="Sign In" PostBackUrl="javascript:void(0);"
                                ImageUrl="/app_themes/onDemand/images/btn_payWatch.gif" OnClientClick="SubmitPayment()"
                                ValidationGroup="PaymentGroup" />
                            &nbsp;
                            <input type="image" id="btnCancelPay" src="/app_themes/onDemand/images/btn_cancel.gif"
                                alt="Cancel" onclick="javascript:return HidePaymentDialogue();" style="border-width: 0px;" />
                        </div>
                        <br />
                        <div id="divProcessing" style="display: none; padding-top: 20px">
                            <img src="/App_Themes/onDemand/images/loading1.gif" alt="processing" style="border: none;
                                vertical-align: text-bottom" />
                            <span class="lbHeadBlue" style="padding-left: 5px;">processing rental transaction, please
                                wait ...</span>
                        </div>
                        <div id="divProcessingResultError" style="display: none; padding-top: 20px">
                            <img src="/app_themes/onDemand/images/icon_excl.gif" alt="Error" style="border-width: 0px;
                                vertical-align: text-top" />
                            <span class="payErrorHead">There was an error processing your rental transaction.</span>
                            <br />
                            <br />
                            <strong>Reason: </strong><span id="spErrorMessage"></span>. Please try again and
                            if the problem persists then please contact DStv Online support (contact) and quote
                            this reference number: <span id="spErrorRefNo"></span>
                        </div>
                        <div id="divProcessingResultSuccess" style="display: none; padding-top: 20px">
                            <table style="border: none;">
                                <tr>
                                    <td valign="top" style="padding: 5px;">
                                        <img src="/app_themes/onDemand/images/pr_tick.gif" alt="Error" style="border-width: 0px;
                                            vertical-align: text-top" />
                                    </td>
                                    <td>
                                        <span class="lbHeadBlue">Your rental transaction has been successfully processed and
                                            the video has been added to your rental list.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        The video will be loaded in your browser shortly. To continue click <a href="javascript:PaymentFinished();">
                                            here</a>.
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">
                        Top up your pre-paid balance <a href="<%=this.TopUpUrl%>">here</a>.
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="90%" id="tblNoWallet" runat="server"
                clientidmode="Static">
                <tr>
                    <td class="formHeadTxt">
                        <img src="/app_themes/onDemand/images/icon_excl.gif" alt="Error" style="border-width: 0px;
                            vertical-align: text-top" />
                        You do not currently have a DStv Online pre-paid account.<br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="lbHeadBlue">
                        Please update your online profile to enable this feature.
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">
                        <br />
                        Update your profile <a href="/Account/About.aspx">here</a>.
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;" align="right">
                        <input type="image" id="btnCancel2" src="/app_themes/onDemand/images/btn_cancel.gif"
                            alt="Cancel" onclick="javascript:return HidePaymentDialogue();" style="border-width: 0px;" />
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="90%" id="tblInsufficentFunds"
                runat="server" clientidmode="Static">
                <tr>
                    <td class="formHeadTxt">
                        <img src="/app_themes/onDemand/images/icon_excl.gif" alt="Error" style="border-width: 0px;
                            vertical-align: text-top" />
                        You do not have sufficient credit in you DStv Online pre-paid account.
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="lbHeadBlue">
                        Your current balance is <strong>
                            <asp:Label ID="lblBalanceInsufficientFunds" runat="server" Text="<%# this.BalanceFormatted %>" /></strong>.
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">
                        <br />
                        Top up your pre-paid balance <a href="<%=this.TopUpUrl%>">here</a>.
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;" align="right">
                            <input type="image" id="btnCancel3" src="/app_themes/onDemand/images/btn_cancel.gif"
                                alt="Cancel" onclick="javascript:return HidePaymentDialogue();" style="border-width: 0px;" />
                    </td>
                </tr>
            </table>
        </div>
        <!-- DSTV CONNECT BEGIN -->
    </div>
    <!-- DSTV CONNECT END -->
</div>
