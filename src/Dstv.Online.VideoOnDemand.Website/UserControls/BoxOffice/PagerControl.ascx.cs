﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice
{
    public partial class PagerControl : System.Web.UI.UserControl
    {
        #region Private Members

        /// <summary>
        /// Amount of items in the datagrid
        /// </summary>
        private int amountOfItems = 0;

        /// <summary>
        /// Items per page
        /// </summary>
        private int amountOfItemsPerPage = 5;

        /// <summary>
        /// The current page number
        /// </summary>
        private int pageNumber = 1;

        /// <summary>
        /// The amount of pages in this collection
        /// </summary>
        private int amountOfPages = 0;

        /// <summary>
        /// The maximum amount of pages visible
        /// </summary>
        private int maxAmountOfPagesVisible = 4;

        /// <summary>
        /// Refresh event
        /// </summary>
        public event EventHandler Refresh;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Populate();
        }

        private void Populate()
        {
            amountOfPages = Math.Max((int)Math.Ceiling((float)amountOfItems / (float)amountOfItemsPerPage), 1); // There should be atleast one page

            int bgn = GetStart();
            int amountOfPagesVisible = Math.Min(amountOfPages - (bgn - 1), maxAmountOfPagesVisible);
            int end = bgn + amountOfPagesVisible;

            List<PagerPage> pages = new List<PagerPage>();

            for (int i = bgn; i < end; i++) 
            {
                pages.Add
                    (
                        new PagerPage
                            (
                                i.ToString(), 
                                i.ToString(),
                                (i == pageNumber ? false : true)
                            )
                    );
            }

            repPages.DataSource = pages;
            repPages.DataBind();

            RefreshButtons(bgn, amountOfPages);

            lblPageNumber.Text = pageNumber.ToString();
            lblTotalPages.Text = amountOfPages.ToString();
        }

        private int GetStart()
        {
            int bgn = 1;

            bgn = pageNumber - (maxAmountOfPagesVisible + 1);

            if (bgn < 1)
                bgn = 1;

            if (pageNumber > 1)
                bgn += 1;

            return bgn;
        }

        private void RefreshButtons(int _start, int _amountOfPages)
        {
            int backId = pageNumber - 1;
            int nextId = pageNumber + 1;

            // ---------------------------------------------
            // Prev Page Button
            // ---------------------------------------------
            if (backId >= 1)
            {
                btnBack.CommandArgument = (backId).ToString();
                btnBack.Style.Add("display", "inline");
            }
            else
            {
                btnBack.Style.Add("display", "none");
            }

            // ---------------------------------------------
            // Next Page Button
            // ---------------------------------------------
            if (nextId <= _amountOfPages)
            {
                btnNext.CommandArgument = (nextId).ToString();
                btnNext.Style.Add("display", "inline");
            }
            else
            {
                btnNext.Style.Add("display", "none");
            }

            // ---------------------------------------------
            // First Page Button
            // ---------------------------------------------
            if (pageNumber > 1)
            {
                btnFirst.Style.Add("display", "inline"); 
                lblFirst.Style.Add("display", "inline"); 
            }
            else
            {
                btnFirst.Style.Add("display", "none");
                lblFirst.Style.Add("display", "none"); // Ellipses
            }

            // ---------------------------------------------
            // Last Page Button
            // ---------------------------------------------
            if (_amountOfPages > ((_start - 1) + maxAmountOfPagesVisible))
            {
                btnLast.Text = _amountOfPages.ToString(); btnLast.CommandArgument = _amountOfPages.ToString();

                btnLast.Style.Add("display", "inline");
                lblLast.Style.Add("display", "inline"); 
            }
            else
            {
                btnLast.Style.Add("display", "none");
                lblLast.Style.Add("display", "none"); 
            }
        }

        protected void Pager_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if (btn.CommandArgument != null)
            {
                pageNumber = Convert.ToInt32(btn.CommandArgument);

                // Refresh the actual gridview on
                // the main page/usercontrol
                if (Refresh != null)
                    Refresh(pageNumber, null);

                Populate(); // Repopulate the pager control
            }
        }

        #region Public Members

        /// <summary>
        /// Amount of items in the datagrid
        /// </summary>
        public int AmountOfItems
        {
            get { return amountOfItems;  }
            set { amountOfItems = value; }
        }

        /// <summary>
        /// Amount of items per page
        /// </summary>
        public int AmountOfItemsPerPage
        {
            get { return amountOfItemsPerPage;  }
            set { amountOfItemsPerPage = value; }
        }

        /// <summary>
        /// The current page number 
        /// </summary>
        public int PageNumber
        {
            get { return pageNumber;  }
            set { pageNumber = value; }
        }

        #endregion
    }

    /// <summary>
    /// Represents a single
    /// page inside the pager control.
    /// </summary>
    public class PagerPage
    {
        #region Private Members

        /// <summary>
        /// Page Number
        /// </summary>
        public string PageNumber;

        /// <summary>
        /// Link
        /// </summary>
        public string Link;

        /// <summary>
        /// Enabled / Disabled
        /// </summary>
        public bool Enabled;

        #endregion

        public PagerPage(string _pageNumber, string _link, bool _enabled) {
            PageNumber = _pageNumber; Link = _link; Enabled = _enabled;
        }
    }
}