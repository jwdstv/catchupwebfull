﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using System.Text;
using Dstv.Online.VideoOnDemand.Website.MasterPages;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.Profile
{
	/// <summary>
	/// Allows the user to pay for a rental item.
	/// </summary>
	public partial class PayDialogue : UserControls.Shared.JsRenderingControl
	{

        public const string showLoginDialogueScript = "jQuery(document).ready(function() { ShowLoginDialogue(); });";
        public const string showPayDialogueScript = "jQuery(document).ready(function(){ ShowPaymentDialogue(); });";

		#region Private Members

		/// <summary>
		/// The current rental item cost
		/// </summary>
		private decimal cost = 0;

		/// <summary>
		/// The user's current balance
		/// </summary>
		private decimal balance = 0;

		/// <summary>
		/// The current rental item
		/// </summary>
		private CatalogueItemIdentifier catalogItemId;

		/// <summary>
		/// The current rental item title
		/// </summary>
		private string catalogItemTitle = string.Empty;

		/// <summary>
		/// Should we show the dialog
		/// </summary>
		private bool showDialogOnLoad = false;

		private bool loadViaUrlParameter = false;

		private string urlParameterName = "ShowPayment";

		private string topUpUrl = string.Format("{0}?{1}", SiteConstants.Urls.Constants.ProfileTopUpPage, SiteUtils.Url.InternalRedirector.GetRedirectUrlParamForQueryStringForCurrentRequest());

		private string paymentSuccessfulRedirectUrl = "";

		#endregion


		#region Public Members

	    /// <summary>
	    /// The control which will activate the 
	    /// payment modal dialog
	    /// </summary>
        private string targetControlId = "pnlPay";
		public string TargetControlID
		{
            get
            {
                return targetControlId;
            }
            set
            {
                //targetControlId = value;
            }
		}

        /// <summary>
        /// Hides the control by setting it's Html display attribute
        /// </summary>
        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
            }
        }


		/// <summary>
		/// The current rental item
		/// </summary>
		public CatalogueItemIdentifier CatalogItemId
		{
			get { return catalogItemId; }
			set { catalogItemId = value; }
		}

		/// <summary>
		/// The rental item, represented as a string
		/// </summary>
		public string CatalogItemIdValue
		{
			get { return catalogItemId == null ? "" : catalogItemId.Value; }
			set
			{
				catalogItemId = new CatalogueItemIdentifier(value);
			}
		}

		/// <summary>
		/// The current rental item cost
		/// </summary>
		public decimal CatalogueItemCost
		{
			get { return cost; }
			set { cost = value; }
		}

		public string CatalogueItemCostFormatted
		{
			get { return string.Format("{0:C}", CatalogueItemCost); }
		}

		public string BalanceFormatted
		{
			get { return string.Format("{0:C}", balance); }
		}

		/// <summary>
		/// The current rental item title
		/// </summary>
		public string CatalogueItemTitle
		{
			get { return catalogItemTitle; }
			set { catalogItemTitle = value; }
		}

		/// <summary>
		/// Should we show the pay dialog on
		/// load of the page?
		/// </summary>
		public bool ShowDialogOnLoad
		{
			get { return showDialogOnLoad; }
			set { showDialogOnLoad = value; }
		}

		public bool LoadViaUrlParameter
		{
			get { return loadViaUrlParameter; }
			set { loadViaUrlParameter = value; }
		}

		public string UrlParameterName
		{
			get { return urlParameterName; }
			set { urlParameterName = value; }
		}

		public string TopUpUrl
		{
			get { return topUpUrl; }
			set { topUpUrl = value; }
		}

		public string PaymentSuccessfulRedirectUrl
		{
			get { return paymentSuccessfulRedirectUrl; }
			set { paymentSuccessfulRedirectUrl = value; }
		}

		public string ServiceUrl
		{
			get { return SiteUtils.Url.GetUrlForPaymentService(); }
		}
		#endregion


		#region Page Load Event

		protected void Page_Load ( object sender, EventArgs e )
		{
			//if (Request.QueryString[urlParameterName] != null)
			//{
			//    if (Page.User.Identity.IsAuthenticated)
			//        Page.ClientScript.RegisterStartupScript(this.GetType(), "showPaymentDialogue", showPayDialogueScript, true);
			//    else
			//        Page.ClientScript.RegisterStartupScript(this.GetType(), "showLoginDialogue", showLoginDialogueScript, true);
			//}

            //temp bypass until IBS is fully integrated
            //if (this.Visible)
            //{
            //    if (this.Page.User.Identity.IsAuthenticated)
            //    {
            //        if (loadViaUrlParameter && Request.QueryString[urlParameterName] != null)
            //            showDialogOnLoad = true;

            //        if (showDialogOnLoad)
            //            Page.ClientScript.RegisterStartupScript(this.GetType(), "showPaymentDialogue", showPayDialogueScript, true);

            //        tblPay.Visible = false;
            //        tblNoWallet.Visible = false;
            //        tblInsufficentFunds.Visible = false;

            //        if (DStvUser.UserProfile.HasWallet)
            //        {
            //            balance = DStvUser.UserProfile.WalletBalance.Value;

            //            if (balance >= cost)
            //            {
            //                tblPay.Visible = true;
            //            }
            //            else
            //            {
            //                // Insufficient funds
            //                tblInsufficentFunds.Visible = true;
            //            }
            //        }
            //        else
            //        {
            //            // User doesn't have a wallet
            //            tblNoWallet.Visible = true;
            //        }
            //    }
            //    else
            //    {
            //        if (Request.QueryString[urlParameterName] != null)
            //        {
            //            Page.ClientScript.RegisterStartupScript(this.GetType(), "showLoginDialogue", showLoginDialogueScript, true);
            //        }
            //    }

            //    this.DataBind();
            //}
		}

		#endregion

		
	}

}
