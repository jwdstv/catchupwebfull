﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice
{
    /// <summary>
    /// My Rentals page which displays the user's video rentals, current and expired.
    /// </summary>
	public partial class MyRentalsListAll : System.Web.UI.UserControl
	{

		#region UserId
		private string _userid = string.Empty;
		/// <summary>
		/// The User Id for which the My Rentals list should be displayed.
		/// </summary>
		public string UserId
		{
			get { return _userid; }
			set { _userid = value; }
		}
		#endregion

		#region VideoDetailUrl
		private string _videoDetailUrl = "about:blank";
		/// <summary>
		/// The composite format string URL for redirect when the user clicks on the 
		/// video title or the video thumbnail.
		/// </summary>
		public string VideoDetailUrl
		{
			get { return _videoDetailUrl; }
			set { _videoDetailUrl = value; }
		}
		#endregion


		#region ItemsPerPage
		private int _itemsPerPage = 10;
		/// <summary>
		/// Items per page
		/// </summary>
		public int ItemsPerPage
		{
			get { return _itemsPerPage; }
			set { _itemsPerPage = value; }
		}
		#endregion


		#region TotalItems
		private int _totalItems = 0;
		/// <summary>
		/// 
		/// </summary>
		public int TotalItems
		{
			get { return _totalItems; }
		}
		#endregion


		#region PageNumber
		private int _pageNumber = 1;
		/// <summary>
		/// The current page number
		/// </summary>
		public int PageNumber
		{
			get { return _pageNumber; }
			set { _pageNumber = value; }
		}
		#endregion


		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack)
			{
				if (DstvUser.IsAuthenticated)
				{
					_userid = DstvUser.CustomerId.Value;
					_pageNumber = wucPagingControlTop.SelectedPageNumber;
					Populate();
				}
				else
				{
					this.Visible = false;
				}
			}
		}


		/// <summary>
		/// Populate the control
		/// </summary>
		private void Populate ()
		{
			RentalResult rentalResult = ClassLibrary.DataAccess.Rentals.GetVideoRentalPageListForCurrentUser(_pageNumber, _itemsPerPage);

			if (rentalResult != null && rentalResult.Rentals != null && rentalResult.Rentals.Count > 0)
			{
				_totalItems = rentalResult.TotalResults;

				wucPagingControlTop.NumberOfItems = _totalItems;
				wucPagingControlTop.NumberOfItemsPerPage = _itemsPerPage;
				wucPagingControlTop.PopulateControl(_pageNumber);

				rptRentals.DataSource = rentalResult.Rentals;
				rptRentals.DataBind();
			}
			else
			{
				_totalItems = 0;
			}
		}


		#region Data Bound (Repeater Item) Methods

		public int GetExpiryMinutesForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				if (!videoRental.HasRentalExpired)
				{
					return (int)videoRental.ExpiryDate.Subtract(DateTime.Now).TotalMinutes;
				}
			}

			return 0;
		}

		public string GetExpiryDateForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			// If Video has expired i.e. no longer available then cannot view!!
			if (videoRental != null && !videoRental.HasVideoExpired)
			{
				return SiteUtils.JavaScript.GetNewDateString(videoRental.ExpiryDate, true);
				// TEST: return SiteUtils.JavaScript.GetNewDateString(DateTime.Now.AddMinutes(1), true);
			}

			return "null";
		}

		public bool HasRentalExpiredForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.HasRentalExpired;
			}

			return false;
		}

		public bool HasVideoExpiredForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.HasVideoExpired;
			}

			return false;
		}

		public string GetDateRentedForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.DateOfPurchase.ToString(SiteConstants.Display.Video.DateWithTime);
			}

			return "-";
		}

		public string GetVideoDetailUrlForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return this.VideoDetailUrl.Replace(SiteConstants.FormatIdentifiers.VideoId, videoRental.VideoId);
			}

			return "#";
		}

		public string GetVideoDetailUrlTooltipForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.HasRentalExpired ? "Click to view the video details, with the option of renting again." : "Click to watch the rented video.";
			}

			return "#";
		}

		

		public string GetCostForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return string.Format(SiteConstants.Display.Video.CurrencyFormat, "R", videoRental.Amount);
			}

			return "-";
		}

		private VideoRentalType GetVideoRentalFromObject ( object dataItem )
		{
			if (dataItem != null)
			{
				try
				{
					return (VideoRentalType)dataItem;
				}
				catch
				{
				}
			}

			return null;
		}

		protected void rptRentals_ItemDataBound ( object sender, RepeaterItemEventArgs e )
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				System.Web.UI.HtmlControls.HtmlTableCell tdVideoExpired = GetCellControlForRepeaterItem("tdVideoExpired", e.Item);
				System.Web.UI.HtmlControls.HtmlTableCell tdRentalExpired = GetCellControlForRepeaterItem("tdRentalExpired", e.Item);
				System.Web.UI.HtmlControls.HtmlTableCell tdRentalActive = GetCellControlForRepeaterItem("tdRentalActive", e.Item);

				if (tdVideoExpired != null && tdRentalExpired != null && tdRentalActive != null)
				{
					tdVideoExpired.Visible = false;
					tdRentalExpired.Visible = false;
					tdRentalActive.Visible = false;

					if (HasVideoExpiredForItem(e.Item.DataItem))
					{
						tdVideoExpired.Visible = true;
					}
					else if (HasRentalExpiredForItem(e.Item.DataItem))
					{
						tdRentalExpired.Visible = true;
					}
					else
					{
						tdRentalExpired.Visible = true;
						tdRentalExpired.Style.Add("display", "none");
						tdRentalActive.Visible = true;
					}
				}
			}
		}

		private System.Web.UI.HtmlControls.HtmlTableCell GetCellControlForRepeaterItem ( string tdId, RepeaterItem repeaterItem )
		{
			try
			{
				return (System.Web.UI.HtmlControls.HtmlTableCell)repeaterItem.FindControl(tdId);
			}
			catch
			{
				return null;
			}
		}

		#endregion

	}
}