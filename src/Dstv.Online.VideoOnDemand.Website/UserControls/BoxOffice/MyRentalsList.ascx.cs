﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using SiteData = Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
#endregion

namespace Dstv.Online.VideoOnDemand.Website.UserControls.BoxOffice
{
    /// <summary>
    /// The My Rental List user control (MyRentalList.ascx) displays a list of videos (up to a maximum of ten) 
    /// rented by a user for a given User Id.  The control displays the video title and the 
    /// rental expiry date if viewable (not expired), otherwise it will display ‘expired’.  
    /// The user control has an optional “more” link.
    /// </summary>
	public partial class MyRentalsList : System.Web.UI.UserControl
	{
		#region Private Members

		/// <summary>
		/// The composite format string URL for redirect when the user clicks on the 
		/// video title or the video thumbnail.
		/// </summary>
		private string videoDetailUrl = "about:blank";

		/// <summary>
		/// The composite format string URL for redirection when the user clicks on the “more” link.
		/// {VideoOrder} will be replaced with VideoReleaseDateDesc SortOrder enum value.
		/// </summary>
		private string moreUrl = string.Empty;

		/// <summary>
		/// The User Id for which the My Rentals list should be displayed.
		/// </summary>
		private string userid = string.Empty;

		/// <summary>
		/// The maximum amount of rentals that
		/// can be displayed.
		/// </summary>
		private int maxRentalsInList = 10;

		#endregion


		#region Public Members

		/// <summary>
		/// The composite format string URL for redirect when the user clicks on the 
		/// video title or the video thumbnail.
		/// </summary>
		public string VideoDetailUrl
		{
			get { return videoDetailUrl; }
			set { videoDetailUrl = value; }
		}

		/// <summary>
		/// The composite format string URL for redirection when the user clicks on the “more” link.
		/// {VideoOrder} will be replaced with VideoReleaseDateDesc SortOrder enum value.
		/// </summary>
		public string MoreUrl
		{
			get { return moreUrl; }
			set { moreUrl = value; }
		}

		/// <summary>
		/// The User Id for which the My Rentals list should be displayed.
		/// </summary>
		public string UserId
		{
			get { return userid; }
			set { userid = value; }
		}


		protected string GetVideoDetailUrlForItem ( object dataItem )
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return this.VideoDetailUrl.Replace(SiteConstants.FormatIdentifiers.VideoId, videoRental.VideoId);
			}

			return "#";
		}

        protected string GetVideoDetailUrlTooltipForItem(object dataItem)
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.HasRentalExpired ? "Click to view the video details, with the option of renting again." : "Click to watch the rented video.";
			}

			return "#";
		}

        public int GetExpiryMinutesForItem(object dataItem)
        {
            VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
            if (videoRental != null)
            {
                if (!videoRental.HasRentalExpired)
                {
                    return (int)videoRental.ExpiryDate.Subtract(DateTime.Now).TotalMinutes;
                }
            }

            return 0;
        }

        protected string GetExpiryDateForItem(object dataItem)
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			// If Video has expired i.e. no longer available then cannot view!!
			if (videoRental != null && !videoRental.HasVideoExpired)
			{
				return SiteUtils.JavaScript.GetNewDateString(videoRental.ExpiryDate, true);
				// TEST: return SiteUtils.JavaScript.GetNewDateString(DateTime.Now.AddMinutes(1), true);
			}

			return "null";
		}

        protected bool HasRentalExpiredForItem(object dataItem)
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.HasRentalExpired;
			}

			return false;
		}

        protected bool HasVideoExpiredForItem(object dataItem)
		{
			VideoRentalType videoRental = GetVideoRentalFromObject(dataItem);
			if (videoRental != null)
			{
				return videoRental.HasVideoExpired;
			}

			return false;
		}

		private VideoRentalType GetVideoRentalFromObject ( object dataItem )
		{
			if (dataItem != null)
			{
				try
				{
					return (VideoRentalType)dataItem;
				}
				catch
				{
				}
			}

			return null;
		}


		private int _numberOfItems = 0;
		/// <summary>
		/// 
		/// </summary>
		public int NumberOfItems
		{
			get { return _numberOfItems; }
		}


		private bool _autoLoad = true;
		/// <summary>
		/// 
		/// </summary>
		public bool AutoLoad
		{
			get { return _autoLoad; }
			set { _autoLoad = value; }
		}
		
		#endregion


		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack && _autoLoad)
			{
				PopulateControl();
				this.Visible = _numberOfItems > 0;
			}
		}

		public void PopulateControl ()
		{
			if (Page.User.Identity.IsAuthenticated)
			{
				if (moreUrl != null && moreUrl != string.Empty)
				{
					lnkMore.NavigateUrl = moreUrl;
				}
				else
				{
					lnkMore.Enabled = false;
					lnkMore.Style.Add("cursor", "default"); // Disable the 'hand' icon when you hover over link
				}

				Populate();
			}
		}

		/// <summary>
		/// Populate the control
		/// </summary>
		private void Populate ()
		{
			_numberOfItems = 0;

			List<VideoRentalType> rentalData = GetData();
			if (rentalData.Count > 0)
			{
				_numberOfItems = rentalData.Count;

				repRentalList.DataSource = rentalData;
				repRentalList.DataBind();

				this.Visible = true;
			}
			else
			{
				this.Visible = false;
			}
		}

		private List<VideoRentalType> GetData ()
		{
			RentalResult rentalResult = ClassLibrary.DataAccess.Rentals.GetVideoRentalsListForCurrentUser(maxRentalsInList);
			if (rentalResult != null && rentalResult.Rentals != null && rentalResult.Rentals.Count > 0)
			{
				// Do not show studio/MMS expired videos in rental list.
				return rentalResult.Rentals.FindAll(delegate( VideoRentalType videoRental ) { return !videoRental.HasVideoExpired; });
			}
			else
			{
				return new List<VideoRentalType>();
			}
		}

	}
}
