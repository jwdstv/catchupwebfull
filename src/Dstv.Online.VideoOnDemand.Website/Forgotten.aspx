﻿<%@ Page Language="C#" Title="Forgot Password" AutoEventWireup="true" CodeBehind="Forgotten.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Forgotten1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register src="~/Controls/Forgot.ascx" tagname="Forgot" tagprefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <title>Forgotten Password</title>
    <link href="/App_Themes/Connect/dstvConnect.css" rel="stylesheet" type="text/css" />
    <link href="/App_Themes/onDemand/controls.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>

    <style type="text/css">
        html {
            overflow: auto;
        }   
        
        body {
            background-color: #F7F8FA;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm1" runat="server" />
    <div>
        <uc1:Forgot ID="Forgot2" runat="server" />
    </div>
    </form>
     <script type="text/javascript">

         $(function () {
             override_signupLinkUrl();

             var isInIFrame = (window.location != window.parent.location) ? true : false;

             if (isInIFrame) {
                 window.parent.hideScrollbars();
             }
         });

         function override_signupLinkUrl() {
             $("a[href$='SignUp.aspx']").attr("href", "/closeparent.aspx?RedirectUrl=SignUp.aspx");
         }
        

        
    </script>

    
</body>
</html>
<%--
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
Forgotten Password
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">

<uc1:Forgot ID="Forgot2" runat="server" />


</asp:Content>--%>
