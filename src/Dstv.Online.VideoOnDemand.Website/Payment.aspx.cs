﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;


namespace Dstv.Online.VideoOnDemand.Website
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            ProgramResult programDetail;
            string programVideoId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, null);

            if (!string.IsNullOrWhiteSpace(programVideoId))
            {
                DataContract.ICatalogueItem videoItem;

                programDetail = DataAccess.VideoData.GetVideoDetailsForProgram(programVideoId, DataContract.VideoProgramType.Movie);
                videoItem = programDetail.PrimaryVideoItem.CatalogueItem;

                PinVerification1.assetID = videoItem.AssetId;
                PinVerification1.amount = videoItem.RentalAmount.Value;
                PinVerification1.description = videoItem.VideoTitle;
                PinVerification1.ReturnPath = "CloseParent.aspx?RedirectUrl=" + Server.UrlEncode(GetFormattedUrl(PinVerification1.ReturnPath, videoItem.ProgramId.Value, videoItem.CatalogueItemId.Value));
                PinVerification1.registerWalletLink = "CloseParent.aspx?RedirectUrl=" + Server.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["ConnectProfileLink"]);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        internal string GetFormattedUrl(string url, int programId, string videoId)
        {
            string seoUrl = SiteUtils.Url.SEO.GetSEOFriendlyUrl(url);

            return seoUrl.
                Replace(SiteConstants.FormatIdentifiers.ProgramId, programId.ToString()).
                Replace(SiteConstants.FormatIdentifiers.VideoId, videoId.ToString());
        }
    }
}