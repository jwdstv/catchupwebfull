﻿<%@ Page Title="Notifications" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master" AutoEventWireup="true" CodeBehind="Notification.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Notification" %>
<%@ Register Src="~/Controls/Notify.ascx" TagPrefix="DStv" TagName="Connect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="Scripts/Site.js"></script>
    <script type="text/javascript">
        var transactionType = GetQueryString("t");
        var isInIFrame = (window.location != window.parent.location) ? true : false;
        var exitLB = GetQueryString("exitlb");

        if (isInIFrame && transactionType.toUpperCase() == "RESET" && exitLB == "") {
            var splitChar = window.location.search ? "&" : "?";

            window.parent.showLoadingLightbox(window.location + splitChar + "exitlb=true");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="server">
    <div class="block980_bg">
        <div class="hdrWht24">
            NOTIFICATIONS
        </div>
    </div>
    <div class="block960">
        <DStv:Connect ID="notify" runat="server" />
    </div>
</asp:Content>
