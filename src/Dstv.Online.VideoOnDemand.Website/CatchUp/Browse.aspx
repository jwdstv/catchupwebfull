﻿<%@ Page Title="Browse Videos" Language="C#" MasterPageFile="~/MasterPages/TitleAndVerticalSplitContentWithLeftNav.master"
    AutoEventWireup="true" CodeBehind="Browse.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.CatchUp.Browse" %>

<%@ Register Src="~/UserControls/Video/VideoGallery.ascx" TagName="VideoGallery"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoFilterList.ascx" TagName="VideoFilterList"
    TagPrefix="wuc" %>
    
<asp:Content ID="contentTitle" ContentPlaceHolderID="cphTitle" runat="server">

Browse Video
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">
    <wuc:VideoFilterList ID="wucVifeoFilterList" runat="server" ConfigFile="VideoFilterSettings.xml"
        ViewStateMode="Disabled" ExpandedProductOnLoad="CatchUp" />
</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="cphRightContent" runat="server">
    <div class="block700top">
        <h4 class="home">
            <strong>Catch Up </strong>
            <img src="/app_themes/onDemand/images/arrow_hdr_grey.gif" width="8" height="14" />
            <asp:Literal ID="litVideoCategory" runat="server">{Video Filter Category}</asp:Literal>
        </h4>
        <span class="grey16">(<asp:Literal ID="litVideoCategoryCount" runat="server">{Video Count}</asp:Literal>)</span>
    </div>
    <wuc:VideoGallery ID="wucVideoGallery" runat="server" ViewStateMode="Disabled" ItemsPerPage="16"
        ItemsPerRow="4">
        <VideoFilter ProductType="CatchUp" VideoTypes="Movie,ShowEpisode,ShowClip" />
        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
</wuc:VideoGallery>
<%--<div id="divNoItems" runat="server" visible="false" clientidmode="Static" style="display:inline;position:relative;">
<div class="padding20px">
No videos were found in the selected category. Please select a different category.
</div>
</div>--%>
<br />
<br />
</asp:Content>

