﻿<%@ Page Title="Video Details" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master"
    AutoEventWireup="true" CodeBehind="VideoDetail.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.CatchUp.VideoDetail" %>

<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register Src="~/UserControls/Video/VideoCastAndCrew.ascx" TagName="CastandCrew"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/EditorialVideoList.ascx" TagName="EditorialVideoList"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoEpisodesSummary.ascx" TagName="EpisodesSummary"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoDetailConsole.ascx" TagName="VideoDetailConsole"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Social/FacebookComments.ascx" TagName="FacebookComments"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/RelatedVideoSearch.ascx" TagName="RelatedVideoSearch"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Social/FacebookLike.ascx" TagName="FacebookLike"
    TagPrefix="wuc" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="cphHeadContent" runat="server">
    <script src="../Scripts/swfobject2.2.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="contentTop" ContentPlaceHolderID="cphTopContent" runat="server">
    <!-- Video Detail + Still + Player -->
    <wuc:VideoDetailConsole ID="wucVideoDetailConsole" runat="server" ViewStateMode="Disabled"
        ConsoleDisplayType="Video" VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
        MediaDisplayAction="PlayVideo">
        <pagelinks videodetailurl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            showdetailurl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
    </wuc:VideoDetailConsole>
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">
    <%-- <div class="fbLike floatLeft">
     <%--   <div class="fbReverse">
        <wuc:FacebookLike ID="wucFacebookLike" runat="server">
                <FBSettings FBType="Like" FB_action="Like" FB_layout="Standard" FB_colorScheme="Light"
                    FB_show_Faces="false" FB_width="450" />
        </wuc:FacebookLike>
    </div>
    </div>--%>
    <div id="divFBLike">
    </div>
    <br />
    <br />
    <wuc:CastandCrew ID="wucCastAndCrew" runat="Server" ViewStateMode="Disabled"></wuc:CastandCrew>
    <wuc:EpisodesSummary ID="wucEpisodeVideoSummary" runat="server" ListTitle="Episodes"
        ViewStateMode="Disabled" VideoCount="4" ExcludeVideoIdFromDisplay="true">
        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
    </wuc:EpisodesSummary>
    <h4>
        Comments</h4>
    <div class="greyHdrLine">
    </div>
    <div class="floatLeft">
        <div class="fbReverse">
            <wuc:FacebookComments ID="wucFacebookComments" runat="server">
                <FBSettings FBType="Comment" FB_width="620" FB_height="267" FB_colorScheme="Dark" />
            </wuc:FacebookComments>
        </div>
    </div>
    <div class="padding20px">
    </div>
</asp:Content>
<asp:Content ID="contentRightTop" ContentPlaceHolderID="cphRightContentTop" runat="server">
    <%--<wuc:RelatedVideoSearch ID="wucRelatedVideoSearch" runat="server" 
        DisplayLayout="VerticalSummary" ListTitle="You might also like:" VideoCount="3" 
        ViewStateMode="Disabled">
    <VideoFilter 
        ProductType="CatchUp" 
        SortFilter="LatestReleases" 
        VideoTypes="Movie,ShowEpisode,ShowClip" 
        />
    <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
</wuc:RelatedVideoSearch>--%>
</asp:Content>
<asp:Content ID="contentRightBottom" ContentPlaceHolderID="cphRightContentBottom"
    runat="server">
    <asp:PlaceHolder ID="plcRelatedVideos" runat="server"></asp:PlaceHolder>
</asp:Content>
<asp:Content ID="contentBottom" ContentPlaceHolderID="cphBottom" runat="server">
    <div id="divLazyFBLike" style="display: none;">
        <div id="fb-root">
        </div>
        <script src="http://connect.facebook.net/en_US/all.js#appId=191822184198193&amp;xfbml=1"></script>
        <fb:like href="<%= Dstv.Online.VideoOnDemand.Website.ClassLibrary.FacebookSettings.GetPageFacebookURL() %>"
            send="true" width="450" show_faces="false" colorscheme="dark" font=""></fb:like>
    </div>
    <script type="text/javascript">
        $(function () {
            if (document.getElementById("divFBLike") != null) {
                document.getElementById("divFBLike").appendChild(document.getElementById("divLazyFBLike"));
                document.getElementById("divLazyFBLike").style.display = "";
            }
        });
    </script>
</asp:Content>
