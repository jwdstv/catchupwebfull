﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.CatchUp
{
	public partial class Browse : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack)
			{
			}
            this.wucVifeoFilterList.HeaderFilter = "FILTER VIDEO";
		}

		protected void Page_PreRender ( object sender, EventArgs e )
		{
			litVideoCategory.Text = string.IsNullOrEmpty(wucVideoGallery.VideoGalleryTitle) ? wucVifeoFilterList.SelectedItemName : wucVideoGallery.VideoGalleryTitle;
			if (string.IsNullOrEmpty(litVideoCategory.Text))
			{
				litVideoCategory.Text = "All";
			}

            // Removed by Jodi Adams in order to make the control function correclty.
            //if (wucVideoGallery.TotalItems > 0)
            //{
            //    wucVideoGallery.Visible = true;
            //    divNoItems.Visible = false;
            //}
            //else
            //{
            //    wucVideoGallery.Visible = false;
            //    divNoItems.Visible = true;
            //}

			litVideoCategoryCount.Text = wucVideoGallery.TotalItems.ToString();
		}
	}
}