﻿<%@ Page Title="Find Out More" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master"
    AutoEventWireup="true" CodeBehind="FindOutMore.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.CatchUp.FindOutMore" %>

<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register Src="~/UserControls/Video/EditorialVideoList.ascx" TagName="EditorialVideoList"
    TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTopContent" runat="server">

    <div class="vidPlay_bg">
        <img src="/app_themes/onDemand/images/catchUpinfo.jpg" alt="Sign up for CatchUp"
            width="980" height="242" border="0" usemap="#Map" />
        <map name="Map" id="Map">
            <area shape="rect" coords="276,41,569,99" alt="Sign up to CatchUp"
                href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx" />
        </map>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftContent" runat="server">

    <!--[if lt IE 8]><style type="text/css">.basList li{padding-bottom:15px}</style><![endif]-->
  	<div class="floatLeft">
        <h4>What is CatchUp?</h4>
        <div class="greyHdrLine"></div>
		<div class="floatLeft">
                <ul class="basList">
                    <li>Missed your favourite show or the big match? No problem. CatchUp lets you watch what you missed over the past 7 days.</li><br/>
                    <li>This cutting-edge service is exclusive to DStv Premium subscribers in South Africa.</li><br/>                    
                </ul>
            </div>
            <div class="floatLeft"><h4> How do I get CatchUp?</h4>
                <div class="greyHdrLine"></div>
                <div class="floatLeft"><strong>There are two ways to access CatchUp:<br /><br /></strong></div>
                <ul class="basList">
                
                    <li>On your PVR decoder – Simply select the “DStv On Demand” menu.</li><br/>
                    <li>On your computer – Either download the shows or watch them online.</li><br/>                    
                </ul>
            </div>
       
      
    </div>
  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphRightContentTop" runat="server">
    <div class="boSignup">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td class="boSignupTxt">
                    New to CatchUp?
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx?product=catchup">
                        <img src="/app_themes/onDemand/images/btn_signUp-CU.gif" width="105" height="39"
                            align="absmiddle" alt="Sign Up" /></a>
                </td>
            </tr>
        </table>
    </div>
    <div class="padding15px">
    </div>
    <wuc:EditorialVideoList ID="wucEditorialVideoList" runat="server" ListId="2"
        ViewStateMode="Disabled">
        <VideoFilter ProductType="CatchUp" VideoCount="10" SortFilter="SearchResultOrder" VideoTypes="Movie,ShowEpisode" />
        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            MoreUrl="/CatchUp/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}" />
    </wuc:EditorialVideoList>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphRightContentBottom" runat="server">
</asp:Content>
