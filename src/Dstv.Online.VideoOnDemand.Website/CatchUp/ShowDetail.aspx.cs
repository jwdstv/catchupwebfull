﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.MasterPages;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.CatchUp
{
	public partial class ShowDetail : System.Web.UI.Page
	{
		private ProgramResult _programDetail = null;

		private string _programVideoId;

		private DataContract.ICatalogueItem _videoItem
		{
			get { return _programDetail != null && _programDetail.PrimaryVideoItem != null ? _programDetail.PrimaryVideoItem.CatalogueItem : null; }
		}

		public string VideoMetaKeywords
		{
			get { return _videoItem == null ? "" : HttpUtility.HtmlEncode(string.Format("{0},{1}", wucShowDetailConsole.VideoTitle, _videoItem.Keywords)); }
		}

		public string VideoMetaDescription
		{
			get { return _videoItem == null ? "Watch videos online." : HttpUtility.HtmlEncode(string.Format("Watch videos online. Show: {0} Detail: {1}", wucShowDetailConsole.VideoTitle, wucShowDetailConsole.VideoAbstractTrimmed)); }
		}

		protected void Page_Load ( object sender, EventArgs e )
		{
			Master.DisplayMyRentalsList = false;

			_programDetail = null;

			_programVideoId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, null);

			if (!string.IsNullOrEmpty(_programVideoId))
			{
				_programDetail = DataAccess.VideoData.GetVideoDetailsForProgram(_programVideoId, DataContract.VideoProgramType.Unknown);

				if (_programDetail != null)
				{
					_programDetail.SetPrimaryVideo(_programVideoId);
					
                    // Test data.
					//wucRelatedVideoSearch.SearchTerm = "Series"; // _programDetail.ProgramTitle;
				}

				wucShowDetailConsole.ProgramDetail = _programDetail;
				wucEpisodeVideoSummary.VideoProgram = _programDetail;
			}
		}

		protected void Page_PreRender ( object sender, EventArgs e )
		{
			Title = string.Format("Show: {0}", wucShowDetailConsole.VideoTitle);

			MetaKeywords = HttpUtility.HtmlEncode(
				string.Format("{0},{1}", wucShowDetailConsole.VideoTitle, _videoItem.Keywords)
				);

			MetaDescription = HttpUtility.HtmlEncode(
				string.Format("Watch videos online. Show: {0} Detail: {1}", wucShowDetailConsole.VideoTitle, wucShowDetailConsole.VideoAbstractTrimmed)
				);

			if (_programDetail == null)
			{
				// wucVideoDetailConsole.Visible = false;
				wucEpisodeVideoSummary.Visible = true;
				wucCastAndCrew.Visible = false;
				//wucRelatedVideoSearch.Visible = false;
			}
			else
			{
				wucCastAndCrew.PopulateControl(_programDetail.CastAndCrew);

				if (_programDetail.HasEpisodes)
				{
					wucEpisodeVideoSummary.VideoProgram = _programDetail;
					wucEpisodeVideoSummary.VideoId = _programVideoId;
					// wucEpisodeVideoSummary.Visible = true;
				}
				else
				{
					wucEpisodeVideoSummary.Visible = false;
				}

				wucShowDetailConsole.Visible = true;
			}

            //Set FacebookComments
            SetFacebook();
            LoadRelatedVids();
		}

        private void LoadRelatedVids()
        {
            if (this.wucShowDetailConsole.VideoItem != null)
            {

                UserControls.Video.RelatedVideoSearch uc = (UserControls.Video.RelatedVideoSearch)Page.LoadControl("~/UserControls/Video/RelatedVideoSearch.ascx");
                uc.SearchTerm = this.wucShowDetailConsole.VideoItem.Keywords;
                uc.DisplayLayout = SiteEnums.VideoListingDisplay.VerticalSummary;
                uc.ListTitle = "You might also like:";
                uc.VideoCount = 3;
                uc.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
                uc.VideoItem = this.wucShowDetailConsole.VideoItem;
                uc.ExcludedProgramName = _programDetail.ProgramTitle;
                uc.VideoExclusionList = string.Join(",", _programDetail.VideoItems.Select<VideoDetailItem, string>(v => v.VideoId).ToArray());

                /*<PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />*/

                Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoControlBasePageLinks links = new UserControls.Video.VideoControlBasePageLinks();
                links.VideoDetailUrl = "/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}";
                links.ShowDetailUrl = "/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}";

                uc.PageLinks = links;

                VideoFilter videoFilter = new VideoFilter();

                videoFilter.VideoTypes = string.Join(",", new VideoProgramType[] { VideoProgramType.FeatureFilm, VideoProgramType.Movie, VideoProgramType.ShowEpisode });
                videoFilter.SortFilter = SiteEnums.VideoSortOrder.None;
                videoFilter.ProductType = this.wucShowDetailConsole.VideoItem.ProductTypeId.Value;

                uc.VideoFilter = videoFilter;

                plcRelatedVideos.Controls.Add(uc);
            }


        }

        #region set Facebook
        /// <summary>
        /// sets facebook settings and adds facebook meta tags for the video
        /// </summary>
        private void SetFacebook()
        {
            FacebookSettings fbSettings = new FacebookSettings();


            if (wucShowDetailConsole.VideoProductType == ProductTypes.BoxOffice || wucShowDetailConsole.VideoItem.VideoProgramType == DataContract.VideoProgramType.Movie)
            {
                fbSettings.OG_type = FacebookSettings.FacebookOGTypes.Movie;
            }
            else
            {
                fbSettings.OG_type = FacebookSettings.FacebookOGTypes.Tv_Show;
            }

            fbSettings.OG_title = wucShowDetailConsole.VideoTitle;
            fbSettings.OG_site_name = string.Format("On Demand {0}", ClassLibrary.FlashVideo.Util.ConfigValues.CategoryValue);
            fbSettings.OG_image = wucShowDetailConsole.VideoThumbnailImgUrl;
            fbSettings.FB_colorScheme = FacebookSettings.FacebookColorscheme.Dark;

            fbSettings.UpdateMetaTagWithOGValues(this.Page);
        }
        #endregion
    }
}