﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master"
    AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.CatchUp.Home" %>

<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register Src="~/UserControls/Video/VideoSummary.ascx" TagName="VideoSummary"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/EditorialVideoList.ascx" TagName="EditorialVideoList"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Social/FacebookRecentActivity.ascx" TagName="FaceBookActivity"
    TagPrefix="fb" %>
<%@ Register Src="~/UserControls/Billboard/Billboard.ascx" TagName="BillBoard" TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Advert/SmallBanner.ascx" TagName="Advert" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTopContent" runat="server">
    <!-- CMS Content -->
    <div class="block980_bg-CU">
        <div class="dstvflash">
            <script src="../Scripts/swfobject2.2.js" type="text/javascript"></script>
            <wuc:BillBoard ID="CatchUpBillBoard" runat="server" FontColor="2AABE4" />
        </div>
        <div class="promo-CU">
            <div class="promo_hdr">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td width="45">
                                <img height="40" width="40" src="/app_themes/onDemand/images/lrgArrow-catchUp.png">
                            </td>
                            <td>
                                <img alt="CatchUp" src="/app_themes/onDemand/images/hdr-catchUp.png">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="iefix_h9">
                Watch your favourite TV shows, sport and movies online whenever you want!<br />
            </div>
            <div class="promoList-CU">
                <ul>
                    <li>Exclusive to DStv Premium subscribers</li>
                    <li>200+ hours of TV shows, sport &amp; movies every month</li>
                </ul>
            </div>
            <div class="floatLeft">
                <table cellspacing="4" cellpadding="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td colspan="3">
                                <img height="3" width="5" alt=" " src="/app_themes/onDemand/images/transparent.gif">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>CatchUp/FindOutMore.aspx">
                                    <img height="39" width="145" alt="FIND OUT MORE" src="/app_themes/onDemand/images/btn_findOutMore-CU.gif"></a>
                            </td>
                            <% if (!User.Identity.IsAuthenticated)// Check if user is logged in or not
                               { %>
                            <td class="catchUp">
                                <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx?product=catchup">
                                    Sign up now</a>
                                <br />
                                It's quick &amp easy
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <%} %>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftContent" runat="server">
    <div class="block620vidrow">
        <wuc:VideoSummary ID="wucVideoSummary1" runat="server" IsEditorial="false" ListTitle="Latest Releases"
            IncludeFullAbstractTooltip="True" ViewStateMode="Disabled">
            <VideoFilter ProductType="CatchUp" VideoCount="4" SortFilter="LatestReleases" VideoTypes="Movie,ShowEpisode" />
            <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                MoreUrl="/CatchUp/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}" />
        </wuc:VideoSummary>
    </div>
    <div class="padding10px">
    </div>
    <div class="block620vidrow">
        <wuc:VideoSummary ID="wucVideoSummary2" runat="server" IsEditorial="false" ListTitle="Most Popular"
            IncludeFullAbstractTooltip="True" ViewStateMode="Disabled">
            <VideoFilter ProductType="CatchUp" VideoCount="4" SortFilter="MostPopular" VideoTypes="Movie,ShowEpisode" />
            <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                MoreUrl="/CatchUp/Browse.aspx?SortFilter={SortFilter}" />
        </wuc:VideoSummary>
    </div>
    <div class="padding10px">
    </div>
    <div class="block620vidrow">
        <wuc:VideoSummary ID="wucVideoSummary3" runat="server" IsEditorial="false" ListTitle="Last Chance"
            IncludeFullAbstractTooltip="True" ViewStateMode="Disabled">
            <VideoFilter ProductType="CatchUp" VideoCount="4" SortFilter="LastChance" VideoTypes="Movie,ShowEpisode" />
            <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
                MoreUrl="/CatchUp/Browse.aspx?SortFilter={SortFilter}" />
        </wuc:VideoSummary>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphRightContentTop" runat="server">
    <wuc:EditorialVideoList ID="wucEditorialVideoList" runat="server" ListId="1" ViewStateMode="Disabled">
        <VideoFilter ProductType="CatchUp" VideoCount="10" SortFilter="SearchResultOrder"
            VideoTypes="Movie,ShowEpisode" />
        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            MoreUrl="/CatchUp/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}" />
    </wuc:EditorialVideoList>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphRightContentBottom" runat="server">
    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
    <fb:activity site="ondemand.dstv.com/Catchup" width="300" height="450" header="true" colorscheme="dark"
        font="" border_color="656565" recommendations="true">
    </fb:activity>
</asp:Content>
