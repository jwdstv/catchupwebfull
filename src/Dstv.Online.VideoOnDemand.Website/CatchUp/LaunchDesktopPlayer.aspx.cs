﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Modules.Rental;

namespace Dstv.Online.VideoOnDemand.Website.CatchUp
{
	public partial class LaunchDesktopPlayer : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!DstvUser.IsAuthenticated)
			{
				throw new UnauthorizedAccessException();
			}

			// Retrieve Video Id and Desktop Player Url from query string
			string videoId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, "");
			if (!string.IsNullOrEmpty(videoId))
			{
				string desktopPlayerUrl = SiteUtils.Url.ExternalRedirector.GetRedirectUrlFromQueryString(SiteConstants.Urls.ParameterNames.DesktopPlayerUrl);
				if (string.IsNullOrEmpty(desktopPlayerUrl))
				{
					throw new ArgumentException(SiteConstants.Urls.ParameterNames.DesktopPlayerUrl);
				}

				// Entitlement ??
				litVideoId.Text = videoId;
				litDesktopPlayerUrl.Text = desktopPlayerUrl;
			}
			else
			{
				throw new ArgumentException(SiteConstants.Urls.ParameterNames.VideoId);
			}
		}
	}
}