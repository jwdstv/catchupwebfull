﻿<%@ Page Title="Show Details" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master" AutoEventWireup="true" CodeBehind="ShowDetail.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.CatchUp.ShowDetail" %>
<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register src="~/UserControls/Video/EditorialVideoList.ascx" tagname="EditorialVideoList" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Social/FacebookComments.ascx" tagname="FacebookComments" tagprefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoDetailConsole.ascx" TagName="VideoDetailConsole" TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoEpisodesSummary.ascx" TagName="EpisodesSummary" TagPrefix="wuc" %>
<%@ Register src="~/UserControls/Video/VideoCastAndCrew.ascx" tagname="CastandCrew" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Video/RelatedVideoSearch.ascx" tagname="RelatedVideoSearch" tagprefix="wuc" %>
<%@ Register Src="~/UserControls/Social/FacebookLike.ascx" TagName="FacebookLike" TagPrefix="wuc" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="cphHeadContent" runat="server">
    <link href="/app_themes/onDemand/shows.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="contentTop" ContentPlaceHolderID="cphTopContent" runat="server">

    <wuc:VideoDetailConsole ID="wucShowDetailConsole" runat="server" 
        ConsoleDisplayType="Show"
        >
        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
    </wuc:VideoDetailConsole>
    

</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">

    <div class="floatLeft">
        <div id="divFBLike"></div>
    </div>

    <wuc:CastAndCrew id="wucCastAndCrew" runat="Server" ViewStateMode="Disabled"></wuc:CastAndCrew>

    <wuc:EpisodesSummary ID="wucEpisodeVideoSummary" runat="server" ListTitle="Episodes" ViewStateMode="Disabled"
        VideoCount="4" ExcludeVideoIdFromDisplay="true">
        <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
    </wuc:EpisodesSummary>

   
    <div class="padding20px"></div>
    
    
    <div class="padding20px"></div>
    <h4>Comments</h4>
    <div class="greyHdrLine"></div>
    <div class="floatLeft">
        <wuc:FacebookComments id="wucFacebookComments" runat="server">
            <FBSettings FBType="Comment" FB_width="541" FB_height="267" />
        </wuc:FacebookComments>
    </div>
</asp:Content>
<asp:Content ID="contentRightTop" ContentPlaceHolderID="cphRightContentTop" runat="server">

</asp:Content>
<asp:Content ID="contentRightBottom" ContentPlaceHolderID="cphRightContentBottom" runat="server">

<%--<wuc:RelatedVideoSearch ID="wucRelatedVideoSearch" runat="server" SearchTerm="Series" DisplayLayout="VerticalSummary" ListTitle="You might also like:" VideoCount="3" ViewStateMode="Disabled">
    <VideoFilter 
        ProductType="CatchUp" 
        SortFilter="LatestReleases" 
        VideoTypes="Movie,ShowEpisode,ShowClip" 
        />
    <PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
</wuc:RelatedVideoSearch>--%>
<asp:PlaceHolder ID="plcRelatedVideos" runat="server"></asp:PlaceHolder>

</asp:Content>

<asp:Content ID="contentBottom" ContentPlaceHolderID="cphBottom" runat="server">
    <div id="divLazyFBLike" style="display: none;">
        <div id="fb-root">
        </div>
        <script src="http://connect.facebook.net/en_US/all.js#appId=191822184198193&amp;xfbml=1"></script>
        <fb:like href="<%= Dstv.Online.VideoOnDemand.Website.ClassLibrary.FacebookSettings.GetPageFacebookURL() %>"
            send="true" width="450" show_faces="false" colorscheme="dark" font=""></fb:like>
    </div>
    <script type="text/javascript">
        $(function () {
            if (document.getElementById("divFBLike") != null) {
                document.getElementById("divFBLike").appendChild(document.getElementById("divLazyFBLike"));
                document.getElementById("divLazyFBLike").style.display = "";
            }
        });
    </script>
</asp:Content>
