﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.MasterPages;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using System.Web.Services;
using System.Web.Script.Services;

namespace Dstv.Online.VideoOnDemand.Website.CatchUp
{
    public partial class VideoDetail : System.Web.UI.Page
    {
        private ProgramResult _programDetail = null;

        private string _programVideoId;

        /// <summary>
        /// Defines whether the user is logged in
        /// </summary>
        public bool UserLoggedIn;

        /// <summary>
        /// Defines whether the user has a ISP linked
        /// </summary>
        public bool ISPLinked;

        /// <summary>
        /// Defines whether the user has a Smartcard linked
        /// </summary>
        public bool SmartcardLinked;

        private DataContract.ICatalogueItem _videoItem
        {
            get { return _programDetail != null && _programDetail.PrimaryVideoItem != null ? _programDetail.PrimaryVideoItem.CatalogueItem : null; }
        }

        public string VideoMetaKeywords
        {
            get { return _videoItem == null ? "" : HttpUtility.HtmlEncode(string.Format("{0},{1}", wucVideoDetailConsole.VideoTitle, _videoItem.Keywords)); }
        }

        public string VideoMetaDescription
        {
            get { return _videoItem == null ? "Watch videos online." : HttpUtility.HtmlEncode(string.Format("Watch videos online. Video: {0} Detail: {1}", wucVideoDetailConsole.VideoTitle, wucVideoDetailConsole.VideoAbstractTrimmed)); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (CheckAuthentication())
            //    if (CheckISPLink())
            //        CheckSmartCardLink();
            //First check if the user is logged in, then check if both the smartcard and ISP is linked

            Master.DisplayMyRentalsList = false;

            _programDetail = null;

            _programVideoId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, null);

            if (!string.IsNullOrEmpty(_programVideoId))
            {
                _programDetail = DataAccess.VideoData.GetVideoDetailsForProgram(_programVideoId, DataContract.VideoProgramType.Unknown);

                if (_programDetail != null)
                {
                    _programDetail.SetPrimaryVideo(_programVideoId);

                    string videoDetailAction = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoDetailAction, null);

                    if (!string.IsNullOrEmpty(videoDetailAction))
                    {
                        string[] videoDetailActionSplit = videoDetailAction.Split(',');
                        if (videoDetailActionSplit.Length == 2)
                        {
                            if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.PlayMovie, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.PlayVideo;
                            }
                            else if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.DownloadMovie, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.DownloadVideo;
                            }
                            else if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.PlayTrailer, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.PlayTrailer;
                            }
                        }
                    }

                    wucVideoDetailConsole.ProgramDetail = _programDetail;

                    if (_programDetail.HasEpisodes)
                    {
                        wucEpisodeVideoSummary.VideoProgram = _programDetail;
                        wucEpisodeVideoSummary.VideoId = _programVideoId;
                    }
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Title = string.Format("Video: {0}", wucVideoDetailConsole.VideoTitle);

            if (_videoItem != null)
            {
                MetaKeywords = HttpUtility.HtmlEncode(
                    string.Format("{0},{1}", wucVideoDetailConsole.VideoTitle, _videoItem.Keywords)
                    );
            }
            else
            {
                MetaKeywords = HttpUtility.HtmlEncode(
                    string.Format("{0},{1}", wucVideoDetailConsole.VideoTitle, "")
                    );
                //added check for null to avoid exception being thrown
            }

            MetaDescription = HttpUtility.HtmlEncode(
                string.Format("Watch videos online. Video: {0} Detail: {1}", wucVideoDetailConsole.VideoTitle, wucVideoDetailConsole.VideoAbstractTrimmed)
                );

            if (_programDetail == null)
            {
                // wucVideoDetailConsole.Visible = false;
                wucEpisodeVideoSummary.Visible = true;
                wucCastAndCrew.Visible = false;
            }
            else
            {
                wucCastAndCrew.PopulateControl(_programDetail.CastAndCrew);

                wucVideoDetailConsole.Visible = true;
            }

            SetFacebook();

            LoadRelatedVids();

        }

        #region set Facebook
        /// <summary>
        /// sets facebook settings and adds facebook meta tags for the video
        /// </summary>
        private void SetFacebook()
        {
            FacebookSettings fbSettings = new FacebookSettings();

            if (wucVideoDetailConsole.VideoProductType == ProductTypes.BoxOffice || wucVideoDetailConsole.VideoItem.VideoProgramType == DataContract.VideoProgramType.Movie)
            {
                fbSettings.OG_type = FacebookSettings.FacebookOGTypes.Movie;
            }
            else
            {
                fbSettings.OG_type = FacebookSettings.FacebookOGTypes.Tv_Show;
            }

            fbSettings.OG_title = wucVideoDetailConsole.VideoTitle;
            fbSettings.OG_site_name = string.Format("On Demand {0}", ClassLibrary.FlashVideo.Util.ConfigValues.CategoryValue);
            fbSettings.OG_image = wucVideoDetailConsole.VideoThumbnailImgUrl;

            fbSettings.UpdateMetaTagWithOGValues(this.Page);
        }
        #endregion

        private void LoadRelatedVids()
        {
            if (this.wucVideoDetailConsole.VideoItem != null)
            {

                UserControls.Video.RelatedVideoSearch uc = (UserControls.Video.RelatedVideoSearch)Page.LoadControl("~/UserControls/Video/RelatedVideoSearch.ascx");
                uc.SearchTerm = this.wucVideoDetailConsole.VideoItem.Keywords;
                uc.DisplayLayout = SiteEnums.VideoListingDisplay.VerticalSummary;
                uc.ListTitle = "You might also like:";
                uc.VideoCount = 3;
                uc.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
                uc.VideoItem = this.wucVideoDetailConsole.VideoItem;
                uc.ExcludedProgramName = _programDetail.ProgramTitle;
                uc.VideoExclusionList = string.Join(",", _programDetail.VideoItems.Select<VideoDetailItem, string>(v => v.VideoId).ToArray());

                /*<PageLinks VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />*/

                Dstv.Online.VideoOnDemand.Website.UserControls.Video.VideoControlBasePageLinks links = new UserControls.Video.VideoControlBasePageLinks();
                links.VideoDetailUrl = "/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}";
                links.ShowDetailUrl = "/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}";

                uc.PageLinks = links;

                VideoFilter videoFilter = new VideoFilter();

                videoFilter.VideoTypes = string.Join(",", new VideoProgramType[] { VideoProgramType.FeatureFilm, VideoProgramType.Movie, VideoProgramType.ShowEpisode });
                videoFilter.SortFilter = SiteEnums.VideoSortOrder.None;
                videoFilter.ProductType = this.wucVideoDetailConsole.VideoItem.ProductTypeId.Value;


                //if (videoFilter.ProductType.ToString() == ClassLibrary.SiteEnums.ProductType.BoxOffice.ToString())
                //    videoFilter.VideoTypes = VideoProgramType.Movie.ToString();

                uc.VideoFilter = videoFilter;

                plcRelatedVideos.Controls.Add(uc);
            }


        }

        public string GetSearchTerm()
        {
            string term = "Series";
            if (this.wucVideoDetailConsole.VideoItem != null)
            {
                term = this.wucVideoDetailConsole.VideoItem.Keywords;
            }

            return term;

        }

        [WebMethod()]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static void InsertPlayHit(string videoId)
        {
            IntegrationBroker.Instance.Catalogue.InsertCatalogueItemPlayHit(int.Parse(videoId));
        }
    }
}