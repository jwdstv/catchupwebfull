﻿#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using System.Configuration;

#endregion

namespace System.Web.UI
{
    public class MasterPageBase : MasterPage
    {
        #region Public Properties and Methods

        // Public properties and methods that nested Master Pages and Content Pages can call.
        // NOTE: all properties for all master pages are being included in the base, primarily for ease of reference.
        // Content pages that want to access the properties in a typed manner can add the following page directive,
        // <%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>

        public string SiteRoot = "";
        public string SiteHomePage = "";

        private SiteEnums.ProductType _productType = SiteEnums.ProductType.OnDemand;
        /// <summary>
        /// 
        /// </summary>
        public SiteEnums.ProductType ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }

        private bool _displayMyRentalsList = (ConfigurationManager.AppSettings["HideBoxOffice"] ?? "false").Equals(bool.FalseString);
        /// <summary>
        /// 
        /// </summary>
        public bool DisplayMyRentalsList
        {
            get { return _displayMyRentalsList; }
            set { _displayMyRentalsList = value; }
        }

        private bool _displayAdvert = true;
        /// <summary>
        /// 
        /// </summary>
        public bool DisplayAdvert
        {
            get { return _displayAdvert; }
            set { _displayAdvert = value; }
        }

        #region EnableNavigation
        private bool _enableNavigation = true;
        /// <summary>
        /// 
        /// </summary>
        public bool EnableNavigation
        {
            get { return _enableNavigation; }
            set { _enableNavigation = value; }
        }
        #endregion


        #region PageRequiresAuthentication
        private bool _pageRequiresAuthentication = false;
        /// <summary>
        /// 
        /// </summary>
        public bool PageRequiresAuthentication
        {
            get { return _pageRequiresAuthentication; }
            set { _pageRequiresAuthentication = value; }
        }
        #endregion


        #region WebAppRootPath
        /// <summary>
        /// 
        /// </summary>
        public string WebAppRootPath
        {
            get { return SiteUtils.WebAppRootPath; }
        }
        #endregion


        #endregion


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);


            try
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["SiteRoot"]))
                {
                    SiteRoot = System.Configuration.ConfigurationManager.AppSettings["SiteRoot"];
                }

            }
            catch
            {
                SiteRoot = "";
            }
            try
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["SiteHomePage"]))
                {
                    SiteHomePage = System.Configuration.ConfigurationManager.AppSettings["SiteHomePage"];
                }

            }
            catch
            {
                SiteHomePage = "";
            }

            if (!Page.IsPostBack)
            {
                // Check source/user territory (Cache in session??)
                SiteEnums.ProductType currentProductType = SiteUtils.Request.GetProductTypeFromRequest();
                string sessionKey = string.Format("Is{0}Supported", currentProductType.ToString());
                bool userInSupportedTerritory;

                if (Session[sessionKey] == null)
                {
                    userInSupportedTerritory = WebsiteSettings.SupportedTerritories.IsUserInSupportedTerritory(currentProductType);
                }
                else
                {
                    userInSupportedTerritory = bool.Parse(Session[sessionKey].ToString());
                }

                if (!userInSupportedTerritory && !WebsiteSettings.SupportedBrowsers.IsCrawler)
                {
                    if (Request.RawUrl.IndexOf(SiteUtils.GetAbsoluteUrlPath(SiteConstants.Urls.Constants.SupportedCountriesPage), StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        //If the user tried to access the browse page (BoxOffice by default) they might still be able to access the CatchUp browse page
                        //If they however were on the CatchUp browse page redirect to the unsupported country page when they try to access the BoxOffice browse page
                        if (currentProductType == SiteEnums.ProductType.BoxOffice
                        && Request.Url.AbsolutePath.EndsWith(SiteConstants.Urls.Constants.BrowsePageName, StringComparison.OrdinalIgnoreCase)
                        && (Request.UrlReferrer == null || !(currentProductType == SiteEnums.ProductType.CatchUp && Request.Url.AbsolutePath.EndsWith(SiteConstants.Urls.Constants.BrowsePageName, StringComparison.OrdinalIgnoreCase))))
                        {
                            Response.Redirect("~/CatchUp/Browse.aspx", true);
                        }
                        else
                        {
                            Response.Redirect(SiteConstants.Urls.Constants.SupportedCountriesPage, true);
                        }
                    }
                }
                
            }
        }

    }
}