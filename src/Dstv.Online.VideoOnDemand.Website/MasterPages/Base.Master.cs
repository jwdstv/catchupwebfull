﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand.Globalization;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.MasterPages
{
	public partial class Base : MasterPageBase
	{

		protected void Page_Load ( object sender, EventArgs e )
		{
			wucMultiChoiceFooter.DisabledForOnDemand = !EnableNavigation;

			// Only do stats in production (Release Build).
            //if (HttpContext.Current.IsDebuggingEnabled)
            //{
            //            wucSiteStats.Visible = false;
            //}
            //else
            //{
            //            wucSiteStats.Visible = false;
            //}

            wucSiteStats.Visible = true;
		}


		protected void Page_PreRender ( object sender, EventArgs e )
		{
			divDisableNav.Visible = !EnableNavigation;
			headContainer.Disabled = !EnableNavigation;
		}


		

	}
}