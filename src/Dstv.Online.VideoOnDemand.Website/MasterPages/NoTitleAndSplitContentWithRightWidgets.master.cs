﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.MasterPages
{
	public partial class NoTitleAndSplitContentWithRightWidgets : MasterPageBase
	{

		protected void Page_Load ( object sender, EventArgs e )
		{
			// Set product type on Editorial List.
			if (Page.User.Identity.IsAuthenticated && DisplayMyRentalsList)
			{
				wucMyRentalsList.PopulateControl();
			}
		}

		protected void Page_PreRender ( object sender, EventArgs e )
		{
			wucMyRentalsList.Visible = DisplayMyRentalsList && wucMyRentalsList.NumberOfItems > 0;
			wucSmallBanner.Visible = DisplayAdvert;
		}
	}
}