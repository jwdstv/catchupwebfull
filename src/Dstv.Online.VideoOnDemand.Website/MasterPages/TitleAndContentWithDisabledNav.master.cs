﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.MasterPages
{
	public partial class TitleAndContentWithDisabledNav : MasterPageBase
	{
		protected void Page_Init ( object sender, EventArgs e )
		{
			// EnableNavigation = false;
			((Base)Master).EnableNavigation = false;
		}

		protected void Page_Load ( object sender, EventArgs e )
		{

		}
	}
}