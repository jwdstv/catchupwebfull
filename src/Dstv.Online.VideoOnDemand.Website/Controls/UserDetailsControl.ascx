﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetailsControl.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.UserDetailsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<span class="ConnectContainer">
    <asp:Panel ID="panleUserDetails" runat="server">
    <div runat="server" id="divIBSCare">
    To update your billing details <a runat="server" id="lnkIBSCare" href="http://care.dstv.com" target="_blank">click here</a><br /><br />
    </div>
        <div class="connectfloatLeft">
            
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">
                        <strong>Title</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:DropDownList runat="server" ID="ddlSalutations">
                                            <asp:ListItem Text="Please Select" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                                            <asp:ListItem Text="Mnr" Value="Mnr"></asp:ListItem>
                                            <asp:ListItem Text="Mnre" Value="Mnre"></asp:ListItem>
                                            <asp:ListItem Text="Miss" Value="Miss"></asp:ListItem>
                                            <asp:ListItem Text="Mrs" Value="Mrs"></asp:ListItem>
                                            <asp:ListItem Text="Mej" Value="Mej"></asp:ListItem>
                                            <asp:ListItem Text="Mev" Value="Mev"></asp:ListItem>
                                            <asp:ListItem Text="Capt" Value="Capt"></asp:ListItem>
                                            <asp:ListItem Text="Dr" Value="Dr"></asp:ListItem>
                                            <asp:ListItem Text="Gen" Value="Gen"></asp:ListItem>
                                            <asp:ListItem Text="Frau" Value="Frau"></asp:ListItem>
                                            <asp:ListItem Text="Herr" Value="Herr"></asp:ListItem>
                                            <asp:ListItem Text="Hon" Value="Hon"></asp:ListItem>
                                            <asp:ListItem Text="HRH" Value="HRH"></asp:ListItem>
                                            <asp:ListItem Text="Maj" Value="Maj"></asp:ListItem>
                                            <asp:ListItem Text="Msrs" Value="Msrs"></asp:ListItem>
                                            <asp:ListItem Text="Prof" Value="Prof"></asp:ListItem>
                                            <asp:ListItem Text="Rev" Value="Rev"></asp:ListItem>
                                            <asp:ListItem Text="Sir" Value="Sir"></asp:ListItem>
                                            <asp:ListItem Text="Ms" Value="Ms"></asp:ListItem>
                                            <asp:ListItem Text="Comp" Value="Comp"></asp:ListItem>
                                            <asp:ListItem Text="Lord" Value="Lord"></asp:ListItem>
                                            <asp:ListItem Text="Kol" Value="Kol"></asp:ListItem>
                                            <asp:ListItem Text="Ds" Value="Ds"></asp:ListItem>
                                            <asp:ListItem Text="Adv" Value="Adv"></asp:ListItem>
                                            <asp:ListItem Text="LT Genl" Value="LT Genl"></asp:ListItem>
                                            <asp:ListItem Text="Kapt" Value="Kapt"></asp:ListItem>
                                            <asp:ListItem Text="Brig" Value="Brig"></asp:ListItem>
                                            <asp:ListItem Text="Supt." Value="Supt."></asp:ListItem>
                                            <asp:ListItem Text="Past." Value="Past."></asp:ListItem>
                                            <asp:ListItem Text="Fr." Value="Fr."></asp:ListItem>
                                            <asp:ListItem Text="COL" Value="COL"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvSalutations" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="ddlSalutations">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.TitleRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong>Initials</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtInitials" MaxLength="10"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvInitials" ValidationGroup="1" Display="Dynamic"
                                        CssClass="connectformError" ForeColor="" ControlToValidate="txtInitials"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.InitialsRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="2">
                        <strong>First name</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtFirstName" MaxLength="50"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvFirstName" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtFirstName"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.FirstNameRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="2">
                        <strong>Last name</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtLastName" MaxLength="50"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvLastName" ValidationGroup="1" Display="Dynamic"
                                        CssClass="connectformError" ForeColor="" ControlToValidate="txtLastName"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.LastNameRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong>Residential Address</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtResAddress1"></asp:TextBox>
                                        <asp:TextBoxWatermarkExtender runat="server" ID="watermarkStreet" TargetControlID="txtResAddress1"
                                            WatermarkText="Street">
                                        </asp:TextBoxWatermarkExtender>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvResAddress1" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtResAddress1"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.ResidentialAddressRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtResAddress2"></asp:TextBox>
                                        <asp:TextBoxWatermarkExtender runat="server" ID="watermarkSuburb" TargetControlID="txtResAddress2"
                                            WatermarkText="Suburb">
                                        </asp:TextBoxWatermarkExtender>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvSubUrb" ValidationGroup="1" Display="Dynamic"
                                        CssClass="connectformError" ForeColor="" ControlToValidate="txtResAddress2">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.SuburbRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtResAddress3" MaxLength="4" onkeypress="return numbersOnly(event);"></asp:TextBox>
                                        <asp:TextBoxWatermarkExtender runat="server" ID="txtPostalCode" TargetControlID="txtResAddress3"
                                            WatermarkText="Postal Code">
                                        </asp:TextBoxWatermarkExtender>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvResPin" ValidationGroup="1" Display="Dynamic"
                                        CssClass="connectformError" ForeColor="" ControlToValidate="txtResAddress3">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.PostalCodeRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regExResPostalCode" runat="server" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtResAddress3"
                                        ValidationExpression="^\d{1,4}$">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.PostalCodeValidity%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:DropDownList runat="server" ID="ddlResProvience">
                                            <asp:ListItem Text="Please Select Province" Value=""></asp:ListItem>
                                            <asp:ListItem Text="None" Value="0" />
                                            <asp:ListItem Text="EasternCape" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="FreeState" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Gauteng" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="KwazuluNatal" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Lesotho" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="Mpumalanga" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="NorthernCape" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="Limpopo" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="NorthWest" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="WesternCape" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="Transkei" Value="11"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvResProvince" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="ddlResProvience">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.ProvinceRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong>Postal Address</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtBillAddress1"></asp:TextBox>
                                        <asp:TextBoxWatermarkExtender runat="server" ID="watermarkPostalStreet" TargetControlID="txtBillAddress1"
                                            WatermarkText="Street/PO Box">
                                        </asp:TextBoxWatermarkExtender>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvBillAddress1" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtBillAddress1"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.BillingAddressRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtBillAddress2"></asp:TextBox>
                                        <asp:TextBoxWatermarkExtender runat="server" ID="watermarkPostalSuburb" TargetControlID="txtBillAddress2"
                                            WatermarkText="Suburb">
                                        </asp:TextBoxWatermarkExtender>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvBilSuburb" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtBillAddress2">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.SuburbRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:TextBox runat="server" ID="txtBillAddress3" MaxLength="4" onkeypress="return numbersOnly(event);"></asp:TextBox>
                                        <asp:TextBoxWatermarkExtender runat="server" ID="watermarkPostalPin" TargetControlID="txtBillAddress3"
                                            WatermarkText="Postal Code">
                                        </asp:TextBoxWatermarkExtender>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvBilPin" ValidationGroup="1" Display="Dynamic"
                                        CssClass="connectformError" ForeColor="" ControlToValidate="txtBillAddress3">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.PostalCodeRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regExPostalCode" runat="server" ValidationGroup="1"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtBillAddress3"
                                        ValidationExpression="^\d{1,4}$">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.PostalCodeValidity%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes">
                                        <asp:DropDownList runat="server" ID="ddlBillProvince">
                                            <asp:ListItem Text="Please Select Province" Value=""></asp:ListItem>
                                            <asp:ListItem Text="None" Value="0" />
                                            <asp:ListItem Text="EasternCape" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="FreeState" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Gauteng" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="KwazuluNatal" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Lesotho" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="Mpumalanga" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="NorthernCape" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="Limpopo" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="NorthWest" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="WesternCape" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="Transkei" Value="11"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <span class="connectreqField">*</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="connectfieldMsgs">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvBillPro" ValidationGroup="1" Display="Dynamic"
                                        CssClass="connectformError" ForeColor="" ControlToValidate="ddlBillProvince">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.ProvinceRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div>
                                        <asp:RadioButtonList ID="rdoUniqueId" runat="server" RepeatDirection="Horizontal"
                                            CellPadding="5" CellSpacing="4" RepeatLayout="Table" OnSelectedIndexChanged="rdoUniqueId_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="ID Number" Value="IDNumber" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Passport Number" Value="PassportNumber"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <asp:MultiView runat="server" ID="multiviewIdentity" ActiveViewIndex="0">
                    <asp:View runat="server" ID="view1">
                        <tr>
                            <td colspan="2">
                                <strong>ID Number</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="connectinputBoxes">
                                                <asp:TextBox runat="server" ID="txtIDNumber"></asp:TextBox>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvIDNumber" ValidationGroup="1" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="txtIDNumber"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.IDNumberRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="customIDNumcheck" runat="server" ValidationGroup="1" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" OnServerValidate="saID_ServerValidate">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.IDNumberValidation%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </asp:View>
                    <asp:View runat="server" ID="view2">
                        <tr>
                            <td colspan="2">
                                <strong>Passport Number</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="connectinputBoxes">
                                                <asp:TextBox runat="server" ID="txtPassportNumber"></asp:TextBox>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvPassportNumber" ValidationGroup="1"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtPassportNumber"> 
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.PassportRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Country of Origin</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="connectinputBoxes">
                                                <asp:DropDownList runat="server" ID="ddlCountryOfOrigin" AppendDataBoundItems="true">
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvCountry" ValidationGroup="" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="ddlCountryOfOrigin">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                        <td colspan="2"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.CountryOfOriginRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Expiry Date</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="connectinputBoxes1">
                                                <asp:TextBox runat="server" ID="txtExpiryDate" MaxLength="10"></asp:TextBox>
                                                <asp:CalendarExtender runat="server" ID="calenderExpiry" Format="dd/MM/yyyy" TargetControlID="txtExpiryDate"
                                                    PopupButtonID="imageButtonShowCalender">
                                                </asp:CalendarExtender>
                                                <asp:TextBoxWatermarkExtender runat="server" ID="waterMarkExpiryDate" TargetControlID="txtExpiryDate"
                                                    WatermarkText="DD/MM/YYYY">
                                                </asp:TextBoxWatermarkExtender>
                                            </div>
                                            <asp:ImageButton ID="imageButtonShowCalender" runat="server" Width="15px" Height="20px"
                                                AlternateText="Click to enter date" />
                                        </td>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvExpiryDate" ValidationGroup="1"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtExpiryDate">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                        <td colspan="2"> <div style="clear:both;padding-top:5px;"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                            <td>&nbsp;</td>
                                                            <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.ExpiryDateRequired%></div></td>
                                                        </tr>
                                                        </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="regExExpiryDate" ValidationGroup="1"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtExpiryDate"
                                                ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[2][0]\d{2})$|^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[2][0]\d{2}\s([0-1]\d|[2][0-3])\:[0-5]\d\:[0-5]\d)$">
                                <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2"> <div style="clear:both;padding-top:5px;"></div></td>
                                </tr>
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.ExpiryDateValidation%></div></td>
                                </tr>
                                </table>
                                            </asp:RegularExpressionValidator>
                                            <asp:CompareValidator runat="server" ID="compareExpiryDate" ValidationGroup="1" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="txtExpiryDate" Operator="GreaterThan"
                                                Type="Date" ValueToCompare="<%# DateTime.Today.ToShortDateString() %>">
                                <table border="0" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td colspan="2"> <div style="clear:both;padding-top:5px;"></div></td>
                                </tr>
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.validationMessages.UserDetailsControl.PassportExpireValidation%></div></td>
                                </tr>
                                </table>
                                            </asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </asp:View>
                    <asp:View ID="view3" runat="server">
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </asp:View>
                </asp:MultiView>
                <tr>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnErrorAtIBS" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</span>