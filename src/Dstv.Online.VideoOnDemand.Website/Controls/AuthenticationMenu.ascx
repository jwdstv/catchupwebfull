﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthenticationMenu.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.AuthenticationMenu" %>
<%@ Register Src="~/Controls/CountryLightBoxs.ascx" TagName="LightboxCountry" TagPrefix="DStvConnect" %>
<span class="ConnectContainer">
    <asp:Panel ID="NotLoggedInPanel" runat="server" Visible="true">
        <table border="0" cellpadding="1" cellspacing="0">
            <tr>
                <td>
                   <asp:HyperLink ID="LoginHyperLink" runat="server" CssClass="connectsmlLink" Text="Login"
                        NavigateUrl="~/Login.aspx" />
                    <span class="connectblueSep">|</span>
                    <asp:HyperLink ID="ForgotHyperLink" runat="server" CssClass="connectsmlLink" Text="Forgot Password?"
                        NavigateUrl="~/Forgotten.aspx" />
                    <span class="connectblueSep">|</span>
                    <asp:HyperLink ID="RegisterHyperLink" runat="server" CssClass="connectsmlLink" Text="Sign Up"
                        NavigateUrl="~/SignUp.aspx" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="LoggedInPanel" runat="server" Visible="false">
    
        <table border="0" cellpadding="1" cellspacing="0">
            <tr>
                <td>
                    You are logged in as
                    <asp:Label ID="NameLabel" runat="server" CssClass="connectuserName" />
                    <span class="connectblueSep">|</span>
                    <asp:HyperLink ID="MyProfileHyperLink" runat="server" CssClass="connectsmlLink" Text="My Profile"
                        NavigateUrl="~/Account/About.aspx" />
                    <span class="connectblueSep">|</span>
                    <asp:HyperLink ID="LogoutHyperLink" runat="server" CssClass="connectsmlLink" Text="Logout"
                        NavigateUrl="~/Logout.aspx" />


            
                        



                    <% if (System.Configuration.ConfigurationManager.AppSettings["showWalletBalanceTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showWalletBalanceTab"] == "true")
                       {%>
                           <script type="text/javascript">
                               $(function () {
                                   $.ajax({
                                       url: "/Controls/AsyncHandler.ashx",
                                       data: { method: "getUpdatedWalletBalance", refreshRate: 15 },
                                       timeout: 12000,
                                       cache: false,
                                       success: function (data) {
                                           $("#userBalance").html(data).css({ "display": "none" });
                                           $("#userBalance").fadeIn("slow");
                                       },
                                       error: function (XMLHttpRequest, textStatus, errorThrown) {
                                           //$("#userBalance").html("<span style='color:red'>" + textStatus + "</span>").css({ "display": "none" });
                                           $("#userBalance").fadeOut("slow");
                                       }
                                   });
                               });
        </script>
                    <span class="connectblueSep">|</span> <span id="userBalance" class="connectsml">&nbsp;
                        Loading Wallet&nbsp;<img alt="" style="height: 10px; width: 10px;" src="<%=this.fullImagePath%>ajax-loader.gif" /></span>
                    <%} %>
                </td>
            </tr>
        </table>
        <asp:PlaceHolder ID="countryContainer" runat="server">
            <DStvConnect:LightboxCountry runat="server" ID="lightBoxCountryControl" />
        </asp:PlaceHolder>
    </asp:Panel>
  <asp:Literal runat="server" ID="SSOLit" Visible="false"/>
</span>
