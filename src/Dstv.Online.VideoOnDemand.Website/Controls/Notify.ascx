﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notify.ascx.cs" Inherits="DStvConnect.WebUI.Controls.Notify" %>
<span class="ConnectContainer">
    <div class="connectform">
        <div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <label title="DStv Connect">
                            <img alt="DStv Connect" src="<%=this.ConnectImagePath%>dstvconnect.png" /></label>
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td>
                        <img alt="" src="<%=this.ConnectImagePath%>pr_headsep.gif" />
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectlbformHeadTxt">
                        DStv Connect is enriching your online experience.
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectformItems">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <div class="connectformThnx" style="padding-left: 0px; padding-right: 0px;">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td>
                                        <%=this.DisplayMessage%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectlbformRight">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="connectlbHead">
                        About DStv Connect
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-weight: normal">
                            DStv Connect is your portal to getting so much more online.</p>
                        <br />
                        <p style="font-weight: normal">
                            Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="RegisterHyperLink" runat="server" Text="Don't have a DStv Connect ID?"
                            NavigateUrl="~/SignUp.aspx" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label title="DStv Connect Availability">
                            <img alt="DStv Connect Availability" src="<%=this.ConnectImagePath%>logossml.gif" /></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</span>