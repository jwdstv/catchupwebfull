﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PinVerification.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.PinVerification" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panelPinVerifivcation" DefaultButton="btnPayWatch">
        <div class="connectform">
            <div class="connectformHead">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td width="20">
                        </td>
                        <td>
                            <img src="<%=this.connectImagePath%>dstvconnect.png" alt="" border="0" />
                        </td>
                        <td width="15">
                        </td>
                        <td>
                            <img src="<%=this.connectImagePath%>pr_headsep.gif" alt="" border="0" />
                        </td>
                        <td width="15">
                        </td>
                        <td class="connectlbformHeadTxt">
                            Video Rental Payment Details
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectlbformItemsPAY">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <div>
                                <asp:Label Text="" ID="lblUpdateSuccess" runat="server" CssClass="connectformError" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="connectlbHeadBlue">This video costs <span class="connectgreenTXT">R
                                <%=this.amount%>
                            </span>to rent and watch.<br />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <asp:MultiView ID="multiViewPin" runat="server" ActiveViewIndex="0">
                                    <asp:View runat="server" ID="viewNothinng">
                                    </asp:View>
                                    <asp:View runat="server" ID="view0">
                                        <tr>
                                            <td>
                                                <br />
                                                <span class="connectlbHeadBlue">Your pre-paid balance, currently <strong>R
                                                    <%=this.userCreditBalance%></strong>, will be debited with this amount.<br />
                                                    <br />
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="connectformHeadTxt">
                                                Please enter your pre-paid PIN to rent and watch this video online.
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>PIN</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="connectinputBoxes1">
                                                    <asp:TextBox runat="server" ID="txtPin1" TextMode="Password" ClientIDMode="Static"
                                                        MaxLength="1" size="1" onkeydown="return numbersOnly(event);" onkeyup="if(document.getElementById('txtPin1').value.length >= 1) { document.getElementById('txtPin2').focus();}"
                                                        EnableViewState="false"></asp:TextBox>&nbsp;&nbsp;
                                                    <asp:TextBox runat="server" ID="txtPin2" TextMode="Password" ClientIDMode="Static"
                                                        MaxLength="1" size="1" onkeydown="return numbersOnly(event);" onkeyup="if(document.getElementById('txtPin2').value.length >= 1) { document.getElementById('txtPin3').focus();}"
                                                        EnableViewState="false"></asp:TextBox>&nbsp;&nbsp;
                                                    <asp:TextBox runat="server" ID="txtPin3" TextMode="Password" ClientIDMode="Static"
                                                        MaxLength="1" size="1" onkeydown="return numbersOnly(event);" onkeyup="if(document.getElementById('txtPin3').value.length >= 1) { document.getElementById('txtPin4').focus();}"
                                                        EnableViewState="false"></asp:TextBox>&nbsp;&nbsp;
                                                    <asp:TextBox runat="server" ID="txtPin4" TextMode="Password" ClientIDMode="Static"
                                                        MaxLength="1" size="1" onkeydown="return numbersOnly(event);"></asp:TextBox>&nbsp;&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator runat="server" ID="custPinValidation" ValidationGroup="101"
                                                    Display="Dynamic" CssClass="connectformError" ForeColor="" OnServerValidate="Pin_ServerValidate">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.validationMessages.PinVerification.PinRequired%></div></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="connectpadding5px">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <br />
                                                <asp:ImageButton runat="server" ID="btnPayWatch" OnClick="btnPayWatch_Click" ValidationGroup="101" />
                                                <asp:ImageButton runat="server" ID="btnCancel" OnClick="btnCancel_Click" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <asp:View runat="server" ID="view1">
                                        <tr>
                                            <td>
                                                <br />
                                                You do not currently have a MultiChoice pre-paid account. Please update your online
                                                profile to enable this feature.<br />
                                                Update your profile <a href="<%=this.registerWalletLink%>">here</a>.<br />
                                                <br />
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <asp:View runat="server" ID="view2">
                                        <tr>
                                            <td>
                                                <br />
                                                You do not have sufficient credit in your MultiChoice pre-paid account. Your current
                                                balance is <strong>R
                                                    <%=this.userCreditBalance%>.</strong><br />
                                                <br />
                                            </td>
                                        </tr>
                                        <% if (System.Configuration.ConfigurationManager.AppSettings["walletTopUpLink"] != null)
                                           {
                                               walletLink = !String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["walletTopUpLink"]) ? Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["walletTopUpLink"]) : "#";%>
                                        <tr>
                                            <td>
                                                Top up your pre-paid balance <a href="<%=this.walletLink %>">here</a>.
                                            </td>
                                        </tr>
                                        <%} %>
                                    </asp:View>
                                    <asp:View runat="server" ID="view3">
                                        <tr>
                                            <td>
                                                <br />
                                                Please link your Smart card and try again.
                                                <br />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</span>