﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationMenu.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.NavigationMenu" %>
<div class="ConnectContainer">
    <asp:Panel runat="server" ID="panelNaviation">
        <script type="text/javascript">
            function openWallet() {
                var v1 = "<%=System.Configuration.ConfigurationManager.AppSettings["walletURL"] != null ? Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["walletURL"]) : ""%>";

                if(v1 != null && v1 != "")
                {
                    document.getElementById('contentContainer').innerHTML = "<iframe height='700px' width='550px' frameborder='1' width src='"+v1+"'/>";
                }
            }
        </script>
        <div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <img alt="DStv Connect" src="<%=this.fullImagePath%>dstvconnect.png" border="0" />
                        &nbsp;&nbsp;
                    </td>
                    <% if (System.Configuration.ConfigurationManager.AppSettings["showMyProfileTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showMyProfileTab"] == "true")
                       {%>
                    <td>
                        <img src="<%=this.fullImagePath%>pr_headsep.gif" alt="" border="0" />
                    </td>
                    <td>
                        &nbsp;<a href="../Account/About.aspx" class="connectformHeadTxtNavigation">About Me</a>&nbsp;
                    </td>
                    <%} %>
                    <% if (System.Configuration.ConfigurationManager.AppSettings["showNewsLetterTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showNewsLetterTab"] == "true")
                       {%>
                    <td>
                        <img src="<%=this.fullImagePath%>pr_headsep.gif" alt="" border="0" />
                    </td>
                    <td>
                        &nbsp;<a href="../Account/Newsletters.aspx" class="connectformHeadTxtNavigation">My NewsLetters</a>&nbsp;
                    </td>
                    <%} %>
                    <% if (System.Configuration.ConfigurationManager.AppSettings["showMyAccountTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showMyAccountTab"] == "true")
                       {%>
                    <td>
                        <img src="<%=this.fullImagePath%>pr_headsep.gif" alt="" border="0" />
                    </td>
                    <td>
                        <%if (System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] == "true")
                          {%>
                        &nbsp;<a href="../MyAccounts/BoxOffice.aspx" class="connectformHeadTxtNavigation">My
                            Account</a>
                        <%} %>
                        <%else if (System.Configuration.ConfigurationManager.AppSettings["showCatchUpTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showCatchUpTab"] == "true")
                            { %>
                        &nbsp;<a href="../MyAccounts/CatchUp.aspx" class="connectformHeadTxtNavigation">My Account</a>
                        <%} %>
                        <%else if (System.Configuration.ConfigurationManager.AppSettings["showOMusicTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showOMusicTab"] == "true")
                            { %>
                        &nbsp;<a href="../MyAccounts/OMusic.aspx" class="connectformHeadTxtNavigation">My Accounts</a>
                        <%} %>
                        <%else
                            { %>
                        &nbsp;<a href="#" class="connectformHeadTxtNavigation">My Account</a>
                        <%} %>
                    </td>
                    <%} %>
                    <% if (System.Configuration.ConfigurationManager.AppSettings["showMyWalletTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showMyWalletTab"] == "true"
                           && (Session["async_balance"] != null && !Session["async_balance"].ToString().ToLower().Contains("register")) //Check if User as wallet
                           )
                       {%>
                    <td>
                        <img src="<%=this.fullImagePath%>pr_headsep.gif" alt="" border="0" />
                    </td>
                    <td>
                        &nbsp;<a href="javascript:openWallet();" class="connectformHeadTxtNavigation">My Wallet</a>
                    </td>
                    <%} %>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</div>