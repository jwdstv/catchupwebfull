﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.SessionState;
using ConnectRemoteImpl;
using ConnectRemoteImpl.Exceptions;
using DStvConnect.WebUI.Platform;
using DStvConnectBase.Newsletters;
using DstvoConnectWrapper;

namespace DStvConnect.WebUI.Controls
{
    /// <summary>
    /// Summary description for AsyncHandler
    /// </summary>
    public class AsyncHandler : IHttpHandler, IRequiresSessionState
    {


        private HttpContext context;

        private ConnectWrapper _connectWrapper;
        public ConnectWrapper wrapper
        {
            get
            {
                return _connectWrapper ?? (_connectWrapper = new ConnectWrapper());
            }
        }



        private String _connectID;
        public String connectID
        {
            get
            {
                try
                {
                    DStvPrincipal principal = DStvPrincipal.CurrentHttpPrincipal;
                    return _connectID ?? (_connectID = principal.DStvConnectID);
                }
                catch
                { return null; }
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            //System.Threading.Thread.Sleep(6000);
            this.context = context;
            context.Response.ContentType = "text/plain";

            if (context.Request.QueryString["method"] == "getUpdatedWalletBalance")
            {
                context.Response.Write(getUpdatedWalletBalance());

            }
            else if (context.Request.QueryString["method"] == "NewsLetterSubscription")
            {
                context.Response.Write(NewsLetterSubscription());

            }
            else if (context.Request.QueryString["CaptchaText"] != null)
            {
                string captchaCode = Convert.ToString(context.Request.QueryString["CaptchaText"]);
                context.Response.Write(GetCaptchVerificationCode(captchaCode));
            }


            else if (context.Request.QueryString["method"] == "ResolveToken" && !String.IsNullOrEmpty(context.Request.QueryString["authToken"])) {
                try {
                    Guid tokenID = Guid.Parse(context.Request.QueryString["authtoken"].ToString());
                    ReplyObject<String> reply = HttpGlobal.ConnectWrapper.tokenRedeem(tokenID, context.Request.Url.OriginalString, HttpContext.Current.Request.UserHostAddress);
                    //TODO READ JSON - GET CONNECTID
                    ClientUserDetails clientUserDetails = HttpGlobal.ConnectWrapper.RequestUserDetails(reply.FunctionResult.ToString());
                    AutoLogin.Login(clientUserDetails, reply.FunctionResult.ToString());
                    context.Response.Write("success");

                }
                catch (Exception e) {
                    context.Session.Clear();
                    context.Session.Abandon();
                }
            }



        }


        private String getUpdatedWalletBalance()
        {
            String balance = "";
            //Initialize the refreshRate from client request
            int refreshRate = 10;
            if (context.Request.QueryString["refreshRate"] != null)
                int.TryParse(context.Request.QueryString["refreshRate"].ToString(), out refreshRate);
            //==============================================

            try
            {
                if (context.Session["async_lastWalletUpdate"] == null || (DateTime.Parse(context.Session["async_lastWalletUpdate"].ToString()).AddSeconds(refreshRate) < DateTime.Now))
                {
                    balance = "Wallet Balance <span style='color:green'>R " + wrapper.GetWalletBalance(connectID).ToString() + "</span>";
                    context.Session["async_lastWalletUpdate"] = DateTime.Now;
                }
                else return context.Session["async_balance"].ToString();
            }
            catch (GeneralServiceException e)
            {
                if (e.ErrorCode == Resource.MSG_Service_Wallet_Profile_Not_Created_Error)
                    balance = "<span style='color:red'>Wallet does NOT EXIST!!!!!</span>";
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Profile Not Found"))
                {
                    context.Session["async_lastWalletUpdate"] = DateTime.Now;
                    balance = "<a class='connectsmlLink' href='" + VirtualPathUtility.ToAbsolute("~/MyAccounts/BoxOffice.aspx") + "'>Register for Wallet</a>";
                }
                else
                {


                    //throw e;
                    balance = "";
                    //balance = "<span style='color:red'>" + e.Message + "</span>";
                    //balance = "<span style='color:red'>Error</span>";
                }
            }
            context.Session["async_balance"] = balance;
            return balance;
        }

        private String NewsLetterSubscription()
        {
            int newsLetterId = 0;
            bool action = false;
            string email = string.Empty;
            string countryCode = string.Empty;

            try
            {
                if (!String.IsNullOrEmpty(connectID))
                {
                    ClientUserDetails user = HttpGlobal.ConnectWrapper.RequestUserDetails(connectID);
                    email = user.clientUserRegisterItem.EMailAddress;
                }



                if (context.Request.QueryString["newsLetterId"] != null)
                {
                    int.TryParse(context.Request.QueryString["newsLetterId"].ToString(), out newsLetterId);
                }

                if (context.Request.QueryString["action"] != null)
                {
                    bool.TryParse(context.Request.QueryString["action"].ToString(), out action);
                }

                if (context.Request.QueryString["countryCode"] != null)
                {
                    countryCode = Convert.ToString(context.Request.QueryString["countryCode"]);
                }

                ReplyObject<Boolean> replyObject = null;
                //INewsletterSubscription newsletterSubscription = new NewsletterSubscription();
                //newsletterSubscription.NewsletterId = newsLetterId;
                //newsletterSubscription.Email = email;
                //newsletterSubscription.ConnectID = connectID;
                //newsletterSubscription.CountryCode = countryCode;
                //newsletterSubscription.doSubscribe = action;
                replyObject = HttpGlobal.ConnectWrapper.subscribeToNewsletter(connectID, countryCode, action, email, newsLetterId);

                if (replyObject.IsSuccess == false)
                {
                    throw (new ApplicationException());
                }
            }
            catch (GeneralServiceException ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                throw e;
            }

            return string.Empty;

        }

        private string GetCaptchVerificationCode(string captchaCode_in)
        {
            if (captchaCode_in != null)
            {
                //Added By Chris 20110310 do decrypt string before display - do reverse of encryption
                // Server.UrlEncode(Convert.ToBase64String(SecurityHelper.EncryptString(Captcha)))
                //String captchaCode_decoded = HttpUtility.UrlDecode(captchaCode_in,System.Text.Encoding.UTF8).Replace(" ",""); //URLDECODE
                String captchaCode_decoded = HttpUtility.UrlDecode(captchaCode_in, System.Text.Encoding.UTF8); //URLDECODE
                //added by Ajay on 10-Mar-2011
                //while doing UrlDecode it replaces '+' with space, so we need to have same character set back so replace space with '+'
                //to have same number of characters
                captchaCode_decoded = captchaCode_decoded.Replace(" ", "+");
                byte[] bArr = Convert.FromBase64String(captchaCode_decoded); //Convert to Byte[]
                String captchaCode = SecurityHelper.DecryptString(bArr); //Decrypt
                //==================

                List<Letter> letter = new List<Letter>();
                int TotalWidth = 0;
                int MaxHeight = 0;
                foreach (char c in captchaCode)
                {
                    var ltr = new Letter(c);
                    letter.Add(ltr);
                    int space = (new Random()).Next(5) + 1;
                    ltr.space = space;
                    System.Threading.Thread.Sleep(1);
                    TotalWidth += ltr.LetterSize.Width + space;
                    if (MaxHeight < ltr.LetterSize.Height)
                        MaxHeight = ltr.LetterSize.Height;
                    System.Threading.Thread.Sleep(1);
                }
                const int HMargin = 5;
                const int VMargin = 3;

                Bitmap bmp = new Bitmap(TotalWidth + HMargin, MaxHeight + VMargin);
                var Grph = Graphics.FromImage(bmp);
                Grph.FillRectangle(new SolidBrush(Color.Lavender), 0, 0, bmp.Width, bmp.Height);
                Pixelate(ref bmp);
                Grph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                Grph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                int xPos = HMargin;
                foreach (var ltr in letter)
                {
                    Grph.DrawString(ltr.letter.ToString(), ltr.font, new SolidBrush(Color.Navy), xPos, VMargin);
                    xPos += ltr.LetterSize.Width + ltr.space;
                }

                bmp.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            return string.Empty;
        }

        private void Pixelate(ref Bitmap bmp)
        {
            Color[] Colors = { Color.Gray, Color.Red, Color.Blue, Color.Olive };
            for (int i = 0; i < 200; i++)
            {
                Random rnd = new Random(DateTime.Now.Millisecond);
                Graphics grp = Graphics.FromImage(bmp);
                Image background = Image.FromFile(HttpContext.Current.Server.MapPath("../app_themes/connect/images/captcha2.jpg"));
                grp.DrawImage(background, new Rectangle(0, 0, bmp.Width, bmp.Height));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }




    }
}