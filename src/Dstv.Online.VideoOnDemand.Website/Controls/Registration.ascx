﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.Registration" %>
<%@ Register Src="~/Controls/CaptchaControl.ascx" TagName="captchaControl" TagPrefix="DStvConnect" %>
<span class="ConnectContainer">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="RegisterImageButton">
        <div class="connectform">
            <div class="connectformHead">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td>
                            <label title="DStv Connect">
                                <img alt="DStv Connect" src="<%=this.ConnectImagePath%>dstvconnect.png" /></label>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td>
                            <img alt="" src="<%=this.ConnectImagePath%>pr_headsep.gif" />
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectformHeadTxt">
                            Register now and use your DStv Connect details to log in to all your favourite DStv
                            websites.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectformItems">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td colspan="2" class="connectHeadBlue">
                                    Complete the form to register and don’t worry, we won’t share your details with
                                    anyone or flood you with messages.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding10px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <strong>Username</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="connectformDesc">
                                                Your unique username to be used on all DStv.com web sites
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 251px;">
                                    <div class="connectinputBoxes">
                                        <asp:TextBox ID="UsernameTextBox" MaxLength="25" runat="server" ValidationGroup="104" /></div>
                                </td>
                                <td class="connectreqField">
                                    <span>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="CheckAvailableImageButton" runat="server" CausesValidation="true"
                                                    ValidationGroup="104" OnClick="CheckAvailability_Click" OnClientClick="if(document.getElementById('UsernameAvailLabel') != null) { document.getElementById('UsernameAvailLabel').innerHTML='';} if(Page_ClientValidate('104')){ document.getElementById('imgProcessingAvailable').style.display = 'block'; }" />
                                            </td>
                                            <td align="right">
                                                <img runat="server" enableviewstate="false" id="imgProcessingAvailable" clientidmode="Static"
                                                    src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none; margin-left: 20px"
                                                    alt="" />
                                            </td>
                                            <td class="connectreqField">
                                                &nbsp;<span id="userValidSpan" runat="server"><asp:Label ClientIDMode="Static" ID="UsernameAvailLabel"
                                                    runat="server" /></span>
                                                <asp:HiddenField ID="IsAvailableField" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding5px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                        ValidationGroup="104" CssClass="connectformError" ForeColor="" ControlToValidate="UsernameTextBox">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError">
                                                <div>
                                                    <%=this.ValidationMessages.Register.UsernameRequired%>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="104"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="UsernameTextBox"
                                        ValidationExpression="^[a-zA-Z0-9-]*$">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.UsernameValid%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>                                            
                                    </asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="regExUserNameLength" runat="server" ValidationGroup="104"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="UsernameTextBox"
                                        ValidationExpression="^.{6,25}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.ValidationMessages.Register.UsernameLengthValid%></div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="connectpadding5px"></div></td>
                                            </tr>
                                        </table>
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding5px">
                                    </div>
                                </td>
                            </tr>
                            <!-- Added by Ujjaval -->
                            <% if (!this.IsUserNameAvailable)
                               {%>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <strong>Suggested Usernames</strong>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 251px;">
                                    <div class="connectinputBoxes">
                                        <asp:ListBox ID="SuggestedUsernamesListBox" Rows="5" runat="server" OnSelectedIndexChanged="SuggestedUsernamesListBox_SelectedIndexChanged"
                                            AutoPostBack="true"></asp:ListBox>
                                    </div>
                                </td>
                            </tr>
                            <%} %>
                            <!-- Added upto here -->
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <strong>Email Address</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="connectformDesc">
                                                Your point of contact to the DStv world. Your email address or username can be used
                                                to login
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 251px;">
                                    <div class="connectinputBoxes">
                                        <asp:TextBox ID="EmailAddressTextBox" ValidationGroup="105" runat="server" /></div>
                                </td>
                                <td class="connectreqField">
                                    <span>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding5px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="105"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.EmailAddressRequired%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="105"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox"
                                        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.EmailAddressValid%></div></td>
                                        </tr>
                                    </table>                                            
                                    </asp:RegularExpressionValidator>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" ValidationGroup="105"
                                        CssClass="connectformError" ForeColor="">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.EmailNotAvailable%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>                                
                                    </asp:CustomValidator>
                                    <asp:Label ID="EmailRegisteredLabel" runat="server" Visible="false">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <img alt="" src="<%=this.fullImagePath%>icon_excl.gif" id="imgError" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="connectformError">
                                                    <div>
                                                        <asp:Label ID="ErrorLabel" runat="server" ClientIDMode="Static" /></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectpadding5px">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <strong>Password</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="connectformDesc">
                                                A strong password consists of more than 6 characters and contains letters, numbers
                                                and special characters
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 251px;">
                                    <div class="connectinputBoxes">
                                        <asp:TextBox ID="PasswordTextBox" ValidationGroup="105" TextMode="Password" runat="server" /></div>
                                </td>
                                <td class="connectreqField">
                                    <span>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding5px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="105"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.PasswordRequired%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="105"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox"
                                        ValidationExpression="^.{6,15}$">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.PasswordValidity%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>                                            
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <strong>Repeat Password</strong>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 251px;">
                                    <div class="connectinputBoxes">
                                        <asp:TextBox ID="RepeatPasswordTextBox" TextMode="Password" runat="server" /></div>
                                </td>
                                <td class="connectreqField">
                                    <span>*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ValidationGroup="105" CssClass="connectformError" ForeColor="" ControlToValidate="RepeatPasswordTextBox">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.PasswordRepeat%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ValidationGroup="105"
                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="RepeatPasswordTextBox"
                                        ControlToCompare="PasswordTextBox">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td>&nbsp;</td>
                                            <td class="connectformError"><div><%=this.ValidationMessages.Register.PasswordNotMatch%></div></td>
                                        </tr>
                                        <tr>
                                            <td><div class="connectpadding5px"></div></td>
                                        </tr>
                                    </table>                                            
                                    </asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding5px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="AcceptTermsCheckBox" runat="server" ValidationGroup="105" />
                                            </td>
                                            <td style="width: 15px;">
                                            </td>
                                            <td>
                                                I accept the <a href="" runat="server" id="lnkRegisterTC" target="_blank">terms & conditions</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="connectpadding10px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" Display="Dynamic" CssClass="connectformError"
                                        ValidationGroup="105" ForeColor="" OnServerValidate="Validate_Terms">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td class="connectformError"><div>Please accept our terms & conditions</div></td>
                                        </tr>
                                    </table>                                
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="connectpadding10px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="panelCaptcha" Visible="false">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <DStvConnect:captchaControl ID="mycaptchacontrol" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="connectformError">
                                                    <asp:Label runat="server" ID="lblVerifyError"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectpadding10px">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="RegisterImageButton" runat="server" OnClick="Register_Click"
                                                    ValidationGroup="105" CausesValidation="true" OnClientClick="if(document.getElementById('ErrorLabel') != null) { document.getElementById('ErrorLabel').innerHTML ='';} if(document.getElementById('imgError') != null) { document.getElementById('imgError').src = '';} if(Page_ClientValidate('105')){ document.getElementById('imgProcessingRegister').style.display = 'block'; }" />
                                            </td>
                                            <td align="right">
                                                <img runat="server" enableviewstate="false" id="imgProcessingRegister" clientidmode="Static"
                                                    src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none; margin-left: 20px"
                                                    alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding5px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectformDesc">
                                        Fields marked with<span class="connecterrorTxt">*</span>&nbsp;&nbsp;&nbsp;are required
                                        fields</div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="connectlbformRight">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="connectlbHead">
                            <div runat="server" id="divConnectTitle">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div runat="server" id="divConnectMessages" style="display: none">
                            </div>
                            DStv Connect is your portal to getting so much more online. Register or login and
                            manage your account.
                            <br />
                            <br />
                            Use your DStv Connect credentials to login to all of the partner sites listed below.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label title="DStv Connect Availability">
                                <img alt="DStv Connect Availability" src="<%=this.ConnectImagePath%>logossml.gif" /></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <span>