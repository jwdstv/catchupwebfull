﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestVerify.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.RequestVerify" %>
<span class="ConnectContainer">
    <div class="connectform">
        <div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <label title="DStv Connect">
                            <img alt="DStv Connect" src="<%=this.ConnectImagePath%>dstvconnect.png" /></label>
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td>
                        <img alt="" src="<%=this.ConnectImagePath%>pr_headsep.gif" />
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectformHeadTxt">
                        Request a resend of your DStv Connect account verification email
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectformItems">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td colspan="2" class="connectlbHeadBlue">
                        <%--<p>Please supply us with your email address in the box below to be emailed a new verification link.</p>--%>
                        <p>
                            Please supply us with the email address you registered with to be emailed a new
                            verification link.</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <strong>Email Address</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 251px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="EmailAddressTextBox" runat="server" ValidationGroup="101" /></div>
                    </td>
                    <td class="connectreqField">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RequiredFieldValidator ID="EmailAddressRequiredValidator" ValidationGroup="101"
                            runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                <td>&nbsp;</td>
                                <td class="connectformError"><div><%=this.ValidationMessages.Login.EmailRequired%></div></td>
                            </tr>
                            <tr>
                                <td><div class="connectpadding5px"></div></td>
                            </tr>
                        </table>
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="EmailAddressRegexValidator" ValidationGroup="101"
                            runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox"
                            ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                <td>&nbsp;</td>
                                <td class="connectformError"><div><%=this.ValidationMessages.Login.EmailValidity%></div></td>
                            </tr>
                            <tr>
                                <td><div class="connectpadding5px"></div></td>
                            </tr>                                
                        </table>
                        </asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="EmailAddressCustomValidator" ValidationGroup="101" runat="server"
                            Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="connectformError">
                                        <div>
                                            <asp:Label ID="EmailValidationLabel" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="connectpadding5px">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="SubmitImageButton" runat="server" OnClick="OnSubmit_Click" ValidationGroup="101" />
                                </td>
                                <td>
                                    <a class="connecthelp" style="display: none;">
                                        <img alt="" src="<%=this.ConnectImagePath%>icon_help.gif" /></a>
                                </td>
                                <td>
                                    <div class="connecthelp">
                                        <asp:Label ID="ErrorLabel" runat="server" CssClass="connectformError" /></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectlbformRight">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="connectlbHead">
                        About DStv Connect
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-weight: normal">
                            DStv Connect is your portal to getting so much more online.</p>
                        <br />
                        <p style="font-weight: normal">
                            Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="RegisterHyperLink" runat="server" Text="Don't have a DStv Connect ID?"
                            NavigateUrl="~/SignUp.aspx" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label title="DStv Connect Availability">
                            <img alt="DStv Connect Availability" src="<%=this.ConnectImagePath%>logossml.gif" /></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <span>