﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeBehind="SmartCard.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.SmartCard" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panelSmartCard"
        DefaultButton="imageButtonLinkSmartCard">
        <div class="connectfloatLeft">
            <asp:UpdatePanel runat="server"
                ID="updatePanelSmartCard">
                <ContentTemplate>
                    <table border="0" cellpadding="0"
                        cellspacing="0" style="width: 100%;">
                        <asp:MultiView runat="server" ID="multiViewSmartCard"
                            ActiveViewIndex="0">
                            <asp:View ID="view1" runat="server">
                                <tr>
                                    <td>
                                        <strong>Choose your method of authentication</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList runat="server"
                                            ID="authMethodRadioList" AutoPostBack="True"
                                            CellPadding="10" OnSelectedIndexChanged="authMethodRadioList_SelectedIndexChanged"
                                            RepeatDirection="Horizontal"
                                            Width="541px">
                                            <asp:ListItem Selected="True" Text="Smartcard"
                                                Value="SmartCard" />
                                            <asp:ListItem Text="Customer Number"
                                                Value="CustomerNumber" />
                                            <asp:ListItem Text="ID Number"
                                                Value="IDNumber" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <!-- Link By Smartcard -->
                                <asp:Panel runat="server" ID="smartcardPanel">
                                    <%-- change by puneet--%>
                                    <%--<tr>
                                <td colspan="2">
                                    <div style="float: left; position: relative;">
                                        <strong>Link your DStv SmartCard Number</strong>
                                    </div>
                                </td>
                            </tr>--%>
                                    <tr>
                                        <td colspan="2">
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;"
                                            colspan="2">
                                            <table border="0" cellpadding="0"
                                                cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <img alt="DStv Smartcard" style="text-align: left;"
                                                            src="<%=this.fullImagePath%>smartcard.gif" />
                                                    </td>
                                                    <td style="vertical-align: top;">
                                                        <span class="connectformDesc">Please
                                                            enter the first 10 digits of your
                                                            SMARTCARD number and click on
                                                            the link button.</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0"
                                                cellspacing="0">
                                                <%-- Addition by Puneet--%>
                                                <tr>
                                                    <td colspan="2">
                                                        <div style="float: left; position: relative;">
                                                            <strong>Link your DStv SmartCard Number</strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="connectinputBoxes">
                                                            <asp:TextBox ID="txtSmartCardNumber"
                                                                runat="server" MaxLength="10"
                                                                onkeydown="return numbersOnly(event);"
                                                                EnableViewState="true" />
                                                        </div>
                                                    </td>
                                                    <td class="connectfieldMsgs">
                                                        <div style="position: relative;
                                                            margin-top: 3px;">
                                                            <asp:RequiredFieldValidator ID="rfvSmartCard"
                                                                ControlToValidate="txtSmartCardNumber"
                                                                runat="server" ForeColor=""
                                                                CssClass="connectformError"
                                                                ValidationGroup="107" Display="Dynamic"
                                                                EnableClientScript="true">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div>Please enter a smartcard number.</div></td>
                                                            </tr>
                                                        </table>
                                                            </asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="custSmartCard"
                                                                OnServerValidate="custSmartCard_ServerValidate"
                                                                runat="server" ForeColor=""
                                                                ValidateEmptyText="true" CssClass="connectformError"
                                                                ValidationGroup="107" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div><%=this.smartcardError%></div></td>
                                                            </tr>
                                                        </table>
                                                            </asp:CustomValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <div id="lnkHelp" style="display: none;">
                                            </div>
                                            <a style="border: none; vertical-align: top"
                                                href="javascript:void(0);"
                                                class="connecthelp" onmouseover="tooltip.show('<b>Why do you need my SmartCard Number?</b><br/>We need to validate your DStv subscription so that you can watch DStv on Demand.<br/>Information is stored securely and will never be shared with a third party.');"
                                                onmouseout="tooltip.hide();">
                                                <img style="border: none; vertical-align: top"
                                                    alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="idNumberPanel"
                                    Visible="false">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0"
                                                cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <div class="connectinputBoxes">
                                                            <asp:TextBox ID="txtIDNumber" runat="server"
                                                                MaxLength="13" onkeydown="return numbersOnly(event);" />
                                                        </div>
                                                    </td>
                                                    <td class="connectfieldMsgs">
                                                        <div style="position: relative;
                                                            margin-top: 3px;">
                                                            <asp:RequiredFieldValidator ID="rfvIDNumber"
                                                                ControlToValidate="txtIDNumber"
                                                                runat="server" ForeColor=""
                                                                CssClass="connectformError"
                                                                ValidationGroup="107" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div>Please enter ID number</div></td>
                                                            </tr>
                                                        </table>
                                                            </asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="custmIDNumber"
                                                                OnServerValidate="custmIDNumber_ServerValidate"
                                                                runat="server" ForeColor=""
                                                                ValidateEmptyText="true" CssClass="connectformError"
                                                                ValidationGroup="107" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div><%=this.smartcardError%></div></td>
                                                            </tr>
                                                        </table>
                                                            </asp:CustomValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <div id="A1" style="display: none;">
                                            </div>
                                            <a style="border: none; vertical-align: top"
                                                href="javascript:void(0);"
                                                class="connecthelp" onmouseover="tooltip.show('<b>Why do you need my ID Number?</b><br/>We need to validate your DStv subscription so that you can watch DStv on Demand.<br/>Information is stored securely and will never be shared with a third party.');"
                                                onmouseout="tooltip.hide();">
                                                <img style="border: none; vertical-align: top"
                                                    alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="customerNumberPanel"
                                    Visible="false">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0"
                                                cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <div class="connectinputBoxes">
                                                            <asp:TextBox ID="txtCustomerNumber"
                                                                runat="server" MaxLength="10"
                                                                onkeydown="return numbersOnly(event);" />
                                                        </div>
                                                    </td>
                                                    <td class="connectfieldMsgs">
                                                        <div style="position: relative;
                                                            margin-top: 3px;">
                                                            <asp:RequiredFieldValidator ID="rfvCustomerNumber"
                                                                ControlToValidate="txtCustomerNumber"
                                                                runat="server" ForeColor=""
                                                                CssClass="connectformError"
                                                                ValidationGroup="107" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div>Please enter Customer number</div></td>
                                                            </tr>
                                                        </table>
                                                            </asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="custmNumber"
                                                                OnServerValidate="custmNumber_ServerValidate"
                                                                runat="server" ForeColor=""
                                                                ValidateEmptyText="true" CssClass="connectformError"
                                                                ValidationGroup="107" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div>Please enter a valid customer number.
</div></td>
                                                            </tr>
                                                        </table>
                                                            </asp:CustomValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            <div id="A2" style="display: none;">
                                            </div>
                                            <a style="border: none; vertical-align: top"
                                                href="javascript:void(0);"
                                                class="connecthelp" onmouseover="tooltip.show('<b>Why do you need my Customer Number?</b><br/>We need to validate your DStv subscription so that you can watch DStv on Demand.<br/>Information is stored securely and will never be shared with a third party.');"
                                                onmouseout="tooltip.hide();">
                                                <img style="border: none; vertical-align: top"
                                                    alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td>
                                        <div class="connectpadding5px">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Account Holder Surname</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0"
                                            cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div class="connectinputBoxes">
                                                        <asp:TextBox runat="server" ID="txtAcctSurname"></asp:TextBox></div>
                                                </td>
                                                <td class="connectfieldMsgs">
                                                    <div style="position: relative;
                                                        margin-top: 3px;">
                                                        <asp:RequiredFieldValidator ID="rfvAcctSurname"
                                                            ControlToValidate="txtAcctSurname"
                                                            runat="server" ForeColor=""
                                                            ValidateEmptyText="true" CssClass="connectformError"
                                                            ValidationGroup="107" Display="Dynamic"
                                                            EnableClientScript="true">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div>Please enter Account holder's surname.</div></td>
                                                            </tr>
                                                        </table>
                                                        </asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator
                                                            ID="regSurname" runat="server"
                                                            ControlToValidate="txtAcctSurname"
                                                            ForeColor="" CssClass="connectformError"
                                                            ValidationGroup="107" Display="Dynamic"
                                                            ValidationExpression="^[a-zA-Z ]+$">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                <div style="clear:both;padding-top:5px;">
                                                                </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div>Please enter characters only.</div></td>
                                                            </tr>
                                                        </table>
                                                        </asp:RegularExpressionValidator>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </asp:View>
                            <asp:View ID="view2" runat="server">
                                <tr>
                                    <td class="connectfieldMsgs">
                                        <table border="0" cellpadding="0"
                                            cellspacing="0">
                                            <tr>
                                                <td>
                                                    <img alt="" src="<%=this.fullImagePath%>tick.gif" />
                                                </td>
                                                <td id="connectSuccessfulLinkText">
                                                    <%=this.successfulLink%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </asp:View>
                        </asp:MultiView>
                        <tr>
                            <td>
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;
                                text-align: left;">
                                <table border="0" cellpadding="0"
                                    cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imageButtonLinkSmartCard"
                                                runat="server" CommandName="link"
                                                ValidationGroup="107" OnClick="imageButtonLinkSmartCard_OnClick"
                                                OnClientClick="if(Page_ClientValidate('107')){ document.getElementById('imgProcessing').style.display = 'block'; } setTimeout('checkIfLinked()', 300); " />
                                        </td>
                                        <td align="right">
                                            <img runat="server" enableviewstate="false"
                                                id="imgProcessing" clientidmode="Static"
                                                src="<%=this.fullImagePath%>ajax-loader.gif"
                                                style="display: none; margin-left: 20px"
                                                alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="connecthr_dotline2">
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</span>