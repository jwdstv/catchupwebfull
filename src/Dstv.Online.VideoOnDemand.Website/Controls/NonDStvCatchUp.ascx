﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonDStvCatchUp.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.NonDStvCatchUp" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panelNonDStvCatchUp">
        <div class="connectformLeft">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="3">
                        <div class="pasdding10px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="connecterrorTxt">
                        <b>On Demand CatchUp is only available to DStv Subscribers.</b><br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <a href="#">See the full benefits of being a DStv Subscriber</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="connectpadding10px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="connecthr_dotline2">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</span>