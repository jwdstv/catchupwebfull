﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountPane.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.AccountPane" %>
<%--<%@ Register Src="~/Controls/NavigationMenu.ascx" TagName="navigationMenu" TagPrefix="DStvConnect" %>--%>
<span class="ConnectContainer">
<asp:Panel ID="Panel1" runat="server" DefaultButton="LinkImageButton">
    <div class="connectform">
        <%--<div>
            <DStvConnect:NavigationMenu runat="server" id="navigationMenuMyAccount" />
        </div>--%>
        <div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <label title="DStv Connect">
                            <img alt="DStv Connect" src="<%=this.fullImagePath%>dstvconnect.png" /></label>
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectprMenuUpdated">
                        <span class="connectprMenuItem"><a href="About.aspx" id="amHelp">
                            <img alt="" src="<%=this.fullImagePath%>pr_aboutme_tick.gif" /></a></span> <span
                                class="connectprMenuItem"><a id="maHelp">
                                    <img alt="" src="<%=this.fullImagePath%>pr_myaccount_tick.gif" /></a></span>
                        <!--                        <span class="connectprMenuItem"><a href="Alerts.aspx" id="alHelp">
                            <img alt="" src="<%=this.fullImagePath%>pr_myalerts_q.gif" /></a></span><span class="connectprMenuItem">
                                <a href="Newsletters.aspx" id="nlHelp">
                                    <img alt="" src="<%=this.fullImagePath%>pr_mynewsletters_q.gif" /></a></span>-->
                    </td>
                    <td>
                        <img alt="" src="<%=this.fullImagePath%>pr_headsep.gif" />
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectlbformHeadTxt">
                        Complete your DStv Connect profile to access exclusive content, special offers and
                        more.
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectformItems">
            <div class="connectfloatLeft">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align: top;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Image ID="ProfileImage" runat="server" ImageUrl="~/App_Themes/Connect/Images/social_temp.jpg"
                                            Width="125px" Height="125px" />
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td>
                                        <a id="__btnUpdatePic" class="connectsmlLink1">Update Profile pic</a>
                                        <div style="width: 250px; height: 75px; z-index: 1000; border: solid 1px silver;
                                            position: absolute; background: #FFF; display: none;">
                                            <div id="file-uploader">
                                                <noscript>
                                                    <p>
                                                        Please enable JavaScript to use this feature.</p>
                                                </noscript>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td style="vertical-align: top;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" class="connectuserHello">
                                        Hello <span class="connectuserTitle">
                                            <%=Page.User.Identity.Name%></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="connectpadding20px">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="connectprogTxt">
                                        Your profile is <span class="connectprogPercent">50%</span> complete
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Complete your DStv Connect Profile by updating your account
                                        <%--& choosing the alerts
                                        & newsletters that you would like.--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectpadding10px">
            </div>
            <div class="connecthdr_21pxGrey_line">
                <h1>
                    UPDATE YOUR ACCOUNT INFO</h1>
            </div>
            <div class="connectfloatLeft">
                Update your DStv Smartcard and ADSL Account details and watch your favourite shows,
                movies and sport online, anywhere, anytime
                <br />
                <br />
            </div>
            <div class="connectfloatLeft">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="">
                                <tr>
                                    <td>
                                        <h3>
                                            <strong>STEP 1</strong> - LINK YOUR DSTV ACCOUNT</h3>
                                        <div class="connecthr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="IBSLinkRadioButtonList" runat="server" AutoPostBack="true"
                                            RepeatDirection="Horizontal" OnSelectedIndexChanged="LinkIBS_SelectedIndexChanged"
                                            RepeatLayout="table" CellPadding="5" CellSpacing="5" Width="350px" Style="display: none;">
                                            <asp:ListItem Text="Smartcard Number" Value="smrtCard" Selected="True" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="connectpadding10px">
                                        </div>
                                    </td>
                                </tr>
                                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="2">
                                    <asp:View ID="View1" runat="server">
                                        <tr>
                                            <td colspan="2">
                                                <div style="float: left; position: relative;">
                                                    <strong>ID Number</strong></div>
                                                <div style="float: left; padding-left: 10px; font-size: 11px; margin-top: 1px;">
                                                    Please supply a valid South African ID number.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div class="connectinputBoxes">
                                                                <asp:TextBox ID="IDNumberTextBox" runat="server" MaxLength="13" onkeypress="return numbersOnly(event);" /></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="connectfieldMsgs">
                                                <div style="position: relative; margin-top: 3px;">
                                                    <asp:CustomValidator ID="IDNumberCustomValidator" OnServerValidate="OnValidateIDNumber_ServeSide"
                                                        runat="server" ForeColor="" CssClass="connectformError" ValidateEmptyText="true"
                                                        ValidationGroup="1" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div><%=this.IDNumberError%></div></td>
                                                            </tr>
                                                        </table>
                                                    </asp:CustomValidator>
                                                </div>
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <asp:View ID="View2" runat="server">
                                        <tr>
                                            <td colspan="2">
                                                <div style="float: left; position: relative;">
                                                    <strong>Customer Number</strong></div>
                                                <div style="float: left; padding-left: 10px; font-size: 11px; margin-top: 1px;">
                                                    Please supply your Multichoice customer / account number.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div class="connectinputBoxes">
                                                                <asp:TextBox ID="CustomerNumberTextBox" runat="server" MaxLength="18" onkeypress="return numbersOnly(event);" /></div>
                                                        </td>
                                                        <td class="connectfieldMsgs">
                                                            <asp:CustomValidator Display="Dynamic" runat="server" OnServerValidate="OnValidateCustomerNumber_ServeSide"
                                                                ForeColor="" ID="CustomerNumberCustomValidator" CssClass="connectformError" ValidateEmptyText="true"
                                                                ValidationGroup="1">
                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                        <td>&nbsp;</td>
                                                                        <td class="connectformError"><div><%=this.CustomerNumberError%></div></td>
                                                                    </tr>
                                                                </table>                                                                
                                                            </asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <asp:View ID="View3" runat="server">
                                        <tr>
                                            <td style="vertical-align: top;" colspan="2">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <img alt="DStv Smartcard" style="text-align: left;" src="<%=this.fullImagePath%>smartcard.gif" />
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            <span class="connectformDesc">DStv On Demand is currently only available to DStv Premium
                                                                subscribers. Please enter the first 10 digits of your SMARTCARD number.</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div style="float: left; position: relative;">
                                                    <strong>DStv SmartCard Number</strong></div>
                                                <div style="float: left; padding-left: 10px; font-size: 11px; margin-top: 1px;">
                                                    Please supply the first ten (10) digits of your DStv Smartcard.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div class="connectinputBoxes">
                                                                <asp:TextBox ID="SmartcardTextBox" runat="server" MaxLength="10" onkeypress="return numbersOnly(event);" /></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="connectfieldMsgs">
                                                <div style="position: relative; margin-top: 3px;">
                                                    <asp:CustomValidator ID="SmartcardCustomValidator" OnServerValidate="OnValidateSmartcard_ServeSide"
                                                        runat="server" ForeColor="" ValidateEmptyText="true" CssClass="connectformError"
                                                        ValidationGroup="1" Display="Dynamic">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                            </tr>
                                                            <tr>
                                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                                <td>&nbsp;</td>
                                                                <td class="connectformError"><div><%=this.SmartcardError%></div></td>
                                                            </tr>
                                                        </table>
                                                    </asp:CustomValidator>
                                                </div>
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <asp:View ID="View4" runat="server">
                                        <tr>
                                            <td class="connectfieldMsgs">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <img src="<%=this.fullImagePath%>tick.gif" />
                                                        </td>
                                                        <td>
                                                            <%=this.SuccessfulLink%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                                <tr>
                                    <td>
                                        <div class="connectpadding10px">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; text-align: left;">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="LinkImageButton" runat="server" CommandName="link" ValidationGroup="1"
                                                        OnClick="LinkSmartcard_OnClick" />
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0);" class="connecthelp" id="lnkHelp">
                                                        <img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="connecthr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="http://care.dstv.com/main.aspx?ID=1907" target="_blank">Not a DStv Premium
                                            Subscriber? Click here.</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connecthdr_21pxGrey_line">
            </div>
            <div class="connectpadding10px">
            </div>
            <asp:Panel ID="Panel2" runat="server" DefaultButton="LinkISPPartnerImageButton">
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <h3>
                                    <strong>STEP 2</strong> - LINK YOUR ADSL ACCOUNT</h3>
                                <div class="connecthr_dotline2">
                                </div>
                            </td>
                        </tr>
                        <asp:MultiView ID="MultiView2" runat="server" ActiveViewIndex="0">
                            <asp:View ID="mv_View1" runat="server">
                                <tr>
                                    <td colspan="2" style="vertical-align: top;">
                                        <span class="connectformDesc">To provide the best possible experience, you'll need an
                                            uncapped ADSL Internet connection from an approved ISP. </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="connectpadding10px">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>ADSL Account Id</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div class="connectinputBoxes">
                                                        <asp:TextBox ID="ISPEmailAddressTextBox" runat="server" ValidationGroup="2" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="connectreqField">
                                                        *</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="connectfieldMsgs">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="2"
                                                        Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="ISPEmailAddressTextBox">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.ValidationMessages.Register.ADSLIdRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="ISPEmailAddressCustomValidator" runat="server"
                                                        ValidationGroup="2" Display="Dynamic" CssClass="connectformError" ForeColor=""
                                                        Enabled="false" ControlToValidate="ISPEmailAddressTextBox" ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z\.]*))$">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.ValidationMessages.Register.EmailAddressValid%></div></td>
                                                    </tr>
                                                </table>                                            
                                                    </asp:RegularExpressionValidator>
                                                    <asp:Label ID="EmailRegisteredLabel" runat="server" Visible="false">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                    <div style="clear: both; padding-top: 5px;">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td class="connectformError">
                                                                    <div>
                                                                        <asp:Label ID="ErrorLabel" runat="server" /></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="connectpadding5px">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Label>
                                                </td>
                                                <td align="right">
                                                    <a href="javascript:void(0);" class="connecthelp" id="ankADSLHelp">
                                                        <img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="connectpadding10px">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>ADSL Password</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <div class="connectinputBoxes">
                                                        <asp:TextBox ID="ISPPasswordTextBox" runat="server" TextMode="Password" ValidationGroup="2" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="connectfieldMsgs">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="2"
                                            Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="ISPPasswordTextBox">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.ValidationMessages.Register.PasswordRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="connectpadding5px"></div></td>
                                                    </tr>
                                                    </table>
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </asp:View>
                            <asp:View ID="mv_View2" runat="server">
                                <tr>
                                    <td class="connectfieldMsgs">
                                        <div>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <img alt="Success" src="<%=this.fullImagePath%>tick.gif" />
                                                    </td>
                                                    <td>
                                                        ISP linked.<%--<b style="color: Red"><%=this.ISPProvider%></b>&nbsp;--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </asp:View>
                        </asp:MultiView>
                        <tr>
                            <td>
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; text-align: left;" colspan="2">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="LinkISPPartnerImageButton" CommandName="link" runat="server"
                                                ValidationGroup="2" OnClick="LinkISPPartner_OnClick" />
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="connecthelp" id="prtHelp">
                                                <img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server">
                <h3 style="float: none">
                    <strong>STEP 2</strong> - LINK YOUR ADSL ACCOUNT</h3>
                <div class="connecthr_dotline2" style="clear: both">
                </div>
                <span class="connectformDesc">Linking your ISP is only required for South African users,
                    and is only used for Ondemand playback.</span>
            </asp:Panel>
        </div>
        <div class="connectlbformRight">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="connectlbHead">
                        About DStv Connect
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-weight: normal">
                            DStv Connect is your portal to getting so much more online.</p>
                        <br />
                        <p style="font-weight: normal">
                            Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label title="DStv Connect Availability">
                            <img alt="DStv Connect Availability" src="<%=this.fullImagePath%>logossml.gif" /></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
</span>
