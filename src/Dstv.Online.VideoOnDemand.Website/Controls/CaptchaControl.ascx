﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CaptchaControl.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.CaptchaControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<table>
    <asp:UpdatePanel ID="updatePanelCaptcha" runat="server" UpdateMode="Always">
        <contenttemplate>
            <tr>
        <td style="text-align: left">
            <asp:Image ID="ImgCaptcha" runat="server" />
            <asp:ImageButton runat="server" ID="ImgNewCaptcha" AlternateText="Try New" ToolTip="Try New"
                OnClick="ImgeNewCaptcha_Click" />
        </td>
    </tr>
            <tr>
        <td valign="middle">
            <asp:Label ID="LblMsg" runat="server" Text="Enter the above code here:"></asp:Label>
            <div class="connectinputBoxes">
                <asp:TextBox ID="TxtCpatcha" runat="server" Text=""></asp:TextBox>
            </div>
        </td>
    </tr>
            <%--<tr> 
        <td valign="middle">
            <asp:LinkButton ID="btnTryNewWords" runat="server" Font-Names="Tahoma" Font-Size="Smaller"
                 OnClick="btnTryNewWords_Click">Can&#39;t read? Try different 
            words.</asp:LinkButton>
        </td>
    </tr> --%>
        </contenttemplate>
    </asp:UpdatePanel>
</table>
