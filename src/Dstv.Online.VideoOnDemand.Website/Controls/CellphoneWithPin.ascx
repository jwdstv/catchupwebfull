﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CellphoneWithPin.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.CellphoneWithPin" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panelCellphone">
        <div class="connectfloatLeft">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <strong>Cellphone number</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        Use this number to send your rental code each time you rent a video. Up to 4 numbers are supported.
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes1">
                                        <asp:TextBox ID="txtCellPhoneNumber1" runat="server" MaxLength="13" ValidationGroup="1"
                                            onkeydown="return numbersOnly(event);" EnableViewState="false" />
                                    </div>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span class="connectreqField">*</span>
                                            </td>
                                            <td class="connectfieldMsgs">
                                                <asp:RequiredFieldValidator runat="server" ID="rfvCellPhoneNumber1" ValidationGroup="1"
                                                    Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtCellPhoneNumber1">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator runat="server" ID="regExLength" ValidationGroup="1"
                                                    Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtCellPhoneNumber1"
                                                    ValidationExpression="^.{10,13}$">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneValidity%></div></td>
                                                    </tr>
                                                </table>                                            
                                                </asp:RegularExpressionValidator>
                                                <asp:CustomValidator runat="server" ID="customCellphone1" Display="Dynamic" CssClass="connectformError"
                                                    ValidationGroup="1" ForeColor="" OnServerValidate="cellphone_ServerValidate1">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneValidity%></div></td>
                                                    </tr>
                                                </table>                                            
                                                </asp:CustomValidator>
                                                <asp:Label ID="CellPhoneLabel" runat="server" Visible="false">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td colspan="3">
                                                                <div style="clear: both; padding-top: 5px;">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="connectformError">
                                                                <div>
                                                                    <asp:Label ID="ErrorLabel" runat="server" /></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="connectpadding5px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                    <contenttemplate>
                <tr>                
               <td>
               <img runat="server" id="imgAddNumber" alt="" src=""/>
                   
                        <asp:LinkButton runat="server" ID="lnkButtonAddNumber" Text="Add a number" OnClick="lnkButtonAddNumber_Click"></asp:LinkButton>
                </td>                
            </tr>
            <tr><td colspan="2">
            <asp:Panel runat="server" ID="panelAddNumbers" Visible="false">
           <div><table border="0" cellpadding="0" cellspacing="0">
           <tr>
           <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="connectinputBoxes1">
                                    <asp:TextBox ID="txtCellPhoneNumber2" runat="server" MaxLength="13" ValidationGroup="1" EnableViewState="false"
                                         />
                                </div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                        <asp:CustomValidator runat="server" ID="customCellphone2" Display="Dynamic" CssClass="connectformError"
                                                    ValidationGroup="1" ForeColor="" OnServerValidate="cellphone_ServerValidate2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneValidity%></div></td>
                                                    </tr>
                                                </table>                                            
                                            </asp:CustomValidator>
                                            <asp:Label ID="lblCellphone2" runat="server" Visible="false">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3">
                                                            <div style="clear: both; padding-top: 5px;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="connectformError">
                                                            <div>
                                                                <asp:Label ID="Label2" runat="server" /></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="connectpadding5px">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                                        </td>
                            </tr>
                            </table>
                            </td>
           </tr>
           <tr>
           <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="connectinputBoxes1">
                                    <asp:TextBox ID="txtCellPhoneNumber3" runat="server" MaxLength="13" ValidationGroup="1" EnableViewState="false"
                                         />
                                </div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                        <asp:CustomValidator runat="server" ID="customCellphone3" Display="Dynamic" CssClass="connectformError"
                                                    ValidationGroup="1" ForeColor="" OnServerValidate="cellphone_ServerValidate3">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneValidity%></div></td>
                                                    </tr>
                                                </table>                                            
                                            </asp:CustomValidator>
                                            <asp:Label ID="lblCellphone3" runat="server" Visible="false">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3">
                                                            <div style="clear: both; padding-top: 5px;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="connectformError">
                                                            <div>
                                                                <asp:Label ID="Label3" runat="server" /></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="connectpadding5px">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                                        </td>
                            </tr>
                            </table>
                            </td>
           </tr>
           <tr>
           <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div class="connectinputBoxes1">
                                    <asp:TextBox ID="txtCellPhoneNumber4" runat="server" MaxLength="13" ValidationGroup="1" EnableViewState="false"
                                         />
                                </div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                        <asp:CustomValidator runat="server" ID="customCellphone4" Display="Dynamic" CssClass="connectformError"
                                                    ValidationGroup="1" ForeColor="" OnServerValidate="cellphone_ServerValidate4">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneValidity%></div></td>
                                                    </tr>
                                                </table>                                            
                                            </asp:CustomValidator>
                                            <asp:Label ID="lblCellphone4" runat="server" Visible="false">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3">
                                                            <div style="clear: both; padding-top: 5px;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td class="connectformError">
                                                            <div>
                                                                <asp:Label ID="Label5" runat="server" /></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="connectpadding5px">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Label>
                                        </td>
                                        </tr>
                                        </table>
                                        </td>
                            </tr>
                            </table>
                            </td>
           </tr>
           </table></div>
                </asp:Panel>
                </td></tr>
                </contenttemplate>
                </asp:UpdatePanel>
                <tr>
                    <td>
                        <div class="connectpadding10px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Enter a 4 digit pin to use for BoxOffice video</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        You will need to enter this pin each time you rent a BoxOffice title.
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="connectinputBoxes1">
                                        <asp:TextBox ID="txtBoxOfficePin1" TextMode="Password" ClientIDMode="Static" runat="server" MaxLength="1"
                                            size="1" onkeydown="return numbersOnly(event);" onkeyup="if(document.getElementById('txtBoxOfficePin1').value.length >= 1) { document.getElementById('txtBoxOfficePin2').focus();}" EnableViewState="false" />&nbsp;&nbsp;
                                        <asp:TextBox ID="txtBoxOfficePin2" TextMode="Password" ClientIDMode="Static" runat="server" MaxLength="1"
                                            size="1" onkeydown="return numbersOnly(event);" onkeyup="if(document.getElementById('txtBoxOfficePin2').value.length >= 1) { document.getElementById('txtBoxOfficePin3').focus();}" EnableViewState="false" />&nbsp;&nbsp;
                                        <asp:TextBox ID="txtBoxOfficePin3" TextMode="Password" ClientIDMode="Static" runat="server" MaxLength="1"
                                            size="1" onkeydown="return numbersOnly(event);" onkeyup="if(document.getElementById('txtBoxOfficePin3').value.length >= 1) { document.getElementById('txtBoxOfficePin4').focus();}" EnableViewState="false" />&nbsp;&nbsp;
                                        <asp:TextBox ID="txtBoxOfficePin4" TextMode="Password" ClientIDMode="Static" runat="server" MaxLength="1"
                                            size="1" onkeydown="return numbersOnly(event);" EnableViewState="false" />&nbsp;&nbsp;
                                    </div>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span class="connectreqField">*</span>
                                            </td>
                                            <td class="connectfieldMsgs">
                                                <asp:CustomValidator runat="server" ID="boxOfficePinValidation" ValidationGroup="1"
                                                    Display="Dynamic" CssClass="connectformError" ForeColor="" OnServerValidate="boxOfficePinValidation_ServerValidate">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>    
                                                        <td colspan="2">
                                                            <div style="clear:both;padding-top:5px;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td class="connectformError"><div><%=this.boxOfficePinError%></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connecthr_dotline2">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnErrorAtIBS" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</span>