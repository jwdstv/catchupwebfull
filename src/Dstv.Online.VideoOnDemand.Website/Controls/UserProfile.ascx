﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.UserProfile" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>
<%@ Register Src="~/Controls/NavigationMenu.ascx" TagName="navigationMenu" TagPrefix="DStvConnect" %>
<script type="text/javascript" language="javascript">

    function loadPhoto() {
        document.forms[0].submit();
    }

    function encodeMyHtml() {
        txt = document.getElementById("<%=txtBio.ClientID %>");
        var encodedHtml = txt.value;
        encodedHtml = encodedHtml.replace("<", "&lt;");
        encodedHtml = encodedHtml.replace(">", "&gt;");
        txt.value = encodedHtml;
    }


    function textboxMultilineMaxNumber(txt, maxLen, evt) {
        try {
            evt = (evt) ? evt : (window.event) ? event : null;
            if (evt) {
                var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
            }

            if (txt.value.length > (maxLen - 1)) {
                if (evt) {
                    if ((charCode != 8) && (charCode != 46) && (charCode != 37) && (charCode != 38) && (charCode != 39) && (charCode != 40)) //Except backspace, delete, LA, UA, RA, DA
                    {
                        if (window.event) {
                            evt.returnValue = false;
                        }
                        else {
                            return false;
                        }
                    }
                }

            }
        } catch (e) {
        }
    }


    $(document).ready(function () {
        $('img#<%=photo.ClientID %>').imgAreaSelect({
            x1: 80, y1: 80, x2: 220, y2: 220,
            onInit: preview,
            aspectRatio: '1:1',
            handles: true,
            fadeSpeed: 200,
            onSelectChange: preview,
            x1: 80, y1: 80, x2: 220, y2: 220,
            persistent: true,
            onInit: preview
        });
    });

    function preview(img, selection) {
        if (!selection.width || !selection.height)
            return;

        var layout = selection.x1.toString() + ',' + selection.y1.toString() + ',' + selection.width.toString() + ',' + selection.height.toString();
        $('#<%=CropInfo.ClientID %>').val(layout);

        var scaleX = 125 / selection.width;
        var scaleY = 125 / selection.height;
        $('#preview img').css({
            width: Math.round(scaleX * 300),
            height: Math.round(scaleY * img.height),
            marginLeft: -Math.round(scaleX * selection.x1),
            marginTop: -Math.round(scaleY * selection.y1)
        });

        //        $('#x1').val(selection.x1);
        //        $('#y1').val(selection.y1);
        //        $('#x2').val(selection.x2);
        //        $('#y2').val(selection.y2);
        //        $('#w').val(selection.width);
        //        $('#h').val(selection.height);


    }




</script>
<span class="ConnectContainer">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="UpdateImageButton">
        <div class="connectform">
            <div>
                <DStvConnect:NavigationMenu runat="server" ID="navigationMenuMyAccount" />
            </div>
            <%--<div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <label title="DStv Connect">
                            <img alt="DStv Connect" src="<%=this.fullImagePath%>dstvconnect.png" /></label>
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectprMenuUpdated">
                        <span class="prMenuItem"><a id="amHelp">
                            <img alt="" src="<%=this.fullImagePath%>pr_aboutme_tick.gif" /></a></span> <span
                                class="prMenuItem"><a href="Account.aspx" id="maHelp">
                                    <img alt="" src="<%=this.fullImagePath%>pr_myaccount_act.gif" /></a></span>
                        <%--                    <span class="prMenuItem""><a href="Alerts.aspx" id="alHelp"><img alt="" src="<%=this.fullImagePath%>pr_myalerts_q.gif" /></a></span>
                    <span class="prMenuItem"><a href="Newsletters.aspx" id="nlHelp"><img alt="" src="<%=this.fullImagePath%>pr_mynewsletters_q.gif" /></a></span>
                    </td>
                    <td>
                        <img alt="" src="<%=this.fullImagePath%>pr_headsep.gif" />
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectlbformHeadTxt">
                        Complete your DStv Connect profile to access exclusive content, special offers and
                        more.
                    </td>
                </tr>
            </table>
        </div>--%>
            <div id="contentContainer" class="connectformItems">
                <div class="connectuserBox" id="divThanks" runat="server" clientidmode="Static" style="display: none">
                    <div class="connecttxt20px">
                        Congratulations
                        <%=Page.User.Identity.Name%></div>
                    <br />
                    Your online entertainment experience just got that much better!<br />
                    Use your DStv Connect details to login to all your favourite DStv websites.
                </div>
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ProfileImage" runat="server" ImageUrl="" Width="125px" Height="125px" />
                                        </td>
                                    </tr>
                                    <asp:Panel ID="UpdateProfilePanel" runat="server" Visible="false">
                                        <tr>
                                            <td>
                                                <asp:HyperLink ID="UpdateProfileHyperLink" runat="server" ValidationGroup="1" Text="Update My Profile pic"
                                                    CssClass="connectsmlLink1" Enabled="false" />
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                </table>
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td style="vertical-align: top;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="connectuserHello">
                                            Hello <span class="connectuserTitle">
                                                <%=Page.User.Identity.Name%></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="connectpadding20px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="connectprogTxt">
                                            You are ready to start completing your profile
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Complete your DStv Connect profile by telling us about yourself or update your account. Access exclusive content and much, much more.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="connectpadding10px">
                </div>
                <div class="connecthdr_21pxGrey_line">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <!-- START SHOW|HIDE GAP BY TITI -->
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        if ($('#ctl00_ctl00_cphMain_cphBody_ctl00_UpdateSuccessLabel').html().trim() != "") {
                                            $('#ctl00_ctl00_cphMain_cphBody_ctl00_UpdateSuccessLabel').css('margin-bottom', '15px');
                                        }
                                    });
                                </script>
                                <!-- END SHOW|HIDE GAP BY TITI -->
                                <!-- <div class="connecthelp"></div> -->
                                <asp:Label Style="position: relative; left: 0px; text-align: left; font-size:24px; display:block; line-height:28px;" ID="UpdateSuccessLabel" runat="server" CssClass="connectuserAvail" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h1>UPDATE YOUR PERSONAL INFO</h1>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="connectfloatLeft">
                    Tell us a little bit about yourself so that we can tailor your DStv experience</div>
                <div class="connectpadding10px">
                </div>
                <div class="connectfloatLeft">
                    <input type="hidden" id="CropInfo" runat="server" />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td colspan="2">
                                <strong>Username</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="UsernameTextBox" MaxLength="25" ValidationGroup="1" runat="server" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField" style="display: none;">*</span>&nbsp;<a rev="Help"
                                                id="unHelp" class="connecthelp"><img alt="" src="<%=this.fullImagePath%>icon_help.gif"
                                                    style="vertical-align: top" /></a>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RequiredFieldValidator ID="UsernameRequiredFieldValidator" runat="server" Enabled="true"
                                                ValidationGroup="1" Display="Dynamic" CssClass="connectformError" ForeColor=""
                                                ControlToValidate="UsernameTextBox">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Please supply a username</div></td>
                                            </tr>
                                        </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="1"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="UsernameTextBox"
                                                ValidationExpression="^[a-zA-Z0-9-]*$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.ValidationMessages.Register.UsernameValid%></div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="connectpadding5px"></div></td>
                                            </tr>
                                        </table>                                            
                                            </asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regExUserNameLength" runat="server" ValidationGroup="1"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="UsernameTextBox"
                                                ValidationExpression="^.{6,25}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.ValidationMessages.Register.UsernameLengthValid%></div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="connectpadding5px"></div></td>
                                            </tr>
                                        </table>
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="connectpadding5px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="CheckAvailableImageButton" runat="server" ValidationGroup="1"
                                                CausesValidation="true" OnClick="CheckAvailability_Click" OnClientClick="if(Page_ClientValidate('1')){ document.getElementById('imgProcessingAvailability').style.display = 'block'; }" />
                                        </td>
                                        <td align="right">
                                            <img runat="server" enableviewstate="false" id="imgProcessingAvailability" clientidmode="Static"
                                                src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none; margin-left: 20px"
                                                alt="" />
                                        </td>
                                        <td class="connectreqField">
                                            <span>&nbsp;<asp:Label ID="UsernameAvailLabel" runat="server" name="UsernameAvailLabel" /></span>
                                            <asp:HiddenField ID="IsAvailableField" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Email Address</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="EmailAddressTextBox" ValidationGroup="1" runat="server" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField" style="display: none;">*</span>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="1" runat="server"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Please supply an email address</div></td>
                                            </tr>
                                        </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="1"
                                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox"
                                                ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Please enter a valid email address</div></td>
                                            </tr>
                                        </table>                                            
                                            </asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="1" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div>Email address already registered</div></td>
                                            </tr>
                                        </table>
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td colspan="2">
                                <strong>Password</strong>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="PasswordTextBox" ValidationGroup="1" TextMode="Password" runat="server" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField"></span>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="1" runat="server"
                                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox"
                                                Enabled="false">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Please supply a password</div></td>
                                            </tr>
                                        </table>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td colspan="2">
                                <strong>Repeat Password</strong>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="RepeatPasswordTextBox" TextMode="Password" runat="server" ValidationGroup="1" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField"></span>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="RepeatPasswordTextBox"
                                                Enabled="false" ValidationGroup="1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Please supply a password</div></td>
                                            </tr>
                                        </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" CssClass="connectformError"
                                                ForeColor="" ControlToValidate="RepeatPasswordTextBox" ControlToCompare="PasswordTextBox"
                                                ValidationGroup="1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Passwords do not match</div></td>
                                            </tr>
                                        </table>                                            
                                            </asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connecthr_dotline1">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>First Name</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="FirstnameTextBox" runat="server" ValidationGroup="1" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="FirstnameTextBox"
                                                ValidationGroup="1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div>Please supply a firstname</div></td>                                                    
                                            </tr>
                                        </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorFirstName" ValidationGroup="1"
                                                ControlToValidate="FirstnameTextBox" runat="server" Display="Dynamic" CssClass="connectformError"
                                                ForeColor="" ValidationExpression="^[a-zA-Z-\s]{1,40}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Only alpha- and no more than 40 characters</div></td>
                                            </tr>
                                        </table>                                          
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Last Name</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="LastnameTextBox" runat="server" ValidationGroup="1" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="LastnameTextBox"
                                                ValidationGroup="1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div>Please supply a lastname</div></td>
                                            </tr>
                                        </table>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorLastName" ValidationGroup="1"
                                                ControlToValidate="LastnameTextBox" runat="server" Display="Dynamic" CssClass="connectformError"
                                                ForeColor="" ValidationExpression="^[a-zA-Z-\s]{1,40}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div>Only alpha- and no more than 40 characters</div></td>
                                            </tr>
                                        </table>                                          
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connecthr_dotline1">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <asp:UpdatePanel ID="CountryUpdatePanel" runat="server" ChildrenAsTriggers="true">
                            <contenttemplate>
                            <tr>
                                <td colspan="2">
                                    <strong>Country</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 275px;">
                                    <div class="connectinputBoxes1">
                                        <!-- TODO: refactor this later, look at a data store of sorts -->
                                        <asp:DropDownList ID="CountryDropDownList" runat="server" ValidationGroup="1" >
                                            <asp:ListItem Text="Please select" Value="0" />
                                            <asp:ListItem Text="Afghanistan" Value="AF" />
                                            <asp:ListItem Text="Albania" Value="AL" />
                                            <asp:ListItem Text="Algeria" Value="DZ" />
                                            <asp:ListItem Text="American Samoa" Value="AS" />
                                            <asp:ListItem Text="Andorra" Value="AD" />
                                            <asp:ListItem Text="Angola" Value="AO" />
                                            <asp:ListItem Text="Anguilla" Value="AI" />
                                            <asp:ListItem Text="Antarctica" Value="AQ" />
                                            <asp:ListItem Text="Antigua and Barbuda" Value="AG" />
                                            <asp:ListItem Text="Argentina" Value="AR" />
                                            <asp:ListItem Text="Armenia" Value="AM" />
                                            <asp:ListItem Text="Aruba" Value="AW" />
                                            <asp:ListItem Text="Australia" Value="AU" />
                                            <asp:ListItem Text="Austria" Value="AT" />
                                            <asp:ListItem Text="Azerbaijan" Value="AZ" />
                                            <asp:ListItem Text="Bahamas" Value="BS" />
                                            <asp:ListItem Text="Bahrain" Value="BH" />
                                            <asp:ListItem Text="Bangladesh" Value="BD" />
                                            <asp:ListItem Text="Barbados" Value="BB" />
                                            <asp:ListItem Text="Belarus" Value="BY" />
                                            <asp:ListItem Text="Belgium" Value="BE" />
                                            <asp:ListItem Text="Belize" Value="BZ" />
                                            <asp:ListItem Text="Benin" Value="BJ" />
                                            <asp:ListItem Text="Bermuda" Value="BM" />
                                            <asp:ListItem Text="Bhutan" Value="BT" />
                                            <asp:ListItem Text="Bolivia" Value="BO" />
                                            <asp:ListItem Text="Bosnia & Herzegovina" Value="BA" />
                                            <asp:ListItem Text="Botswana" Value="BW" />
                                            <asp:ListItem Text="Brazil" Value="BR" />
                                            <asp:ListItem Text="British Indian Ocean Territory" Value="IO" />
                                            <asp:ListItem Text="Brunei Darussalam" Value="BN" />
                                            <asp:ListItem Text="Bulgaria" Value="BG" />
                                            <asp:ListItem Text="Burkina Faso" Value="BF" />
                                            <asp:ListItem Text="Burundi" Value="BI" />
                                            <asp:ListItem Text="Cambodia" Value="KH" />
                                            <asp:ListItem Text="Cameroon" Value="CM" />
                                            <asp:ListItem Text="Canada" Value="CA" />
                                            <asp:ListItem Text="Cape Verde" Value="CV" />
                                            <asp:ListItem Text="Cayman Islands" Value="KY" />
                                            <asp:ListItem Text="Central African Republic" Value="CF" />
                                            <asp:ListItem Text="Chad" Value="TD" />
                                            <asp:ListItem Text="Chile" Value="CL" />
                                            <asp:ListItem Text="China" Value="CN" />
                                            <asp:ListItem Text="Christmas Island" Value="CX" />
                                            <asp:ListItem Text="Cocos (Keeling) Islands" Value="CC" />
                                            <asp:ListItem Text="Colombia" Value="CO" />
                                            <asp:ListItem Text="Comoros" Value="KM" />
                                            <asp:ListItem Text="Congo" Value="CG" />
                                            <asp:ListItem Text="Cook Islands" Value="CK" />
                                            <asp:ListItem Text="Costa Rica" Value="CR" />
                                            <asp:ListItem Text="Cote dIvoire" Value="CI" />
                                            <asp:ListItem Text="Croatia" Value="HR" />
                                            <asp:ListItem Text="Cuba" Value="CU" />
                                            <asp:ListItem Text="Cyprus" Value="CY" />
                                            <asp:ListItem Text="Czech Republic" Value="CZ" />
                                            <asp:ListItem Text="Denmark" Value="DK" />
                                            <asp:ListItem Text="Djibouti" Value="DJ" />
                                            <asp:ListItem Text="Dominica" Value="DM" />
                                            <asp:ListItem Text="Dominican Republic" Value="DO" />
                                            <asp:ListItem Text="Ecuador" Value="EC" />
                                            <asp:ListItem Text="Egypt" Value="EG" />
                                            <asp:ListItem Text="El Salvador" Value="SV" />
                                            <asp:ListItem Text="Equatorial Guinea" Value="GQ" />
                                            <asp:ListItem Text="Eritrea" Value="ER" />
                                            <asp:ListItem Text="Estonia" Value="EE" />
                                            <asp:ListItem Text="Ethiopia" Value="ET" />
                                            <asp:ListItem Text="Falkland Islands" Value="FK" />
                                            <asp:ListItem Text="Faroe Islands" Value="FO" />
                                            <asp:ListItem Text="Fiji" Value="FJ" />
                                            <asp:ListItem Text="Finland" Value="FI" />
                                            <asp:ListItem Text="France" Value="FR" />
                                            <asp:ListItem Text="French Guiana" Value="GF" />
                                            <asp:ListItem Text="French Polynesia" Value="PF" />
                                            <asp:ListItem Text="Gabon" Value="GA" />
                                            <asp:ListItem Text="Gambia" Value="GM" />
                                            <asp:ListItem Text="Georgia" Value="GE" />
                                            <asp:ListItem Text="Germany" Value="DE" />
                                            <asp:ListItem Text="Ghana" Value="GH" />
                                            <asp:ListItem Text="Gibraltar" Value="GI" />
                                            <asp:ListItem Text="Greece" Value="GR" />
                                            <asp:ListItem Text="Greenland" Value="GL" />
                                            <asp:ListItem Text="Grenada" Value="GD" />
                                            <asp:ListItem Text="Guadeloupe" Value="GP" />
                                            <asp:ListItem Text="Guam" Value="GU" />
                                            <asp:ListItem Text="Guatemala" Value="GT" />
                                            <asp:ListItem Text="Guinea" Value="GN" />
                                            <asp:ListItem Text="Guinea-Bissau" Value="GW" />
                                            <asp:ListItem Text="Guyana" Value="GY" />
                                            <asp:ListItem Text="Haiti" Value="HT" />
                                            <asp:ListItem Text="Heard Island" Value="HM" />
                                            <asp:ListItem Text="Honduras" Value="HN" />
                                            <asp:ListItem Text="Hong Kong" Value="HK" />
                                            <asp:ListItem Text="Hungary" Value="HU" />
                                            <asp:ListItem Text="Iceland" Value="IS" />
                                            <asp:ListItem Text="India" Value="IN" />
                                            <asp:ListItem Text="Indonesia" Value="ID" />
                                            <asp:ListItem Text="Iran" Value="IR" />
                                            <asp:ListItem Text="Iraq" Value="IQ" />
                                            <asp:ListItem Text="Ireland" Value="IE" />
                                            <asp:ListItem Text="Israel" Value="IL" />
                                            <asp:ListItem Text="Italy" Value="IT" />
                                            <asp:ListItem Text="Jamaica" Value="JM" />
                                            <asp:ListItem Text="Japan" Value="JP" />
                                            <asp:ListItem Text="Jordan" Value="JO" />
                                            <asp:ListItem Text="Kazakhstan" Value="KZ" />
                                            <asp:ListItem Text="Kenya" Value="KE" />
                                            <asp:ListItem Text="Kiribati" Value="KI" />
                                            <asp:ListItem Text="Korea (North)" Value="KR" />
                                            <asp:ListItem Text="Korea (South)" Value="KP" />
                                            <asp:ListItem Text="Kuwait" Value="KW" />
                                            <asp:ListItem Text="Kyrgyzstan" Value="KG" />
                                            <asp:ListItem Text="Lao" Value="LA" />
                                            <asp:ListItem Text="Latvia" Value="LV" />
                                            <asp:ListItem Text="Lebanon" Value="LB" />
                                            <asp:ListItem Text="Lesotho" Value="LS" />
                                            <asp:ListItem Text="Liberia" Value="LR" />
                                            <asp:ListItem Text="Libyan Arab Jamahiriya" Value="LY" />
                                            <asp:ListItem Text="Liechtenstein" Value="LI" />
                                            <asp:ListItem Text="Lithuania" Value="LT" />
                                            <asp:ListItem Text="Luxembourg" Value="LU" />
                                            <asp:ListItem Text="Macao" Value="MO" />
                                            <asp:ListItem Text="Macedonia" Value="MK" />
                                            <asp:ListItem Text="Madagascar" Value="MG" />
                                            <asp:ListItem Text="Malawi" Value="MW" />
                                            <asp:ListItem Text="Malaysia" Value="MY" />
                                            <asp:ListItem Text="Maldives" Value="MV" />
                                            <asp:ListItem Text="Mali" Value="ML" />
                                            <asp:ListItem Text="Malta" Value="MT" />
                                            <asp:ListItem Text="Marshall Islands" Value="MH" />
                                            <asp:ListItem Text="Martinique" Value="MQ" />
                                            <asp:ListItem Text="Mauritania" Value="MR" />
                                            <asp:ListItem Text="Mauritius" Value="MU" />
                                            <asp:ListItem Text="Mayotte" Value="YT" />
                                            <asp:ListItem Text="Mexico" Value="MX" />
                                            <asp:ListItem Text="Micronesia" Value="FM" />
                                            <asp:ListItem Text="Moldova" Value="MD" />
                                            <asp:ListItem Text="Monaco" Value="MC" />
                                            <asp:ListItem Text="Mongolia" Value="MN" />
                                            <asp:ListItem Text="Montserrat" Value="MS" />
                                            <asp:ListItem Text="Morocco" Value="MA" />
                                            <asp:ListItem Text="Mozambique" Value="MZ" />
                                            <asp:ListItem Text="Myanmar" Value="MM" />
                                            <asp:ListItem Text="Namibia" Value="NA" />
                                            <asp:ListItem Text="Nauru" Value="NR" />
                                            <asp:ListItem Text="Nepal" Value="NP" />
                                            <asp:ListItem Text="Netherlands" Value="NL" />
                                            <asp:ListItem Text="Netherlands Antilles" Value="AN" />
                                            <asp:ListItem Text="New Caledonia" Value="NC" />
                                            <asp:ListItem Text="New Zealand" Value="NZ" />
                                            <asp:ListItem Text="Nicaragua" Value="NI" />
                                            <asp:ListItem Text="Niger" Value="NE" />
                                            <asp:ListItem Text="Nigeria" Value="NG" />
                                            <asp:ListItem Text="Niue" Value="NU" />
                                            <asp:ListItem Text="Norfolk Island" Value="NF" />
                                            <asp:ListItem Text="Northern Mariana Islands" Value="MP" />
                                            <asp:ListItem Text="Norway" Value="NO" />
                                            <asp:ListItem Text="Oman" Value="OM" />
                                            <asp:ListItem Text="Pakistan" Value="PK" />
                                            <asp:ListItem Text="Palau" Value="PW" />
                                            <asp:ListItem Text="Panama" Value="PA" />
                                            <asp:ListItem Text="Papua New Guinea" Value="PG" />
                                            <asp:ListItem Text="Paraguay" Value="PY" />
                                            <asp:ListItem Text="Peru" Value="PE" />
                                            <asp:ListItem Text="Philippines" Value="PH" />
                                            <asp:ListItem Text="Pitcairn" Value="PN" />
                                            <asp:ListItem Text="Poland" Value="PL" />
                                            <asp:ListItem Text="Portugal" Value="PT" />
                                            <asp:ListItem Text="Puerto Rico" Value="PR" />
                                            <asp:ListItem Text="Qatar" Value="QA" />
                                            <asp:ListItem Text="Reunion" Value="RE" />
                                            <asp:ListItem Text="Romania" Value="RO" />
                                            <asp:ListItem Text="Russian Federation" Value="RU" />
                                            <asp:ListItem Text="Rwanda" Value="RW" />
                                            <asp:ListItem Text="Saint Helena" Value="SH" />
                                            <asp:ListItem Text="Saint Kitts and Nevis" Value="KN" />
                                            <asp:ListItem Text="Saint Lucia" Value="LC" />
                                            <asp:ListItem Text="Saint Pierre and Miquelon" Value="PM" />
                                            <asp:ListItem Text="Saint Vincent" Value="VC" />
                                            <asp:ListItem Text="Samoa" Value="WS" />
                                            <asp:ListItem Text="San Marino" Value="SM" />
                                            <asp:ListItem Text="Sao Tome and Principe" Value="ST" />
                                            <asp:ListItem Text="Saudi Arabia" Value="SA" />
                                            <asp:ListItem Text="Senegal" Value="SN" />
                                            <asp:ListItem Text="Serbia and Montenegro" Value="CS" />
                                            <asp:ListItem Text="Seychelles" Value="SC" />
                                            <asp:ListItem Text="Sierra Leone" Value="SL" />
                                            <asp:ListItem Text="Singapore" Value="SG" />
                                            <asp:ListItem Text="Slovakia" Value="SK" />
                                            <asp:ListItem Text="Slovenia" Value="SI" />
                                            <asp:ListItem Text="Solomon Islands" Value="SB" />
                                            <asp:ListItem Text="Somalia" Value="SO" />
                                            <asp:ListItem Text="South Africa" Value="ZA" />
                                            <asp:ListItem Text="South Georgia" Value="GS" />
                                            <asp:ListItem Text="Spain" Value="ES" />
                                            <asp:ListItem Text="Sri Lanka" Value="LK" />
                                            <asp:ListItem Text="Sudan" Value="SD" />
                                            <asp:ListItem Text="Suriname" Value="SR" />
                                            <asp:ListItem Text="Svalbard and Jan Mayen" Value="SJ" />
                                            <asp:ListItem Text="Swaziland" Value="SZ" />
                                            <asp:ListItem Text="Sweden" Value="SE" />
                                            <asp:ListItem Text="Switzerland" Value="CH" />
                                            <asp:ListItem Text="Syrian Arab Republic" Value="SY" />
                                            <asp:ListItem Text="Taiwan" Value="TW" />
                                            <asp:ListItem Text="Tajikistan" Value="TJ" />
                                            <asp:ListItem Text="Tanzania" Value="TZ" />
                                            <asp:ListItem Text="Thailand" Value="TH" />
                                            <asp:ListItem Text="Timor-Leste" Value="TL" />
                                            <asp:ListItem Text="Togo" Value="TG" />
                                            <asp:ListItem Text="Tonga" Value="TO" />
                                            <asp:ListItem Text="Trinidad and Tobago" Value="TT" />
                                            <asp:ListItem Text="Tunisia" Value="TN" />
                                            <asp:ListItem Text="Turkey" Value="TR" />
                                            <asp:ListItem Text="Turkmenistan" Value="TM" />
                                            <asp:ListItem Text="Turks and Caicos Islands" Value="TC" />
                                            <asp:ListItem Text="Tuvalu" Value="TV" />
                                            <asp:ListItem Text="Uganda" Value="UG" />
                                            <asp:ListItem Text="Ukraine" Value="UA" />
                                            <asp:ListItem Text="United Arab Emirates" Value="AE" />
                                            <asp:ListItem Text="United Kingdom" Value="GB" />
                                            <asp:ListItem Text="United States" Value="US" />
                                            <asp:ListItem Text="Uruguay" Value="UY" />
                                            <asp:ListItem Text="Uzbekistan" Value="UZ" />
                                            <asp:ListItem Text="Vanuatu" Value="VU" />
                                            <asp:ListItem Text="Vatican" Value="VA" />
                                            <asp:ListItem Text="Venezuela" Value="VE" />
                                            <asp:ListItem Text="Viet Nam" Value="VN" />
                                            <asp:ListItem Text="Virgin Islands" Value="VI" />
                                            <asp:ListItem Text="Wallis and Futuna" Value="WF" />
                                            <asp:ListItem Text="Western Sahara" Value="EH" />
                                            <asp:ListItem Text="Yemen" Value="YE" />
                                            <asp:ListItem Text="Zambia" Value="ZM" />
                                            <asp:ListItem Text="Zimbabwe" Value="ZW" />
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span class="connectreqField">*</span>
                                            </td>
                                            <td style="padding-left: 5px;">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                                                    CssClass="connectformError" ForeColor="" ControlToValidate="CountryDropDownList"
                                                    InitialValue="0" ValidationGroup="1">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td class="connectformError"><div>Please indicate your country</div></td>
                                                    </tr>
                                                </table>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="connectpadding10px">
                                    </div>
                                </td>
                            </tr>
                            <% 
                                if (this.CountryDropDownList.SelectedItem.Value == "ZA")
                                { 
                            %>
                            <tr>
                                <td colspan="2">
                                    <strong>Province</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 275px;">
                                    <div class="connectinputBoxes">
                                        <asp:TextBox ID="ProvinceTextBox" runat="server" /></div>
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="1"
                                        ControlToValidate="ProvinceTextBox" runat="server" Display="Dynamic" CssClass="formError"
                                        ForeColor="" ValidationExpression="^[a-zA-Z-\s]{1,40}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="formError"><div>Only alpha- and no more than 40 characters</div></td>
                                            </tr>
                                        </table>                                          
                                    </asp:RegularExpressionValidator>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="padding-left: 5px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                        </contenttemplate>
                        </asp:UpdatePanel>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Mobile Number</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <asp:TextBox ID="MobileNumberTextBox" runat="server" ValidationGroup="1" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <span class="connectreqField" style="display: none;">*</span>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="1"
                                                ControlToValidate="MobileNumberTextBox" runat="server" Display="Dynamic" CssClass="connectformError"
                                                ForeColor="" ValidationExpression="^[0-9\s\(\)\+\-]{8,16}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>Please enter a valid international mobile number</div></td>
                                            </tr>
                                        </table>                                          
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connecthr_dotline1">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Date of Birth</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes1">
                                    <asp:DropDownList ID="DayDropDownList" runat="server" ValidationGroup="1">
                                        <asp:ListItem Text="Day" Value="-1" Selected="True" />
                                        <asp:ListItem Text="1" Value="01" />
                                        <asp:ListItem Text="2" Value="02" />
                                        <asp:ListItem Text="3" Value="03" />
                                        <asp:ListItem Text="4" Value="04" />
                                        <asp:ListItem Text="5" Value="05" />
                                        <asp:ListItem Text="6" Value="06" />
                                        <asp:ListItem Text="7" Value="07" />
                                        <asp:ListItem Text="8" Value="08" />
                                        <asp:ListItem Text="9" Value="09" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                        <asp:ListItem Text="21" Value="21" />
                                        <asp:ListItem Text="22" Value="22" />
                                        <asp:ListItem Text="23" Value="23" />
                                        <asp:ListItem Text="24" Value="24" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="26" Value="26" />
                                        <asp:ListItem Text="27" Value="27" />
                                        <asp:ListItem Text="28" Value="28" />
                                        <asp:ListItem Text="29" Value="29" />
                                        <asp:ListItem Text="30" Value="30" />
                                        <asp:ListItem Text="31" Value="31" />
                                    </asp:DropDownList>
                                </div>
                                <div class="connectinputBoxes1">
                                    <asp:DropDownList ID="MonthDropDownList" runat="server" ValidationGroup="1">
                                        <asp:ListItem Text="Month" Value="-1" Selected="True" />
                                        <asp:ListItem Text="January" Value="01" />
                                        <asp:ListItem Text="February" Value="02" />
                                        <asp:ListItem Text="March" Value="03" />
                                        <asp:ListItem Text="April" Value="04" />
                                        <asp:ListItem Text="May" Value="05" />
                                        <asp:ListItem Text="June" Value="06" />
                                        <asp:ListItem Text="July" Value="07" />
                                        <asp:ListItem Text="August" Value="08" />
                                        <asp:ListItem Text="September" Value="09" />
                                        <asp:ListItem Text="October" Value="10" />
                                        <asp:ListItem Text="November" Value="11" />
                                        <asp:ListItem Text="December" Value="12" />
                                    </asp:DropDownList>
                                </div>
                                <div class="connectinputBoxes1">
                                    <asp:DropDownList ID="YearDropDownList" runat="server" ValidationGroup="1">
                                        <asp:ListItem Text="Year" Value="-1" Selected="True" />
                                        <asp:ListItem Text="1910" Value="1910 " />
                                        <asp:ListItem Text="1911" Value="1911 " />
                                        <asp:ListItem Text="1912" Value="1912 " />
                                        <asp:ListItem Text="1913" Value="1913 " />
                                        <asp:ListItem Text="1914" Value="1914 " />
                                        <asp:ListItem Text="1915" Value="1915 " />
                                        <asp:ListItem Text="1916" Value="1916 " />
                                        <asp:ListItem Text="1917" Value="1917 " />
                                        <asp:ListItem Text="1918" Value="1918 " />
                                        <asp:ListItem Text="1919" Value="1919 " />
                                        <asp:ListItem Text="1920" Value="1920 " />
                                        <asp:ListItem Text="1921" Value="1921 " />
                                        <asp:ListItem Text="1922" Value="1922 " />
                                        <asp:ListItem Text="1923" Value="1923 " />
                                        <asp:ListItem Text="1924" Value="1924 " />
                                        <asp:ListItem Text="1925" Value="1925 " />
                                        <asp:ListItem Text="1926" Value="1926 " />
                                        <asp:ListItem Text="1927" Value="1927 " />
                                        <asp:ListItem Text="1928" Value="1928 " />
                                        <asp:ListItem Text="1929" Value="1929 " />
                                        <asp:ListItem Text="1930" Value="1930 " />
                                        <asp:ListItem Text="1931" Value="1931 " />
                                        <asp:ListItem Text="1932" Value="1932 " />
                                        <asp:ListItem Text="1933" Value="1933 " />
                                        <asp:ListItem Text="1934" Value="1934 " />
                                        <asp:ListItem Text="1935" Value="1935" />
                                        <asp:ListItem Text="1936" Value="1936" />
                                        <asp:ListItem Text="1937" Value="1937" />
                                        <asp:ListItem Text="1938" Value="1938" />
                                        <asp:ListItem Text="1939" Value="1939" />
                                        <asp:ListItem Text="1940" Value="1940" />
                                        <asp:ListItem Text="1941" Value="1941" />
                                        <asp:ListItem Text="1942" Value="1942" />
                                        <asp:ListItem Text="1943" Value="1943" />
                                        <asp:ListItem Text="1944" Value="1944" />
                                        <asp:ListItem Text="1945" Value="1945" />
                                        <asp:ListItem Text="1946" Value="1946" />
                                        <asp:ListItem Text="1947" Value="1947" />
                                        <asp:ListItem Text="1948" Value="1948" />
                                        <asp:ListItem Text="1949" Value="1949" />
                                        <asp:ListItem Text="1950" Value="1950" />
                                        <asp:ListItem Text="1951" Value="1951" />
                                        <asp:ListItem Text="1952" Value="1952" />
                                        <asp:ListItem Text="1953" Value="1953" />
                                        <asp:ListItem Text="1954" Value="1954" />
                                        <asp:ListItem Text="1955" Value="1955" />
                                        <asp:ListItem Text="1956" Value="1956" />
                                        <asp:ListItem Text="1957" Value="1957" />
                                        <asp:ListItem Text="1958" Value="1958" />
                                        <asp:ListItem Text="1959" Value="1959" />
                                        <asp:ListItem Text="1960" Value="1960" />
                                        <asp:ListItem Text="1961" Value="1961" />
                                        <asp:ListItem Text="1962" Value="1962" />
                                        <asp:ListItem Text="1963" Value="1963" />
                                        <asp:ListItem Text="1964" Value="1964" />
                                        <asp:ListItem Text="1965" Value="1965" />
                                        <asp:ListItem Text="1966" Value="1966" />
                                        <asp:ListItem Text="1967" Value="1967" />
                                        <asp:ListItem Text="1968" Value="1968" />
                                        <asp:ListItem Text="1969" Value="1969" />
                                        <asp:ListItem Text="1970" Value="1970" />
                                        <asp:ListItem Text="1971" Value="1971" />
                                        <asp:ListItem Text="1972" Value="1972" />
                                        <asp:ListItem Text="1973" Value="1973" />
                                        <asp:ListItem Text="1974" Value="1974" />
                                        <asp:ListItem Text="1975" Value="1975" />
                                        <asp:ListItem Text="1976" Value="1976" />
                                        <asp:ListItem Text="1977" Value="1977" />
                                        <asp:ListItem Text="1978" Value="1978" />
                                        <asp:ListItem Text="1979" Value="1979" />
                                        <asp:ListItem Text="1980" Value="1980" />
                                        <asp:ListItem Text="1981" Value="1981" />
                                        <asp:ListItem Text="1982" Value="1982" />
                                        <asp:ListItem Text="1983" Value="1983" />
                                        <asp:ListItem Text="1984" Value="1984" />
                                        <asp:ListItem Text="1985" Value="1985" />
                                        <asp:ListItem Text="1986" Value="1986" />
                                        <asp:ListItem Text="1987" Value="1987" />
                                        <asp:ListItem Text="1988" Value="1988" />
                                        <asp:ListItem Text="1989" Value="1989" />
                                        <asp:ListItem Text="1990" Value="1990" />
                                        <asp:ListItem Text="1991" Value="1991" />
                                        <asp:ListItem Text="1992" Value="1992" />
                                        <asp:ListItem Text="1993" Value="1993" />
                                        <asp:ListItem Text="1994" Value="1994" />
                                        <asp:ListItem Text="1995" Value="1995" />
                                        <asp:ListItem Text="1996" Value="1996" />
                                        <asp:ListItem Text="1997" Value="1997" />
                                        <asp:ListItem Text="1998" Value="1998" />
                                        <asp:ListItem Text="1999" Value="1999" />
                                        <asp:ListItem Text="2000" Value="2000" />
                                        <asp:ListItem Text="2001" Value="2001" />
                                        <asp:ListItem Text="2002" Value="2002" />
                                        <asp:ListItem Text="2003" Value="2003" />
                                        <asp:ListItem Text="2004" Value="2004" />
                                        <asp:ListItem Text="2005" Value="2005" />
                                        <asp:ListItem Text="2006" Value="2006" />
                                        <asp:ListItem Text="2007" Value="2007" />
                                        <asp:ListItem Text="2008" Value="2008" />
                                        <asp:ListItem Text="2009" Value="2009" />
                                        <asp:ListItem Text="2010" Value="2010" />
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-left: 5px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--<tr>
                        <td style="width: 275px;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="bottom">
                                        <div class="connectinputBoxes1">
                                            <asp:TextBox runat="server" ID="txtDateOfBirth" MaxLength="10"></asp:TextBox>
                                            <asp:CalendarExtender ID="calenderDOB" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDateOfBirth"
                                                PopupButtonID="imageButtonShowCalender">
                                            </asp:CalendarExtender>
                                            <asp:TextBoxWatermarkExtender ID="waterMarkDateOfBirth" runat="server" TargetControlID="txtDateOfBirth"
                                                WatermarkText="DD/MM/YYYY">
                                            </asp:TextBoxWatermarkExtender>
                                            <asp:ImageButton ID="imageButtonShowCalender" runat="server" Width="15px" Height="20px"
                                                AlternateText="Click to enter date" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="connectfieldMsgs">
                            <asp:RegularExpressionValidator runat="server" ID="regExDateOfBirth" ValidationGroup="1"
                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtDateOfBirth"
                                ValidationExpression="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d">
                                <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2"> <div style="clear:both;padding-top:5px;"></div></td>
                                </tr>
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Register.DateOfBirthValidity%></div></td>
                                </tr>
                                </table>
                            </asp:RegularExpressionValidator>
                            <asp:CompareValidator runat="server" ID="compareDateOfBirth" ValidationGroup="1"
                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtDateOfBirth"
                                Operator="LessThan" Type="Date" ValueToCompare="<%# DateTime.Today.ToShortDateString() %>">
                                <table border="0" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td colspan="2"> <div style="clear:both;padding-top:5px;"></div></td>
                                </tr>
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Register.CompareDateOfBirth%></div></td>
                                </tr>
                                </table>
                            </asp:CompareValidator>
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Gender</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <asp:RadioButtonList ID="GenderRadioButtonList" runat="server">
                                    <asp:ListItem Text="Male" Value="Male" />
                                    <asp:ListItem Text="Female" Value="Female" />
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-left: 5px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Upload a profile picture (file size 4 mb maximum)</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 275px;">
                                <div class="connectinputBoxes">
                                    <%-- <asp:FileUpload ID="FileUpload1" runat="server"/></div>--%>
                                    <asp:FileUpload ID="FileUpload1" EnableViewState="true" runat="server" onchange="javascript:loadPhoto();" /></div>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:Label ID="FileSizeLimitLabel" runat="server" Visible="false">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td class="connectformError"><div>File size must be 4 mb or less</div></td>
                                            </tr>
                                        </table>
                                            </asp:Label>
                                            <asp:Label ID="AnErrorLabel" CssClass="connectformError" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!--Added by Ujjaval-->
                        <%if (!string.IsNullOrEmpty(photo.ImageUrl))
                          {%>
                        <tr>
                            <td>
                                <%-- <div class="frame" style="margin: 0 0.3em; width: 300px; height: 300px;">--%>
                                <a id="cropimage"></a>
                                <asp:Image runat="server" ID="photo" Width="300" />
                                <%--</div>--%>
                            </td>
                            <td>
                                <div id="preview" style="width: 125px; height: 125px; overflow: hidden;">
                                    <img runat="server" id="SelectedPhoto" />
                                </div>
                                <br />
                                Preview
                            </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connecthr_dotline1">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding10px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Brief description about yourself (Max 300 characters)</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px;">
                                <div class="connectinputBoxes1">
                                    <%--<asp:TextBox TextMode="MultiLine" Rows="10" Width="400" onKeyUp="javascript:Count(this,300);" onChange="javascript:Count(this,300);" ID="txtBio" runat="server" /></div>--%>
                                    <asp:TextBox TextMode="MultiLine" Rows="10" Width="350px" onkeypress="return textboxMultilineMaxNumber(this,300,event);"
                                        ID="txtBio" runat="server" /></div>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="BioRadioButtonList" runat="server">
                                    <asp:ListItem Selected Text="Private" Value="Private"></asp:ListItem>
                                    <asp:ListItem Text="Public" Value="Public"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-left: 5px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RegularExpressionValidator ID="BioRegularExpression" ValidationGroup="1" ControlToValidate="txtBio"
                                    runat="server" Display="Dynamic" CssClass="formError" ForeColor="" ValidationExpression="^^.{0,300}$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="formError"><div>Brief Description can have maximum 300 characters.</div></td>
                                            </tr>
                                        </table>                                          
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <!--Added upto here -->
                    </table>
                </div>
                <div class="connecthr_dotline2">
                </div>
                <div class="connectpadding10px">
                </div>
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top; text-align: center;">
                                <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="UpdateImageButton" runat="server" OnClick="UpdateProfile_Click"
                                                ValidationGroup="1" OnClientClick="encodeMyHtml();if(Page_ClientValidate('1')){ document.getElementById('imgProcessingSubmit').style.display = 'block'; }" />
                                        </td>
                                        <td align="right">
                                            <img runat="server" enableviewstate="false" id="imgProcessingSubmit" clientidmode="Static"
                                                src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none; margin-left: 20px"
                                                alt="" />
                                        </td>
                                        <td>
                                            <div class="connecthelp">
                                                <asp:Label Style="position: relative; left: 50px; text-align: left" ID="UpdateSuccessLabel1"
                                                    runat="server" CssClass="connectuserAvail" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectpadding5px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="connectformDesc">
                                    Fields marked with<span class="connecterrorTxt">*</span>&nbsp;&nbsp;&nbsp;are required
                                    fields</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="connectlbformRight">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="connectlbHead">
                            About DStv Connect
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight: normal">
                                DStv Connect is your portal to getting so much more online.</p>
                            <br />
                            <p style="font-weight: normal">
                                Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label title="DStv Connect Availability">
                                <img alt="DStv Connect Availability" src="<%=this.fullImagePath%>logossml.gif" /></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</span>