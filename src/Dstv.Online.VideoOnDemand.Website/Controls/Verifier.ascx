﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Verifier.ascx.cs" Inherits="DStvConnect.WebUI.Controls.Verifier" %>
<span class="ConnectContainer">
    <div class="connectform">
        <div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 20px;">
                    </td>
                    <td>
                        <label title="DStv Connect">
                            <img alt="DStv Connect" src="<%=this.ConnectImagePath%>dstvconnect.png" /></label>
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td>
                        <img alt="" src="<%=this.ConnectImagePath%>pr_headsep.gif" />
                    </td>
                    <td style="width: 15px;">
                    </td>
                    <td class="connectlbformHeadTxt">
                        Use your DStv Connect details to login to all your favourite DStv websites
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectformItems">
            <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
                <tr>
                    <td style="text-align: center;">
                        <div class="connectlbformThnxHead">
                            Congratulations
                            <%=this.Username%></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding10px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="connectformTitleText">Your online entertainment experience just got that
                            much better</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding20px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style='margin: 0px auto; width: 400px; text-align: center; border: 0px solid green'>
                            <b>Loading...</b><br />
                            <asp:Image runat="server" ID="loadingImage" ImageUrl="<%=this.ConnectImagePath%>verifierLoader.gif" />
                            <br />
                            <asp:Label ID="UsernameValidationLabel" runat="server" Text="Label" Style='color: red'
                                Visible='False'></asp:Label>
                        </div>
                        <div class="connectborderDiv">
                            <div class="padding20">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <img src="<%=this.ConnectImagePath%>social_temp.jpg" />
                                        </td>
                                        <td style="width: 15px;">
                                        </td>
                                        <td>
                                            <strong>Complete your DStv Connect Profile now and receive a range of benefits</strong>
                                            <br />
                                            Watch premium video content on DStv, SuperSport, DStv On Demand and Omusic. Link
                                            to social networks. Get reminders for your favourite shows. Receive newsletters
                                            and much much more.
                                            <br />
                                            <br />
                                            <a href="" runat="server" id="lnkRedirect">
                                                <img alt="" src="<%=this.ConnectImagePath%>btn_updateprofile.gif" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td colspan="3">
                                            Want to update later? Why not check out whats on DStv in the TV Guide or browse
                                            a large selection of online video in our On Demand section.
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="connectlbformRight">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="connectlbHead">
                        <p>
                            About DStv Connect</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-weight: normal">
                            DStv Connect is your portal to getting so much more online.</p>
                        <br />
                        <p style="font-weight: normal">
                            Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                        <ul class="connectlb">
                            <li class="connectlb">Watch premium video content on DStv, SuperSport, DStv On Demand</li>
                            <li class="connectlb">Manage your personal subscriber info</li>
                            <li class="connectlb">Access special offers and competitions</li>
                            <li class="connectlb">Get all the news as and when it breaks</li>
                            <li class="connectlb">Download music</li>
                        </ul>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                        <a href="SignUp.aspx">Don't have a DStv Connect ID?</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label title="DStv Connect Availability">
                            <img alt="DStv Connect Availability" src="<%=this.ConnectImagePath%>logossml.gif" /></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</span>