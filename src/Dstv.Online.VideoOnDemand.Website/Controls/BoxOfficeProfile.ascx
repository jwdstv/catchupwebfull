﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BoxOfficeProfile.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.BoxOfficeProfile" %>
    <span class="ConnectContainer">
<asp:Panel ID="panelBoxProfile" runat="server">
    <div class="connectform">
        <div class="connectformHead">
            <table border="0" cellpadding="0" cellspacing="0">
            </table>
        </div>
    </div>
    <div class="connectformItems">
        <div class="connectformLeft">
        </div>
        <div class="connectpadding10px">
        </div>
        <div class="connecthdr_21pxGrey_line">
            <h1>
                BoxOffice
            </h1>
        </div>
        <div class="connectfloatLeft">
            Sign uo for BoxOffice and rent movies online for only R25.95 for SD and R29.95 for
            HD
        </div>
        <div class="connectpadding10px">
        </div>
        <div class="connectfloatLeft">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td>
                        <strong>First Name</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtFirstName" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Last Name</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtLastName" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Residential Address</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtResAddress1" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtResAddress2" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtResAddress3" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtResAddress4" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Billing Address</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtBillAddress1" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtBillAddress2" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtBillAddress3" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:TextBox ID="txtBillAddress4" MaxLength="25" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Country</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <div class="connectinputBoxes">
                            <asp:DropDownList runat="server" ID="ddlCountry" ValidationGroup="1">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <span class="connectreqField" style="display: none;">*</span>&nbsp;<a rev="Help"
                                        id="unHelp" class="connecthelp"><img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:RequiredFieldValidator runat="server" ValidationGroup="1" ID="rfvCountry" ControlToValidate="ddlCountry">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.validationMessages.CountryValidation.CountryRequired%></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>
                            </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>ISP email address</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        The email address supplied to you by your Internet Service Provider
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <asp:TextBox runat="server" ID="txtISMEmailId"></asp:TextBox>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <span class="connectreqField" style="display: none;">*</span>&nbsp;<a rev="Help"
                                        id="A1" class="connecthelp"><img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvISPEmailId" ControlToValidate="txtISMEmailId"
                                        ValidationGroup="1" Display="Dynamic" CssClass="connectformError" ForeColor="">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.validationMessages.ISPEmailId.ISPEmailIdRequired%></div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="connectpadding5px"></div></td>
                                            </tr>
                                        </table>
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator runat="server" ID="regExISPEmailId" ControlToValidate="txtISMEmailId"
                                        ValidationGroup="1" Display="Dynamic" CssClass="connectformError" ForeColor=""
                                        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                </td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.validationMessages.ISPEmailId.ISPEmailIdValidity%></div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectpadding5px"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>ISP password</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <asp:TextBox runat="server" ID="txtISPPassword" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <span class="connectreqField" style="display: none;">*</span>&nbsp;<a rev="Help"
                                        id="A2" class="connecthelp"><img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvISPPassword" ControlToValidate="txtISMEmailId"
                                        ValidationGroup="1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.validationMessages.ISPEmailId.ISPPasswordRequired%></div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="connectpadding5px"></div></td>
                                            </tr>
                                        </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Is my ISP supported</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Cellphone Number</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <asp:TextBox runat="server" ID="txtCellPhone1"></asp:TextBox>
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <span class="connectreqField" style="display: none;">*</span>&nbsp;<a rev="Help"
                                        id="A3" class="connecthelp"><img alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                </td>
                                <td style="padding-left: 5px;">
                                    <asp:RequiredFieldValidator runat="server" ID="rfvCellNumber1" ControlToValidate="txtCellPhone1"
                                        ValidationGroup="1">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                <td>&nbsp;</td>
                                                <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.CellPhoneRequired%></div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="connectpadding5px"></div></td>
                                            </tr>
                                        </table>
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <asp:TextBox runat="server" ID="txtCellPhone2"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <asp:TextBox runat="server" ID="txtCellPhone3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 275px;">
                        <asp:TextBox runat="server" ID="txtCellPhone4"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Enter a 4 digit pin for BoxOffice video</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        You will need to enter the pin on your TV each time uyou rent a BoxOffice
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectinputBoxes1">
                            <asp:TextBox runat="server" ID="txtPin1" TextMode="Password" MaxLength="1" size="1"></asp:TextBox>&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txtPin2" TextMode="Password" MaxLength="1" size="1"></asp:TextBox>&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txtPin3" TextMode="Password" MaxLength="1" size="1"></asp:TextBox>&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="txtPin4" TextMode="Password" MaxLength="1" size="1"></asp:TextBox>&nbsp;&nbsp;
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CustomValidator runat="server" ID="custPinValidation" ValidationGroup="101"
                            Display="Dynamic" CssClass="connectformError" ForeColor="" OnServerValidate="Pin_ServerValidate">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.validationMessages.CellphoneWithPin.BoxOfficePinRequired%></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>
                            </table>
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="chkTC" Text="I accept On Demand and DStv Wallet Terms and Conditions" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:ImageButton on runat="server" ID="btnRegisterBoxOffice" OnClick="btnRegisterBoxOffice_Click" />
                        <a class="connecthelp" style="display: none">
                            <img alt="" src="" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
</span>
