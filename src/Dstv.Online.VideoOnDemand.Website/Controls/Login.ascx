﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="DStvConnect.WebUI.Controls.Login" %>
<span class="ConnectContainer">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="LoginImageButton">
        <div class="connectform">
            <div class="connectformHead">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td>
                            <label title="DStv Connect">
                                <img alt="DStv Connect" src="<%=this.ConnectImagePath%>dstvconnect.png" /></label>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td>
                            <img alt="" src="<%=this.ConnectImagePath%>pr_headsep.gif" />
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectlbformHeadTxt">
                            Login with your DStv Connect details
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectlbformItems">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td colspan="2" class="connectlbHeadBlue">
                         Start accessing exclusive content and more by logging into DStv Connect now.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:CustomValidator ID="InvalidOperationCustomValidator" runat="server" ValidationGroup="101"
                                Display="Dynamic" CssClass="connectformError" ForeColor="">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Login.AuthenticationFailed %></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>
                            </table>                            
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <strong>Email Address</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 251px;">
                            <div class="connectinputBoxes">
                                <asp:TextBox ID="EmailAddressTextBox" class='EmailAddressTextBoxSelector' runat="server" ValidationGroup="101" /></div>
                        </td>
                        <td class="connectreqField">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ID="EmailAddressRequiredValidator" ValidationGroup="101"
                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Login.EmailRequired%></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>
                            </table>
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="EmailAddressRegexValidator" ValidationGroup="101"
                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox"
                                ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Login.EmailValidity%></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>                                
                            </table>
                            </asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="EmailAddressCustomValidator" ValidationGroup="101" runat="server"
                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="EmailAddressTextBox">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="connectformError">
                                            <div>
                                                <asp:Label ID="UsernameValidationLabel" runat="server" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding5px">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <strong>Password</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 251px;">
                            <div class="connectinputBoxes">
                                <asp:TextBox ID="PasswordTextBox" class='PasswordTextBoxSelector' runat="server" TextMode="Password" /></div>
                        </td>
                        <td class="connectreqField">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ID="PasswordRequiredValidator" ValidationGroup="101"
                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Login.PasswordRequired%></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>
                            </table>
                            </asp:RequiredFieldValidator><asp:CustomValidator ID="PasswordCustomValidator" ValidationGroup="101"
                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="connectformError">
                                            <div>
                                                <asp:Label ID="PasswordValidationLabel" runat="server" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding5px">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:CustomValidator><asp:RegularExpressionValidator ID="PasswordLengthValidator"
                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox"
                                ValidationExpression="^.{6,15}$">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                    <td>&nbsp;</td>
                                    <td class="connectformError"><div><%=this.ValidationMessages.Login.PasswordValidity%></div></td>
                                </tr>
                                <tr>
                                    <td><div class="connectpadding5px"></div></td>
                                </tr>
                            </table>                                            
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 251px; text-align: left;">
                            <asp:CheckBox ID="RemeberMeCheckBox" runat="server" Font-Bold="true" Text="&nbsp;Remember me next time" />
                        </td>
                        <td class="connectreqField">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="LoginImageButton" runat="server" OnClick="OnLogin_Click" ValidationGroup="101"
                                            OnClientClick="if(Page_ClientValidate('101')){ document.getElementById('imgProcessingLogin').style.display = 'block'; }" />

                              
                                    </td>
                                    <td>
                                        <img runat="server" enableviewstate="false" id="imgProcessingLogin" clientidmode="Static"
                                            src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none;margin-left : 20px" alt="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:HyperLink ID="ForgotHyperLink" runat="server" CssClass="connectsmlLink" Text="Forgot Password?"
                                NavigateUrl="~/Forgotten.aspx" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectlbformRight">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="connectlbHead">
                            About DStv Connect
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight: normal">
                                DStv Connect is your portal to getting so much more online. Register or login and manage your account.</p>
                            <p style="font-weight: normal">
                                Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="RegisterHyperLink" runat="server" Text="Don't have a DStv Connect ID?"
                                NavigateUrl="~/SignUp.aspx" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label title="DStv Connect Availability">
                                <img alt="DStv Connect Availability" src="<%=this.ConnectImagePath%>logossml.gif" /></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</span>