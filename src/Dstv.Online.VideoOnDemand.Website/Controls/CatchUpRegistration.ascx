﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeBehind="CatchUpRegistration.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.CatchUpRegistration" %>
<%@ Register Src="~/Controls/CountryControl.ascx"
    TagName="countryControl" TagPrefix="DstvConnect" %>
<%@ Register Src="~/Controls/SmartCard.ascx"
    TagName="smartCard" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/ISPEmailId.ascx"
    TagName="iSPEmailId" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/NonDStvCatchUp.ascx"
    TagName="nonDStvCatchUp" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/NavigationMenu.ascx"
    TagName="navigationMenu" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/NonSADisclaimer.ascx"
    TagName="nonSADisclaimer" TagPrefix="DStvConnect" %>
<style type="text/css">
    .Notification
    {
        padding-bottom: 6px;
        color: #7cb42b;
        font-size: 22px;
        padding-top: 4px;
    }
</style>
<span class="ConnectContainer">
    <asp:Panel ID="panelRegistration"
        runat="server" DefaultButton="">
        <div class="connectform">
            <div>
                <DStvConnect:navigationMenu runat="server"
                    ID="navigationMenuMyAccount" />
            </div>
            <asp:MultiView runat="server" ID="multiviewMain"
                ActiveViewIndex="0">
                <asp:View runat="server" ID="mainView1">
                    <div>
                        <DStvConnect:nonSADisclaimer runat="server"
                            ID="nonSADisclaimerControl" />
                    </div>
                </asp:View>
                <asp:View runat="server" ID="mainView2">
                    <div id="contentContainer" style="width: 940px;">
                        <%--<div class="connectformTabs">
                            <% if (System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] == "true")
                               {%>
                            <div class="tab_BO">
                                <a href="BoxOffice.aspx">BoxOffice</a></div>
                            <%} %>
                            <% if (System.Configuration.ConfigurationManager.AppSettings["showCatchUpTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showCatchUpTab"] == "true")
                               {%>
                            <div class="tab_CU">
                                CatchUp</div>
                            <%} %>
                            <% if (System.Configuration.ConfigurationManager.AppSettings["showOMusicTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showOMusicTab"] == "true")
                               {%>
                            <div class="tab_OM">
                                <a href="OMusic.aspx">OMusic</a></div>
                            <%} %>
                        </div>--%>
                        <div class="connectformItems">
                            <div style="display: none">
                                <div class="connectuserBox" id="divThanks"
                                    runat="server" clientidmode="Static">
                                    <div class="connecttxt30px">
                                        Thank You</div>
                                    <br />
                                    <div runat="server" id="divThnksMessage">
                                        Thanks for registering with DStv
                                        Connect!<br />
                                        You can now sign up for CatchUp
                                        by completing the form below.
                                    </div>
                                </div>
                            </div>
                            <div id="CongratulationsMessage"
                                style="display: none">
                                <div class="Notification">
                                    CONGRATULATIONS!</div>
                                <div class="connecthdr_21pxGrey_line"
                                    style="font-size: 12px; color: #626262;">
                                    You are ready to start watching
                                    video on DStv On Demand. <a href="../">
                                        Click here to Browse.</a>
                                </div>
                                <script language="javascript" type="text/javascript">

                                    function checkIfLinked() {
                                        try {
                                            if ($('#connectSuccessfulLinkText') != null &&
                                            $('#connectSuccessfulLinkText').html() != null &&
                                            $('#connectSuccessfulLinkText').html().indexOf('linked.') > 0) {
                                                return true;
                                            }
                                        }
                                        catch (e) {
                                        }

                                        return false;
                                    }

                                    function updateCongratulationMessageDisplay() {
                                        if (checkIfLinked()) {
                                            $('#CongratulationsMessage').show();
                                            $('#trNotPremiumSubscriberLink').hide();
                                        }
                                        else {
                                            $('#CongratulationsMessage').hide();
                                            $('#trNotPremiumSubscriberLink').show();
                                        }

                                    }

                                    $(function () {
                                        updateCongratulationMessageDisplay();
                                    });
                                </script>
                            </div>
                            <div class="connectfloatLeft">
                                <table border="0" cellpadding="0"
                                    cellspacing="0" width="100%">
                                    <tr id="trLinkAccountInfoHead">
                                        <td>
                                            <div class="connecthdr_21pxGrey_line">
                                                <h1>
                                                    LINK YOUR ACCOUNT</h1>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="panelSub" runat="server">
                                                <%--Change by Puneet--%>
                                                <%-- <div class="connectfloatLeft" style="margin-top:5px;">
                                                    Sign up for CatchUp and watch your favourite TV shows, sport and movies online whenever
                                                    you want!
                                                    <br />
                                                    <br />
                                                    <a href="http://care.dstv.com/main.aspx?ID=1695">Not a DStv Premium Subscriber?</a>
                                                </div>
                                                <br />
                                                <br />--%>
                                            </asp:Panel>
                                            <%--Change by Puneet--%>
                                            <%-- <br />
                                            <br />--%>
                                            <asp:Panel ID="panelNonSub" runat="server"
                                                Visible="false">
                                                <div class="connectinfoBox">
                                                    CatchUp lets you watch what you
                                                    missed on DStv over the past 7
                                                    days. To sign up for CatchUp you
                                                    need to be a DStv Premium subscriber.
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <asp:Label ID="lblUpdateSuccess"
                                                    runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td>
                                            <asp:RadioButtonList runat="server"
                                                ID="rdoDStvSubList" OnSelectedIndexChanged="rdoDStvSubList_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal"
                                                CellPadding="5" CellSpacing="4"
                                                RepeatLayout="Table">
                                                <asp:ListItem Text=" I am a DStv Subscriber     "
                                                    Value="DStvSubscriber" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text=" I am not a DStv Subscriber"
                                                    Value="NonDStvSubscriber"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td>
                                            <div class="connecthr_dotline2">
                                            </div>
                                        </td>
                                    </tr>
                                    <asp:MultiView ID="multiviewCatchUp"
                                        runat="server" ActiveViewIndex="0">
                                        <asp:View runat="server" ID="view1">
                                            <tr>
                                                <td>
                                                    <div class="connectpadding5px">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trLinkAccountInfo">
                                                <td>
                                                    <div class="connectfloatLeft">
                                                        <div style="display: none;">
                                                            <DStvConnect:CountryControl runat="server"
                                                                id="countryControlDStv" /></div>
                                                        <DStvConnect:smartCard runat="server"
                                                            ID="smartCardDStv" />
                                                        <DStvConnect:iSPEmailId runat="server"
                                                            ID="ispDStv" IsActive="false" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trNotPremiumSubscriberLink">
                                                <td>
                                                <%--Addition by Puneet--%>
                                                    If you are not a DStv Premium Subscriber, click
                                                    <a href="http://care.dstv.com/main.aspx?ID=1695">
                                                        here</a>.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectpadding10px">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0"
                                                        cellspacing="0">
                                                        <tr>
                                                            <td style="width: 0px;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="display: none">
                                                                <asp:CheckBox ID="chkAcceptTC"
                                                                    Checked="true" runat="server"
                                                                    ValidationGroup="none" />
                                                            </td>
                                                            <td style="width: 0px;">
                                                            </td>
                                                            <td>
                                                                For more information on our DStv
                                                                On Demand Terms and Conditions
                                                                click <a href="" runat="server"
                                                                    id="lnkCatchupTC" target="_blank">
                                                                    here</a>.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectpadding10px">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="customAcceptConditions"
                                                        runat="server" Display="Dynamic"
                                                        CssClass="connectformError"
                                                        ValidationGroup="1" ForeColor=""
                                                        OnServerValidate="customAcceptConditions_ServerValidate">
                                <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td class="connectformError"><div>Please accept our terms & conditions</div></td>
                                        </tr>
                                </table>
                                                    </asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0"
                                                        cellspacing="0" style="display: none">
                                                        <tr>
                                                            <td style="vertical-align: top;
                                                                text-align: left;">
                                                                <table border="0" cellpadding="0"
                                                                    cellspacing="0" style="margin: 0 auto;">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="imageButtonSubmit"
                                                                                runat="server" OnClick="imageButtonSubmit_Click"
                                                                                ValidationGroup="1" OnClientClick="if(Page_ClientValidate('1')){ document.getElementById('imgProcessingSubmit').style.display = 'block'; }" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <img runat="server" enableviewstate="false"
                                                                                id="imgProcessingSubmit" clientidmode="Static"
                                                                                src="<%=this.fullImagePath%>ajax-loader.gif"
                                                                                style="display: none; margin-left: 20px"
                                                                                alt="" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectpadding20px">
                                                    </div>
                                                </td>
                                            </tr>
                                            <%--  <tr>
                                                <td>
                                                    <div class="connectformDesc">
                                                        Fields marked with<span class="connecterrorTxt">*</span>&nbsp;&nbsp;&nbsp;are required
                                                        fields</div>
                                                </td>
                                            </tr>--%>
                                        </asp:View>
                                        <asp:View ID="view2" runat="server">
                                            <tr>
                                                <td>
                                                    <div>
                                                        <DStvConnect:nonDStvCatchUp runat="server"
                                                            ID="nonDStvCatchUp" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </asp:View>
                                    </asp:MultiView>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connecthrSolid" style="display: none">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <% if (System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] == "true")
                                               {%>
                                            <div class="connectfloatLeft">
                                                <h2>
                                                    <b>Do you know about BoxOffice?</b>
                                                </h2>
                                                <br />
                                                <br />
                                                <ul class="connectbasList">
                                                    <li>South Africa’s premier movies
                                                        on demand service, featuring the
                                                        latest blockbuster releases</li>
                                                    <li>Rent movies for only R24 – download
                                                        them to your computer or PVR decoder,
                                                        or watch them online</li>
                                                </ul>
                                            </div>
                                            <div class="connectfloatLeft">
                                                <br />
                                                <a href="BoxOffice.aspx">Subscribe
                                                    to BoxOffice</a>
                                                <br />
                                                <br />
                                                <a runat="server" id="lnkAboutboxOffice"
                                                    href="">Find out more about BoxOffice</a>
                                                <br />
                                                <br />
                                            </div>
                                            <%} %>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="connectlbformRight">
                            <span class="connectlbHead">About
                                DStv Connect</span>
                            <br />
                            <br />
                            <div class="connectformRight10pxMargin">
                                DStv Connect is your portal to
                                getting so much more online.
                                <br />
                                <br />
                                Use your DStv Connect credentials
                                to login to all of the partner
                                sites listed below.
                                <br />
                                <br />
                                <img src="<%=this.fullImagePath%>logossml.gif"
                                    alt="" border="0" /></div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </asp:Panel>
</span>