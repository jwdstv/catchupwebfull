﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ISPEmailId.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.ISPEmailId" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panelISPEmail" DefaultButton="imageButtonLinkISP">
        <div class="connectfloatLeft">
            <asp:UpdatePanel runat="server" ID="updatePanelSmartCard">
                <contenttemplate>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <asp:MultiView runat="server" ID="multiViewISP">
                <asp:View ID="view1" runat="server">
                    <tr>
                        <td colspan="2">
                            <strong>ADSL Account ID</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            The account identifier supplied to you by your Internet Service Provider to set up your DSL router.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div class="connectinputBoxes">
                                            <asp:TextBox ID="txtISPEmailAddress" runat="server" ValidationGroup="108" EnableViewState="false" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="connectreqField">*</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>                                
                                    <td class="connectfieldMsgs">
                                        <asp:RequiredFieldValidator ID="rfvISPEmailId" runat="server" ValidationGroup="108"
                                            Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtISPEmailAddress">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.ISPEmailId.ISPEmailIdRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regExISPEmailId" runat="server" ValidationGroup="108"
                                            Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtISPEmailAddress"
                                            ValidationExpression="^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z\.]*))$">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="3"><div style="clear:both;padding-top:5px;"></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.ISPEmailId.ISPEmailIdValidity%></div></td>
                                                    </tr>
                                                </table>                                            
                                        </asp:RegularExpressionValidator>
                                        <asp:Label ID="EmailRegisteredLabel" runat="server" Visible="false">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td colspan="3">
                                                        <div style="clear: both; padding-top: 5px;">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" />
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td class="connectformError">
                                                        <div>
                                                            <asp:Label ID="ErrorLabel" runat="server" /></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="connectpadding5px">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Label>
                                    </td>
                                    <td align="right">
                                    <a style="border:none;vertical-align:top" href="javascript:void(0);" class="connecthelp" id="ankADSLHelp">
                                    <img style="border:none;vertical-align:top" alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                                </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding10px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <strong>Password</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div class="connectinputBoxes">
                                            <asp:TextBox ID="txtISPEmailPassword" runat="server" TextMode="Password" ValidationGroup="108" EnableViewState="false"/>
                                        </div>
                                    </td>
                                    <td>
                                        <span class="connectreqField">*</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="connectfieldMsgs">
                            <asp:RequiredFieldValidator ID="rfvISPPassword" runat="server" ValidationGroup="108"
                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="txtISPEmailPassword" >
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.ISPEmailId.ISPPasswordRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </asp:View>
                <asp:View runat="server" ID="view3">
                    <tr>
                        <td class="connectfieldMsgs">
                            <div>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <img alt="Success" src="<%=this.fullImagePath%>tick.gif" />
                                        </td>
                                        <td>
                                            ISP linked.
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </asp:View>
            </asp:MultiView>
            <tr>
                <td colspan="2">
                    <div class="connectpadding10px">
                    </div>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-align: left;">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imageButtonLinkISP" runat="server" CommandName="link" ValidationGroup="108"
                                    OnClick="imageButtonLinkISP_OnClick" OnClientClick="if(Page_ClientValidate('108')){ document.getElementById('imgProcessingISP').style.display = 'block';}"/>
                            </td>
                            <td align="right" > 
                                <img   runat="server" enableviewstate="false" id="imgProcessingISP" ClientidMode="Static" src="<%=this.fullImagePath%>ajax-loader.gif"  style="display:none;margin-left:20px" alt=""/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right">
                            <a style="border:none;vertical-align:top" href="javascript:void(0);" class="connecthelp" id="prtHelp">
                                <img style="border:none;vertical-align:top" alt="" src="<%=this.fullImagePath%>icon_help.gif" /></a>
                </td>
            </tr>
            <tr>
                <td >
                    <div class="connecthr_dotline2">
                    </div>
                </td>
            </tr>
        </table>
        </contenttemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="panelNonSA" Visible="false">
        <div class="connectfloatLeft">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td colspan="2">
                        <strong>ADSL Acount Id</strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        The email address supplied to you by your Internet Service Provider
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="connecthr_dotline2" style="clear: both">
                        </div>
                        <span class="connectformDesc">Linking your ISP is only required for South African users,
                            and is only used for Ondemand playback.</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="connecthr_dotline2">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</span>