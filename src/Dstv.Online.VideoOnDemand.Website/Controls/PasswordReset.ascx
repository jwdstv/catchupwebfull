﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PasswordReset.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.PasswordReset" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="Panel1" DefaultButton="ResetImageButton">
        <div class="connectform">
            <div class="connectformHead">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td>
                            <label title="DStv Connect">
                                <img alt="DStv Connect" src="<%=this.ConnectImagePath%>dstvconnect.png" /></label>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td>
                            <img alt="" src="<%=this.ConnectImagePath%>pr_headsep.gif" />
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectlbformHeadTxt">
                            Request a reset of your DStv Connect password
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectlbformItems">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td colspan="2" class="connectlbHeadBlue">
                            Complete the form below to reset your password.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <strong>New Password</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 251px;">
                            <div class="connectinputBoxes">
                                <asp:TextBox ID="PasswordTextBox" runat="server" TextMode="Password" /></div>
                        </td>
                        <td class="connectreqField">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ID="PasswordRequiredValidator" ValidationGroup="103"
                                runat="server" Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                <td>&nbsp;</td>
                                <td class="connectformError"><div><%=this.ValidationMessages.Reset.PasswordRequired%></div></td>
                            </tr>
                            <tr>
                                <td><div class="connectpadding5px"></div></td>
                            </tr>
                        </table>
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="PasswordLengthValidator" runat="server" ValidationGroup="103"
                                Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="PasswordTextBox"
                                ValidationExpression="^.{6,15}$">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                <td>&nbsp;</td>
                                <td class="connectformError"><div><%=this.ValidationMessages.Reset.PasswordValidity%></div></td>
                            </tr>
                            <tr>
                                <td><div class="connectpadding5px"></div></td>
                            </tr>
                        </table>                                            
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <strong>Repeat Password</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 251px;">
                            <div class="connectinputBoxes">
                                <asp:TextBox ID="RepeatPasswordTextBox" TextMode="Password" runat="server" ValidationGroup="103" /></div>
                        </td>
                        <td class="connectreqField">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding5px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                ValidationGroup="103" CssClass="connectformError" ForeColor="" ControlToValidate="RepeatPasswordTextBox">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                <td>&nbsp;</td>
                                <td class="connectformError"><div><%=this.ValidationMessages.Reset.PasswordRepeat%></div></td>
                            </tr>
                        </table>
                            </asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server"
                                ValidationGroup="103" Display="Dynamic" CssClass="connectformError" ForeColor=""
                                ControlToValidate="RepeatPasswordTextBox" ControlToCompare="PasswordTextBox">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                <td>&nbsp;</td>
                                <td class="connectformError"><div><%=this.ValidationMessages.Reset.PasswordNotMatch%></div></td>
                            </tr>
                        </table>                                            
                            </asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="">
                            <asp:ImageButton ID="ResetImageButton" runat="server" OnClick="OnReset_Click" ValidationGroup="103"
                                OnClientClick="if(Page_ClientValidate('103')){ document.getElementById('imgProcessingLogin').style.display = 'block'; }" />
                        </td>
                        <td>
                            <img runat="server" enableviewstate="false" id="imgProcessingLogin" clientidmode="Static"
                                src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none; margin-left: 20px"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectlbformRight">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="connectlbHead">
                            About DStv Connect
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight: normal">
                                DStv Connect is your portal to getting so much more online.</p>
                            <br />
                            <p style="font-weight: normal">
                                Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="RegisterHyperLink" runat="server" Text="Don't have a DStv Connect ID?"
                                NavigateUrl="~/SignUp.aspx" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label title="DStv Connect Availability">
                                <img alt="DStv Connect Availability" src="<%=this.ConnectImagePath%>logossml.gif" /></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</span>