﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Newsltrs.ascx.cs" Inherits="DStvConnect.WebUI.Controls.Newsltrs" %>
<span class="ConnectContainer">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="UpdateImageButton">
        <div class="connectform">
            <div class="connectformHead">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td>
                            <label title="DStv Connect">
                                <img alt="DStv Connect" src="<%=this.FullImagePath%>dstvconnect.png" /></label>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectprMenu">
                            <span class="connectprMenuItem"><a href="About.aspx" id="amHelp">
                                <img alt="" src="<%=this.FullImagePath%>pr_aboutme_tick.gif" /></a></span> <span
                                    class="connectprMenuItem"><a href="Account.aspx" id="maHelp">
                                        <img alt="" src="<%=this.FullImagePath%>pr_myaccount_tick.gif" /></a></span>
                            <%--<span class="connectprMenuItem"><a href="Alerts.aspx" id="alHelp"><img alt="" src="<%=this.FullImagePath%>pr_myalerts_tick.gif" /></a></span>
                        <span class="connectprMenuItem"><a id="nlHelp"><img alt="" src="<%=this.FullImagePath%>pr_mynewsletters_act.gif" /></a></span>
                        <span class="connectprMenuItem"><a href="About.aspx" id="amHelp">
                            <img alt="" src="<%=this.FullImagePath%>pr_aboutme_tick.gif" /></a></span> <span
                                class="connectprMenuItem"><a href="Account.aspx" id="maHelp">
                                    <img alt="" src="<%=this.FullImagePath%>pr_myaccount_tick.gif" /></a></span>
                        <span class="connectprMenuItem"><a href="Alerts.aspx" id="alHelp">
                            <img alt="" src="<%=this.FullImagePath%>pr_myalerts_tick.gif" /></a></span>
                        <span class="connectprMenuItem"><a id="nlHelp">
                            <img alt="" src="<%=this.FullImagePath%>pr_mynewsletters_act.gif" /></a></span>--%>
                        </td>
                        <td>
                            <img alt="" src="<%=this.FullImagePath%>pr_headsep.gif" />
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectlbformHeadTxt">
                            Complete your DStv Connect profile to access exclusive content, special offers and
                            more.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectformItems">
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ProfileImage" runat="server" ImageUrl="~/App_Themes/Connect/Images/social_temp.jpg"
                                                Width="125px" Height="125px" />
                                        </td>
                                    </tr>
                                    <asp:Panel ID="UpdateProfilePanel" runat="server" Visible="false">
                                        <tr>
                                            <td>
                                                <asp:HyperLink ID="UpdateProfileHyperLink" runat="server" ValidationGroup="1" Text="Update My Profile pic"
                                                    CssClass="connectsmlLink1" Enabled="false" />
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                </table>
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td style="vertical-align: top;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="connectuserHello">
                                            Hello <span class="connectuserTitle">
                                                <%=Page.User.Identity.Name%></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="connectpadding20px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img alt="" src="<%=FullImagePath%>pr_75.gif" />
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="connecthelp" id="pbHelp">
                                                <img alt="" src="<%=FullImagePath%>icon_help.gif" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="connectpadding5px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="connectprogTxt">
                                            Your profile is <span class="connectprogPercent">75%</span> complete
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Complete your DStv Connect profile by telling us about yourself, updting your account
                                            and choosing the alerts and newsletters that you would like to receive. Access exclusive
                                            content and special offers, update your subscriber info, and much, much more.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="connectpadding10px">
                </div>
                <div class="connecthdr_21pxGrey_line">
                    <h1>
                        MY NEWSLETTERS</h1>
                </div>
                <div class="connectfloatLeft">
                    <div class="connectfloatLeft">
                        Select the newsletters that you would like to subscribe to from the list below<br />
                        <br />
                    </div>
                    <div class="connectfloatLeft">
                        <a href="#dstv">DStv.com</a> | <a href="#supersport">Supersport</a> | <a href="#channels">
                            Channels</a></div>
                    <div class="connecthrSolid">
                    </div>
                    <div class="connectfloatLeft">
                        <table>
                            <tr>
                                <td>
                                    <strong>How would you like to subscribe newsletters</strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RadioButtonList runat="server" ID="rdoListLetterOptions" RepeatDirection="Vertical">
                                        <asp:ListItem Text="Html" Value="Html"></asp:ListItem>
                                        <asp:ListItem Text="Plain Text" Value="Plain"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="connecthrSolid">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <a name="dstv">
                                <img alt="DStv.com" src="<%=this.FullImagePath%>logo_dstv.gif"  /></a></div>
                                
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="DStv1CheckBox_30_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>DStv.com</strong><br />
                            All the latest programming and website additions, exclusive DStv competitions and
                            special offers from your favourite channels and shows.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="DStv Indian" src="<%=this.FullImagePath%>logo_dstvindian.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="DStv2CheckBox_31_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>DStv Indian</strong><br />
                            A must read for all subscribers who love their Indian film and entertainment. Weekly
                            highlights of premium programming, entertainment events and more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="DStv Sport" src="<%=this.FullImagePath%>logo_dstv.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="DStv3CheckBox_32_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>DStv Sport</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="On Demand Catch Up" src="<%=this.FullImagePath%>logo_ondemand.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="DStv4CheckBox_33_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>DStv On Demand Catch Up</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="On Demand Box Offices" src="<%=this.FullImagePath%>logo_ondemand.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="DStv5CheckBox_34_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>DStv On Demand Box Office</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connectfloatLeft">
                        &nbsp;</div>
                    <div class="connecthrSolid">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <a name="supersport">
                                <img alt="Supersport" src="<%=this.FullImagePath%>logo_supersport.gif" /></a></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Supersport1CheckBox_35_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Supersport – Preview</strong><br />
                            A must read for all subscribers who love their Indian film and entertainment. Weekly
                            highlights of premium programming, entertainment events and more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Supersport" src="<%=this.FullImagePath%>logo_supersport.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Supersport2CheckBox_36_" runat="server" /></div>
                                <input type="checkbox" />
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Supersport Review</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Supersport" src="<%=this.FullImagePath%>logo_supersport.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Supersport3CheckBox_37_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Supersport Rugby</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Supersport" src="<%=this.FullImagePath%>logo_supersport.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Supersport4CheckBox_38_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Supersport Football</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Supersport" src="<%=this.FullImagePath%>logo_supersport.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Supersport5CheckBox_39_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Supersport Cricket</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Supersport" src="<%=this.FullImagePath%>logo_supersport.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Supersport6CheckBox_40_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Supersport TV</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connectfloatLeft">
                        &nbsp;</div>
                    <div class="connecthrSolid">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <a name="channels">
                                <img alt="M-Net Magic RSA" src="<%=this.FullImagePath%>logo_mnet.gif" /></a></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels1CheckBox_41_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>M-Net Magic RSA</strong><br />
                            A must read for all subscribers who love their Indian film and entertainment. Weekly
                            highlights of premium programming, entertainment events and more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="M-Net Magic Africa" src="<%=this.FullImagePath%>logo_mnet.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels2CheckBox_42_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>M-Net Magic Africa</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="M-Net Newsflash RSA" src="<%=this.FullImagePath%>logo_mnet.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels3CheckBox_43_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>M-Net Newsflash - RSA</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="M-Net Newsflash Africa" src="<%=this.FullImagePath%>logo_mnet.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels4CheckBox_44_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>M-Net Newsflash - Africa</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Channel-O" src="<%=this.FullImagePath%>logo_channelo.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels5CheckBox_45_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Channel O</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="Vuzu" src="<%=this.FullImagePath%>logo_vuzu.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels6CheckBox_46_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>Vuzu</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="KTv" src="<%=this.FullImagePath%>logo_ktv.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels7CheckBox_47_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>KTv</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="O-Music" src="<%=this.FullImagePath%>logo_omusic.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels8CheckBox_48_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>O-Music</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                    <div class="connecthr_dotline1">
                    </div>
                    <div class="connectfloatLeft">
                        <div class="connectnewsLlogo">
                            <img alt="kykNet" src="<%=this.FullImagePath%>logo_kyknet.gif" /></div>
                        <div class="connectnewsLcheck">
                            <div class="connectcheckBoxes">
                                <asp:CheckBox ID="Channels9CheckBox_49_" runat="server" /></div>
                        </div>
                        <div class="connectnewsLdesc">
                            <strong>kykNET</strong><br />
                            Reviews of the past weekends major events, previews of upcoming fixtures, plays
                            of the week, quotes from famous players, competitions and much much more.
                        </div>
                    </div>
                </div>
                <div class="connecthr_dotline2">
                </div>
                <div class="connectpadding10px">
                </div>
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top; text-align: center;">
                                <asp:ImageButton ID="UpdateImageButton" runat="server" OnClick="OnUpdate_Click" />
                                <a href="javascript:void(0);" class="connecthelp" id="alrHelp">
                                    <img alt="" src="<%=this.FullImagePath%>icon_help.gif" /></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="connectlbformRight">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="connectlbHead">
                            About DStv Connect
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight: normal">
                                DStv Connect is your portal to getting so much more online. Register and manage
                                your personal subscriber info, access special offers and competitions and get all
                                the latest news and more!</p>
                            <p style="font-weight: normal">
                                Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label title="DStv Connect Availability">
                                <img alt="DStv Connect Availability" src="<%=this.FullImagePath%>logossml.gif" /></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</span>