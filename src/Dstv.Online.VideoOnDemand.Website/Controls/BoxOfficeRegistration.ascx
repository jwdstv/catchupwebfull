﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BoxOfficeRegistration.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.BoxOfficeRegistration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/UserDetailsControl.ascx" TagName="userDetailsControl"
    TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/CountryControl.ascx" TagName="countryControl" TagPrefix="DstvConnect" %>
<%@ Register Src="~/Controls/ISPEmailId.ascx" TagName="iSPEmailId" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/CellphoneWithPin.ascx" TagName="cellphoneWithPin" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/SmartCard.ascx" TagName="smartCard" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/NavigationMenu.ascx" TagName="navigationMenu" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/NonSADisclaimer.ascx" TagName="nonSADisclaimer" TagPrefix="DStvConnect" %>
<%@ Register Src="~/Controls/CaptchaControl.ascx" TagName="captchaControl" TagPrefix="DStvConnect" %>
<span class="ConnectContainer">
    <asp:Panel ID="panelRegistration" runat="server" DefaultButton="imageButtonSubmit">
        <div class="connectform">
            <div>
                <DstvConnect:NavigationMenu runat="server" ID="navigationMenuMyAccount" />
            </div>
            <asp:MultiView runat="server" ID="multiviewMain" ActiveViewIndex="0">
                <asp:View runat="server" ID="mainView1">
                    <div>
                        <DstvConnect:NonSADisclaimer runat="server" ID="nonSADisclaimerControl" />
                    </div>
                </asp:View>
                <asp:View runat="server" ID="mainView2">
                    <div id="contentContainer" style='width: 940px;'>
                        <div class="connectformHdr">
                            My Accounts
                        </div>
                        <div class="connectformTabs">
                            <% if (System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showBoxOfficeTab"] == "true")
                               {%>
                            <div class="tab_BO">
                                BoxOffice</div>
                            <%} %>
                            <% if (System.Configuration.ConfigurationManager.AppSettings["showCatchUpTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showCatchUpTab"] == "true")
                               {%>
                            <div class="tab_CU">
                                <a href="CatchUp.aspx">CatchUp</a></div>
                            <%} %>
                            <% if (System.Configuration.ConfigurationManager.AppSettings["showOMusicTab"] != null && System.Configuration.ConfigurationManager.AppSettings["showOMusicTab"] == "true")
                               {%>
                            <div class="tab_OM">
                                <a href="OMusic.aspx">OMusic</a></div>
                            <%} %>
                        </div>
                        <div class="connectformItems" id="connectformItems" runat="server" clientidmode="Static">
                            <div class="connectuserBox" id="divThanks" runat="server" clientidmode="Static" style="display: none">
                                <div class="connecttxt30px">
                                    Thank You</div>
                                <br />
                                Thanks for registering with DStv Connect!<br />
                                You can now sign up for BoxOffice by completing the form below.
                            </div>
                            <div class="connectfloatLeft">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <h1>
                                                BoxOffice
                                            </h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="panelSub" runat="server">
                                                <div class="connectinfoBox">
                                                    Sign up for BoxOffice and rent the latest movies online or via your PVR decoder.
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="panelNonSub" runat="server" Visible="false">
                                                <div class="connectinfoBox">
                                                    Sign up for BoxOffice and rent the latest movies online.
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img style="vertical-align: middle; float: left" id="imgError" alt="" src="../App_Themes/Connect/images/icon_excl.gif"
                                                runat="server" visible="false" />&nbsp;
                                            <asp:Label ID="lblUpdateSuccess" runat="server" ClientIDMode="Static" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList runat="server" ID="rdoDStvSubList" OnSelectedIndexChanged="rdoDStvSubList_SelectedIndexChanged"
                                                AutoPostBack="true" RepeatDirection="Horizontal" CellPadding="5" CellSpacing="4"
                                                RepeatLayout="Table">
                                                <asp:ListItem Text=" I am a DStv Subscriber     " Value="DStvSubscriber" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text=" I am not a DStv Subscriber" Value="NonDStvSubscriber"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connecthr_dotline2">
                                            </div>
                                        </td>
                                    </tr>
                                    <asp:MultiView ID="multiViewBoxOffice" runat="server" ActiveViewIndex="0">
                                        <asp:View runat="server" ID="view1">
                                            <tr>
                                                <td>
                                                    <div class="connectpadding5px">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="connectfloatLeft">
                                                        <DstvConnect:CountryControl runat="server" ID="countryControlDStv" />
                                                        <DstvConnect:SmartCard runat="server" ID="smartCardDStv" />
                                                        <DstvConnect:CellphoneWithPin runat="server" ID="cellphoneWithPinDStv" />
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                        </asp:View>
                                        <asp:View runat="server" ID="view2">
                                            <tr>
                                                <td>
                                                    <div class="connectfloatLeft">
                                                        <DstvConnect:CountryControl runat="server" ID="countryControlNonDStv" />
                                                        <DstvConnect:UserDetailsControl runat="server" ID="userDetailsControlNonDStv" />
                                                        <DstvConnect:CellphoneWithPin runat="server" ID="cellphoneWithPinNonDStv" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </asp:View>
                                    </asp:MultiView>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkAcceptTC" runat="server" ValidationGroup="1" />
                                                    </td>
                                                    <td style="width: 15px;">
                                                    </td>
                                                    <td>
                                                        I accept the On Demand and DStv Wallet <a href="" runat="server" id="lnkBoxOfficeTC"
                                                            target="_blank">Terms & Conditions</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CustomValidator ID="customAcceptConditions" runat="server" Display="Dynamic"
                                                CssClass="connectformError" ValidationGroup="1" ForeColor="" OnServerValidate="customAcceptConditions_ServerValidate">
                                <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                            <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                            <td class="connectformError"><div>Please accept our terms & conditions</div></td>
                                        </tr>
                                </table>
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="vertical-align: top; text-align: left;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="imageButtonSubmit" runat="server" OnClick="imageButtonSubmit_Click"
                                                                        ValidationGroup="1" CausesValidation="true" OnClientClick="if(Page_ClientValidate('1')){ document.getElementById('imgProcessingSubmit').style.display = 'block'; }" />
                                                                </td>
                                                                <td align="right">
                                                                    <img runat="server" enableviewstate="false" id="imgProcessingSubmit" clientidmode="Static"
                                                                        src="<%=this.fullImagePath%>ajax-loader.gif" style="display: none; margin-left: 20px"
                                                                        alt="" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding20px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectformDesc">
                                                Fields marked with<span class="connecterrorTxt">*</span>&nbsp;&nbsp;&nbsp;are required
                                                fields</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connecthrSolid">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectpadding10px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectfloatLeft">
                                                <h2>
                                                    <b>Do you know about CatchUp?</b>
                                                </h2>
                                                <br />
                                                <br />
                                                <ul class="connectbasList">
                                                    <li>Watch your favourite TV shows, sport and movies whenever you want</li>
                                                    <li>Exclusive to DStv Premium subscribers with a partner ISP</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="connectfloatLeft">
                                                <br />
                                                <a href="CatchUp.aspx">Subscribe to CatchUp</a>
                                                <br />
                                                <br />
                                                <a runat="server" id="lnkAboutCatchUp" href="">Find out more about CatchUp</a>
                                                <br />
                                                <br />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="connectformRight">
                        <div class="connectformRight10pxMargin">
                            DStv Connect is your portal to getting so much more online.
                            <br />
                            <br />
                            <img src="<%=this.fullImagePath%>logossml.gif" alt="" border="0" /></div>
                    </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </asp:Panel>
</span>