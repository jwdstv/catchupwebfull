﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountryControl.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.CountryControl" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panelCountry">
        <div class="connectfloatLeft">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <asp:MultiView runat="server" ID="multiviewCountry">
                    <asp:View ID="view1" runat="server">
                        <tr>
                            <td colspan="2">
                                <strong>Country</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <div class="connectinputBoxes">
                                                <asp:DropDownList runat="server" ID="ddlCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                                    AutoPostBack="true" ValidationGroup="1">
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="connectreqField">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="connectfieldMsgs">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvCounrty" ValidationGroup="1" Display="Dynamic"
                                                CssClass="connectformError" ForeColor="" ControlToValidate="ddlCountry">
                                    <table border="0" cellpadding="0" cellspacing="0">                                                    
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CountryValidation.CountryRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="connecthr_dotline2">
                                </div>
                            </td>
                        </tr>
                    </asp:View>
                    <asp:View ID="view2" runat="server">
                    </asp:View>
                </asp:MultiView>
            </table>
        </div>
    </asp:Panel>
</span>