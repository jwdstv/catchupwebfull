﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonSADisclaimer.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.NonSADisclaimer" %>
<span class="ConnectContainer">
    <asp:Panel runat="server" ID="panleDisclaimer">
        <div class="connectglobal_wrapper">
            <div class="connecterrBg">
                <div class="connecterrDisplay">
                    <div class="connecterrMsg">
                        We’re sorry, this service is not available in your country.
                    </div>
                    <div class="connecterrTxt">
                        <br />
                        If you believe you’ve received this message in error, please <a href="#">let us know.</a>
                    </div>
                </div>
            </div>
            <div class="connectpadding20px">
            </div>
        </div>
    </asp:Panel>
    <span>