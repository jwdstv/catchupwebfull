﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileAlerts.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.ProfileAlerts" %>
<span class="ConnectContainer">
    <asp:Panel ID="Panel1" runat="server">
        <div class="connectform">
            <div class="connectformHead">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20px;">
                        </td>
                        <td>
                            <label title="DStv Connect">
                                <img alt="DStv Connect" src="<%=this.FullImagePath%>dstvconnect.png" /></label>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectprMenu">
                            <span class="connectprMenuItem"><a href="About.aspx" id="amHelp">
                                <img alt="" src="<%=this.FullImagePath%>pr_aboutme_tick.gif" /></a></span> <span
                                    class="connectprMenuItem"><a href="Account.aspx" id="maHelp">
                                        <img alt="" src="<%=this.FullImagePath%>pr_myaccount_tick.gif" /></a></span>
                            <%--<span class="connectprMenuItem"><a id="alHelp"><img alt="" src="<%=this.FullImagePath%>pr_myalerts_act.gif" /></a></span>
                        <span class="connectprMenuItem"><a href="Newsletters.aspx" id="nlHelp"><img alt="" src="<%=this.FullImagePath%>pr_mynewsletters_q.gif" /></a></span>--%>
                        </td>
                        <td>
                            <img alt="" src="<%=this.FullImagePath%>pr_headsep.gif" />
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td class="connectlbformHeadTxt">
                            Complete your DStv Connect profile to access exclusive content, special offers and
                            more.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectformItems">
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ProfileImage" runat="server" ImageUrl="~/App_Themes/Connect/Images/social_temp.jpg"
                                                Width="125px" Height="125px" />
                                        </td>
                                    </tr>
                                    <asp:Panel ID="UpdateProfilePanel" runat="server" Visible="false">
                                        <tr>
                                            <td>
                                                <asp:HyperLink ID="UpdateProfileHyperLink" runat="server" ValidationGroup="1" Text="Update My Profile pic"
                                                    CssClass="connectsmlLink1" Enabled="false" />
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                </table>
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td style="vertical-align: top;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="connectuserHello">
                                            Hello <span class="connectuserTitle">
                                                <%=Page.User.Identity.Name%></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="connectpadding20px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img alt="" src="<%=FullImagePath%>pr_50.gif" />
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="connecthelp" id="pbHelp">
                                                <img alt="" src="<%=FullImagePath%>icon_help.gif" /></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="connectpadding5px">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="connectprogTxt">
                                            Your profile is <span class="connectprogPercent">50%</span> complete
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Complete your DStv Connect profile by telling us about yourself, updting your account
                                            and choosing the alerts and newsletters that you would like to receive. Access exclusive
                                            content and special offers, update your subscriber info, and much, much more.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="connectpadding10px">
                </div>
                <div class="connecthdr_21pxGrey_line">
                    <h1>
                        MY ALERTS</h1>
                </div>
                <div class="connectfloatLeft">
                    Tell us where you would like to receive alerts for events in the DStv guide and
                    we’ll send you reminders for your favourite programs<br />
                    <br />
                </div>
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <img src="<%=this.FullImagePath%>logo_twitter.gif" alt="Twitter" />
                            </td>
                            <td style="width: 15px">
                            </td>
                            <td>
                                <b>Twitter</b><br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertTwitterTextBox" runat="server" />
                                </div>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertTwitterPwdTextBox" TextMode="Password" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="connectpadding5px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img alt="GMail!" src="<%=this.FullImagePath%>logo_gtalk.gif" />
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td>
                                <strong>Gmail</strong>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertGmailTextBox" runat="server" />
                                </div>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertGmailPwdTextBox" TextMode="Password" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="connectpadding5px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img alt="MSN" src="<%=this.FullImagePath%>logo_msn.gif" />
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td>
                                <strong>MSN</strong>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertMSNTextBox" runat="server" />
                                </div>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertMSNPwdTextBox" TextMode="Password" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="connectpadding5px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img alt="Yahoo!" src="<%=this.FullImagePath%>logo_yahoo.gif" />
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td>
                                <strong>Yahoo</strong>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertYahooTextBox" runat="server" />
                                </div>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertYahooPwdTextBox" TextMode="Password" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="connectpadding5px">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img alt="Facebook" src="<%=this.FullImagePath%>logo_facebook.gif" />
                            </td>
                            <td style="width: 15px;">
                            </td>
                            <td>
                                <strong>Facebook</strong>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertFacebookTextBox" runat="server" />
                                </div>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td>
                                <br />
                                <div class="connectinputBoxes2">
                                    <asp:TextBox ID="AlertFacebookPwdTextBox" TextMode="Password" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td colspan="2">
                                <div class="connecthr_dotline2">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>How long before the event would you like to be notified?</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%;">
                                <div class="connectinputBoxes1">
                                    <asp:DropDownList ID="DaysDropDownList" runat="server" ValidationGroup="1" />
                                </div>
                                <div class="connectinputBoxes1">
                                    <asp:DropDownList ID="HoursDropDownList" runat="server" ValidationGroup="1" />
                                </div>
                                <div class="connectinputBoxes1">
                                    <asp:DropDownList ID="MinutesDropDownList" runat="server" ValidationGroup="1" />
                                </div>
                            </td>
                            <td style="width: 50%;" class="connectfieldMsgs">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="connecthr_dotline2">
                </div>
                <div class="connectpadding10px">
                </div>
                <div class="connectfloatLeft">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top; text-align: center;">
                                <asp:ImageButton ID="UpdateImageButton" runat="server" />
                                <a href="javascript:void(0);" class="connecthelp" id="alrHelp">
                                    <img alt="" src="<%=this.FullImagePath%>icon_help.gif" /></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="connectlbformRight">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="connectlbHead">
                            About DStv Connect
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-weight: normal">
                                DStv Connect is your portal to getting so much more online. Register and manage
                                your personal subscriber info, access special offers and competitions and get all
                                the latest news and more!</p>
                            <p style="font-weight: normal">
                                Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="connectpadding15px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label title="DStv Connect Availability">
                                <img alt="DStv Connect Availability" src="<%=this.FullImagePath%>logossml.gif" /></label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</span>