﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountryLightBoxs.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.CountryLightBoxs" ClassName="CountryLightBoxs" %>
<div class="connectCountrylightBox">
    <!-- LOGIN BEGIN -->
    <div class="connectCountryBox">
        <div class="connectCountryBg">
            <span class="connectlbHeadBlue" id="lblHeading" runat="server"></span>
            <br />
            <br />
            <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center">
                                        <div class="connectinputBoxes">
                                            <asp:DropDownList ID="ddlCountry" runat="server" DataTextField="Title" DataValueField="CountryCode">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="connectfieldMsgs">
                                        <asp:RequiredFieldValidator runat="server" ID="rfvCounrty" ValidationGroup="109"
                                            Display="Dynamic" CssClass="connectformError" ForeColor="" ControlToValidate="ddlCountry">
                                    <table border="0" cellpadding="0" cellspacing="0">                                                    
                                                    <tr>
                                                        <td><img alt="Validation Error" src="<%=this.fullImagePath%>icon_excl.gif" /></td>
                                                        <td>&nbsp;</td>
                                                        <td class="connectformError"><div><%=this.validationMessages.CountryValidation.CountryRequired%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> <div class="connectpadding5px"></div></td>
                                                    </tr>
                                                </table>
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <br />
                            <asp:ImageButton ID="btnConfirm" runat="server" Height="35" Width="108" OnClick="btnConfirm_Click"
                                ValidationGroup="109" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
