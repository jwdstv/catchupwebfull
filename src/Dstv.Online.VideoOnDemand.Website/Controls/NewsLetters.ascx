﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsLetters.ascx.cs"
    Inherits="DStvConnect.WebUI.Controls.NewsLetters" %>
<%@ Register Src="~/Controls/NavigationMenu.ascx" TagName="navigationMenuNews" TagPrefix="DStvConnect" %>
<span class="ConnectContainer">
    <script type="text/javascript">
        function function1(id, email, countryCode) {
            if (document.getElementById('NewsLetterCheckBox_' + id) != null) {
                var action = document.getElementById('NewsLetterCheckBox_' + id).checked;
                var el = $('#NewsLetterCheckBox_' + id);
                el.attr({ 'readonly': 'readonly', 'disabled': 'disabled' });
                var t = setTimeout("$('" + '#NewsLetterCheckBox_' + id + "').attr({ 'readonly': '', 'disabled': '' })", 4000); 
                
                var temp = "";
                var message = "";
                if (action == true) {
                    temp = "Subscribing...";
                    message = "Subscribed Successfully";
                }
                else {
                    temp = "Unsubscribing...";
                    message = "Unsubscribed Successfully";
                }

                var loadingDiv = $("#load_" + id);
                $.ajax({

                    beforeSend: function () {
                        loadingDiv.html("<img src='../app_themes/connect/images/ajax-loader.gif' />&nbsp;" + temp);
                        loadingDiv.slideDown("fast");
                    },
                    
                    url: "../Controls/AsyncHandler.ashx",
                    data: { method: "NewsLetterSubscription", newsLetterId: id, action: action, countryCode: countryCode },
                    timeout: 22000,
                    cache: false,
                    success: function (data) {
                        message = "<span style='color:green'>" + message + "</span>";
                        loadingDiv.slideUp("slow");
                        $("#staticBox").html(message).slideDown("slow").delay(500).slideUp("slow");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        message = "An errror occured, Please try again later.";
                        message = "<span style='color:red'>" + message + "</span>";
                        loadingDiv.slideUp("slow");
                        $("#staticBox").html(message).slideDown("slow").delay(1000).slideUp("slow");
                        
                        if (action == true) {
                            el.attr('checked', false );
                        }
                        else {
                            el.attr('checked', true);
                        }
                    }

                });
            }
        }

    </script>
    <div class="connectform">
        <div>
            <DStvConnect:navigationmenuNews runat="server" id="navigationMenuMyAccount" />
        </div>
        <div class="connectformItems">
            <div class="connectfloatLeft">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align: top;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Image ID="ProfileImage" runat="server" ImageUrl="" Width="125px" Height="125px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 15px;">
                        </td>
                        <td style="vertical-align: top;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="connectuserHello">
                                        Hello <span class="connectuserTitle">
                                            <%=Page.User.Identity.Name%></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="connectpadding20px">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Receive exclusive content and special offers by subscribing to your favourite newsletters.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="connectpadding10px">
            </div>
            <div class="connecthdr_21pxGrey_line">
                <h1>
                    MY NEWSLETTERS</h1>
            </div>
            <div class="connectfloatLeft">
                <div class="connectfloatLeft">
                    Select the newsletters that you would like to subscribe to from the list below<br />
                    <br />
                </div>
                <div class="connecthrSolid">
                </div>
                <div id="divNewsLetters" runat="server">
                </div>
            </div>
        </div>
        <div class="connectlbformRight">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="connectlbHead">
                        About DStv Connect
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-weight: normal">
                            DStv Connect is your portal to getting so much more online. Register and manage
                            your personal subscriber info, access special offers and competitions and get all
                            the latest news and more!</p>
                        <p style="font-weight: normal">
                            Use your DStv Connect credentials to login to all of the partner sites listed below.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="connectpadding15px">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label title="DStv Connect Availability">
                            <img alt="DStv Connect Availability" src="<%=this.fullImagePath%>logossml.gif" /></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</span>