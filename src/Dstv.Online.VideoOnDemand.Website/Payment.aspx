﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="Controls/PinVerification.ascx" tagname="PinVerification" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ajaxToolkit:ToolkitScriptManager ID="smAjaxScriptManager" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    
        <uc1:PinVerification ID="PinVerification1" runat="server" ReturnPath="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
    
    </div>
    </form>
</body>
</html>
