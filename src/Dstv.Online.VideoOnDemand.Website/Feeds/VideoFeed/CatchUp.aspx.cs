﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using System.Configuration;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo;

namespace Dstv.Online.VideoOnDemand.Website.Feeds.VideoFeed
{
    public partial class CatchUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string vid = Request.QueryString["vid"];
            string msg;
            PlayCheck playCheck = new PlayCheck();

            if (playCheck.CanBuildFeedCatchUp(vid, out msg))
            {
                BuildFeed(vid);
            }
            else
            {
                Response.Write(string.Format("Error :: {0}", msg));
            }
        }

        

        private void BuildFeed(string vid)
        {            

            Transactions.ProductTypeIdentifier product = new Transactions.ProductTypeIdentifier(ProductTypes.CatchUp);

            ClassLibrary.FlashVideo.VideoFeed vf = new ClassLibrary.FlashVideo.VideoFeed();
            ClassLibrary.FlashVideo.Types.FeedCriteria feedCriteria = new ClassLibrary.FlashVideo.Types.FeedCriteria(true, product);
            feedCriteria.TrackingIdentifier = product.Value.ToString();
            feedCriteria.ShowEmbedCode = false;
            feedCriteria.ShowAds = false;
            feedCriteria.PlayeUrl = (ConfigurationManager.AppSettings["PlayURL" + product.ToString()]);
            feedCriteria.VideoProdtype = ProductTypes.CatchUp;

            var feed = vf.GetJsonFeed(vid, feedCriteria);
            if (feed.IsSuccessful)
            {
                Response.Write(feed.JasonFeedString);
            }
            else
            {
                //choose what to tell the user
                Response.Write(feed.ErrorMessage);
            }
        }
    }
}