﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using Dstv.Online.VideoOnDemand.Integration;
using Dstv.Online.VideoOnDemand.MemCached;


namespace Dstv.Online.VideoOnDemand.Website.Feeds
{
    public partial class Billboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildVideoFeed();

        }

        private void BuildVideoFeed()
        {
            //Uses caching to get results
            //string result = string.Empty;

            //Get ProductID from Query String [productID]

            /* Read the initial time. */
            DateTime startTime = DateTime.Now;


            string product = Request.QueryString["productid"];
            if (product != null)
            {
                if (product.Contains("?"))
                {
                    product = product.Substring(0, product.IndexOf("?"));
                }
            }

            if (!string.IsNullOrEmpty(product))
            {

                string[] vals = product.Split(',');


                int productID = int.TryParse(vals[0], out productID) ? productID : 0;

                //IsBoxOffcie
                bool isBoxOffice = false;
                if (vals.Length > 1)
                {
                    isBoxOffice = bool.TryParse(vals[1], out isBoxOffice) ? isBoxOffice : false;
                }

                string result = GetXMLData(productID, isBoxOffice);

                /* Read the end time. */
                DateTime stopTime = DateTime.Now;

                /* Compute the duration between the initial and the end time. 
                 * Print out the number of elapsed hours, minutes, seconds and milliseconds. */
                TimeSpan duration = stopTime - startTime;

                result = result.Replace("[duration]", duration.ToString());

                Response.Write(result);
            }
        }

        /// <summary>
        /// Gets the data from sql and returns the data in an xml format
        /// </summary>
        /// <returns></returns>
        private string GetXMLData(int productID, bool isBoxOffice)
        {
            string xmlResult = string.Empty;

            //Sample XML
            //Results must be orderd by "staus", "Rank" then "title" for the feed to work properly
            /*
            <dataset>
                <item status="last night" time="20:00 - Mon, 12 May" showBillboard="images/CarteBlanche_HomepageBB.jpg" showName="Carte Blanche" sponsorLogo="images/logos/outsurance.jpg" synopsis="Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue." showLink="shows/?showid=9" />
                <item status="last night" time="20:00 - Tue, 13 May" showBillboard="images/DexterIV_HomepageBB.jpg" showName="Dexter IV" chnlNo="" chnlLogo="" synopsis="Dexter goes on a house-building excursion with Trinity, but still doesn't kill him. The team investigates more into..." showLink="shows/?showid=4" />
                <item status="tonight" time="20:00 - Wed, 14 May" showBillboard="images/FlashForward_HomepageBB.jpg" showName="FlashForward" chnlNo="101" chnlLogo="images/logos/mnet_display.jpg" synopsis="Mark must see a therapist after he is suspended from duty. Demetri takes on a partner in his search for Lloyd. " showLink="test" />
                <item status="tonight" time="20:00 - Thu, 15 May" showBillboard="images/BrideWars_HomepageBB.jpg" showName="Bride Wars" chnlNo="103" chnlLogo="images/logos/mm1_large.jpg" synopsis="Two close friends become bitter enemies when their wedding planner books the same venue on the same day for their posh..." showLink="Movies/Video/?vid=13" />
                <item status="tonight" time="20:00 - Fri, 16 May" showBillboard="images/CarteBlanche_HomepageBB.jpg" showName="Carte Blanche" chnlNo="101" chnlLogo="images/logos/mnet_display.jpg" synopsis="Carte Blanche covers a variety of subjects and prides itself on the diversity and depth of its stories. " showLink="shows/?showid=9" />
                <item status="tonight" time="20:00 - Sat, 17 May" showBillboard="images/HouseVI_HomepageBB.jpg" showName="House VI" chnlNo="110" chnlLogo="images/logos/mnet_series_large.jpg" synopsis="After House's medical license is reinstated, he reclaims his role as Head of Diagnostics in time to treat Hank Hardwick." showLink="shows/?showid=14" />
                <item status="coming soon" time="20:00 - Sun, 18 May" showBillboard="images/YesMan_HomepageBB.jpg" showName="Yes Man" chnlNo="105" chnlLogo="images/logos/mnetstars_large.jpg" synopsis="Mayhem ensues when a banker with a negative mindset agrees to say yes to everything for a whole year. " showLink="shows/?showid=11" />
                <item status="last chance" time="20:00 - Mon, 19 May" showBillboard="images/Survivor_HomepageBB.jpg" showName="Survivor SA" chnlNo="101" chnlLogo="images/logos/mnet_display.jpg" synopsis="Eighteen SA celebrities are marooned on a deserted island on the Mozambique coast. Here they scheme, plot and play to..." showLink="shows/?showid=3" />
                <item status="last chance" time="20:00 - Tue, 20 May" showBillboard="images/GreysAnatomy6_HomepageBB.jpg" showName="Aenean porta nibh ac elit fermentum consequat Proin" chnlNo="607" chnlLogo="images/logos/mnet_display.jpg" synopsis="Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat..." showLink="shows/?showid=2" />
            </dataset>             
             */

            string cacheKey = string.Format("{0}_BillboardFeed_GetXMLData_{1}_{2}", CacheConstants.CachePrefix, productID, isBoxOffice.ToString()).CacheRemoveInvalidChars();
            string source;

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                xmlResult = MemCacheManager.Get<string>(cacheKey);
                source = "cache";
            }
            else
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.Encoding = System.Text.Encoding.UTF8;

                StringBuilder sb = new StringBuilder();
                using (XmlWriter wr = XmlTextWriter.Create(sb, settings))
                {
                    wr.WriteRaw("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    wr.WriteStartElement("dataset");
                    //testing purposes. need to show where the feed is coming from. i.e database or cache
                    wr.WriteAttributeString("source", "[source]");
                    wr.WriteAttributeString("product", productID.ToString());
                    wr.WriteAttributeString("duration", "[duration]");

                    string imageServerPath = Dstv.Online.VideoOnDemand.Integration.Extensions.GetImageServerPathSetting();
                    string chnlLogoPath = ConfigurationManager.AppSettings["BillBoardChanlLogoPath"];
                    if (string.IsNullOrEmpty(imageServerPath))
                        imageServerPath = string.Empty;
                    if (string.IsNullOrEmpty(chnlLogoPath))
                        chnlLogoPath = string.Empty;


                    //Get billboard items by productID                
                    List<Dstv.Online.VideoOnDemand.Data.BillboardDTO> billboardItems = Dstv.Online.VideoOnDemand.Data.BillboardDb.GetBillBoardByProductId(productID);



                    for (int i = 0; i < billboardItems.Count; i++)
                    {
                        //Sample result data
                        //<item status="last night" time="20:00 - Mon, 12 May" showBillboard="images/CarteBlanche_HomepageBB.jpg" showName="Carte Blanche" sponsorLogo="images/logos/outsurance.jpg" synopsis="Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue. Aenean porta nibh ac elit fermentum consequat. Proin eu vehicula augue." showLink="shows/?showid=9" />

                        string status = billboardItems[i].CategoryTitle;
                        string time = billboardItems[i].DisplayDate.ToString("HH:mm - ddd, dd MMM").Replace("00:00 - ", string.Empty).Replace("12:00 - ", string.Empty);
                        string showBillboard = string.Format("{0}{1}", imageServerPath, billboardItems[i].ImageUrl);
                        string showName = billboardItems[i].BillBoarditemTitle;
                        string sponsorLogo = billboardItems[i].SponsorImageUrl;
                        string synopsis = billboardItems[i].Blurb;
                        string channelnumber = billboardItems[i].ChannelNumber;
                        string channelLogo = billboardItems[i].ChannelImageUrl;
                        string showLink = billboardItems[i].LinkUrl;

                        if (!String.IsNullOrEmpty(channelLogo) && !channelLogo.ToUpper().StartsWith(chnlLogoPath.ToUpper()))
                            channelLogo = chnlLogoPath + channelLogo;


                        bool isDisplaydateValid = CheckDisplayDate(billboardItems[i].DisplayDate);


                        wr.WriteStartElement("item");
                        wr.WriteAttributeString("status", status);
                        if (!isBoxOffice && isDisplaydateValid)
                            wr.WriteAttributeString("time", time);//Only show dateTime if this is not boxOffice and the date is a valid date. i.e the date is greater than the 2000's
                        wr.WriteAttributeString("showBillboard", showBillboard);
                        wr.WriteAttributeString("showName", showName);

                        wr.WriteAttributeString("synopsis", synopsis);
                        if (!string.IsNullOrEmpty(showLink))
                        {
                            wr.WriteAttributeString("showLink", showLink);
                        }

                        //If channel Number is defined , dont show the sponsor logo and Vise Versa
                        if (!string.IsNullOrEmpty(channelnumber) && !string.IsNullOrEmpty(channelLogo) && !isBoxOffice)//add functionality to say and !boxOffice
                        {
                            //Channel logo exist
                            wr.WriteAttributeString("chnlNo", channelnumber);
                            wr.WriteAttributeString("chnlLogo", channelLogo);
                        }
                        else if (!string.IsNullOrEmpty(sponsorLogo))
                        {
                            //show the sponsor Logo
                            wr.WriteAttributeString("sponsorLogo", sponsorLogo);
                        }

                        wr.WriteEndElement();
                    }

                }

                xmlResult = sb.ToString();
                source = "db";

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, xmlResult);
                }
            }

            xmlResult = xmlResult.Replace("[source]", source);

            return xmlResult;
        }

        /// <summary>
        /// Checks the display date. if the date is 
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        private bool CheckDisplayDate(DateTime thedate)
        {
            bool isDateValid = false;
            DateTime theComparedate = new DateTime(2000, 1, 1);

            if (thedate > theComparedate)
                isDateValid = true;



            return isDateValid;
        }


    }


}
