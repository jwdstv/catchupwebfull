﻿<%@ Page Title="Account" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Account.Account" %>
<%@ Register Src="~/Controls/AccountPane.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
Account
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
   <asp:PlaceHolder ID="plcAccount" runat="server"></asp:PlaceHolder>
</asp:Content>
