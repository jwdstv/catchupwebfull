﻿<%@ Page Title="My Accounts \ Profile" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master"
    AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Account.About" %>
<%@ Register Src="~/Controls/UserProfile.ascx" TagPrefix="uc1" TagName="MyProfile" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="server">
    <div class="block980_bg">
        <div class="hdrWht24">
            PROFILE
        </div>
    </div>
    <div class="block960">
        <asp:PlaceHolder ID="plcAbout" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>
