﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

namespace Dstv.Online.VideoOnDemand.Website.Account
{
    public partial class Account : System.Web.UI.Page
    {
        /// <summary>
        /// Defines what message the user will see on the popUp
        /// </summary>
        public string VideoDisplayMessage;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DstvUser.IsAuthenticated)
            {
                // Add control to place holder
                UserControl uc2 = (UserControl)Page.LoadControl("~/Controls/AccountPane.ascx");
                plcAccount.Controls.Add(uc2);
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), Guid.NewGuid().ToString(), "$(function() {triggerLoginClick();});", true);
            }

        }

    }
}