﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.Account
{
    public partial class BoxOffice : System.Web.UI.Page
    {
        /// <summary>
        /// Defines what message the user will see on the popUp
        /// </summary>
        public string VideoDisplayMessage;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DstvUser.IsAuthenticated)
            {
                // Add control to place holder
                UserControl uc = (UserControl)Page.LoadControl("~/Controls/BoxOfficeProfile.ascx");
                plcAccount.Controls.Add(uc);
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), Guid.NewGuid().ToString(), "triggerLoginClick()", true);
            }

        }

    }
}