﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using System.Configuration;

namespace Dstv.Online.VideoOnDemand.Website.Help
{
	public partial class FAQ : BasePage
	{
        protected string ToggleCode = "";

        protected bool HideBoxOffice
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["HideBoxOffice"]);
            }
        }

        protected void Page_Load ( object sender, EventArgs e )
		{
            if (!string.IsNullOrEmpty(Request.QueryString["Section"])) {
                ToggleCode = "ToggleFilterMenuStyle('li" + Request.QueryString["Section"] + "', 'divCatchUp'); ToggleFAQ('div" + Request.QueryString["Section"] + "_FAQ');";

                if (Request.QueryString["Section"] == "CatchUp_About") // RSS: assumes BoxOffice hidden
                {
                    ToggleCode += "Accordion1.openNextPanel();";
                }
            }
            else
            {
                ToggleCode = "$('.AccordionPanelContent ul li a').first().trigger(\"click\");";
            }
		}
	}
}