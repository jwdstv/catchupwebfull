﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.Help
{
	public partial class ContactUs : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{

		}

        protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
        {
            if (this.Page.IsValid)
            {
                string strPlatform = "";
                string strBrowser = "";

                strPlatform = Request["optionaldata1"].ToString();
                strBrowser = Request["optionaldata2"].ToString();

                string to = "IS6068_18116@is.instantservice.com";
                string body = "******************************" + Environment.NewLine + "USER PROVIDED DATA" + Environment.NewLine + "******************************" + Environment.NewLine +
                            "NAME: " + txtFirstname.Text + " " + txtLastname.Text + Environment.NewLine + Environment.NewLine +
                            "EMAIL: " + txtEmail.Text + Environment.NewLine + Environment.NewLine +
                            "SITE: " + selSite.Value + Environment.NewLine + Environment.NewLine +
                            "SUPPORT CATEGORY: " + selSupport.Value + Environment.NewLine + Environment.NewLine +
                            "WINDOWS MEDIA PLAYER VERSION: " + selWinMedia.Value + Environment.NewLine + Environment.NewLine +
                            "CONNECTION TYPE: " + selConnType.Value + Environment.NewLine + Environment.NewLine +
                            "ADDITIONAL COMMENTS: " + AddComments.Value + Environment.NewLine + Environment.NewLine +
                            "******************************" + Environment.NewLine + "AUTOMATICALLY COLLECTED DATA" + Environment.NewLine + "******************************" + Environment.NewLine +
                            "BROWSER: " + strBrowser + Environment.NewLine + Environment.NewLine +
                            "PLATFORM: " + strPlatform + Environment.NewLine + Environment.NewLine +
                            "IP ADDRESS: " + Request.ServerVariables["REMOTE_ADDR"].ToString();

                Common.Smtp.Emailer.SendHtmlEmail(to, new System.Net.Mail.MailAddress(txtEmail.Text), "DStv On Demand" + " - " + selSupport.Value, body.Replace(Environment.NewLine, "<br />"));

                reset();

                divForm.Visible = false;
                divThankYou.Visible = true;
            }
        }

        public void reset()
        {
            txtFirstname.Text = "";
            txtLastname.Text = "";
            txtEmail.Text = "";
            selSite.SelectedIndex = 0;
            selSupport.SelectedIndex = 0;
            selWinMedia.SelectedIndex = 0;
            selConnType.SelectedIndex = 0;
            AddComments.Value = "";

        }
	}
}