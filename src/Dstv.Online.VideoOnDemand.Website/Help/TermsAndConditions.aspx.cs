﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Dstv.Online.VideoOnDemand.Website.Help
{
	public partial class TermsAndConditions : System.Web.UI.Page
	{
        protected bool HideBoxOffice
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["HideBoxOffice"]);
            }
        }

		protected void Page_Load ( object sender, EventArgs e )
		{
		}
	}
}