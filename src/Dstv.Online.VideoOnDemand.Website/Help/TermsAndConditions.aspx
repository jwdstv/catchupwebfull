﻿<%@ Page Title="Terms and Conditions" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master"
    AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Help.TermsAndConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
<div class="block980_bg">
    <div class="hdrWht24">TERMS AND CONDITIONS</div>
</div>
    <div class="block940_20px">
        <div class="padding20px">
        </div>
        <div class="floatLeft">
            <a href="#DStvONDEMAND" class="floatLeft">DStv On Demand</a><br />
            <br />
            <div style='display: <%= HideBoxOffice ? "none" : "" %>'>
                <a href="#DStvWallet" class="floatLeft">DStv Wallet</a><br />
                <br />
            </div>
            <a href="#TCMultiChoiceSub" class="floatLeft">Terms and conditions of subscription to
                MultiChoice Service by individuals for their private use (South Africa and Lesotho)</a>
        </div>
        <div class="padding20px">
        </div>
        <h3>
            <strong><a id="DStvONDEMAND"></a>DStv On Demand</strong></h3>
        <div class="greyHdrLine">
        </div>
        <div class="floatLeft">
            <a href="#OnDemandAcceptance">Acceptance</a> | <a href="#OnDemandInterpretation">Interpretation</a></div>
        <div class="padding20px">
        </div>
        <div class="floatLeft">
            These terms and conditions (“the terms”) - set out the contractual basis upon which
            you as a subscriber to MultiChoice’s DStv Premium Bouquet will be entitled to access
            and use the DStv On Demand Service made available on the DStv.com website (this
            “website”); and form a part of and must be read in conjunction the MultiChoice Digital
            Satellite Terms and Conditions in respect of MultiChoice’s digital services (“DStv
            Terms and Conditions”) which you have already agreed to in respect of your access
            to and use of the DStv Premium Bouquet and associated services as a subscriber to
            the DStv Premium Bouquet. In addition to the DStv Terms and Conditions, the MultiChoice
            Privacy Policy is also of application to your access and use of the DStv On Demand
            Service. The DStv Terms and Conditions and the MultiChoice Privacy Policy can be
            accessed and read by you at www.dstv.com.<br />
            <br />
            <b><a id="OnDemandAcceptance"></a>Your acceptance of these Terms and Conditions</b><br />
            <br />
            Your access and use of the DStv On Demand Service constitutes your acceptance of
            the terms of use set out below. If you do not wish to be bound by these terms, you
            may not access and use the DStv On Demand Service.<br />
            By accessing and using the DStv On Demand Service, you agree to be bound by the
            DStv Terms and Conditions and the MultiChoice Privacy Policy applicable at the time
            of such use.<br />
            <br />
            <b><a id="OnDemandInterpretation"></a>Interpretation</b><br />
            <br />
            In these terms:<br />
            <br />
            “DStv On Demand Service” means a service which allows you to view certain Programmes
            which are broadcast at a specific time on the DStv Premium Bouquet, at a time other
            than the scheduled broadcast time, by means of the streaming or temporary download
            of such Programmes to your personal computer (“PC”); “DStv Premium Bouquet” means
            the programming bouquet of MultiChoice’s pay television services that contains the
            most channels of any programming bouquet offered by MCA and includes any similar
            successor bouquet;<br />
            <br />
            “DStv On Demand Desktop” means the software that you are required to install on
            your PC in order to download and view the Programmes on the DStv On Demand Service;
            “DStv Premium Subscriber(s)” means a person who subscribes to the DStv Premium Bouquet
            in terms of a subscription agreement with MultiChoice but specifi cally excludes
            persons who are commercial subscribers to the DStv Premium Bouquet, including but
            not limited to hotels, motels, hospitals, places of multiple occupancy, multiple
            dwelling units, bulk billed establishments and communal areas;<br />
            <br />
            “Programme(s)” means all video, audio and other content owned or licensed, packaged
            and broadcast by MultiChoice on the DStv Premium Bouquet; and “we”, “our” and “us”
            means MultiChoice Africa (Proprietary) Limited. We reserve the right to vary or
            amend any of these terms from time to time. If we elect to amend these terms, we
            will post the updated terms on this website. All amended terms shall become effective
            immediately after they are posted on this website, and any subsequent use of this
            website or of the DStv On Demand Service shall be governed by such amended terms.
            By accessing and using the DStv On Demand Service, you agree to be bound by the
            terms effective at the time of such access and use. You are advised to regularly
            check these terms for any amendments or updates.<br />
            <br />
            All words and phrases defined in the DStv Terms and Conditions shall bear corresponding
            meanings in these terms and all rules of interpretation set out in the DStv Terms
            and Conditions shall apply equally to these terms.<br />
            <br />
            Unless otherwise specifically stated to the contrary, no term or provision contained
            in these terms is to be interpreted or construed so as to exclude any rights granted
            by Chapter VII of the Electronic Communications and Transactions Act of 2002 (“ECT
            Act”) to any natural person who enters into or who intends entering into an electronic
            transaction with MultiChoice for the provision of the DStv On Demand Service.<br />
            <br />
            <div style='display: <%= HideBoxOffice ? "none" : "" %>'>
                <h3>
                    <strong><a id="DStvWallet"></a>DStv Wallet<a id="DStvWallet"></a></strong></h3>
                <div class="greyHdrLine">
                </div>
            </div>
            <div class="floatLeft">
                <a href="#DStvWalletAcceptance">Acceptance</a> | <a href="#DStvWalletInterpretation">
                    Interpretation</a></div>
            <div class="padding20px">
            </div>
            <div class="floatLeft">
                These terms and conditions (“the terms”) - set out the contractual basis upon which
                you as a subscriber to MultiChoice’s DStv Premium Bouquet will be entitled to access
                and use the DStv On Demand Service made available on the DStv.com website (this
                “website”); and form a part of and must be read in conjunction the MultiChoice Digital
                Satellite Terms and Conditions in respect of MultiChoice’s digital services (“DStv
                Terms and Conditions”) which you have already agreed to in respect of your access
                to and use of the DStv Premium Bouquet and associated services as a subscriber to
                the DStv Premium Bouquet. In addition to the DStv Terms and Conditions, the MultiChoice
                Privacy Policy is also of application to your access and use of the DStv On Demand
                Service. The DStv Terms and Conditions and the MultiChoice Privacy Policy can be
                accessed and read by you at www.dstv.com.<br />
                <br />
                <b><a id="DStvWalletAcceptance"></a>Your acceptance of these Terms and Conditions</b><br />
                <br />
                Your access and use of the DStv On Demand Service constitutes your acceptance of
                the terms of use set out below. If you do not wish to be bound by these terms, you
                may not access and use the DStv On Demand Service.<br />
                By accessing and using the DStv On Demand Service, you agree to be bound by the
                DStv Terms and Conditions and the MultiChoice Privacy Policy applicable at the time
                of such use.<br />
                <br />
                <b><a id="DStvWalletInterpretation"></a>Interpretation</b><br />
                <br />
                In these terms:<br />
                <br />
                “DStv On Demand Service” means a service which allows you to view certain Programmes
                which are broadcast at a specific time on the DStv Premium Bouquet, at a time other
                than the scheduled broadcast time, by means of the streaming or temporary download
                of such Programmes to your personal computer (“PC”); “DStv Premium Bouquet” means
                the programming bouquet of MultiChoice’s pay television services that contains the
                most channels of any programming bouquet offered by MCA and includes any similar
                successor bouquet;<br />
                <br />
                “DStv On Demand Desktop” means the software that you are required to install on
                your PC in order to download and view the Programmes on the DStv On Demand Service;
                “DStv Premium Subscriber(s)” means a person who subscribes to the DStv Premium Bouquet
                in terms of a subscription agreement with MultiChoice but specifi cally excludes
                persons who are commercial subscribers to the DStv Premium Bouquet, including but
                not limited to hotels, motels, hospitals, places of multiple occupancy, multiple
                dwelling units, bulk billed establishments and communal areas;<br />
                <br />
                “Programme(s)” means all video, audio and other content owned or licensed, packaged
                and broadcast by MultiChoice on the DStv Premium Bouquet; and “we”, “our” and “us”
                means MultiChoice Africa (Proprietary) Limited. We reserve the right to vary or
                amend any of these terms from time to time. If we elect to amend these terms, we
                will post the updated terms on this website. All amended terms shall become effective
                immediately after they are posted on this website, and any subsequent use of this
                website or of the DStv On Demand Service shall be governed by such amended terms.
                By accessing and using the DStv On Demand Service, you agree to be bound by the
                terms effective at the time of such access and use. You are advised to regularly
                check these terms for any amendments or updates.<br />
                <br />
                All words and phrases defined in the DStv Terms and Conditions shall bear corresponding
                meanings in these terms and all rules of interpretation set out in the DStv Terms
                and Conditions shall apply equally to these terms.<br />
                <br />
                Unless otherwise specifically stated to the contrary, no term or provision containedk
                in these terms is to be interpreted or construed so as to exclude any rights granted
                by Chapter VII of the Electronic Communications and Transactions Act of 2002 (“ECT
                Act”) to any natural person who enters into or who intends entering into an electronic
                transaction with MultiChoice for the provision of the DStv On Demand Service.<br />
                <br />
            </div>
            <h3>
                <strong><a id="TCMultiChoiceSub"></a>Terms and conditions of subscription to MultiChoice
                    Service by individuals for their private use (South Africa and Lesotho)</strong></h3>
            <div class="greyHdrLine">
            </div>
            <div class="floatLeft">
                <a href="#TCMInterpretation">Interpretation</a><br />
                <br />
                <a href="#TCMTandC">Terms and conditions of Agreement</a><br />
                <br />
                <a href="#TCMCommencementAndDuration">Commencement, duration and place of Agreement</a><br />
                <br />
                <a href="#TCMHardware">Hardware</a><br />
                <br />
                <a href="#TCMUserManual">User Manual</a><br />
                <br />
                <a href="#TCMAdvancedDecoderFunctionality">Advanced Decoder Functionality</a><br />
                <br />
                <div style='display: <%= HideBoxOffice ? "none" : "" %>'>
                    <a href="#TCMBoxOfficeService">BoxOffice Service</a><br />
                    <br />
                    <a href="#TCMBoxOfficeRegistration">BoxOffice Registration</a><br />
                    <br />
                </div>
                <a href="#TCMFees">Fees</a><br />
                <br />
                <a href="#TCMCommunicationsWithUs">Communications with us</a><br />
                <br />
                <a href="#TCMIntellectualProperty">Intellectual property</a><br />
                <br />
                <a href="#TCMMultiChoiceObligations">MultiChoice's obligations</a><br />
                <br />
                <a href="#TCMNoWarranties">No warranties, limitation of liability, and indemnities</a><br />
                <br />
                <a href="#TCMGeneral">General</a><br />
                <br />
            </div>
            <div class="padding20px">
            </div>
            <div class="floatLeft">
                <b><a id="TCMInterpretation"></a>Interpretation</b>
                <br />
                <br />
                1. In these terms and conditions the following terms will have the following meanings:<br />
                <br />
                1.1 "Advanced Decoder Functionality" means new and developing technological functions
                supported by your Approved Decoder such as PVR Functionality, the ability to view
                different channels simultaneously on two different viewing environments, to access
                the BoxOffice Service and other existing or future functions; 1.2 "Agreement" means
                this subscription agreement;<br />
                <br />
                1.3 “Approved Decoder” means a decoder which complies with our specifications;<br />
                <br />
                1.4 "Bouquet" means a package of audiovisual, audio and/or data channels;
                <br />
                <br />
                1.5 “BoxOffice Service” means the service provided by MultiChoice, subject to you
                completing the required registration process, whereby a selection of Movie Titles
                are made available to you on your PVR Decoder and which Movie Titles may be accessed
                at a time selected by you for viewing within a limited time period;<br />
                <br />
                1.6 "Consumer Protection Act" means the Consumer Protection Act, 68 of 2008, as
                amended from time to time;
                <br />
                <br />
                1.7 "Content Rights" means the copyright and other intellectual property rights
                to all materials, including the content, constituting the MultiChoice Service;
                <br />
                <br />
                1.8 "Coverage Area" means the geographic area in South Africa and Lesotho in which
                the MultiChoice Service is provided;
                <br />
                <br />
                1.9 “Drifta” means a mobile Approved Decoder that receives the DVB-H signal, converts
                it into Wi-Fi, then relays it to Wi-Fi enabled viewing devices (including selected
                mobile phones, laptops, PCs and tablets);<br />
                <br />
                1.10 “DVB-H” means digital video broadcast via handheld;<br />
                <br />
                1.11 "Due Date" means the date by when you must pay, and we must receive payment
                of, your fees in terms of this Agreement for each subscription period, being -
                <br />
                <br />
                1.11.1 in the case of a New Subscription, the day on which you wish us to activate
                your subscription to the MultiChoice Service; and<br />
                <br />
                1.11.2 in the case of the renewal of an existing subscription, the last day of the
                most recent period for which you have subscribed to, and paid for, the MultiChoice
                Service;
                <br />
                <br />
                1.12 “Equipment” means the subscriber equipment required for the receipt of the
                MultiChoice Service, including, the Drifta, Approved Decoder, Smartcard and the
                related satellite reception equipment and cabling;
                <br />
                <br />
                1.13 “General Amendment” means an amendment of this Agreement by us from time to
                time on notice to you and other subscribers;
                <br />
                <br />
                1.14 "HDD" means the hard drive memory of a PVR Decoder;
                <br />
                <br />
                1.15 "Manufacturers" means the manufacturers of the Equipment;
                <br />
                <br />
                1.16 “Movie Title(s)” means audiovisual and other content owned or licensed by us
                and made available on the BoxOffice Service. Movie Titles shall be selected and
                determined by us in our sole discretion;<br />
                <br />
                1.17 "MultiChoice", “we” or “us” means MultiChoice Africa (Pty) Limited, a company
                registered in South Africa with registration number 1994/009083/07, and any other
                person (including any legal person) to which that company may transfer its rights,
                obligations and interest in terms of this Agreement;
                <br />
                <br />
                1.18 "MultiChoice Service" means one or more of the Bouquets and the associated
                services, features, facilities and applications provided by us;<br />
                <br />
                1.19 "New Subscription" means a request for access to the MultiChoice Service by
                a person who does not have an active subscription to the requested service at the
                time of requesting access to the MultiChoice Service, regardless of whether or not
                that person has previously subscribed to the MultiChoice Service in respect of which
                access is requested;
                <br />
                <br />
                1.20 "Payment Instruction" means the instruction by you to your bank to pay your
                fees due in terms of this Agreement by way of a debit order or similar payment method;
                <br />
                <br />
                1.21 "PVR Decoder" means an Approved Decoder with PVR Functionality;
                <br />
                <br />
                1.22 “PVR Functionality” means the function of time-shifting, buffer, copying, memory,
                replay and any other similar functionality of the PVR Decoder introduced from time
                to time;
                <br />
                <br />
                1.23 "Smartcard" means a smartcard supplied by us for use in an Approved Decoder
                in order for you to access and use the MultiChoice Service;
                <br />
                <br />
                1.24 "Subscriber" or “you” means a person who requests, or who has authorised another
                person to request on his behalf, the right to have access to the MultiChoice Service
                on the terms and conditions of this Agreement, whose request is accepted by us,
                and whose subscription to the MultiChoice Service is active;<br />
                <br />
                1.25 “System Rights” means the copyright and other intellectual property rights
                in the Equipment and in the MultiChoice Service, including the software incorporated
                therein;<br />
                <br />
                1.26 “User Manual” is the standard operating instructions for your Approved Decoder,
                a copy of which is also available on the Website;
                <br />
                <br />
                1.27 "VAT" means Value Added Tax or any similar consumption based tax which we or
                our agents may be obliged to levy and/or collect;
                <br />
                <br />
                1.28 “Website” means www.dstv.com;<br />
                <br />
                1.29 “your Request” means your request for access to the MultiChoice Service.
                <br />
                <br />
                2. Any reference in this Agreement to:
                <br />
                <br />
                2.1 the singular includes the plural, and vice versa; and<br />
                2.2 one gender includes the other gender.
                <br />
                <br />
                <b><a id="TCMTandC"></a>Terms and conditions of Agreement</b>
                <br />
                <br />
                3. The terms and conditions on which we authorise persons to have access to the
                MultiChoice Service are set out below.<br />
                <br />
                4. By subscribing to the MultiChoice Service you agree to be bound by this Agreement
                as amended from time to time.<br />
                <br />
                Request for MultiChoice Service<br />
                <br />
                5. Your Request may be made:<br />
                <br />
                5.1 telephonically; or
                <br />
                <br />
                5.2 if applicable, via your Approved Decoder; or<br />
                <br />
                5.3 in writing to us by completing and submitting a Subscriber Agreement Schedule
                in the form used by us from time to time (“your Schedule”); or<br />
                <br />
                5.4 for the BoxOffice Service, by registering on the Website or any other website
                we may use from time to time.
                <br />
                <br />
                6. If we accept your Request, an agreement will come into effect between you and
                us on the terms and conditions set out in this Agreement read together with your
                Schedule (if applicable). If there is a conflict between these terms and conditions
                and your Schedule, the provisions of your Schedule will prevail.<br />
                <br />
                7. If we refuse your Request (which we reserve the right to do), we will have no
                obligation to you arising from your Request.<br />
                <br />
                <b><a id="TCMCommencementAndDuration"></a>Commencement, duration and place of Agreement</b><br />
                <br />
                8. Regardless of the place or date of your Request or of the acceptance of your
                Request by us, this Agreement is deemed to be concluded at our principal place of
                business in Randburg, South Africa and commences on the date on which we accept
                your Request by activating your subscription to the MultiChoice Service.
                <br />
                <br />
                9. This Agreement is binding on you and us unless and until
                <br />
                <br />
                9.1 we notify you (in any manner, whether telephonically, electronically, in writing
                or in person) that we are terminating this Agreement with effect from a specified
                future date, which we may do at any time;
                <br />
                <br />
                9.2 you notify us (in any manner, whether telephonically, electronically, in writing
                or in person) that you are terminating this Agreement with effect from a specified
                future date, which you may do at any time subject to clauses 11 and 12; or
                <br />
                <br />
                9.3 in case of annual subscriptions, the period for which you have paid, and we
                have received, full and valid payment of your fees in terms of this Agreement expires
                and you have not yet renewed your subscription for a subsequent period and your
                subscription to the MultiChoice Service is disabled by us.
                <br />
                <br />
                10. The termination or expiry of this Agreement will not affect
                <br />
                <br />
                10.1 our rights or remedies, or yours, for the period prior to termination or expiry,
                as the case may be; or<br />
                <br />
                10.2 those rights and obligations which this Agreement intends, either expressly
                or by implication, will survive beyond termination.<br />
                <br />
                11. You may at any time terminate the Agreement by notifying us thereof and such
                termination shall take effect from the last day of the billing month on which the
                termination notice is received; or, in the case of annual subscriptions, at the
                end of the month on which the termination notice is received.
                <br />
                <br />
                12. It is your responsibility to ensure that your notice of termination is received
                by us at least five business days prior to the Due Date in respect of the next billing
                month. If we do not receive your notice of termination by then, we cannot guarantee
                that payment instructions for the following billing month will be cancelled in time,
                and you hereby waive any claim against us in respect of any costs or expenses incurred
                as a result thereof.<br />
                <br />
                <b><a id="TCMHardware"></a>Hardware</b><br />
                <br />
                13. Other than in respect of the Drifta, only you and members of your household
                may use the Smartcard to access the MultiChoice Service.<br />
                <br />
                14. You may not tamper with the Smartcard or other Equipment or attempt to use it
                for any purpose not authorised by us.<br />
                <br />
                15. We may disable the Smartcard, whether temporarily or permanently, if –<br />
                <br />
                15.1 any unauthorised person uses the Smartcard;<br />
                <br />
                15.2 the Smartcard is damaged, lost or stolen;
                <br />
                <br />
                15.3 you are in breach of this Agreement;
                <br />
                <br />
                15.4 the period for which you have paid for your subscription to the MultiChoice
                Service has expired and we have not received payment for the following subscription
                period,
                <br />
                <br />
                15.5 it is necessary to protect the integrity of the conditional access system used
                for the MultiChoice Service; or<br />
                <br />
                15.6 it is otherwise reasonable to do so.<br />
                <br />
                16. We recommend that you
                <br />
                <br />
                16.1 access the MultiChoice Service by using a Smartcard supplied by us in an Approved
                Decoder and in accordance with the applicable User Manual, if any;
                <br />
                <br />
                16.2 keep the Smartcard in the Approved Decoder; and<br />
                <br />
                16.3 keep your decoder connected to the main power supply and to the satellite reception
                equipment, at least in standby mode when not in use, to enable the updating of the
                software in the decoder via satellite and in order to ensure that your continued
                access to the MultiChoice Service is not negatively affected. You acknowledge that
                in standby mode certain Approved Decoders generate heat and that you will position
                your decoder with these factors in mind.<br />
                <br />
                17. We cannot guarantee that you will be able to access the MultiChoice Service,
                or any aspect thereof, either optimally or at all, if you do not act in accordance
                with our recommendations in clauses 13, 14 and 16.<br />
                <br />
                18. If your Smartcard is damaged, lost or stolen
                <br />
                <br />
                18.1 you must inform us immediately; and
                <br />
                <br />
                18.2 we or one of our duly authorised representatives will replace the Smartcard
                subject to the payment by you of a replacement fee.<br />
                <br />
                <b><a id="TCMUserManual"></a>User Manual</b><br />
                <br />
                19. You agree to comply with the User Manual applicable to the Approved Decoder
                used by you, if any.
                <br />
                <br />
                20. If there is any conflict between the provisions of this Agreement and the User
                Manual, this Agreement will prevail.<br />
                <br />
                Warranty<br />
                <br />
                21. Subject to the provisions of the Consumer Protection Act (if and to the extent
                applicable), if and for so long as the period of the Manufacturer’s warranty in
                relation to your Approved Decoder has not expired and you are not in breach of the
                terms thereof, you may be entitled to have the decoder repaired or replaced subject
                to the terms of that warranty.
                <br />
                <br />
                <b><a id="TCMAdvancedDecoderFunctionality"></a>Advanced Decoder Functionality</b><br />
                <br />
                22. If you own a decoder with Advanced Decoder Functionality you may request us
                to authorise you to use one or more such functions for purposes of accessing and
                using the MultiChoice Service, for which you agree to pay the requisite fees as
                amended by us from time to time. The provisions of clause 23 apply if we agree to
                your Request by authorising you to use such functionality.
                <br />
                <br />
                23. You acknowledge that
                <br />
                <br />
                23.1 Advanced Decoder Functionalities are developing technologies;
                <br />
                <br />
                23.2 part of the memory space in the HDD is reserved for use by us for future applications,
                fault reporting and capturing user profiles;<br />
                <br />
                23.3 we are not liable to you for any loss of memory space in the HDD or for any
                loss or corruption of content recorded on the HDD;<br />
                <br />
                23.4 the memory space available for your use on the HDD is a finite resource and
                we will have no obligation to provide you with additional memory space if you exhaust
                the memory space in your HDD.<br />
                <br />
                24. If we agree to your Request by authorising you to use the Advanced Decoder Functionality
                you may only use it for so long as this Agreement remains in force and you comply
                with all the provisions of the Agreement. For clarity, if we suspend your authority
                to access the MultiChoice Service and/or if the Agreement expires or is terminated
                you will no longer have the right to use the Advanced Decoder Functionality or to
                access any content recorded on the HDD.<br />
                <br />
                25. While we have no reason to believe that subscribers in South Africa and Lesotho
                will not be able to use the Advanced Decoder Functionality, we cannot guarantee
                that such functionality will be available at all places in South Africa and Lesotho.
                <br />
                <br />
                26. Clause 36.11 will apply if you sell or otherwise transfer a decoder with Advanced
                Decoder Functionality. You acknowledge that any content recorded on the HDD may
                not be sold or transferred to any person.<br />
                <br />
                <div style='display: <%= HideBoxOffice ? "none" : "" %>'>
                    <b><a id="TCMBoxOfficeService"></a>BoxOffice Service</b><br />
                    <br />
                    27. The BoxOffice Service is only available to Subscribers using PVR Decoders and
                    with active MultiChoice subscription accounts. This service is designed to enable
                    you to select and view Movie Titles as are made available by MultiChoice on the
                    BoxOffice Service as follows-<br />
                    <br />
                    27.1 Movie Titles will be available on the BoxOffice Service for a period predetermined
                    by MultiChoice from time to time and set out on the Website, subject to clause 30
                    below (“Availability Period”);<br />
                    <br />
                    27.2 upon the expiry of the Availability Period, the Movie Title(s) will be deleted
                    from your PVR Decoder. Once deleted, the Movie Title(s) will no longer be made available
                    to you to purchase on the BoxOffice Service.<br />
                    <br />
                    28. A Movie Title may be viewed by you for a maximum of 48 hours after your purchase
                    transaction has been completed and an entitlement instruction has been sent by us
                    to your PVR Decoder authorising the viewing of the purchased Movie Title (“Viewing
                    Period”).<br />
                    <br />
                    29. During the Viewing Period, there are no limitations on the number of times that
                    you may view a Movie Title.<br />
                    <br />
                    30. Upon the expiry of the Viewing Period the Movie Title will no longer be accessible
                    for purposes of viewing, whether or not you have commenced viewing or have viewed
                    the whole Movie Title. You will be required to complete an additional purchase transaction
                    in order to view the same Movie Title for any additional Viewing Period.<br />
                    <br />
                    <b><a id="TCMBoxOfficeRegistration"></a>BoxOffice Registration</b><br />
                    <br />
                    31. You will have to register on the Website in order to access and use the BoxOffice
                    Service.
                    <br />
                </div>
                <br />
                Drifta<br />
                <br />
                32. The Drifta must be used within the DVB-H Coverage Areas set out on the Website
                (the “DVB-H Coverage Areas”) in order for it to work.<br />
                <br />
                33. The Drifta is only compatible with certain devices. A list of devices compatible
                with the Drifta can be found on the Website.<br />
                <br />
                34. Only Bouquets set out on the Website are available on the Drifta device.<br />
                <br />
                Restrictions on access to and use of MultiChoice Service<br />
                <br />
                35. You may receive, access and/or use the MultiChoice Service and the Equipment
                only
                <br />
                <br />
                35.1 in a single residential unit;<br />
                <br />
                35.2 for private domestic use; and<br />
                <br />
                35.3 save for the Drifta and its accessories, at the address in South Africa or
                Lesotho provided by you and as recorded in our billing system.
                <br />
                <br />
                36. You may not use the MultiChoice Service in any manner or for any purpose other
                than as set out in these terms and conditions. Nor may you, whether intentionally
                or negligently, permit any other person to do so. Without limiting the restrictions
                in clause 35, you may not attempt to or
                <br />
                <br />
                36.1 use decoders that enable you to receive and/or access the MultiChoice Service
                on multiple viewing devices (as a way of illustration, XtraView decoders), in different
                residential units;
                <br />
                <br />
                36.2 access any aspect of the MultiChoice Service other than those aspects to which
                we have authorised you to have access;<br />
                <br />
                36.3 use the MultiChoice Service, or any aspect thereof, for any commercial purpose;
                <br />
                <br />
                36.4 receive and/or use the MultiChoice Service in a hotel, motel, pub, club, hostel,
                embassy, office, business or any similar premises;<br />
                <br />
                36.5 exhibit or provide the MultiChoice Service to the public, whether or not admission
                fees are charged;
                <br />
                <br />
                36.6 charge any person a fee to access any aspect of the MultiChoice Service;<br />
                <br />
                36.7 copy any of the content of the MultiChoice Service, except by using the PVR
                Functionality for timeshifting or later private use during the term of this Agreement;<br />
                <br />
                36.8 hire-out, sell, redistribute, relay, retransmit or rebroadcast any of the content
                of the MultiChoice Service, including any copy thereof that you may have made, whether
                using the Advanced Decoder Functionality or otherwise;<br />
                <br />
                36.9 hack, reverse engineer or otherwise compromise the security of the conditional
                access system, operating software or encryption software used in the Smartcard and
                any decoder used to receive the MultiChoice Service, or in the HDD of a decoder;<br />
                <br />
                36.10 permit, facilitate or condone any other person doing any of the prohibited
                activities in this clause 36, whether using your Equipment or otherwise; or
                <br />
                <br />
                36.11 sell or otherwise transfer the Equipment without advising us in writing, within
                seven days, of the identity and contact details of the transferee. You indemnify
                us against any claim by the transferee in relation to such sale or transfer, including
                any claim by reason of the non functionality of the Equipment, including, in the
                case of an Approved Decoder with Advanced Decoder Functionality, the non functionality
                of the advanced functions or by reason of offensive content stored in the HDD.<br />
                <br />
                37. If you wish to receive and/or use the MultiChoice Service at an address other
                than the address referred to in clause 35.3, you must request our prior consent
                to do so, which request we may consider.<br />
                <br />
                38. You indemnify us and our affiliated companies, and their directors, officers,
                employees, agents and representatives (“Indemnified Parties”) against any claim
                by a third party in respect of a breach by you of clause 35 and/or 36.
                <br />
                <br />
                39. You acknowledge that a breach by you of the provisions of clauses 35 and/or
                36 may constitute criminal activity and could result in considerable prejudice and
                damage to us and/or our licensors and/or channel suppliers.
                <br />
                <br />
                <b><a id="TCMFees"></a>Fees</b><br />
                <br />
                40. We provide the MultiChoice Service on a "pre paid" basis.<br />
                <br />
                41. In order for you to receive the MultiChoice Service, you must pay us directly
                or via one of our duly authorised representatives the requisite fees for those aspects
                of the MultiChoice Service to which you have Requested access (including, without
                limitation, in respect of any Advanced Decoder Functionality) and any VAT and all
                other taxes, duties, levies or charges that may be levied by any government authority
                directly or indirectly in relation to the MultiChoice Service.
                <br />
                <br />
                42. You must make, and we must receive, these payments in advance and in full for
                the forthcoming subscription period on or before the Due Date.
                <br />
                <br />
                43. We will activate your subscription to the MultiChoice Service only once, and
                as soon as practicable after, you have made, and we have received, payment in full
                in terms of clause 41.<br />
                <br />
                44. If we, notwithstanding clause43, activate your subscription to the MultiChoice
                Service on a date before the next Due Date, as selected by you or specified by us,
                then you must pay, on a pro rata basis, the requisite fees for the period for which
                your subscription was activated prior to the Due Date plus the requisite fees for
                the forthcoming subscription period.
                <br />
                <br />
                45. If you do not pay the requisite fees in full for the forthcoming subscription
                period on or before the Due Date, your subscription to the MultiChoice Service will
                automatically expire and your Smartcard will be disabled at the end of the subscription
                period for which you have paid.<br />
                <br />
                46. You may not deduct from, or set off against, the fees which you are required
                to pay to us in terms of this Agreement, any amount whatsoever which you claim from
                us or which we owe you.<br />
                <br />
                47. We may from time to time, in our sole discretion, change the fees payable to
                us for the MultiChoice Service, or any aspect thereof, by way of a General Amendment.
                Whilst we will endeavour to notify you at least one month in advance of such change,
                we cannot guarantee that we will do so.
                <br />
                <br />
                48. We may alter the Payment Instruction under which you pay your fees to us if
                the amounts payable by you to us should change for any reason. We may also charge
                and recover from you under that same instruction any other amounts owing by you
                to us under this Agreement.<br />
                <br />
                49. This agreement, read with your Schedule, is deemed to be a tax invoice as it
                meets all of the requirements as detailed in section 20(7) of the Value Added Tax
                Act, 1991. You may at any time request an invoice and/or statement of account in
                respect of your subscription to the MultiChoice Service and we will provide one
                to you. You may also access an invoice and/or statement of account at any time on
                the Website.
                <br />
                <br />
                <div style='display: <%= HideBoxOffice ? "none" : "" %>'>
                    50. In the case of the BoxOffice Service, you will be required to:<br />
                    <br />
                    50.1 open a BoxOffice account with MultiChoice; and<br />
                    <br />
                    50.2 credit your BoxOffice account by way of online credit card payments, bank transfers
                    made on the Website as well as cash payments made at any MultiChoice branch in denominations
                    as determined by you.
                    <br />
                    <br />
                    51. The completion of each transaction for a Movie Title shall be subject to payment
                    of the applicable fee from your BoxOffice account. You hereby agree that upon submission
                    of the rental code to MultiChoice to access the BoxOffice Service, MultiChoice shall
                    be entitled to deduct as payment in consideration of the grant of rights to view
                    the Movie Title requested, the fee from your BoxOffice account. In the event that
                    there are insufficient funds in your BoxOffice account the transaction shall be
                    deemed incomplete and MultiChoice will not send an entitlement instruction to your
                    PVR Decoder to enable you to view the Movie Title. 52 MultiChoice may, at its election,
                    use payment systems owned and operated by third parties to facilitate the collection
                    of Fees and other amounts payable by you for the MultiChoice Service, BoxOffice
                    Service or such other service that we may provide from time to time (“Payment System
                    Providers”). We will not be liable to you or any third party for any loss or damage
                    which you may sustain or incur as a result of any error, system downtime or delay
                    experienced by such Payment System Provider.<br />
                    <br />
                </div>
                <b><a id="TCMCommunicationsWithUs"></a>Communications with us</b><br />
                <br />
                53. You may authorise another person to communicate with us on your behalf. Any
                person who communicates with us in relation to your subscription will be deemed
                to be authorised by you to represent you if that person meets our standard security
                check. You agree to be bound by all requests and undertakings made by third parties
                on your behalf in terms of this clause. It is your responsibility to ensure that
                your personal information relevant to our standard security checks is not disclosed
                to third parties whom you have not authorised to represent you. Communications with
                you<br />
                <br />
                54. We may communicate with you, amongst other methods, by means of e mail, on-air
                communications, or by way of text or other messages to your cellular phone, PC,
                laptop or to your decoder for display on your television screen.<br />
                <br />
                55. The nature of such messages will vary, and we may use such messages (although
                we are not obliged to do so), amongst other things, to
                <br />
                <br />
                55.1 market or promote our services and/or those of our channel suppliers, affiliates
                and/or clients;<br />
                <br />
                55.2 advise you of the status of your account and any amounts owing by you to us;
                <br />
                <br />
                55.3 inform you that your subscription is about to expire unless it is renewed on
                or before a particular date;<br />
                <br />
                55.4 inform you about the MultiChoice Service and operational systems, and changes
                to these;
                <br />
                <br />
                55.5 notify you of a General Amendment; and/or
                <br />
                <br />
                55.6 provide you with information which we believe may be of particular interest
                or relevance to you.<br />
                <br />
                56. We will not be liable for any loss or damage suffered by you or any third party
                as a result of or in connection with communications with you in accordance with
                clause 55, and you indemnify and hold us harmless against any loss or damage that
                you or a third party may suffer as a result of any such communications.<br />
                <br />
                Access to and disclosure of information relating to you<br />
                <br />
                57. You authorise us to
                <br />
                <br />
                57.1 access from credit bureaux who are members of the Credit Bureau Association
                and subscribe to its Code of Conduct (“credit bureaux”) your personal information
                concerning financial risk and payment habits (“payment profile”) for purposes of
                credit information sharing, fraud prevention and debtor tracing, and to disclose
                information regarding your payment profile to such credit bureaux;
                <br />
                <br />
                57.2 obtain, capture, store, analyse and use for our marketing purposes the viewing
                habits and profile of you and the members of your household, including to retrieve
                such information from your decoder;<br />
                <br />
                57.3 use data that we may hold in relation to you ("your personal information")
                for the purposes of
                <br />
                <br />
                57.3.1 processing your Requests;<br />
                <br />
                57.3.2 administering this Agreement;<br />
                <br />
                57.3.3 informing you of any amendments to this Agreement;<br />
                <br />
                57.3.4 informing you of any new aspects of the MultiChoice Service or services provided
                by our affiliates;
                <br />
                <br />
                57.3.5 informing you of promotional competitions;<br />
                <br />
                57.4 disclose your personal information
                <br />
                <br />
                57.4.1 to companies affiliated to us for purposes of marketing the services of those
                affiliated companies;
                <br />
                <br />
                57.4.2 to any company which acquires our business or any part thereof; or
                <br />
                <br />
                57.4.3 to any company we are associated with from time to time;
                <br />
                <br />
                57.4.4 if and to the extent that we are required to do so to comply with any applicable
                law, including the requirements of statutory authorities; and
                <br />
                <br />
                57.5 retain your personal information referred to in this clause 57 indefinitely.<br />
                <br />
                58 It is your responsibility to proactively ensure that the data which you provide
                to us is complete, accurate and up to date.
                <br />
                <br />
                <b><a id="TCMIntellectualProperty"></a>Intellectual property</b><br />
                <br />
                59 You acknowledge that
                <br />
                <br />
                59.1 the Content Rights and the System Rights are either owned by, or licensed to,
                us or our affiliates and you acquire no right or interest in such rights; and
                <br />
                <br />
                59.2 you will use your best endeavours to maintain and protect our rights and interests,
                and that of our suppliers and licensors, in the Content Rights and the System Rights.<br />
                <br />
                60. You indemnify us and the other Indemnified Parties against any claim by a third
                party in respect of a breach by you of clause 59.2.
                <br />
                <br />
                61. You understand that the use of the MultiChoice Service may include and/or rely
                on a technological security framework designed to protect the MultiChoice Service
                against unauthorised use, which framework will be automatically implemented, maintained
                and amended from time to time. You hereby agree to such implementation, maintenance
                and amendment of the technological security framework, which may include the disconnection
                or discontinuation of any features of the Equipment which facilitate unauthorised
                use of the MultiChoice Service. A failure by MultiChoice to implement the whole
                or part of the security framework will not constitute a grant or waiver of any of
                its rights resulting from unauthorised use of the MultiChoice Service in terms of
                these terms and conditions.<br />
                <br />
                <b><a id="TCMMultiChoiceObligations"></a>MultiChoice's obligations</b><br />
                <br />
                62. In consideration for the fees paid by you, and subject to you complying with
                this Agreement, we, in accordance with this Agreement, will provide you with access
                to those aspects of the MultiChoice Service which you have subscribed to.<br />
                <br />
                63. Subject to you complying with this Agreement, our authorising you to have access
                to the MultiChoice Service will continue for the duration of your subscription period,
                provided that we will have no obligation
                <br />
                <br />
                63.1 to ensure that the Equipment is installed correctly;<br />
                <br />
                63.2 subject to the terms of the Manufacturers' warranties and/or the decoder care
                plan (if that plan is available and you have chosen to be covered by that plan)
                –<br />
                <br />
                63.2.1 to ensure that the Equipment is or remains functional or error-free and enables
                you to receive the MultiChoice Service;
                <br />
                <br />
                63.2.2 to ensure that any aspect of the Advanced Decoder Functionality is or remains
                functional or error-free;<br />
                <br />
                63.2.3 to maintain any component or aspect of the Equipment or the Advanced Decoder
                Functionality; or<br />
                <br />
                63.3 to ensure the integrity or error-free playback of the content copied to the
                HDD.<br />
                <br />
                64. Certain content made available by us may be subject to age restrictions, strong
                language or other material which may offend sensitive users or which may be inappropriate
                for younger users. In this regard, we will attempt to provide appropriate audience
                guidelines relating to the relevant content but we assume no responsibility whatsoever
                for the implementation of those audience guidelines on any device. You shall take
                all reasonable steps to prevent the viewing of such content by children below the
                prescribed age restriction specified by us, our channels and content suppliers or
                by any applicable regulatory authority. Subscriber's obligations<br />
                <br />
                65. Subject to this Agreement, and in addition to any other obligations imposed
                on you in terms of this Agreement, you must
                <br />
                <br />
                65.1 provide us with your personal information required by us in order to activate
                and administer your subscription;
                <br />
                <br />
                65.2 inform us in writing within seven days of any change to any of the information
                provided by you in relation to this Agreement;<br />
                <br />
                65.3 inform us immediately if you become aware of
                <br />
                <br />
                65.3.1 any act or attempt by any party in relation to the Equipment or any aspect
                of the MultiChoice Service which, if committed by you, would be a breach of this
                Agreement;<br />
                <br />
                65.3.2 any damage to, loss, theft or unauthorised use of the Equipment;
                <br />
                <br />
                65.4 inform us in writing within seven days if you transfer your Equipment to any
                other person, and provide us with the identity and contact details of the transferee;
                <br />
                <br />
                65.5 use the parental control mechanism on the MultiChoice Service to block your
                and the members of your household's access to content on the MultiChoice Service
                which you consider inappropriate or undesirable.<br />
                <br />
                <b><a id="TCMNoWarranties"></a>No warranties, limitation of liability, and indemnities</b><br />
                <br />
                66. Subject to the relevant provisions of the Consumer Protection Act (if and to
                the extent applicable) we make no warranty or representation, whether expressly
                or implicitly,
                <br />
                <br />
                66.1 subject to the terms of the warranty and a decoder care plan described in clause
                63.2, as regards any component of the Equipment nor that you will be able to access,
                or to continue to access, the MultiChoice Service using the Equipment, nor that
                such access will be continuous and/or uninterrupted;<br />
                <br />
                66.2 as to the Coverage Area, the DVB-H Coverage Areas or that the MultiChoice Service,
                or any aspect thereof, will be available in all parts of the Coverage Area or the
                DVB-H Coverage Areas; or
                <br />
                <br />
                66.3 as to the quality of reception by you of the MultiChoice Service, or any aspect
                thereof, in the Coverage Area to the extent that this results from causes beyond
                our control; or
                <br />
                <br />
                66.4 as to the content of the MultiChoice Service, including as regards the subject,
                nature, quality, reliability, truthfulness or accuracy of the content, or that the
                content will meet your particular tastes or expectations.<br />
                <br />
                67. Subject to the terms of the warranty and a decoder care plan described in clause
                63.2, and the relevant provisions of the Consumer Protection Act (if and to the
                extent applicable) we are not liable
                <br />
                <br />
                67.1 for any loss or damage suffered by you or any other third party, which arises
                out of
                <br />
                <br />
                67.1.1 any act or omission of ours or our employees or agents, subject to the provisions
                of clause 67.4;<br />
                <br />
                67.1.2 any act or omission of our consultants, subcontractors or affiliated companies;<br />
                <br />
                67.1.3 the exercise by us of any of our rights in terms of this Agreement; or<br />
                <br />
                67.1.4 any breach by you of your obligations under this Agreement, and you specifically
                indemnify us and the Indemnified Parties against any claim by you or any third party
                arising out of such breach;<br />
                <br />
                67.2 to any person for the content on and/or the use of materials constituting the
                MultiChoice Service, whether provided by us or a third party, and you agree that
                –
                <br />
                <br />
                67.2.1 the MultiChoice Service may contain images and/or content that may be regarded
                as unsuitable or offensive by some viewers;
                <br />
                <br />
                67.2.2 we mainly acquire complete channels from channel suppliers and package them
                into Bouquets and as such we have no influence over, and are unable to alter, the
                content of the channels; and<br />
                <br />
                67.2.3 the channels on each Bouquet may vary from time to time, and that the content
                of each individual channel may, from time to time, vary;<br />
                <br />
                67.3 for any delay or failure by us to provide the MultiChoice Service, or any aspect
                thereof, to you to the extent that such delay or failure results from causes beyond
                our direct or indirect control;
                <br />
                <br />
                67.4 for any delay, interruption, defect or failure in the distribution or reception
                of the MultiChoice Service, or any aspect thereof, regardless of the nature, duration
                or cause thereof, in the absence of gross negligence or wilful default on our part.
                If such delay, interruption, defect or failure is due to our gross negligence or
                willful default, then you will be entitled, as your sole and exclusive remedy, to
                a credit against future payments of subscription fees equal to the pro-rata portion
                of the fees representing the period of the delay, interruption, defect or failure
                to the extent caused by our gross negligence or willful default;
                <br />
                <br />
                67.5 for any delay, interruption, defect or failure in the distribution or reception
                of the MultiChoice Service, or any aspect thereof, regardless of the nature, duration
                or cause thereof, which we deem necessary for any purpose related to our business,
                including, without limitation, in order to support the provision, operation, maintenance
                and security of the MultiChoice Service, or any aspect thereof;
                <br />
                <br />
                67.6 subject to the provisions of the Consumer Protection Act, the terms of the
                Manufacturers' warranties and/or the decoder care plan (if that plan is available
                and you have chosen to be covered by that plan), for any defect in or failure or
                malfunction of the Equipment, regardless of the nature or cause thereof. 68. Although
                we strongly recommend the use of MultiChoice accredited installers to install any
                Equipment requiring installation, each installer acts as an independent contractor
                and is not an employee, subcontractor, agent or representative of MultiChoice. Neither
                the Manufacturer(s) of the Equipment nor we are liable for any loss or damage of
                any nature whatsoever caused by the conduct of an installer, including as a result
                of
                <br />
                <br />
                68.1 any breach by an installer of any of its obligations to you; and<br />
                <br />
                68.2 an act or omission on the part of an installer.<br />
                <br />
                69. You indemnify the Indemnified Parties against any claim by any third party arising
                from a breach by you of any provision of this Agreement.<br />
                <br />
                70. No provision of this Agreement is intended to exclude or limit our liability
                for any loss attributable to our gross negligence or willful default.
                <br />
                <br />
                Amendments to and variations in the MultiChoice Service<br />
                <br />
                71. The nature, composition and content of the MultiChoice Service are determined
                by us in our sole discretion, and may be changed by us from time to time.
                <br />
                <br />
                72. Without limiting clause 71, we may –
                <br />
                <br />
                72.1 replace, remove or otherwise amend, or restrict the availability of, any aspect
                of the MultiChoice Service, including, but not limited to -<br />
                <br />
                72.1.1 any facets, applications, facilities, features and/or functionality of the
                MultiChoice Service;
                <br />
                <br />
                72.1.2 the range, nature and format of the MultiChoice Service;<br />
                <br />
                72.1.3 the content of the MultiChoice Service, including but not limited to the
                number, nature, composition and content of specific Bouquets and the number, nature,
                composition and content of specific channels, including (without limitation) adding
                new programming, channels, services, and/or Bouquets, and replacing, removing or
                otherwise amending others; and
                <br />
                <br />
                72.1.4 the range, nature and number of any ancillary applications, facilities or
                services;
                <br />
                <br />
                72.2 advise you of any additional aspects of the MultiChoice Service which become
                available, the conditions applicable thereto and the charges therefore, if any,
                and which, if you request to receive this additional aspect, you will be obliged
                to pay the additional charge, if any;<br />
                <br />
                72.3 advise you of any aspect of the MultiChoice Service for which you must pay
                an additional charge if you wish to continue receiving it, and which, if you request
                to continue to receive this aspect, you will be obliged to pay the additional charge.<br />
                <br />
                73. The various systems necessary for or associated with the provision of the MultiChoice
                Service (including without limitation technical services, signal distribution and
                satellite capacity, the conditional access system, the software operating system,
                software applications, subscriber management services and business systems) are
                determined by us and are subject to ongoing innovation and change and may be amended
                by us from time to time. Without limiting the above you agree that we may
                <br />
                <br />
                73.1 update, vary or replace these systems or any aspect thereof, including, without
                limitation, as regards software by means of “over the air” software downloads, to
                address any system errors or other problems relating to the software, to improve
                security, to provide additional features or functionality, to limit any use of the
                Equipment outside the scope of this Agreement, and to ensure that the decoders used
                by Subscribers are not authorised to decrypt signals other than those authorised
                by us;
                <br />
                <br />
                73.2 recommend that you upgrade, reconfigure, change or replace (“upgrade”), at
                your cost if applicable, any of the Equipment used by you to access the MultiChoice
                Service, or any aspect thereof. Your continued access to the MultiChoice Service
                could be negatively affected if you do not act on this recommendation; or<br />
                <br />
                73.3 disable or remotely alter the functionality of the PVR Decoder to prevent you
                from copying certain programmes or channels if we become obliged to do so.<br />
                <br />
                74. You will have no rights, interests or expectations to any increases or decreases
                in the fees payable by you if we make any amendment to the MultiChoice Service in
                terms of clauses 71 to 73, or any other provision of this Agreement.<br />
                <br />
                Amendment of Agreement
                <br />
                <br />
                75. You agree to be bound by this Agreement and the User Manual as amended from
                time to time.
                <br />
                <br />
                76. We may, in our sole discretion, amend this Agreement and/or the User Manual
                in any respect from time to time by way of a General Amendment.
                <br />
                <br />
                77. You agree to be bound by such General Amendment from the date specified in the
                General Amendment notice, which will be a reasonable time after the date of the
                General Amendment notice.
                <br />
                <br />
                78. No amendment requested by you will be valid or effective unless either captured
                in a General Amendment or recorded in writing and signed by you and by us.<br />
                <br />
                Breach of Agreement<br />
                <br />
                79. Your failure to comply with this Agreement or the User Manual constitutes a
                material breach of the Agreement.<br />
                <br />
                80. If you breach this Agreement
                <br />
                <br />
                80.1 we may, without prejudice to any other remedy that we may have, immediately
                and without notice to you, disable the Smartcard, terminate your authority to have
                access to the MultiChoice Service, and/or terminate this Agreement depending on
                the severity of such breach;
                <br />
                <br />
                80.2 you must pay us all legal costs, including attorney and own client costs, tracing
                agent’s fees and collection charges which we may incur in taking any steps pursuant
                to your breach; and<br />
                <br />
                80.3 you must pay us any loss or damages incurred by us directly or indirectly as
                a result of your breach.<br />
                <br />
                81. If you –
                <br />
                <br />
                81.1 remedy your breach;
                <br />
                <br />
                81.2 comply with this Agreement; and
                <br />
                <br />
                81.3 pay us all amounts due to us in terms of this Agreement and any reconnection
                fee stipulated by us,
                <br />
                <br />
                then we may reactivate your subscription to the MultiChoice Service.
                <br />
                <br />
                If the Agreement had been terminated, you may submit a Request with a view to us
                concluding a new Agreement.
                <br />
                <br />
                <b><a id="TCMGeneral"></a>General</b><br />
                <br />
                82. This Agreement
                <br />
                <br />
                82.1 constitutes the sole and complete record of the agreement between you and us
                in regard to its subject matter; and
                <br />
                <br />
                82.2 supersedes any previous agreement between you and us, or between you and any
                other company in the MultiChoice Group, in terms of which you were authorised to
                have access to the MultiChoice Service.<br />
                <br />
                83. Neither you nor we are bound by any express or implied representation, warranty,
                undertaking, promise or the like not recorded in this Agreement.<br />
                <br />
                84. Any relaxation or indulgence which we may show you at any time in regard to
                this Agreement is without prejudice to, and does not constitute a waiver of, any
                rights we may have, either in terms of this Agreement or any law.<br />
                <br />
                85. If any provision of this Agreement is found to be wholly or partly invalid,
                unenforceable or unlawful, then
                <br />
                <br />
                85.1 this Agreement will be severable in respect of the provision in question to
                the extent of its invalidity, unenforceability or unlawfulness; and
                <br />
                <br />
                85.2 the remaining provisions of this Agreement will remain in full force and effect.<br />
                <br />
                86. The rule of construction that this Agreement will be interpreted against the
                party responsible for the drafting or preparation of this Agreement will not apply.<br />
                <br />
                87. We may cede any of our rights and/or assign any of our obligations under this
                Agreement to any person.<br />
                <br />
                88. You may not cede any of your rights and/or assign any of your obligations under
                this Agreement to any person.
                <br />
                <br />
                89. This Agreement is subject to, and will be interpreted, implemented and enforced,
                in terms of the laws of South Africa.<br />
                <br />
                90. You consent to the jurisdiction of the Magistrates Court in respect of all proceedings
                arising out of or pursuant to this Agreement. We may, in our discretion, institute
                any proceedings arising out of or pursuant to this Agreement in any Division of
                the High Court of South Africa having jurisdiction.
                <br />
                <br />
                91. The parties choose the following addresses for the service of all notices and
                processes arising out of this Agreement
                <br />
                <br />
                91.1 MultiChoice: 251 Oak Avenue, Randburg, 2125, Gauteng, South Africa.<br />
                <br />
                91.2 You: The physical address supplied by you when making your Request and as recorded
                in our billing system, as amended by you from time to time.<br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
