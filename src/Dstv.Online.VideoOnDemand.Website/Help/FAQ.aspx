﻿<%@ Page Title="FAQ" Language="C#" MasterPageFile="~/MasterPages/TitleAndVerticalSplitContentWithLeftNav.master"
    AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Help.FAQ" %>

<asp:Content ID="contentTitle" ContentPlaceHolderID="cphTitle" runat="server">
    Frequently Asked Questions
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            
             <%=ToggleCode%>
        });

    </script>
    <script type="text/javascript">
    /*** START Created by Titi ***/
        $(document).ready(function () {
            $('.AccordionPanel').removeClass('AccordionPanel_SubItemOpen');
            $('#m1').addClass('AccordionPanel_SubItemTabHover');
            Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
                $('#m1').addClass('AccordionPanel_SubItemTabHover');
                $('#m2').removeClass('AccordionPanel_SubItemTabHover');
            };
        });

        function doClick(obj) {
            if (obj.id == "m1") {
                oppositeId = "#m2";
            } else {
                oppositeId = "#m1";
            }
            $(oppositeId).removeClass('AccordionPanel_SubItemTabHover');
            $(oppositeId).mouseout(function () {
                $(oppositeId).removeClass('AccordionPanel_SubItemTabHover');
            });

            Spry.Widget.Accordion.prototype.onPanelTabClick = function (e, panel) {
                if (panel != this.currentPanel) {
                    this.openPanel(panel);
                    $('.AccordionPanel').removeClass('AccordionPanel_SubItemOpen');
                    Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
                        $('#' + obj.id).addClass('AccordionPanel_SubItemTabHover');
                    };
                } else {
                    this.closePanel();
                    Spry.Widget.Accordion.prototype.onPanelTabMouseOut = function (e, panel) {
                        $('#' + obj.id).removeClass('AccordionPanel_SubItemTabHover');
                    };
                }
            }
        }
    /*** END Created by Titi ***/
	</script>
    <div class="hdrFilter"><img src="/app_themes/onDemand/images/arrows_filterHdr_down.png" width="10" height="12" />FILTER FAQs</div>
    <div id="Accordion1" class="Accordion" tabindex="0">
        <div class="AccordionPanel">
            <div class="AccordionPanelTab" id="m1" onclick="doClick(this);">ON DEMAND</div>
            <div id="divOndemand" class="AccordionPanelContent">
                <ul>
                    <li><a id="liOnDemand_Overview" onclick="ToggleFilterMenuStyle('liOnDemand_Overview', 'divOndemand'); ToggleFAQ('divOnDemand_Overview_FAQ');"
                        href="#" class="down"><strong>Product Overview</strong></a></li>
                    <%--<li><a id="liOnDemand_CSR" onclick="ToggleFilterMenuStyle('liOnDemand_CSR', 'divOndemand'); ToggleFAQ('divOnDemand_CSR_FAQ');"
                        href="#" class="down"><strong>Some CSR queries</strong></a></li>--%>
                </ul>
            </div>
        </div>
        <%if (!HideBoxOffice)
          { %>
        <div class="AccordionPanel">
            <div class="AccordionPanelTab">
                BOXOFFICE</div>
            <div id="divBoxOffice" class="AccordionPanelContent">
                <ul>
                    <li><a id="liBoxOffice_About" onclick="ToggleFilterMenuStyle('liBoxOffice_About', 'divBoxOffice'); ToggleFAQ('divBoxOffice_About_FAQ');"
                        href="#" class="down"><strong>All about DStv BoxOffice</strong></a></li>
                    <li><a id="liBoxOffice_Content" onclick="ToggleFilterMenuStyle('liBoxOffice_Content', 'divBoxOffice'); ToggleFAQ('divBoxOffice_Content_FAQ');"
                        href="#" class="down"><strong>All about DStv BoxOffice Content</strong></a></li>
                    <li><a id="liBoxOffice_Troubleshooting" onclick="ToggleFilterMenuStyle('liBoxOffice_Troubleshooting', 'divBoxOffice'); ToggleFAQ('divBoxOffice_Troubleshooting_FAQ');"
                        href="#" class="down"><strong>DStv BoxOffice - Troubleshooting</strong></a></li>
                    <li><a id="liBoxOffice_Registration" onclick="ToggleFilterMenuStyle('liBoxOffice_Registration', 'divBoxOffice'); ToggleFAQ('divBoxOffice_Registration_FAQ');"
                        href="#" class="down"><strong>All about DStv BoxOffice Registration and the DStv Wallet</strong></a></li>
                </ul>
            </div>
        </div>
        <% } %>
        <div class="AccordionPanel">
            <div class="AccordionPanelTab" id="m2" onclick="doClick(this);">CATCHUP</div>
            <div id="divCatchUp" class="AccordionPanelContent">
                <ul>
                    <li><a id="liCatchUp_About" onclick="ToggleFilterMenuStyle('liCatchUp_About', 'divCatchUp'); ToggleFAQ('divCatchUp_About_FAQ');"
                        href="#" class="down"><strong>All about DStv CatchUp</strong></a></li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="cphRightContent" runat="server">
    <div class="block700">
        <div id="divOnDemand_Overview_FAQ">
            <div class="block700top">
                <h4 class="home">
                    <b>Product Overview</b></h4>
            </div>
            <div class="faq_QuesDiv">
                <div class="faq_Questions">
                    <a href="#OD_Overview1">What is DStv On Demand?</a><br />
                    <a href="#OD_Overview2">What is DStv CatchUp?</a><br />
                    <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                        <a href="#OD_Overview3">What is DStv BoxOffice?</a><br />
                    </div>
                    <a href="#OD_Overview4">What decoder do I need to access DStv On Demand /
                        <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                            DStv BoxOffice /</div>
                        DStv CatchUp?</a><br />
                    <a href="#OD_Overview5">What impact does DStv On Demand have on my scheduled recordings?</a><br />
                    <a href="#OD_Overview6">Will I have less space for my recordings?</a><br />
                    <a href="#OD_Overview7">Does the DStv On Demand content have HI subtitling?</a><br />
                    <a href="#OD_Overview8">Is DStv On Demand content available in widescreen?</a><br />
                    <a href="#OD_Overview9">What happens if a reminder triggers while I'm watching DStv
                        On Demand content?</a><br />
                    <a href="#OD_Overview10">Why is there copy protection on DStv On Demand content?</a><br />
                    <a href="#OD_Overview11">I pressed the RED button and right arrow to access my Scheduled
                        Recordings, but they're not there. Where did they go?</a><br />
                </div>
            </div>
            <div class="greyHdrLine6">
            </div>
            <div class="padding20px">
            </div>
            <div class="faq_Answers">
                <div id="OD_Overview1">
                    <b>What is DStv On Demand?</b>
                    <div>
                        DStv On Demand<sup></sup> is available on the SD PVR and 4 tuner HD PVRs. It will
                        be available on the HD PVR 2P before year end. Please note, DStv On Demand is only
                        available to PVR customers with an active PVR subscription.
                        <br />
                        <br />
                        <b>DStv CatchUp</b> is available on the SD PVR and 4 tuner HD PVR to DStv Premium
                        subscribers with a PVR subscription.
                        <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                            <b>DStv BoxOffice</b> is available on the SD PVR and 4 tuner HD PVR PVR to all staff
                            in South Africa with a PVR subscription. Terms & Conditions apply. Pre-registration
                            is required.
                        </div>
                        <sup>#</sup>DStv On Demand is not available to commercial subscribers.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview2">
                    <b>What is DStv CatchUp?</b>
                    <div>
                        DStv CatchUp is a value added service available free to all DStv Premium subscribers
                        with a PVR subscription. It allows you the opportunity to "catch up" on a selection
                        of your favourite series & sporting highlights for up to 7* days after their first
                        broadcast on DStv.<br />
                        <br />
                        To access DStv CatchUp, press <b>RED</b> (to display the Playlist) and then press
                        the <b>RIGHT</b> arrow twice.<br />
                        <br />
                        DStv CatchUp is also available online. For more information about DStv CatchUp online,
                        please visit <a href="http://www.dstv.com">www.dstv.com</a>
                        <br />
                        <i>*may vary on certain titles, check expiry date for confirmation</i>
                    </div>
                </div>
                <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                    <div class="padding10px">
                    </div>
                    <div id="OD_Overview3">
                        <b>What is DStv BoxOffice?</b>
                        <div>
                            DStv BoxOffice is our video on demand service and allows you to rent and view the
                            latest blockbusters (not yet broadcast on DStv) from the comfort of your home.<br />
                            <br />
                            DStv BoxOffice is available on the PVR to all SA staff (MultiChoice, M-Net, SuperSport,
                            Oracle and DStv Online) with a valid PVR subscription. Terms & Conditions apply
                            and pre-registration is required.<br />
                            <br />
                            Please visit <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a> for
                            more information and to sign-up for DStv BoxOffice.<br />
                            <br />
                            <sup>#</sup><b>DStv BoxOffice</b> is not available to commercial subscribers.
                        </div>
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview4">
                    <b>What decoder do I need to access DStv On Demand /
                        <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                            DStv BoxOffice /
                        </div>
                        DStv CatchUp?</b>
                    <div>
                        DStv On Demand<sup>#</sup> is available on the SD PVR and 4 tuner HD PVRs. It will
                        be available on the HD PVR 2P before year end. Please note, DStv On Demand is only
                        available to PVR customers with an active PVR subscription.<br />
                        <br />
                        <b>DStv CatchUp</b> is available on the SD PVR and 4 tuner HD PVR to DStv Premium
                        subscribers with a PVR subscription.<br />
                        <br />
                        <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                            <b>DStv BoxOffice</b> is available on the SD PVR and 4 tuner HD PVR PVR to all staff
                            in South Africa with a PVR subscription. Terms & Conditions apply. Pre-registration
                            is required.<br />
                            <br />
                        </div>
                        <sup>#</sup><b>DStv On Demand</b> is not available to commercial subscribers.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview5">
                    <b>What impact does DStv On Demand have on my scheduled recordings?</b>
                    <div>
                        DStv On Demand has no impact on your scheduled recordings. It is important to note
                        however that on both the SD PVR and HD PVR 2P, DStv on Demand titles can only be
                        downloaded when no user recordings are taking place.</div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview6">
                    <b>Will I have less space for my recordings?</b>
                    <div>
                        No, the space available to you (up to 80 hours of recording on the SD PVR and up
                        to 150 hours on the HD PVR) remains unchanged.</div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview7">
                    <b>Does the DStv On Demand content have HI subtitling?</b>
                    <div>
                        The DStv On Demand content does not have HI subtitles.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview8">
                    <b>Is DStv On Demand content available in widescreen?</b>
                    <div>
                        Where possible, widescreen is made available.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview9">
                    <b>What happens if a reminder triggers while I'm watching DStv On Demand content?</b>
                    <div>
                        The standard reminder behavior occurs with the reminder pop-up displaying on-screen,
                        giving you the option to EXIT and remain viewing your current content.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview10">
                    <b>Why is there copy protection on DStv On Demand content?</b>
                    <div>
                        The studios from whom the content is sourced insist on this copy protection mechanism
                        to protect their content against piracy.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="OD_Overview11">
                    <b>I pressed the RED button and right arrow to access my Scheduled Recordings, but they're
                        not there. Where did they go?</b>
                    <div>
                        The Scheduled Recording screen has moved. To access Scheduled Recordings, press
                        <b>RED</b> button and then the <b>RIGHT</b> arrow 3 times OR press <b>RED</b> button
                        and then the <b>LEFT</b> arrow twice.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divOnDemand_CSR_FAQ">
        <div class="block700top">
            <h4 class="home">
                <b>Some CSR queries</b></h4>
        </div>
        <div class="faq_QuesDiv">
            <div class="faq_Questions">
                <a href="#OD_CSR1">What standard error messages can appear while viewing on demand content?</a><br />
                <a href="#OD_CSR2">If a customer's subscription is cutoff whilst he is viewing
                    <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                        BoxOffice or</div>
                    CatchUp content, will he be able to continue viewing that programme?</a><br />
                <a href="#OD_CSR3">What happens to DStv On Demand content if the viewer's product (PRM
                    or PVRPK) is disconnected and then reconnected? (Will the original content which
                    has not expired) still be listed & available?</a><br />
                <a href="#OD_CSR4">What might possibly impact on DStv On Demand? Ie scan, new software,
                    reboot, hot disk, etc.</a><br />
                <a href="#OD_CSR5">HELLO does not have the latest DStv On Demand schedules.</a><br />
                <a href="#OD_CSR6">What is the difference between FVOD, TVOD, SVOD and Push vs Pull
                    VOD?</a><br />
            </div>
        </div>
        <div class="greyHdrLine6">
        </div>
        <div class="padding20px">
        </div>
        <div class="faq_Answers">
            <div id="OD_CSR1">
                <b>What standard error messages can appear while viewing on demand content?</b>
                <div>
                    All the usual error messages can appear, ie. E04, E16, etc</div>
            </div>
            <div class="padding10px">
            </div>
            <div id="OD_CSR2">
                <b>If a customer's subscription is cutoff whilst he is viewing
                    <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                        BoxOffice or
                    </div>
                    CatchUp content, will he be able to continue viewing that programme?</b>
                <div>
                    No. The PVR access service is required to use DStv On Demand, if the PVR access
                    service is disconnected, the customer will not be able to view any DStv On Demand
                    content.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="OD_CSR3">
                <b>What happens to DStv On Demand content if the viewer's product (PRM or PVRPK) is
                    disconnected and then reconnected? (Will the original content which has not expired)
                    still be listed & available?</b>
                <div>
                    Yes.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="OD_CSR4">
                <b>What might possibly impact on DStv On Demand? Ie scan, new software, reboot, hot
                    disk, etc.</b>
                <div>
                    With the exception of signal loss, anything that would impact normal viewing, will
                    also impact viewing of DStv On Demand content.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="OD_CSR5">
                <b>HELLO does not have the latest DStv On Demand schedules.</b>
                <div>
                    Please escalate to <a href="mailto:support@dstvondemand.com">support@dstvondemand.com</a>
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="OD_CSR6">
                <b>What is the difference between FVOD, TVOD, SVOD and Push vs Pull VOD?</b>
                <div>
                    FVOD, TVOD and SVOD refer to the business models used for the content.<br />
                    <br />
                    Push vs Pull VOD refers to the technical architecture used to deliver the VOD content.<br />
                    <br />
                    <b>FVOD (Free Video On Demand)</b> – this is the model used for DStv CatchUp. The
                    service includes a number of titles which the viewer can watch without incurring
                    additional cost. There is no limit to the number of times the content may be viewed,
                    however the content expires at a predetermined time.<br />
                    <br />
                    <b>TVOD (Transactional Video On Demand / Pay-Per-View)</b> – this model
                    <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
                        (BoxOffice)
                    </div>
                    requires the viewer to pay for the content they want to view (example : R20 per
                    movie). There is no limit to the number of times the content may be viewed, however
                    the content expires after a predetermined viewing period (example : 36 hours after
                    paying for it). In this business model, we don't talk about viewers buying the content,
                    but rather renting it.<br />
                    <br />
                    <b>SVOD (Subscription Video On Demand)</b> – in this model, viewers are charged
                    a monthly fee (example : R40 per month) and can view all the content available on
                    the decoder. There is no limit to the number of times the content may be viewed,
                    however the content expires at a predetermined time. MultiChoice do not offer SVOD
                    services.<br />
                    <br />
                    <b>Push VOD</b> – this is the mechanism where content is "pushed" to the end device
                    (in our instance, the PVR) and where the viewer watches content which has already
                    recorded and is available.<br />
                    <br />
                    <b>Pull VOD</b> – this is the mechanism where the content is only "pulled" to the
                    end device once requested by the viewer. Example : the viewer can access a library
                    of hundreds of titles and then select the ones they wish to watch and/or pay for.
                    MultiChoice do not yet offer Pull VOD on the PVR.
                </div>
            </div>
        </div>
    </div>
    <div style='display: <% = HideBoxOffice ? "none" : "" %>'>
        <div id="divBoxOffice_About_FAQ">
            <div class="block700top">
                <h4 class="home">
                    <b>All about DStv BoxOffice</b></h4>
            </div>
            <div class="faq_QuesDiv">
                <div class="faq_Questions">
                    <a href="#BO_About1">What is DStv BoxOffice?</a><br />
                    <a href="#BO_About2">How does DStv BoxOffice work on the PVR?</a><br />
                    <a href="#BO_About3">Why is DStv BoxOffice not available outside SA? / When will BoxOffice
                        be available outside SA?</a><br />
                    <a href="#BO_About4">How much does a movie cost on DStv BoxOffice?</a><br />
                    <a href="#BO_About5">Why do the HD movies on BoxOffice cost more than the SD movies?</a><br />
                    <a href="#BO_About6">Is there a limit to how many times I can view a movie which I've
                        rented?</a><br />
                    <a href="#BO_About7">Do the settings for hard disk preservation (ie. sleep time or automatic
                        standby) have any impact on the recording of DStv BoxOffice content?</a><br />
                    <a href="#BO_About8">Can I watch DStv BoxOffice movies when there is no signal?</a><br />
                    <a href="#BO_About9">What is the Rental History?</a><br />
                    <a href="#BO_About10">Does DStv BoxOffice work on a SD PVR or HD PVR 2P with a single
                        LNB installation?</a><br />
                    <a href="#BO_About11">Can I cancel an order once I've sent the SMS to rent a BoxOffice
                        movie?</a><br />
                    <a href="#BO_About12">What is the BoxOffice refund policy?</a><br />
                    <a href="#BO_About13">I didn't get a chance to watch X on BoxOffice before it expired.
                        When will it be available for viewing again?</a><br />
                    <a href="#BO_About14">Why does the PVR generate a Rental Code even if I input the incorrect
                        BoxOffice PIN?</a><br />
                    <a href="#BO_About15">I was watching something on DStv BoxOffice, when the PVR "jumped"
                        back to live TV. What happened?</a><br />
                    <a href="#BO_About16">I'm ordering a BoxOffice movie. Do I have to include the spaces
                        in the Rental Code when I send the SMS?</a><br />
                    <a href="#BO_About17">Why is the BoxOffice Rental Code so long?</a><br />
                    <a href="#BO_About18">There is an error with one of the BoxOffice movies (break up in
                        video / audio problems, etc). Please can you give me a refund?</a><br />
                </div>
            </div>
            <div class="greyHdrLine6">
            </div>
            <div class="padding20px">
            </div>
            <div class="faq_Answers">
                <div id="BO_About1">
                    <b>What is DStv BoxOffice?</b>
                    <div>
                        DStv BoxOffice is our video on demand service and allows you to rent and view the
                        latest blockbusters (not yet broadcast on DStv) from the comfort of your home.<br />
                        <br />
                        DStv BoxOffice is available on the PVR to all SA staff (MultiChoice, M-Net, SuperSport,
                        Oracle and DStv Online) with a valid PVR subscription. Terms & Conditions apply
                        and pre-registration is required.<br />
                        <br />
                        Please visit <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a> for
                        more information and to sign-up for DStv BoxOffice.<br />
                        <br />
                        <sup>#</sup><b>DStv BoxOffice</b> is not available to commercial subscribers.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About2">
                    <b>How does DStv BoxOffice work on the PVR?</b>
                    <div>
                        DStv BoxOffice<sup>+</sup> offers up to 15 blockbusters on your PVR at any given
                        time. You are able to rent any movie for a period of 48 hours and watch it at a
                        time convenient to you.<br />
                        <br />
                        As a special offer to all staff, HD movies will cost
                        <%= ConfigurationManager.AppSettings["DecoderHDBoxOfficePrice"]%>
                        and SD movies
                        <%= ConfigurationManager.AppSettings["DecoderSDBoxOfficePrice"]%>* until the launch
                        to customers.<br />
                        <br />
                        Renting a movie is as quick & simple as making the popcorn (and is done via SMS).<br />
                        <br />
                        To rent a movie you must already be registered (visit <a href="http://ondemand.dstvo.com">
                            http://ondemand.dstvo.com</a> to sign-up) and must have sufficient funds in
                        your DStv Wallet. (Payment for DStv BoxOffice is made via an online DStv Wallet).<br />
                        <br />
                        All staff who register will automatically get R30 added into their DStv Wallet.<br />
                        <br />
                        Also, all staff who top up their DStv Wallet with R10 or more, will stand in line
                        to win an iPad. The winner will be announced at a later stge.<br />
                        <br />
                        <u>To rent a BoxOffice movie on the SD PVR</u> : press GREEN to access the PVR menu,
                        select DStv On Demand, select the movie of your choice, press OK to rent the movie,
                        enter your BoxOffice PIN and SMS the on-screen code to 37569.<br />
                        <br />
                        <u>To rent a BoxOffice movie on the HD PVR</u> : press GREEN to access DStv BoxOffice,
                        select the movie of your choice, press <b>OK</b> to rent the movie, enter your BoxOffice
                        PIN and SMS the on-screen code to 37569.<br />
                        <br />
                        You'll receive confirmation of your rental via SMS within 5 minutes.<br />
                        <br />
                        An on-screen message will then be displayed when the movie becomes available for
                        viewing (the status of your selected movie changes from "Rent Now" to "View Now"
                        on the BoxOffice screen.)<br />
                        <br />
                        You can watch the movie as many times as you wish during the 48 hours and can rent
                        the same movie multiple times (although you will need to pay for it again each time
                        you rent it).<br />
                        <br />
                        <sup>+</sup>DStv BoxOffice is available on the PVR to all SA staff (MultiChoice,
                        M-Net, SuperSport, Oracle & DStv Online) with a PVR subscription. Terms & Conditions
                        apply. Pre-registration is required.<br />
                        <br />
                        *Prices subject to change without notification.<br />
                        <br />
                        #DStv BoxOffice is not available to commercial subscribers.<br />
                        <br />
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About3">
                    <b>Why is DStv BoxOffice not available outside SA? / When will BoxOffice be available
                        outside SA?</b>
                    <div>
                        DStv is working on rolling this service out to other countries and will announce
                        details as more information becomes available.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About4">
                    <b>How much does a movie cost on DStv BoxOffice?</b>
                    <div>
                        As a special offer to all staff, HD movies will cost
                        <%= ConfigurationManager.AppSettings["DecoderHDBoxOfficePrice"]%>
                        and SD movies
                        <%= ConfigurationManager.AppSettings["DecoderSDBoxOfficePrice"]%>* until the launch
                        to customers.<br />
                        <br />
                        The rental period for all movies is the same (ie. 48 hours).<br />
                        <br />
                        *Prices are subject to change without notification.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About5">
                    <b>Why do the HD movies on BoxOffice cost more than the SD movies?</b>
                    <div>
                        The HD movies offer superior quality and we pay a higher price for them.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About6">
                    <b>Is there a limit to how many times I can view a movie which I've rented?</b>
                    <div>
                        There are no limits to how many times you can view a BoxOffice movie which has been
                        rented, however, the content will automatically become unavailable for viewing at
                        the end of the rental period.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About7">
                    <b>Do the settings for hard disk preservation (ie. sleep time or automatic standby)
                        have any impact on the recording of DStv BoxOffice content?</b>
                    <div>
                        No, your hard disk preservation settings have no impact on DStv BoxOffice.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About8">
                    <b>Can I watch DStv BoxOffice movies when there is no signal?</b>
                    <div>
                        You are able to view BoxOffice movies when there is no signal, but only if you have
                        already rented them (ie. you will not be able to successfully rent a movie if there
                        is no signal).
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About9">
                    <b>What is the Rental History?</b>
                    <div>
                        The Rental History (which is accessed by pressing the <b>BLUE</b> button on the
                        PVR BoxOffice screen) lists ALL movies for which you have generated a Rental Code.
                        Even if the rental has been unsuccessful, the Rental Code which was generated will
                        be listed. The information on this screen is vital should you get billed for a movie
                        that was not made available for viewing on your PVR, or if you rent a movie but
                        do not receive it.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About10">
                    <b>Does DStv BoxOffice work on a SD PVR or HD PVR 2P with a single LNB installation?</b>
                    <div>
                        We do not guarantee the performance of DStv BoxOffice on installations that do not
                        meet the minimum requirements.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About11">
                    <b>Can I cancel an order once I've sent the SMS to rent a BoxOffice movie?</b>
                    <div>
                        There is no way to cancel the order once the Rental Code has been sent to us.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About12">
                    <b>What is the BoxOffice refund policy?</b>
                    <div>
                        If you successfully rent a BoxOffice movie (money is removed from your DStv Wallet)
                        and the movie does not become available for viewing, please call the MultiChoice
                        Contact Centre. You will need your smartcard number and will need to have access
                        to the Rental History screen when you call us. The full terms & conditions for BoxOffice
                        are available on <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About13">
                    <b>I didn't get a chance to watch X on BoxOffice before it expired. When will it be
                        available for viewing again?</b>
                    <div>
                        The movie will not be available again on BoxOffice on the PVR. It may however be
                        available on M-Net at a future date, we suggest you check the TV Guide.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About14">
                    <b>Why does the PVR generate a Rental Code even if I input the incorrect BoxOffice PIN?</b>
                    <div>
                        The PVR does not know what your BoxOffice PIN is. The PIN used to rent BoxOffice
                        movies is verified by our systems when processing your rental request. If the PIN
                        used to generate the Rental Code is incorrect, you will be advised of this by return
                        SMS.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About15">
                    <b>I was watching something on DStv BoxOffice, when the PVR "jumped" back to live TV.
                        What happened?</b>
                    <div>
                        The following are possibilities :<br />
                        <ul>
                            <li>You accidentally pressed the TV button on the remote control</li>
                            <li>A reminder was activated</li>
                            <li>The rental period for the movie expired.</li>
                        </ul>
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About16">
                    <b>I'm ordering a BoxOffice movie. Do I have to include the spaces in the Rental Code
                        when I send the SMS?</b>
                    <div>
                        The spaces have been inserted on-screen to make the number easier to read. The BoxOffice
                        system will accept the number regardless of whether you insert spaces or not.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About17">
                    <b>Why is the BoxOffice Rental Code so long?</b>
                    <div>
                        The Rental Code includes information about your decoder and the details of the movie
                        that you're renting. The only way to make it shorter is to use an alphanumeric system
                        (combination of letters and numbers), which is more difficult for users to enter
                        correctly. Research in this field indicates that numbers are easier for customers
                        to use, even though it results in a longer Rental Code.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_About18">
                    <b>There is an error with one of the BoxOffice movies (break up in video / audio problems,
                        etc). Please can you give me a refund?</b>
                    <div>
                        Our apologies for the technical error. Please report this to the MultiChoice Contact
                        Centre (including the nature of the problem and the title on which the problem was
                        experienced).
                    </div>
                </div>
            </div>
        </div>
        <div id="divBoxOffice_Content_FAQ">
            <div class="block700top">
                <h4 class="home">
                    <b>All about DStv BoxOffice Content</b></h4>
            </div>
            <div class="faq_QuesDiv">
                <div class="faq_Questions">
                    <a href="#BO_Content1">What type of movies are available on DStv BoxOffice?</a><br />
                    <a href="#BO_Content2">How old are the movies on DStv BoxOffice?</a><br />
                    <a href="#BO_Content3">Will DStv BoxOffice movies become available on M-Net at a later
                        date?</a><br />
                    <a href="#BO_Content4">Where can I find out which movies are going to be available on
                        DStv BoxOffice?</a><br />
                    <a href="#BO_Content5">How many movies are available on the PVR for DStv BoxOffice?</a><br />
                    <a href="#BO_Content6">Are all BoxOffice movies on the HD PVR offered in HD?</a><br />
                    <a href="#BO_Content7">Do all BoxOffice movies come with Dolby Digital audio?</a><br />
                    <a href="#BO_Content8">Do DStv BoxOffice movies have HI subtitles?</a><br />
                    <a href="#BO_Content9">How does PG work on the PVR DStv BoxOffice movies?</a><br />
                </div>
            </div>
            <div class="greyHdrLine6">
            </div>
            <div class="padding20px">
            </div>
            <div class="faq_Answers">
                <div id="BO_Content">
                    <b>What type of movies are available on DStv BoxOffice?</b>
                    <div>
                        Agreements have been reached with all major Hollywood studios, as well as most independent
                        studios. The movies are the latest Hollywood blockbusters from a range of different
                        genres, available at the same time or shortly after they are released on DVD.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content2">
                    <b>How old are the movies on DStv BoxOffice?</b>
                    <div>
                        Movies are available very shortly after their release on DVD in South Africa. In
                        some cases this will be on the same day as DVD, and in others it will be up to 60
                        days later.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content3">
                    <b>Will DStv BoxOffice movies become available on M-Net at a later date?</b>
                    <div>
                        Yes, most of the movies available on DStv BoxOffice will become available on M-Net,
                        although only several months later.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content4">
                    <b>Where can I find out which movies are going to be available on DStv BoxOffice?</b>
                    <div>
                        Please visit <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content5">
                    <b>How many movies are available on the PVR for DStv BoxOffice?</b>
                    <div>
                        There are up to 15 movies available on the PVR, with titles refreshed on a weekly
                        basis. Up to 40 titles will be available online at any given time.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content6">
                    <b>Are all BoxOffice movies on the HD PVR offered in HD?</b>
                    <div>
                        Yes, the BoxOffice service on the HD PVR only carries HD content.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content7">
                    <b>Do all BoxOffice movies come with Dolby Digital audio?</b>
                    <div>
                        All BoxOffice titles on the HD PVR have Dolby Digital audio. Please note that this
                        does not necessarily apply to the trailers.<br />
                        <br />
                        All BoxOffice titles on the SD PVR have stereo audio.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content8">
                    <b>Do DStv BoxOffice movies have HI subtitles?</b>
                    <div>
                        DStv BoxOffice movies are not offered with HI subtitles.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Content9">
                    <b>How does PG work on the PVR DStv BoxOffice movies?</b>
                    <div>
                        PG works as it does on all other content. If the rating of the movie results in
                        the content being blocked, a valid PG PIN must be entered to view the content.
                    </div>
                </div>
            </div>
        </div>
        <div id="divBoxOffice_Troubleshooting_FAQ">
            <div class="block700top">
                <h4 class="home">
                    <b>DStv BoxOffice - Troubleshooting</b>
                </h4>
            </div>
            <div class="faq_QuesDiv">
                <div class="faq_Questions">
                    <a href="#BO_Trouble1">The PVR has no BoxOffice titles (E01)</a><br />
                    <a href="#BO_Trouble2">The PVR has no BoxOffice titles (E02)</a><br />
                    <a href="#BO_Trouble3">The PVR has no BoxOffice titles (E03)</a><br />
                    <a href="#BO_Trouble4">The PVR has no BoxOffice titles (E04)</a><br />
                    <a href="#BO_Trouble5">I sent a BoxOffice rental request and the movie has not become
                        available for viewing.</a><br />
                    <a href="#BO_Trouble6">I sent a BoxOffice rental request and received this reply "Sorry,
                        your request was unsuccessful. The Rental Code sent has timed out. Reselect the
                        title & resend new Rental Code." Why did this happen?</a><br />
                    <a href="#BO_Trouble7">When trying to rent a BoxOffice title, I get an SMS which states
                        "the title requested has already been activated".</a><br />
                    <a href="#BO_Trouble8">I registered for DStv BoxOffice but when renting a movie, get
                        an SMS which states " your mobile number is not registered".</a><br />
                    <a href="#BO_Trouble9">When trying to rent a BoxOffice title, I get an SMS which states
                        "PIN or Rental Code are incorrect."</a><br />
                    <a href="#BO_Trouble10">I've been billed for a BoxOffice movie which never became available
                        for me to view on the PVR. Please can I have a refund?</a><br />
                    <a href="#BO_Trouble11">I deposited money into my DStv Wallet, but it is not reflecting
                        the deposit / When trying to rent BoxOffice movies I get an SMS stating "You have
                        insufficient BoxOffice funds".</a><br />
                    <a href="#BO_Trouble12">When I view DStv BoxOffice movies I get a distorted picture
                        on my TV set.</a><br />
                    <a href="#BO_Trouble13">I don't have 15 movies on BoxOffice / the movies on my friend's
                        PVR don't match mine / I have fewer BoxOffice movies than on my friend's PVR.</a><br />
                </div>
            </div>
            <div class="greyHdrLine6">
            </div>
            <div class="padding20px">
            </div>
            <div class="faq_Answers">
                <div id="BO_Trouble1">
                    <b>The PVR has no BoxOffice titles (E01)</b>
                    <div>
                        <table cellpadding="5" cellspacing="0">
                            <tr>
                                <td width="97" valign="top" bgcolor="#CCCCCC">
                                    Error Message
                                </td>
                                <td width="553" valign="top" bgcolor="#CCCCCC">
                                    Potential Cause
                                </td>
                                <td width="763" valign="top" bgcolor="#CCCCCC">
                                    Solution
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" valign="top">
                                    E01
                                </td>
                                <td valign="top">
                                    PVR has not had sufficient time to download titles.
                                </td>
                                <td valign="top">
                                    The PVR requires time to download the movies. If the PVR has recently been off /
                                    there have been signal problems (ie. bad weather) / the PVR was only recently installed
                                    / the PVR only received BoxOffice software in the past 2 days - please check again
                                    in 24 hours and let us know if the problem persists.
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    The quality of the installation is insufficient to allow for BoxOffice downloads.<br />
                                    <br />
                                    Note :<br />
                                    <ul>
                                        <li>If you are a DStv Premium subscriber, check to see if there are CatchUp titles available.
                                            If the PVR has most of the CatchUp titles, it is unlikely the E01 is caused by an
                                            installation problem.</li>
                                        <li>Where the cause is poor installation, you may also note problems with the EPG data
                                            (ie. TV Guide)</li>
                                        <li>The quality of installation required to download BoxOffice is HIGHER than that required
                                            to view normal channels, ie. you may be able to view channels without a problem,
                                            but the PVR is unable to download BoxOffice titles.</li>
                                    </ul>
                                </td>
                                <td valign="top">
                                    We suggest you check your installation. On the 4 tuner HD PVR, you can try swapping
                                    the cable connected to tuner 4 with the cable connected to tuner 1 (this is not
                                    valid for SatCR installations). If the problem persists, ie. the PVR still has no
                                    BoxOffice titles in 24 hours, we suggest you get the installation checked.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble2">
                    <b>The PVR has no BoxOffice titles (E02)</b>
                    <div>
                        Your PVR is missing information that is required to display the available BoxOffice
                        titles. This information is broadcast multiple times per day, so please ensure your
                        PVR is switched on at all times. This error should resolve itself automatically.
                        If your PVR continues to get this error, it may indicate a problem with the installation.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble3">
                    <b>The PVR has no BoxOffice titles (E03)</b>
                    <div>
                        This error message indicates that no BoxOffice titles are available in your country.<br />
                        <br />
                        If you live in SA and are a SA subscriber, your PVR is missing information that
                        is required to display the available BoxOffice titles. This information is broadcast
                        multiple times per day, so please ensure you PVR is switched on at all times. This
                        error should resolve itself automatically. If your PVR continues to get this error,
                        it may indicate a problem with the installation.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble4">
                    <b>The PVR has no BoxOffice titles (E04)</b>
                    <div>
                        This message indicates that movies have been downloaded to your PVR, however these
                        movies are not yet available to rent, ie. at the moment, there really are no movies
                        available to rent. Please check the BoxOffice schedule on <a href="http://ondemand.dstvo.com">
                            http://ondemand.dstvo.com</a> to see when the next movies will become available
                        for rental.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble5">
                    <b>I sent a BoxOffice rental request and the movie has not become available for viewing.</b>
                    <div>
                        If more than 5 minutes have passed, please try again. If still unsuccessful, please
                        let us know by calling the MultiChoice Contact Centre (please have your smartcard
                        number handy). As a precautionary measure, the Rental Code expires within 5 minutes
                        of being created (this is to ensure that any delay in delivering the SMS to us will
                        not result in your DStv Wallet being charged for a movie that you may no longer
                        want to watch).
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble6">
                    <b>I sent a BoxOffice rental request and received this reply "Sorry, your request was
                        unsuccessful. The Rental Code sent has timed out. Reselect the title & resend new
                        Rental Code." Why did this happen?</b>
                    <div>
                        As a precautionary measure, the Rental Code expires within 5 minutes of being created
                        (this is to ensure that any delay in delivering the SMS to us will not result in
                        your DStv Wallet being charged for a movie that you may no longer want to watch).
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble7">
                    <b>Register and Create an Account</b>
                    <div>
                        First check the status of the movie you're trying to rent. (Press <b>RED</b>, press
                        <b>RIGHT</b> arrow once). If the status of the movie you're trying to rent is still
                        "Rent now", please call the MultiChoice Contact Centre. Please have your smartcard
                        number handy.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble8">
                    <b>I registered for DStv BoxOffice but when renting a movie, get an SMS which states
                        " your mobile number is not registered".</b>
                    <div>
                        If you have registered the mobile number in question for BoxOffice, please check
                        the number to which you sent the rental request. The only number to send rental
                        requests to is 37569. If you sent the rental request to 37569 and received the error
                        message that your mobile number is not registered, please call the MultiChoice Contact
                        Centre (and have your smartcard number handy).
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble9">
                    <b>When trying to rent a BoxOffice title, I get an SMS which states "PIN or Rental Code
                        are incorrect."</b>
                    <div>
                        To ensure an error was not made when typing the SMS, please try again (you can check
                        the Rental Code in your SMS against the Rental Code stored in the Rental History
                        screen to confirm that your SMS was correct. To go to the Rental History screen,
                        press BLUE from the BoxOffice screen). If you get the same SMS message again or
                        you have confirmed via the Rental History screen that the Rental Code you sent is
                        correct, please call the MultiChoice Contact Centre. Please have your smartcard
                        number handy.<br />
                        <br />
                        Note : our BoxOffice systems will accept Rental Codes with our without the spaces.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble10">
                    <b>I've been billed for a BoxOffice movie which never became available for me to view
                        on the PVR. Please can I have a refund?</b>
                    <div>
                        Please call the MultiChoice Contact Centre. You will need your smartcard number
                        and will need to have access to the Rental History screen when you call us.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble11">
                    <b>I deposited money into my DStv Wallet, but it is not reflecting the deposit / When
                        trying to rent BoxOffice movies I get an SMS stating "You have insufficient BoxOffice
                        funds".</b>
                    <div>
                        Your DStv Wallet must have sufficient funds to pay for the rental. You can check
                        the available funds in your DStv Wallet on <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>.
                        Any amount paid into your DStv Wallet could take up to 15 minutes to reflect. If
                        you deposited money more than 15 minutes ago, please call the MultiChoice Contact
                        Centre and have your smartcard number available.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble12">
                    <b>When I view DStv BoxOffice movies I get a distorted picture on my TV set.</b>
                    <div>
                        DStv BoxOffice movies are copy protected in such a way that any content received
                        by VCRs/DVD recorders becomes distorted so as not to allow the VCR/DVD to tape this
                        content. It is likely you are running the output of your PVR through your VCR/DVD
                        and then onto your TV, which is causing the distortion.<br />
                        <br />
                        Please connect your TV set directly to your PVR without going through your VCR/DVD
                        first. If you use your VCR/DVD to receive free to air channels via a terrestrial
                        aerial and you pass this signal through to the PVR before it arrives at the TV set,
                        we suggest you also use RCA cables to connect your PVR to your TV set. In this way
                        you can toggle your TV set between the RF input from your VCR/DVD and RCA's from
                        the PVR depending on whether you are watching terrestrial free to air services or
                        content delivered via your PVR.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Trouble13">
                    <b>I don't have 15 movies on BoxOffice / the movies on my friend's PVR don't match mine
                        / I have fewer BoxOffice movies than on my friend's PVR.</b>
                    <div>
                        <b>If you are using the SD PVR or HD PVR 2P : </b>The movies are downloaded to all
                        SD PVRs at the same time. However, the SD PVR is only able to accept these downloads
                        if no user recordings are taking place. Due to you and your friend most likely having
                        different recording patterns, the movies available on BoxOffice may vary for a short
                        time, but ultimately should match up within a few days / week. Issues such as weather
                        may also influence the difference between what is available on different decoders.<br />
                        <br />
                        <b>If you are using a 4 tuner HD PVR : </b>Issues such as weather or a poorer quality
                        installation may influence the difference between what is available on different
                        decoders, however ultimately should match up within a few days.<br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <div id="divBoxOffice_Registration_FAQ">
            <div class="block700top">
                <h4 class="home">
                    <b>All about DStv BoxOffice Registration and the DStv Wallet</b>
                </h4>
            </div>
            <div class="faq_QuesDiv">
                <div class="faq_Questions">
                    <a href="#BO_Registration1">How do I sign-up for DStv BoxOffice?</a><br />
                    <a href="#BO_Registration2">How do I top-up my DStv Wallet?</a><br />
                    <a href="#BO_Registration3">I am unable to access http://ondemand.dstvo.com Help! I
                        need to register for DStv BoxOffice / use CatchUp online / use BoxOffice online.</a><br />
                    <a href="#BO_Registration4">I don't have a credit card. How else can I make payment
                        into my DStv Wallet?</a><br />
                    <a href="#BO_Registration5">What credit cards can I use to make a payment into my DStv
                        Wallet?</a><br />
                    <a href="#BO_Registration6">How do I change my DStv BoxOffice PIN? / Please reset my
                        BoxOffice PIN.</a><br />
                    <a href="#BO_Registration7">How do I add / remove / change mobile numbers on my BoxOffice
                        registration?</a><br />
                </div>
            </div>
            <div class="greyHdrLine6">
            </div>
            <div class="padding20px">
            </div>
            <div class="faq_Answers">
                <div id="BO_Registration1">
                    <b>How do I sign-up for DStv BoxOffice?</b>
                    <div>
                        You must be registered to use DStv BoxOffice. Please go to <a href="http://ondemand.dstvo.com">
                            http://ondemand.dstvo.com</a> to sign-up for BoxOffice. (You will first need
                        to create a DStv Connect profile).<br />
                        <br />
                        To register, you need either your DStv account number or smartcard number, a 4 digit
                        PIN of your choice as well as the mobile numbers you wish to register for use with
                        BoxOffice. You are able to link 4 mobile numbers to any ONE DStv account.<br />
                        <br />
                        BONUS! We will deposit R30 into the DStv Wallet of any staff member who signs-up
                        for DStv BoxOffice, ensuring that your first BoxOffice rental is on us – hassle
                        free. (T&Cs apply).<br />
                        <br />
                        Note : If you have multiple PVRs on your DStv account, the mobile numbers you register
                        for use with BoxOffice will be linked to all PVRs on your account and the same PIN
                        number must be used when renting movies from any of the decoders.<br />
                        <br />
                        To use BoxOffice on an ongoing basis, you need to deposit funds into your DStv Wallet.
                        There are a number of options to deposit money into your DStv Wallet.<br />
                        <ul>
                            <li>Online (<a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>) – currently
                                only credit cards are accepted</li>
                            <li>At DStv Branches and Agends – both cash and credit cards are accepted</li>
                        </ul>
                        BONUS! if you deposit R10 (or more) in one transaction into your DStv Wallet, you
                        stand in line to win an iPad.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Registration2">
                    <b>How do I top-up my DStv Wallet?</b>
                    <div>
                        You can top-up your DStv Wallet at anytime. You can do this :
                        <ul>
                            <li>Online (<a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>) – currently
                                only credit cards are accepted.</li>
                            <li>At DStv Branches and Agends – both cash and credit cards are accepted.</li>
                        </ul>
                        <br />
                        BONUS! if you deposit R10 (or more) in one transaction into your DStv Wallet, you
                        stand in line to win an iPad.<br />
                        <br />
                        Additional payment options will be added in the future and we will communicate all
                        changes to our BoxOffice customers.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Registration3">
                    <b>I am unable to access <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>
                        Help! I need to register for DStv BoxOffice / use CatchUp online / use BoxOffice
                        online.</b>
                    <div>
                        To confirm that the problem is not localized, first check that you can load other
                        internet sites. If you can load other sites (example : <a href="http://www.news24.com">
                            www.news24.com</a> and <a href="http://www.supersport.com">www.supersport.com</a>,
                        please report this to the MultiChoice Contact Centre.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Registration4">
                    <b>I don't have a credit card. How else can I make payment into my DStv Wallet?</b>
                    <div>
                        If you do not have a credit card, you can make payment into your DStv Wallet :
                        <ul>
                            <li>At DStv Branches and Agents – cash is accepted</li>
                        </ul>
                        <br />
                        BONUS! if you deposit R10 (or more) in one transaction into your DStv Wallet, you
                        stand in line to win an iPad.<br />
                        <br />
                        Additional payment options will be added in the future and we will communicate all
                        changes to our BoxOffice customers.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Registration5">
                    <b>What credit cards can I use to make a payment into my DStv Wallet?</b>
                    <div>
                        You can use either a Visa and / or Mastercard to make payments into your DStv Wallet.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Registration6">
                    <b>How do I change my DStv BoxOffice PIN? / Please reset my BoxOffice PIN.</b>
                    <div>
                        There is no default BoxOffice PIN, so we are unable to reset it. You can change
                        your BoxOffice PIN anytime by visiting <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>.
                        Alternatively you can contact your nearest MultiChoice Contact Centre (please have
                        your smartcard number or account number handy). Don't forget – the BoxOffice PIN
                        is the same for all decoders on your DStv Account.
                    </div>
                </div>
                <div class="padding10px">
                </div>
                <div id="BO_Registration7">
                    <b>How do I add / remove / change mobile numbers on my BoxOffice registration?</b>
                    <div>
                        You can change the mobile numbers registered for use on BoxOffice at anytime by
                        visiting <a href="http://ondemand.dstvo.com">http://ondemand.dstvo.com</a>. Alternatively
                        you can contact your nearest MultiChoice Contact Centre (please have your smartcard
                        number or account number handy). Don't forget – you can only register FOUR mobile
                        numbers per DStv account (ie. the number of mobile numbers you can register is not
                        per decoder, but per account).
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divCatchUp_About_FAQ">
        <div class="block700top">
            <h4 class="home">
                <b>All about DStv CatchUp</b>
            </h4>
        </div>
        <div class="faq_QuesDiv">
            <div class="faq_Questions">
                <a href="#CU_About1">What is DStv CatchUp?</a><br />
                <a href="#CU_About2">How does DStv CatchUp work on the PVR?</a><br />
                <a href="#CU_About3">What content is offered on DStv CatchUp?</a><br />
                <a href="#CU_About4">What does it cost me to use DStv CatchUp?</a><br />
                <a href="#CU_About5">Is the DStv CatchUp content in HD on the HD PVRs?</a><br />
                <a href="#CU_About6">Is there a limit to how many times I can view DStv CatchUp content?</a><br />
                <a href="#CU_About7">Please can you include programme X on DStv CatchUp.</a><br />
                <a href="#CU_About8">Explain the CatchUp screen to me</a><br />
                <a href="#CU_About9">Can I watch DStv CatchUp when there is no signal?</a><br />
                <a href="#CU_About10">Where can I find out the schedule of programmes available on DStv
                    CatchUp?</a><br />
                <a href="#CU_About11">Why is DStv CatchUp only available to DStv Premium subscribers?</a><br />
                <a href="#CU_About12">How does PG work on CatchUp?</a><br />
                <a href="#CU_About13">Does DStv CatchUp work on a SD PVR or HD PVR 2P with a single
                    LNB installation?</a><br />
                <a href="#CU_About14">When I select to watch a DStv CatchUp item, the video from the
                    channel the PVR is currently tuned to is displayed, ie. I cannot watch the DStv
                    CatchUp content.</a><br />
                <a href="#CU_About15">I didn't get a chance to watch X on CatchUp before it expired.
                    When will it be available for viewing again?</a><br />
                <a href="#CU_About16">The CatchUp screen on the PVR has no items / titles - it displays
                    a message "no titles found".</a><br />
                <a href="#CU_About17">I was watching something on DStv CatchUp, when suddenly the system
                    stopped and went back to live TV. What happened?</a><br />
                <a href="#CU_About18">I'm watching something from DStv CatchUp, and the sound quality
                    is poor / there is video breakup / (any quality issue).</a><br />
                <a href="#CU_About19">Programme X which is advertised (web), is not listed. Why don't
                    I have it and how do I get it?</a><br />
                <a href="#CU_About20">Do the settings for hard disk preservation (ie. sleep time or
                    automatic standby) have any impact on the recording of DStv CatchUp content?</a><br />
                <a href="#CU_About21">When I view DStv CatchUp content I get a distorted picture on
                    my TV set.</a><br />
            </div>
        </div>
        <div class="greyHdrLine6">
        </div>
        <div class="padding20px">
        </div>
        <div class="faq_Answers">
            <div id="CU_About1">
                <b>What is DStv CatchUp?</b>
                <div>
                    DStv CatchUp is a value added service available free to all DStv Premium subscribers
                    with a PVR subscription. It allows you the opportunity to "catch up" on a selection
                    of your favourite series & sporting highlights for up to 7* days after their first
                    broadcast on DStv. To access DStv CatchUp, press RED (to display the Playlist) and
                    then press the <b>RIGHT</b> arrow twice.<br />
                    <br />
                    DStv CatchUp is also available online. For more information about DStv CatchUp online,
                    please visit <a href="http://www.dstv.com">www.dstv.com</a><br />
                    <br />
                    *<i>may vary on certain titles, check expiry date for confirmation</i>
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About2">
                <b>How does DStv CatchUp work on the PVR?</b>
                <div>
                    We download a selection of our best series and sporting highlights to your PVR.
                    The download process is automatic, ie. you don't need to do anything. You can then
                    select and view the content available on the DStv CatchUp screen.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About3">
                <b>What content is offered on DStv CatchUp?</b>
                <div>
                    CatchUp offers a variety of general entertainment, sport and actuality programming.
                    The programmes are available within hours after first being broadcast on DStv and
                    are normally available for viewing for a full 7 days thereafter (may vary on certain
                    titles, check expiry date for confirmation). The content on CatchUp does not have
                    advert breaks, allowing you a quick & convenient way to catch up on your favourite
                    series.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About4">
                <b>What does it cost me to use DStv CatchUp?</b>
                <div>
                    DStv CatchUp is currently included in the cost of your DStv Premium subscription.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About5">
                <b>Is the DStv CatchUp content in HD on the HD PVRs?</b>
                <div>
                    The content is in HD where available and is clearly indicated by the HD icon in
                    the programme information.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About6">
                <b>Is there a limit to how many times I can view DStv CatchUp content?</b>
                <div>
                    There are no limits to how many times you can view DStv CatchUp content, however,
                    keep in mind the content is automatically deleted at the advertised expiry time.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About7">
                <b>Please can you include programme X on DStv CatchUp.</b>
                <div>
                    Many thanks for your suggestion, it will be forwarded to the relevant team. Please
                    note - the channels have strict licensing agreements with the content providers
                    which determines the content which can be shown and when it can be shown.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About8">
                <b>Explain the CatchUp screen to me</b>
                <div>
                    <b>SD PVR</b><br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top">
                                <asp:Image ID="imgFaq1" runat="server" AlternateText="DStv Connect" ImageUrl="~/App_Themes/onDemand/images/faqImg1.jpg" />
                            </td>
                            <td valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td width="6%" valign="top">
                                            <p>
                                                1</p>
                                        </td>
                                        <td width="94%" valign="top">
                                            <p>
                                                Programme title. If the title is BLUE it indicates this programme has been partially
                                                or fully viewed.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                2</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Duration of programme</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                3</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                PG rating of programme</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                4</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                PG lock key</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                5</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Expiry date / time. This is the date and time at which the programme will automatically
                                                be deleted from the PVR. The expiry time is displayed in the last 24 hours.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                RED</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Deletes programme temporarily and schedules a new recording of the same programme.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                GREEN</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Displays 2 lines of information</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                i</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Displays extended information screen</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                OK</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Starts playback</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <b>HD PVR</b><br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top">
                                <asp:Image ID="Image1" runat="server" AlternateText="DStv Connect" ImageUrl="~/App_Themes/onDemand/images/faqImg2.jpg" />
                            </td>
                            <td valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td width="6%" valign="top">
                                            <p>
                                                1</p>
                                        </td>
                                        <td width="94%" valign="top">
                                            <p>
                                                Programme title. If the title is BLUE it indicates this programme has been partially
                                                or fully viewed.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                2</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Duration of programme</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                3</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                PG rating of programme</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                4</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                PG lock key</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                5</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Expiry date / time. This is the date and time at which the programme will be automatically
                                                be deleted from the PVR. The expiry time is displayed in the last 24 hours.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                WHITE</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Sort</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                i</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Displays extended information screen</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <p>
                                                OK</p>
                                        </td>
                                        <td valign="top">
                                            <p>
                                                Starts playback</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About9">
                <b>Can I watch DStv CatchUp when there is no signal?</b>
                <div>
                    Yes you can watch DStv CatchUp content when there is no signal.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About10">
                <b>Where can I find out the schedule of programmes available on DStv CatchUp?</b>
                <div>
                    The monthly DStv CatchUp schedule is available on <a href="http://www.dstv.com">www.dstv.com</a>.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About11">
                <b>Why is DStv CatchUp only available to DStv Premium subscribers?</b>
                <div>
                    Current agreements with the studios stipulate that DStv CatchUp can only be made
                    available to full DStv Premium subscribers.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About12">
                <b>How does PG work on CatchUp?</b>
                <div>
                    PG works as it does on all other content. If the rating of the programme results
                    in the content being blocked, a valid PG PIN must be entered to view the content.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About13">
                <b>Does DStv CatchUp work on a SD PVR or HD PVR 2P with a single LNB installation?</b>
                <div>
                    We do not guarantee the performance of DStv CatchUp on installations that do not
                    meet the minimal requirement.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About14">
                <b>When I select to watch a DStv CatchUp item, the video from the channel the PVR is
                    currently tuned to is displayed, ie. I cannot watch the DStv CatchUp content.</b>
                <div>
                    Please call the MultiChoice Contact Centre and have your smartcard number handy.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About15">
                <b>I didn't get a chance to watch X on CatchUp before it expired. When will it be available
                    for viewing again?</b>
                <div>
                    Once a programme has expired, it will not be made available on DStv CatchUp again.
                    The programme may however be repeated on one of the M-Net channels. We suggest you
                    check the TV Guide.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About16">
                <b>The CatchUp screen on the PVR has no items / titles - it displays a message "no titles
                    found".</b>
                <div>
                    <b>For the SD PVR and HD PVR 2P : </b>
                    <br />
                    DStv CatchUp content is recorded to your PVR during times when you have no scheduled
                    recordings. It is possible the content has not yet had an opportunity to record.
                    If you have no scheduled recordings over the next day and the same error message
                    is displayed, there may be a system error or problem with your installation. Please
                    check again in 24 hours and let us know if the problem has not been resolved.
                    <br />
                    <br />
                    <b>For the SD PVR and HD PVR 2P : </b>
                    <br />
                    It is possible the content has not yet had an opportunity to record. Please check
                    again in 24 hours and let us know if the problem has not been resolved.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About17">
                <b>I was watching something on DStv CatchUp, when suddenly the system stopped and went
                    back to live TV. What happened?</b>
                <div>
                    The following are possibilities :<br />
                    <ul>
                        <li>You accidentally pressed the TV button on the remote control</li>
                        <li>A reminder was activated</li>
                        <li>The programme you were watching expired</li>
                    </ul>
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About18">
                <b>I'm watching something from DStv CatchUp, and the sound quality is poor / there is
                    video breakup / (any quality issue).</b>
                <div>
                    <b>For the SD PVR : </b>
                    <br />
                    Our apologies for the inconvenience. It is possible that during the recording of
                    the content to your PVR, the quality was compromised. To get a new copy of the programme
                    recorded to your PVR, you first need to delete the current copy. Go to the DStv
                    CatchUp screen, highlight that particular programme and press RED to delete it and
                    request a new copy. If you have no scheduled recordings, a new copy should be available
                    for viewing within 48 hours.
                    <br />
                    <br />
                    <b>For the HD PVR : </b>
                    <br />
                    Many thanks for reporting this, we will escalate the issue to our operations team.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About19">
                <b>Programme X which is advertised (web), is not listed. Why don't I have it and how
                    do I get it?</b>
                <div>
                    The programmes on DStv CatchUp can only download to the SD PVR or HD PVR 2P when
                    there are no user recordings taking place. It may be that your PVR was busy when
                    the programme was scheduled to download.<br />
                    <br />
                    Please report this to the MultiChoice Contact Centre.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About20">
                <b>Do the settings for hard disk preservation (ie. sleep time or automatic standby)
                    have any impact on the recording of DStv CatchUp content?</b>
                <div>
                    No, your hard disk preservation settings have no impact on DStv CatchUp.
                </div>
            </div>
            <div class="padding10px">
            </div>
            <div id="CU_About21">
                <b>When I view DStv CatchUp content I get a distorted picture on my TV set.</b>
                <div>
                    DStv CatchUp content is copy protected in such a way that any content received by
                    VCRs/DVD recorders becomes distorted so as not to allow the VCR/DVD to tape this
                    content. It is likely you are running the output of your PVR through your VCR/DVD
                    and then onto your TV, which is causing the distortion.<br />
                    <br />
                    Please connect your TV set directly to your PVR without going through your VCR/DVD
                    first. If you use your VCR/DVD to receive free to air channels via a terrestrial
                    aerial and you pass this signal through to the PVR before it arrives at the TV set,
                    we suggest you also use RCA cables to connect your PVR to your TV set. In this way
                    you can toggle your TV set between the RF input from your VCR/DVD and RCA's from
                    the PVR depending on whether you are watching terrestrial free to air services or
                    content delivered via your PVR.
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    <!--
        var Accordion1 = new Spry.Widget.Accordion("Accordion1", { useFixedPanelHeights: false });
        
    //-->
    </script>
    <script type="text/javascript">
        function ToggleFilterMenuStyle(objectId, parent) {
            var object = document.getElementById(objectId);
            //alert(object.className);
            if (object.className == "down") {
                object.className = "up";
                var menuList = document.getElementById(parent).getElementsByTagName("a");
                for (i = 0; i < menuList.length; i++) {
                    if (menuList[i] != object) {
                        menuList[i].className = "down";
                    }
                }
            }
        }

    </script>
    <script type="text/javascript">
        function ToggleFAQ(objectId) {
            var object = document.getElementById(objectId);

            $('[id$="_FAQ"]').hide();
            $('#' + objectId).show();
        }
       
    </script>
</asp:Content>
