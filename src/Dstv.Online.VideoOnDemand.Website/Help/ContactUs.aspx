﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/MasterPages/Base.master"
    AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Help.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script src="../Scripts/jquery.browser.js" type="text/javascript"></script>
    <script src="../Scripts/systemRequirements.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <script language="JavaScript" type="text/javascript">
	<!--
        var submitted = "";

        // -------------------------------------------  Browser name and version
        var BrowserName = "Other";
        var Browser = "";
        var BrowserVersion = is_minor.toString();
        var Platform = "";


        if (is_ie) BrowserName = "Internet Explorer";
        else if (is_fx) BrowserName = "Firefox";
        else if (is_nav) BrowserName = "Netscape Navigator";
        else if (is_opera) BrowserName = "Opera";
        else if (is_konq) BrowserName = "Konqueror";
        else if (is_safari) BrowserName = "Safari";
        else if (is_fb) BrowserName = "Firebird";
        else if (is_aol) BrowserName = "AOL";
        else if (is_webtv) BrowserName = "WebTV";
        else if (is_TVNavigator) BrowserName = "TV Navigator";
        else if (is_hotjava) BrowserName = "Hot Java";

        Browser = BrowserName + " " + BrowserVersion

        if (is_winxp) Platform = "Windows XP";
        else if (is_win2k) Platform = "Windows 2000";
        else if (is_win98) Platform = "Windows 98";
        else if (is_win95) Platform = "Windows 95";
        else if (is_winnt) { Platform = "Windows NT"; if (OSName != null && OSName != "Win NT") Platform += " (" + OSName + ")"; }
        else if (is_mac) Platform = "Apple Mac";
        else if (is_linux) Platform = "Linux";
        else if (is_win31) Platform = "Windows 3.1";
        else if (is_winme) Platform = "Windows ME";
	//-->
    </script>
    <script language="javascript" type="text/javascript">
    <!--
        function SetChat() {
            if (document.getElementById('sbLink2') != undefined) {
                document.getElementById('iChat').innerHTML = '<img alt="Closed" src="/Images/Chat_Closed.gif" border="0" />';
            }
        }
        function OpenForm(strVal) {
            document.forms[0].Subject.value = strVal;
            AttrSet('td', 'FormHeader', 'innerHTML', strVal, true);
            AttrSet('table', 'FormTable', 'style.display', 'block', true);
        }
        function HideForm() {
            document.forms[0].Subject.value = '';
            AttrSet('td', 'FormHeader', 'innerHTML', '', true);
            AttrSet('table', 'FormTable', 'style.display', 'none', true);
            window.scrollTo(0, 0);
        }
        function SendMail() {
            objForm = document.forms[0];
            var strErr = '';
            if (objForm.Email.value.length == 0) strErr += 'Please enter your email address.\t\n';
            if (objForm.Details.value.length == 0) strErr += 'Please enter the details of your query.\t\n';
            if (strErr.length == 0) {
                objForm.submit();
            }
            else {
                alert(strErr);
            }
        }
    //-->
    </script>
    <script type="text/javascript">
    <!--
        // IF AGENTS ARE AVAILABLE:
        function agents_available() {
            document.getElementById('smartbutton').innerHTML = '<A HREF="" onClick="window.open(\'https://admin.instantservice.com/links/6068/18116\',' +
   '\'custclient\',\'width=500,height=320,scrollbars=0\');return false;">' +
   '<center><IMG SRC="/app_themes/onDemand/images/btn-livechat_avail.gif" border="0"/></center></A>';
            return true;
        }
        // IF AGENTS ARE NOT AVAILABLE:
        function agents_not_available() {
            document.getElementById('smartbutton').innerHTML = '<center><img alt="" align="left" src="/app_themes/onDemand/images/btn-livechat_unavail.gif" border="0"/></center>';
            return true;
        }
    //-->
    </script>
    <div class="global_wrapper">
        <div class="block980_bg">
            <div class="hdrWht24">
                CONTACT US
            </div>
        </div>
        <script language="javascript" type="text/javascript">
	    <!--
            document.write('<input type="hidden" name="optionaldata1" value="' + Platform + '" />');
            document.write('<input type="hidden" name="optionaldata2" value="' + BrowserName + '" />');
	    //-->
        </script>
        <div class="padding20px">
        </div>
        <div class="block620">
            <div class="floatLeft">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    <!--[if IE 7]>
                    <style type="text/css">
                        #ctl00_cphMain_divForm table tr td {height:20px; vertical-align:bottom}
                        .hr_dotline2{padding-bottom:10px}
                        .reqField{margin-bottom:5px;}
                    </style>
                    <![endif]-->
                        <div id="divForm" runat="server" class="floatLeft">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                <td colspan="3">
                                        Got a question, comment or complaint? We’re here to help. Fill in the form below
                                        and we’ll get back to you within the next working day.
                                        <br />
                                        <br />
                                        <div class="errorTxt">
                                            <asp:ValidationSummary ID="ValidationSummary" runat="server" />
                                        </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><div class="formDesc">Fields marked with&nbsp;<span class="reqField">*</span>&nbsp;are required fields</div><br /></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>First name</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="inputBoxes">
                                            <asp:TextBox ID="txtFirstname" runat="server"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="reqField">
                                            *</div>
                                        <asp:RequiredFieldValidator ID="rfvFirstname" runat="server" ControlToValidate="txtFirstname"
                                            ErrorMessage="Please Enter Your First Name" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>Last name</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="inputBoxes">
                                            <asp:TextBox ID="txtLastname" runat="server"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="reqField">
                                            *</div>
                                        <asp:RequiredFieldValidator ID="rfvLastname" runat="server" ControlToValidate="txtLastname"
                                            ErrorMessage="Please Enter Your Last Name" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>Email Address</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="inputBoxes">
                                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="reqField">
                                            *</div>
                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="Please Enter Your Email Address" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="Please Enter Your Email Address" Display="None" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"></asp:RegularExpressionValidator>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="hr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td colspan="3">
                                        <b>Site</b>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td width="47%">
                                        <div class="inputBoxes">
                                            <select id="selSite" runat="server">
                                                <option value="CatchUp" selected="selected">CatchUp</option>
                                                <option value="BoxOffice">BoxOffice</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="51%">
                                        <div class="reqField">
                                            *</div>
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td colspan="3">
                                        <div class="hr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>Support Category</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="47%">
                                        <div class="inputBoxes">
                                            <select id="selSupport" runat="server">
                                                <option value="Technical Problems">I'm experiencing technical problems</option>
                                                <%-- DCCU-100----Commenting As per story ID--%>
                                                <%--<option value="Billing">I'm experiencing billing problems</option>--%>
                                               <%-- DCCU-100----Commenting As per story ID--%>
                                               <%-- <option value="Product Information">I'd like to know more about the product</option>--%>
                                                <option value="Registration">I'm having trouble registering</option>
                                                <option value="Feedback" selected="selected">I'd like to provide feedback</option>
                                               <%-- Adding as per Story Id - DCCU-101--%>
                                                  <option value="SmartCard Problems">I'm having trouble linking my smartcard</option>
                                                 <%-- DCCU-100----Commenting As per story ID--%>
                                                <%--<option value="Other">I'd like other feedback</option>--%>
                                            </select>
                                        </div>
                                    </td>
                                    <td width="51%">
                                        <div class="reqField">
                                            *</div>
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="hr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>Windows Media Player Version</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="inputBoxes">
                                            <select id="selWinMedia" runat="server">
                                                <option selected="selected" value="V.6 Series or earlier">V.6 Series or earlier</option>
                                                <option value="V.7 Series">V.7 Series</option>
                                                <option value="V.9 Series">V.9 Series</option>
                                                <option value="V.10 Series">V.10 Series</option>
                                                <option value="V.11 Series">V.11 Series</option>
                                                <option value="I don't know">I don't know</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="reqField">
                                            *</div>
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="hr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>Connection Type</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="inputBoxes">
                                            <select id="selConnType" runat="server">
                                                <option selected="selected" value="ADSL">ADSL</option>
                                                <option value="ISDN">ISDN</option>
                                                <option value="E1 (2Mbps)">E1 (2Mbps)</option>
                                                <option value="3G / EDGE / GPRS">3G / EDGE / GPRS</option>
                                                <option value="HSDPA">HSDPA</option>
                                                <option value="Dial-up">Dial-up</option>
                                                <option value="Not sure">Not sure</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="reqField">
                                            *</div>
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="hr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <b>Additional Details</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="inputBoxes">
                                            <textarea name="textarea" id="AddComments" runat="server" cols="45" rows="5"></textarea>
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="right" valign="top">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="hr_dotline2">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:ImageButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" ImageUrl="~/app_themes/onDemand/images/btn_submit.gif"
                                            Width="87" Height="37" alt="Submit" />
                                        <br /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="formDesc">
                                            Fields marked with&nbsp;<span class="reqField">*</span>&nbsp;are required fields</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divThankYou" runat="server" class="connectformItems" visible="false">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td>
                                        <div class="connectformThnx">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblStatus" runat="server">Your message has been sent. Thank you for your query.<%--Thank you, your message has been sent.--%></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="padding20px">
            </div>
        </div>
        <div class="block300_right">
            <h5>
                Live Chat Support</h5>
            <div class="greyHdrLine">
            </div>
            <div class="floatLeft">
                Chat support available weekdays from 11AM To 7PM (GMT +2)<br />
                <br />
            </div>
            <script type="text/javascript">
                document.write('<img src="http://rs.instantservice.com/resources/smartbutton/6068/18116/available.gif?' +
                    Math.floor(Math.random() * 10001) +
                    '" style="width:0;height:0;visibility:hidden;position:absolute;border-width:0px"' +
                    ' onLoad="agents_available()" onError="agents_not_available()">');
            </script>
            <div class="block300_grey">
            <div id="smartbutton">
            </div>
        </div>
        </div>
        <div class="padding20px">
        </div>
    </div>
</asp:Content>
