﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using Dstv.Online.VideoOnDemand.URLRewriter;
using RewriterConfig = Dstv.Online.VideoOnDemand.URLRewriter.Config;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    public sealed partial class SiteUtils
    {

        #region public static string WebAppRootPath
        /// <summary>
        /// 
        /// </summary>
        public static string WebAppRootPath
        {
            get
            {
                string webAppPath = HttpContext.Current.Request.ApplicationPath;
                if (!webAppPath.EndsWith("/"))
                {
                    webAppPath += "/";
                }
                return webAppPath;
            }
        }
        #endregion


        #region public static string GetAbsoluteUrlPath ( string urlPath )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlPath"></param>
        /// <returns></returns>
        public static string GetAbsoluteUrlPath(string urlPath)
        {
            if (urlPath.StartsWith("/") || urlPath.StartsWith("http://", StringComparison.OrdinalIgnoreCase) || urlPath.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return urlPath;
            }
            else if (urlPath.StartsWith("~/"))
            {
                return VirtualPathUtility.ToAbsolute(urlPath);
            }
            else
            {
                return VirtualPathUtility.ToAbsolute(urlPath);
            }
        }
        #endregion


        #region public static string TrimString ( string stringToTrim, int trimLength, string appendString )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringToTrim"></param>
        /// <param name="trimLength"></param>
        /// <param name="appendString"></param>
        /// <returns></returns>
        public static string TrimString(string stringToTrim, int trimLength, string appendString)
        {
            if (stringToTrim.Length <= trimLength)
            {
                return stringToTrim;
            }
            else
            {
                int spacePos = stringToTrim.IndexOf(' ', trimLength);
                if (spacePos > 0)
                {
                    return string.Format("{0}{1}", stringToTrim.Substring(0, spacePos), appendString);
                }
                else
                {
                    return stringToTrim;
                }
            }
        }
        #endregion


        #region public sealed class Url
        /// <summary>
        /// 
        /// </summary>
        public sealed class Url
        {

            private const bool UrlDoBase64 = true;

            #region public static string EncodeString ( string stringToEncode )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="stringToEncode"></param>
            /// <returns></returns>
            public static string EncodeString(string stringToEncode)
            {
                if (UrlDoBase64)
                {
                    return HttpUtility.UrlEncode(
                        System.Convert.ToBase64String(
                            System.Text.ASCIIEncoding.ASCII.GetBytes(stringToEncode)
                            )
                        );
                }
                else
                {
                    return HttpUtility.UrlEncode(stringToEncode);
                }
            }
            #endregion


            #region public static string DecodeString ( string stringToDecode )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="stringToDecode"></param>
            /// <returns></returns>
            public static string DecodeString(string stringToDecode)
            {
                if (UrlDoBase64)
                {
                    return System.Text.ASCIIEncoding.ASCII.GetString(
                         System.Convert.FromBase64String(
                             HttpUtility.UrlDecode(stringToDecode)
                             )
                         );
                }
                else
                {
                    return HttpUtility.UrlDecode(stringToDecode);
                }
            }
            #endregion


            #region public static string GetUrlForLoginService ()
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public static string GetUrlForLoginService()
            {
                bool useSSL = WebsiteSettings.AppSettings.GetValue("UseSSLForLogin", false);

                return string.Format("{0}://{1}{2}", useSSL ? "https" : "http", WebsiteSettings.Request.Url.Authority, SiteConstants.Urls.Constants.LoginService);
            }
            #endregion


            #region public static string GetUrlForPaymentService ()
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public static string GetUrlForPaymentService()
            {
                bool useSSL = WebsiteSettings.AppSettings.GetValue("UseSSLForPayment", false);

                return string.Format("{0}://{1}{2}", useSSL ? "https" : "http", WebsiteSettings.Request.Url.Authority, SiteConstants.Urls.Constants.PaymentService);
            }
            #endregion


            #region public sealed class InternalRedirector
            /// <summary>
            /// 
            /// </summary>
            public sealed class InternalRedirector
            {
                private const string Key_QueryStringParam = "redir";

                #region public static string GetRedirectUrlParamForQueryString ( string urlForRedirect )
                /// <summary>
                /// 
                /// </summary>
                /// <param name="urlForRedirect"></param>
                /// <returns></returns>
                public static string GetRedirectUrlParamForQueryString(string urlForRedirect)
                {
                    try
                    {
                        UriBuilder uriBuilder = new UriBuilder(urlForRedirect);
                        urlForRedirect = uriBuilder.Uri.PathAndQuery;
                    }
                    catch
                    {
                        // Trust that Url is relative and not malicious.
                    }

                    return string.Format("{0}={1}", Key_QueryStringParam, EncodeString(urlForRedirect));
                }
                #endregion


                #region public static string GetRedirectUrlParamForQueryStringForCurrentRequest ()
                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>
                public static string GetRedirectUrlParamForQueryStringForCurrentRequest()
                {
                    return string.Format("{0}={1}", Key_QueryStringParam, EncodeString(WebsiteSettings.Request.Url.PathAndQuery));
                }
                #endregion


                #region public static string GetRedirectUrlFromQueryString ()
                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>
                public static string GetRedirectUrlFromQueryString()
                {
                    if (WebsiteSettings.Request.QueryString[Key_QueryStringParam] != null)
                    {
                        string urlToRedirectTo = DecodeString(WebsiteSettings.Request.QueryString[Key_QueryStringParam].ToString());
                        try
                        {
                            UriBuilder uriBuilder = new UriBuilder(urlToRedirectTo);
                            return uriBuilder.Uri.PathAndQuery;
                        }
                        catch
                        {
                            // Trust that Url is relative and not malicious.
                            return VirtualPathUtility.ToAbsolute(urlToRedirectTo);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion
            }
            #endregion


            #region public sealed class ExternalRedirector
            /// <summary>
            /// 
            /// </summary>
            public sealed class ExternalRedirector
            {
                private const string Key_QueryStringParam = "redir";

                #region public static string GetRedirectUrlParamForQueryString ( string urlForRedirect )
                /// <summary>
                /// 
                /// </summary>
                /// <param name="urlForRedirect"></param>
                /// <returns></returns>
                public static string GetRedirectUrlParamForQueryString(string parameterName, string urlForRedirect)
                {
                    try
                    {
                        UriBuilder uriBuilder = new UriBuilder(urlForRedirect);
                        urlForRedirect = uriBuilder.Uri.AbsoluteUri;
                    }
                    catch
                    {
                        // Trust that Url is relative and not malicious.
                    }

                    return string.Format("{0}={1}", parameterName, EncodeString(urlForRedirect));
                }
                #endregion

                #region public static string GetRedirectUrlFromQueryString ()
                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>
                public static string GetRedirectUrlFromQueryString(string parameterName)
                {
                    if (WebsiteSettings.Request.QueryString[parameterName] != null)
                    {
                        string urlToRedirectTo = DecodeString(WebsiteSettings.Request.QueryString[parameterName].ToString());
                        try
                        {
                            // TODO: as this is a fully qualified url there needs to be a white list of hosts (domains) thaat the app will redirect to. Need to prevent redirection exploit primarily used by phishing.
                            UriBuilder uriBuilder = new UriBuilder(urlToRedirectTo);
                            return uriBuilder.Uri.AbsoluteUri;
                        }
                        catch
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion
            }
            #endregion


            public static void RedirectToTopUp()
            {
                const string topupUrl = "~/Profile/TopUp.aspx?ReturnUrl={0}";

                string returnUrl = WebsiteSettings.Request.RawUrl; ;

                byte[] returnUrlAsBytes = Encoding.ASCII.GetBytes(returnUrl);
                string returnQueryString = Convert.ToBase64String(returnUrlAsBytes);

                WebsiteSettings.Response.Redirect(string.Format(topupUrl, returnQueryString));
            }

            /// <summary>
            /// Redirect to current page on SSL and
            /// show the login dialog as modal lightbox.
            /// </summary>
            public static void RedirectAndShowLogin()
            {
                if (!WebsiteSettings.Request.IsSecureConnection)
                {
                    WebsiteSettings.Response.Redirect(Request.AddParameterToUri(WebsiteSettings.Request.Url, SiteConstants.Urls.ParameterNames.LoginDialogue, "1").PathAndQuery);
                }
            }

            /// <summary>
            /// Redirect to current page on SSL and
            /// show the login dialog as modal lightbox.
            /// </summary>
            public static void RedirectAndShowLoginAndPaydialogue()
            {
                string url = WebsiteSettings.Request.Url.OriginalString;

                if (!WebsiteSettings.Request.IsSecureConnection)
                {
                    //url = "https" + url.Substring(4);
                    NameValueCollection urlParams = new NameValueCollection(2);
                    urlParams.Add(SiteConstants.Urls.ParameterNames.LoginDialogue, "1");
                    urlParams.Add(SiteConstants.Urls.ParameterNames.PayDialogue, "1");

                    WebsiteSettings.Response.Redirect(Request.AddParametersToUri(WebsiteSettings.Request.Url, urlParams).PathAndQuery);
                }
            }

            public sealed class SEO
            {
                private static bool _useSEOForUrls = WebsiteSettings.AppSettings.GetValue("UseSEOForUrls", false);

                private static Dictionary<string, string> _rawUrlToSEOFormatUrl = new Dictionary<string, string>();

                public static string GetSEOFriendlyUrl(string urlPathAndQuery)
                {
                    if (!string.IsNullOrEmpty(urlPathAndQuery) && _useSEOForUrls)
                    {
                        string[] urlPathAndQuerySplit = urlPathAndQuery.Split('?');
                        if (urlPathAndQuerySplit.Length >= 1)
                        {
                            string seoFormatUrl = "";
                            if (_rawUrlToSEOFormatUrl.TryGetValue(urlPathAndQuery.ToLower(), out seoFormatUrl))
                            {
                                return string.IsNullOrEmpty(seoFormatUrl) ? urlPathAndQuery : seoFormatUrl;
                            }
                            else
                            {
                                seoFormatUrl = Rewriter.GetSEOFormatUrl(urlPathAndQuerySplit[0]);
                                if (!string.IsNullOrEmpty(seoFormatUrl))
                                {
                                    seoFormatUrl = seoFormatUrl.Replace(SiteConstants.FormatIdentifiers.QueryStringParamaters, urlPathAndQuerySplit.Length >= 2 ? urlPathAndQuerySplit[1] : "");
                                }
                                _rawUrlToSEOFormatUrl.Add(urlPathAndQuery.ToLower(), seoFormatUrl);

                                return string.IsNullOrEmpty(seoFormatUrl) ? urlPathAndQuery : seoFormatUrl;
                            }
                        }
                    }

                    return urlPathAndQuery;
                }

            }
        }
        #endregion


        #region public sealed class Request
        /// <summary>
        /// 
        /// </summary>
        public static class Request
        {
            public static SiteEnums.ProductType GetProductTypeFromRequest()
            {
                return GetProductTypeFromRawUrl(WebsiteSettings.Request.RawUrl);
            }

            public static SiteEnums.ProductType GetProductTypeFromRawUrl(string rawUrl)
            {
                if (rawUrl.IndexOf(string.Format("/{0}/", SiteEnums.ProductType.BoxOffice), StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return SiteEnums.ProductType.BoxOffice;
                }
                else if (rawUrl.IndexOf(string.Format("/{0}/", SiteEnums.ProductType.CatchUp), StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return SiteEnums.ProductType.CatchUp;
                }
                else if (rawUrl.IndexOf(string.Format("/{0}/", SiteEnums.ProductType.OpenTime), StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return SiteEnums.ProductType.OpenTime;
                }
                else
                {
                    return SiteEnums.ProductType.OnDemand;
                }
            }

            public static string GetQueryStringParameterValue(string parameter, string defaultValue)
            {
                try
                {
                    if (WebsiteSettings.Request.QueryString[parameter] == null)
                    {
                        return defaultValue;
                    }

                    return WebsiteSettings.Request.QueryString[parameter].ToString();
                }
                catch
                {
                    return defaultValue;
                }
            }

            public static int GetQueryStringParameterValue(string parameter, int defaultValue)
            {
                try
                {
                    if (WebsiteSettings.Request.QueryString[parameter] == null)
                    {
                        return defaultValue;
                    }

                    return Convert.ToInt32(WebsiteSettings.Request.QueryString[parameter]);
                }
                catch
                {
                    return defaultValue;
                }
            }

            public static bool GetQueryStringParameterValue(string parameter, bool defaultValue)
            {
                try
                {
                    if (WebsiteSettings.Request.QueryString[parameter] == null)
                    {
                        return defaultValue;
                    }

                    return Convert.ToBoolean(WebsiteSettings.Request.QueryString[parameter]);
                }
                catch
                {
                    return defaultValue;
                }
            }

            public static TEnum GetQueryStringParameterValue<TEnum>(string parameter, TEnum defaultEnumValue) where TEnum : struct
            {
                if (WebsiteSettings.Request.QueryString[parameter] == null)
                {
                    return defaultEnumValue;
                }

                TEnum enumValue;
                if (Enum.TryParse<TEnum>(WebsiteSettings.Request.QueryString[parameter], out enumValue))
                {
                    return enumValue;
                }
                else
                {
                    return defaultEnumValue;
                }
            }

            public static Uri AddParameterToRequest(HttpRequest request, string paramName, string paramValue)
            {
                return AddParameterToUri(request.Url, paramName, paramValue);
            }

            public static Uri AddParameterToUri(Uri uri, string paramName, string paramValue)
            {
                NameValueCollection parameters = new NameValueCollection();
                parameters.Add(paramName, paramValue);
                return AddParametersToUri(uri, parameters);
            }

            public static Uri AddParametersToUri(Uri uri, NameValueCollection parameters)
            {
                // If there are no query string params then straight add.
                NameValueCollection currentParameters = GetParametersFromQueryString(uri.Query);
                foreach (string parameterName in parameters.AllKeys)
                {
                    if (currentParameters[parameterName] == null)
                    {
                        currentParameters.Add(parameterName, parameters[parameterName]);
                    }
                    else
                    {
                        currentParameters[parameterName] = parameters[parameterName];
                    }
                }

                UriBuilder uriBuilder = new UriBuilder(uri);
                uriBuilder.Query = GetQueryStringForParameters(currentParameters);

                return uriBuilder.Uri;
            }

            private static string GetQueryStringForParameters(NameValueCollection parameters)
            {
                string queryString = "";
                for (int i = 0; i < parameters.Count; i++)
                {
                    queryString += string.Format("{0}{1}={2}", i == 0 ? "" : "&", parameters.GetKey(i), parameters[i]);
                }
                return queryString;
            }

            private static NameValueCollection GetParametersFromQueryString(string queryString)
            {
                NameValueCollection parameters = new NameValueCollection();
                if (!string.IsNullOrEmpty(queryString))
                {
                    string[] parametersSplit = queryString.TrimStart('?').Split('&');
                    foreach (string parameterNameValue in parametersSplit)
                    {
                        string[] parameterSplit = parameterNameValue.Split('=');
                        if (parameterSplit.Length >= 1 && !string.IsNullOrEmpty(parameterSplit[0]))
                        {
                            parameters.Add(parameterSplit[0], parameterSplit.Length >= 2 ? parameterSplit[1] : "");
                        }
                    }
                }
                return parameters;
            }

            public static Uri RemoveParametersFromUri(Uri uri, string[] parameterNames)
            {
                if (string.IsNullOrEmpty(uri.Query) || parameterNames.Length == 0)
                {
                    return uri;
                }

                string actionUrl = uri.AbsoluteUri.Replace(uri.Query, "");
                string actionQueryString = "";
                string[] queryNameValues = uri.Query.TrimStart('?').Split('&');
                for (int i = 0; i < queryNameValues.Length; i++)
                {
                    string[] queryParamSplit = queryNameValues[i].Split('=');
                    if (queryParamSplit.Length >= 1)
                    {
                        if (!parameterNames.Contains<string>(queryParamSplit[0], StringComparer.Create(System.Globalization.CultureInfo.InvariantCulture, true)))
                        {
                            actionQueryString += string.Format("{0}={1}&", queryParamSplit[0], queryParamSplit.Length == 2 ? queryParamSplit[1] : "");
                        }
                    }
                }

                return new Uri(string.Format("{0}?{1}", actionUrl, actionQueryString.TrimEnd('&')));
            }
        }
        #endregion


        #region public sealed class JavaScript
        /// <summary>
        /// 
        /// </summary>
        public sealed class JavaScript
        {

            public static string GetNewDateStringWithVar(string varName, DateTime? dateTime, bool includeTime)
            {
                if (!dateTime.HasValue)
                {
                    return string.Format("var {0} = null;", varName);
                }

                return string.Format("var {0} = {1}", varName, GetNewDateString(dateTime, includeTime));
            }

            public static string GetNewDateString(DateTime? dateTime, bool includeTime)
            {
                if (!dateTime.HasValue)
                {
                    return "null";
                }

                // Note the month index in Javascript starts at 0 i.e. 0 = January
                if (includeTime)
                {
                    return string.Format("new Date({0}, {1}, {2}, {3}, {4}, {5});", dateTime.Value.Year, dateTime.Value.Month - 1, dateTime.Value.Day, dateTime.Value.Hour, dateTime.Value.Minute, dateTime.Value.Second);
                }
                else
                {
                    return string.Format("new Date({0}, {1}, {2});", dateTime.Value.Year, dateTime.Value.Month - 1, dateTime.Value.Day);
                }
            }
        }
        #endregion

    }
}