﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{

	public partial class SiteConstants
	{

		#region public sealed class Urls
		/// <summary>
		/// 
		/// </summary>
		public sealed class Urls
		{

			#region Constants
			public sealed class Constants
			{
				public const string BoxOfficeHomePage = "~/BoxOffice/Home.aspx";
				public const string CatchUpHomePage = "~/CatchUp/Home.aspx";
				public const string OpenTimeHomePage = "~/OpenTime/Home.aspx";
				public const string OnDemandHomePage = "~/Home.aspx";
				public const string DStvHomePage = "http://www.dstv.com";

				public const string HomePageName = "Home.aspx";
                public const string ShowPageName = "ShowDetail.aspx";
                public const string VideoPageName = "VideoDetail.aspx";
				public const string BrowsePageName = "Browse.aspx";

				public const string SupportedCountriesPage = "/Other/SupportedCountries.aspx";

				public const string ProfileTopUpPage = "/Profile/TopUp.aspx";

				public const string LoginService = "/Services/AsyncCalls.asmx";
				public const string PaymentService = "/Services/AsyncCalls.asmx";
			}
			#endregion


			#region public sealed class ParameterNames
			/// <summary>
			/// 
			/// </summary>
			public sealed class ParameterNames
			{
				public const string EditorialListId = "EditorialId";
				public const string SearchTerm = "SearchTerm";
				public const string ClassificationId = "ClassificationId";
				public const string ClassificationType = "ClassificationType";
				public const string ClassificationName = "ClassificationName";
				public const string PageNumber = "Page";
				public const string SortFilter = "SortFilter";
				public const string ProgramId = "ProgramId";
				public const string VideoId = "VideoId";
				public const string PayDialogue = "Pay";
				public const string LoginDialogue = "Login";
				public const string VideoDetailAction = "VDA";
				public const string DesktopPlayerUrl = "DPU";
				public const string IsForthcoming = "IsFC";
                public const string ParentClassificationId = "ParentId";
			}
			#endregion


			#region public sealed class ParameterValues
			/// <summary>
			/// 
			/// </summary>
			public sealed class ParameterValues
			{
				public const string PlayMovie = "PM";
				public const string DownloadMovie = "DM";
				public const string PlayTrailer = "PT";
				public const string PlayEPK = "PE";
				public const string PayForMovie = "PY";
				public const string Shows = "Shows";
			}
			#endregion


			#region public static string GetHomePageUrl ( SiteEnums.ProductType productType )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="productType"></param>
			/// <returns></returns>
			public static string GetHomePageUrl ( SiteEnums.ProductType productType )
			{
				switch (productType)
				{
					case SiteEnums.ProductType.BoxOffice:
						return Constants.BoxOfficeHomePage;
					case SiteEnums.ProductType.CatchUp:
						return Constants.CatchUpHomePage;
					case SiteEnums.ProductType.OpenTime:
						return Constants.OpenTimeHomePage;
					case SiteEnums.ProductType.OnDemand:
						return Constants.OnDemandHomePage;
					case SiteEnums.ProductType.DStv:
						return Constants.DStvHomePage;
				}

				return "";
			}
			#endregion

		}
		#endregion


		#region public sealed class DisplayNames
		/// <summary>
		/// 
		/// </summary>
		public sealed class DisplayNames
		{

			#region Constants
			public const string BoxOffice = "BoxOffice";
			public const string CatchUp = "CatchUp";
			public const string OpenTime = "Open Time";
            public const string OnDemand = "On Demand";
			public const string DStv = "DStv.com";
			#endregion


			#region public static string GetDisplayNameForSite ( SiteEnums.ProductType productType )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="productType"></param>
			/// <returns></returns>
			public static string GetDisplayNameForSite ( SiteEnums.ProductType productType )
			{
				switch (productType)
				{
					case SiteEnums.ProductType.BoxOffice:
						return BoxOffice;
					case SiteEnums.ProductType.CatchUp:
						return CatchUp;
					case SiteEnums.ProductType.OpenTime:
						return OpenTime;
					case SiteEnums.ProductType.OnDemand:
						return OnDemand;
					case SiteEnums.ProductType.DStv:
						return DStv;
				}

				return "unknown";
			}
			#endregion


			#region public static string GetDisplayNameForSite ( ProductTypes productType )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="productType"></param>
			/// <returns></returns>
			public static string GetDisplayNameForSite ( ProductTypes productType )
			{
				switch (productType)
				{
					case ProductTypes.BoxOffice:
						return BoxOffice;
					case ProductTypes.CatchUp:
						return CatchUp;
					case ProductTypes.OpenTime:
						return OpenTime;
					case ProductTypes.OnDemand:
						return OnDemand;
				}

				return "unknown";
			}
			#endregion


			#region public static string GetDisplayNameForVideoSortOrder ( SiteEnums.VideoSortOrder videoSortOrder )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="videoSortOrder"></param>
			/// <returns></returns>
			public static string GetDisplayNameForVideoSortOrder ( SiteEnums.VideoSortOrder videoSortOrder )
			{
				switch (videoSortOrder)
				{
					case SiteEnums.VideoSortOrder.ForthcomingReleases:
						return "Forthcoming Releases";
					case SiteEnums.VideoSortOrder.LastChance:
						return "Last Chance";
					case SiteEnums.VideoSortOrder.LatestReleases:
						return "Latest Releases";
                    case SiteEnums.VideoSortOrder.ProgramName:
					case SiteEnums.VideoSortOrder.Title:
						return "Title";
					case SiteEnums.VideoSortOrder.None:
					default:
						return "";
				}
			}
			#endregion


			#region public static string GetShortDisplayNameForVideoSortOrder ( SiteEnums.VideoSortOrder videoSortOrder )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="videoSortOrder"></param>
			/// <returns></returns>
			public static string GetShortDisplayNameForVideoSortOrder ( SiteEnums.VideoSortOrder videoSortOrder )
			{
				switch (videoSortOrder)
				{
					case SiteEnums.VideoSortOrder.ForthcomingReleases:
						return "Forthcoming";
					case SiteEnums.VideoSortOrder.LastChance:
						return "Last Chance";
					case SiteEnums.VideoSortOrder.LatestReleases:
						return "Latest";
					case SiteEnums.VideoSortOrder.Title:
						return "Video Title (A-Z)";
					case SiteEnums.VideoSortOrder.MostPopular:
						return "Most Popular";
					case SiteEnums.VideoSortOrder.SearchResultOrder:
						return "Search Ranking";
					case SiteEnums.VideoSortOrder.ShowsByTitle:
						return "Show Title (A-Z)";
					case SiteEnums.VideoSortOrder.ShowsByLatestVideo:
						return "Latest";
					case SiteEnums.VideoSortOrder.ProgramName:
						return "Video Title (A-Z)";
					case SiteEnums.VideoSortOrder.None:
					default:
						return "";
				}
			}
			#endregion

		}
		#endregion


		#region public sealed class Display
		/// <summary>
		/// 
		/// </summary>
		public sealed class Display
		{

			#region public sealed class Video
			/// <summary>
			/// 
			/// </summary>
			public sealed class Video
			{
                public const string ShortDateDisplayFormat = "dd MMM";
                public const string StartDateDisplayFormat = "dd MMM \"'\"yy";
                public const string ExpiryDateDisplayFormat = "dd MMM \"'\"yy";
                public const string AirDateDisplayFormat = "ddd, dd MMM \"'\"yy";
				public const int AbstractTrimLength = 80;
				public const string EpisodeTitleFormatString = "Episode {EpisodeNumber}";
				public const string EpisodeTitleWithSeasonNumberFormatString = "Episode {EpisodeNumber}, S{SeasonNumber}";
				public const string FullTitleWithEpisodeFormatString = "{VideoTitle}: Episode {EpisodeNumber}";
				public const string FullTitleWithEpisodeAndSeasonNumberFormatString = "{VideoTitle}: Episode {EpisodeNumber} (S{SeasonNumber})";

				public const string DateWithTime = "yyyy/MM/dd (HH:mm)";
                public const string CurrencyFormat = "{0} {1:#,##0.00}"; //{0} = Currency Identifier, {1} = Amount

				private static DateTime _minDateValue = new DateTime(1900, 01, 01);
				public static DateTime MinDateValue
				{
					get { return _minDateValue; }
				}
			}
			#endregion


			#region public sealed class Message
			/// <summary>
			/// 
			/// </summary>
            public sealed class Message
			{
                public static string GetLoginHTML()
                {
                    return string.Format("Sorry, you need to be logged in to view videos, please <a href=\"{0}login.aspx?" +
                        "hl=en&tab=nw&lightbox[iframe]=true&lightbox[width]=700&lightbox[height]=475&lightbox[overflow]='hidden'\"" + 
                        " id=\"LoginLightbox\">login</a> to continue.<br /><br />Not a registered user yet? <a href=\"{0}SignUp.aspx\">" + 
                        "Sign up</a>.", Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath);
                }

			}
			#endregion

		}
		#endregion


		#region public sealed class FormatIdentifiers
		/// <summary>
		/// 
		/// </summary>
		public sealed class FormatIdentifiers
		{
			public const string EpisodeNumber = "{EpisodeNumber}";
			public const string EpisodeTitle = "{EpisodeTitle}";
			public const string Id = "{Id}";
			public const string ProductType = "{ProductType}";
			public const string ProgramId = "{ProgramId}";
			public const string ProgramTitle = "{ProgramTitle}";
			public const string SeasonNumber = "{SeasonNumber}";
			public const string VideoId = "{VideoId}";
			public const string VideoTitle = "{VideoTitle}";
			public const string ListId = "{ListId}";
			public const string ListTitle = "{ListTitle}";
			public const string CatalogueCriteria = "{CatalogueCriteria}";
			public const string CatalogueCriteriaWithItemIds = "{CatalogueCriteriaWithItemIds}";
			public const string SortFilter = "{SortFilter}";
			public const string SubItemId = "{SubItemId}";
			public const string SubItemType = "{SubItemType}";
			public const string SubItemName = "{SubItemName}";
			public const string ClassificationId = "{ClassificationId}";
			public const string ClassificationType = "{ClassificationType}";
			public const string ClassificationName = "{ClassificationName}";
			public const string QueryStringParamaters = "{Params}";
            public const string ParentId = "{ParentId}";
            public const string ConnectId = "{ConnectId}";
            public const string ExpiryDate = "{ExpiryDate}";
		}
		#endregion


        #region public static string GetProductIdsForProduct ( ProductTypes productType )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        public static List<int> GetProductIdsForProduct(SiteEnums.ProductType productType)
        {
            List<int> productIdList = new List<int>();

            if (productType == SiteEnums.ProductType.OnDemand || productType == SiteEnums.ProductType.BoxOffice)
            {
                productIdList.Add(10);
            }

            if (productType == SiteEnums.ProductType.OnDemand || productType == SiteEnums.ProductType.CatchUp)
            {
                productIdList.Add(9);
            }

            return productIdList;
        }
        #endregion

	}
}