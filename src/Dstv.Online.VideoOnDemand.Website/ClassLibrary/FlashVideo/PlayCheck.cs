﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data.ResponseTypes;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo
{
    /// <summary>
    /// this class checks if a user is able to watch a video or not
    /// </summary>
    public class PlayCheck
    {
        /// <summary>
        /// Determines whether or not the json feed needs to be generated.
        /// If the video is free the user does not need to be logged in and no valid rental needs to exist.
        /// If the video is NOT free, the user needs to be logged in and have a valid rental (not yet expired) for the video
        /// </summary>
        /// <param name="videoId">The video id.</param>
        /// <param name="msg">The result message.</param>
        /// <returns>
        /// 	<c>true</c> if the feed json can be built feed for the specified video and user; otherwise, <c>false</c>.
        /// </returns>
        public bool CanBuildFeedBoxOffice(string videoId, out string msg)
        {
            bool canBuildFeed = true;
            msg = string.Empty;

            try
            {
                if (checkVideoIdValid(videoId, out msg))
                {
                    MediaCatalogue.CatalogueItemIdentifier catalogueId = new MediaCatalogue.CatalogueItemIdentifier(videoId);
                    VideoDetailResult video = VideoCatalogue.GetVideoDetail(catalogueId);

                    if (video != null)
                    {
                        //Check if video belongs to box office
                        Transactions.ProductTypeIdentifier product = new Transactions.ProductTypeIdentifier(ProductTypes.BoxOffice);
                        if (video.CatalogueItem.ProductTypeId.Value != product.Value)
                        {
                            canBuildFeed = false;
                            msg = string.Format("Video does not belong to this product. BoxOffice.");
                        }
                        //Check if the user is in a supported country
                        //The user shouldn't get to this point if they are in an unsupported country though as this check is done on the master page (but that check tries to get the product from the URL)
                        else if (!WebsiteSettings.SupportedTerritories.IsUserInSupportedTerritory(SiteEnums.ProductType.BoxOffice))
                        {
                            canBuildFeed = false;
                            msg = string.Format("BoxOffice is currently not available to the country you are in.");
                        }
                        else
                        {

                            //if the video is free, no need to authenticate user
                            if (!isVideoFree(video))
                            {
                                if (!DstvUser.IsAuthenticated)
                                {
                                    canBuildFeed = false;
                                    msg = SiteConstants.Display.Message.GetLoginHTML();//"This video is not free. You Must Log In To Watch This Video";

                                }
                                else // check if the user has the right to view this video
                                {
                                    //Check if user has payed for the video
                                    RentalUser userRental = new Modules.Rental.RentalUser();
                                    string userID = DstvUser.UserDStvPrincipal.DStvConnectID;
                                    Rental rental = new Modules.Rental.Rental();
                                    RentalResult result = rental.GetVideoRentalsByUserId(userID);
                                    VideoRentalType rentalItem = null;

                                    if (result != null && result.TotalResults > 0)
                                    {
                                        rentalItem = result.Rentals.Where(item => item.VideoId == rentalItem.VideoId).FirstOrDefault();
                                    }
                                    
                                    if (rentalItem != null)
                                    {
                                        if (rentalItem.HasRentalExpired)
                                        {
                                            canBuildFeed = false;
                                            msg = "Payment required. Rental has expired.";
                                        }
                                        else if (rentalItem.HasVideoExpired)
                                        {
                                            canBuildFeed = false;
                                            msg = "This video has expired.";                                                
                                        }
                                    }
                                    else
                                    {
                                        canBuildFeed = false;
                                        msg = "Payment required. NO rentals found.";

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                canBuildFeed = false;
                msg = string.Format("Exception Thrown :: {0}", ex.ToString());
            }

            return canBuildFeed;
        }

        /// <summary>
        /// Determines whether or not the json feed needs to be generated.
        /// The user must be in a supported country.
        /// If the video is free the user does not need to be logged in.
        /// If the video is NOT free, the user needs to be logged in and must be a premium DSTV subscriber.
        /// </summary>
        /// <param name="videoId">The video id.</param>
        /// <param name="msg">The result message.</param>
        /// <returns>
        /// 	<c>true</c> if the feed json can be built feed for the specified video and user; otherwise, <c>false</c>.
        /// </returns>
        public bool CanBuildFeedCatchUp(string videoId, out string msg)
        {


            bool canBuildFeed = true;
            msg = string.Empty;

            try
            {
                if (checkVideoIdValid(videoId, out msg))
                {
                    MediaCatalogue.CatalogueItemIdentifier credentials = new MediaCatalogue.CatalogueItemIdentifier(videoId);
                    var video = VideoCatalogue.GetVideoDetail(credentials);

                    if (video != null)
                    {
                        //Check if video belongs to catch up
                        Transactions.ProductTypeIdentifier product = new Transactions.ProductTypeIdentifier(ProductTypes.CatchUp);
                        if (video.CatalogueItem.ProductTypeId.Value != product.Value)
                        {
                            canBuildFeed = false;
                            msg = string.Format("Video does not belong to this product. CatchUp.");
                        }
                        //Check if the user is in a supported country
                        //The user shouldn't get to this point if they are in an unsupported country though as this check is done on the master page (but that check tries to get the product from the URL)
                        else if (!WebsiteSettings.SupportedTerritories.IsUserInSupportedTerritory(SiteEnums.ProductType.CatchUp))
                        {
                            canBuildFeed = false;
                            msg = string.Format("CatchUp is currently not available to the country you are in.");
                        }
                        else
                        {
                            //if the video is free, no need to authenticate user
                            if (!isVideoFree(video))
                            {
                                //if not logged in
                                if (!DstvUser.IsAuthenticated)
                                {
                                    canBuildFeed = false;
                                    msg = SiteConstants.Display.Message.GetLoginHTML();//"This video is not free. You Must Log In To Watch This Video";
                                }
                                else// check if the user has the right to view this video
                                {
                                    TransactionResponse entitleResult = DstvUser.EntitleCatchupContent();

                                    if (!entitleResult.Success)
                                    {
                                        canBuildFeed = false;

                                        //if entitlement is not successful because the user is not a premium subscriber, the error message will be replaced below
                                        msg = "There was an error generating a media player license for the video.\r\n\r\nPlease try the video action again and if the problem persists please contact DStv Online support.";
                                    }

                                    //Only for premium Dstv subscribers
                                    if (WebsiteSettings.DisablePremiumSubsCheck || !DstvUser.IsPremiumDstvSubscriber)
                                    {
                                        string ConnectProfileLink = System.Configuration.ConfigurationManager.AppSettings["ConnectProfileLinkCU"];
                                        string accountLink = string.Format("<a href=\"{0}" + ConnectProfileLink + "\">update your DStv Connect profile</a>", ClassLibrary.SiteUtils.WebAppRootPath);

                                        canBuildFeed = false;
                                        msg = "Sorry, this service is currently only available to DStv Premium subscribers. To link your DStv Premium account please " + accountLink + " with your Smartcard details. ";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                canBuildFeed = false;
                msg = string.Format("Exception Thrown :: {0}", ex.ToString());
            }
            return canBuildFeed;
        }

        /// <summary>
        /// Check if the video id is not null and is a positive integer.
        /// </summary>
        /// <param name="videoId">The video id.</param>
        /// <param name="msg">The result message.</param>
        /// <returns></returns>
        private bool checkVideoIdValid(string videoId, out string msg)
        {
            bool isValid = false;
            msg = string.Empty;

            if (string.IsNullOrEmpty(videoId))
            {
                //Tell The user that the video ID is null
                msg = "Video ID cannot be null";
            }
            else
            {
                // Check if videoID is valid number
                int videoIdInt = 0;
                int.TryParse(videoId, out videoIdInt);

                if (videoIdInt < 1)
                {
                    msg = "Video ID is invalid";
                }
                else
                {
                    isValid = true;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Check if the video is free, i.e. The video is and EPK or trailer
        /// </summary>
        /// <param name="video">The video object</param>
        /// <returns></returns>
        private bool isVideoFree(VideoDetailResult video)
        {
            bool isFree = false;
            VideoProgramType vidType = video.CatalogueItem.VideoProgramType;

            if (vidType == VideoProgramType.MovieEPK || vidType == VideoProgramType.MovieTrailer || vidType == VideoProgramType.ShowTrailer)
            {
                isFree = true;
            }

            return isFree;
        }

    }
}