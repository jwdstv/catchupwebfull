﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo
{
    /// <summary>
    /// this class will be used to generate jason feeds that will be used by the flash player
    /// </summary>
    public class VideoFeed
    {
        /// <summary>
        /// Gets the json feed. based on video ID and criteria
        /// </summary>
        /// <param name="videoID">The video ID.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public Types.FeedResponse GetJsonFeed(string videoID, Types.FeedCriteria criteria)
        {
            Types.FeedResponse jsonFeed = new Types.FeedResponse();
            MediaCatalogue.CatalogueItemIdentifier credentials = new MediaCatalogue.CatalogueItemIdentifier(videoID);           
            //Dstv.Online.VideoOnDemand.MediaCatalogue.CatalogueResult videoList = IntegrationBroker.Instance.Catalogue.GetCatalogueItem(credentials, false);
            var videoList = VideoCatalogue.GetVideoDetail(credentials);

            

            if (videoList != null)
            {
                
                    jsonFeed = GetFeedFromVideo(videoList.CatalogueItem, criteria);
                
            }
            else
            {
                jsonFeed.ErrorMessage = "Video Is Null";
                jsonFeed.IsSuccessful = false;
                jsonFeed.JasonFeed = null;
                jsonFeed.JasonFeedString = null;
            }

            return jsonFeed;
        }

        public Types.FeedResponse GetJasonFeedForFreeContent(string videoID, Types.FeedCriteria criteria)
        {
            Types.FeedResponse jsonFeed = new Types.FeedResponse();
            MediaCatalogue.CatalogueItemIdentifier credentials = new MediaCatalogue.CatalogueItemIdentifier(videoID);
            Dstv.Online.VideoOnDemand.MediaCatalogue.CatalogueResult videoList = IntegrationBroker.Instance.Catalogue.GetCatalogueItem(credentials, false);
            //var videoList = VideoCatalogue.GetVideoDetail(credentials);



            if (videoList != null)
            {

                jsonFeed = GetFeedFromVideo(videoList.CatalogueItems[0], criteria);

            }
            else
            {
                jsonFeed.ErrorMessage = "Video Is Null";
                jsonFeed.IsSuccessful = false;
                jsonFeed.JasonFeed = null;
                jsonFeed.JasonFeedString = null;
            }

            return jsonFeed; 
        }

        /// <summary>
        /// Gets the jason feed. based on a given video and criteria
        /// </summary>
        /// <param name="video">The video.</param>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public Types.FeedResponse GetJsonFeed(MediaCatalogue.ICatalogueItem video, Types.FeedCriteria criteria)
        {
            return GetFeedFromVideo(video, criteria);
        }

        private Types.FeedResponse GetFeedFromVideo(MediaCatalogue.ICatalogueItem video, Types.FeedCriteria criteria)
        {
            /*
             TO DO : [done 1 - November - 2010 - Desmond Nzuza]
             * I need to create a video feed criteria datatype. so that I know what things to show/hide\
             * this should include things like, showPreandPostRollAds, show embed code, playUrl, 
             */

            Types.FeedResponse jsonResult = new Types.FeedResponse();
            

            try
            {
                bool checkProduct = true;
                if (video != null && video.ProductTypeId == null)
                    checkProduct = false;

                bool productChecksOut = true;
                if(checkProduct && video.ProductTypeId == null && video.ProductTypeId.Value != criteria.ProductId.Value)
                    productChecksOut = false;

                if (video != null && (productChecksOut))//if (video != null && video.ProductTypeId != null && video.ProductTypeId.Value == criteria.ProductId.Value)//checks if the video belongs to a given product
                {

                    #region playVideo


                    if (criteria.CanPlay)
                    {
                        //create the json object and fill it with data from the video object or catelog
                        //JsonFeed feed = new JsonFeed();
                        JsonFeed feed = JsonFeed.InitializeJson();
                        //Server.MapPath(OnDemandAPI.Util.ConfigValues.PlayerJsonURL));
                        feed.status = "false";

                        feed.result.video.main.allow_seek = true;//Enabled 2010-07-09
                        feed.result.video.main.auto_start = false;

                        string img = string.Empty;
                        string imageServerPath = Dstv.Online.VideoOnDemand.Integration.Extensions.GetImageServerPathSetting();
                        string mediaImageUri = video.MediaImageUri;
                        if (string.IsNullOrWhiteSpace(imageServerPath) || System.Text.RegularExpressions.Regex.IsMatch(mediaImageUri, @"^(HTTP|\.)", System.Text.RegularExpressions.RegexOptions.IgnoreCase) || mediaImageUri.Contains(imageServerPath))
                        {
                            img = mediaImageUri;
                        }
                        else
                        {
                            img = string.Format("{0}{1}", imageServerPath, mediaImageUri);
                        }

                        feed.result.video.main.image_path = img;

                        feed.result.video.main.meta = new JsonFeed.ResultObject.SubVideo.SubMain.SubMeta();

                        feed.result.video.main.meta.comment_count = 0;
                        feed.result.video.main.meta.description = video.Abstract;
                        feed.result.video.main.meta.duration = GetVidDuration(video.DurationDescription);


                        if (video.Airdate.HasValue)
                        {
                            var time_ago = "about " + ((video.Airdate.Value.Month > 0)
                                ? video.Airdate.Value.Month.ToString() + " weeks ago"
                                : video.Airdate.Value.Day.ToString() + " days ago");
                            feed.result.video.main.meta.time_ago = time_ago;
                        }

                        feed.result.video.main.meta.view_count = 0;

                        feed.result.video.main.start_time = 0;
                        feed.result.video.main.thumb_path = feed.result.video.main.image_path;



                        feed.result.video.main.title = GetVidTitle(video, false); //vidTitle;//video.VideoTitle;

                        //feed.result.video.main.url = criteria.PlayeUrl.Replace("[vid]", video.CatalogueItemId.Value.ToString());

                        feed.result.related_videos = GetSubRelatedVideos(video, criteria);

                                                
                        JsonFeed.ResultObject.SubUi.SubGeneric.SubData.SubCodes subData = new JsonFeed.ResultObject.SubUi.SubGeneric.SubData.SubCodes();
                        string url = GetUrlWithoutQueryStrings();
                        string feedUrl = Util.StringManager.GetLinkURL(video.CatalogueItemId.Value.ToString(), url + "?&id=[vid]");
                        string flashSWFUrl = Util.ConfigValues.PlayerSwfURL;
                        //Embed Data
                        if (criteria.ShowEmbedCode) //OM Changed it to use the ShowEmbedCode instead of always true.
                        {
                            string embed_text = "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://macromedia.com/cabs/swflash.cab#version=9,0,0,246' id='[ID]' width='[width]' height='[height]'><param name='movie' value='" + flashSWFUrl + "' /><param name='flashvars' value='base_url=" + feedUrl + "' /><param name='quality' value='high' /><param name='allowscriptaccess' value='always' /><param name='allowfullscreen' value='true' /><embed src='" + flashSWFUrl + "' allowfullscreen='true' flashvars='base_url=" + feedUrl + "' width='[width]' height='[height]' type='application/x-shockwave-flash' allowscriptaccess='always'></embed></object>";
                            subData.small = embed_text.Replace("[width]", "420").Replace("[height]", "313").Replace("[ID]", "dstvSml");
                            subData.medium = embed_text.Replace("[width]", "478").Replace("[height]", "355").Replace("[ID]", "dstvMid");
                            subData.large = embed_text.Replace("[width]", "536").Replace("[height]", "400").Replace("[ID]", "dstvLrg");
                        }




                        //check if one can diaplay ads
                        if (criteria.ShowAds && !string.IsNullOrEmpty(criteria.PostRollTag) && !string.IsNullOrEmpty(criteria.PreRolLAdTag))
                        {
                            //add Post and Pre Roll Adds

                            //add advert tags
                            JsonFeed.ResultObject.SubVideo.SubAds addverts = new JsonFeed.ResultObject.SubVideo.SubAds();
                            JsonFeed.ResultObject.SubVideo.SubAds.SubPost post = new JsonFeed.ResultObject.SubVideo.SubAds.SubPost();
                            JsonFeed.ResultObject.SubVideo.SubAds.SubPre pre = new JsonFeed.ResultObject.SubVideo.SubAds.SubPre();
                            post.ad_path = criteria.PostRollTag;
                            pre.ad_path = criteria.PreRolLAdTag;
                            addverts.post = post;
                            addverts.pre = pre;
                            feed.result.video.ads = addverts;
                        }

                        //set cat for Tracking
                        if (criteria.TrackingIdentifier != null)
                        {
                            string subCategory = criteria.SubTrackingIdentifier;
                            if (string.IsNullOrEmpty(subCategory) && (!string.IsNullOrEmpty(video.ProgramTitle)))//set the subcategory info to the program title if one is not provided
                                subCategory = video.ProgramTitle;
                            feed.result.video.main.category = criteria.TrackingIdentifier;
                            feed.result.video.main.subcategory = criteria.SubTrackingIdentifier;
                        }

                        feed.result.ui.generic_controls.data.embed_codes = subData;
                        feed.result.assets_path = Util.ConfigValues.PlayerAssetURL;//ConfigurationManager.AppSettings["PlayerAssetURL"]; //Util.ConfigValues.ApplicationPath + "Flash/assets/embed_assets.swf";


                        //Get filename and path of the file from video meta
                        //MediaCatalogue.MediaTypeEnum videoType = new MediaCatalogue.MediaTypeIdentifier(MediaCatalogue.MediaTypeEnum.Flash.ToString());
                        //var videoMeta = IntegrationBroker.Instance.Catalogue.GetCatalogueItemMedia(video.CatalogueItemId, videoType);

                        var videoMeta = VideoCatalogue.GetVideoMedia(video.CatalogueItemId, MediaCatalogue.MediaTypeEnum.Flash);
                        //videoMeta.Dimensions

                        //TODO: Remove this code as it is jst meant for testing purposes
                        //feed.result.video.main.video_path = "http://64.71.233.138/mnetvideo/MN_surv3_Chrty_Ashley_300K.flv";                        
                        feed.result.video.main.video_path = Dstv.Framework.Cryptography.Encrypt(Util.ConfigValues.CryptoKey, videoMeta.ServerPath + videoMeta.FileName);

                        feed.status = "ok";

                        jsonResult.JasonFeed = feed;
                        jsonResult.JasonFeedString = Newtonsoft.Json.JsonConvert.SerializeObject(feed);//.Replace("\"related_videos\":null,",string.Empty);
                        jsonResult.IsSuccessful = true;

                    }
                    else
                    {

                        //tell control the user is not autherised to play item;
                        jsonResult.JasonFeed = null;
                        jsonResult.JasonFeedString = "User is not Autherised to play this video";
                        jsonResult.IsSuccessful = false;
                    }

                    #endregion

                }
                else
                {
                    jsonResult.JasonFeedString = null;
                    jsonResult.JasonFeed = null;
                    jsonResult.IsSuccessful = false;
                    if (video != null && video.ProductTypeId != null && video.ProductTypeId.Value != criteria.ProductId.Value)
                    {
                        jsonResult.ErrorMessage = string.Format("This video is not available for this product: {0}", criteria.ProductId.Value);
                    }
                    else if (video != null && video.ProductTypeId == null)
                    {
                        jsonResult.ErrorMessage = "Error: ProductTypeId is Null";
                    }
                    else
                    {
                        jsonResult.ErrorMessage = "Error: VIDEO is Null";
                    }
                }



            }
            catch (Exception ex)
            {
                jsonResult.JasonFeed = null;
                jsonResult.JasonFeedString = null;
                jsonResult.ErrorMessage = "Error: unable to play video: "+ ex.ToString();
                jsonResult.IsSuccessful = false;
            }


            return jsonResult;

        }

        /// <summary>
        /// Gets the duration of the vid. converts the display time into milliseconds so that the flash player will be able to read it.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        public int GetVidDuration(string time)
        {
            int time_from_timepsan = 0;

            if (!string.IsNullOrEmpty(time))
            {

                if (time.Split(':').Length == 2)
                    time = "00:" + time;
            }
            else
            {
                time = "00:00:00";
            }

            time_from_timepsan = Convert.ToInt32(TimeSpan.Parse(time).TotalMilliseconds);


            return time_from_timepsan;
        }

        /// <summary>
        /// Gets the vid title. If the title is empty it will build the title based on information like program title, season and episode information
        /// </summary>
        /// <param name="video">The video.</param>
        /// <returns></returns>
        public string GetVidTitle(ICatalogueItem video, bool isShotTitle)
        {
            string vidTitle = video.VideoTitle;
            string progTitle = video.ProgramTitle;
            int? season = video.SeasonNumber;
            int? episode = video.EpisodeNumber;

            if (!string.IsNullOrEmpty(progTitle) && season.HasValue && episode.HasValue)
            {
                if (!isShotTitle)
                    progTitle = string.Format("{0}: Episode {1} (S{2})", progTitle, episode, season);
                else
                    progTitle = string.Format("E{0}(S{1}): {2}", episode, season, progTitle);
            }

            if (string.IsNullOrEmpty(vidTitle) && !string.IsNullOrEmpty(progTitle))
                vidTitle = progTitle;
            if (string.IsNullOrEmpty(vidTitle) && string.IsNullOrEmpty(progTitle))
                vidTitle = string.Empty;

            return vidTitle;
        }

        /// <summary>
        /// Gets the URL without query strings.
        /// </summary>
        /// <returns></returns>
        public string GetUrlWithoutQueryStrings()
        {
            //assigned the string value...
            string uri1 = HttpContext.Current.Request.Url.AbsoluteUri;
            //creating uriobject
            Uri weburi = new Uri(uri1);
            //querying uri object
            string query = weburi.Query;
            //returning url without querystring by using substring ()of immutable string class
            string weburl = uri1.Substring(0, uri1.Length - query.Length);

            return weburl;
        }

        private List<JsonFeed.ResultObject.SubRelated> GetSubRelatedVideos(ICatalogueItem video, ClassLibrary.FlashVideo.Types.FeedCriteria criteria)
        {
            string VideoExclusionList = video.CatalogueItemId.Value.ToString();            

            string[] excludedVideoIds = VideoExclusionList.Split(',');

            // It's possible that excluded videos may be returned in the result set
            int searchResultLimit = 10;

            VideoFilter videoFilter = new VideoFilter();
            string searchTerm = /*"Series, "+*/ video.Keywords;

            videoFilter.VideoCount = searchResultLimit;
            videoFilter.VideoTypes = "Movie,ShowEpisode,ShowClip";
            videoFilter.SortFilter = SiteEnums.VideoSortOrder.LatestReleases;
            //videoFilter.ProductType = credentials.ProductTypeId.Value;
                        
            videoFilter.CatalogueCriteria.ClassificationCriteria.VideoProgramTypes = new VideoProgramType[] { VideoProgramType.FeatureFilm, VideoProgramType.Movie, VideoProgramType.ShowEpisode };

            

            if (videoFilter.ProductType.ToString() == ClassLibrary.SiteEnums.ProductType.BoxOffice.ToString())
                videoFilter.VideoTypes = "Movie";


            //Set the product type to be what is on the video

            //ProductTypes a = videoFilter.ProductType;
            videoFilter.ProductType = criteria.VideoProdtype;

            // find videos that match search term
            CatalogueResult searchResult = DataAccess.VideoData.GetRelatedVideoItems(SiteEnums.VideoDataType.Summary, videoFilter, int.Parse(video.CatalogueItemId.Value), video.Keywords, video.ProgramId.Value, video.ProgramGenre, video.ProgramSubGenre, new List<int>(new int[] { video.ProductId }));

            // declare subrelated videos
            List<JsonFeed.ResultObject.SubRelated> related_video = new List<JsonFeed.ResultObject.SubRelated>();
            ////iterate throught videos and add them to the realted list
            foreach (var v in searchResult.CatalogueItems)
            {
                if (CanAddRelatedVideo(v, video))//if (v.CatalogueItemId.Value != credentials.CatalogueItemId.Value && !(v.VideoProgramType == VideoProgramType.MovieEPK) && !(v.VideoProgramType == VideoProgramType.MovieTrailer) && !(v.VideoProgramType == VideoProgramType.ShowClip) && !(v.VideoProgramType == VideoProgramType.ShowTrailer)) //test if the video ID is not the same, not an EPK or Trailer
                {

                    //string playUrl = Util.StringManager.GetLinkURL(v.CatalogueItemId.ToString(), Util.ConfigValues.PlayURL);  //Util.ConfigValues.PlayURL.Replace("[vid]",v.ID.ToString());                
                    string playUrl = criteria.PlayeUrl.Replace("[vid]", v.CatalogueItemId.Value.ToString());
                    string vidTitle = GetVidTitle(v, true);

                    JsonFeed.ResultObject.SubRelated rv = new JsonFeed.ResultObject.SubRelated()
                    {
                        thumb_path = v.MediaThumbnailUri.ToString(),
                        title = vidTitle,
                        url = playUrl
                    };
                    //add video as a related video
                    related_video.Add(rv);
                }
            }

            if (related_video.Count < 1)
            {
                related_video = null;
            }

            return related_video;
        }

        private bool CanAddRelatedVideo(ICatalogueItem v, ICatalogueItem credentials)
        {
            bool canAddRelatedVideo = false;

            if (v.CatalogueItemId.Value != credentials.CatalogueItemId.Value && !(v.VideoProgramType == VideoProgramType.MovieEPK) && !(v.VideoProgramType == VideoProgramType.MovieTrailer) && !(v.VideoProgramType == VideoProgramType.ShowClip) && !(v.VideoProgramType == VideoProgramType.ShowTrailer))// its a valid vid
            {
                
                if ((v.VideoProgramType == VideoProgramType.Movie) && (v.ProgramId == credentials.ProgramId))//if its a movie and program IDs match. then it does not qualify
                {
                    canAddRelatedVideo = false;
                }
                else
                {
                    canAddRelatedVideo = true;
                }
 
            }

            return canAddRelatedVideo;
        }
        
    }
}