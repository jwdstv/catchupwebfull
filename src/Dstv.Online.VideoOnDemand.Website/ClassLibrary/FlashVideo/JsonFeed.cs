﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo
{

    /// <summary>
    /// this class represents the jason feed. it is used to build a dynamic jason feed
    /// </summary>
    public class JsonFeed
    {
        public string status { get; set; }
        public ResultObject result { get; set; }
        public string exception { get; set; }

        public class ResultObject
        {
            public SubVideo video { get; set; }
            public bool allow_pseudo { get; set; }
            public List<SubRelated> related_videos { get; set; }
            public string assets_path { get; set; }
            public SubUi ui { get; set; }
            public string uid { get; set; }

            public class SubVideo
            {
                public SubMain main { get; set; }
                public SubAds ads { get; set; }

                public class SubMain
                {
                    public string uid { get; set; }
                    public SubUser user { get; set; }
                    public string url { get; set; }
                    public string url_hd { get; set; }
                    public string image_path { get; set; }
                    public string title { get; set; }
                    public string video_path { get; set; }
                    public string video_path_hd { get; set; }
                    public bool auto_start { get; set; }
                    public SubMeta meta { get; set; }
                    public string thumb_path { get; set; }
                    public int start_time { get; set; }
                    public bool allow_seek { get; set; }
                    public string category { get; set; }
                    public string subcategory { get; set; }

                    public class SubUser
                    {
                        public string uid { get; set; }
                        public string image_path { get; set; }
                        public string screen_name { get; set; }
                    }

                    public class SubMeta
                    {
                        public int view_count { get; set; }
                        public int comment_count { get; set; }
                        public int duration { get; set; }
                        public string description { get; set; }
                        public string time_ago { get; set; }
                    }
                }

                public class SubAds
                {
                    public SubPre pre { get; set; }
                    public SubPost post { get; set; }

                    public class SubPre
                    {
                        //public string type { get; set; }
                        public string ad_path { get; set; }
                    }

                    public class SubPost
                    {
                        //public string type { get; set; }
                        public string ad_path { get; set; }
                    }
                }

            }

            public class SubRelated
            {
                public string thumb_path { get; set; }
                public string title { get; set; }
                public string url { get; set; }
            }

            public class SubUi
            {
                public SubPlayer player_controls { get; set; }
                public SubGeneric generic_controls { get; set; }

                public class SubPlayer
                {
                    public List<string> assets { get; set; }
                    public int progress_padding_left { get; set; }
                    public int progress_padding_right { get; set; }
                    public int padding_side { get; set; }
                    public int padding_bottom { get; set; }
                }

                public class SubGeneric
                {
                    public SubGeneric()
                    {
                        this.assets = new List<string>();
                        this.data = new SubData();
                    }

                    public List<string> assets { get; set; }
                    public SubData data { get; set; }

                    public class SubData
                    {
                        public SubData()
                        {
                            this.embed_codes = new SubCodes();
                        }

                        public SubCodes embed_codes { get; set; }

                        public class SubCodes
                        {
                            public string small { get; set; }
                            public string medium { get; set; }
                            public string large { get; set; }
                        }
                    }
                }
            }
        }

        public static JsonFeed InitializeJson()
        {
            string js = Encoding.ASCII.GetString(Properties.Resources.player);
            js = System.Text.RegularExpressions.Regex.Replace(js, @"^\?\?\?", string.Empty);

            return Newtonsoft.Json.JsonConvert.DeserializeObject<JsonFeed>(js);
        }
               
    }    
}
