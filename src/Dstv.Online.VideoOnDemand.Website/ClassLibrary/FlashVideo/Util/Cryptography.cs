﻿namespace Dstv.Framework
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class Cryptography
    {
        public static byte[] ArrayToPaddingZero(byte[] plainText)
        {
            int length = plainText.Length;
            byte[] buffer = new byte[length];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (i < plainText.Length) ? plainText[i] : byte.Parse("0");
            }
            return buffer;
        }

        public static void byte2Hex(byte b, StringBuilder buf)
        {
            char[] chArray = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
            int index = (b & 240) >> 4;
            int num2 = b & 15;
            buf.Append(chArray[index]);
            buf.Append(chArray[num2]);
        }

        public static string Decrypt(string Key, string EncryptedString)
        {
            string hex = EncryptedString;
            byte[] key = StringToByteArray(Key);
            return decryptStringFromBytes_AES(HexStringToByteArray(hex), key, null).TrimEnd(new char[1]);
        }

        public static string DecryptChangePasswordData(string Key, string EncryptedString)
        {
            string hex = EncryptedString;
            byte[] key = StringToByteArray(Key);
            return decryptStringFromBytes_AES(HexStringToByteArray(hex), key, null).TrimEnd(new char[1]);
        }

        public static string DecryptForgotPasswordData(string Key, string EncryptedString)
        {
            string hex = EncryptedString;
            byte[] key = StringToByteArray(Key);
            return decryptStringFromBytes_AES(HexStringToByteArray(hex), key, null).TrimEnd(new char[1]);
        }

        public static string DecryptInsertLapTimeData(string Key, string EncryptedString)
        {
            string hex = EncryptedString;
            byte[] key = StringToByteArray(Key);
            return decryptStringFromBytes_AES(HexStringToByteArray(hex), key, null).TrimEnd(new char[1]);
        }

        private static string decryptStringFromBytes_AES(byte[] cipherText, byte[] Key, byte[] IV)
        {
            if ((cipherText == null) || (cipherText.Length <= 0))
            {
                throw new ArgumentNullException("cipherText");
            }
            if ((Key == null) || (Key.Length <= 0))
            {
                throw new ArgumentNullException("Key");
            }
            MemoryStream stream = null;
            CryptoStream stream2 = null;
            StreamReader reader = null;
            RijndaelManaged managed = null;
            string str = null;
            try
            {
                managed = new RijndaelManaged();
                managed.Key = Key;
                managed.Mode = CipherMode.ECB;
                managed.Padding = PaddingMode.Zeros;
                ICryptoTransform transform = managed.CreateDecryptor(managed.Key, managed.IV);
                stream = new MemoryStream(cipherText);
                stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
                reader = new StreamReader(stream2);
                str = reader.ReadToEnd();
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (stream2 != null)
                {
                    stream2.Close();
                }
                if (stream != null)
                {
                    stream.Close();
                }
                if (managed != null)
                {
                    managed.Clear();
                }
            }
            return str;
        }

        public static string EncodePassword(string key, string password)
        {
            string str = password;
            if (key.Length > 0)
            {
                HMACMD5 hmacmd = new HMACMD5();
                hmacmd.Key = HexToByte(key);
                str = Convert.ToBase64String(hmacmd.ComputeHash(Encoding.Unicode.GetBytes(password)));
            }
            return str;
        }

        public static string Encrypt(string Key, string StringToEncrypt)
        {
            return encryptToString_AES(Key, StringToEncrypt);
        }

        public static string encryptToString_AES(string Key, string StringToEncrypt)
        {
            byte[] inputBuffer = ArrayToPaddingZero(StrToChars(StringToEncrypt));
            RijndaelManaged managed = new RijndaelManaged();
            managed.Key = Encoding.UTF8.GetBytes(Key);
            managed.Mode = CipherMode.ECB;
            managed.Padding = PaddingMode.Zeros;
            return toHexString(managed.CreateEncryptor(managed.Key, managed.IV).TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
        }

        private static byte[] HexStringToByteArray(string hex)
        {
            byte[] buffer = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
            {
                buffer[i / 2] = byte.Parse(hex.Substring(i, 2), NumberStyles.HexNumber);
            }
            return buffer;
        }

        private static byte[] HexToByte(string hexString)
        {
            byte[] buffer = new byte[hexString.Length / 2];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 0x10);
            }
            return buffer;
        }

        public static byte[] MD5HashUserPassword(string PlainTextPassword)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            UTF8Encoding encoding = new UTF8Encoding();
            return provider.ComputeHash(encoding.GetBytes(PlainTextPassword));
        }

        private static byte[] StringToByteArray(string input)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            return encoding.GetBytes(input);
        }

        public static byte[] StrToChars(string str)
        {
            byte[] buffer = new byte[str.Length];
            for (int i = 0; i < str.Length; i++)
            {
                buffer[i] = Convert.ToByte(str[i]);
            }
            return buffer;
        }

        public static string toHexString(byte[] block)
        {
            StringBuilder buf = new StringBuilder();
            int length = block.Length;
            for (int i = 0; i < length; i++)
            {
                byte2Hex(block[i], buf);
            }
            return buf.ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

