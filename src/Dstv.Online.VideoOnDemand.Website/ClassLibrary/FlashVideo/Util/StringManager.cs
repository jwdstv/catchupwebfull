﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo.Util
{
    /// <summary>
    /// This Class Is Used to manipulate strings.
    /// </summary>
    public class StringManager
    {
        /// <summary>
        /// Trims the abstract.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="Length">The length.</param>
        /// <param name="itemId">The item id.</param>
        /// <param name="suffix">The suffix.</param>
        /// <returns></returns>
        public static string TrimAbstract(object obj, int Length, string itemId, string suffix)
        {
            System.Guid guid = System.Guid.NewGuid();
            itemId = guid.ToString() + itemId;

            string Abstract = "";
            StringBuilder tmpAbstract = new StringBuilder();
            if (obj != null) { Abstract = obj.ToString().Trim(); }

            if (Abstract.Length > Length)
            {
                tmpAbstract.Append("<div id=\"AbstractLong_" + itemId + suffix + "\" class=\"item_moreOL\" style=\"display:none;\" onmouseout=\"showLess('" + itemId + suffix + "')\" onmouseover=\"showMore('" + itemId + suffix + "')\"><span class=\"browntxt\">" + Abstract + "</span></div>");//
                tmpAbstract.Append("<div id=\"AbstractShort_" + itemId + suffix + "\" onmouseover=\"showMore('" + itemId + suffix + "')\" onmouseout=\"showLess('" + itemId + suffix + "')\">"); //
                tmpAbstract.Append("<span class=\"browntxt\">" + Abstract.Substring(0, Length) + " ...</span></div>");
            }
            else
            {
                tmpAbstract.Append("<span class=\"browntxt\">" + Abstract + "</span>");
            }

            return tmpAbstract.ToString();
        }

        
        /// <summary>
        /// Gets the air date time. in the format eg Tue, 15 Jan '10
        /// </summary>
        /// <param name="theDate">The date to convert into special format.</param>
        /// <returns></returns>
        private static string GetAirDateTime(DateTime theDate)
        {

            string shortDate = theDate.ToString("ddd, dd MMM") + " '" + theDate.ToString("yy");


            return shortDate;// +" | " + theDate.ToShortTimeString();
        }

        /// <summary>
        /// Gets the short date. format eg. 01 Jan '10
        /// </summary>
        /// <param name="theDate">The date.</param>
        /// <returns></returns>
        public static string GetShortDate(DateTime theDate)
        {
            return theDate.ToString("dd MMM") + " '" + theDate.ToString("yy");

        }

        /// <summary>
        /// Gets the video title. Used to cater for emptyTitle
        /// </summary>
        /// <param name="theDate">The date.</param>
        /// <returns></returns>
        public static string GetVideoTitle(string title, string defaultMSG)
        {
            string resultTitle = defaultMSG;
            if(!string.IsNullOrEmpty(title))
            {
                resultTitle = title;
            }


            return resultTitle;

        }

        /// <summary>
        /// Gets the program show image.
        /// </summary>
        /// <param name="showId">The show id.</param>
        /// <param name="imgUrl">The img URL.</param>
        /// <param name="programName">Name of the program.</param>
        /// <param name="widthAndHeight">Height of the width and.</param>
        /// <returns></returns>
        public static string GetProgramShowImage(int showId, string imgUrl, string programName, string widthAndHeight)
        {

            if (string.IsNullOrEmpty(imgUrl))
                imgUrl = ConfigValues.ApplicationPath + "Images/NoImage/ondemandthumb.jpg";
            else
            {
                imgUrl = ConfigValues.ImageRoot + imgUrl;
            }


            if (string.IsNullOrEmpty(programName))
                programName = "";

            if (imgUrl.Length > 0)
                imgUrl = "<span>&nbsp;</span><img border=\"0\" alt=\"View Video for " + programName + "\" " + widthAndHeight + " src=\"" + imgUrl + "\"/>";


            return imgUrl;

        }

        /// <summary>
        /// Gets the link URL that determines where a video link needs to go
        /// </summary>
        /// <param name="vid">The vid.</param>
        /// <param name="playUrl">The play URL.</param>
        /// <returns></returns>
        public static string GetLinkURL(string vid, string playUrl)
        {
            
            string linkUrl = string.Empty;
            //if PlayURL Is Not Specified
            //Try And Get it from webConfig
            if (playUrl == null)
                playUrl = Util.ConfigValues.PlayURL;
            //If It's Still Null. Then make it an empty string so that the default of OnDemandAPI.Util.ConfigValues.ApplicationPath + "Movies/Video/?vid=" + vid; will apply
            if (playUrl == null)
                playUrl = string.Empty;

            if (playUrl.ToLower().Contains("[vid]"))
            {
                linkUrl = playUrl.ToLower().Replace("[vid]", vid);
            }
            else
            {
                linkUrl = Util.ConfigValues.ApplicationPath + "Movies/Video/?vid=" + vid;
            }

            return linkUrl;
        }

        /// <summary>
        /// Gets the footer. This gets the string that represents the pager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="psize">The psize.</param>
        /// <param name="currPage">The curr page.</param>
        /// <param name="totPages">The tot pages.</param>
        /// <param name="arrRowList">The arr row list.</param>
        /// <param name="sPageWindowSize">Size of the s page window.</param>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public static string GetFooter<T>(int psize, int currPage, int totPages, List<T> arrRowList, int sPageWindowSize, string url, bool showRowInfo)
        {
            //url += "&Page=";
            

            string footer = "";


            if (currPage == 0)
                currPage = 1;



            decimal tRow = arrRowList.Count;
            decimal ps = psize;
            decimal max = 0;
            if (psize > 0)
                max = Math.Ceiling((tRow / ps));

            if (currPage == -1)
            {
                currPage = Convert.ToInt32(max);
            }

            footer = "";//"<div>";
            int PageIndex = currPage;
            int PageWindow = sPageWindowSize;
            //int PageCount = totPages;
            string Pages = "";
            string Previous = "";
            string Next = "";

            decimal t = totPages;
            decimal p = psize;

            int PageCount = 0; // Convert.ToInt32( Math.Ceiling((t / p)));           
            if (psize > 0)
                PageCount = Convert.ToInt32(Math.Ceiling((t / p)));

            int s = Math.Max(1, Math.Min(Math.Max(1, PageIndex - (PageWindow >> 1)), PageCount - PageWindow + 1));
            int els = Math.Min(s + PageWindow - 1, PageCount);

            //Pages.Text = "";

            if (PageIndex > 1)
            {
                Previous = "<a class=\"page_down\" href=\"" + url.Replace("[page]",(PageIndex - 1).ToString()) + "\">" + "&lt;" + "</a> ";
            }
            else
            {
                Previous = "";// "<img src=\"images/but_paging_back.gif\" alt=\"Back\" border=\"0\" />";
            }

            if (PageIndex < PageCount)
            {
                Next = "<a class=\"page_down\" href=\"" + url.Replace("[page]", (PageIndex + 1).ToString()) + "\">&gt;</a> ";
            }
            else
            {
                Next = "";// "<img src=\"images/but_paging_next.gif\" alt=\"Next\" border=\"0\" />";
            }

            if (s > 1)
            {
                Pages += "<a class=\"page_down\" href=\"" + url.Replace("[page]", (1).ToString()) + "\">" + "1" + "</a> ... ";
            }
            for (int i = s; i < els; ++i)
            {
                if (i == PageIndex)
                    Pages += "<span class=\"page_up\">" + i.ToString() + "</span>" + " ";//(i.ToString()) + " "; 
                else
                    Pages += "<a class=\"page_down\" href=\"" + url.Replace("[page]" ,(i).ToString()) + "\">" + (i.ToString()) + "</a> ";


            }
            if (PageIndex == els && PageIndex == 1)
            {
                Pages += "";//"<span class=\"page_up\">" + els.ToString() + "</span>" + " ";
            }
            else if (PageIndex == els)
            {
                Pages += "<span class=\"page_up\">" + els.ToString() + "</span>" + " ";//sPagerSelectedStyle.Replace("[Page]", els.ToString()) + " ";
            }
            else if (els < 1)
            {
                Pages = "";
            }
            else
            {
                Pages += "<a class=\"page_down\" href=\"" + url.Replace("[page]", (els).ToString()) + "\">" + (els.ToString()) + "</a> ";
            }
            if (els < PageCount)
            {
                Pages += " ... <a class=\"page_down\" href=\"" + url.Replace("[page]", (PageCount).ToString()) + "\">" + (PageCount.ToString()) + "</a> ";
            }

            if (PageIndex < PageCount)
            {
                Next = "<a class=\"page_down\" href=\"" + url.Replace("[page]", (PageIndex + 1).ToString()) + "\">" + "&gt;" + "</a> ";
            }
            else
            {
                Next = "";//"<img src=\"images/but_paging_next.gif\" alt=\"Next\" border=\"0\" /> ";
            }

            //footer += Previous + Pages + Next;

            int _start = (currPage - 1) * psize;
            if (currPage == -1)
            {
                //get last row i need to get 53
                decimal _tRow = arrRowList.Count - 1;
                decimal _ps = psize;
                decimal _max = 0;// Math.Floor((_tRow / _ps));
                if (psize > 0)
                    _max = Math.Floor((_tRow / _ps));

                _start = Convert.ToInt32((_max)) * psize;
            }

            int _end = _start + psize;

            if (_end > arrRowList.Count)
            {
                _end = arrRowList.Count;
            }
            _start++;

            //check to see if one needs to show the row info
            if (showRowInfo)
            {
                if (tRow == _end && tRow < psize)
                    footer += "Showing 1 of 1";
                else if (tRow > 1)
                    footer += "Showing " + _start.ToString() + " to " + _end.ToString() + " of " + tRow.ToString() + "&nbsp;&nbsp; | " + Previous + Pages + Next;
                else if (tRow == 1)
                    footer += "Showing 1 of 1";
            }
            else
            {
                if (tRow == _end && tRow < psize)
                    footer += "Showing 1 of 1";
                else if (tRow > 1)
                    footer += Previous + Pages + Next;
                else if (tRow == 1)
                    footer += "Showing 1 of 1";
            }

            

            return footer;
        }

    }
}
