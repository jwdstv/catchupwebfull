﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo.Util
{
    /// <summary>
    /// Used to get configuration values.
    /// Will either get the values from the sites web config file or a custom "OnDemandAPISettings.xml" file
    /// </summary>
    public class ConfigValues
    {
        /// <summary>
        /// Gets the application path of the current apllication. used to resolve parth issues.
        /// </summary>
        /// <value>The application path.</value>
        public static string ApplicationPath
        {
            get
            {
                
                ////If application path is not defined on web.config file it will look at the virtual path of the application
                //string APP_PATH = ConfigurationManager.AppSettings["ApplicationPath"];
                //if (APP_PATH == null)
                //{
                   string APP_PATH = System.Web.HttpContext.Current.Request.ApplicationPath.ToLower();
                    if (APP_PATH == "/")      //a site
                        APP_PATH = "/";
                    else if (!APP_PATH.EndsWith(@"/")) //a virtual
                        APP_PATH += @"/";

                    
                //}

                return APP_PATH;
            }
        }
        
        /// <summary>
        /// Gets a value indicating whether to enable the proxy when making calls to the MMS Service.
        /// </summary>
        /// <value><c>true</c> if [use proxy]; otherwise, <c>false</c>.</value>
        public static bool UseProxy
        {
            get
            {
                bool useProxy = false;
                try
                {
                    useProxy = Convert.ToBoolean(ConfigurationManager.AppSettings["UseProxy"]);
                }
                catch { }

                return useProxy;
            }
        }

        /// <summary>
        /// Gets the authentication key. This key is used to bring up the correct videos for a given site
        /// </summary>
        /// <value>The authentication key.</value>
        public static string AuthenticationKey
        {
            get 
            {
                string authKey = string.Empty;

                try
                {
                    authKey = ConfigurationManager.AppSettings["AuthenticationKey"];
                }
                catch{}
                return authKey;
            }
        }

        /// <summary>
        /// Gets the authentication key. This key is used to bring up the correct videos for a given site
        /// </summary>
        /// <value>The authentication key.</value>
        public static string FreeVideoAuthenticationKey
        {
            get
            {
                string authKey = null;

                try
                {
                    authKey = ConfigurationManager.AppSettings["FreeVideoAuthenticationKey"];
                }
                catch { }
                if (string.IsNullOrEmpty(authKey))
                    authKey = "47A16784-18D9-49CE-887E-FF92DEFE9B75";

                return authKey;
            }
        }

        /// <summary>
        /// Gets the image root. This is the URL of where images are stored
        /// </summary>
        /// <value>The image root.</value>
        public static string ImageRoot
        {
            get { return ConfigurationManager.AppSettings["ImageRoot"]; }
        }

        /// <summary>
        /// Gets the play URL from the web config file. 
        /// Play URL Is used to redirect the listing control to the correct place. 
        /// </summary>
        /// <value>The play URL.</value>
        public static string PlayURL
        {
            get 
            {
                string url = string.Empty;
                try
                {
                    url = GetURL(ConfigurationManager.AppSettings["PlayURL"]);
                }
                catch { }

                return url;
            }
        }


        /// <summary>
        /// Gets the URL. Replaces the tilde with the application path
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        internal static string GetURL(string url)
        {            
            if (url.Contains("~/"))
            {
                url = url.Replace("~/", Util.ConfigValues.ApplicationPath);
            }

            return url;
        }

        /// <summary>
        /// Gets the control image root. Images for the Listing Control
        /// </summary>
        /// <value>The control image root.</value>
        public static string ControlImageRoot
        {
            get 
            {
                //setting the default value
                string imageRoot = ApplicationPath + "app_themes/global/images/";
                try
                {
                    imageRoot = GetURL(ConfigurationManager.AppSettings["ControlImageRoot"]);
                }
                catch { }

                return imageRoot;
            }
        }

        /// <summary>
        /// Gets the show ID.
        /// </summary>
        /// <value>The show ID.</value>
        public static int ShowID
        {
            get
            {
                //default to zero
                int showID = int.TryParse(ConfigurationManager.AppSettings["ShowID"], out showID) ? showID : 0;

                return showID;

            }
        }

        public static string QueryStringJoiner
        {
            get
            {
                string queryStringJoiner = "&";
                try
                {
                    queryStringJoiner = GetURL(ConfigurationManager.AppSettings["QueryStringJoiner"]);
                }
                catch { }
                return queryStringJoiner;
            }
            
        }

        public static string QueryStringEquals
        {
            get
            {
                string queryStringEquals = "=";
                try
                {
                    queryStringEquals = ConfigurationManager.AppSettings["QueryStringEquals"];
                }
                catch { }
                return queryStringEquals;
            }

        }

        /// <summary>
        /// Gets the video not found message. Default message to display if no videos are found
        /// </summary>
        /// <value>The video not found message.</value>
        public static string VideoNotFoundMessage
        {
            get 
            {
                string msg = "Sorry, no videos were found for your current selection.";
                try
                {
                    msg = ConfigurationManager.AppSettings["VideoNotFoundMessage"];
                }
                catch { }

                return msg;
            }
        }

        /// <summary>
        /// Gets the default image.
        /// </summary>
        /// <value>The default image.</value>
        public static string DefaultImage
        {
            get
            {
                string imgUrl = "http://ondemand.dstv.com/images/noimage/ondemandthumb.jpg";
                try
                {
                    imgUrl = ConfigurationManager.AppSettings["DefaultImage"];
                }
                catch { }

                return imgUrl;
            }
        }

        public static bool EnableNonFreeContent
        {
            get
            {
                bool enableFree = false;
                try
                {
                    enableFree = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableNonFreeContent"]);
                }
                catch { }

                return enableFree;
            }
        }

        /// <summary>
        /// Gets the player feed URL. Feed for The player
        /// </summary>
        /// <value>The player feed URL.</value>
        public static string PlayerFeedURL
        {
            get
            {
                string playerFeedURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri; // "http://" + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + System.Web.HttpContext.Current.Request.RawUrl.ToString();// System.Web.HttpContext.Current.Request.RawUrl;
                try
                {
                    playerFeedURL = GetURL(ConfigurationManager.AppSettings["PlayerFeedURL"]);
                }
                catch { }

                return playerFeedURL;
            }
        }

        public static string PlayerAssetURL
        {
            get 
            {
                string playerAssetURL = "http://core.dstv.com/video/flash/assets/embed_assets.swf";
                try
                {
                    playerAssetURL = GetURL(ConfigurationManager.AppSettings["PlayerAssetURL"]);
                }
                catch { }

                return playerAssetURL;
            }
        }

        /// <summary>
        /// Gets the player SWF File URL URL. The file responsible for playing the video
        /// </summary>
        /// <value>The player SWF URL.</value>
        public static string PlayerSwfURL
        {
            get 
            {
                string playerSwfURL = "http://ondemand.dstv.com/Flash/dstv_drmvideoplayer.swf";
                try
                {
                    playerSwfURL = GetURL(ConfigurationManager.AppSettings["PlayerSwfURL"]);
                }
                catch { }

                return playerSwfURL;
            }
        }

        public static string CryptoKey
        {
            get
            {
                string cryptoKey = "1233901199002223000111A2";
                try
                {
                    cryptoKey = ConfigurationManager.AppSettings["CryptoKey"];
                }
                catch
                { }
                return cryptoKey;
            }
        }

        /// <summary>
        /// Gets the category value from web.config. This is used for tracking purposes
        /// </summary>
        /// <value>The category value.</value>
        public static string CategoryValue
        {
            get
            {
                string cat = string.Empty;
                try
                {
                    cat = ConfigurationManager.AppSettings["categoryValue"];
                }
                catch { }
                if (string.IsNullOrEmpty(cat))
                    cat = string.Empty;


                return cat;
            }
        }

        /// <summary>
        /// Gets the addvert category ID.
        /// </summary>
        /// <value>The addvert category ID.</value>
        public static int AddvertCategoryID
        {
            get
            {
                int catID = 0;
                //try
                //{
                //    catID = int.TryParse(ConfigurationManager.AppSettings["AddvertCategoryID"], out catID) ? catID : 0;
                //}
                //catch { }
                string rawCatId = ConfigurationManager.AppSettings["AddvertCategoryID"];
                if (!string.IsNullOrEmpty(rawCatId))//if a cat is specified
                {
                    if (rawCatId == "*")//* for all
                    {
                        catID = -1;
                    }
                    else 
                    {
                        try
                        {
                            catID = int.TryParse(rawCatId, out catID) ? catID : 0;
                        }
                        catch { }
                    }
                }

                return catID;
            }
        }

        //AddvertPreRollPath
        /// <summary>
        /// Gets the addvert pre roll path.
        /// </summary>
        /// <value>The addvert pre roll path.</value>
        public static string AddvertPreRollPath
        {
            get
            {
                string prepath = string.Empty;
                try
                {
                    prepath = ConfigurationManager.AppSettings["AddvertPreRollPath"];
                }
                catch { }
                if (string.IsNullOrEmpty(prepath))
                    prepath = string.Empty;

                return prepath;
            }
        }

        //AddvertPostRollPath
        /// <summary>
        /// Gets the addvert post roll path.
        /// </summary>
        /// <value>The addvert post roll path.</value>
        public static string AddvertPostRollPath
        {
            get
            {
                string postpath = string.Empty;
                try
                {
                    postpath = ConfigurationManager.AppSettings["AddvertPostRollPath"];
                }
                catch { }
                if (string.IsNullOrEmpty(postpath))
                    postpath = string.Empty;

                return postpath;
            }
        }

        /// <summary>
        /// Gets the related video limit. If the value is not found on the web config file it defaults to 10
        /// Desmond Nzuza
        /// 2010 - Sep - 06
        /// </summary>
        /// <value>The related video limit. int</value>
        public static int RelatedVideoLimit
        {
            get
            {
                int limit = 10;
                try
                {
                    limit = Convert.ToInt32(ConfigurationManager.AppSettings["RelatedVideoLimit"]);
                }
                catch { }                

                return limit;
            }
        }

    }
}
