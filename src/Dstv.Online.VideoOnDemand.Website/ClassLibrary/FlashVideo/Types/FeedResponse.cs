﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo.Types
{
    /// <summary>
    /// this type is returned to the user when the user calls uses the videoFeed class.
    /// </summary>
    public class FeedResponse
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is successful.
        /// if the videofeed class was successful in generating the video feed, this will be set to true
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is successful; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccessful { get; set; }
        /// <summary>
        /// Gets or sets the jason feed. this is the object returned as a result of the video call
        /// </summary>
        /// <value>The jason feed.</value>
        public JsonFeed JasonFeed { get; set; }
        /// <summary>
        /// Gets or sets the jason feed string. this is the string representation of the video feed
        /// </summary>
        /// <value>The jason feed string.</value>
        public string JasonFeedString { get; set; }
        /// <summary>
        /// Gets or sets the error message. this will contain messaging for any errors that may occur when building the video feed
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage {get; set;}

        public FeedResponse()
        {
            this.IsSuccessful = false;
        }
    }
}