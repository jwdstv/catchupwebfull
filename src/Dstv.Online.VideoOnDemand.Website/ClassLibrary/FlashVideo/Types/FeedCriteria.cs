﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.FlashVideo.Types
{
    public class FeedCriteria
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance can play. after the user has been verified. this falue can be set to true
        /// </summary>
        /// <value><c>true</c> if this instance can play; otherwise, <c>false</c>.</value>
        public bool CanPlay {get; set;}


        /// <summary>
        /// Gets or sets the product id. this is the product that the user will have to belong to, in order for the data to be valid
        /// </summary>
        /// <value>The product id.</value>
        public Transactions.ProductTypeIdentifier ProductId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show embed code]. setting this to true will allow the user to copy the embed code that comes with the video player
        /// </summary>
        /// <value><c>true</c> if [show embed code]; otherwise, <c>false</c>.</value>
        public bool ShowEmbedCode {get; set;}
        /// <summary>
        /// Represents the url that the player will be played on. eg. http://localhost:62627/play.aspx?vid=[vid]. 
        /// Must include [vid] which will be substituted with the video ID
        /// </summary>
        /// <value>The playe URL.</value>
        public string PlayeUrl { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether preroll and postroll ads should be displayed.
        /// </summary>
        /// <value><c>true</c> if [show ads]; otherwise, <c>false</c>.</value>
        public bool ShowAds {get; set;}
        /// <summary>
        /// Gets or sets the post roll tag. eg . http://ad.za.doubleclick.net/pfadx/P4306.dstv.com/mnetafrica/naija;tile=1;sz=536x400;kw=postroll;dcmt=text/xml;ord=987654321?
        /// this is the video advert to be played after the vodeo ends
        /// </summary>
        /// <value>The post roll tag.</value>
        public string PostRollTag { get; set; }
        /// <summary>
        /// Gets or sets the pre rol L ad tag. eg. http://ad.za.doubleclick.net/pfadx/P4306.dstv.com/mnetafrica/naija;tile=1;sz=536x400;kw=preroll;dcmt=text/xml;ord=987654321?
        /// this is the video advert to be played before the vodeo starts
        /// </summary>
        /// <value>The pre rol L ad tag.</value>
        public string PreRolLAdTag { get; set; }

        /// <summary>
        /// Gets or sets the tracking identifier. this is the category ID entry that will be used for tracking purposes. eg idols, bba, boxoffice
        /// </summary>
        /// <value>The tracking identifier.</value>
        public string TrackingIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the sub tracking identifier. this is the sub category that will need to be used for tracking purposes. eg WoodenMike, FeedVideo, Movies
        /// </summary>
        /// <value>The sub tracking identifier.</value>
        public string SubTrackingIdentifier { get; set; }



        public ProductTypes VideoProdtype { get; set; }




        /// <summary>
        /// Initializes a new instance of the <see cref="FeedCriteria"/> class.
        /// </summary>
        /// <param name="canPlay">if set to <c>true</c> the feed will allow the user to play the video content.</param>
        public FeedCriteria(bool canPlay, Transactions.ProductTypeIdentifier product)
        {
            this.CanPlay = canPlay;
            this.PlayeUrl = Util.ConfigValues.PlayURL;//if the user does not set the play URL the control will use default values from the utils class
            this.ProductId = product;
        }
    }
}