﻿using System;
using System.Web;
using System.Configuration;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    public class BaseControl : System.Web.UI.UserControl
    {
        public bool HideBoxOffice = true;

        public string SiteRoot = "";
        /// <summary>
        /// Set the pre initialize method for all pages to check if the 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       protected override void OnInit(EventArgs e){
            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HideBoxOffice"]))
                {
                    HideBoxOffice = Convert.ToBoolean(ConfigurationManager.AppSettings["HideBoxOffice"]);
                }
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteRoot"]))
                {
                    SiteRoot = ConfigurationManager.AppSettings["SiteRoot"];
                }
            }
            catch
            {

            }
            base.OnInit(e);
        }
    }
}