﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    public class EncryptionHelper
    {

        public static string PASSWORD_SALT = "d5tv0nl1n3";

        /// <summary> 
        /// Encrypt the data 
        /// </summary> 
        /// <param name="input">String to encrypt</param>
        /// <param name="password">the salt to encrypt the password with</param>
        /// <returns>Encrypted string</returns> 
        public static string Encrypt(string input, string password)
        {
            byte[] utfData = Encoding.UTF8.GetBytes(input);
            byte[] saltBytes = Encoding.UTF8.GetBytes(password);
            string encryptedString = string.Empty;
            using (var aes = new AesManaged())
            {
                var rfc = new Rfc2898DeriveBytes(password, saltBytes);

                aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
                aes.KeySize = aes.LegalKeySizes[0].MaxSize;
                aes.Key = rfc.GetBytes(aes.KeySize/8);
                aes.IV = rfc.GetBytes(aes.BlockSize/8);

                using (ICryptoTransform encryptTransform = aes.CreateEncryptor())
                {
                    using (var encryptedStream = new MemoryStream())
                    {
                        using (var encryptor =
                            new CryptoStream(encryptedStream, encryptTransform, CryptoStreamMode.Write))
                        {
                            encryptor.Write(utfData, 0, utfData.Length);
                            encryptor.Flush();
                            encryptor.Close();

                            byte[] encryptBytes = encryptedStream.ToArray();
                            encryptedString = Convert.ToBase64String(encryptBytes);
                        }
                    }
                }
            }
            return encryptedString;
        }

        public static string Encrypt(string input)
        {
            return Encrypt(input, PASSWORD_SALT);
        }


        /// <summary> 
        /// Decrypt a string 
        /// </summary> 
        /// <param name="input">Input string in base 64 format</param>
        /// <param name="password">the salt to decrypt the password with</param>
        /// <returns>Decrypted string</returns> 
        public static string Decrypt(string input, string password)
        {
            byte[] encryptedBytes = Convert.FromBase64String(input);
            byte[] saltBytes = Encoding.UTF8.GetBytes(password);
            string decryptedString = string.Empty;
            using (var aes = new AesManaged())
            {
                var rfc = new Rfc2898DeriveBytes(password, saltBytes);
                aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
                aes.KeySize = aes.LegalKeySizes[0].MaxSize;
                aes.Key = rfc.GetBytes(aes.KeySize/8);
                aes.IV = rfc.GetBytes(aes.BlockSize/8);

                using (ICryptoTransform decryptTransform = aes.CreateDecryptor())
                {
                    using (var decryptedStream = new MemoryStream())
                    {
                        var decryptor =
                            new CryptoStream(decryptedStream, decryptTransform, CryptoStreamMode.Write);
                        decryptor.Write(encryptedBytes, 0, encryptedBytes.Length);
                        decryptor.Flush();
                        decryptor.Close();

                        byte[] decryptBytes = decryptedStream.ToArray();
                        decryptedString =
                            Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
                    }
                }
            }

            return decryptedString;
        }

        public static string Decrypt(string input)
        {
            return Decrypt(input, PASSWORD_SALT);
        }

    }
}