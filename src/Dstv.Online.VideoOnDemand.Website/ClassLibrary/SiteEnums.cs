﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
	public partial class SiteEnums
	{
        #region public enum CacheProvider
        /// <summary>
        ///
        /// </summary>
        public enum CacheProvider
        {
            None,
            MemCache,
            ASPNetCache
        }
        #endregion

		#region public enum ProductType
		/// <summary>
		/// 
		/// </summary>
		public enum ProductType
		{
			BoxOffice,
			CatchUp,
			OpenTime,
			OnDemand,
			DStv
		}
		#endregion


		#region public enum SupportedBrowserState
		/// <summary>
		/// 
		/// </summary>
		public enum SupportedBrowserState
		{
			Unknown,
			SupportedBrowser,
			UnsupportedBrowser,
			UnsupportedBrowserAndUserAccepted
		}
		#endregion


		#region public enum VideoTextAlign
		/// <summary>
		/// 
		/// </summary>
		public enum VideoTextAlign
		{
			Bottom,
			Right
		}
		#endregion


		#region public enum VideoRuntimeDisplay
		/// <summary>
		/// 
		/// </summary>
		public enum VideoRuntimeDisplay
		{
			RuntimeInMinutes,
			Duration
		}
		#endregion


		#region public enum VideoSummaryDisplay
		/// <summary>
		/// 
		/// </summary>
		public enum VideoSummaryDisplay
		{
			VideoLayoutTextRight,
			VideoLayoutTextBottom,
			VideoLayoutTextBottom2,
			EpisodeLayout,
			ShowLayout,
			Unknown
		}
		#endregion

		public enum VideoListingDisplay
		{
			HorizontalSummary,
			VerticalSummary,
			VerticalList
		}

		#region public enum VideoConsoleDisplay
		/// <summary>
		/// 
		/// </summary>
		public enum VideoConsoleDisplay
		{
			Video,
			Show
		}
		#endregion


		#region public enum MediaDisplayActionType
		/// <summary>
		/// 
		/// </summary>
		public enum MediaDisplayActionType
		{
			Unknown,
			DisplayTrailerImage,
			DisplayEPKImage,
			DisplayVideoImage,
			PlayTrailer,
			PlayEPK,
			PlayVideo,
			DownloadVideo,
			PayForVideo
		}
		#endregion


		#region public enum VideoSortOrder
		/// <summary>
		/// 
		/// </summary>
		public enum VideoSortOrder
		{
			SearchResultOrder,
			LatestReleases,
			LastChance,
			ForthcomingReleases,
			Title,
			MostPopular,
			ShowsByTitle, //Only returns 1st video for program (show)
			ShowsByLatestVideo,
			ProgramName, //Returns all videos for program
			None
		}
		#endregion


		#region public enum VideoGalleryDisplayType
		/// <summary>
		/// 
		/// </summary>
		public enum VideoGalleryDisplayType
		{
			VideoCategory,
			VideoEditorial,
			VideoSearch,
			VideoShows
		}
		#endregion


		#region public enum VideoDataType
		/// <summary>
		/// 
		/// </summary>
		public enum VideoDataType
		{
			List,
			Summary,
			Gallery,
			Search,
			Detail,
			ClassificationList,
			ClassificationHierarchy
		}
		#endregion


		#region public enum VideoClassificationType
		/// <summary>
		/// 
		/// </summary>
		public enum VideoClassificationType
		{
			ProgramType_All,
			ProgramType_Type,
			ProgramType_Genre,
			Item_Editorial,
			Item_TvShows,
			Item_TvChannels,
			Item_Categories,
			Item_Link,
			Unknown
		}
		#endregion


		#region public sealed class Mapper
		/// <summary>
		/// Class that provides methods for mapping one type of enum to another, including base enums.
		/// </summary>
		public sealed class Mapper
		{

			#region public static SiteEnums.VideoSortOrder MapBaseSortOrderToVideoSortOrder ( SortOrders sortOrder )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="sortOrder"></param>
			/// <returns></returns>
			public static SiteEnums.VideoSortOrder MapBaseSortOrderToVideoSortOrder ( SortOrders sortOrder )
			{
				switch (sortOrder)
				{
					case SortOrders.LastChance:
						return SiteEnums.VideoSortOrder.LastChance;
					case SortOrders.ProgramTitleAsc:
						return SiteEnums.VideoSortOrder.Title;
                    case SortOrders.LatestReleases:
						return SiteEnums.VideoSortOrder.LatestReleases;
                    case SortOrders.ForthcomingReleases:
						return SiteEnums.VideoSortOrder.ForthcomingReleases;
					case SortOrders.MostPopular:
						return SiteEnums.VideoSortOrder.MostPopular;
					case SortOrders.Ordinal:
						return VideoSortOrder.SearchResultOrder;
					case SortOrders.ByShowProgramTitleAsc:
						return VideoSortOrder.ShowsByTitle;
					case SortOrders.ByShowVideoStartDateDesc:
						return VideoSortOrder.ShowsByLatestVideo;
                    case SortOrders.ByShowProgramTitleAllVideosAsc:
						return SiteEnums.VideoSortOrder.ProgramName;
					default:
						return SiteEnums.VideoSortOrder.None;
				}
			}
			#endregion


			#region public static SortOrders MapVideoSortOrderToBaseSortOrder ( SiteEnums.VideoSortOrder videoSortOrder )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="videoSortOrder"></param>
			/// <returns></returns>
			public static SortOrders MapVideoSortOrderToBaseSortOrder ( SiteEnums.VideoSortOrder videoSortOrder )
			{
				switch (videoSortOrder)
				{
					case SiteEnums.VideoSortOrder.ForthcomingReleases:
                        return SortOrders.ForthcomingReleases;
					case SiteEnums.VideoSortOrder.LastChance:
						return SortOrders.LastChance;
					case SiteEnums.VideoSortOrder.LatestReleases:
                        return SortOrders.LatestReleases;
					case SiteEnums.VideoSortOrder.Title:
						return SortOrders.ProgramTitleAsc;
					case SiteEnums.VideoSortOrder.MostPopular:
						return SortOrders.MostPopular;
					case VideoSortOrder.SearchResultOrder:
						return SortOrders.Ordinal;
					case SiteEnums.VideoSortOrder.ShowsByTitle:
						return SortOrders.ByShowProgramTitleAsc;
					case SiteEnums.VideoSortOrder.ShowsByLatestVideo:
						return SortOrders.ByShowVideoStartDateDesc;
					case SiteEnums.VideoSortOrder.ProgramName:
						return SortOrders.ByShowProgramTitleAllVideosAsc;
					default:
						return SortOrders.Ordinal;
				}
			}
			#endregion


			#region public static VideoClassificationType MapBaseCatalogueItemClassificationTypeToVideoClassificationType ( CatalogueItemClassificationTypes classificationType )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="classificationType"></param>
			/// <returns></returns>
			public static VideoClassificationType MapBaseCatalogueItemClassificationTypeToVideoClassificationType ( CatalogueItemClassificationTypes catalogueItemClassificationType )
			{
				switch (catalogueItemClassificationType)
				{
					case CatalogueItemClassificationTypes.All:
						return VideoClassificationType.ProgramType_All;
					case CatalogueItemClassificationTypes.Categories:
						return VideoClassificationType.Item_Categories;
					case CatalogueItemClassificationTypes.Genre:
						return VideoClassificationType.ProgramType_Genre;
					case CatalogueItemClassificationTypes.ProgramType:
						return VideoClassificationType.ProgramType_Type;
					case CatalogueItemClassificationTypes.TvChannels:
						return VideoClassificationType.Item_TvChannels;
					case CatalogueItemClassificationTypes.TvShows:
						return VideoClassificationType.Item_TvShows;
					default:
						return VideoClassificationType.Unknown;
				}
			}
			#endregion


			#region public static CatalogueItemClassificationTypes MapVideoClassificationTypeToBaseCatalogueItemClassificationType ( VideoClassificationType videoClassificationType )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="videoClassificationType"></param>
			/// <returns></returns>
			public static CatalogueItemClassificationTypes MapVideoClassificationTypeToBaseCatalogueItemClassificationType ( VideoClassificationType videoClassificationType )
			{
				switch (videoClassificationType)
				{
					case VideoClassificationType.Item_Categories:
						return CatalogueItemClassificationTypes.Categories;
					case VideoClassificationType.Item_TvChannels:
						return CatalogueItemClassificationTypes.TvChannels;
					case VideoClassificationType.Item_TvShows:
						return CatalogueItemClassificationTypes.TvShows;
					case VideoClassificationType.ProgramType_All:
						return CatalogueItemClassificationTypes.All;
					case VideoClassificationType.ProgramType_Genre:
						return CatalogueItemClassificationTypes.Genre;
					case VideoClassificationType.ProgramType_Type:
						return CatalogueItemClassificationTypes.ProgramType;
					//case VideoClassificationType.Unknown:
					//case VideoClassificationType.Item_Editorial:
					//case VideoClassificationType.Item_Link:
					default:
						return CatalogueItemClassificationTypes.Unknown;
				}
			}
			#endregion



			#region public static ProductTypes MapSiteProductTypeToBaseProductType ( SiteEnums.ProductType siteProductType )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="siteProductType"></param>
			/// <returns></returns>
			public static ProductTypes MapSiteProductTypeToBaseProductType ( SiteEnums.ProductType siteProductType )
			{
				switch (siteProductType)
				{
					case ProductType.BoxOffice:
						return ProductTypes.BoxOffice;
					case ProductType.CatchUp:
						return ProductTypes.CatchUp;
					case ProductType.OpenTime:
						return ProductTypes.OpenTime;
					default:
						return ProductTypes.OnDemand;
				}
			}
			#endregion


		}
		#endregion

	}
}