﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dstv.Online.VideoOnDemand.Data;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    /// <summary>
    /// this class will be used to log errors to the Ondemand2010 database
    /// </summary>
    public class ErrorLogManager
    {
        
        public static void LogError(string source, string ex)
        {
            LogErrorToSQL(source, ex);
        }

        public static void LogError(string ex)
        {
            string source = string.Format("Ondeamnd IP=\"{0}\", Url=\"{1}\"", HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.Url.AbsoluteUri);
            LogErrorToSQL(source, ex);
        }

        /// <summary>
        /// Logs the error to SQL.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="errorDetails">The error details.</param>
        private static void LogErrorToSQL(string location, string errorDetails)
        {
            try
            {
                ErrorDTO errDetails = new ErrorDTO();
                errDetails.location = location;
                errDetails.errorDetail = errorDetails;
                ErrorDb.AddErrorToDB(errDetails);
                //consider sending an email to a group of developers               
            }
            catch (Exception x)
            {
                throw new Exception(string.Format("Unable To Log Error: Locarion = \"{0}\", Error Detals = \"{1}\", Exception = \"{3}\" ",location, errorDetails, x.ToString()));
            }
        }


    }

}