﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.TestData
{
	#region public class TestVideoCatalogue
	/// <summary>
	/// 
	/// </summary>
	public class TestVideoCatalogue
	{
		#region Constants for seeding

		public const int Items_BoxOfficeProgramTypes = 1;
		public const int Items_CatchUpProgramTypes = 6;
		public const int Items_OpenTimeProgramTypes = 4;
		public const int Items_BoxOfficeGenresMax = 8;
		public const int Items_CatchUpGenresMax = 5;
		public const int Items_OpenTimeGenresMax = 4;

		public const string Items_OpenTimeTVShowsId = "20DF95D6-463D-42BF-85A1-C000E2CB6ACF";
		public const string Items_OpenTimeTVChannelsId = "DE6F8D2B-067B-483D-A68F-C748D1A6F69E";
		public const int Items_OpenTimeTVShowsMax = 6;
		public const int Items_OpenTimeTVChannelsMax = 4;
		public const int Items_OpenTimeTVChannelCategoriesMax = 5;

		public const int Items_BoxOfficeVideos = 26;
		public const int Items_CatchUpVideos = 38;
		public const int Items_OpenTimeVideos = 52;

		#endregion


		public static TestVideoCatalogue CachedVideoCatalogue
		{
			get
			{
				if (HttpContext.Current.Application["CachedVideoCatalogue"] == null)
				{
					TestVideoCatalogue videoCatalogue = new TestVideoCatalogue();
					HttpContext.Current.Application["CachedVideoCatalogue"] = videoCatalogue;
				}

				return (TestVideoCatalogue)HttpContext.Current.Application["CachedVideoCatalogue"];
			}
		}

		List<VideoItemSummary> _testVideoSummaries = new List<VideoItemSummary>();

		VideoClassificationHierarchy _testVideoClassifications = new VideoClassificationHierarchy();

		public TestVideoCatalogue ()
		{
			VideoCatalogueSeedItemsDynamically();
			_testVideoClassifications.Seed();
		}


		#region private void VideoCatalogueSeedStatically ()
		/// <summary>
		/// 
		/// </summary>
		private void VideoCatalogueSeedStatically ()
		{
			// Box Office
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 1, "Video 1 (BO)", "Video 1 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 08, 31), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_aTeam.jpg", 93, "01:33:23",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 2, "Video 2 (BO)", "Video 2 (BO)", null, null, null, new DateTime(2010, 08, 20), null, new DateTime(2010, 09, 30), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_dinnerForSmucks.jpg", 121, "02:01:00",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 3, "Video 3 - with a really much longer title (BO)", "Video 3 - with a longer title (BO)", null, null, null, new DateTime(2010, 07, 15), null, new DateTime(2010, 09, 16), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_grownUp.jpg", 148, "02:28:55",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 4, "Video 4 (BO)", "Video 4 (BO)", null, null, null, new DateTime(2010, 06, 01), null, new DateTime(2010, 12, 31), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_switch.jpg", 87, "01:27:15",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 5, "Video 5 (BO)", "Video 5 (BO)", null, null, null, new DateTime(2010, 07, 01), null, new DateTime(2010, 10, 09), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_knightDay.jpg", 68, "01:08:00",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 6, "Video 6 (BO)", "Video 6 (BO)", null, null, null, new DateTime(2010, 07, 01), null, new DateTime(2010, 10, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 75, "01:15:00",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 7, "Video 7 (BO)", "Video 7 (BO)", null, null, null, new DateTime(2010, 07, 21), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 81, "01:21:23",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 8, "Video 8 (BO)", "Video 8 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 9, "Video 9 (BO)", "Video 9 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 10, "Video 10 (BO)", "Video 10 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 11, "Video 11 (BO)", "Video 11 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 12, "Video 12 (BO)", "Video 12 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 13, "Video 13 (BO)", "Video 13 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 14, "Video 14 (BO)", "Video 14 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 15, "Video 15 (BO)", "Video 15 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 16, "Video 16 (BO)", "Video 16 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 17, "Video 17 (BO)", "Video 17 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 18, "Video 18 (BO)", "Video 18 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 19, "Video 19 (BO)", "Video 19 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 20, "Video 20 (BO)", "Video 20 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 21, "Video 21 (BO)", "Video 21 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.BoxOffice, 22, "Video 22 (BO)", "Video 22 (BO)", null, null, null, new DateTime(2010, 08, 01), null, new DateTime(2010, 11, 09), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg", 94, "01:34:10",
					"Box Office video abstact here. A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home."
					)
				);

			// Catch Up
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.CatchUp, 11, "Movie 1 (CU)", "Movie 1 (CU)", null, null, null, new DateTime(2010, 05, 01), new DateTime(2010, 05, 01), new DateTime(2010, 11, 01), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_together.jpg", 93, "01:33:23",
					"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.CatchUp, 12, "Series 1 (CU)", "Series 1 (CU)", 1, "Episode Title 1", 2, new DateTime(2010, 08, 01), new DateTime(2010, 07, 25), new DateTime(2010, 11, 01), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_lost1.jpg", 44, "00:44:36",
					"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
					)
				);
			_testVideoSummaries.Add(
				new VideoItemSummary(ProductTypes.CatchUp, 13, "Series 1 (CU)", "Series 1 (CU)", 2, "Episode Title 2", 2, new DateTime(2010, 08, 07), new DateTime(2010, 08, 02), new DateTime(2010, 11, 07), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_lost2.jpg", 45, "00:45:05",
					"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
					)
				);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.CatchUp, 14, "Series 2 (CU)", "Series 2 (CU)", 3, "Episode Title 3", 1, new DateTime(2010, 08, 21), new DateTime(2010, 08, 18), new DateTime(2010, 11, 21), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_lieToMe.jpg", 39, "00:39:53",
						"Catch Up video abstract here. Short abstract for this video."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.CatchUp, 15, "Sport 1 (CU)", "Sport 1 (CU)", 5, "Episode Title 5", 1, new DateTime(2010, 08, 22), new DateTime(2010, 08, 21), new DateTime(2010, 11, 21), "http://localhost:64973/tmp/Images/VideoThumbs/thumb_sport3.jpg", 85, "01:25:45",
						"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.CatchUp, 16, "Sport 2 (CU)", "Sport 2 (CU)", 6, "Episode Title 5", 1, new DateTime(2010, 08, 22), new DateTime(2010, 08, 21), new DateTime(2010, 11, 21), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg", 85, "01:25:45",
						"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.CatchUp, 17, "Sport 3 (CU)", "Sport 3 (CU)", 7, "Episode Title 5", 1, new DateTime(2010, 08, 22), new DateTime(2010, 08, 21), new DateTime(2010, 11, 21), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg", 85, "01:25:45",
						"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.CatchUp, 18, "Sport 4 (CU)", "Sport 4 (CU)", 8, "Episode Title 5", 1, new DateTime(2010, 08, 22), new DateTime(2010, 08, 21), new DateTime(2010, 11, 21), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg", 85, "01:25:45",
						"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.CatchUp, 19, "Sport 5 (CU)", "Sport 5 (CU)", 9, "Episode Title 5", 1, new DateTime(2010, 08, 22), new DateTime(2010, 08, 21), new DateTime(2010, 11, 21), "http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg", 85, "01:25:45",
						"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
						)
					);

			// Open Time
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.OpenTime, 21, "Open Time 1 (OT)", "Open Time 1 (OT)", 1, "Episode Title 1", 1, new DateTime(2010, 08, 21), new DateTime(2010, 08, 20), null, "http://localhost:64973/tmp/Images/VideoThumbs/thumb_idols1.jpg", 1, "00:01:23",
						"Open Time video abstract here. Britney Spears opened up at the latest Video Music Awards in a bejeweled bra and panties, performing her single \"Gimme More\"."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.OpenTime, 22, "Open Time 2 (OT)", "Open Time 2 (OT)", 2, "Episode Title 2", 1, new DateTime(2010, 07, 21), new DateTime(2010, 08, 02), null, "http://localhost:64973/tmp/Images/VideoThumbs/thumb_idols2.jpg", 2, "00:02:45",
						"Open Time video abstract here. Britney Spears opened up at the latest Video Music Awards in a bejeweled bra and panties, performing her single \"Gimme More\"."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.OpenTime, 23, "Open Time 3 (OT)", "Open Time 3 (OT)", null, null, null, new DateTime(2010, 08, 15), new DateTime(2010, 08, 15), null, "http://localhost:64973/tmp/Images/VideoThumbs/thumb_prisoner.jpg", 5, "00:05:34",
						"Open Time video abstract here. Britney Spears opened up at the latest Video Music Awards in a bejeweled bra and panties, performing her single \"Gimme More\"."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.OpenTime, 24, "Open Time 4 (OT)", "Open Time 4 (OT)", null, null, null, new DateTime(2010, 07, 26), new DateTime(2010, 07, 20), null, "http://localhost:64973/tmp/Images/VideoThumbs/thumb_music3.jpg", 3, "00:03:51",
						"Open Time video abstract here. Britney Spears opened up at the latest Video Music Awards in a bejeweled bra and panties, performing her single \"Gimme More\"."
						)
					);
			_testVideoSummaries.Add(
					new VideoItemSummary(ProductTypes.OpenTime, 25, "Open Time 5 (OT)", "Open Time 5 (OT)", 6, "Episode Title 6", 1, new DateTime(2010, 08, 05), new DateTime(2010, 08, 02), null, "http://localhost:64973/tmp/Images/VideoThumbs/thumb_bba2.jpg", 2, "00:02:15",
						"Open Time video abstract here. Britney Spears opened up at the latest Video Music Awards in a bejeweled bra and panties, performing her single \"Gimme More\"."
						)
					);
		}
		#endregion


		#region private void VideoCatalogueSeedItemsDynamically ()
		/// <summary>
		/// 
		/// </summary>
		private void VideoCatalogueSeedItemsDynamically ()
		{
			Random random = new Random();

			int currentId = 1;
			string title;
			int runtime;
			string videoAbstract;
			DateTime startDate;

			DateTime startDateFrom = new DateTime(2010, 07, 01);
			int startDateMaxDays = 95;
			DateTime expiryDateFrom = DateTime.Now;
			int expiryDateMaxDays = 120;
			int minRuntime = 43;
			int maxRuntime = 200;

			string abstractShort = "Short abstract for this video.";
			string abstractMed = "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.";
			string abstractLong = "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home. Longer abstract for this video to test display, trimming etc.";

			string titleLonger = "with a longer title for testing ";

			#region Box Office - Movies Only
			for (int i = 1; i <= Items_BoxOfficeVideos; i++)
			{
				title = string.Format("Movie Video {1}BO:{0}", i, random.Next(0, 8) == 3 ? titleLonger : "");
				runtime = random.Next(minRuntime, maxRuntime);
				videoAbstract = string.Format("Box Office video abstact here. {0}", random.Next(0, 8) != 4 ? abstractMed : (random.Next(0, 3) == 2 ? abstractShort : abstractLong));

				_testVideoSummaries.Add(
					new VideoItemSummary(
						ProductTypes.BoxOffice,
						currentId,
						title,
						title,
						null,
						null,
						null,
						startDateFrom.AddDays(random.Next(0, startDateMaxDays)),
						null,
						expiryDateFrom.AddDays(random.Next(0, expiryDateMaxDays)),
						"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoBoxOffice.jpg",
						runtime,
						string.Format("{0}:{1}:{2}", runtime / 60, runtime % 60, random.Next(0, 60)),
						videoAbstract
						)
					);

				currentId++;
			}
			#endregion


			#region Catch Up
			startDateFrom = new DateTime(2010, 08, 01);
			startDateMaxDays = 60;
			expiryDateFrom = DateTime.Now;
			expiryDateMaxDays = 30;
			int airDateMaxDays = 18;
			int seriesEpisodesMax = 4;
			minRuntime = 43;
			maxRuntime = 200;

			int currentCatchUpCount = 0;
			while (currentCatchUpCount < Items_CatchUpVideos)
			{
				int randomVal = random.Next(0, 5);
				if (randomVal == 0)
				{
					minRuntime = 43;
					maxRuntime = 200;

					// Movie.
					title = string.Format("Movie Video {1}CU:{0}", currentCatchUpCount + 1, random.Next(0, 8) == 3 ? titleLonger : "");
					runtime = random.Next(minRuntime, maxRuntime);
					startDate = startDateFrom.AddDays(random.Next(0, startDateMaxDays));

					_testVideoSummaries.Add(
						new VideoItemSummary(
							ProductTypes.CatchUp,
							currentId,
							title, title,
							null, null, null,
							startDate,
							startDate.AddDays(-1 * random.Next(0, airDateMaxDays)),
							expiryDateFrom.AddDays(random.Next(0, expiryDateMaxDays)),
							"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg",
							runtime,
							string.Format("{0}:{1}:{2}", runtime / 60, runtime % 60, random.Next(0, 60)),
							"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
							)
						);

					currentCatchUpCount++;
				}
				else if (randomVal == 1 || randomVal == 3)
				{
					minRuntime = 34;
					maxRuntime = 58;

					int seriesId = currentCatchUpCount + 1;
					int seriesNo = random.Next(1, 8);
					// Series.
					int seriesEpisodes = random.Next(0, seriesEpisodesMax) + 1;
					for (int i = 1; i <= seriesEpisodes; i++)
					{
						title = string.Format("Series {1}CU:{0}", seriesId, random.Next(0, 8) == 3 ? titleLonger : "");
						runtime = random.Next(minRuntime, maxRuntime);
						startDate = startDateFrom.AddDays(random.Next(0, startDateMaxDays));

						_testVideoSummaries.Add(
						new VideoItemSummary(
							ProductTypes.CatchUp,
							currentId,
							title,
							string.Format("Series {1} Video CU:{0}", currentCatchUpCount + 1, seriesId),
							i,
							"Episode",
							seriesNo,
							startDate,
							startDate.AddDays(-1 * random.Next(0, airDateMaxDays)),
							expiryDateFrom.AddDays(random.Next(0, expiryDateMaxDays)),
							"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg",
							runtime,
							string.Format("{0}:{1}:{2}", runtime / 60, runtime % 60, random.Next(0, 60)),
							"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
							)
						);

						currentCatchUpCount++;
					}
				}
				else if (randomVal == 2)
				{
					// Sport
					minRuntime = 15;
					maxRuntime = 120;

					int showId = currentCatchUpCount + 1;
					int showNo = random.Next(1, 8);

					int episodes = random.Next(0, seriesEpisodesMax) + 1;

					for (int i = 1; i <= episodes; i++)
					{
						title = string.Format("Sport {1}CU:{0}", showId, random.Next(0, 8) == 3 ? titleLonger : "");
						runtime = random.Next(minRuntime, maxRuntime);
						startDate = startDateFrom.AddDays(random.Next(0, startDateMaxDays));

						_testVideoSummaries.Add(
						new VideoItemSummary(
							ProductTypes.CatchUp,
							currentId,
							title,
							string.Format("Series {1} Video CU:{0}", currentCatchUpCount + 1, showId),
							i,
							"Episode",
							showNo,
							startDate,
							startDate.AddDays(-1 * random.Next(0, airDateMaxDays)),
							expiryDateFrom.AddDays(random.Next(0, expiryDateMaxDays)),
							"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg",
							runtime,
							string.Format("{0}:{1}:{2}", runtime / 60, runtime % 60, random.Next(0, 60)),
							"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
							)
						);

						currentCatchUpCount++;
					}
				}
				else
				{
					// Single Show.
					minRuntime = 24;
					maxRuntime = 133;

					// Movie.
					title = string.Format("Show Video {1}CU:{0}", currentCatchUpCount + 1, random.Next(0, 8) == 3 ? titleLonger : "");
					runtime = random.Next(minRuntime, maxRuntime);
					startDate = startDateFrom.AddDays(random.Next(0, startDateMaxDays));

					_testVideoSummaries.Add(
						new VideoItemSummary(
							ProductTypes.CatchUp,
							currentId,
							title, title,
							null, null, null,
							startDate,
							startDate.AddDays(-1 * random.Next(0, airDateMaxDays)),
							expiryDateFrom.AddDays(random.Next(0, expiryDateMaxDays)),
							"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoCatchUp.jpg",
							runtime,
							string.Format("{0}:{1}:{2}", runtime / 60, runtime % 60, random.Next(0, 60)),
							"Catch Up video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
							)
						);

					currentCatchUpCount++;
				}
			}
			#endregion


			#region Open Time
			minRuntime = 30;
			maxRuntime = 800;
			int currentOpenTimeCount = 0;
			while (currentOpenTimeCount < Items_OpenTimeVideos)
			{
				int randomVal = random.Next(0, 3);
				if (randomVal == 0)
				{
					// Individual Clip.
					title = string.Format("Clip Video {1}OT:{0}", currentOpenTimeCount + 1, random.Next(0, 8) == 3 ? titleLonger : "");
					runtime = random.Next(minRuntime, maxRuntime);
					startDate = startDateFrom.AddDays(random.Next(0, startDateMaxDays));

					_testVideoSummaries.Add(
						new VideoItemSummary(
							ProductTypes.OpenTime,
							currentId,
							title, title,
							null, null, null,
							startDate,
							startDate.AddDays(-1 * random.Next(0, airDateMaxDays)),
							null,
							"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoOpenTime.jpg",
							runtime / 60 == 0 ? 1 : runtime / 60,
							string.Format("00:{0}:{1}", runtime / 60, runtime % 60),
							"Open Time video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
							)
						);

					currentOpenTimeCount++;
				}
				else
				{
					int seriesId = currentOpenTimeCount + 1;
					int seriesNo = random.Next(1, 8);
					// Series.
					int seriesEpisodes = random.Next(0, seriesEpisodesMax) + 1;
					for (int i = 1; i <= seriesEpisodes; i++)
					{
						title = string.Format("Show Clips {1}OT:{0}", seriesId, random.Next(0, 8) == 3 ? titleLonger : "");
						runtime = random.Next(minRuntime, maxRuntime);
						startDate = startDateFrom.AddDays(random.Next(0, startDateMaxDays));

						_testVideoSummaries.Add(
						new VideoItemSummary(
							ProductTypes.OpenTime,
							currentId,
							title,
							string.Format("Show {1} Clip OT:{0}", currentOpenTimeCount + 1, seriesId),
							i,
							"Episode",
							seriesNo,
							startDate,
							startDate.AddDays(-1 * random.Next(0, airDateMaxDays)),
							null,
							"http://localhost:64973/tmp/Images/VideoThumbs/Thumb_VideoOpenTime.jpg",
							runtime / 60 == 0 ? 1 : runtime / 60,
							string.Format("00:{0}:{1}", runtime / 60, runtime % 60),
							"Open Time video abstract here. The mislaid characters and their increasing troubles return to M-Net Series for the fifth season of the Golden-Globe winning series, Lost. This newest series of Lost delivers everything fans have come to expect."
							)
						);

						currentOpenTimeCount++;
					}
				}
			}
			#endregion

		}
		#endregion


		public List<VideoItemSummary> GetVideoSummaries ( ProductTypes productTypeFilter, int videoCountFilter )
		{
			List<VideoItemSummary> returnList = new List<VideoItemSummary>();

			// Randomly return an empty list to test control logic and visibility.
			Random random = new Random();
			if (random.Next(0, 10) == 3)
			{
				return returnList;
			}

			foreach (VideoItemSummary item in _testVideoSummaries)
			{
				if (productTypeFilter == ProductTypes.OnDemand || item.ProductType == productTypeFilter)
				{
					returnList.Add(item);
					if (returnList.Count >= videoCountFilter)
					{
						break;
					}
				}
			}

			return returnList;
		}

		public List<VideoItemSummary> GetVideoSummariesByPage ( ProductTypes productTypeFilter, int forPage, int itemsPerPage, out int totalItems )
		{
			totalItems = 0;

			List<VideoItemSummary> returnList = new List<VideoItemSummary>();

			// Randomly return an empty list to test control logic and visibility.
			Random random = new Random();
			if (forPage == 1 && random.Next(0, 10) == 3)
			{
				return returnList;
			}

			int pageIndex = (forPage - 1) * itemsPerPage;
			for (int i = pageIndex; i < _testVideoSummaries.Count; i++)
			{
				VideoItemSummary item = _testVideoSummaries[i];
				if (productTypeFilter == ProductTypes.OnDemand || item.ProductType == productTypeFilter)
				{
					returnList.Add(item);
					if (returnList.Count >= itemsPerPage)
					{
						break;
					}
				}
			}

			totalItems = GetItemCountForProductType(productTypeFilter);

			return returnList;
		}

		public VideoEditorial GetEditorialPlaylist ( ProductTypes productType, int videoCount )
		{
			VideoEditorial videoEditorial = new VideoEditorial(GetEditorialTitle(productType, true), productType);

			List<VideoItemSummary> editorialItems = GetVideoSummaries(productType, videoCount);
			foreach (VideoItemSummary item in editorialItems)
			{
				videoEditorial.Add(item);
			}

			return videoEditorial;
		}

		public VideoEditorial GetEditorialList ( ProductTypes productType, int videoCount )
		{
			VideoEditorial videoEditorial = new VideoEditorial(GetEditorialTitle(productType, false), productType);

			List<VideoItemSummary> editorialItems = GetVideoSummaries(productType, videoCount);
			foreach (VideoItemSummary item in editorialItems)
			{
				videoEditorial.Add(item);
			}

			return videoEditorial;
		}

		public List<VideoClassificationItemType> GetClassificationItemsForProductType ( ProductTypes productType, string programTypeFilter )
		{
			return _testVideoClassifications.GetClassificationItemsForProductType(productType, programTypeFilter);
		}

		public List<VideoClassificationItemType> GetClassificationItems ( string parentId, CatalogueItemClassificationTypes classificationType )
		{
			return _testVideoClassifications.GetClassificationItems(parentId, classificationType);
		}

		private string GetEditorialTitle ( ProductTypes productType, bool isPlaylist )
		{
			string title = "";
			switch (productType)
			{
				case ProductTypes.BoxOffice:
					title = "(BO)";
					break;
				case ProductTypes.CatchUp:
					title = "(CU)";
					break;
				case ProductTypes.OpenTime:
					title = "(OT)";
					break;
				case ProductTypes.OnDemand:
					title = "(OD)";
					break;
			}

			return string.Format("Editorial {0} {1}", isPlaylist ? "Playlist" : "List", title);
		}

		private int GetItemCountForProductType ( ProductTypes productType )
		{
			switch (productType)
			{
				case ProductTypes.BoxOffice:
					return _testVideoSummaries.Count(video => video.ProductType == ProductTypes.BoxOffice);
				case ProductTypes.CatchUp:
					return _testVideoSummaries.Count(video => video.ProductType == ProductTypes.CatchUp);
				case ProductTypes.OpenTime:
					return _testVideoSummaries.Count(video => video.ProductType == ProductTypes.OpenTime);
				case ProductTypes.OnDemand:
					return _testVideoSummaries.Count;
				default:
					return 0;
			}
		}
	}
	#endregion
}