﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.TestData
{

	public class VideoClassificationHierarchy
	{
		private List<VideoClassificationItem> _classificationItems = new List<VideoClassificationItem>();

		public VideoClassificationHierarchy ()
		{
			
		}

		public void Seed ()
		{
			SeedBoxOffice();
			SeedCatchUp();
			SeedOpenTime();
		}


		#region private void SeedBoxOffice ()
		/// <summary>
		/// 
		/// </summary>
		private void SeedBoxOffice ()
		{
			for (int i = 1; i <= TestVideoCatalogue.Items_BoxOfficeProgramTypes; i++)
			{
				VideoClassificationItem rootItem = new VideoClassificationItem(ProductTypes.BoxOffice, Guid.NewGuid().ToString("D"), CatalogueItemClassificationTypes.ProgramType, string.Format("Program Type BO:{0}", i), null, 0);
				Random random = new Random();
				int numGenres = random.Next(0, TestVideoCatalogue.Items_BoxOfficeGenresMax) + 1;
				for (int j = 1; j <= numGenres; j++)
				{
					rootItem.AddVideoClassificationItemChild(
						Guid.NewGuid().ToString("D"),
						CatalogueItemClassificationTypes.Genre,
						string.Format("Gendre BO:{0}", i),
						random.Next(1, TestVideoCatalogue.Items_BoxOfficeVideos / random.Next(2, 5))
						);
				}
				_classificationItems.Add(rootItem);
			}
		}
		#endregion


		#region private void SeedCatchUp ()
		/// <summary>
		/// 
		/// </summary>
		private void SeedCatchUp ()
		{
			for (int i = 1; i <= TestVideoCatalogue.Items_CatchUpProgramTypes; i++)
			{
				VideoClassificationItem rootItem = new VideoClassificationItem(ProductTypes.CatchUp, Guid.NewGuid().ToString("D"), CatalogueItemClassificationTypes.ProgramType, string.Format("Program Type CU:{0}", i), null, 0);
				Random random = new Random();
				int numGenres = random.Next(0, TestVideoCatalogue.Items_CatchUpGenresMax) + 1;
				for (int j = 1; j <= numGenres; j++)
				{
					rootItem.AddVideoClassificationItemChild(
						Guid.NewGuid().ToString("D"),
						CatalogueItemClassificationTypes.Genre,
						string.Format("Gendre CU:{0}", i),
						random.Next(1, TestVideoCatalogue.Items_CatchUpVideos / random.Next(2, 5))
						);
				}
				_classificationItems.Add(rootItem);
			}
		}
		#endregion


		#region private void SeedOpenTime ()
		/// <summary>
		/// 
		/// </summary>
		private void SeedOpenTime ()
		{
			Random random = new Random();

			for (int i = 1; i <= TestVideoCatalogue.Items_OpenTimeProgramTypes; i++)
			{
				VideoClassificationItem rootItem = new VideoClassificationItem(ProductTypes.OpenTime, Guid.NewGuid().ToString("D"), CatalogueItemClassificationTypes.ProgramType, string.Format("Program Type OT:{0}", i), null, 0);
				
				int numGenres = random.Next(0, TestVideoCatalogue.Items_OpenTimeGenresMax) + 1;
				for (int j = 1; j <= numGenres; j++)
				{
					rootItem.AddVideoClassificationItemChild(
						Guid.NewGuid().ToString("D"),
						CatalogueItemClassificationTypes.Genre,
						string.Format("Gendre OT:{0}", i),
						random.Next(1, TestVideoCatalogue.Items_OpenTimeVideos / random.Next(2, 5))
						);
				}
				_classificationItems.Add(rootItem);
			}

			// TV Shows
			VideoClassificationItem tvShowsItem = new VideoClassificationItem(ProductTypes.OpenTime, TestVideoCatalogue.Items_OpenTimeTVShowsId, CatalogueItemClassificationTypes.TvShows, "TV Shows OT", null, 0);
			int numSubItems = random.Next(0, TestVideoCatalogue.Items_OpenTimeTVShowsMax) + 1;
			for (int i = 1; i <= numSubItems; i++)
			{
				tvShowsItem.AddVideoClassificationItemChild(
						Guid.NewGuid().ToString("D"),
						CatalogueItemClassificationTypes.TvShows,
						string.Format("Show OT:{0}", i),
						random.Next(1, TestVideoCatalogue.Items_OpenTimeVideos / random.Next(2, 5))
						);
			}
			_classificationItems.Add(tvShowsItem);

			// TV Channels and Categories (??)
			VideoClassificationItem tvChannelsItem = new VideoClassificationItem(ProductTypes.OpenTime, TestVideoCatalogue.Items_OpenTimeTVChannelsId, CatalogueItemClassificationTypes.TvChannels, "TV Channels OT", null, 0);
			numSubItems = random.Next(0, TestVideoCatalogue.Items_OpenTimeTVChannelsMax) + 1;
			for (int i = 1; i <= numSubItems; i++)
			{
				VideoClassificationItem subItem = new VideoClassificationItem(
						ProductTypes.OpenTime, 
						Guid.NewGuid().ToString("D"),
						CatalogueItemClassificationTypes.TvChannels,
						string.Format("Channel OT:{0}", i),
						tvChannelsItem.Item.ClassificationItemId,
						0
						);

				int numCategories = random.Next(0, TestVideoCatalogue.Items_OpenTimeTVChannelCategoriesMax) + 1;
				for (int j = 1; j <= numCategories; j++)
				{
					subItem.AddVideoClassificationItemChild(
							Guid.NewGuid().ToString("D"),
							CatalogueItemClassificationTypes.Categories,
							string.Format("Category OT:{0} CH:{1}", j, i),
							random.Next(1, TestVideoCatalogue.Items_OpenTimeVideos / random.Next(2, 5))
							);
				}

				tvChannelsItem.Add(subItem);
			}
			_classificationItems.Add(tvChannelsItem);

			// Categories ?????
		}
		#endregion


		public List<VideoClassificationItemType> GetClassificationItemsForProductType ( ProductTypes productType, string programTypeFilter )
		{
			List<VideoClassificationItemType> returnList = new List<VideoClassificationItemType>();

			string[] filters = string.IsNullOrEmpty(programTypeFilter) ? new string[]{"All"} : programTypeFilter.Split(',');

			foreach (VideoClassificationItem item in _classificationItems)
			{
				if (item.Item.ClassificationType == CatalogueItemClassificationTypes.ProgramType 
					&& item.ProductType == productType
					&& (filters[0] == "All" || filters.Contains<string>(item.Item.Name))
					)
				{
					returnList.Add(item.Item);
				}
			}

			return returnList;
		}

		public List<VideoClassificationItemType> GetClassificationItems ( string parentId, CatalogueItemClassificationTypes classificationType )
		{
			List<VideoClassificationItemType> returnList = new List<VideoClassificationItemType>();

			foreach (VideoClassificationItem item in _classificationItems)
			{
				if (item.Item.ClassificationItemId.Equals(parentId, StringComparison.OrdinalIgnoreCase))
				{
					foreach (VideoClassificationItem subItem in item)
					{
						returnList.Add(subItem.Item);
					}
				}
			}

			return returnList;
		}
	}

	public class VideoClassificationItem : List<VideoClassificationItem>
	{
		private VideoClassificationItemType _item;
		private ProductTypes _productType;

		public VideoClassificationItemType Item
		{
			get { return _item; }
		}

		public ProductTypes ProductType
		{
			get { return _productType; }
		}

		public VideoClassificationItem ( ProductTypes productType, string classificationItemId, CatalogueItemClassificationTypes classificationType, string name, string parentClassificationItemId, int totalNumberOfCatalogueItems )
		{
			_productType = productType;
			_item = new VideoClassificationItemType();
			_item.ClassificationItemId = classificationItemId;
			_item.ClassificationType = classificationType;
			_item.HasAdditionalClassification = false;
			_item.Name = name;
			_item.ParentClassificationItemId = parentClassificationItemId;
			_item.TotalNumberOfCatalogueItems = totalNumberOfCatalogueItems;
		}

		public void AddVideoClassificationItemChild ( string classificationItemId, CatalogueItemClassificationTypes classificationType, string name, int totalNumberOfCatalogueItems )
		{
			VideoClassificationItem childItem = new VideoClassificationItem(_productType, classificationItemId, classificationType, name, _item.ClassificationItemId, totalNumberOfCatalogueItems);
			this.Add(childItem);
			_item.HasAdditionalClassification = true;
		}
	}

	public class VideoClassificationItemType
	{
		#region IClassificationItem Members

		public string ClassificationItemId
		{
			get;
			set;
		}

		public CatalogueItemClassificationTypes ClassificationType
		{
			get;
			set;
		}

		public bool HasAdditionalClassification
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string ParentClassificationItemId
		{
			get;
			set;
		}

		public int TotalNumberOfCatalogueItems
		{
			get;
			set;
		}

		#endregion
	}
}