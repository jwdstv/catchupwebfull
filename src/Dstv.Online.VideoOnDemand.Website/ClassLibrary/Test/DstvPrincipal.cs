﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.Test
{
	// TODO: use integration stubs/classes once finalised.

	public class DstvPrincipal
	{
		public const string TestUserId = "ProfileUserId1";
		public const string KeyLoggedIn = "LoggedIn";
		public const string KeyUserName = "UserName";
		public const string KeyUserId = "UserId";

		public static bool LoggedIn
		{
			get { return WebsiteSettings.SessionStore.GetValue(KeyLoggedIn, false); }
		}

		public static string UserName
		{
			get { return WebsiteSettings.SessionStore.GetValue(KeyUserName, "unknown"); }
		}

		public static string UserId
		{
			get { return WebsiteSettings.SessionStore.GetValue(KeyUserId, ""); }
		}

		public static string Territory
		{
			get { return "South Africa"; }
		}

		public static bool HasPrepaidFacility
		{
			get { return true; }
		}

		public static decimal PrepaidBalance
		{
			get { return 104.87m; }
		}

		public static void Login ( string userName )
		{
			WebsiteSettings.SessionStore.SetValue(KeyLoggedIn, true);
			WebsiteSettings.SessionStore.SetValue(KeyUserName, userName);
			WebsiteSettings.SessionStore.SetValue(KeyUserId, TestUserId);
		}

		public static void Logout ()
		{
			WebsiteSettings.SessionStore.ClearSession();
		}

		
	}
}