﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.TestData
{
	// Currently providing test data. Will update once VideoCatalogue MidTier is done and understood.

	public class VideoEditorial : List<VideoItemSummary>
	{
		private string _title;
		private ProductTypes _productType;

		public string Title
		{
			get { return _title; }
		}

		public ProductTypes ProductType
		{
			get { return _productType; }
		}

		public VideoEditorial ( string title, ProductTypes productType )
		{
			_title = title;
			_productType = productType;
		}
	}
}