﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.TestData
{
	// Currently providing test data. Will update once VideoCatalogue MidTier is done and understood.

	public class VideoItemSummary
	{
		private string _abstract = "";
		private DateTime? _airDate = null;
		private string _durationDescription = "";
		private int? _episodeNumber = null;
		private string _episodeTitle = null;
		private DateTime? _expiryDate = null;
		private Uri _mediaThumbNailUri;
		private int? _programId;
		private string _programTitle;
		private int _runTimeInMinutes;
		private int? _seasonNumber;
		private DateTime? _startDate;
		private string _videoTitle;
		private ProductTypes _productType;


		#region ICatalogueItemSummary Members

		public string Abstract
		{
			get { return _abstract; }
		}

		public DateTime? Airdate
		{
			get { return _airDate; }
		}

		//public CatalogueItemIdentifier CatalogItemId
		//{
		//   get { throw new NotImplementedException(); }
		//}

		//public ClassificationItemIdentifier ClassificationId
		//{
		//   get { throw new NotImplementedException(); }
		//}

		public string DurationDescription
		{
			get { return _durationDescription; }
		}

		public int? EpisodeNumber
		{
			get { return _episodeNumber; }
		}

		public string EpisodeTitle
		{
			get { return _episodeTitle; }
		}

		public DateTime? ExpiryDate
		{
			get { return _expiryDate; }
		}

		public string Keywords
		{
			get { throw new NotImplementedException(); }
		}

		public Uri MediaThumbNailUri
		{
			get { return _mediaThumbNailUri; }
		}

		//public Dstv.Online.VideoOnDemand.Transactions.ProductTypeIdentifier ProductTypeId
		//{
		//   get { throw new NotImplementedException(); }
		//}

		public ProductTypes ProductType
		{
			get { return _productType; }
		}

		public int? ProgramId
		{
			get { return _programId; }
		}

		public string ProgramTitle
		{
			get { return _programTitle; }
		}

		//public Dstv.Online.VideoOnDemand.CurrencyAmount RentalAmount
		//{
		//   get { throw new NotImplementedException(); }
		//}

		public int RentalPeriodInHours
		{
			get { throw new NotImplementedException(); }
		}

		public int RunTimeInMinutes
		{
			get { return _runTimeInMinutes; }
		}

		public int? SeasonNumber
		{
			get { return _seasonNumber; }
		}

		public DateTime? StartDate
		{
			get { return _startDate; }
		}

		public string VideoTitle
		{
			get { return _videoTitle; }
		}

		#endregion

		public VideoItemSummary ( ProductTypes productType, int programId, string programTitle, string videoTitle, int? episodeNumber, string episodeTitle, int? seasonNumber, DateTime? startDate, DateTime? airDate, DateTime? expiryDate, string mediaThumbnailUrl, int runTimeInMinutes, string durationDescription, string videoAbstract )
		{
			_productType = productType;
			_programId = programId;
			_programTitle = programTitle;
			_videoTitle = videoTitle;
			_episodeNumber = episodeNumber;
			_episodeTitle = episodeTitle;
			_seasonNumber = seasonNumber;
			_startDate = startDate;
			_airDate = airDate;
			_expiryDate = expiryDate;
			_mediaThumbNailUri = new Uri(mediaThumbnailUrl);
			_runTimeInMinutes = runTimeInMinutes;
			_durationDescription = durationDescription;
			_abstract = videoAbstract;
		}
	}
}