﻿using System;
using System.Web;
using System.Configuration;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    public class BasePage : System.Web.UI.Page
    {
        public bool HideBoxOffice = true;

        /// <summary>
        /// Set the pre initialize method for all pages to check if the 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HideBoxOffice"]))
                {
                    HideBoxOffice = Convert.ToBoolean(ConfigurationManager.AppSettings["HideBoxOffice"]);
                }
            }
            catch
            {
                HideBoxOffice = true;
            }
        }


        protected override void OnLoad(EventArgs e)
        {
            Response.Expires = 0;
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");

            Response.AppendHeader("Cache-Control", "no-cache");
            Response.CacheControl = "no-cache";

            Response.ExpiresAbsolute = new DateTime(1900, 1, 1);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            base.OnLoad(e);

        }

    }
}