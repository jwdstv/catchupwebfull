﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using Dstv.Online.Contracts;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.Proxies
{
    public class InvoiceServiceProxy : ClientBase<IInvoiceService>
    {
        /// <summary>
        /// Generate a customer invoice
        /// </summary>
        /// <param name="_customerId"></param>
        /// <param name="_accountNumber"></param>
        /// <param name="_transactionCodes"></param>
        /// <param name="_startDate"></param>
        /// <param name="_endDate"></param>
        /// <param name="_documentBytes"></param>
        /// <returns></returns>
        public ServiceResult Generate
            (
                uint _customerId,
                uint _accountNumber,
                List<string> _transactionCodes,
                DateTime _startDate,
                DateTime _endDate,
                out byte[] _documentBytes
            )
        {
            return base.Channel.Generate(_customerId, _accountNumber, _transactionCodes, _startDate, _endDate, out _documentBytes);
        }
    }
}