﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data.ResponseTypes
{
	public sealed class Constants
	{
		public const string SystemError = "";
	}

	/// <summary>
	/// Login response
	/// </summary>
	public class LoginResponse
	{
		/// <summary>
		/// Was the user logged in successfully
		/// </summary>
		public bool Success;

		/// <summary>
		/// Any error messages
		/// </summary>
		public string ErrorMsg;
	}

	/// <summary>
	/// Payment response 
	/// </summary>
	public class TransactionResponse
	{
		/// <summary>
		/// Was the payment successfull
		/// </summary>
		public bool Success;

		/// <summary>
		/// Any error messages
		/// </summary>
		public string ErrorMsg;

		public string ReferenceNo;
	}
}