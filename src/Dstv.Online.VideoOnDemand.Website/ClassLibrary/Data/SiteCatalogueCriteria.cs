﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data
{
	public class SiteCatalogueCriteria : DataContract.CatalogueCriteria
	{
		// Need to set productType identifier based on SiteEnums.ProductType
		public SiteEnums.ProductType ProductType
		{
			get
			{
				return SiteEnums.ProductType.OnDemand; 
				// TODO: Return based on mapping from ProductTypeIdentifer
			}
			set
			{
				// TODO: Set ProductTypeIdentifer based on mapping from SiteEnums.ProductType
			}
		}

		public string GetSerialisedForUrlParam ()
		{
			return "";
			// TODO: serialise the object (CatalogueCriteria) for Url query string parameter. Include parameter name ??
		}

		public void SetFromUrlParam ( string urlParameterSerialised )
		{
			// TODO: deserialise the Url parameter and set the properties.
		}
	}
}