﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using VideoModule = Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess
{

    #region VideoFilter
    /// <summary>
    /// 
    /// </summary>
    public class VideoFilter
    {

        #region Default Constants

        private const int DefaultVideoCount = 5;
        private const ProductTypes DefaultProductType = ProductTypes.OnDemand;
        private VideoProgramType[] DefaultVideoTypes = new VideoProgramType[] { VideoProgramType.ShowEpisode, VideoProgramType.Movie };

        private const string UrlClassificationItemIdentifierKey = "a";
        private const string UrlClassificationItemNameKey = "j";
        private const string UrlParentClassificationIdentifierKey = "k";
        private const string UrlCatalogueItemClassificationTypesKey = "b";
        private const string UrlProductTypeIdentifierKey = "c";
        private const string UrlTerritoryIdentifierKey = "d";
        private const string UrlPageNumberKey = "e";
        private const string UrlItemsPerPageKey = "f";
        private const string UrlSortOrdersKey = "g";
        private const string UrlCatalogueItemIdentifiersKey = "h";
        private const string UrlVideoProgramTypesKey = "i";
        private const char UrlPropertyAssignment = '=';
        private const char UrlPropertySeperator = '|';
        private const char UrlPropertyArraySeperator = ',';
        private const bool UrlDoBase64 = false;
        #endregion


        #region CatalogueCriteria
        private CatalogueCriteria _catalogueCriteria;
        /// <summary>
        /// 
        /// </summary>
        public CatalogueCriteria CatalogueCriteria
        {
            get { return _catalogueCriteria; }
        }
        #endregion


        #region ProductType
        /// <summary>
        /// 
        /// </summary>
        public ProductTypes ProductType
        {
            get { return _catalogueCriteria.ClassificationCriteria.ProductTypeId.Value; }
            set { _catalogueCriteria.ClassificationCriteria.ProductTypeId = new Transactions.ProductTypeIdentifier(value); }
        }
        #endregion


        #region VideoCount
        /// <summary>
        /// 
        /// </summary>
        public int VideoCount
        {
            get { return _catalogueCriteria.Paging.ItemsPerPage; }
            set { _catalogueCriteria.Paging.ItemsPerPage = value; }
        }
        #endregion


        public string ClassificationId
        {
            get { return _catalogueCriteria.ClassificationCriteria == null || _catalogueCriteria.ClassificationCriteria.ClassificationItemId == null ? "" : _catalogueCriteria.ClassificationCriteria.ClassificationItemId.Value; }
            set
            {
                if (_catalogueCriteria.ClassificationCriteria == null)
                {
                    _catalogueCriteria.ClassificationCriteria = new ClassificationCriteria();
                }

                if (string.IsNullOrEmpty(value))
                {
                    _catalogueCriteria.ClassificationCriteria.ClassificationItemId = null;
                }
                else
                {
                    _catalogueCriteria.ClassificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(value);
                }
            }
        }


        public string ParentClassificationId
        {
            get { return _catalogueCriteria.ClassificationCriteria == null || _catalogueCriteria.ClassificationCriteria.ParentClassificationId == null ? "" : _catalogueCriteria.ClassificationCriteria.ParentClassificationId.Value; }
            set
            {
                if (_catalogueCriteria.ClassificationCriteria == null)
                {
                    _catalogueCriteria.ClassificationCriteria = new ClassificationCriteria();
                }

                if (string.IsNullOrEmpty(value))
                {
                    _catalogueCriteria.ClassificationCriteria.ParentClassificationId = null;
                }
                else
                {
                    _catalogueCriteria.ClassificationCriteria.ParentClassificationId = new ClassificationItemIdentifier(value);
                }
            }
        }


        #region public SiteEnums.VideoSortOrder SortFilter
        /// <summary>
        /// 
        /// </summary>
        public SiteEnums.VideoSortOrder SortFilter
        {
            get
            {
                return SiteEnums.Mapper.MapBaseSortOrderToVideoSortOrder(_catalogueCriteria.SortOrder);
            }
            set
            {
                _catalogueCriteria.SortOrder = SiteEnums.Mapper.MapVideoSortOrderToBaseSortOrder(value);
            }
        }
        #endregion


        #region public VideoProgramType VideoTypes
        /// <summary>
        /// 
        /// </summary>
        public string VideoTypes
        {
            get { return _catalogueCriteria.ClassificationCriteria != null ? _catalogueCriteria.ClassificationCriteria.GetVideoProgramTypesAsCsv() : ""; }
            set
            {
                if (_catalogueCriteria.ClassificationCriteria == null)
                {
                    _catalogueCriteria.ClassificationCriteria = new ClassificationCriteria();
                }
                _catalogueCriteria.ClassificationCriteria.SetVideoProgramTypesFromCsv(value);
            }
        }
        #endregion


        #region public VideoFilter ()
        /// <summary>
        /// 
        /// </summary>
        public VideoFilter()
        {
            InitialiseFilter(DefaultProductType, DefaultVideoTypes, DefaultVideoCount);
        }
        #endregion


        #region public VideoFilter ( ProductTypes defaultProductType )
        /// <summary>
        /// 
        /// </summary>
        public VideoFilter(ProductTypes defaultProductType, VideoProgramType[] defaultVideoTypes)
        {
            InitialiseFilter(defaultProductType, defaultVideoTypes, DefaultVideoCount);
        }
        #endregion


        #region public VideoFilter ( ProductTypes defaultProductType, int defaultVideoCount )
        /// <summary>
        /// 
        /// </summary>
        public VideoFilter(ProductTypes defaultProductType, VideoProgramType[] defaultVideoTypes, int defaultVideoCount)
        {
            InitialiseFilter(defaultProductType, defaultVideoTypes, defaultVideoCount);
        }
        #endregion


        #region private void InitialiseFilter ( ProductTypes defaultProductType, int defaultVideoCount )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultProductType"></param>
        /// <param name="defaultVideoCount"></param>
        /// <param name="defaultProgramTypes"></param>
        private void InitialiseFilter(ProductTypes defaultProductType, VideoProgramType[] defaultProgramTypes, int defaultVideoCount)
        {
            _catalogueCriteria = new CatalogueCriteria();

            // Initialise ProductType with default value.
            if (_catalogueCriteria.ClassificationCriteria == null)
            {
                _catalogueCriteria.ClassificationCriteria = new ClassificationCriteria();
            }
            if (_catalogueCriteria.ClassificationCriteria.ProductTypeId == null)
            {
                _catalogueCriteria.ClassificationCriteria.ProductTypeId = new Transactions.ProductTypeIdentifier(defaultProductType);
            }
            // Set VideoTypes filter.
            _catalogueCriteria.ClassificationCriteria.VideoProgramTypes = defaultProgramTypes;

            // Initialise Region/Territory with current user's region.
            if (_catalogueCriteria.ClassificationCriteria.TerritoryId == null)
            {
                _catalogueCriteria.ClassificationCriteria.TerritoryId = DstvUser.Territory.TerritoryId;
            }

            // Initialise PagingCrieria with default video count.
            if (_catalogueCriteria.Paging != null)
            {
                _catalogueCriteria.Paging.PageNumber = 1;
                _catalogueCriteria.Paging.ItemsPerPage = defaultVideoCount;
            }
            // else we have a problem!!

        }
        #endregion


        #region public string GetUrlParameterValue ()
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetUrlParameterValue(bool includeCatalogueItemIds)
        {
            return GetUrlParameterValueForCatalogueCriteria(_catalogueCriteria, includeCatalogueItemIds);
        }
        #endregion


        #region public static string GetUrlParameterValueForCatalogueCriteria ( CatalogueCriteria catalogueCriteria, bool includeCatalogueItemIds )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueCriteria"></param>
        /// <param name="includeCatalogueItemIds"></param>
        /// <returns></returns>
        public static string GetUrlParameterValueForCatalogueCriteria(CatalogueCriteria catalogueCriteria, bool includeCatalogueItemIds)
        {
            StringBuilder urlParameterValue = new StringBuilder();
            if (catalogueCriteria.ClassificationCriteria != null)
            {
                if (catalogueCriteria.ClassificationCriteria.ClassificationItemId != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlClassificationItemIdentifierKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.ClassificationCriteria.ClassificationItemId.Value,
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.ClassificationCriteria.ClassificationItemName != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                            UrlClassificationItemNameKey,
                            UrlPropertyAssignment,
                            catalogueCriteria.ClassificationCriteria.ClassificationItemName,
                            UrlPropertySeperator
                            );
                }
                if (catalogueCriteria.ClassificationCriteria.ParentClassificationId != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlParentClassificationIdentifierKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.ClassificationCriteria.ParentClassificationId.Value,
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.ClassificationCriteria.ClassificationType != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlCatalogueItemClassificationTypesKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.ClassificationCriteria.ClassificationType,
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.ClassificationCriteria.ProductTypeId != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlProductTypeIdentifierKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.ClassificationCriteria.ProductTypeId.Value,
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.ClassificationCriteria.TerritoryId != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlTerritoryIdentifierKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.ClassificationCriteria.TerritoryId.Value,
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.Paging != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlPageNumberKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.Paging.PageNumber,
                        UrlPropertySeperator
                        );
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlItemsPerPageKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.Paging.ItemsPerPage,
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.SortOrder != null)
                {
                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlSortOrdersKey,
                        UrlPropertyAssignment,
                        catalogueCriteria.SortOrder,
                        UrlPropertySeperator
                        );
                }
                if (includeCatalogueItemIds && catalogueCriteria.CatalogueItemIds != null && catalogueCriteria.CatalogueItemIds.Length > 0)
                {
                    string catalogueItemIds = "";
                    foreach (CatalogueItemIdentifier itemId in catalogueCriteria.CatalogueItemIds)
                    {
                        catalogueItemIds += string.Format("{0}{1}", itemId.Value, UrlPropertyArraySeperator);
                    }

                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlCatalogueItemIdentifiersKey,
                        UrlPropertyAssignment,
                        catalogueItemIds.TrimEnd(UrlPropertyArraySeperator),
                        UrlPropertySeperator
                        );
                }
                if (catalogueCriteria.ClassificationCriteria.VideoProgramTypes != null && catalogueCriteria.ClassificationCriteria.VideoProgramTypes.Length > 0)
                {
                    string videoProgramTypes = "";
                    foreach (VideoProgramType itemId in catalogueCriteria.ClassificationCriteria.VideoProgramTypes)
                    {
                        videoProgramTypes += string.Format("{0}{1}", itemId, UrlPropertyArraySeperator);
                    }

                    urlParameterValue.AppendFormat("{0}{1}{2}{3}",
                        UrlCatalogueItemIdentifiersKey,
                        UrlPropertyAssignment,
                        videoProgramTypes.TrimEnd(UrlPropertyArraySeperator),
                        UrlPropertySeperator
                        );
                }

            }

            return SiteUtils.Url.EncodeString(urlParameterValue.ToString().TrimEnd(UrlPropertySeperator));

        }
        #endregion


        #region public void ReadFromUrlParameterValue ( string serialisedUrlParameterValue )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialisedUrlParameterValue"></param>
        public void ReadFromUrlParameterValue(string serialisedUrlParameterValue)
        {
            CatalogueCriteria catalogueCriteria = ReadCatalogueCriteriaFromUrlParameterValue(serialisedUrlParameterValue);
            if (catalogueCriteria != null)
            {
                _catalogueCriteria = catalogueCriteria;
            }
        }
        #endregion


        #region public static CatalogueCriteria ReadCatalogueCriteriaFromUrlParameterValue ( string serialisedUrlParameterValue )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialisedUrlParameterValue"></param>
        /// <returns></returns>
        public static CatalogueCriteria ReadCatalogueCriteriaFromUrlParameterValue(string serialisedUrlParameterValue)
        {
            if (string.IsNullOrEmpty(serialisedUrlParameterValue))
            {
                return null;
            }

            // Initialise Catalogue Criteria objects.
            CatalogueCriteria catalogueCriteria = new CatalogueCriteria();
            catalogueCriteria.ClassificationCriteria = new ClassificationCriteria();

            string parameterValue = "";

            parameterValue = SiteUtils.Url.DecodeString(serialisedUrlParameterValue);


            #region Parse Properties
            // Lets get array of properties and values.
            string[] propertiesAndValues = parameterValue.Split(UrlPropertySeperator);
            int intValue;
            foreach (string propertyAndValue in propertiesAndValues)
            {
                string[] propertyValueSplit = propertyAndValue.Split(UrlPropertyAssignment);
                if (propertyValueSplit.Length == 2 && !string.IsNullOrEmpty(propertyValueSplit[0]) && !string.IsNullOrEmpty(propertyValueSplit[1]))
                {
                    switch (propertyValueSplit[0])
                    {
                        case UrlCatalogueItemClassificationTypesKey:
                            CatalogueItemClassificationTypes enumValue;
                            if (Enum.TryParse<CatalogueItemClassificationTypes>(propertyValueSplit[1], out enumValue))
                            {
                                catalogueCriteria.ClassificationCriteria.ClassificationType = enumValue;
                            }
                            break;
                        case UrlCatalogueItemIdentifiersKey:
                            // Array of IDs (identifiers) so process.
                            string[] idsSplit = propertyValueSplit[1].Split(UrlPropertyArraySeperator);
                            List<CatalogueItemIdentifier> ids = new List<CatalogueItemIdentifier>();
                            foreach (string id in idsSplit)
                            {
                                ids.Add(new CatalogueItemIdentifier(id));
                            }
                            catalogueCriteria.CatalogueItemIds = ids.ToArray();
                            break;
                        case UrlClassificationItemIdentifierKey:
                            catalogueCriteria.ClassificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(propertyValueSplit[1]);
                            break;
                        case UrlParentClassificationIdentifierKey:
                            catalogueCriteria.ClassificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(propertyValueSplit[1]);
                            break;
                        case UrlClassificationItemNameKey:
                            catalogueCriteria.ClassificationCriteria.ClassificationItemName = propertyValueSplit[1];
                            break;
                        case UrlItemsPerPageKey:
                            if (int.TryParse(propertyValueSplit[1], out intValue))
                            {
                                catalogueCriteria.Paging.ItemsPerPage = intValue;
                            }
                            break;
                        case UrlPageNumberKey:
                            if (int.TryParse(propertyValueSplit[1], out intValue))
                            {
                                catalogueCriteria.Paging.PageNumber = intValue;
                            }
                            break;
                        case UrlProductTypeIdentifierKey:
                            catalogueCriteria.ClassificationCriteria.ProductTypeId = new Transactions.ProductTypeIdentifier(propertyValueSplit[1]);
                            break;
                        case UrlSortOrdersKey:
                            SortOrders enumValue2;
                            if (Enum.TryParse<SortOrders>(propertyValueSplit[1], out enumValue2))
                            {
                                catalogueCriteria.SortOrder = enumValue2;
                            }
                            break;
                        case UrlTerritoryIdentifierKey:
                            catalogueCriteria.ClassificationCriteria.TerritoryId = new Globalization.TerritoryIdentifier(propertyValueSplit[1]);
                            break;
                        case UrlVideoProgramTypesKey:
                            // Array of enums so process.
                            catalogueCriteria.ClassificationCriteria.SetVideoProgramTypesFromCsv(propertyValueSplit[1]);
                            //string[] videoTypesSplit = propertyValueSplit[1].Split(UrlPropertyArraySeperator);
                            //List<VideoProgramType> videoTypes = new List<VideoProgramType>();
                            //VideoProgramType enumValue3;
                            //foreach (string videoType in videoTypesSplit)
                            //{
                            //   if (Enum.TryParse<VideoProgramType>(videoType, out enumValue3))
                            //   {
                            //      videoTypes.Add(enumValue3);
                            //   }
                            //}
                            //_catalogueCriteria.VideoProgramTypes = videoTypes.ToArray();
                            break;
                    }
                }
            }
            #endregion // foreach

            return catalogueCriteria;
        }
        #endregion


    }
    #endregion

}