﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Common;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using System.Diagnostics;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess
{
	public partial class Rentals
	{
		#region Cache Keys

		private static string CacheKey_VideoRentalsListForCurrentUser
		{
			get { return string.Format("VideoRentalsListForCurrentUser_{0}", DstvUser.CustomerId.Value.ToUpper()); }
		}

		private static string CacheKey_VideoRentalPageListForCurrentUser
		{
			get { return string.Format("VideoRentalPageListForCurrentUser_{0}", DstvUser.CustomerId.Value.ToUpper()); }
		}

		private static string CacheKey_VideoRentalsActiveCountForCurrentUser
		{
			get { return string.Format("VideoRentalsActiveCountForCurrentUser_{0}", DstvUser.CustomerId.Value.ToUpper()); }
		}

		#endregion


		internal static int GetActiveVideoRentalCountForCurrentUser ()
		{
			if (DstvUser.IsAuthenticated)
			{
				string cacheKey = CacheKey_VideoRentalsActiveCountForCurrentUser;

				int activeRentalCount = 0;
				
				object cachedObject = WebsiteSettings.CacheStore.GetObject(cacheKey);
				if (cachedObject != null)
				{
					return (int)cachedObject;
				}

				Rental rentalMgr = new Rental();
				RentalResult rentalResult = rentalMgr.GetVideoRentalsByUserId(DstvUser.CustomerId.Value, 0, 0);

				if (rentalResult != null && rentalResult.TotalResults > 0)
				{
					// Get active rentals and then get soonest to expire - this will be used for cache absolute expiration datetime.
					List<VideoRentalType> activeRentals = rentalResult.Rentals.FindAll(delegate( VideoRentalType videoRental ) { return !videoRental.HasRentalExpired; });
					if (activeRentals.Count > 0)
					{
						activeRentalCount = activeRentals.Count;
						activeRentals.Sort(delegate( VideoRentalType videoRental1, VideoRentalType videoRental2 ) { return videoRental1.ExpiryDate.CompareTo(videoRental2.ExpiryDate); });

						WebsiteSettings.CacheStore.AddObjectWithAbsoluteExpiration(cacheKey, activeRentalCount, activeRentals[0].ExpiryDate, WebsiteSettings.CacheStore.CachedItemPriority.Medium);
					}
				}
				
				if (activeRentalCount == 0)
				{
					// Set to zero with a 30 minute expiry ??
					WebsiteSettings.CacheStore.AddObjectWithAbsoluteExpiration(cacheKey, activeRentalCount, 30, WebsiteSettings.CacheStore.CachedItemPriority.Medium);
				}

				return activeRentalCount;
			}
			else
			{
				return 0;
			}
		}

		internal static RentalResult GetVideoRentalsListForCurrentUser ( int maxRentalsToReturn )
		{
			if (DstvUser.IsAuthenticated)
			{
				string cacheKey = CacheKey_VideoRentalsListForCurrentUser;
				object cachedObject = WebsiteSettings.CacheStore.GetObject(cacheKey);
				if (cachedObject != null)
				{
					return (RentalResult)cachedObject;
				}

				// If not cached.
				Rental rentalMgr = new Rental();
				RentalResult rentalResult = rentalMgr.GetVideoRentalsByUserId(DstvUser.CustomerId.Value, maxRentalsToReturn, 1);

				if (rentalResult != null)
				{
					WebsiteSettings.CacheStore.AddObjectWithSlidingExpiration(cacheKey, rentalResult, 30, WebsiteSettings.CacheStore.CachedItemPriority.Medium);
				}

				return rentalResult;
			}
			else
			{
				return null;
			}
		}


		#region internal static RentalResult GetVideoRentalPageListForCurrentUser ( int pageNumber, int numberOfItemPerPage )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="numberOfItemPerPage"></param>
		/// <returns></returns>
		internal static RentalResult GetVideoRentalPageListForCurrentUser ( int pageNumber, int numberOfItemPerPage )
		{
			if (DstvUser.IsAuthenticated)
			{
				string userId = DstvUser.CustomerId.Value.ToUpper();
				RentalPageCacheList rentalPageCacheList;

				string cacheKey = CacheKey_VideoRentalPageListForCurrentUser;
				object cachedObject = WebsiteSettings.CacheStore.GetObject(cacheKey);
				if (cachedObject != null)
				{
					rentalPageCacheList = (RentalPageCacheList)cachedObject;
					RentalPageCache rentalPageCache = rentalPageCacheList.GetRentalPageCache(userId, pageNumber);
					if (rentalPageCache != null)
					{
						return rentalPageCache.RentalResult;
					}
				}
				else
				{
					rentalPageCacheList = new RentalPageCacheList(userId);
				}

				// If not cached.
				Rental rentalMgr = new Rental();
				RentalResult rentalResult = rentalMgr.GetVideoRentalsByUserId(DstvUser.CustomerId.Value, numberOfItemPerPage, pageNumber);

				if (rentalResult != null)
				{
					rentalPageCacheList.AddRentalPageCache(userId, pageNumber, rentalResult);
					WebsiteSettings.CacheStore.AddObjectWithSlidingExpiration(cacheKey, rentalPageCacheList, 5, WebsiteSettings.CacheStore.CachedItemPriority.Medium);
				}

				return rentalResult;
			}
			else
			{
				return null;
			}
		}
		#endregion

		#region internal static RentVideoResult RentVideoForCurrentUser ( string catalogItemId, decimal catalogItemCost, string customerPin )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="catalogItemId"></param>
		/// <param name="catalogItemCost"></param>
		/// <param name="customerPin"></param>
		/// <returns></returns>
		internal static RentVideoResult RentVideoForCurrentUser ( string catalogItemId, decimal catalogItemCost, string customerPin )
		{
            return RentVideoForCurrentUser(catalogItemId, catalogItemCost, customerPin, null);
		}
		#endregion


        #region internal static RentVideoResult RentVideoForCurrentUser ( string catalogItemId, decimal catalogItemCost, string customerPin, string resultRef )
        /// <summary>
		/// 
		/// </summary>
		/// <param name="catalogItemId"></param>
		/// <param name="catalogItemCost"></param>
		/// <param name="customerPin"></param>
        /// <param name="resultRef"></param>
		/// <returns></returns>
        internal static RentVideoResult RentVideoForCurrentUser(string catalogItemId, decimal catalogItemCost, string customerPin, string resultRef)
		{
			RentVideoResult rentVideoResult = new RentVideoResult { Success = false, Message = "", ReferenceNo = "", Exception = null };

			Logger logger = new Logger();
			string loggerAction = "RentVideoForCurrentUser";

			if (DstvUser.IsAuthenticated)
			{
				// User doing rental.
				RentalUser rentalUser = new RentalUser();
				rentalUser.UserId = DstvUser.CustomerId.Value;
				rentalUser.UserEmail = DstvUser.Email;
				rentalUser.UserPin = customerPin;
				// TODO: this is not currently available on the DStvPrincipal
				rentalUser.UserFullName = DstvUser.UserName;

				Rental rentalModule = new Rental();

				try
				{
					ResultType rentalResult = rentalModule.AddVideoRentalByUser(rentalUser, new CatalogueItemIdentifier(catalogItemId), resultRef);

					rentVideoResult.Success = rentalResult.Success;
					rentVideoResult.Message = rentalResult.ResultInfo;
					rentVideoResult.ReferenceNo = "NA";

					// Log result.
					logger.Log(
						Config.EventLogSource.BoxOffice,
						rentVideoResult.Success ? EventLogEntryType.Information : EventLogEntryType.Warning,
						rentalResult.Success ? string.Format("Video successfully rented. catalogItemId: {0}", catalogItemId) : rentalResult.ResultInfo,
						HttpContext.Current.Request,
						loggerAction,
						DstvUser.Email,
						DstvUser.UserName,
						DstvUser.CustomerId.Value
						);

					// If successful then we need to invalidate rental cache for user.
					if (rentVideoResult.Success)
					{
						WebsiteSettings.CacheStore.RemoveObject(CacheKey_VideoRentalsListForCurrentUser);
						WebsiteSettings.CacheStore.RemoveObject(CacheKey_VideoRentalPageListForCurrentUser);
						WebsiteSettings.CacheStore.RemoveObject(CacheKey_VideoRentalsActiveCountForCurrentUser);
					}
				}
				catch (Exception exception)
				{
					// Log exception
					string supportReferenceNumber = logger.LogException(
						Config.EventLogSource.BoxOffice,
						exception,
						HttpContext.Current.Request,
						loggerAction,
						DstvUser.Email,
						DstvUser.UserName,
						DstvUser.CustomerId.Value
						);

					string errorMessage = "SystemError";

					rentVideoResult.Success = false;
					rentVideoResult.ReferenceNo = supportReferenceNumber;
					rentVideoResult.Exception = exception;

                    if (HttpContext.Current.IsDebuggingEnabled)
                    {
                        errorMessage = string.Format("{1}{0}{0}DEBUG:{0}{2}",
                        Environment.NewLine,
                        errorMessage,
                        Logger.GetExceptionAsString(exception)
                        );
                    }
                    
                    rentVideoResult.Message = errorMessage;
				}
			}
			else
			{
				rentVideoResult.Success = false;
				rentVideoResult.Message = "NotAuthenticated.";
				rentVideoResult.ReferenceNo = "NA";

				// Log error.
				// TODO: add additional property to logging which is full client detail.
				logger.LogError(
					Config.EventLogSource.BoxOffice,
					"User not authenticated.",
					HttpContext.Current.Request,
					loggerAction,
					"unknown",
					"unknown",
					"unknown"
					);
			}

			return rentVideoResult;
		}
		#endregion


		#region internal class RentVideoResult
		/// <summary>
		/// 
		/// </summary>
		internal class RentVideoResult
		{
			public bool Success;
			public string Message;
			public string ReferenceNo;
			public Exception Exception;

		}
		#endregion


		#region Rental Page Cache Objects

		#region private class RentalPageCacheList: List<RentalPageCache>
		/// <summary>
		/// 
		/// </summary>
		[Serializable]
		private class RentalPageCacheList: List<RentalPageCache>
		{
			public string UserId
			{
				get;
				set;
			}

			public RentalPageCacheList ( string userId )
			{
				UserId = userId;
			}

			public RentalPageCache GetRentalPageCache ( string userId, int pageNumber )
			{
				return this.Find(delegate( RentalPageCache rentalPageCache ) { return rentalPageCache != null && rentalPageCache.UserId == userId && rentalPageCache.PageNumber == pageNumber; });
			}

			public void AddRentalPageCache ( string userId, int pageNumber, RentalResult rentalResult )
			{
				if (userId == UserId)
				{
					RentalPageCache rentalPageCache = GetRentalPageCache(userId, pageNumber);
					if (rentalPageCache == null)
					{
						this.Add(new RentalPageCache { PageNumber = pageNumber, UserId = userId, RentalResult = rentalResult });
					}
					else
					{
						rentalPageCache.RentalResult = rentalResult;
					}
				}
			}
		}
		#endregion


		#region private class RentalPageCache
		/// <summary>
		/// 
		/// </summary>
		[Serializable]
		private class RentalPageCache
		{
			public RentalResult RentalResult
			{
				get;
				set;
			}
			public int PageNumber
			{
				get;
				set;
			}
			public string UserId
			{
				get;
				set;
			}

		}
		#endregion

		#endregion

	}
}