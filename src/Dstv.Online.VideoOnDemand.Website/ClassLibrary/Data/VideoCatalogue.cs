﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Settings.Types;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using VideoModule = Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess
{
	// TODO: validate this:  Currently providing test data. Will update once VideoCatalogue MidTier is done and understood.

	#region public class VideoData
	/// <summary>
	/// 
	/// </summary>
	public sealed class VideoData
	{

		#region public static CatalogueResult GetVideoItems ( SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoDataType"></param>
		/// <param name="videoFilter"></param>
		/// <returns></returns>
		public static CatalogueResult GetVideoItems ( SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter )
		{
			// Use VideoDataType to determine caching options, local memory (Application Cache), MemCache or no caching (control will do page output caching).
			CatalogueResult catalogueResult = VideoModule.VideoCatalogue.GetVideoItems(videoFilter.CatalogueCriteria);
			RemoveFutureDatedCatchUpVideos(catalogueResult);
			return catalogueResult;
		}
		#endregion


		#region public static VideoModule.EditorialPlaylist GetEditorialPlaylist ( SiteEnums.VideoDataType videoDataType, string editorialPlaylistId, VideoFilter videoFilter )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoDataType"></param>
		/// <param name="editorialPlaylistId"></param>
		/// <param name="videoFilter"></param>
		/// <returns></returns>
		public static VideoModule.EditorialPlaylist GetEditorialPlaylist ( SiteEnums.VideoDataType videoDataType, string editorialPlaylistId, VideoFilter videoFilter )
		{
			// Use VideoDataType to determine caching options, local memory (Application Cache), MemCache or no caching (control will do page output caching).
			return GetEditorialPlaylist(videoDataType, editorialPlaylistId, videoFilter.CatalogueCriteria);
		}
		#endregion


		#region public static VideoModule.EditorialPlaylist GetEditorialPlaylist ( SiteEnums.VideoDataType videoDataType, string editorialPlaylistId, CatalogueCriteria catalogueCriteria )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoDataType"></param>
		/// <param name="editorialPlaylistId"></param>
		/// <param name="catalogueCriteria"></param>
		/// <returns></returns>
		public static VideoModule.EditorialPlaylist GetEditorialPlaylist ( SiteEnums.VideoDataType videoDataType, string editorialPlaylistId, CatalogueCriteria catalogueCriteria )
		{
			// Use VideoDataType to determine caching options, local memory (Application Cache), MemCache or no caching (control will do page output caching).
			VideoModule.EditorialPlaylist editorialPlaylist = VideoModule.VideoCatalogue.GetEditorialList(editorialPlaylistId, catalogueCriteria);
			RemoveFutureDatedCatchUpVideos(editorialPlaylist.CatalogueItemSummaryList);
			return editorialPlaylist;
		}
		#endregion


		#region public static CatalogueResult GetVideoItemsForSearchTerm ( SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter, string searchTerm )
		/// <summary>
		/// Returns a CatalogueResult for a given search terms and filter criteria
		/// </summary>
		/// <param name="videoDataType"></param>
		/// <param name="videoFilter"></param>
		/// <param name="searchTerm"></param>
		/// <returns></returns>
		public static CatalogueResult GetVideoItemsForSearchTerm ( SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter, string searchTerm )
		{
            return GetVideoItemsForSearchTerm(videoDataType, videoFilter, searchTerm, null);
		}
		#endregion


        #region public static CatalogueResult GetVideoItemsForSearchTerm ( SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter, string searchTerm, List<string> excludedVideoIds )
        /// <summary>
        /// Returns a CatalogueResult for a given search terms and filter criteria
        /// </summary>
        /// <param name="videoDataType"></param>
        /// <param name="videoFilter"></param>
        /// <param name="searchTerm"></param>
        /// <param name="productIdList"></param>
        /// <returns></returns>
        public static CatalogueResult GetVideoItemsForSearchTerm(SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter, string searchTerm, List<int> productIdList)
        {
            // Use VideoDataType to determine caching options, local memory (Application Cache), MemCache or no caching (control will do page output caching).
            CatalogueResult catalogueResult = VideoModule.VideoCatalogue.SearchCatalogForSearchTerm(videoFilter.CatalogueCriteria, searchTerm, productIdList);
            RemoveFutureDatedCatchUpVideos(catalogueResult);
            return catalogueResult;
        }
        #endregion


        #region public CatalogueSearchResult GetRelatedVideoItems(SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList)
        /// <summary>
        /// Returns a CatalogueResult for a given video's keywords terms and filter criteria
        /// </summary>
        /// <param name="videoDataType"></param>
        /// <param name="videoFilter"></param>
        /// <param name="relatedVideoId"></param>
        /// <param name="videoKeywords"></param>
        /// <param name="excludedProgramId"></param>
        /// <param name="genre"></param>
        /// <param name="subGenre"></param>
        /// <param name="productIdList"></param>
        /// <returns></returns>
        public static CatalogueResult GetRelatedVideoItems(SiteEnums.VideoDataType videoDataType, VideoFilter videoFilter, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList)
        {
            // Use VideoDataType to determine caching options, local memory (Application Cache), MemCache or no caching (control will do page output caching).
            CatalogueResult catalogueResult = VideoModule.VideoCatalogue.SearchCatalogForRelated(videoFilter.CatalogueCriteria, relatedVideoId, videoKeywords, excludedProgramId, genre, subGenre, productIdList);
            RemoveFutureDatedCatchUpVideos(catalogueResult);
            return catalogueResult;
        }
        #endregion

		#region public static List<VideoClassificationForSection> GetVideoClassificationHierarchy ( SiteEnums.VideoDataType videoDataType, string configFile )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoDataType"></param>
		/// <param name="configFile"></param>
		/// <returns></returns>
		public static VideoClassificationForSite GetVideoClassificationHierarchy ( SiteEnums.VideoDataType videoDataType, string configFile )
		{
			// Doing in memory caching of classification hierarchy.
			string cacheKey = string.Format("VideoClassificationHierarchy_{0}", configFile);
			object cachedObject = WebsiteSettings.CacheStore.GetObject(cacheKey);
			if (cachedObject != null)
			{
				return (VideoClassificationForSite)cachedObject;
			}

			// If not cached.

			// Get Configuration via custom config section handler.
			VideoFilterSettings videoFilterSettings = WebsiteSettings.VideoFilterSetting.GetVideoFilterSettings(configFile);

			VideoClassificationForSite videoClassificationForSite = new VideoClassificationForSite(videoFilterSettings);

			WebsiteSettings.CacheStore.AddObjectWithSlidingExpiration(cacheKey, videoClassificationForSite, 10, WebsiteSettings.CacheStore.CachedItemPriority.Medium);

			return videoClassificationForSite;
		}
		#endregion


		#region public static VideoClassificationItem GetVideoClassification(string classificationId)
		/// <summary>
		/// Gets classification for a given classification Id
		/// </summary>
		/// <param name="classificationId"></param>
		/// <returns></returns>
		public static VideoClassificationItem GetVideoClassification ( string classificationId )
		{
			ClassificationCriteria classificationCriteria = new ClassificationCriteria();
			classificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(classificationId);

			List<IClassificationItem> classificationItems = VideoModule.VideoCatalogue.GetCatalogueClassification(classificationCriteria);
			if (classificationItems == null)
			{
				return null;
			}
			else
			{
				IClassificationItem classificationItem = classificationItems[0];
				VideoClassificationItem videoClassificationItem = new VideoClassificationItem(
					 classificationItem.ClassificationItemId.Value,
					 classificationItem.Name,
                    SiteEnums.VideoClassificationType.Unknown,
					// SiteEnums.Mapper.MapBaseCatalogueItemClassificationTypeToVideoClassificationType(classificationItem.ClassificationType),
					 false,
					 classificationItem.TotalNumberOfCatalogueItems,
					 1
					 );

				return videoClassificationItem;
			}
		}
		#endregion


		#region public static List<VideoModule.ProgramChannel> GetChannels(string videoId)
		/// <summary>
		/// Returns the releated channels 
		/// </summary>
		/// <param name="videoId"></param>
		/// <returns></returns>
		public static List<VideoModule.ProgramChannel> GetChannels ( string videoId )
		{
			string cacheKey = string.Format("GetChannels{0}", videoId);
			object cachedObject = WebsiteSettings.CacheStore.GetObject(cacheKey);
			if (cachedObject != null)
			{
				return (List<VideoModule.ProgramChannel>)cachedObject;
			}

			List<VideoModule.ProgramChannel> channels = VideoModule.VideoCatalogue.GetChannelsByCatalogueId(new CatalogueItemIdentifier(videoId));

			if (channels.Count > 0)
			{
				WebsiteSettings.CacheStore.AddObjectWithSlidingExpiration(cacheKey, channels, 10, WebsiteSettings.CacheStore.CachedItemPriority.Medium);
			}

			return channels;
		}
		#endregion


		#region public static VideoModule.ProgramResult GetVideoDetailsForProgram ( string videoId, VideoProgramType primaryVideoType )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoId"></param>
		/// <param name="primaryVideoType"></param>
		/// <returns></returns>
		public static VideoModule.ProgramResult GetVideoDetailsForProgram ( string videoId, VideoProgramType primaryVideoType )
		{
            return GetVideoDetailsForProgram(videoId, primaryVideoType, false);
		}
		#endregion
        
		#region public static VideoModule.ProgramResult GetVideoDetailsForProgram ( string videoId, VideoProgramType primaryVideoType, bool isForthcoming )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoId"></param>
		/// <param name="primaryVideoType"></param>
		/// <returns></returns>
        public static VideoModule.ProgramResult GetVideoDetailsForProgram(string videoId, VideoProgramType primaryVideoType, bool isForthcoming)
		{
			// Doing a sliding cache. Once we get to video detail the data will potentially be displayed a number of times within a few minutes. 
			// Rent, play etc.

			string cacheKey = string.Format("VideoDetailsForProgram_{0}_ForthComing{1}", videoId, isForthcoming);
			object cachedObject = WebsiteSettings.CacheStore.GetObject(cacheKey);
			if (cachedObject != null)
			{
				return (VideoModule.ProgramResult)cachedObject;
			}

			VideoModule.ProgramResult programResult = VideoModule.VideoCatalogue.GetProgramVideoDetail(new CatalogueItemIdentifier(videoId), primaryVideoType, isForthcoming);

			if (programResult != null)
			{
				WebsiteSettings.CacheStore.AddObjectWithSlidingExpiration(cacheKey, programResult, 10, WebsiteSettings.CacheStore.CachedItemPriority.Medium);
			}

			return programResult;
		}
		#endregion


		#region private static void RemoveFutureDatedCatchUpVideos ( CatalogueResult catalogueResult )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="catalogueResult"></param>
		private static void RemoveFutureDatedCatchUpVideos ( CatalogueResult catalogueResult )
		{
			List<ICatalogueItem> videos = catalogueResult.CatalogueItems.ToList<ICatalogueItem>();
			videos =	videos.FindAll(delegate( ICatalogueItem catalogueItem )
				{
					return catalogueItem.ProductTypeId.Value != ProductTypes.CatchUp || (
						catalogueItem.ProductTypeId.Value == ProductTypes.CatchUp && catalogueItem.StartDate.HasValue && catalogueItem.StartDate <= DateTime.Now
						);
				}
			);

			catalogueResult.CatalogueItems = videos.ToArray<ICatalogueItem>();
		}
		#endregion


	}
	#endregion



}