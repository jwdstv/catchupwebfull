﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using DStvConnect.WebUI.Platform;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using Dstv.Online.VideoOnDemand.Common;
using System.Web.Security;
using DStvConnect.WebUI.Controls;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data
{
	public static class DstvUser
	{

		public static bool IsAuthenticated
		{
			get { return HttpContext.Current.User.Identity.IsAuthenticated && UserProfile != null; }
		}
		/// <summary>
		/// Used to retrieve user details, Wallet Balance 
		/// and the CustomerId
		/// </summary>
		public static IProfileInformation UserProfile
		{
			get { return IntegrationBroker.Instance.UserProfile.GetCurrentUserProfile(); }
		}

		public static DStvPrincipal UserDStvPrincipal
		{
			get { return (DStvPrincipal)HttpContext.Current.User; }
		}

		/// <summary>
		/// Used to purchase rentals etc.
		/// </summary>
		public static DstvWallet Wallet
		{
			get { return IntegrationBroker.Instance.DstvWallet; }
		}

		/// <summary>
		/// User's current territory, used
		/// to identify currency and access.
		/// </summary>
		public static ITerritory Territory
		{
			get
			{
			    try
			    {
			        ITerritory territory = IntegrationBroker.Instance.Geotargeting.ResolveUserTerritory();
			        return territory;
			    }
			    catch (Exception)
			    {
			        return null;
			    }
			}
		}

		/// <summary>
		/// The current territory's currency.
		/// </summary>
		public static CurrencyIdentifier Currency
		{
			get { return new CurrencyIdentifier(Territory.TerritoryId.Value); }
		}

		// Public methods so that this can be switched with DStvPrincipal when the time comes.

		public static CustomerIdentifier CustomerId
		{
			get { return UserProfile.CustomerId; }
		}

		public static bool HasWallet
		{
			get { return UserProfile.HasWallet; }
		}

		public static CurrencyAmount WalletBalance
		{
			get { return UserProfile.WalletBalance; }
		}

		public static string UserName
		{
			get { return UserProfile.UserName; }
		}

		public static string Email
		{
			get { return UserProfile.Email; }
		}

        //public static bool HasDesktopPlayer
        //{
        //    // get { return IntegrationBroker.Instance.DesktopPlayer.IsDesktopPlayerInstalled(); }
        //    get { return UserProfile.IsPlayerInstalled; }
        //}

        public static bool IsSmartCardLinked
        {
            get { return UserProfile.IsSmartCardLinked; }
        }

        public static bool IsPremiumDstvSubscriber
        {
            get { return UserProfile.IsPremiumDstvSubscriber; }
        }

		public static bool IsLinkedToISP
		{
			get { return UserProfile.IsLinkedToISP; }
		}


		public static ResponseTypes.LoginResponse LoginUser ( string userName, string password, bool rememberMe )
		{
			string mobile = "", connectId = ""; // TODO: needs to come from connect

			ResponseTypes.LoginResponse loginResponse = new ResponseTypes.LoginResponse();

			loginResponse.Success = true;
			loginResponse.ErrorMsg = "";

			try
			{
				// initialize FormsAuthentication
				FormsAuthentication.Initialize();

				// base initializer to store an indication of whether the login should be persisted. always true
				bool isPersistent = rememberMe;

				// create the authentication ticket
				FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
						  1, // the ticket version
						  userName, // the name of the ticket
						  DateTime.Now, // local date and time ticket was initialized
						  DateTime.Now.AddDays(30), // local date and time when ticket expires. default is 30 days
						  isPersistent, // should the ticket be persisted
						  connectId + ",0," + mobile + "," + isPersistent, // extra user data to be stored in the ticket. default is roles, but we don't have roles
						  FormsAuthentication.FormsCookiePath // cookie path
					 );

				// the ticket's content needs to be hashed as a security measurement
				// ticket should then be cookied
				string ticket_hash = FormsAuthentication.Encrypt(ticket);
				HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticket_hash);

				// if the ticket was set to be persisted, should we persist the cookie for the same duration
				if (ticket.IsPersistent)
					cookie.Expires = ticket.Expiration;

				// store the cookie to the current context
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
			catch (Exception ex)
			{
				loginResponse.Success = false;
				loginResponse.ErrorMsg = ex.Message;
			}

			return loginResponse;
		}


		#region public static ResponseTypes.TransactionResponse EntitleVideo ( string videoId )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoId"></param>
		/// <returns></returns>
        [Obsolete("Relook at entitling single video logic")]
		public static ResponseTypes.TransactionResponse EntitleVideo ( string videoId )
		{
			ResponseTypes.TransactionResponse entitlementResponse = new ResponseTypes.TransactionResponse();

			try
			{
				// Should we get catalogue item and check that it's CatchUp i.e. prevent creating free entitlement for BoxOffice ??
                TransactionResult transactionResult = IntegrationBroker.Instance.Entitlement.GenerateVideoEntitlement(CustomerId, new MediaCatalogue.CatalogueItemIdentifier(videoId));

				entitlementResponse.Success = transactionResult.Result == OperationResults.Success;
                entitlementResponse.ReferenceNo = transactionResult.ResultReference != null ? transactionResult.ResultReference.Value : string.Empty;
                entitlementResponse.ErrorMsg = transactionResult.Details != null ? transactionResult.Details : string.Empty;

				if (!entitlementResponse.Success)
				{
					Logger logger = new Logger();
					// Log exception
					string supportReferenceNumber = logger.LogError(
						Config.EventLogSource.CatchUp,
						string.Format("Error generating entitlement for video (id: {0}). transactionResult.ResultReference: {1}, transactionResult.Details: {2}", 
							videoId, 
							entitlementResponse.ReferenceNo, 
							entitlementResponse.ErrorMsg),
						HttpContext.Current.Request,
                        "DstvUser.EntitleVideo",
						DstvUser.Email,
						DstvUser.UserName,
						DstvUser.CustomerId.Value
						);
				}
			}
			catch (Exception exception)
			{
				Logger logger = new Logger();
				// Log exception
				string supportReferenceNumber = logger.LogException(
					Config.EventLogSource.CatchUp,
					exception,
					HttpContext.Current.Request,
                    "DstvUser.EntitleVideo",
                    DstvUser.Email,
                    DstvUser.UserName,
                    DstvUser.CustomerId.Value
					);

				string errorMessage = "SystemError";
				entitlementResponse.Success = false;
				entitlementResponse.ReferenceNo = supportReferenceNumber;

                if (HttpContext.Current.IsDebuggingEnabled)
                {
                    errorMessage = string.Format("{1}{0}{0}DEBUG:{0}{2}",
                        Environment.NewLine,
                        errorMessage,
                        Logger.GetExceptionAsString(exception)
                        );
                }

				entitlementResponse.ErrorMsg = errorMessage;
			}

			return entitlementResponse;	
		}
		#endregion


        public static ResponseTypes.TransactionResponse EntitleCatchupContent()
        {
            ResponseTypes.TransactionResponse entitlementResponse = new ResponseTypes.TransactionResponse();

            try
            {
                TransactionResult transactionResult = null;
                
                if (IsPremiumDstvSubscriber)
                {
                    transactionResult = IntegrationBroker.Instance.Entitlement.AuthorizeCatchupPackages(CustomerId);
                }
                else
                {
                    transactionResult = IntegrationBroker.Instance.Entitlement.DeauthorizeCatchupPackages(CustomerId);
                }

                string entitledString = SessionManager.GetSession("IsEntitled");

                if (!string.IsNullOrWhiteSpace(entitledString))
                {
                    bool.TryParse(entitledString, out entitlementResponse.Success);
                }

                entitlementResponse.ReferenceNo = transactionResult.ResultReference != null ? transactionResult.ResultReference.Value : string.Empty;
                entitlementResponse.ErrorMsg = transactionResult.Details != null ? transactionResult.Details : string.Empty;

                if (transactionResult.Result != OperationResults.Success)
                {
                    Logger logger = new Logger();
                    // Log exception
                    string supportReferenceNumber = logger.LogError(
                        Config.EventLogSource.CatchUp,
                        string.Format("Error generating entitlement for CatchUp packages. transactionResult.ResultReference: {0}, transactionResult.Details: {1}",
                            entitlementResponse.ReferenceNo,
                            entitlementResponse.ErrorMsg),
                        HttpContext.Current.Request,
                        "DstvUser.EntitleCatchupContent",
                        DstvUser.Email,
                        DstvUser.UserName,
                        DstvUser.CustomerId.Value
                        );
                }
            }
            catch (Exception exception)
            {
                Logger logger = new Logger();
                // Log exception
                string supportReferenceNumber = logger.LogException(
                    Config.EventLogSource.CatchUp,
                    exception,
                    HttpContext.Current.Request,
                    "DStvUser.EntitleCatchupContent",
                    DstvUser.Email,
                    DstvUser.UserName,
                    DstvUser.CustomerId.Value
                    );

                string errorMessage = "SystemError";
                entitlementResponse.Success = false;
                entitlementResponse.ReferenceNo = supportReferenceNumber;

                if (HttpContext.Current.IsDebuggingEnabled)
                {
                    errorMessage = string.Format("{1}{0}{0}DEBUG:{0}{2}",
                        Environment.NewLine,
                        errorMessage,
                        Logger.GetExceptionAsString(exception)
                        );
                }

                entitlementResponse.ErrorMsg = errorMessage;
            }

            return entitlementResponse;
        }


		#region public static ResponseTypes.PaymentResponse RentVideo ( string catalogItemId, decimal catalogItemCost, string customerPin )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="catalogItemId"></param>
		/// <param name="catalogItemCost"></param>
		/// <param name="customerPin"></param>
		/// <returns></returns>
		public static ResponseTypes.TransactionResponse RentVideo ( string catalogItemId, decimal catalogItemCost, string customerPin )
		{
            return RentVideo(catalogItemId, catalogItemCost, customerPin, null);
		}
		#endregion


        #region public static ResponseTypes.PaymentResponse RentVideo ( string catalogItemId, decimal catalogItemCost, string customerPin, string resultRef )
        /// <summary>
		/// 
		/// </summary>
		/// <param name="catalogItemId"></param>
		/// <param name="catalogItemCost"></param>
		/// <param name="customerPin"></param>
		/// <param name="resultRef"></param>
		/// <returns></returns>
        public static ResponseTypes.TransactionResponse RentVideo(string catalogItemId, decimal catalogItemCost, string customerPin, string resultRef)
		{
			ResponseTypes.TransactionResponse paymentResponse = new ResponseTypes.TransactionResponse();

			try
			{
				Rentals.RentVideoResult rentVideoResult = Rentals.RentVideoForCurrentUser(catalogItemId, catalogItemCost, customerPin, resultRef);

				paymentResponse.Success = rentVideoResult.Success;
				paymentResponse.ReferenceNo = rentVideoResult.ReferenceNo;
				paymentResponse.ErrorMsg = rentVideoResult.Message;

				if (!rentVideoResult.Success && rentVideoResult.Exception != null)
				{
					throw rentVideoResult.Exception;
				}
			}
			catch (Exception exception)
			{
				Logger logger = new Logger();
				// Log exception
				string supportReferenceNumber = logger.LogException(
					Config.EventLogSource.BoxOffice,
					exception,
					HttpContext.Current.Request,
					"DStvUser.RentVideo",
                    DstvUser.Email,
                    DstvUser.UserName,
                    DstvUser.CustomerId.Value
					);

				string errorMessage = "SystemError";
				paymentResponse.Success = false;
				paymentResponse.ReferenceNo = supportReferenceNumber;

                if (HttpContext.Current.IsDebuggingEnabled)
                {
                    errorMessage = string.Format("{1}{0}{0}DEBUG:{0}{2}",
                    Environment.NewLine,
                    errorMessage,
                    Logger.GetExceptionAsString(exception)
                    );
                }
                
                paymentResponse.ErrorMsg = errorMessage;
			}

			return paymentResponse;
		}
		#endregion

	}
}