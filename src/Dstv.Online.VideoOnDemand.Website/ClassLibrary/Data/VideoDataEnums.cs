﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess
{

	#region public enum VideoDataType
	/// <summary>
	/// 
	/// </summary>
	public enum VideoDataType
	{
		List,
		Summary,
		Gallery,
		Search,
		Detail,
		ClassificationList,
		ClassificationHierarchy
	}
	#endregion


	#region public enum VideoClassificationType
	/// <summary>
	/// 
	/// </summary>
	public enum VideoClassificationType
	{
		ProgramType_All,
		ProgramType_Type,
		ProgramType_Genre,
		Item_Editorial,
		Item_TvShows,
		Item_TvChannels,
		Item_Categories,
		Item_Link,
		Unknown
	}
	#endregion

}