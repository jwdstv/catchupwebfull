﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Settings.Types;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using VideoModule = Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;


namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess
{

	#region public class VideoClassificationItem
	/// <summary>
	/// 
	/// </summary>
	public class VideoClassificationItem
	{

		#region Id
		private string _id;
		/// <summary>
		/// 
		/// </summary>
		public string Id
		{
			get { return _id; }
			set { _id = value; }
		}
		#endregion


		#region Name
		private string _name;
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		#endregion


		#region ClassificationType
		private SiteEnums.VideoClassificationType _classificationType;
		/// <summary>
		/// 
		/// </summary>
		public SiteEnums.VideoClassificationType ClassificationType
		{
			get { return _classificationType; }
			set { _classificationType = value; }
		}
		#endregion


		#region FormattedUrl
		private string _formattedUrl;
		/// <summary>
		/// 
		/// </summary>
		public string FormattedUrl
		{
			get { return _formattedUrl; }
		}
		#endregion

	    public SiteEnums.ProductType SiteProductType
	    {
	        get { return SiteUtils.Request.GetProductTypeFromRawUrl(FormattedUrl); }
	    }


		#region DisplayForLoggedInUserOnly
		private bool _displayForLoggedInUserOnly = false;
		/// <summary>
		/// 
		/// </summary>
		public bool DisplayForLoggedInUserOnly
		{
			get { return _displayForLoggedInUserOnly; }
			set { _displayForLoggedInUserOnly = value; }
		}
		#endregion


		#region VideoCount
		private int _videoCount;
		/// <summary>
		/// 
		/// </summary>
		public int VideoCount
		{
			get { return _videoCount; }
			set { _videoCount = value; }
		}
		#endregion


		#region Order
		private int _order = 0;
		/// <summary>
		/// 
		/// </summary>
		public int Order
		{
			get { return _order; }
		}
		#endregion


		#region SubItems
		private List<VideoClassificationItem> _subItems = new List<VideoClassificationItem>();
		/// <summary>
		/// 
		/// </summary>
		public List<VideoClassificationItem> SubItems
		{
			get { return _subItems; }
		}
		#endregion


        #region ParentId
        private string _parentId;
        /// <summary>
        /// 
        /// </summary>
        public string ParentId
        {
            get { return _parentId; }
        }
        #endregion


		#region public VideoClassificationItem ( string id, string name, VideoClassificationType classificationType, string compositeUrl, bool displayForLoggedInUserOnly, int videoCount )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="classificationType"></param>
		/// <param name="compositeUrl"></param>
		/// <param name="displayForLoggedInUserOnly"></param>
		/// <param name="videoCount"></param>
		public VideoClassificationItem ( string id, string name, SiteEnums.VideoClassificationType classificationType, string compositeUrl, bool displayForLoggedInUserOnly, int videoCount, int order, string parentId )
		{
			_id = id;
			_name = name;
			_classificationType = classificationType;
			_displayForLoggedInUserOnly = displayForLoggedInUserOnly;
			_videoCount = videoCount;
			_order = order;
            _parentId = parentId;

			SetFormattedUrl(compositeUrl);
		}
		#endregion


		#region public VideoClassificationItem ( string id, string name, VideoClassificationType classificationType, bool displayForLoggedInUserOnly, int videoCount )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="classificationType"></param>
		/// <param name="displayForLoggedInUserOnly"></param>
		/// <param name="videoCount"></param>
		public VideoClassificationItem ( string id, string name, SiteEnums.VideoClassificationType classificationType, bool displayForLoggedInUserOnly, int videoCount, int order )
		{
			_id = id;
			_name = name;
			_classificationType = classificationType;
			_displayForLoggedInUserOnly = displayForLoggedInUserOnly;
			_videoCount = videoCount;
			_formattedUrl = "";
			_order = order;
		}
		#endregion


		#region public void AddClassificationSubItem ( string id, string name, VideoClassificationType classificationType, string compositeUrl, bool displayForLoggedInUserOnly, int videoCount )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="classificationType"></param>
		/// <param name="compositeUrl"></param>
		/// <param name="displayForLoggedInUserOnly"></param>
		/// <param name="videoCount"></param>
		public void AddClassificationSubItem ( string id, string name, SiteEnums.VideoClassificationType classificationType, string compositeUrl, bool displayForLoggedInUserOnly, int videoCount, int order )
		{
			_subItems.Add(new VideoClassificationItem(id, name, classificationType, compositeUrl, displayForLoggedInUserOnly, videoCount, order, _id));
		}
		#endregion


		#region public void AddClassificationSubItem ( VideoClassificationItem videoClassificationItem, bool addToStart )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoClassificationItem"></param>
		/// <param name="addToStart"></param>
		public void AddClassificationSubItem ( VideoClassificationItem videoClassificationItem, bool addToStart )
		{
			if (addToStart)
			{
				_subItems.Insert(0, videoClassificationItem);
			}
			else
			{
				_subItems.Add(videoClassificationItem);
			}
		}
		#endregion


		#region public void AddClassificationSubItem ( IClassificationItem classificationItem, string compositeUrl, int order )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="classificationItem"></param>
		/// <param name="compositeUrl"></param>
		public void AddClassificationSubItem ( IClassificationItem classificationItem, string compositeUrl, int order )
		{
			_subItems.Add(
				new VideoClassificationItem(
					classificationItem.ClassificationItemId.Value,
					classificationItem.Name,
                    SiteEnums.VideoClassificationType.Unknown,
					//SiteEnums.Mapper.MapBaseCatalogueItemClassificationTypeToVideoClassificationType(classificationItem.ClassificationType),
					compositeUrl,
					false,
					classificationItem.TotalNumberOfCatalogueItems,
					order,
                    _id
					)
				);
		}
		#endregion


		#region private void SetFormattedUrl ( string compositeUrl )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="compositeUrl"></param>
		private void SetFormattedUrl ( string compositeUrl )
		{
			_formattedUrl = SiteUtils.Url.SEO.GetSEOFriendlyUrl(compositeUrl);

			_formattedUrl = _formattedUrl.
				Replace(SiteConstants.FormatIdentifiers.SubItemId, _id).
				Replace(SiteConstants.FormatIdentifiers.SubItemType, _classificationType.ToString()).
				Replace(SiteConstants.FormatIdentifiers.SubItemName, HttpUtility.UrlEncode(_name)).
				Replace(SiteConstants.FormatIdentifiers.ListId, _id).
				Replace(SiteConstants.FormatIdentifiers.ListTitle, HttpUtility.UrlEncode(_name)).
				Replace(SiteConstants.FormatIdentifiers.ClassificationName, HttpUtility.UrlEncode(_name));

            //remove parent name querystring parameter from url if the item has no parent
            if (_parentId == null)
            {
                _formattedUrl = System.Text.RegularExpressions.Regex.Replace(_formattedUrl, string.Format("[?&]{0}={1}", SiteConstants.Urls.ParameterNames.ParentClassificationId, SiteConstants.FormatIdentifiers.ParentId), string.Empty);
            }
            else
            {
                _formattedUrl = _formattedUrl.Replace(SiteConstants.FormatIdentifiers.ParentId, _parentId);
            }
		}
		#endregion


		#region public void GetTotalVideoCount ( ref int totalVideoCount )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="totalVideoCount"></param>
		public void GetTotalVideoCount ( ref int totalVideoCount )
		{
			if (_classificationType != SiteEnums.VideoClassificationType.Item_Link)
			{
				if (_subItems.Count == 0)
				{
					totalVideoCount += _videoCount;
				}
				else
				{
					foreach (VideoClassificationItem subItem in _subItems)
					{
						subItem.GetTotalVideoCount(ref totalVideoCount);
					}
				}
			}
		}
		#endregion


		#region public bool ContainsSubItem ( string subItemId )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="subItemId"></param>
		/// <param name="productType"></param>
		/// <returns></returns>
		public bool ContainsSubItem ( string subItemId, SiteEnums.ProductType productType, string parentItemId)
		{
			foreach (VideoClassificationItem subItem in _subItems)
			{
                if (subItemId.Equals(subItem.Id, StringComparison.OrdinalIgnoreCase) && subItem.SiteProductType == productType && parentItemId.Equals(subItem.ParentId, StringComparison.OrdinalIgnoreCase))
				{
					return true;
				}
			}

			return false;
		}
		#endregion


		#region public static int CompareByOrder ( VideoClassificationItem item1, VideoClassificationItem item2 )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="item1"></param>
		/// <param name="item2"></param>
		/// <returns></returns>
		public static int CompareByOrder ( VideoClassificationItem item1, VideoClassificationItem item2 )
		{
			return item1.Order.CompareTo(item2.Order);
		}
		#endregion


		#region public static int CompareByOrderThenByName ( VideoClassificationItem item1, VideoClassificationItem item2 )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="item1"></param>
		/// <param name="item2"></param>
		/// <returns></returns>
		public static int CompareByOrderThenByName ( VideoClassificationItem item1, VideoClassificationItem item2 )
		{
			int orderCompare = item1.Order.CompareTo(item2.Order);
			return orderCompare == 0 ? item1.Name.CompareTo(item2.Name) : orderCompare;
		}
		#endregion
	
	}
	#endregion


	#region public class VideoClassificationForSection : List<VideoClassificationItem>
	/// <summary>
	/// 
	/// </summary>
	public class VideoClassificationForSection : List<VideoClassificationItem>
	{

		#region ProductType
		private ProductTypes _productType;
		/// <summary>
		/// 
		/// </summary>
		public ProductTypes ProductType
		{
			get { return _productType; }
			set { _productType = value; }
		}
		#endregion


		#region Id
		private string _id;
		/// <summary>
		/// 
		/// </summary>
		public string Id
		{
			get { return _id; }
			set { _id = value; }
		}
		#endregion


		#region Name
		private string _name;
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		#endregion


		#region VideoTypesFilter
		private VideoProgramType[] _videoTypesFilter;
		/// <summary>
		/// 
		/// </summary>
		public VideoProgramType[] VideoTypesFilter
		{
			get { return _videoTypesFilter; }
			set { _videoTypesFilter = value; }
		}
		#endregion


		#region public VideoClassificationForSection ( ProductTypes productType, string id, string name )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="productType"></param>
		/// <param name="id"></param>
		/// <param name="name"></param>
		public VideoClassificationForSection ( ProductTypes productType, string id, string name )
		{
			_productType = productType;
			_id = id;
			_name = name;
		}
		#endregion


		#region public VideoClassificationForSection ( ProductTypes productType, string id, string name, VideoProgramType[] videoTypesFilter )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="productType"></param>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="videoTypesFilter"></param>
		public VideoClassificationForSection ( ProductTypes productType, string id, string name, VideoProgramType[] videoTypesFilter )
		{
			_productType = productType;
			_id = id;
			_name = name;
			_videoTypesFilter = videoTypesFilter;
		}
		#endregion


		#region public void AddClassificationSubItem ( string id, string name, VideoClassificationType classificationType, string compositeUrl, bool displayForLoggedInUserOnly, int videoCount )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="classificationType"></param>
		/// <param name="compositeUrl"></param>
		/// <param name="displayForLoggedInUserOnly"></param>
		/// <param name="videoCount"></param>
		public void AddClassificationSubItem ( string id, string name, SiteEnums.VideoClassificationType classificationType, string compositeUrl, bool displayForLoggedInUserOnly, int videoCount, int order )
		{
			this.Add(new VideoClassificationItem(id, name, classificationType, compositeUrl, displayForLoggedInUserOnly, videoCount, order, _id));
		}
		#endregion


		#region public void AddClassificationSubItem ( VideoClassificationItem videoClassificationItem, bool addToStart )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="videoClassificationItem"></param>
		/// <param name="addToStart"></param>
		public void AddClassificationSubItem ( VideoClassificationItem videoClassificationItem, bool addToStart )
		{
			if (addToStart)
			{
				this.Insert(0, videoClassificationItem);
			}
			else
			{
				this.Add(videoClassificationItem);
			}
		}
		#endregion


		#region public int GetTotalVideoCount ()
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int GetTotalVideoCount ()
		{
			int totalVideoCount = 0;
			foreach (VideoClassificationItem item in this)
			{
				item.GetTotalVideoCount(ref totalVideoCount);
			}
			return totalVideoCount;
		}
		#endregion


		#region public List<VideoClassificationItem> GetFirstLevelItems ( bool includeItemsWithVideos, bool includeItemsWithSubItems, bool userIsLoggedIn )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="includeItemsWithVideos"></param>
		/// <param name="includeItemsWithSubItems"></param>
		/// <param name="userIsLoggedIn"></param>
		/// <returns></returns>
		public List<VideoClassificationItem> GetFirstLevelItems ( bool includeItemsWithVideosTopOrder, bool includeItemsWithVideosBottomOrder, bool includeItemsWithSubItems, bool userIsLoggedIn )
		{
			List<VideoClassificationItem> firstLevelList = new List<VideoClassificationItem>();

			if (!includeItemsWithSubItems && !includeItemsWithVideosTopOrder && !includeItemsWithVideosBottomOrder)
			{
				return firstLevelList;
			}

			int itemWithSubItemsStartOrder = GetItemsWithSubItemsStartOrder();
			if (!includeItemsWithVideosTopOrder && !includeItemsWithVideosBottomOrder && includeItemsWithSubItems && itemWithSubItemsStartOrder < 0)
			{
				return firstLevelList;
			}

			for (int i = 0; i < this.Count; i++)
			{
				if (includeItemsWithVideosTopOrder && (itemWithSubItemsStartOrder < 0 || (itemWithSubItemsStartOrder >= 0 && this[i].Order < itemWithSubItemsStartOrder)))
				{
					if (this[i].VideoCount > 0 ||
							(this[i].ClassificationType == SiteEnums.VideoClassificationType.Item_Link && (!this[i].DisplayForLoggedInUserOnly || (this[i].DisplayForLoggedInUserOnly && userIsLoggedIn))))
					{
						firstLevelList.Add(this[i]);
					}
				}

				if (includeItemsWithSubItems && this[i].SubItems.Count > 0)
				{
					firstLevelList.Add(this[i]);
				}

				if (includeItemsWithVideosBottomOrder && itemWithSubItemsStartOrder >= 0 && this[i].Order > itemWithSubItemsStartOrder)
				{
					if (this[i].VideoCount > 0 ||
							(this[i].ClassificationType == SiteEnums.VideoClassificationType.Item_Link && (!this[i].DisplayForLoggedInUserOnly || (this[i].DisplayForLoggedInUserOnly && userIsLoggedIn))))
					{
						firstLevelList.Add(this[i]);
					}
				}
			}

			return firstLevelList;
		}
		#endregion


		#region private int GetItemsWithSubItemsStartOrder ()
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		private int GetItemsWithSubItemsStartOrder ()
		{
			for (int i = 0; i < this.Count; i++)
			{
				if (this[i].SubItems.Count > 0)
				{
					return this[i].Order;
				}
			}

			return -1;
		}
		#endregion


		#region public bool ContainsItemsOrSubItem ( string itemId )
		/// <summary>
		/// 
		/// </summary>
		/// <param name="itemId"></param>
		/// <returns></returns>
		public bool ContainsItemsOrSubItem ( string itemId, SiteEnums.ProductType productType, string parentId )
		{
			foreach (VideoClassificationItem item in this)
			{
				if (itemId.Equals(item.Id, StringComparison.OrdinalIgnoreCase) && item.SiteProductType == productType)
				{
					return true;
				}
                else if (item.ContainsSubItem(itemId, productType, parentId ?? string.Empty))
				{
					return true;
				}
			}

			return false;
		}
		#endregion


		public void ReadVideoClassifications ( VideoFilterSettingsSection filterSection )
		{
			ClassificationCriteria classificationCriteria = new ClassificationCriteria();
			classificationCriteria.ClassificationType = CatalogueItemClassificationTypes.ProductType;
			classificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(Id);
			classificationCriteria.VideoProgramTypes = VideoTypesFilter;
		    classificationCriteria.ProductTypeId = new ProductTypeIdentifier(ProductType);
			classificationCriteria.TerritoryId = DstvUser.Territory.TerritoryId; // new Dstv.Online.VideoOnDemand.Globalization.TerritoryIdentifier("{6D0DF4EB-663A-4f76-B46A-4B02EBE57DEC}");

			// Read the ProgramType sub items for this list.
			VideoClassificationItem allClassificationItem = null;

			#region // ProgramTypes

			if (filterSection.ProgramType != null && filterSection.ProgramType.Display)
			{
				if (filterSection.ProgramType.DisplayAllItemsLink)
				{
					// Add All Videos link. Video count ??
					allClassificationItem = new VideoClassificationItem("", string.Format("All {0}", Name), SiteEnums.VideoClassificationType.ProgramType_All, filterSection.ProgramType.SubItemUrl, false, 0, filterSection.ProgramType.Order - 1, _id);
					AddClassificationSubItem(allClassificationItem, true);
				}

				List<IClassificationItem> classificationList = VideoModule.VideoCatalogue.GetCatalogueClassification(classificationCriteria);
				bool checkFilter = filterSection.ProgramType.FilterArray.Length > 0 && !filterSection.ProgramType.FilterArray.Contains<string>("All");
				for (int i = classificationList.Count - 1; i >= 0; i--)
				{
				    bool isInvalidItem = ((checkFilter &&
				                           !filterSection.ProgramType.FilterArray.Contains<string>(classificationList[i].Name)));
				                         // ||
				                         // classificationList[i].ClassificationType != CatalogueItemClassificationTypes.ProgramType);
                    if (isInvalidItem)
					{
						classificationList.RemoveAt(i);
					}
				}

				// Loop through items and read sub-items. If only 1 item will then add sub-items at first level.
				foreach (IClassificationItem classificationItem in classificationList)
				{
					VideoClassificationItem programItem = new VideoClassificationItem(
						classificationItem.ClassificationItemId.Value,
						classificationItem.Name,
						SiteEnums.VideoClassificationType.ProgramType_Type,
						false,
						classificationItem.TotalNumberOfCatalogueItems,
						filterSection.ProgramType.Order
						);

					// Get sub-items
					if (classificationItem.HasAdditionalClassification)
					{
						classificationCriteria.ClassificationItemId = classificationItem.ClassificationItemId;
						//classificationCriteria.ClassificationType = classificationItem.ClassificationType;

						List<IClassificationItem> subClassificationList = VideoModule.VideoCatalogue.GetCatalogueClassification(classificationCriteria);

						foreach (IClassificationItem classificationSubItem in subClassificationList)
						{
							// Only going to a max of two levels on ProgramType and therefore expect catalogue items (videos) at this level.
							if (classificationSubItem.TotalNumberOfCatalogueItems > 0)
							{
								programItem.AddClassificationSubItem(classificationSubItem, filterSection.ProgramType.SubItemUrl, filterSection.ProgramType.Order);
							}
						}
					}

					if (classificationList.Count == 1)
					{
						// If only 1 item will then add sub-items at first level.
						foreach (VideoClassificationItem programTypeSubItem in programItem.SubItems)
						{
							AddClassificationSubItem(programTypeSubItem, false);

						}
						break;
					}
					else if (programItem.SubItems.Count > 0)
					{
						AddClassificationSubItem(programItem, false);
					}
				}
			}

			#endregion

			#region // Editorial Items

			if (filterSection.ItemEditorials.Count > 0)
			{
				CatalogueCriteria catalogueCriteria = new CatalogueCriteria();
				catalogueCriteria.ClassificationCriteria = classificationCriteria;
				catalogueCriteria.ClassificationCriteria.ClassificationItemId = null;
				catalogueCriteria.ClassificationCriteria.ParentClassificationId = null;

				foreach (VideoFilterSettingsSectionItemEditorial itemEditorial in filterSection.ItemEditorials)
				{
					if (!string.IsNullOrEmpty(itemEditorial.ListId))
					{
						try
						{
							VideoModule.EditorialPlaylist editorialPlaylist = VideoData.GetEditorialPlaylist(SiteEnums.VideoDataType.List, itemEditorial.ListId, catalogueCriteria);
							if (editorialPlaylist != null && editorialPlaylist.CatalogueItemSummaryList.TotalResultCount > 0)
							{
								AddClassificationSubItem(
									itemEditorial.ListId,
									editorialPlaylist.PlaylistName,
									SiteEnums.VideoClassificationType.Item_Editorial,
									itemEditorial.Url,
									false,
									editorialPlaylist.CatalogueItemSummaryList.TotalResultCount,
									itemEditorial.Order
									);
							}
						}
						catch
						{
						}
					}
				}
			}

			#endregion

			#region // Item

			foreach (VideoFilterSettingsSectionItem filterItem in filterSection.Items)
			{
				VideoClassificationItem programItem = new VideoClassificationItem(
						filterItem.ClassificationId,
						filterItem.Title,
						SiteEnums.Mapper.MapBaseCatalogueItemClassificationTypeToVideoClassificationType(filterItem.ClassificationType),
						false,
						0,
						filterItem.Order
						);

				classificationCriteria.ClassificationItemId = new ClassificationItemIdentifier(filterItem.ClassificationId);
				classificationCriteria.ClassificationType = filterItem.ClassificationType;

				// Set new ClassificationCriteria
				List<IClassificationItem> subClassificationList = VideoModule.VideoCatalogue.GetCatalogueClassification(classificationCriteria);
				foreach (IClassificationItem classificationSubItem in subClassificationList)
				{
					// Only going to a max of two levels on ProgramType and therefore expect catalogue items (videos) at this level.
					if (classificationSubItem.TotalNumberOfCatalogueItems > 0)
					{
						programItem.AddClassificationSubItem(classificationSubItem, filterItem.SubItemUrl, 0);
					}
				}

				if (programItem.SubItems.Count > 0)
				{
					AddClassificationSubItem(programItem, false);
				}
			}

			#endregion

			#region // Item Links

			foreach (VideoFilterSettingsSectionItemLink filterItemLink in filterSection.ItemLinks)
			{
				AddClassificationSubItem(
					"",
					filterItemLink.Title,
					SiteEnums.VideoClassificationType.Item_Link,
					filterItemLink.Url,
					filterItemLink.UserVisibility.Equals("LoggedIn", StringComparison.OrdinalIgnoreCase),
					-1,
					filterItemLink.Order
					);
			}

			#endregion

			// Add all item.
			if (allClassificationItem != null)
			{
				allClassificationItem.VideoCount = GetTotalVideoCount();
				// classificationForSection.AddClassificationSubItem(allClassificationItem, true);
			}

			// Sort by order.
			Sort(VideoClassificationItem.CompareByOrderThenByName);
		}

	}
	#endregion


	public class VideoClassificationForSite : List<VideoClassificationForSection>
	{
		public VideoClassificationForSite ( VideoFilterSettings videoFilterSettings )
		{
			// For each section (product)
			foreach (VideoFilterSettingsSection filterSection in videoFilterSettings.Sections)
			{
				VideoClassificationForSection classificationForSection = new VideoClassificationForSection(
					filterSection.Product,
					filterSection.ClassificationId,
					filterSection.Title,
					filterSection.VideoTypesFilter
					);

				classificationForSection.ReadVideoClassifications(filterSection);

				this.Add(classificationForSection);
			}
		}

	}
}