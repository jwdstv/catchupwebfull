﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Text;


namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    public class FacebookSettings
    {
        #region enums

        /// <summary>
        /// defines what type of facebook control these settings belong to
        /// </summary>
        public enum FacebookControlType 
        {
            /// <summary>
            /// None
            /// </summary>
            None,
            /// <summary>
            /// 
            /// </summary>
            Like,
            /// <summary>
            /// 
            /// </summary>
            Comment,
            /// <summary>
            /// 
            /// </summary>
            Activity
        }

        /// <summary>
        /// there are three options.
        /// </summary>
        public enum FacebookLayout
        {
            /// <summary>
            /// None
            /// </summary>
            None,
            /// <summary>
            /// displays social text to the right of the button and friends' profile photos below. Minimum width: 225 pixels. Default width: 450 pixels. Height: 35 pixels (without photos) or 80 pixels (with photos). 
            /// </summary>
            Standard ,
            /// <summary>
            /// displays the total number of likes to the right of the button. Minimum width: 90 pixels. Default width: 90 pixels. Height: 20 pixels. 
            /// </summary>
            Button_count,
            /// <summary>
            /// displays the total number of likes above the button. Minimum width: 55 pixels. Default width: 55 pixels. Height: 65 pixels.
            /// </summary>
            Box_count
        }

        /// <summary>
        /// the color scheme for the like button. Options: 'light', 'dark' 
        /// </summary>
        public enum FacebookColorscheme
        {
            /// <summary>
            /// None
            /// </summary>
            None,
            /// <summary>
            /// will render the light version of the control
            /// </summary>
            Light,
            /// <summary>
            /// will render the dark version of the control
            /// </summary>
            Dark
        }

        /// <summary>
        /// the verb to display on the button. Options: 'like', 'recommend' 
        /// </summary>
        public enum FacebookAction
        {
            /// <summary>
            /// None
            /// </summary>
            None,
            /// <summary>
            /// will render the light version of the control
            /// </summary>
            Like,
            /// <summary>
            /// will render the dark version of the control
            /// </summary>
            Recommend
        }

        /// <summary>
        /// The type of your object, e.g., "movie". 
        /// </summary>
        public enum FacebookOGTypes
        {
            /// <summary>
            /// 
            /// </summary>
            Movie,
            /// <summary>
            /// 
            /// </summary>
            Tv_Show,
            /// <summary>
            /// 
            /// </summary>
            Blog,
            /// <summary>
            /// 
            /// </summary>
            Website,
            /// <summary>
            /// 
            /// </summary>
            Article
        }

        #endregion
        /// <summary>
        /// Facebook url for Iframe calls
        /// </summary>
        private string src = "http://www.facebook.com/plugins/";

        //declare pUblic Properties
        /// <summary>
        /// comment, like or recent activity
        /// </summary>
        //public FaceBookControlType FBType;
        public FacebookControlType FBType = FacebookControlType.None;

        

        #region Facebook Attributes

        /// <summary>
        /// the id of the facebook application.
        /// </summary>
        public long? FB_appId;//app id from facebook

        /// <summary>
        /// the URL to like. The XFBML version defaults to the current page. 
        /// </summary>
        public string FB_href;

        /// <summary>
        /// there are three options. 
        /// </summary>
        public FacebookLayout FB_layout;

        /// <summary>
        /// specifies whether to display profile photos below the button (standard layout only) 
        /// </summary>
        public bool FB_show_Faces;

        /// <summary>
        /// specifies whether to display send link button in the like control
        /// </summary>
        public bool FB_Like_Send = true;

        /// <summary>
        /// sets the url to the css file which overrides the facebook styles
        /// </summary>
        public string FB_Css = ConfigurationManager.AppSettings["FacebookCSS"].ToString();


        /// <summary>
        /// the width. Like button only. 
        /// </summary>
        public int FB_width;

        public int FB_height;

        /// <summary>
        /// the verb to display on the button. Options: 'like', 'recommend'. like button only 
        /// </summary>
        public FacebookAction FB_action;

        /// <summary>
        /// the font to display in the button. Options: 'arial', 'lucida grande', 'segoe ui', 'tahoma', 'trebuchet ms', 'verdana' 
        /// </summary>
        public string FB_font;

        /// <summary>
        /// the color scheme for the like button. Options: 'light', 'dark' 
        /// </summary>
        public FacebookColorscheme FB_colorScheme;

        public string FB_ref;

        //header=true&amp;colorscheme=light&amp;recommendations=true
        /// <summary>
        /// Used for recent Activities
        /// </summary>
        public bool? FB_header;

        /// <summary>
        /// used by the recent activities
        /// </summary>
        public bool? Fb_recommendations;


        #endregion

        #region Facebook OpenGRaph Attributes

        /// <summary>
        /// The title of the entity.
        /// </summary>
        public string OG_title;

        /// <summary>
        /// You can use FacebookOGTypes to set this value .The type of entity. You must select a type from the list of Open Graph types.
        /// </summary>
        public FacebookOGTypes? OG_type;

        /// <summary>
        /// The URL to an image that represents the entity. Images must be at least 50 pixels by 50 pixels. Square images work best, but you are allowed to use images up to three times as wide as they are tall.
        /// </summary>
        public string OG_image;

        /// <summary>
        /// The canonical, permanent URL of the page representing the entity. When you use Open Graph tags, the Like button posts a link to the og:url instead of the URL in the Like button code.
        /// </summary>
        public string OG_url;

        /// <summary>
        /// A human-readable name for your site, e.g., "IMDb".
        /// </summary>
        public string OG_site_name;

        #endregion


        //functions to  render iframe text
        public string GetIframe()
        {
            string controlResult = string.Empty;

            if ((FBType != FacebookControlType.None))//if a type is specified
            {
                System.Text.StringBuilder controlSB = new System.Text.StringBuilder();
                string scriptTag = string.Format("<div id=\"fb-root\"></div><script src=\"http://connect.facebook.net/en_US/all.js#appId={0}&amp;xfbml=1\"></script>", FB_appId);
                string quoteStr = FBType == FacebookControlType.Activity ? string.Empty : "\"";
                List<string> attrList = new List<string>();
                List<string> styleList = new List<string>();

                if (FB_width > 0)
                {
                    attrList.Add(string.Format("width={0}{1}{0}", quoteStr, FB_width));
                    styleList.Add(string.Format("width:{0}px;", FB_width));
                }

                if (FB_height > 0)
                {
                    attrList.Add(string.Format("height={0}{1}{0}", quoteStr, FB_height));
                    styleList.Add(string.Format("height:{0}px;", FB_height));
                }

                if (FBType != FacebookControlType.Comment)
                {
                    if (FB_colorScheme != FacebookColorscheme.None)
                        attrList.Add(string.Format("colorscheme={0}{1}{0}", quoteStr, FB_colorScheme));
                    if (!string.IsNullOrWhiteSpace(FB_font))
                        attrList.Add(string.Format("font={0}{1}{0}", quoteStr, FB_font));
                }

                switch (FBType)
                {
                    case FacebookControlType.Like:

                        attrList.Add(string.Format("href=\"{0}\"", FB_href));

                        if (FB_layout != FacebookLayout.None)
                            attrList.Add(string.Format("layout=\"{0}\"", FB_layout));    

                        attrList.Add(string.Format("showfaces=\"{0}\"", FB_show_Faces));
                        attrList.Add(string.Format("send=\"{0}\"", FB_Like_Send));
                        attrList.Add(string.Format("css=\"{0}\"", FB_Css)); 

                        controlSB.Append(scriptTag);
                        
                        controlSB.AppendFormat("<fb:like {0}></fb:like>", string.Join(" ", attrList.ToArray()));

                        break;

                    case FacebookControlType.Comment:

                        attrList.Add(string.Format("css={0}{1}{0}", quoteStr, FB_Css));
                        attrList.Add(string.Format("simple={0}{1}{0}", quoteStr, 1));
                        attrList.Add(string.Format("css=\"{0}\"", FB_Css)); 

                        controlSB.Append("<div id=\"fb-root\"></div>");
                        controlSB.Append(scriptTag);
                        controlSB.AppendFormat("<fb:comments migrated=\"1\" width=\"620\" numposts=\"10\" simple=\"1\" {0}></fb:comments>", string.Join(" ", attrList.ToArray()));

                        break;

                    case FacebookControlType.Activity:

                        if (!string.IsNullOrWhiteSpace(FB_href))
                            attrList.Add(string.Format("site={0}", FB_href));
                        if (FB_header.HasValue) 
                            attrList.Add(string.Format("header={0}", FB_header));
                        if (Fb_recommendations.HasValue) 
                            attrList.Add(string.Format("recommendations={0}", Fb_recommendations));

                        controlSB.AppendFormat("<iframe src=\"{0}activity.php?{1}\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; {2}\" allowTransparency=\"true\"></iframe>", src, string.Join("&amp;", attrList.ToArray()), string.Join(" ", styleList.ToArray()));

                        break;

                    default:
                        break;
                }

                controlResult = controlSB.ToString();
            }

            return controlResult;
        }

        private Dictionary<string, string> GetMetaTagDictionary()
        {
            Dictionary<string, string> ogMetaDictionary = new Dictionary<string, string>();

            if (FB_appId.HasValue)
            {
                ogMetaDictionary.Add("fb:app_id", FB_appId.ToString());
            }

            if (!string.IsNullOrEmpty(OG_image))
            {
                ogMetaDictionary.Add("og:image", OG_image);
            }

            if (!string.IsNullOrEmpty(OG_site_name))
            {
                ogMetaDictionary.Add("og:site_name", OG_site_name);
            }

            if (!string.IsNullOrEmpty(OG_title))
            {
                ogMetaDictionary.Add("og:title", OG_title);
            }

            if (OG_type.HasValue)
            {
                ogMetaDictionary.Add("og:type", OG_type.ToString().ToLower());
            }

            if (!string.IsNullOrEmpty(OG_url))
            {
                ogMetaDictionary.Add("og:url", OG_url);
            }

            return ogMetaDictionary;
        }

        public void UpdateMetaTagWithOGValues(System.Web.UI.Page page)
        {
            string metaTag = GetMetaTagString();
            System.Web.UI.LiteralControl tagLabel = new System.Web.UI.LiteralControl();
            tagLabel.Text = metaTag;
            page.Header.Controls.Add(tagLabel);

        }

        public string GetMetaTagString()
        {
            Dictionary<string, string> ogMetaDictionary = GetMetaTagDictionary();
            StringBuilder metaSB = new StringBuilder();

            foreach (KeyValuePair<string, string> item in ogMetaDictionary)
            {
                metaSB.AppendLine();
                metaSB.AppendFormat("\t<meta property=\"{0}\" content=\"{1}\" />", item.Key, item.Value);
            }

            if (metaSB.Length > 0)
            {
                metaSB.AppendLine();
                metaSB.AppendFormat("\t");
            }

            return metaSB.ToString();
        }

        //Constructor
        public FacebookSettings()
        {
            try
            {
                this.FB_appId = Convert.ToInt64(ConfigurationManager.AppSettings["FB_appId"]);
            }
            catch { } 

            //set the url to the current URL
            string url = GetPageFacebookURL();

            this.FB_href = url;
            this.OG_url = url;
        }

        /// <summary>
        /// Get current URL, but if on the play page only return url with videoId querystring parameter so that all facebook likes refer to the same url (regardless of play actions, etc)
        /// </summary>
        /// <returns>Page facebook url</returns>
        public static string GetPageFacebookURL()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string parameterName = "";
            string id = "";

            if (path.EndsWith(SiteConstants.Urls.Constants.VideoPageName, StringComparison.OrdinalIgnoreCase))
            {
                parameterName = SiteConstants.Urls.ParameterNames.VideoId;
                id = SiteUtils.Request.GetQueryStringParameterValue(parameterName, null);
            }
            else if (path.EndsWith(SiteConstants.Urls.Constants.ShowPageName, StringComparison.OrdinalIgnoreCase))
            {
                parameterName = SiteConstants.Urls.ParameterNames.ProgramId;
                id = SiteUtils.Request.GetQueryStringParameterValue(parameterName, null);
            }

            if (!string.IsNullOrWhiteSpace(id) && !string.IsNullOrWhiteSpace(parameterName))
            {
                url = url.Replace(HttpContext.Current.Request.Url.Query, string.Format("?{0}={1}", parameterName, id));
            }


            return url;
        }

    }
}