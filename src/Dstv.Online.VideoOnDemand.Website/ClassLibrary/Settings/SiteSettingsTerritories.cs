﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
	public partial class WebsiteSettings
	{
		private static readonly string AllTerritories = string.Empty;

		#region public sealed class SupportedTerritories
		/// <summary>
		/// 
		/// </summary>
		public sealed class SupportedTerritories
		{
			private const string SouthAfricaTerritoryName = "South Africa";

			#region public static string[] ListOfSupportedTerritories
			/// <summary>
			/// 
			/// </summary>
			public static string[] ListOfSupportedTerritories
			{
				get { return WebsiteSettings.AppSettings.SupportedTerritories; }
			}
			#endregion

			#region public static List<string> SupportedTerritoriesForProduct(ProductTypes product)
			public static List<string> SupportedTerritoriesForProduct ( SiteEnums.ProductType product )
			{
				List<string> territories = new List<string>() { "All" };

				switch (product)
				{
					case SiteEnums.ProductType.BoxOffice:
						return (from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.BoxOfficeTerritories select territory.Name).ToList();

					case SiteEnums.ProductType.CatchUp:
						return (from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.CatchUpTerritories select territory.Name).ToList();

					default:
						return territories;

				}
			}
			#endregion


			public static bool IsUserInSupportedTerritory ()
			{
				SiteEnums.ProductType currentProductType = SiteUtils.Request.GetProductTypeFromRequest();
				return IsUserInSupportedTerritory(currentProductType);
			}

			public static bool IsUserInSupportedTerritory ( Uri requestUri )
			{
				SiteEnums.ProductType currentProductType = SiteUtils.Request.GetProductTypeFromRequest();
				return IsUserInSupportedTerritory(currentProductType);
			}

            public static bool IsUserInSupportedTerritory(Uri requestUri, SiteEnums.ProductType currentProductType)
			{
				return IsUserInSupportedTerritory(currentProductType);
			}

			#region public static bool IsUserInSupportedTerritory(ProductTypes product)
			/// <summary>
			/// 
			/// </summary>
			/// <returns></returns>
			public static bool IsUserInSupportedTerritory ( SiteEnums.ProductType product )
			{
				// IntegrationBroker.Instance.Geotargeting.ResolveUserTerritory();

				List<string> listOfTerritories = new List<string>();

				switch (product)
				{
					case SiteEnums.ProductType.BoxOffice:
						listOfTerritories.AddRange(from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.BoxOfficeTerritories select territory.TerritoryId);
						break;

					case SiteEnums.ProductType.CatchUp:
						listOfTerritories.AddRange(from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.CatchUpTerritories select territory.TerritoryId);
						break;

					default:  
						listOfTerritories.AddRange(from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.OnDemandTerritories select territory.TerritoryId);
						break;
				}

				if (listOfTerritories.Count < 1)
					throw new ApplicationException("An item in the list of territories is expected, however none were found!");

				// return listOfTerritories[0] == AllTerritories || listOfTerritories.Contains(Guid.Parse(IntegrationBroker.Instance.Geotargeting.ResolveUserTerritory().TerritoryId.Value));
                bool UserInValidTerritory = false;
                UserInValidTerritory = listOfTerritories[0] == AllTerritories || listOfTerritories.Contains(TerritoryId());
				return listOfTerritories[0] == AllTerritories || listOfTerritories.Contains(TerritoryId());
			}
			#endregion


			#region public static bool IsUserInIspRequiredTerritory ()
			/// <summary>
			/// 
			/// </summary>
			/// <returns></returns>
			public static bool IsUserInIspRequiredTerritory ()
			{
				List<string> listOfTerritories = new List<string>();
				listOfTerritories.AddRange(from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.IspRequiredTerritories select territory.TerritoryId);
				return listOfTerritories.Contains(TerritoryId());
			}

            #region public static string GetTerritoryName ()
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public static string TerritoryName()
            {
                string territoryName = "";

                territoryName = (from Territory territory in WebSettings.GetSettings.SubSitePermittedTerritories.OnDemandTerritories where territory.TerritoryId == TerritoryId() select territory.Name).FirstOrDefault();

                if (string.IsNullOrWhiteSpace(territoryName))
                {
                    territoryName = "Unknown";
                }

                return territoryName;
            }
            #endregion

            private static string TerritoryId()
		    {
		        ITerritory territory = DstvUser.Territory;

                if(territory != null)
                    return territory.TerritoryId.Value;
                else
                {
                    return "UnknownTerritory";
                }
		    }

		    #endregion
		}
		#endregion
	}
}