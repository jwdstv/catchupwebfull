﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// TODO: refactor, create common collection class with get and set values and derive SessionStore, CookieStore etc from this base class.
    /// </remarks>
    public partial class WebsiteSettings
    {
        public const int DefaultCookieExpiryInDays = 30;
        public const string DefaultCookieName = "DStvOnDemand";

        #region private static HttpSessionState SessionState
        /// <summary>
        /// 
        /// </summary>
        public static HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }
        #endregion

        public static bool DisablePremiumSubsCheck
        {
            get { return bool.Parse(ConfigurationManager.AppSettings["DisablePremiumSubsCheck"] ?? bool.FalseString) && DstvUser.IsSmartCardLinked; }
        }

        // TODO: create a cache interface and factory for interchanging between ASP.Net application cache and MemCache.
        public static System.Web.Caching.Cache ApplicationCache
        {
            get { return HttpContext.Current.Cache; }
        }


        #region public static HttpRequest Request
        /// <summary>
        /// 
        /// </summary>
        public static HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }
        #endregion


        #region public static HttpResponse Response
        /// <summary>
        /// 
        /// </summary>
        public static HttpResponse Response
        {
            get { return HttpContext.Current.Response; }
        }
        #endregion


        #region public static HttpCookieCollection RequestCookies
        /// <summary>
        /// 
        /// </summary>
        public static HttpCookieCollection RequestCookies
        {
            get { return Request.Cookies; }
        }
        #endregion


        #region public static HttpCookieCollection ResponseCookies
        /// <summary>
        /// 
        /// </summary>
        public static HttpCookieCollection ResponseCookies
        {
            get { return Response.Cookies; }
        }
        #endregion


        #region public sealed class SessionStore
        /// <summary>
        /// 
        /// </summary>
        public sealed class SessionStore
        {

            #region public static bool ContainsProperty ( string propertyName )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <returns></returns>
            public static bool ContainsProperty(string propertyName)
            {
                return Session[propertyName] != null;
            }
            #endregion


            #region public static object GetValue ( string propertyName )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <returns></returns>
            public static object GetValue(string propertyName)
            {
                if (Session[propertyName] != null)
                {
                    return Session[propertyName];
                }
                else
                {
                    return null;
                }
            }
            #endregion


            #region public static string GetValue ( string propertyName, string defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static string GetValue(string propertyName, string defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    return value.ToString();
                }
            }
            #endregion


            #region public static int GetValue ( string propertyName, int defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static int GetValue(string propertyName, int defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    int.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public static bool GetValue ( string propertyName, bool defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static bool GetValue(string propertyName, bool defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    bool.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public static Guid GetValue ( string propertyName, Guid defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static Guid GetValue(string propertyName, Guid defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    Guid.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public static void SetValue ( string propertyName, object propertyValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="propertyValue"></param>
            public static void SetValue(string propertyName, object propertyValue)
            {
                Session[propertyName] = propertyValue;
            }
            #endregion


            #region public static void RemoveValue ( string propertyName )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            public static void RemoveValue(string propertyName)
            {
                Session.Remove(propertyName);
            }
            #endregion


            #region public static void ClearSession ()
            /// <summary>
            /// 
            /// </summary>
            public static void ClearSession()
            {
                Session.Clear();
                Session.Abandon();
            }
            #endregion


            #region public static void AbandonSession ()
            /// <summary>
            /// 
            /// </summary>
            public static void AbandonSession()
            {
                Session.Clear();
                Session.Abandon();
            }
            #endregion

        }
        #endregion

        #region public sealed class CacheStore
        /// <summary>
        ///
        /// </summary>
        public sealed class CacheStore
        {
            private static SiteEnums.CacheProvider _cacheProviderToUse = AppSettings.CacheProvider;

            public enum CachedItemPriority
            {
                Low,
                Medium,
                High
            }

            public static bool ContainsObject(string cacheKey)
            {
                if (_cacheProviderToUse == SiteEnums.CacheProvider.ASPNetCache)
                {
                    return ApplicationCache[cacheKey] != null;
                }
                else if (_cacheProviderToUse == SiteEnums.CacheProvider.MemCache)
                {
                    return MemCached.MemCacheManager.Exists(cacheKey);
                }
                else
                {
                    return false;
                }
            }

            public static object GetObject(string cacheKey)
            {
                if (_cacheProviderToUse == SiteEnums.CacheProvider.ASPNetCache)
                {
                    return ApplicationCache[cacheKey];
                }
                else if (_cacheProviderToUse == SiteEnums.CacheProvider.MemCache)
                {
                    // return _memcachedClient.Get(cacheKey);
                    return MemCached.MemCacheManager.Get(cacheKey);
                }
                else
                {
                    return false;
                }
            }

            public static bool RemoveObject(string cacheKey)
            {
                if (_cacheProviderToUse == SiteEnums.CacheProvider.ASPNetCache)
                {
                    return ApplicationCache.Remove(cacheKey) != null;
                }
                else if (_cacheProviderToUse == SiteEnums.CacheProvider.MemCache)
                {
                    // return _memcachedClient.Delete(cacheKey);
                    return MemCached.MemCacheManager.Remove(cacheKey);
                }
                else
                {
                    return false;
                }
            }

            public static void AddObjectWithFileDependencyAndAbsoluteExpiration(string cacheKey, object objectToCache, string fileDependency, DateTime absoluteExpiration, CachedItemPriority priority)
            {
                AddObject(cacheKey, objectToCache, fileDependency, absoluteExpiration, null, priority);

            }

            public static void AddObjectWithFileDependencyAndSlidingExpiration(string cacheKey, object objectToCache, string fileDependency, int slidingExpirationInMinutes, CachedItemPriority priority)
            {
                AddObject(cacheKey, objectToCache, fileDependency, null, slidingExpirationInMinutes, priority);
            }

            public static void AddObjectWithAbsoluteExpiration(string cacheKey, object objectToCache, DateTime absoluteExpiration, CachedItemPriority priority)
            {
                AddObject(cacheKey, objectToCache, null, absoluteExpiration, null, priority);
            }

            public static void AddObjectWithAbsoluteExpiration(string cacheKey, object objectToCache, int absoluteExpirationInMinutes, CachedItemPriority priority)
            {
                AddObject(cacheKey, objectToCache, null, DateTime.Now.AddMinutes(absoluteExpirationInMinutes), null, priority);
            }

            public static void AddObjectWithSlidingExpiration(string cacheKey, object objectToCache, int slidingExpirationInMinutes, CachedItemPriority priority)
            {
                AddObject(cacheKey, objectToCache, null, null, slidingExpirationInMinutes, priority);
            }

            private static void AddObject(string cacheKey, object objectToCache, string fileDependency, DateTime? absoluteExpiration, int? slidingExpirationInMinutes, CachedItemPriority priority)
            {
                if (_cacheProviderToUse == SiteEnums.CacheProvider.ASPNetCache)
                {
                    if (ContainsObject(cacheKey))
                    {
                        ApplicationCache.Insert(
                        cacheKey,
                        objectToCache,
                        string.IsNullOrEmpty(fileDependency) ? null : new System.Web.Caching.CacheDependency(fileDependency),
                        absoluteExpiration.HasValue ? absoluteExpiration.Value : System.Web.Caching.Cache.NoAbsoluteExpiration,
                        slidingExpirationInMinutes.HasValue ? GetTimeSpanForMinutes(slidingExpirationInMinutes.Value) : System.Web.Caching.Cache.NoSlidingExpiration,
                        GetCacheItemPriority(priority),
                        null
                        );
                    }
                    else
                    {
                        ApplicationCache.Add(
                               cacheKey,
                               objectToCache,
                               string.IsNullOrEmpty(fileDependency) ? null : new System.Web.Caching.CacheDependency(fileDependency),
                               absoluteExpiration.HasValue ? absoluteExpiration.Value : System.Web.Caching.Cache.NoAbsoluteExpiration,
                               slidingExpirationInMinutes.HasValue ? GetTimeSpanForMinutes(slidingExpirationInMinutes.Value) : System.Web.Caching.Cache.NoSlidingExpiration,
                               GetCacheItemPriority(priority),
                               null
                               );
                    }
                }
                else if (_cacheProviderToUse == SiteEnums.CacheProvider.MemCache)
                {
                    if (absoluteExpiration.HasValue || slidingExpirationInMinutes.HasValue)
                    {
                        MemCached.MemCacheManager.Add(cacheKey, objectToCache, absoluteExpiration.HasValue ? absoluteExpiration.Value : DateTime.Now.AddMinutes(slidingExpirationInMinutes.Value));
                    }
                    else
                    {
                        MemCached.MemCacheManager.Add(cacheKey, objectToCache);
                    }
                }
            }

            private static System.Web.Caching.CacheItemPriority GetCacheItemPriority(CachedItemPriority priority)
            {
                switch (priority)
                {
                    case CachedItemPriority.High:
                        return System.Web.Caching.CacheItemPriority.High;
                    case CachedItemPriority.Low:
                        return System.Web.Caching.CacheItemPriority.Low;
                    default:
                        return System.Web.Caching.CacheItemPriority.Normal;
                }
            }

            private static TimeSpan GetTimeSpanForMinutes(int minutes)
            {
                return new TimeSpan(0, minutes, 0);
            }
        }
        #endregion

        #region public class CookieStore
        /// <summary>
        /// 
        /// </summary>
        public class CookieStore
        {
            private string _cookieName;
            private HttpCookie _cookie = null;


            public CookieStore()
            {
                _cookieName = DefaultCookieName;
                ReadCookie(DateTime.Now.AddDays(DefaultCookieExpiryInDays));
            }

            #region public CookieStore ( string cookieName )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="cookieName"></param>
            public CookieStore(string cookieName)
            {
                _cookieName = cookieName;
                ReadCookie(DateTime.Now.AddDays(DefaultCookieExpiryInDays));
            }
            #endregion


            #region public CookieStore ( string cookieName, DateTime cookieExpiryDate )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="cookieName"></param>
            /// <param name="cookieExpiryDate"></param>
            public CookieStore(string cookieName, DateTime cookieExpiryDate)
            {
                _cookieName = cookieName;
                ReadCookie(cookieExpiryDate);
            }
            #endregion


            #region public CookieStore ( string cookieName, TimeSpan cookieExpiryPeriod )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="cookieName"></param>
            /// <param name="cookieExpiryPeriod"></param>
            public CookieStore(string cookieName, TimeSpan cookieExpiryPeriod)
            {
                _cookieName = cookieName;
                ReadCookie(DateTime.Now.Add(cookieExpiryPeriod));
            }
            #endregion


            #region private void ReadCookie ( DateTime cookieExpiryDate )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="cookieExpiryDate"></param>
            private void ReadCookie(DateTime cookieExpiryDate)
            {
                _cookie = RequestCookies.Get(_cookieName);

                if (_cookie == null)
                {
                    _cookie = new HttpCookie(_cookieName);
                    _cookie.Expires = cookieExpiryDate;
                    SaveCookie();
                }
            }
            #endregion


            #region public object GetValue ( string propertyName )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <returns></returns>
            public object GetValue(string propertyName)
            {
                if (_cookie.Values[propertyName] != null)
                {
                    return _cookie.Values[propertyName];
                }
                else
                {
                    return null;
                }
            }
            #endregion


            #region public string GetValue ( string propertyName, string defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public string GetValue(string propertyName, string defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    return value.ToString();
                }
            }
            #endregion


            #region public int GetValue ( string propertyName, int defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public int GetValue(string propertyName, int defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    int.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public bool GetValue ( string propertyName, bool defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public bool GetValue(string propertyName, bool defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    bool.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public Guid GetValue ( string propertyName, Guid defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public Guid GetValue(string propertyName, Guid defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    Guid.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public static void SetValue ( string propertyName, object propertyValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="propertyValue"></param>
            public void SetValue(string propertyName, object propertyValue)
            {
                _cookie.Values[propertyName] = propertyValue.ToString();
                SaveCookie();
            }
            #endregion


            #region public void SaveCookie ()
            /// <summary>
            /// 
            /// </summary>
            public void SaveCookie()
            {
                ResponseCookies.Set(_cookie);
            }
            #endregion

        }
        #endregion


        #region public sealed class AppSettings
        /// <summary>
        /// 
        /// </summary>
        public sealed class AppSettings
        {

            #region public static object GetValue ( string propertyName )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <returns></returns>
            public static object GetValue(string propertyName)
            {
                if (ConfigurationManager.AppSettings[propertyName] != null)
                {
                    return ConfigurationManager.AppSettings[propertyName];
                }
                else
                {
                    return null;
                }
            }
            #endregion


            #region public static string GetValue ( string propertyName, string defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static string GetValue(string propertyName, string defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    return value.ToString();
                }
            }
            #endregion


            #region public static int GetValue ( string propertyName, int defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static int GetValue(string propertyName, int defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    int.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public static bool GetValue ( string propertyName, bool defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static bool GetValue(string propertyName, bool defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    bool.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            #region public static Guid GetValue ( string propertyName, Guid defaultValue )
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propertyName"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static Guid GetValue(string propertyName, Guid defaultValue)
            {
                object value = GetValue(propertyName);
                if (value == null)
                {
                    return defaultValue;
                }
                else
                {
                    Guid.TryParse(value.ToString(), out defaultValue);
                    return defaultValue;
                }
            }
            #endregion


            public static string[] SupportedTerritories
            {
                get
                {
                    string supportedTerritories = GetValue("SupportedTerritories", "");
                    return string.IsNullOrEmpty(supportedTerritories) ? new string[] { "All" } : GetValue("SupportedTerritories", "").Split(',');
                }
            }


            public static SiteEnums.CacheProvider CacheProvider
            {
                get
                {
                    SiteEnums.CacheProvider cacheProvider = SiteEnums.CacheProvider.None;
                    if (Enum.TryParse<SiteEnums.CacheProvider>(GetValue("CacheProvider").ToString(), out cacheProvider))
                    {
                        return cacheProvider;
                    }
                    else
                    {
                        return SiteEnums.CacheProvider.None;
                    }

                }
            }

            public static string ChannelImagePath
            {
                get { return GetValue("ChannelImagePath", "/app_themes/onDemand/images/"); }
            }
        }
        #endregion


        #region public sealed class VideoFilterSetting
        /// <summary>
        /// 
        /// </summary>
        public sealed class VideoFilterSetting
        {
            public static Settings.Types.VideoFilterSettings GetVideoFilterSettings(string settingsFileName)
            {
                return Settings.Types.VideoFilterSettings.Parse(settingsFileName);
            }
        }
        #endregion
    }


    #region public sealed class WebSettings : ConfigurationSection
    public sealed class WebSettings : ConfigurationSection
    {
        private WebSettings() { }

        private static WebSettings webSettings;
        public static WebSettings GetSettings
        {
            get { return webSettings ?? (webSettings = (WebSettings)ConfigurationManager.GetSection("WebSettings")); }
        }

        [ConfigurationProperty("isDebug")]
        public bool IsDebug
        {
            get { return (bool)this["isDebug"]; }
        }

        [ConfigurationProperty("topUpUrl")]
        public TopUpUrl TopUpPageUrl
        {
            get
            {
                return (TopUpUrl)this["topUpUrl"];
            }
        }


        [ConfigurationProperty("featuredChannels", IsRequired = true)]
        public FeaturedChannels FeaturedProgramChannels
        {
            get
            {
                return (FeaturedChannels)this["featuredChannels"];
            }
        }

        [ConfigurationProperty("urlWhiteList", IsRequired = true)]
        public UrlWhiteList SiteWhiteListUrls
        {
            get
            {
                return (UrlWhiteList)this["urlWhiteList"];
            }
        }

        [ConfigurationProperty("permittedTerritories", IsRequired = true)]
        public PermittedTerritories SubSitePermittedTerritories
        {
            get
            {
                return (PermittedTerritories)this["permittedTerritories"];
            }
        }

        [ConfigurationProperty("supportedBrowsers", IsRequired = true)]
        public SiteSupportedBrowsers SiteSupportedBrowsers
        {
            get
            {
                return (SiteSupportedBrowsers)this["supportedBrowsers"];
            }
        }

    }

    public class PermittedTerritories : ConfigurationElement
    {
        [ConfigurationProperty("catchUp", IsRequired = true)]
        public Territories CatchUpTerritories
        {
            get
            {
                return (Territories)this["catchUp"];
            }
        }

        [ConfigurationProperty("boxOffice", IsRequired = true)]
        public Territories BoxOfficeTerritories
        {
            get
            {
                return (Territories)this["boxOffice"];
            }
        }

        [ConfigurationProperty("onDemand", IsRequired = true)]
        public Territories OnDemandTerritories
        {
            get
            {
                return (Territories)this["onDemand"];
            }
        }

        [ConfigurationProperty("ispRequired", IsRequired = true)]
        public Territories IspRequiredTerritories
        {
            get
            {
                return (Territories)this["ispRequired"];
            }
        }
    }

    public class Territory : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
        }

        [ConfigurationProperty("territoryId", IsRequired = true)]
        public string TerritoryId
        {
            get
            {
                return (string)this["territoryId"];
            }
        }
    }

    public class Territories : ConfigurationElementCollection
    {
        public Territory this[int index]
        {
            get
            {
                return base.BaseGet(index) as Territory;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Territory();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Territory)element).Name;
        }
    }



    public class SiteUrl : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
        }

        [ConfigurationProperty("enabled", IsRequired = true)]
        public bool Enabled
        {
            get
            {
                return (bool)this["enabled"];
            }
        }

        [ConfigurationProperty("site", IsRequired = true)]
        public string Site
        {
            get
            {
                return (string)this["site"];
            }
        }
    }



    public class UrlWhiteList : ConfigurationElementCollection
    {
        public SiteUrl this[int index]
        {
            get
            {
                return base.BaseGet(index) as SiteUrl;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteUrl();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteUrl)element).Name;
        }
    }




    public class Channel : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
        }
        [ConfigurationProperty("displayImage", IsRequired = true)]
        public string DisplayImage
        {
            get
            {
                return (string)this["displayImage"];
            }
        }
        [ConfigurationProperty("hoverImage", IsRequired = true)]
        public string HoverImage
        {
            get
            {
                return (string)this["hoverImage"];
            }
        }
        [ConfigurationProperty("redirectUrl", IsRequired = true)]
        public string RedirectUrl
        {
            get
            {
                return (string)this["redirectUrl"];
            }
        }

        [ConfigurationProperty("mmsId", IsRequired = true)]
        public string MMSId
        {
            get
            {
                return (string)this["mmsId"];
            }
        }
    }

    public class FeaturedChannels : ConfigurationElementCollection
    {
        public Channel this[int index]
        {
            get
            {
                return base.BaseGet(index) as Channel;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Channel();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Channel)element).Name;
        }
    }

    public class SiteSupportedBrowser : ConfigurationElement
    {
        [ConfigurationProperty("displayName", IsRequired = true)]
        public string DisplayName
        {
            get
            {
                return (string)this["displayName"];
            }
        }

        [ConfigurationProperty("browserName", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["browserName"];
            }
        }

        [ConfigurationProperty("version", IsRequired = true)]
        public int Version
        {
            get
            {
                return (int)this["version"];
            }
        }
    }

    public class SiteSupportedBrowsers : ConfigurationElementCollection
    {

        #region this[int index]
        public SiteSupportedBrowser this[int index]
        {
            get
            {
                return base.BaseGet(index) as SiteSupportedBrowser;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }
        #endregion

        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteSupportedBrowser();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteSupportedBrowser)element).Name;
        }
    }

    public class TopUpUrl : ConfigurationElement
    {
        [ConfigurationProperty("url", IsRequired = true)]
        public string Url
        {
            get
            {
                return (string)this["url"];
            }
        }
    }

    #endregion

}