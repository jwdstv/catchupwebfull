﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

#endregion

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary.Settings.Types
{
    public abstract class SettingsBase
    {
        /// <summary>
        /// Settings directory
        /// </summary>
        private const string settingsDirectory = "/App_Data/Config/";

        /// <summary>
        /// Parse the settings file
        /// </summary>
        /// <param name="_settingsFileName"></param>
        /// <param name="_settingsType"></param>
        /// <returns></returns>
        protected static SettingsBase Parse(string _settingsFileName, Type _settingsType)
        {
            if (_settingsFileName == null || _settingsFileName == string.Empty)
                throw new Exception("Settings filename not defined.");

            string fullPath = System.Web.HttpContext.Current.Server.MapPath(Path.Combine(settingsDirectory, _settingsFileName));

            if (!File.Exists(fullPath))
                throw new Exception("Settings file not found.");

            SettingsBase settings = null;

            using (TextReader textReader = new StreamReader(fullPath))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(_settingsType);
                settings = xmlSerializer.Deserialize(textReader) as SettingsBase;
            }

            return settings;
        }
    }
}
