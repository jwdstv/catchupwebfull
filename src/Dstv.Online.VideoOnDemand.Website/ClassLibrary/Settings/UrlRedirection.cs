﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
    public class UrlRedirection
    {

        /// <summary>
        /// Returns a dictionary containing the whitelist site urls
        /// </summary>
        /// <returns>dictionary</returns>
        private static Dictionary<string, SiteUrl> GetSafeUrlDictionary()
        {
            return WebSettings.GetSettings.SiteWhiteListUrls.Cast<SiteUrl>().Where(url => url.Enabled).ToDictionary(url => url.Site.ToLower());
        }


        /// <summary>
        /// Returns a value indicating if the URL is to a known trusted site
        /// </summary>
        /// <param name="urlToValidate">URL to be validated</param>
        /// <returns>boolean value indicating if the URL is trusted</returns>
        public static bool IsSafeUrl(Uri urlToValidate)
        {
            return GetSafeUrlDictionary().ContainsKey(urlToValidate.Host.ToLower());
        }

        /// <summary>
        /// Returns the friendly / display name for a given url configured in the urlWhitelist
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetSafeUrlFriendlyName(Uri url)
        {
            return IsSafeUrl(url) ? GetSafeUrlDictionary()[url.Host.ToLower()].Name : "Unknown";
        }
    }
}