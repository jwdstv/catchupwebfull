﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
	public partial class WebsiteSettings
	{

		#region public class SupportedBrowser
		/// <summary>
		/// 
		/// </summary>
		public class SupportedBrowser
		{
			private string _name;
			private int _minimumVersion;
			private string _displayName;

			public string Name
			{
				get { return _name; }
			}

			public int MinimumVersion
			{
				get { return _minimumVersion; }
			}

			public string DisplayName
			{
				get { return _displayName; }
			}

			public SupportedBrowser ( string name, int minimumVersion, string displayName )
			{
				_name = name;
				_minimumVersion = minimumVersion;
				_displayName = displayName;
			}

			public override string ToString ()
			{
				return string.Format("{0} version {1} or higher", _displayName, _minimumVersion);
			}
		}
		#endregion


		#region public partial class SupportedBrowsers: List<SupportedBrowser>
		/// <summary>
		/// 
		/// </summary>
		public partial class SupportedBrowsers : List<SupportedBrowser>
		{
			private const string Key_SupportedBrowserState = "SupportedBrowserState";

			private SiteEnums.SupportedBrowserState _supportedBrowserState = SiteEnums.SupportedBrowserState.Unknown;

			public SiteEnums.SupportedBrowserState SupportedState
			{
				get { return _supportedBrowserState; }
			}

			public string CurrentUserBrowser
			{
				get { return string.Format("{0} version {1}", GetBrowserDisplayName(Request.Browser.Browser), Request.Browser.Version); }
			}

			#region public SupportedBrowsers ()
			/// <summary>
			/// 
			/// </summary>
			public SupportedBrowsers ()
			{
                foreach(SiteSupportedBrowser browser in WebSettings.GetSettings.SiteSupportedBrowsers)
                {
                    this.Add(new SupportedBrowser(browser.Name, browser.Version, browser.DisplayName));
                }

				// Get supported state from cookie.
				GetStateFromCookie();

				if (_supportedBrowserState == SiteEnums.SupportedBrowserState.SupportedBrowser || _supportedBrowserState == SiteEnums.SupportedBrowserState.UnsupportedBrowserAndUserAccepted)
				{
					return;
				}

				ValidateBrowser();
			}
			#endregion


			#region public static void UserAcceptsBrowserSupport ()
			/// <summary>
			/// 
			/// </summary>
			public static void UserAcceptsBrowserSupport ()
			{
				CookieStore cookieStore = new CookieStore();
				cookieStore.SetValue(Key_SupportedBrowserState, SiteEnums.SupportedBrowserState.UnsupportedBrowserAndUserAccepted.ToString());
			}
			#endregion


			#region private void GetStateFromCookie ()
			/// <summary>
			/// 
			/// </summary>
			private void GetStateFromCookie ()
			{
				_supportedBrowserState = SiteEnums.SupportedBrowserState.Unknown;

				CookieStore cookieStore = new CookieStore();
				Enum.TryParse<SiteEnums.SupportedBrowserState>(cookieStore.GetValue(Key_SupportedBrowserState, "Unknown"), out _supportedBrowserState);
			}
			#endregion


			#region private void WriteStateToCookie ()
			/// <summary>
			/// 
			/// </summary>
			private void WriteStateToCookie ()
			{
				CookieStore cookieStore = new CookieStore();
				cookieStore.SetValue(Key_SupportedBrowserState, _supportedBrowserState.ToString());
			}
			#endregion

		    public static bool IsCrawler
		    {
		        get
		        {
		            bool isBot = false;
                    if(Request.UserAgent != null)
                    {
                       isBot = Request.UserAgent.ToString().IndexOf("bot", StringComparison.OrdinalIgnoreCase) >= 0
                                   || Request.UserAgent.Equals(@"facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)", StringComparison.OrdinalIgnoreCase);
                    }

		            return Request.Browser.Crawler || isBot;
		        }
		    }


			#region private void ValidateBrowser ()
			/// <summary>
			/// 
			/// </summary>
			private void ValidateBrowser ()
			{
				_supportedBrowserState = SiteEnums.SupportedBrowserState.UnsupportedBrowser;

                // check for Bot/Crawlers - they need to be able to index the site
                if(IsCrawler)
                {
                    _supportedBrowserState = SiteEnums.SupportedBrowserState.SupportedBrowser;
                }

				string userBrowserName = Request.Browser.Browser;
				int userBrowserMajorVersion = Request.Browser.MajorVersion;

				foreach (SupportedBrowser supportedBrowser in this)
				{
					if (userBrowserName.Equals(supportedBrowser.Name, StringComparison.OrdinalIgnoreCase) && userBrowserMajorVersion >= supportedBrowser.MinimumVersion)
					{
						_supportedBrowserState = SiteEnums.SupportedBrowserState.SupportedBrowser;
						break;
					}
				}

				WriteStateToCookie();
			}
			#endregion


			#region public string GetBrowserDisplayName ( string browserName )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="browserName"></param>
			/// <returns></returns>
			public string GetBrowserDisplayName ( string browserName )
			{
				switch (browserName.ToLower())
				{
					case "ie":
						return "Internet Explorer (Microsoft)";
					case "safari":
						return "Safari (Apple)";
					case "chrome":
						return "Chrome (Google)";
					default:
						return browserName;
				}
			}
			#endregion

		}
		#endregion
	}
}

#region Browser Capabilities Output
/*
Browser Capabilities:
Type = IE8
Name = IE
Version = 8.0
Major Version = 8
Minor Version = 0
Platform = WinXP
Is Beta = False
Is Crawler = False
Is AOL = False
Is Win16 = False
Is Win32 = True
Supports Frames = True
Supports Tables = True
Supports Cookies = True
Supports VB Script = True
Supports JavaScript = True
Supports Java Applets = True
Supports ActiveX Controls = True
CDF = False
Request.UserAgent = Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; InfoPath.2; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E; FDM)


Browser Capabilities:
Type = Firefox3
Name = Firefox
Version = 3.5
Major Version = 3
Minor Version = 5
Platform = WinXP
Is Beta = False
Is Crawler = False
Is AOL = False
Is Win16 = False
Is Win32 = True
Supports Frames = True
Supports Tables = True
Supports Cookies = True
Supports VB Script = False
Supports JavaScript = True
Supports Java Applets = True
Supports ActiveX Controls = False
CDF = False
Request.UserAgent = Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)


Browser Capabilities:
Type = Safari5
Name = Safari
Version = 5.0
Major Version = 5
Minor Version = 0
Platform = WinXP
Is Beta = False
Is Crawler = False
Is AOL = False
Is Win16 = False
Is Win32 = True
Supports Frames = True
Supports Tables = True
Supports Cookies = True
Supports VB Script = False
Supports JavaScript = True
Supports Java Applets = True
Supports ActiveX Controls = False
CDF = False
Request.UserAgent = Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8


Browser Capabilities:
Type = Chrome5
Name = Chrome
Version = 5.0
Major Version = 5
Minor Version = 0
Platform = WinXP
Is Beta = False
Is Crawler = False
Is AOL = False
Is Win16 = False
Is Win32 = True
Supports Frames = True
Supports Tables = True
Supports Cookies = True
Supports VB Script = False
Supports JavaScript = True
Supports Java Applets = True
Supports ActiveX Controls = False
CDF = False
Request.UserAgent = Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.127 Safari/533.4

*/
#endregion
