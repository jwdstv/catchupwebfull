﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Website.ClassLibrary
{
	public partial class SiteBreadcrumb
	{

		#region public class BreadcrumbItem
		/// <summary>
		/// 
		/// </summary>
		public class BreadcrumbItem
		{
			private string _navigateUrl = "";
			private string _displayName = "";

			public string NavigateUrl
			{
				get { return _navigateUrl; }
			}

			public string DisplayName
			{
				get { return _displayName; }
			}


			#region public BreadcrumbItem ( string navigateUrl, string displayName )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="navigateUrl"></param>
			/// <param name="displayName"></param>
			public BreadcrumbItem ( string navigateUrl, string displayName )
			{
				_navigateUrl = navigateUrl;
				_displayName = displayName;
			}
			#endregion

		}
		#endregion


		#region public class BreadcrumbList : List<BreadcrumbItem>
		/// <summary>
		/// 
		/// </summary>
		public class BreadcrumbList : List<BreadcrumbItem>
		{
			private string _currentBreadcrumb = "";

			public string CurrentPageBreadcrumb
			{
				get { return _currentBreadcrumb; }
			}

            private string incomingRequestUrl = string.Empty;
            private string incomingPageTitle = string.Empty;
			public BreadcrumbList (string requestUrl, string requestPageTitle )
			{
                incomingRequestUrl = requestUrl;
                incomingPageTitle = requestPageTitle;
				PopulateFromUrl(requestUrl, requestPageTitle);
			}

            private const string onDemandTitlePrefix = "DStv On Demand | Video | VOD | TV | DStv ";
            private const string catchUpTitlePrefix = "CatchUp TV | DStv On Demand  ";
            private const string boxOfficeUpTitlePrefix = "BoxOffice | DStv On Demand ";
			#region public string GetPageTitle ()
			/// <summary>
			/// 
			/// </summary>
			/// <returns></returns>
			public string GetPageTitle ()
			{
                string pageTitle = "";
                pageTitle = string.Format("{0} &gt; {1}", onDemandTitlePrefix, _currentBreadcrumb);

                if (incomingRequestUrl.ToLower().Contains("catchup"))
                {
                    pageTitle = string.Format("{0} &gt; {1} ", catchUpTitlePrefix, _currentBreadcrumb);
                }
                if (incomingRequestUrl.ToLower().Contains("boxoffice"))
                {
                    pageTitle = string.Format("{0} &gt; {1}", boxOfficeUpTitlePrefix, _currentBreadcrumb);
                }

                return pageTitle;
			}

            private const string catchUpMetaDescription = "Watch your favourite TV shows, movies and sports with DStv CatchUp. As a DStv Premium subscriber access when and where you like now.";
            private const string boxOfficeMetaDescription = "Watch the latest DVD releases with DStv BoxOffice. Download the movies and watch them from the comfort of your own home now.";
            private const string onDemandMetaDescription = "DStv On Demand gives you access to the greatest content DStv has to offer when and where you want it. Catchup on your favourite TV shows, movies and sports with DStv CatchUp. Watch the latest DVD releases with DStv BoxOffice. Access great free content online with DStv OpenTime.";

            public string GetPageMetaDescription()
            {
                string pageMetaDescription = "";
                pageMetaDescription = onDemandMetaDescription;

                if (incomingRequestUrl.ToLower().Contains("catchup"))
                {
                    pageMetaDescription = catchUpMetaDescription;
                }
                if (incomingRequestUrl.ToLower().Contains("boxoffice"))
                {
                    pageMetaDescription = boxOfficeMetaDescription;
                }
                return pageMetaDescription;
            }

            private const string catchUpMetaKeywords = "catchup tv entertainment sports series movies kids programming premium subscriber hd pvr website computer watch online register content dstv on demand";
            private const string boxOfficeMetaKeywords = "boxoffice dvd releases store movies titles rent watch computer online hd pvr subscribers digital wallet renting latest blockbuster rent safe legal dstv on demand";
            private const string onDemandMetaKeywords = "catchup tv entertainment sports series movies kids programming premium subscriber hd pvr website computer watch online register content dstv on demand boxoffice dvd releases store movies titles rent watch computer online hd pvr subscribers digital wallet renting latest blockbuster rent safe legal dstv on demand opentime free content watch online website offering download computer south African content full episodes blockbuster trailers interviews movies lifestyle watch access dstv on demand";

            public string GetPageMetaKeywords()
            {
                string pageMetaKeywords = "";
                pageMetaKeywords = onDemandMetaKeywords;

                if (incomingRequestUrl.ToLower().Contains("catchup"))
                {
                    pageMetaKeywords = catchUpMetaKeywords;
                }
                if (incomingRequestUrl.ToLower().Contains("boxoffice"))
                {
                    pageMetaKeywords = boxOfficeMetaKeywords;
                }
                return pageMetaKeywords;
            }
			#endregion


			#region private void PopulateFromUrl ( string requestUrl, string requestPageTitle )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="requestUrl"></param>
			/// <param name="requestPageTitle"></param>
			private void PopulateFromUrl ( string requestUrl, string requestPageTitle )
			{
				if (string.IsNullOrEmpty(requestPageTitle))
				{
					requestPageTitle = "{page title}";
				}

				SiteEnums.ProductType currentSite = SiteEnums.ProductType.OnDemand;
				if (requestUrl.IndexOf(string.Format("{0}/", SiteEnums.ProductType.BoxOffice), StringComparison.OrdinalIgnoreCase) >= 0)
				{
					currentSite = SiteEnums.ProductType.BoxOffice;
				}
				else if (requestUrl.IndexOf(string.Format("{0}/", SiteEnums.ProductType.CatchUp), StringComparison.OrdinalIgnoreCase) >= 0)
				{
					currentSite = SiteEnums.ProductType.CatchUp;
				}
				else if (requestUrl.IndexOf(string.Format("{0}/", SiteEnums.ProductType.OpenTime), StringComparison.OrdinalIgnoreCase) >= 0)
				{
					currentSite = SiteEnums.ProductType.OpenTime;
				}

				this.AddBreadcrumbItem(SiteConstants.Urls.Constants.DStvHomePage, SiteConstants.DisplayNames.DStv);

				bool isHomePage = requestUrl.IndexOf(SiteConstants.Urls.Constants.HomePageName, StringComparison.OrdinalIgnoreCase) >= 0;

				if (currentSite == SiteEnums.ProductType.OnDemand)
				{
					if (isHomePage)
					{
						this.AddCurrentBreadcrumbItem(SiteConstants.DisplayNames.OnDemand);
					}
					else
					{
						this.AddBreadcrumbItem(SiteConstants.Urls.Constants.OnDemandHomePage, SiteConstants.DisplayNames.OnDemand);
						this.AddCurrentBreadcrumbItem(requestPageTitle);
					}
				}
				else
				{
					this.AddBreadcrumbItem(SiteConstants.Urls.Constants.OnDemandHomePage, SiteConstants.DisplayNames.OnDemand);

					if (isHomePage)
					{
						this.AddCurrentBreadcrumbItem(SiteConstants.DisplayNames.GetDisplayNameForSite(currentSite));
					}
					else
					{
						this.AddBreadcrumbItem(SiteConstants.Urls.GetHomePageUrl(currentSite), SiteConstants.DisplayNames.GetDisplayNameForSite(currentSite));
						this.AddCurrentBreadcrumbItem(requestPageTitle);
					}
				}
			}
			#endregion


			#region private void AddBreadcrumbItem ( string navigateUrl, string displayName )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="navigateUrl"></param>
			/// <param name="displayName"></param>
			private void AddBreadcrumbItem ( string navigateUrl, string displayName )
			{
				this.Add(
					new BreadcrumbItem(
						string.IsNullOrEmpty(navigateUrl) ? navigateUrl : SiteUtils.GetAbsoluteUrlPath(navigateUrl),
						displayName
						)
					);
			}
			#endregion


			#region private void AddBreadcrumbItem ( string displayName )
			/// <summary>
			/// 
			/// </summary>
			/// <param name="displayName"></param>
			private void AddCurrentBreadcrumbItem ( string displayName )
			{
				_currentBreadcrumb = displayName;
			}
			#endregion

		}
		#endregion

	}
}