﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Dstv.Online.VideoOnDemand.Common;
using Dstv.Online.VideoOnDemand.Common.Configuration;
using DStvConnect.WebUI.Platform;
using System.Threading;

namespace Dstv.Online.VideoOnDemand.Website
{
	public class Global : DStvConnect.WebUI.HttpGlobal
	{
		public static bool CONST_IsIIS7 = GetIISVersion();

		private static bool GetIISVersion ()
        {
            if (HttpContext.Current.IsDebuggingEnabled)
            {
                string serverSoftware = HttpContext.Current.Request.ServerVariables["SERVER_SOFTWARE"];
                return serverSoftware.IndexOf("IIS/7", StringComparison.OrdinalIgnoreCase) >= 0;
            }
            {
                return true;
            }
        }

		protected override void Application_BeginRequest ( object sender, EventArgs e )
		{
            //HttpContext context = HttpContext.Current;

            //// Assumption: SEO friendly URLs will not have file extension (IIS7)
            //if (string.IsNullOrEmpty(context.Request.CurrentExecutionFilePathExtension) || !CONST_IsIIS7)
            //{
            //    string newUrl = URLRewriter.Rewriter.ParseUrl(context.Request.RawUrl);

            //    if (newUrl != context.Request.RawUrl)
            //        context.RewritePath(newUrl);
            //}

            //// DStv connect may have logic here
            //base.Application_BeginRequest(sender, e);
		}

        protected override void Application_Error(object sender, EventArgs e)
        {
            // log iin to SQL
            try
            {
                string err = Server.GetLastError().ToString();
                if (string.IsNullOrEmpty(err))
                    err = "Last Error Not Found";
                ClassLibrary.ErrorLogManager.LogError(err);  
            }
            catch
            {
            }
           

        }


		protected override void RegisterRoutes ( System.Web.Routing.RouteCollection routes )
		{
			base.RegisterRoutes(routes);
		}

		public override string GetVaryByCustomString ( HttpContext context, string custom )
		{
			if (custom.Equals("UrlPathAndPage", StringComparison.OrdinalIgnoreCase))
			{
				return context.Request.Url.AbsolutePath.ToLower();
			}

			return base.GetVaryByCustomString(context, custom);
		}
	}
}