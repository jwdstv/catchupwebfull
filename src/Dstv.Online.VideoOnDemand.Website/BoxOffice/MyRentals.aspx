﻿<%@ Page Title="My Rentals" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master"
    AutoEventWireup="true" CodeBehind="MyRentals.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.BoxOffice.MyRentals" %>
<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register Src="~/UserControls/Video/VideoSummary.ascx" TagName="VideoSummary" TagPrefix="wuc" %>
<%@ Register Src="../UserControls/BoxOffice/MyRentalsListAll.ascx" TagName="MyRentalsListAll" TagPrefix="wuc" %>

<asp:Content id="contentTop" ContentPlaceHolderID="cphTopContent" runat="Server">
<div style="margin:20px;">
<wuc:MyRentalsListAll ID="MyRentalsListAll1" runat="server" ViewStateMode="Disabled" 
    VideoDetailUrl="/BoxOffice/VideoDetail.aspx?VideoId={VideoId}"
    ItemsPerPage="10" 
    />

    <br />
    <table>
        <tr>
            <th class="rentalListHdr">VAT Invoice for Period: </td>
            <td style="padding-right: 10px;padding-left:10px;">
                <asp:DropDownList ID="ddlPeriod" runat="server" Width="200px" />
            </td>
            <td>
                <asp:Button ID="btnSubmit" Text="Download Invoice" runat="server" Width="150px" Height="25px" OnClick="btnSubmit_Click" />
            </td>
            <td>
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true" />
            </td>
        </tr>
    </table>

    <div class="padding20px"></div>
    <div class="greyHdrLine"></div>
    <div class="padding20px"></div>
   
</div>
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">
    
   <wuc:VideoSummary ID="wucVideoSummaryEditorial" runat="server" ViewStateMode="Disabled"
	    IsEditorial="true"
        ListId="2"
	    ListTitle="Editorial Playlist"
	    >
        <VideoFilter
            ProductType="BoxOffice" 
            VideoCount="4" 
            SortFilter="LatestReleases"
            VideoTypes="Movie"
            />
        <PageLinks 
            VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	        MoreUrl="/BoxOffice/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}"
            />
    </wuc:VideoSummary>

</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="cphRightContentTop" runat="server">

</asp:Content>
