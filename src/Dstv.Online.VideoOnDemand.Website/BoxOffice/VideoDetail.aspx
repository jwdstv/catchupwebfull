﻿<%@ Page Title="Video Details" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master" AutoEventWireup="true" CodeBehind="VideoDetail.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.BoxOffice.VideoDetail" %>
<%@ Register src="~/UserControls/Video/EditorialVideoList.ascx" tagname="EditorialVideoList" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Video/VideoCastAndCrew.ascx" tagname="CastandCrew" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Video/VideoDetailConsole.ascx" tagname="VideoDetailConsole" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Social/FacebookComments.ascx" tagname="FacebookComments" tagprefix="wuc" %>
<%@ Register Src="~/UserControls/Social/FacebookLike.ascx" TagName="FacebookLike" TagPrefix="wuc" %>



<asp:Content ID="contentHead" ContentPlaceHolderID="cphHeadContent" runat="server">
    <script src="../Scripts/swfobject2.2.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="contentTop" ContentPlaceHolderID="cphTopContent" runat="server">
    <!-- Video Detail + Still + Player -->
	<wuc:VideoDetailConsole id="wucVideoDetailConsole" runat="server"
        ConsoleDisplayType="Video" 
		TopUpUrl="/Profile/TopUp.aspx" 
       />
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">

<div class="fbLike floatLeft">
    <%--<wuc:FacebookLike ID="wucFacebookLike" runat="server">
        <FBSettings FBType="Like" FB_action="Like" FB_layout="Standard" FB_colorScheme="Dark" FB_show_Faces="false" FB_width="450" />
    </wuc:FacebookLike>--%>
    <div id="fb-root">
        </div>
        <script type="text/javascript" src="http://connect.facebook.net/en_US/all.js#appId=140975995948089&xfbml=1">
        </script>
        <fb:like
        width="450"
        colorscheme="Dark"
        href="<%= Dstv.Online.VideoOnDemand.Website.ClassLibrary.FacebookSettings.GetPageFacebookURL() %>"
        layout="Standard"
        showfaces="False"
        send="True"
        css="http://core.dstv.com/css/ondemand.dstv.com/CatchUpFacebook.css">
        </fb:like>


        </div>

<!-- Video Synopsis, Cast and Crew + Facebook Comments Control -->
<h4>Full Synopsis</h4>
    <div class="greyHdrLine"></div>
    <div class="padding5px"></div>
    <div class="floatLeft"><asp:Literal ID="litFullSynopsis" runat="server"></asp:Literal></div>

	 <div class="padding20px"></div>

    <wuc:CastAndCrew id="wucCastAndCrew" runat="Server" ViewStateMode="Disabled"></wuc:CastAndCrew>
    
    <div class="padding20px"></div>
    <h4>Comments</h4>
    <div class="greyHdrLine"></div>
    <div class="floatLeft">
        <wuc:FacebookComments id="wucFacebookComments" runat="server">
            <FBSettings FBType="Comment" FB_width="541" FB_height="267" />
        </wuc:FacebookComments>
    </div>
    
    <div class="padding20px"></div>
	 
</asp:Content>
<asp:Content ID="contentRightTop" ContentPlaceHolderID="cphRightContentTop" runat="server">

<wuc:EditorialVideoList id="wucEditorialVideoList" runat="server" ViewStateMode="Disabled"
    ListId="<%$ AppSettings:Top10VideoListId %>"
	>
    <VideoFilter
        ProductType="BoxOffice" 
        VideoCount="10" 
        SortFilter="LatestReleases"
        VideoTypes="Movie"
        />
    <PageLinks 
        VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/BoxOffice/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}"
        />
</wuc:EditorialVideoList>

</asp:Content>
<asp:Content ID="contentRightBottom" ContentPlaceHolderID="cphRightContentBottom" runat="server">

</asp:Content>

