﻿<%@ Page Title="Browse Videos" Language="C#" MasterPageFile="~/MasterPages/TitleAndVerticalSplitContentWithLeftNav.master"
    AutoEventWireup="true" CodeBehind="Browse.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.BoxOffice.Browse" %>

<%@ Register Src="~/UserControls/Video/VideoGallery.ascx" TagName="VideoGallery"
    TagPrefix="wuc" %>
<%@ Register Src="~/UserControls/Video/VideoFilterList.ascx" TagName="VideoFilterList"
    TagPrefix="wuc" %>
<asp:Content ID="contentTitle" ContentPlaceHolderID="cphTitle" runat="server">
    Browse Video
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">
    <wuc:VideoFilterList ID="wucVifeoFilterList" runat="server" ConfigFile="VideoFilterSettings.xml"
        ViewStateMode="Disabled" ExpandedProductOnLoad="BoxOffice" />
</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="cphRightContent" runat="server">
    <h4>
        <strong>BoxOffice </strong>
        <img src="/app_themes/onDemand/images/arrow_hdr_grey.gif" width="8" height="14" />
        <asp:Literal ID="litVideoCategory" runat="server">{Video Filter Category}</asp:Literal>
    </h4>
    <span class="grey16">(<asp:Literal ID="litVideoCategoryCount" runat="server">{Video Count}</asp:Literal>)</span>
    <div class="greyHdrLine">
    </div>
    <wuc:VideoGallery ID="wucVideoGallery" runat="server" ViewStateMode="Disabled" ItemsPerPage="16"
        ItemsPerRow="4">
        <VideoFilter ProductType="BoxOffice" VideoTypes="Movie" />
        <PageLinks VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}" />
    </wuc:VideoGallery>
</asp:Content>
