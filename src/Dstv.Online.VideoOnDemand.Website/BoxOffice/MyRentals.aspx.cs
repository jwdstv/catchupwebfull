﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Proxies;
using System.Configuration;
using Dstv.Online.Contracts;

namespace Dstv.Online.VideoOnDemand.Website.BoxOffice
{
	public partial class MyRentals : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			Master.PageRequiresAuthentication = true;
			Master.DisplayMyRentalsList = false;

			if (!IsPostBack)
			{
				PopulateDateRanges();
			}
		}

		private void PopulateDateRanges ()
		{
			int yy = 1990;
			int mm = 1;

			yy = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceStartYear"]);
			mm = Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceStartMonth"]);

			DateTime startDate = new DateTime(yy, mm, 1);
			DateTime currDate = DateTime.Now.AddMonths(-1);

			while (currDate > startDate)
			{
				ddlPeriod.Items.Add
					 (
						  new ListItem
								(
									 string.Format("{0:Y}", currDate),
									 string.Format("{0:d}", currDate)
								)
					 );

				currDate = currDate.AddMonths(-1);
			}
		}

		/// <summary>
		/// Download the customer invoice
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSubmit_Click ( object sender, EventArgs e )
		{
			DateTime bgn = Convert.ToDateTime(ddlPeriod.SelectedValue);
			DateTime end = bgn.AddMonths(1);

			byte[] documentBytes; ServiceResult result;

			// Retrieve the pdf document
			using (InvoiceServiceProxy clientProxy = new InvoiceServiceProxy())
			{
				result = clientProxy.Generate(81182835, 44676790, new List<string>(), bgn, end, out documentBytes);
			}

			if (!result.Success)
			{
				// Display the error message
				lblError.Text = result.ErrorMsg;
			}
			else
			{
				lblError.Text = string.Empty;

				// Download the attachment
				Download(documentBytes, "invoice.pdf");
			}
		}

		/// <summary>
		/// Download the byte array as attachment
		/// </summary>
		/// <param name="_buffer"></param>
		/// <param name="_fileName"></param>
		protected void Download ( byte[] _buffer, string _fileName )
		{
			System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
			System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + _fileName);
			System.Web.HttpContext.Current.Response.OutputStream.Write(_buffer, 0, _buffer.Length);
			System.Web.HttpContext.Current.Response.Flush();
		}
	}
}
