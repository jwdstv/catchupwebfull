﻿<%@ Page Title="Launch Desktop Player" Language="C#" AutoEventWireup="true" CodeBehind="LaunchDesktopPlayer.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.BoxOffice.LaunchDesktopPlayer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>On Demand Desktop Player</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
	 Video Id: <asp:Literal ID="litVideoId" runat="server"></asp:Literal><br /><br />
	 Redirects to Desktop Player via Url: <asp:Literal ID="litDesktopPlayerUrl" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
