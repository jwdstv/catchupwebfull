﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Modules.Rental;


namespace Dstv.Online.VideoOnDemand.Website.BoxOffice
{
	public partial class LaunchDesktopPlayer : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!DstvUser.IsAuthenticated)
			{
				throw new UnauthorizedAccessException();
			}

			// Retrieve Video Id and Desktop Player Url from query string
			string videoId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, "");
			if (!string.IsNullOrEmpty(videoId))
			{
				string desktopPlayerUrl = SiteUtils.Url.ExternalRedirector.GetRedirectUrlFromQueryString(SiteConstants.Urls.ParameterNames.DesktopPlayerUrl);
				if (string.IsNullOrEmpty(desktopPlayerUrl))
				{
					throw new ArgumentException(SiteConstants.Urls.ParameterNames.DesktopPlayerUrl);
				}
				
				// Get Video Item from user's rental list.
				//	a) to check that it is indeed in user's rental list
				// b) that it has not expired (rental period)
				Rental rental = new Rental();
				string customerId = DstvUser.UserProfile.CustomerId.Value; // TODO: MasterPage.UserProfile.CustomerId.Value
				RentalResult rentalResult = rental.GetVideoRentalItemByUserIdAndItemId(customerId, videoId);
				if (rentalResult.TotalResults == 0 || rentalResult.Rentals.Count == 0 || rentalResult.Rentals[0].HasRentalExpired)
				{
					throw new ApplicationException("Video is no longer a valid rental. // TODO: display decent error message to user. Waiting on final Desktop Player initialisation to understand if this is the correct 'dialogue'");
				}
			}
			else
			{
				throw new ArgumentException(SiteConstants.Urls.ParameterNames.VideoId);
			}
		}
	}
}