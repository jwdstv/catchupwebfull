﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dstv.Online.VideoOnDemand.Website.MasterPages;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using DataAccess = Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;
using DataContract = Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;



namespace Dstv.Online.VideoOnDemand.Website.BoxOffice
{
    public partial class VideoDetail : System.Web.UI.Page
    {

        private ProgramResult _programDetail = null;

        private string _programVideoId;

        private DataContract.ICatalogueItem _videoItem
        {
            get { return _programDetail != null && _programDetail.PrimaryVideoItem != null ? _programDetail.PrimaryVideoItem.CatalogueItem : null; }
        }

        public string VideoMetaKeywords
        {
            get { return _videoItem == null ? "" : HttpUtility.HtmlEncode(string.Format("{0},{1}", wucVideoDetailConsole.VideoTitle, _videoItem.Keywords)); }
        }

        public string VideoMetaDescription
        {
            get { return _videoItem == null ? "Rent and watch movies online." : HttpUtility.HtmlEncode(string.Format("Rent and watch movies online. Movie: {0} Detail: {1}", wucVideoDetailConsole.VideoTitle, wucVideoDetailConsole.VideoAbstractTrimmed)); }
        }

        private FacebookSettings FBSettings;


        /// <summary>
        /// Defines whether the user is logged in
        /// </summary>
        public bool UserLoggedIn;

        /// <summary>
        /// Defines whether the user has a ISP linked
        /// </summary>
        public bool ISPLinked;

        /// <summary>
        /// Defines whether the user has a Smartcard linked
        /// </summary>
        public bool SmartcardLinked;
        
        /// <summary>
        /// Returns the casted master page
        /// reference.
        /// </summary>
        public MasterPageBase MasterPage
        {
            get
            {
                return (MasterPageBase)this.Page.Master;
            }
        }

        private string getjQueryCode(string jsCodetoRun)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$(document).ready(function() {");
            sb.AppendLine(jsCodetoRun);
            sb.AppendLine(" });");

            return sb.ToString();
        }

        private void runjQueryCode(string jsCodetoRun)
        {

            ScriptManager requestSM = ScriptManager.GetCurrent(this);
            //if (requestSM != null && requestSM.IsInAsyncPostBack)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this,
            //                                            typeof(Page),
            //                                            Guid.NewGuid().ToString(),
            //                                            getjQueryCode(jsCodetoRun),
            //                                            true);
            //}
            //else
            //{
                ClientScript.RegisterClientScriptBlock(typeof(Page),
                                                       Guid.NewGuid().ToString(),
                                                       getjQueryCode(jsCodetoRun),
                                                       true);
            //}
        }

        private void CheckAuthentication()
        {
            if (DstvUser.IsAuthenticated)
            {

                UserLoggedIn = true;
            }
            else 
            {

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            //CheckAuthentication();  //Checks if user is logged in, if the user isn't, pops up lightbox with login link -OM
            //Commented out, because the logic is different in Boxoffice, this needs to be checked on rent click
            _programDetail = null;

            _programVideoId = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoId, null);

            if (!string.IsNullOrEmpty(_programVideoId))
            {
                bool isForthcoming = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.IsForthcoming, 0) == 1;

                //Set value for VideoDetails Console
                this.wucVideoDetailConsole.IsForthcomingRelease = isForthcoming;

                _programDetail = DataAccess.VideoData.GetVideoDetailsForProgram(_programVideoId, DataContract.VideoProgramType.Movie, isForthcoming);

                if (_programDetail != null)
                {
                    string videoDetailAction = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoDetailAction, null);

                    bool containsPayAction = SiteUtils.Request.GetQueryStringParameterValue(SiteConstants.Urls.ParameterNames.VideoDetailAction, null) == SiteConstants.Urls.ParameterValues.PayForMovie;
                    if (containsPayAction)
                    {
                        wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.PayForVideo;
                    }
                    else if (!string.IsNullOrEmpty(videoDetailAction))
                    {
                        string[] videoDetailActionSplit = videoDetailAction.Split(',');
                        if (videoDetailActionSplit.Length == 2)
                        {
                            if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.PlayMovie, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.PlayVideo;
                            }
                            else if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.DownloadMovie, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.DownloadVideo;
                            }
                            else if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.PlayTrailer, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.PlayTrailer;
                            }
                            else if (videoDetailActionSplit[0].Equals(SiteConstants.Urls.ParameterValues.PlayEPK, StringComparison.OrdinalIgnoreCase))
                            {
                                wucVideoDetailConsole.MediaDisplayAction = SiteEnums.MediaDisplayActionType.PlayEPK;
                            }
                        }
                    }

                    wucVideoDetailConsole.ProgramDetail = _programDetail;
                }
            }

            if (_programDetail == null)
            {
                wucVideoDetailConsole.Visible = false;
            }

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            Title = string.Format("Movie: {0}", wucVideoDetailConsole.VideoTitle);
            if (_videoItem!= null && _videoItem.Keywords != null && wucVideoDetailConsole != null && wucVideoDetailConsole.VideoTitle != null)
            {
                MetaKeywords = HttpUtility.HtmlEncode(
                    string.Format("{0},{1}", wucVideoDetailConsole.VideoTitle, _videoItem.Keywords)
                    );
            }
            if (wucVideoDetailConsole !=null && wucVideoDetailConsole.VideoTitle != null && wucVideoDetailConsole.VideoAbstractTrimmed != null)
            {

                MetaDescription = HttpUtility.HtmlEncode(
                    string.Format("Rent and watch movies online. Movie: {0} Detail: {1}", wucVideoDetailConsole.VideoTitle, wucVideoDetailConsole.VideoAbstractTrimmed)
                    );
            }

            SetDetail();
            SetFacebook();

        }


        #region private void SetDetail ()
        /// <summary>
        /// 
        /// </summary>
        private void SetDetail()
        {
            // Set synopsis
            string synopsis = string.Empty;
            if (_videoItem != null && (!string.IsNullOrEmpty(_videoItem.Synopsis)))//Check for Null Values
            {
                synopsis = _videoItem.Synopsis;
            }
            litFullSynopsis.Text = synopsis;

            // Set Cast and Crew 
            if (_programDetail != null && _programDetail.CastAndCrew != null)// Checking for Null Values
                wucCastAndCrew.PopulateControl(_programDetail.CastAndCrew);
        }

        #region set Facebook
        /// <summary>
        /// sets facebook settings and adds facebook meta tags for the video
        /// </summary>
        private void SetFacebook()
        {
            FacebookSettings fbSettings = new FacebookSettings();

            if (wucVideoDetailConsole.VideoProductType == ProductTypes.BoxOffice || wucVideoDetailConsole.VideoItem.VideoProgramType == DataContract.VideoProgramType.Movie)
            {
                fbSettings.OG_type = FacebookSettings.FacebookOGTypes.Movie;
            }
            else
            {
                fbSettings.OG_type = FacebookSettings.FacebookOGTypes.Tv_Show;
            }

            fbSettings.OG_title = wucVideoDetailConsole.VideoTitle;
            fbSettings.OG_site_name = string.Format("On Demand {0}", ClassLibrary.FlashVideo.Util.ConfigValues.CategoryValue);
            fbSettings.OG_image = wucVideoDetailConsole.VideoThumbnailImgUrl;

            fbSettings.UpdateMetaTagWithOGValues(this.Page);
        }
        #endregion
        #endregion

        [WebMethod()]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public static void InsertPlayHit(string videoId)
        {
            IntegrationBroker.Instance.Catalogue.InsertCatalogueItemPlayHit(int.Parse(videoId));
        }

    }


}