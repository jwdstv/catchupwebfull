﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.BoxOffice
{
	public partial class Browse : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack)
			{
			}

            this.wucVifeoFilterList.HeaderFilter ="FILTER VIDEO";
		}

		protected void Page_PreRender ( object sender, EventArgs e )
		{
			litVideoCategory.Text = wucVideoGallery.VideoGalleryTitle;

            // Removed by Jodi Adams in order to make the control function correclty.
            //if (wucVideoGallery.TotalItems > 0)
            //{
            //    wucVideoGallery.VideoRepeater.Visible = true;
            //    divNoItems.Visible = false;
            //}
            //else
            //{
            //    wucVideoGallery.VideoRepeater.Visible = false;
            //    //divNoItems.Visible = true;
            //}

			litVideoCategoryCount.Text = wucVideoGallery.TotalItems.ToString();
		}
	}
}