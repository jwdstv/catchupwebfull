﻿<%@ Page Title="Find Out More" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master"
    AutoEventWireup="true" CodeBehind="FindOutMore.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.BoxOffice.FindOutMore" %>

<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register Src="~/UserControls/Video/EditorialVideoList.ascx" TagName="EditorialVideoList"
    TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTopContent" runat="server">
    <div class="vidPlay_bg">
        <img src="/app_themes/onDemand/images/boxOfficeinfo.jpg" alt="Sign up for BoxOffice"
            width="980" height="242" border="0" usemap="#Map" />
        <map name="Map" id="Map">
            <area shape="rect" coords="276,41,569,99" target="_blank" alt="Sign up to BoxOffice"
                href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx" />
        </map>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftContent" runat="server">

    
  	<div class="floatLeft">
        <h4>What is BoxOffice?</h4>
        <div class="greyHdrLine"></div> 
			<div class="floatLeft">
                <ul class="basList">
                    <li>South Africa’s premier movies on demand service, featuring the latest blockbuster releases.</li><br>
                    <li>Available to everyone, not just DStv subscribers.</li><br>
                    <li>Rent movies for <%= ConfigurationManager.AppSettings["BillBoardBoxOfficePrice"]%> – download them to your computer or watch them online. DStv subscribers can also rent movies on their PVR decoder.</li><br>
                    <li>DStv subscribers with an HD PVR can rent HD movies for just <%= ConfigurationManager.AppSettings["DecoderHDBoxOfficePrice"]%></li><br>
                    <li>You can keep each movie for 48 hours.</li><br />
                </ul>
            </div>
     	  <div class="floatLeft">
                <h4>How do I get BoxOffice?</h4>
                <div class="greyHdrLine"></div>
                <ul class="basList">
                    <li>Sign up and create your DStv Wallet – <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx">Sign up online</a> to get free credit!</li><br>
                    <li>If you are a DStv subscriber with a PVR decoder you can also sign up via SMS – SMS your smartcard number to 34989.</li><br>
                    <li>Load credit into your DStv Wallet via credit card or EFT.</li><br>
                </ul>
            </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphRightContentTop" runat="server">
    <div class="boSignup">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td class="boSignupTxt">
                    New to BoxOffice?
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx?product=boxoffice">
                        <img src="/app_themes/onDemand/images/btn_signUp-BO.gif" width="95" height="42"
                            align="absmiddle" alt="Sign Up" /></a>
                </td>
            </tr>
        </table>
    </div>
    <div class="padding15px">
    </div>
    <%--Removed the recommended list as requested by Debs--%>
    <%--<wuc:EditorialVideoList ID="wucEditorialVideoList" runat="server" ViewStateMode="Disabled"
        ListId="1">
        <VideoFilter ProductType="BoxOffice" VideoCount="10" SortFilter="SearchResultOrder" VideoTypes="Movie" />
        <PageLinks VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
            MoreUrl="/BoxOffice/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}" />
    </wuc:EditorialVideoList>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphRightContentBottom" runat="server">
</asp:Content>
