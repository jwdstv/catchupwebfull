﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterPages/NoTitleAndSplitContentWithRightWidgets.master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.BoxOffice.Home" %>
<%@ MasterType TypeName="System.Web.UI.MasterPageBase" %>
<%@ Register src="~/UserControls/Video/VideoSummary.ascx" tagname="VideoSummary" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Video/EditorialVideoList.ascx" tagname="EditorialVideoList" tagprefix="wuc" %>
<%@ Register Src="~/UserControls/Social/FacebookRecentActivity.ascx" TagName="FaceBookActivity" TagPrefix="fb" %>
<%@ Register Src="~/UserControls/Billboard/Billboard.ascx" TagName="BillBoard" TagPrefix="wuc" %>

<asp:Content ID="contentTop" ContentPlaceHolderID="cphTopContent" runat="server">
    <%--<div class="block980_bg" style="vertical-align: middle;">
        
        <script src="../Scripts/swfobject2.2.js" type="text/javascript"></script>
        <wuc:BillBoard ID="CatchUpBillBoard" ProductId="2" runat="server" FontColor="" />
    </div>--%>
    <div class="block980_bg-BO">
        <div class="dstvflash">
            <script src="../Scripts/swfobject2.2.js" type="text/javascript"></script>
        <wuc:BillBoard ID="CatchUpBillBoard" runat="server" FontColor="5CB65C" />
        </div>
    <div class="promo-BO">
    	<div class="promo_hdr">
        	<table cellspacing="0" cellpadding="0" border="0" width="100%">
              <tbody><tr>
                <td width="45"><img src="/app_themes/onDemand/images/lrgArrow-boxOffice.png"></td>
                <td><img alt="BoxOffice" src="/app_themes/onDemand/images/hdr-boxOffice.png"></td>
              </tr>
            </tbody></table>
        </div>
        <div class="iefix_h9">Rent movies online for only&nbsp;</div>
        <div class="padding10px"></div>
		<div class="promoPrice"><%=ConfigurationManager.AppSettings["BillBoardBoxOfficePrice"]%>&nbsp;&nbsp;&nbsp;</div>
        <div class="padding10px"></div>
        <div class="promoList-BO">
        	<ul>
            	<li>Stream or download the latest movies</li>
                <li>Keep the movies for 48 hours</li>
                <li>5 new movies added every week</li>
            </ul>
        </div>
        <div class="floatLeft">
        	<table cellspacing="10" cellpadding="0" border="0" width="100%">
              <tbody><tr>
                <td><a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>BoxOffice/FindOutMore.aspx"><img height="42" width="151" alt="FInd out more about BoxOffice" src="/app_themes/onDemand/images/btn_findOutMore.gif"></a></td>
              <% if (!User.Identity.IsAuthenticated)// Check if user is logged in or not
                 { %>
                <td>
                    <a class="boxOffice" href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx?product=boxoffice">Sign up now.</a>
                    <br />It's quick &amp easy.</td>
              <%} %>
              </tr>
            </tbody></table>
      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="contentLeft" ContentPlaceHolderID="cphLeftContent" runat="server">

<wuc:VideoSummary ID="wucVideoSummaryNewReleases" runat="server" ViewStateMode="Disabled"
	ListTitle="New Releases"
	>
     
     <VideoFilter
        ProductType="BoxOffice" 
        VideoCount="4" 
        SortFilter="LatestReleases"
        VideoTypes="Movie"
        />
     <PageLinks 
        VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/BoxOffice/Browse.aspx?SortFilter={SortFilter}"
        />
    </wuc:VideoSummary>

<div class="padding5px"></div>

<wuc:VideoSummary ID="wucVideoSummaryLastChance" runat="server" ViewStateMode="Disabled"
	ListTitle="Last Chance"
	>
     
     <VideoFilter
        ProductType="BoxOffice" 
        VideoCount="4" 
        SortFilter="LastChance"
        VideoTypes="Movie"
        />
     <PageLinks 
       VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/BoxOffice/Browse.aspx?SortFilter={SortFilter}"
        />
    </wuc:VideoSummary>

<div class="padding5px"></div>

<wuc:VideoSummary ID="wucVideoSummaryForthcomingReleases" runat="server" ViewStateMode="Disabled"
	ListTitle="Forthcoming Releases"
	>
    
     <VideoFilter
        ProductType="BoxOffice" 
        VideoCount="4" 
        SortFilter="ForthcomingReleases"
        VideoTypes="Movie"
        />
     <PageLinks 
        VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/BoxOffice/Browse.aspx?SortFilter={SortFilter}"
        />
    </wuc:VideoSummary>

</asp:Content>
<asp:Content ID="contentRightTop" ContentPlaceHolderID="cphRightContentTop" runat="server">

<wuc:EditorialVideoList id="wucEditorialVideoList" runat="server" ViewStateMode="Disabled"
    ListId="2" 
	>
    
     <VideoFilter
        ProductType="BoxOffice" 
        VideoCount="10" 
        SortFilter="SearchResultOrder"
        VideoTypes="Movie"
        />
    <PageLinks 
        VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/BoxOffice/Browse.aspx?EditorialId={ListId}&SortFilter={SortFilter}"
        />
    </wuc:EditorialVideoList>

</asp:Content>
<asp:Content ID="contentRightBottom" ContentPlaceHolderID="cphRightContentBottom" runat="server">
    <%--<fb:FaceBookActivity ID="fbRecentActivity" runat="server">
        <FBSettings FBType="Activity" FB_height="450" />
    </fb:FaceBookActivity>--%>

    <iframe src="http://www.facebook.com/plugins/activity.php?site=ondemand.dstv.com&amp;width=300&amp;height=300&amp;header=true&amp;colorscheme=dark&amp;font&amp;border_color=656565&amp;recommendations=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:300px;" allowTransparency="true"></iframe>

    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
    <fb:activity site="ondemand.dstv.com" width="300" height="450" header="true" colorscheme="dark" font="" border_color="656565" recommendations="true"></fb:activity>

</asp:Content>
