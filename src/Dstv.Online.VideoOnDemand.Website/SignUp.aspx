﻿<%@ Page Title="Sign Up" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master"
    AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Register" %>

<%@ Register Src="Controls/Registration.ascx" TagName="Registration" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="block980_bg">
        <div class="hdrWht24">
            SIGN UP
        </div>
    </div>
    <div class ="block960">
    <uc1:Registration ID="Registration1" runat="server" />
    </div>
</asp:Content>
