﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace Dstv.Online.VideoOnDemand.Website
{
    public partial class Default : System.Web.UI.Page

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SiteHomePage"]))
                {
                    if (ConfigurationManager.AppSettings["SiteHomePage"].ToLower() != Request.ServerVariables["SCRIPT_NAME"].ToLower())
                        Response.Redirect(ConfigurationManager.AppSettings["SiteHomePage"]);
                }

            }
            catch
            {

                throw;
            }
        }
    }
}