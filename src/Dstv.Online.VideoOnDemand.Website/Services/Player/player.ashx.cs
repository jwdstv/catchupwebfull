﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Xml;
using System.IO;
using System.Text;
using System.Configuration;
using Dstv.Online.VideoOnDemand.Website.Services;

namespace stv.Online.VideoOnDemand.Website.Services.Player
{
    [WebService(Namespace = "http://ondemand.dstv.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class player : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var videoid = context.Request.QueryString["vid"] == null ? 1 : Convert.ToInt32(context.Request.QueryString["vid"]);

            context.Response.ContentType = "text/plain";
            context.Response.Write(this.PlayerOutput(context, videoid));
        }

        public bool IsReusable {
            get { return false; }
        }

        public string PlayerOutput(HttpContext context, int videoid)
        {
            var js = string.Empty;

            using (var fs = new FileStream(context.Server.MapPath("/App_Data/player.json"), FileMode.Open))
            {
                var bytes = new byte[fs.Length];
                fs.Read(bytes, 0, bytes.Length);
                js = Encoding.ASCII.GetString(bytes);
                js = js.Replace("???", "");
            }

            var vo = Newtonsoft.Json.JsonConvert.DeserializeObject<VideoObject>(js); // parse the JSON string as an instance of the VideoObject class

            vo.result.related_videos        = new List<VideoObject.ResultObject.SubRelated>();
            vo.result.video.main.url        = string.Format("/BoxOffice/VideoDetail.aspx?vid={0}", videoid); 
            vo.result.video.main.title      = "Avatar";
            vo.result.video.main.thumb_path = "/App_Themes/onDemand/images/VideoThumb.jpg";
            vo.result.video.main.image_path = "/App_Themes/onDemand/images/VideoBillboard.jpg"; 
            vo.result.video.main.video_path = "/Avatar.mpg"; 
            vo.result.assets_path           = "/Flash/dstvassets.swf";
            vo.result.video.main.auto_start = true;

            string time = "00:01:00";

            vo.result.video.main.meta = new VideoObject.ResultObject.SubVideo.SubMain.SubMeta()
            {
                comment_count   = 0,
                description     = "Avatar",
                duration        = Convert.ToInt32(TimeSpan.Parse(time).TotalMilliseconds),
                time_ago        = "Today",
                view_count      = 0
            };

            vo.status = "ok";

            return Newtonsoft.Json.JsonConvert.SerializeObject(vo);
        }
		  
    }
}
