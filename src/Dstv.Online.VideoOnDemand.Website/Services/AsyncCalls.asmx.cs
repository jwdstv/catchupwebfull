﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Configuration;

using System.Net;
using System.Diagnostics;
using System.Web.Security;

using DStvConnect.WebUI.Platform;

using DStvConnect.WebUI.Controls;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Common;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.DataAccess;

namespace Dstv.Online.VideoOnDemand.Website.Services
{
    /// <summary>
    /// Summary description for AsynCalls
    /// </summary>
    [WebService(Namespace = "http://www.dstv.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]    
    [System.Web.Script.Services.ScriptService]
    public class AsyncCalls : System.Web.Services.WebService
    {      
        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public string InjectFlashPlayer()
        {
            var player = new StringWriter();
            var download = new StringWriter();
            
            try
            {
                var page = new System.Web.UI.Page();
                var control = (System.Web.UI.UserControl)page.LoadControl("/UserControls/Players/FlashPlayer.ascx");
                page.Controls.Add(control);

                HttpContext.Current.Server.Execute(page, player, false);
            }
            catch (System.Exception ex) {
                player.WriteLine(ex.ToString());
            }

            var response = new StandardResponse();
            response.FlashPlayer = player.ToString();

            return Newtonsoft.Json.JsonConvert.SerializeObject(response);
        }

		  [WebMethod]
		  [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
		  public string SubmitVideoRentalPayment ( string _catalogItemId, decimal _catalogItemCost, string _customerPin )
		  {
			  return Newtonsoft.Json.JsonConvert.SerializeObject(DstvUser.RentVideo(_catalogItemId, _catalogItemCost, _customerPin));
		  }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public string LoginUser(string _userName, string _password, bool _rememberMe)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(DstvUser.LoginUser(_userName, _password, _rememberMe));
        }
    }

    

    internal class ArticleResults
    {
        #region ~properties~
        public string Abstract { get; set; }
        public string Title { get; set; }
        public int ID { get; set; }
        #endregion
    }

    internal class VideoResults
    {
        #region ~properties~
        public string ImageUrl { get; set; }
        public int ProgramId { get; set; }
        public int VideoId { get; set; }
        public string ProgramName { get; set; }
        public string VideoTitle { get; set; }
        public string Runtime { get; set; }
        public string AirDate { get; set; }
        public string Abstract { get; set; }
        public string Channel { get; set; }
        public int TotalRecords { get; set; }
        public List<int> Ids { get; set; }
        public DateTime CreatedDate { get; set; }
        #endregion
    }

    internal class EPGResults
    {
    }

    internal class ExceptionResults
    {
        #region ~properties
        public string Exception { get; set; }
        public string Stack { get; set; }
        #endregion
    }

    internal class ResponseString
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string Operating { get; set; }
    }

    internal class StandardResponse
    {
        public string Download { get; set; }
        public string FlashPlayer { get; set; }
    }

    internal class HelpResults
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }

    internal class VideoObject
    {
        public string status { get; set; }
        public ResultObject result { get; set; }
        public string exception { get; set; }

        public class ResultObject
        {
            public SubVideo video { get; set; }
            public bool allow_pseudo { get; set; }
            public List<SubRelated> related_videos { get; set; }
            public string assets_path { get; set; }
            public SubUi ui { get; set; }
            public string uid { get; set; }

            public class SubVideo
            {
                public SubMain main { get; set; }
                public SubAds ads { get; set; }

                public class SubMain
                {
                    public string uid { get; set; }
                    public SubUser user { get; set; }
                    public string url { get; set; }
                    public string url_hd { get; set; }
                    public string image_path { get; set; }
                    public string title { get; set; }
                    public string video_path { get; set; }
                    public string video_path_hd { get; set; }
                    public bool auto_start { get; set; }
                    public SubMeta meta { get; set; }
                    public string thumb_path { get; set; }
                    public int start_time { get; set; }
                    public bool allow_seek { get; set; }

                    public class SubUser
                    {
                        public string uid { get; set; }
                        public string image_path { get; set; }
                        public string screen_name { get; set; }
                    }

                    public class SubMeta
                    {
                        public int view_count { get; set; }
                        public int comment_count { get; set; }
                        public int duration { get; set; }
                        public string description { get; set; }
                        public string time_ago { get; set; }
                    }
                }

                public class SubAds
                {
                    public SubPre pre { get; set; }
                    public SubPost post { get; set; }

                    public class SubPre
                    {
                        public string type { get; set; }
                        public string ad_path { get; set; }
                    }

                    public class SubPost
                    {
                        public string type { get; set; }
                        public string ad_path { get; set; }
                    }
                }

            }

            public class SubRelated
            {
                public string thumb_path { get; set; }
                public string title { get; set; }
                public string url { get; set; }
            }

            public class SubUi
            {
                public SubPlayer player_controls { get; set; }
                public SubGeneric generic_controls { get; set; }

                public class SubPlayer
                {
                    public List<string> assets { get; set; }
                    public int progress_padding_left { get; set; }
                    public int progress_padding_right { get; set; }
                    public int padding_side { get; set; }
                    public int padding_bottom { get; set; }
                }

                public class SubGeneric
                {
                    public SubGeneric()
                    {
                        this.assets = new List<string>();
                        this.data = new SubData();
                    }

                    public List<string> assets { get; set; }
                    public SubData data { get; set; }

                    public class SubData
                    {
                        public SubData()
                        {
                            this.embed_codes = new SubCodes();
                        }

                        public SubCodes embed_codes { get; set; }

                        public class SubCodes
                        {
                            public string small { get; set; }
                            public string medium { get; set; }
                            public string large { get; set; }
                        }
                    }
                }
            }
        }
    }
}
