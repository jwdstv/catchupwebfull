﻿<%@ Page Title="Forgotten Password" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master" AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.RecoverPassword" %>
<%@ Register Src="~/Controls/PasswordReset.ascx" TagName="PasswordReset" TagPrefix="DStv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
Recover Forgotten Password
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">

<DStv:PasswordReset ID="PasswordReset" runat="server" />

</asp:Content>
