﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Web;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Settings;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website
{
    public partial class TopUp : System.Web.UI.Page
    {
        public string TopUpIFrameSrc
        {
            get
            {
                //TODO:  When the connect control works, use this below
                //return WebSettings.GetSettings.TopUpPageUrl.Url.Replace("{connectUserId}", ((TitleAndContent)Master).UserProfile.CustomerId.Value);
                return WebSettings.GetSettings.TopUpPageUrl.Url;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // defensively check required query string params

            string returnUrl = SiteUtils.Url.InternalRedirector.GetRedirectUrlFromQueryString();
            string actionIndicator = Request["Action"];

				if (string.IsNullOrEmpty(returnUrl))
                throw new ArgumentNullException("returnUrl");

            if (!string.IsNullOrEmpty(actionIndicator))
            {
                Response.Redirect(returnUrl);
            }


            if (!Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect(Request.Url.PathAndQuery);
                return;
            }

            if (!Page.User.Identity.IsAuthenticated)
                throw new AuthenticationException();

        }
    }
}