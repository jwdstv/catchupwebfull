﻿<%@ Page Title="Update Profile" Language="C#" MasterPageFile="~/MasterPages/TitleAndContent.master" AutoEventWireup="true" CodeBehind="UpdateProfile.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.UpdateProfile" %>
<%@ Register Src="~/Controls/UserProfile.ascx" TagName="UserProfile" TagPrefix="DStv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
Update Profile
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
<DStv:UserProfile ID="Profile" runat="server" />
</asp:Content>
