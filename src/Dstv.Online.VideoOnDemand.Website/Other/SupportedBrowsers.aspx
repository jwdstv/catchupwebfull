﻿<%@ Page Title="Supported Web Browsers" Language="C#" MasterPageFile="~/MasterPages/TitleAndContentWithDisabledNav.master" AutoEventWireup="true" CodeBehind="SupportedBrowsers.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Other.SupportedBrowsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
Supported Web Browsers
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">

<div class="formItems">
<p>
The DStv On Demand website has been designed and optimised for the following web browsers and versions.
</p>
<br />
<asp:Repeater ID="rptBrowsers" runat="server">
<HeaderTemplate>
<ul>
</HeaderTemplate>
<ItemTemplate>
<li><%#Container.DataItem %></li>
</ItemTemplate>
<FooterTemplate>
</ul>
</FooterTemplate>
</asp:Repeater>

<br />
The browser you are using is <asp:Label ID="lblBrowser" runat="server"></asp:Label>. You can continue browsing the website in this browser, however DStv cannot guarantee that the site will display or function correctly in your current browser.
<br /><br />
Click <asp:LinkButton ID="btnContinueBrowsing" runat="server" onclick="btnContinueBrowsing_Click" >here</asp:LinkButton> to continue browsing the On Demand website with your current browser.
</div>

</asp:Content>
