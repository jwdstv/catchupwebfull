﻿<%@ Page Title="Supported Countries" Language="C#" MasterPageFile="~/MasterPages/TitleAndContentWithDisabledNav.master"
    AutoEventWireup="true" CodeBehind="SupportedCountries.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Other.SupportedCountries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    Supported Countries
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="formItems">
        <p>
            The DStv On Demand Service is currently only available in the following countries:
        </p>
        <br />
        <br />
        <%--Hiding the Boxoffice supported countries for initial Launch
        TODO: Uncomment Boxoffice when it gets reintroduced.--%>
        <%--<p>
            Box Office:</p>
        <br />
        <asp:Repeater ID="rptTerritoriesBoxOffice" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <%#Container.DataItem %></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
        <br />
        <br />--%>
        <p>
            Catch Up:</p>
        <br />
        <asp:Repeater ID="rptTerritoriesCatchUp" runat="server">
            <HeaderTemplate>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <%#Container.DataItem %></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
