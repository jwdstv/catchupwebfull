﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.Other
{
	public partial class SupportedCountries : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
        //    Hiding the Boxoffice supported countries for initial Launch
        //TODO: Uncomment Boxoffice when it gets reintroduced.
            //rptTerritoriesBoxOffice.DataSource = WebsiteSettings.SupportedTerritories.SupportedTerritoriesForProduct(SiteEnums.ProductType.BoxOffice);
            //rptTerritoriesBoxOffice.DataBind();

			rptTerritoriesCatchUp.DataSource = WebsiteSettings.SupportedTerritories.SupportedTerritoriesForProduct(SiteEnums.ProductType.CatchUp);
			rptTerritoriesCatchUp.DataBind();
		}
	}
}