﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website.Other
{
	public partial class SupportedBrowsers : System.Web.UI.Page
	{
		protected void Page_Load ( object sender, EventArgs e )
		{
			if (!IsPostBack)
			{
				WebsiteSettings.SupportedBrowsers supportedBrowsers = new WebsiteSettings.SupportedBrowsers();

				rptBrowsers.DataSource = supportedBrowsers;
				rptBrowsers.DataBind();

				lblBrowser.Text = supportedBrowsers.CurrentUserBrowser;
			}
		}

		protected void btnContinueBrowsing_Click ( object sender, EventArgs e )
		{
			WebsiteSettings.SupportedBrowsers.UserAcceptsBrowserSupport();
			string redirectUrl = SiteUtils.Url.InternalRedirector.GetRedirectUrlFromQueryString();
			if (string.IsNullOrEmpty(redirectUrl))
			{
				Response.Redirect("~/");
			}
			else
			{
				Response.Redirect(redirectUrl);
			}
		}
	}
}