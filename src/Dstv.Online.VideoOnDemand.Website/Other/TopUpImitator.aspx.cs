﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Globalization;

namespace Dstv.Online.VideoOnDemand.Website.Other
{
    public partial class TopUpImitator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_OnClick(object sender, EventArgs e)
        {

        }


        protected void btnTopUp_OnClick(object sender, EventArgs e)
        {
            CurrencyIdentifier currency = new CurrencyIdentifier("R");
            CurrencyAmount amount = new CurrencyAmount(currency, Convert.ToDecimal(txtTopUpAmount.Text));
            
            //RSS: no longer on stubs, so we cannot do this.
            //TestBroker.TopupWalletBalance(amount);
        }
    }
}