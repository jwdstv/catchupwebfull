﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TopUpImitator.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Other.TopUpImitator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Simulate a topup...  (not stubbed, cannot simulate anymore.)
        <br />
        <br />

        Top up amount R <asp:TextBox runat="server" ID="txtTopUpAmount" Text="5.50" />
        <asp:Button ID="btnTopUp" runat="server" OnClick="btnTopUp_OnClick" Text="Credit balance" />


        <asp:Button ID="btn" runat="server" Text="Top up complete" OnClientClick="return clicked();" Enabled="false" />
    </div>
    </form>

    <script language="javascript" type="text/javascript">

        function clicked() {
            window.parent.location = window.parent.location + '&Action=topup';
        }
    </script>

</body>
</html>
