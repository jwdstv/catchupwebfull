﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dstv.Online.VideoOnDemand.Website.ErrorPages
{
    public partial class GenericError : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();

            if (Request.IsLocal && exception != null)
            {
                TechnicalError.Visible = true;
                FriendlyError.Visible = false;

                if (exception.InnerException != null) // Container error normally HttpUnhandledException
                    exception = exception.InnerException;

                ErrorType.InnerText = exception.GetType().ToString().Replace("\n\r", "<br/>");
                if (exception.Message != null)
                {
                    ErrorMessage.InnerText = exception.Message.Replace("\n\r", "<br/>");
                }
                else
                {
                    ErrorMessage.InnerText = string.Empty;
                }
                if (exception.StackTrace != null)
                {
                    StackTrace.InnerText = exception.StackTrace.Replace("\n\r", "<br/>");
                }
                else
                {
                    StackTrace.InnerText = string.Empty;
                }

                CompleteMessage.InnerText = exception.ToString().Replace("\n\r", "<br/>");
                ErrorUrl.InnerText = Request.Url.ToString();
            }
        }
    }
}