﻿<%@ Page Title="Error" Language="C#" AutoEventWireup="true" CodeBehind="GenericError.aspx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.ErrorPages.GenericError" %>

<%@ Register TagPrefix="wuc" TagName="LargeBanner" Src="~/UserControls/Advert/LargeBanner.ascx" %>
<%@ Register TagPrefix="wuc" TagName="MultiChoiceFooter" Src="~/UserControls/Navigation/MultiChoiceFooter.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="ctl00_Head1">
    <title>DStv.com &gt; On Demand &gt; an error has occurred </title>
    <link href="/app_themes/onDemand/onDemand.css" rel="stylesheet" type="text/css" />
    <link href="/app_themes/onDemand/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="/app_themes/onDemand/controls.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Scripts/toolTip.js"></script>
</head>
<body>
    <form name="aspnetForm" method="post" action="GenericError.aspx"
    id="aspnetForm">
    <div id="headContainer">
        <div class="toprow">
            <div class="breadcrumbs">
                <a href="http://www.dstv.com"><strong>DStv.com</strong></a> <a href="/Home.aspx"><strong>
                    On Demand</strong></a> <strong>An Error has occurred</strong>
            </div>
        </div>
        <div class="global_header">
            <div class="headerContent">
                <div class="toplogo">
                    <a href="/Home.aspx" title="On Demand Home">
                        <img src="/app_themes/onDemand/images/logo_onDemand.png" style="border-style: none;
                            width: 113; height: 43" alt="On Demand" /></a>
                </div>
                <div class="leaderBoard">
                    <wuc:LargeBanner runat="server" ID="LargeBannerAd" AdvertSize="728x90" />
                </div>
            </div>
            <div class="global_nav">
                <ul id="MenuBar" class="MenuBarHorizontal">
                    <li><a href="/Home.aspx" class="nav" title="On Demand Home">HOME</a></li>
                    <li><a href="/Catchup/Browse.aspx" class="nav" title="Browse On Demand Video Catalogue">
                        BROWSE</a></li>
                    <li><a href="/Download/DesktopPlayer.aspx" class="nav" title="On Demand Desktop Player">
                        DESKTOP PLAYER</a></li>
                    <li><a href="/CatchUp/FindOutMore.aspx" class="nav" title="On Demand Find Out More">
                        FIND OUT MORE</a></li>
                    <li><a href="/Help/FAQ.aspx" class="nav" title="On Demand Frequently Asked Questions">
                        FAQ</a></li>
                    <li><a href="/Help/ContactUs.aspx" class="nav" title="On Demand Contact Us">CONTACT
                        US</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="global_wrapper">
        <div class="show_bb">
            <div class="bb_content" id="FriendlyError" runat="server">
                <div class="errBg">
                    <div class="errDisplay">
                        <div class="errTItle">
                            oops...</div>
                        <div class="errMsg">
                            We’re sorry, but we seem to have encountered a problem.</div>
                        <div class="errTxt">
                            <br />
                            Please try again or alternatively <a href="../Help/ContactUs.aspx">Contact Us</a>
                            and we'll try and resolve your problem as soon as possible.
                        </div>
                    </div>
                </div>
            </div>
            <div id="TechnicalError" visible="false" runat="server">
                <h1 class="formGenError">
                    <strong>&nbsp;&nbsp;&nbsp;Error occurred - Technical details</strong></h1>
                <br />
                <br />
                <br />
                <table style="border: 1px solid black; background-color: White;" cellpadding="5"
                    cellspacing="5" runat="server" id="tblError">
                    <tr>
                        <td style="width: 200px; vertical-align: top;">
                            <b>Error Type:</b>
                        </td>
                        <td>
                            <span runat="server" id="ErrorType" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px; vertical-align: top;">
                            <b>URL:</b>
                        </td>
                        <td>
                            <span runat="server" id="ErrorUrl" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px; vertical-align: top;">
                            <b>Error Description:</b>
                        </td>
                        <td>
                            <span runat="server" id="ErrorMessage" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px; vertical-align: top;">
                            <b>Error Stacktrace:</b>
                        </td>
                        <td>
                            <span runat="server" id="StackTrace" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Complete:</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span runat="server" id="CompleteMessage" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="global_footer">
                <wuc:MultiChoiceFooter ID="wucMultiChoiceFooter" runat="server" ViewStateMode="Disabled">
                </wuc:MultiChoiceFooter>
                <br />
            </div>
        </div>
    </form>
</body>
</html>
