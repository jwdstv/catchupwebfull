﻿<%@ Page Title="Page Not Found" Language="C#" AutoEventWireup="true" CodeBehind="PageNotFoundError.aspx.cs"
    Inherits="Dstv.Online.VideoOnDemand.Website.ErrorPages.PageNotFoundError" %>

<%@ Register TagPrefix="wuc" TagName="LargeBanner" Src="~/UserControls/Advert/LargeBanner.ascx" %>
<%@ Register TagPrefix="wuc" TagName="MultiChoiceFooter" Src="~/UserControls/Navigation/MultiChoiceFooter.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="ctl00_Head1">
    <title>DStv.com &gt; On Demand &gt; an error has occurred </title>
    <link href="/app_themes/onDemand/onDemand.css" rel="stylesheet" type="text/css" />
    <link href="/app_themes/onDemand/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
    <link href="/app_themes/onDemand/controls.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Scripts/toolTip.js"></script>
</head>
<body>
    <form name="aspnetForm" method="post" action="GenericError.aspx" onsubmit="javascript:return WebForm_OnSubmit();"
    id="aspnetForm">
    <div id="headContainer">
        <div class="toprow">
            <div class="breadcrumbs">
                <a href="http://www.dstv.com"><strong>DStv.com</strong></a> <a href="/Home.aspx"><strong>
                    On Demand</strong></a> <strong>An Error has occurred</strong>
            </div>
            <!-- LOGGED OUT BEGIN -->
            <div class="logreg">
                <table id="ctl00_wucProfileStatusBar_tblNotLoggedIn" cellpadding="1" cellspacing="0"
                    border="0">
                    <tr>
                        <td class="logregitem">
                            <a id="ctl00_wucProfileStatusBar_lnkLogin" class="smlLink" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$wucProfileStatusBar$lnkLogin&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, true))">
                            </a>
                        </td>
                        <td class="blueSep">
                        </td>
                        <td class="logregitem">
                            <a href="/Profile/RecoverPassword.aspx" class="smlLink" title="Click to recover forgotten password">
                            </a>
                        </td>
                        <td class="blueSep">
                        </td>
                        <td class="logregitem">
                            <a href="/SignUp.aspx" class="smlLink" title="Click to register/sign up with DStv Online">
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="global_header">
            <div class="headerContent">
                <div class="toplogo">
                    <a href="/Home.aspx" title="On Demand Home">
                        <img src="/app_themes/onDemand/images/logo_onDemand.png" style="border-style: none;
                            width: 113; height: 43" alt="On Demand" /></a>
                </div>
                <div class="leaderBoard">
                    <!-- CMS Content Control by DStv Online Here -->
                    <%--<img src="/tmp/Images/DStvOnlineControls/LeaderBoard.jpg" title="// TODO: DStv Online Team"
                        alt="// TODO: DStv Online Team" style="border-bottom-style: none;" />--%>
                    <wuc:LargeBanner runat="server" ID="LargeBannerAd" AdvertSize="728x90" />
                </div>
            </div>
            <div class="global_nav">
                <script type="text/javascript">
                    function DoSearch() {
                        var searchTerm = $.trim($('#txtSearchTerm').val());
                        if (searchTerm.length > 0) {
                            document.location.href = '/Video/Search.aspx?searchTerm=' + searchTerm;
                        }
                        else {
                            alert("Please enter a word or term to search for.");
                            $('#txtSearchTerm').focus();
                        }
                    }

                    function SearchKeyPress(e) {
                        var evt = e ? e : window.event;
                        if (evt.keyCode == 13) {
                            DoSearch();
                            return false;
                        }
                    }
 
                </script>
                <ul id="MenuBar" class="MenuBarHorizontal">
                    <li><a href="/Home.aspx" class="nav" title="On Demand Home">HOME</a></li>
                    <li><a href="/Catchup/Browse.aspx" class="nav" title="Browse On Demand Video Catalogue">
                        BROWSE</a></li>
                    <%--<li><a href="/OpenTime/Home.aspx" class="nav-openTime" title="Open Time Home">OPEN TIME</a></li>--%>
                    <li><a href="/Download/DesktopPlayer.aspx" class="nav" title="On Demand Desktop Player">
                        DESKTOP PLAYER</a></li>
                    <li><a href="/CatchUp/FindOutMore.aspx" class="nav" title="On Demand Find Out More">
                        FIND OUT MORE</a></li>
                    <li><a href="/Help/FAQ.aspx" class="nav" title="On Demand Frequently Asked Questions">
                        FAQ</a></li>
                    <li><a href="/Help/ContactUs.aspx" class="nav" title="On Demand Contact Us">CONTACT
                        US</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="global_wrapper">
        <div class="show_bb">
            <div class="errBg">
                <div class="errDisplay">
                    <div class="errTItle">
                        oops...</div>
                    <div class="errMsg">
                        We’re sorry, the page you have requested does not exist.</div>
                    <div class="errTxt">
                        <br />
                        <a href="../Home.aspx">Click here</a> to return to the home page.
                        <br />
                    </div>
                </div>
            </div>
            <div class="global_footer">
                <wuc:MultiChoiceFooter ID="wucMultiChoiceFooter" runat="server" ViewStateMode="Disabled">
                </wuc:MultiChoiceFooter>
                <br />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
