﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using DStvConnect.WebUI.Controls;

namespace Dstv.Online.VideoOnDemand.Website
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            // sign the user out and destroy the current authentication ticket
            Session.Abandon();
            Page.Request.Cookies.Clear();
            FormsAuthentication.SignOut();

            SessionManager.ClearCookies();

            // redirect the user to the landing page
            string url = Request.ServerVariables["HTTP_REFERER"];
            if (!(url != null && !url.ToLower().Contains("account")))//If Url null or if it is no account... redirect to the application path
            {
                url = ClassLibrary.SiteUtils.WebAppRootPath;
            }
            

            Page.Response.Redirect(url, false);
        }
    }
}