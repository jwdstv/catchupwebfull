﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CloseParent.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.CloseParent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Close</title>
    <link href="/App_Themes/Connect/dstvConnect.css" rel="stylesheet" type="text/css" />
    <link href="/app_themes/onDemand/controls.css" rel="stylesheet" type="text/css" />
    <link href="/app_themes/onDemand/onDemand.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.urldecoder.min.js"></script>
    <script type="text/javascript" src="Scripts/Site.js"></script>
    <script type="text/javascript">
        $(function () {
            var isInIFrame = (window.location != window.parent.location) ? true : false;

            if (isInIFrame) {
                var redirectURL = GetQueryString("RedirectUrl", window.parent.location.href); // if no RedirectUrl qs parameter exists, close this page (tell the parent page to redirect to itsef)
                redirectURL = $.url.decode(redirectURL);

                window.parent.showLoadingLightbox(redirectURL);
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    </form>
</body>
</html>
