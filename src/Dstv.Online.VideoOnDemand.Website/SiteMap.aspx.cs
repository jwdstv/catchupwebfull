﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Website.UserControls.Shared.LightTreeView;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;

namespace Dstv.Online.VideoOnDemand.Website
{
	public partial class SiteMap : BasePage
	{

        protected void Page_Load ( object sender, EventArgs e )
		{

            tvHome.TreeView = new LightTreeView(tvHome.ClientID);

            if (HideBoxOffice)
            {
                tvHome.TreeView.Nodes.Add("Browse", "/CatchUp/Browse.aspx");
            }
            else { 
                tvHome.TreeView.Nodes.Add("Browse", "/BoxOffice/Browse.aspx");
            }

            if (!HideBoxOffice)
            {

                Node boxOffice = new Node("BoxOffice", "/BoxOffice/Home.aspx");
                boxOffice.IsExpanded = true;
                boxOffice.AddChild("New Releases", "/BoxOffice/Browse.aspx?SortFilter=LatestReleases");
                boxOffice.AddChild("Coming Soon", "/BoxOffice/Browse.aspx?SortFilter=ForthcomingReleases");
                boxOffice.AddChild("Last Chance", "/BoxOffice/Browse.aspx?SortFilter=LastChance");
                boxOffice.AddChild("About BoxOffice", "/Help/FAQ.aspx");

                tvHome.TreeView.Nodes.Add(boxOffice);
            }

            Node catchUp = new Node("CatchUp", "/CatchUp/Home.aspx");
            catchUp.IsExpanded = true;

            catchUp.AddChild("Latest Video", "/CatchUp/Browse.aspx?EditorialId=&SortFilter=LatestReleases");
            catchUp.AddChild("Most Popular", "/CatchUp/Browse.aspx?SortFilter=MostPopular");
            catchUp.AddChild("Last Chance", "/CatchUp/Browse.aspx?SortFilter=LastChance");
            catchUp.AddChild("About CatchUp", "/Help/FAQ.aspx?Section=CatchUp_About");

            tvHome.TreeView.Nodes.Add(catchUp);

            tvHome.TreeView.Nodes.Add(new Node("Desktop Player", "/Download/DesktopPlayer.aspx"));
            tvHome.TreeView.Nodes.Add(new Node("FAQs", "/Help/FAQ.aspx"));
            tvHome.TreeView.Nodes.Add(new Node("Terms & Conditions", "http://www.dstv.com/Pages/TermsConditions/"));
            tvHome.TreeView.Nodes.Add(new Node("Contact Us", "/Help/ContactUs.aspx"));
		}
	}
}