﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/MasterPages/Base.master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Home" %>
<%@ Register src="~/UserControls/Content/CMSContent.ascx" tagname="CMSContent" tagprefix="wuc" %>
<%@ Register src="~/UserControls/Video/LatestProduct.ascx" tagname="LatestProduct" tagprefix="wuc" %>
<%@ Register Src="~/UserControls/Billboard/Billboard.ascx" TagName="BillBoard" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
	<!-- CMS Content -->
<%--<div class="block980_bg" style="vertical-align:middle;">
	
    <script src="/Scripts/swfobject2.2.js" type="text/javascript"></script>
    <wuc:BillBoard ID="BoxOfficeBillBoard" ProductId="3" runat="server"  />
</div>--%>
<div class="block980_bg">
    <div class="dstvflash">
        <script src="/Scripts/swfobject2.2.js" type="text/javascript"></script>
        <wuc:BillBoard ID="BoxOfficeBillBoard" runat="server" FontColor="BBBBBB" />
    </div>
    <div class="promo">
    	<div class="hdr">Rent movies and watch TV online</div>
        <div class="promoList">
        	<ul>
                <li><a class="boxOffice" href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>BoxOffice/Home.aspx">BoxOffice</a> lets you rent the latest movies, on demand. You don't even need to be a DSTV subscriber!</li>
            	<li type="disc">Watch your favourite TV shows, sports and movies online whenever you want with <a class="catchUp" href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>CatchUp/Home.aspx">CatchUp</a></li>
            </ul>
        </div>        
        <div class="floatLeft">
            <% if (!User.Identity.IsAuthenticated)// Check if user is logged in or not
           { %>
        	<table cellspacing="10" cellpadding="0" border="0" width="100%">
              <tbody><tr>
                <td>
                    <a href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>SignUp.aspx?product=ondemand"><img alt="Sign Up" src="/app_themes/onDemand/images/btn_signUp.gif"></a>
                        <%--<a class="connectsmlLink" href="<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>Register.aspx?retunUrl=<%=Dstv.Online.VideoOnDemand.Website.ClassLibrary.SiteUtils.WebAppRootPath %>closeparent.aspx?hl=en&tab=nw&lightbox[iframe]=true&lightbox[width]=770&lightbox[height]=460" id="SignUpLightbox"><img height="44" width="99" alt="Sign Up" src="/app_themes/onDemand/images/btn_signUp.gif" /></a>--%>                
                </td>
                <td class="auth_nbtxt">It's quick &amp; easy.</td>
              </tr>
            </tbody></table>
            <%} %>
      </div>
    </div>
  </div>

<!-- Site/Product Tabs -->
<div class="lrgTabs">
  	<div class="block450">
    	<div class="padding15px"></div>
        <div class="floatLeft">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="45"><img src="/app_themes/onDemand/images/lrgArrow-boxOffice.png" width="40" height="40" /></td>
                <td><img src="/app_themes/onDemand/images/hdr-boxOffice.png" alt="BoxOffice" /></td>
              </tr>
          </table>
        </div>
        <div class="padding15px"></div>
        <div class="floatLeft">
        	<table width="100%" border="0" cellspacing="3" cellpadding="0">
              <tr>
                <td width="100"><a href="/BoxOffice/Home.aspx" title="On Demand Box Office"><img src="/app_themes/onDemand/images/btn_enter-boxOffice.gif" width="90" height="50" alt="ENTER" /></a></td>
                <td>Rent movies online for only <%=ConfigurationManager.AppSettings["BillBoardBoxOfficePrice"]%></td>
              </tr>
            </table>
        </div>
    </div>
    <div class="divider"><img src="/app_themes/onDemand/images/lrgTabs_divider.jpg" /></div>
    <div class="block450">
    	<div class="padding15px"></div>
        <div class="floatLeft">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="45"><img src="/app_themes/onDemand/images/lrgArrow-catchUp.png" width="40" height="40" /></td>
                <td><img src="/app_themes/onDemand/images/hdr-catchUp.png" alt="CatchUp" /></td>
              </tr>
            </table>
        </div>
        <div class="padding15px"></div>
        <div class="floatLeft">
        	<table width="100%" border="0" cellspacing="3" cellpadding="0">
              <tr>
                <td width="100"><a href="/CatchUp/Home.aspx" title="On Demand Catch Up"><img src="/app_themes/onDemand/images/btn_enter-catchUp.gif" width="90" height="50" alt="ENTER" /></a></td>
                <td>Watch TV online as a DStv Subscriber</td>
              </tr>
            </table>
        </div>
    </div>
    

</div>

<!-- Latest -->
<div class="cols3">
<!-- // TODO: Use SEO Urls -->
<wuc:LatestProduct id="wucLatestProductBO" runat="server" ViewStateMode="Disabled"
	RuntimeDisplay="RuntimeInMinutes" 
	ListTitle="Top 5"
    >
    <VideoFilter
        ProductType="BoxOffice" 
        VideoCount="5" 
        SortFilter="LatestReleases"
        VideoTypes="Movie"
        />
    <PageLinks 
        VideoDetailUrl="/BoxOffice/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/BoxOffice/Browse.aspx?SortFilter={SortFilter}"
        />
    </wuc:LatestProduct>
    
<wuc:LatestProduct id="wucLatestProductCU" runat="server" ViewStateMode="Disabled"
	RuntimeDisplay="RuntimeInMinutes"
	ListTitle="Top 5"
	>
    <VideoFilter
        ProductType="CatchUp" 
        VideoCount="5" 
        SortFilter="LatestReleases"
        VideoTypes="ShowEpisode,ShowClip,Movie"
        />
    <PageLinks 
       VideoDetailUrl="/CatchUp/VideoDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    ShowDetailUrl="/CatchUp/ShowDetail.aspx?ProgramId={ProgramId}&VideoId={VideoId}"
	    MoreUrl="/CatchUp/Browse.aspx?SortFilter={SortFilter}"
        />
    </wuc:LatestProduct>

</div>

</asp:Content>
