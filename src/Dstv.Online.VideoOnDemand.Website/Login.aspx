﻿<%@ Page Title="Login" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Dstv.Online.VideoOnDemand.Website.Login" %>

<%@ Register src="~/Controls/Login.ascx" tagname="Login" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="/App_Themes/Connect/dstvConnect.css" rel="stylesheet" type="text/css" />
    <link href="/App_Themes/onDemand/controls.css" rel="stylesheet" type="text/css" />


     <script src="../Scripts/serverproxy.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/lightbox/themes/default/jquery.lightbox.css" />
    <script type="text/javascript" src="../Scripts/lightbox/jquery.lightbox.js"></script>

    <style type="text/css">
        html {
            overflow: auto;
        }   
        
        body {
            background-color: #F7F8FA;
        }
        
         #wrapper {
        bottom:50%;
        height:470px;
        position:absolute;
        right:50%;
        width:700px;
        }



        #container {
  	        position: relative;
  	        left: 50%;
  	        top: 50%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper"> 
            <div id="container"> 
                <uc1:Login ID="Login1" runat="server" ReturnURL="~/CloseParent.aspx" />
            </div>
        </div>
   </form>
   <script type="text/javascript">

       $(function () {
           loadLightBox_forgot();
           override_signupLinkUrl();

           var isInIFrame = (window.location != window.parent.location) ? true : false;

           if (isInIFrame) {
               window.parent.hideScrollbars();
           }
       });

       function loadLightBox_forgot() {
           $("a[href$='Forgotten.aspx']").click(function (ev) {
               window.parent.triggerForgotPasswordClick();
               ev.preventDefault();
           });
       }   
     
       function override_signupLinkUrl() {
           $("a[href$='SignUp.aspx']").attr("href", "/closeparent.aspx?RedirectUrl=%2fSignUp.aspx");          
       }
        

        
    </script>
</body>
</html>
