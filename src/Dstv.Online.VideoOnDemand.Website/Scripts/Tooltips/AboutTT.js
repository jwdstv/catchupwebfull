﻿$().ready(function (e) {
    $("#amHelp").mouseover(function () { tooltip.show('Tell us a little bit about yourself so that we can tailor your DStv experience.'); }).mouseout(function () { tooltip.hide(); });
    $("#maHelp").mouseover(function () { tooltip.show('Link your Smartcard and MWEB accounts and watch DStv online anywhere, anytime.'); }).mouseout(function () { tooltip.hide() });
    $("#alHelp").mouseover(function () { tooltip.show('Receive reminders for your favourite programs.'); }).mouseout(function () { tooltip.hide(); });
    $("#nlHelp").mouseover(function () { tooltip.show('Subscribe to a variety of newsletters and get the latest updates on your favourite shows.'); }).mouseout(function () { tooltip.hide(); });
    $("#unHelp").mouseover(function () { tooltip.show('Changes to your username will only reflect during your next log on.'); }).mouseout(function () { tooltip.hide(); });
    $("#pbHelp").mouseover(function () { tooltip.show('<strong>What is this?</strong><br/>The progress bar indicates how close you are to completing your DStv Connect Profile<br/>Click for more info'); }).mouseout(function () { tooltip.hide(); });
});