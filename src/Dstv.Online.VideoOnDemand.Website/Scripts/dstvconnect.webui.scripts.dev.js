﻿Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

function startRequest(sender, e) {
    //disable button during the AJAX call
    document.getElementById('<%=CheckAvailableImageButton.ClientID%>').disabled = true;
    //document.getElementById('<%=CheckAvailableImageButton.ClientID%>').value = 'Checking availability...';
}
function endRequest(sender, e) {
    //re-enable button once the AJAX call has completed
    document.getElementById('<%=CheckAvailableImageButton.ClientID%>').disabled = false;
    //document.getElementById('<%=CheckAvailableImageButton.ClientID%>').value = 'Get Current Time';
}

