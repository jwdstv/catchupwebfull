﻿// All Javascript Calls Should Be made via this js file
//Used For The More info Button on Abstracts
function toggleVID(ID) {
    var myStr = ID.toString();
    jQuery('#customMoreButton_' + myStr).slideToggle();
    jQuery('#customMoreSpan_' + myStr).slideToggle("slow,");
    jQuery('#customDotSpan_' + myStr).slideToggle("fast,");
}

function toggleLinks() {   
    jQuery('#PlayerDownloadLink').slideToggle();
}

function toggleDiv(div) {
    jQuery('#' + div).slideToggle();    
}


////Enabling waterMarks
//$(document).ready(function() {
//$.updnWatermark.attachAll();
////$.updnWatermark.attachAll({ cssClass: "updnWatermark" });
////$.updnWatermark.attachAll({ cssClass: "searchInput" });
////$.updnWatermark.attachAll({ cssClass: "searchInput" });
//});
function GetURL() {
    //var url = GetApplicationDefaultPage(); //local host
    //var url = "http://staging.dstv.com/ondemand/default.aspx"; //Staging
    var url = "http://ondemand.dstv.com/default.aspx"; //Live
    //alert(url);
    return url
}




function LoadAdd(divName) {


    var url = GetURL();
    //alert(url);
    $.ajax({
        type: "POST",
        url: url + "/LoadAdd",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(msg) {
            // Hide the fake progress indicator graphic.
            //$('#RSSContent').removeClass('loading');

            // Insert the returned HTML into the <div>.
            //$('#RSSContent').html(msg.d);
        //alert(msg.d);
        $('#' + divName).html(msg.d);
        //$get('#LargeBannerAdd').html = msg.d;
        $('#' + divName).fadeIn(500);
        }
    });
}
function LoadAddSmall(divName) {


    var url = GetURL();
    //alert(url);
    $.ajax({
        type: "POST",
        url: url + "/LoadSmallAdd",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(msg) {
            // Hide the fake progress indicator graphic.
            //$('#RSSContent').removeClass('loading');

            // Insert the returned HTML into the <div>.
            //$('#RSSContent').html(msg.d);
            //alert(msg.d);
            $('#' + divName).html(msg.d);
            //$get('#LargeBannerAdd').html = msg.d;
            $('#' + divName).fadeIn(500);
        }
    });
}
//LoadProgramList(int _SortBy, int _VideoFileTypeID, string _HeaderText, string _MoreLinkButton )
function LoadProgramList(divName, _SortBy, _VideoProgramTypeID, _HeaderText, _MoreLinkButton) {


    var url = GetURL();
    //alert(url);
    $.ajax({
        type: "POST",
        url: url + "/LoadProgramList",
        data: "{'_SortBy':'" + _SortBy + "','_VideoProgramTypeID':'" + _VideoProgramTypeID + "','_HeaderText':'" + _HeaderText + "','_MoreLinkButton':'" + _MoreLinkButton + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(msg) {
            $('#' + divName).fadeIn(500);
            $('#' + divName).html(msg.d);
            //$get('#LargeBannerAdd').html = msg.d;
            
        }
    });
}



var domWrite = (function() {            // by Frank Thuerigen
    // private 

    var dw = document.write,              // save document.write()
          myCalls = [],                // contains all outstanding Scripts
          t = '';                      // timeout

    function startnext() {                 // start next call in pipeline
        if (myCalls.length > 0) {
            if (Object.watch) console.log('next is ' + myCalls[0].f.toString());
            myCalls[0].startCall();
        }
    }

    function evals(pCall) {            // eval embedded script tags in HTML code
        var scripts = [],
      script,
      regexp = /<script[^>]*>([\s\S]*?)<\/script>/gi;
        while ((script = regexp.exec(pCall.buf))) scripts.push(script[1]);
        scripts = scripts.join('\n');
        if (scripts) {
            eval(scripts);
        }
    }

    function finishCall(pCall) {
        pCall.e.innerHTML = pCall.buf;             // write output to element
        evals(pCall);
        document.write = dw;                        // restore document.write()
        myCalls.shift();
        window.setTimeout(startnext, 50);
    }

    function testDone(pCall) {
        var myCall = pCall;
        return function() {
            if (myCall.buf !== myCall.oldbuf) {
                myCall.oldbuf = myCall.buf;
                t = window.setTimeout(testDone(myCall), myCall.ms);
            }
            else {
                finishCall(myCall);
            }
        }
    }

    function MyCall(pDiv, pSrc, pFunc) {                    // Class
        this.e = (typeof pDiv == 'string' ?
             document.getElementById(pDiv) :
             pDiv),                     // the div element
  this.f = pFunc || function() { },
  this.stat = 0,                         // 0=idle, 1=waiting, 2=running, 3=finished
  this.src = pSrc,                       // script source address
  this.buf = '',                         // output string buffer
  this.oldbuf = '',                      // compare buffer
  this.ms = 100,                         // milliseconds
  this.scripttag;                        // the script tag 
    }

    MyCall.prototype = {
        startCall: function() {
            this.f.apply(window);                 // execute settings function
            this.stat = 1;
            var that = this;                            // status = waiting
            document.write = (function() {
                var o = that,
        cb = testDone(o),
        t;
                return function(pString) {            // overload document.write()
                    window.clearTimeout(t);
                    o.stat = 2;                             // status = running
                    window.clearTimeout(t);
                    o.oldbuf = o.buf;
                    o.buf += pString;                     // add string to buffer
                    t = window.setTimeout(cb, o.ms);
                };
            })();
            var s = document.createElement('script');
            s.setAttribute('language', 'javascript');
            s.setAttribute('type', 'text/javascript');
            s.setAttribute('src', this.src);
            document.getElementsByTagName('head')[0].appendChild(s);
        }
    }

    return function(pDiv, pSrc, pFunc) {  // public
        var c = new MyCall(pDiv, pSrc, pFunc);
        myCalls.push(c);
        if (myCalls.length === 1) {
            startnext();
        }
    }
})();

function GetQueryString(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}
