﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary.Data;
using Dstv.Online.VideoOnDemand.Website.ClassLibrary;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Website.MyAccounts
{
    public partial class CatchUp : System.Web.UI.Page
    {
        /// <summary>
        /// Defines what message the user will see on the popUp
        /// </summary>
        public string VideoDisplayMessage;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (DstvUser.IsAuthenticated)
            {
                // Add control to place holder
                UserControl uc1 = (UserControl)Page.LoadControl("~/Controls/CatchUpRegistration.ascx");
                plcAbout.Controls.Add(uc1);

                if (IsPostBack)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), Guid.NewGuid().ToString(), "$(function() { if (checkIfLinked()) { $('#trLinkAccountInfoHead').hide(); $('#trLinkAccountInfo').hide(); } updateCongratulationMessageDisplay(); });", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), Guid.NewGuid().ToString(), "$(function() { triggerLoginClick(); });", true);

            }
        }

    }
}