
#region Declarations

using System;
using System.Web;
using System.Web.Caching;
using System.Configuration;
using System.Xml.Serialization;
using System.Xml;

#endregion

namespace Dstv.Online.VideoOnDemand.URLRewriter.Config
{
	/// <summary>
	/// Specifies the configuration settings in the Web.config for the RewriterRule.
	/// </summary>
	[Serializable()]
	[XmlRoot("RewriterConfig")]
	public class RewriterConfiguration
	{
		/// <summary>
		/// Instance of the rules collection
		/// </summary>
		private RewriterRuleCollection rules;

		/// <summary>
		/// GetConfig() returns an instance of the <b>RewriterConfiguration</b> class with the values populated from
		/// the Web.config file.  It uses XML deserialization to convert the XML structure in Web.config into
		/// a <b>RewriterConfiguration</b> instance.
		/// </summary>
		/// <returns>A <see cref="RewriterConfiguration"/> instance.</returns>
		public static RewriterConfiguration GetConfig()
		{
			if (HttpContext.Current.Cache["RewriterConfig"] == null)
				HttpContext.Current.Cache.Insert("RewriterConfig", ConfigurationSettings.GetConfig("RewriterConfig"));

			return (RewriterConfiguration) HttpContext.Current.Cache["RewriterConfig"];
		}

		/// <summary>
		/// A <see cref="RewriterRuleCollection"/> instance that provides access to a set of <see cref="RewriterRule"/>s.
		/// </summary>
		public RewriterRuleCollection Rules
		{
			get { return rules;  }
			set { rules = value; }
		}
	}

    /// <summary>
    /// Deserializes the markup in Web.config into an instance of the <see cref="RewriterConfiguration"/> class.
    /// </summary>
    public class RewriterConfigSerializerSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlSerializer ser = new XmlSerializer(typeof(RewriterConfiguration));

            // Return the Deserialized object from the Web.config XML
            return ser.Deserialize(new XmlNodeReader(section));
        }
    }
}
