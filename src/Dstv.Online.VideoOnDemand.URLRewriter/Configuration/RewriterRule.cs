using System;

namespace Dstv.Online.VideoOnDemand.URLRewriter.Config
{
	[Serializable()]
	public class RewriterRule
	{
		/// <summary>
		/// Gets or sets the pattern to look for
		/// </summary>
        public string Rule
        {
            get;
            set;
        }

		/// <summary>
		/// The string to replace the pattern with, if found.
		/// </summary>
        public string RedirectTo
        {
            get;
            set;
        }

        /// <summary>
        /// The regular expression to extract the query string
        /// </summary>
        public string AppendRule
        {
            get;
            set;
        }

        /// <summary>
        /// The name of the query string
        /// that has to be appended to the url
        /// </summary>
        public string Append
        {
            get;
            set;
        }

		/// <summary>
		/// The format for the SEO friendly URL to generate
		/// </summary>
		  public string SEOFormatUrl
		  {
			  get;
			  set;
		  }
	}
}
