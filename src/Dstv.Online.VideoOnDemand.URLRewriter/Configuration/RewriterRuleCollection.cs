
#region References

using System;
using System.Collections;

#endregion

namespace Dstv.Online.VideoOnDemand.URLRewriter.Config
{
	[Serializable()]
	public class RewriterRuleCollection : CollectionBase
	{
		/// <summary>
		/// Adds a new RewriterRule to the collection.
		/// </summary>
		/// <param name="r">A RewriterRule instance.</param>
		public virtual void Add(RewriterRule r) {
			this.InnerList.Add(r);
		}

		/// <summary>
		/// Gets or sets a RewriterRule at a specified ordinal index.
		/// </summary>
		public RewriterRule this[int index]
		{
			get { return (RewriterRule) this.InnerList[index]; }
			set { this.InnerList[index] = value; }
		}
	}
}
