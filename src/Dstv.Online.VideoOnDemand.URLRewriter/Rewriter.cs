﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.URLRewriter.Config;
using System.Text.RegularExpressions;

#endregion

namespace Dstv.Online.VideoOnDemand.URLRewriter
{
    public class Rewriter
    {
        /// <summary>
        /// The url rewriter rules
        /// </summary>
        private static RewriterRuleCollection rules;

        static Rewriter() {
            rules = URLRewriter.Config.RewriterConfiguration.GetConfig().Rules;
        }

        public static string ParseUrl(string _url)
        {
            string result = _url;

            // Iterate through each rule
            foreach (RewriterRule rule in rules)
            {
                if (Regex.Match(_url, rule.Rule, RegexOptions.IgnoreCase).Success)
                {
                    result = rule.RedirectTo;

                    // Append any additional query string names to the end of the url, specifcally in the case of the search page.
                    if (rule.Append != null && rule.Append != string.Empty)
                        result = result + rule.Append;

                    // Check if we should extract the query string from the current url, and append this query string to the end of the result url
                    if (rule.AppendRule != null && rule.AppendRule != string.Empty)
                    {
                        Match expResult = Regex.Match(_url, rule.AppendRule, RegexOptions.IgnoreCase);

                        if (expResult.Success)
                            result = result + expResult.Groups[expResult.Groups.Count - 1].Value;
                    }

                    return result;
                }
            }

            return result;
        }

		  public static string GetSEOFormatUrl ( string rawUrl )
		  {
			  foreach (RewriterRule rule in rules)
			  {
				  if (rule.RedirectTo.Equals(rawUrl, StringComparison.OrdinalIgnoreCase))
				  {
					  return rule.SEOFormatUrl;
				  }
			  }

			  return null;
		  }
    }
}
