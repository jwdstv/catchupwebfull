﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    public class RentalUser : IRentalUser
    {
        #region IRentalUser Members

        public string UserId
        {
            get;

            set;
        }

        public string UserEmail
        {
            get;
            set;
        }

        public string UserFullName
        {
            get;
            set;
        }
        public string UserPin
        {
            get;
            set;
        }

        #endregion


    }
}
