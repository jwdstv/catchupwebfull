﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
	[Serializable]
    public class VideoRentalType : IVideoRental
    {
        public string RentalTypeDescription
        {
            get { return "BoxOfficeRental"; }
        }


        #region IVideoRental Members

        public int VideoRentalPeriodInHours
        {
            get;
            set;
        }

        public Guid OrderDetailsId
        {
            get;
            set;
        }

        public string RentalName
        {
            get;
            set;
        }

        public string RentalDescription
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public string TransactionReference
        {
            get;
            set;
        }

        public DateTime ActivationDate
        {
            get;
            set;
        }

        public DateTime ExpiryDate
        {
            get;
            set;
        }

        public DateTime DateOfPurchase
        {
            get;
            set;
        }

        public string VideoId
        {
            get;
            set;
        }

        /// <summary>
        /// Has this detail item
        /// passed it's expiry date.
        /// </summary>
        public bool HasRentalExpired
        {
            get
            {
                if (ExpiryDate != null)
                    return (DateTime.Now > ExpiryDate);

                return false;
            }
        }

		  public bool HasVideoExpired
		  {
			  get;
			  set;
		  }

        /// <summary>
        /// Link to the video details
        /// </summary>
        public string DetailLink
        {
            get;
            set;
        }

        #endregion
    }
}
