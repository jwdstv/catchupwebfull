﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    public class ResultType : IResult
    {

        #region IResult Members

        public bool Success
        {
            get;
            set;
        }

        public string ResultInfo
        {
            get;
            set;
        }

        public Exception ResultException
        {
            get; 
            set; 
        }

        #endregion
    }
}
