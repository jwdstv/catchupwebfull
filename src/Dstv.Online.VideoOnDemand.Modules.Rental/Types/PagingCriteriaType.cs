﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Modules.Rental.Interfaces;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    public class PagingCriteriaType : IPagingCriteria
    {

        #region IPagingCriteria Members

        public int ItemsPerPage
        {
            get;
            set;
        }

        public int CurrentPage
        {
            get;
            set;
        }

        #endregion
    }
}
