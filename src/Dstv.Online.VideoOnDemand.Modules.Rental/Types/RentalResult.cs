﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.Modules.Rental.Interfaces;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    [DataContract]
	[Serializable]
    public class RentalResult : IRentalResult
    {
        public RentalResult(List<VideoRentalType> rentals, int totalResults, int currentPage)
        {
            Rentals = rentals;
            TotalResults = totalResults;
            CurrentPage = currentPage;
        }

        #region IRentalResult Members

        [DataMember]
        public List<VideoRentalType> Rentals { get; internal set; }

        [DataMember]
        public int TotalResults { get; internal set; }

        [DataMember]
        public int CurrentPage { get; internal set; }

        #endregion
    }
}
