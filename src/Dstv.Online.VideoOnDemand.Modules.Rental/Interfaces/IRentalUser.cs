﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    public interface IRentalUser
    {
         string UserPin
        {
            get;
            set;
        }

        string UserId
        {
            get;
            set;
        }

        string UserEmail
        {
            get;
            set;
        }

        string UserFullName
        {
            get;
            set;
        }
    }
}
