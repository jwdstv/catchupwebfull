﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    public interface IVideoRental
    {
        int VideoRentalPeriodInHours
        {
            get;
            set;
        }

        Guid OrderDetailsId
        {
            get;
            set;
        }

        string RentalName
        {
            get;
            set;
        }

        string RentalDescription
        {
            get;
            set;
        }

        Decimal Amount
        {
            get;
            set;
        }

        string TransactionReference
        {
            get;
            set;
        }

        DateTime ActivationDate
        {
            get;
            set;
        }

        DateTime ExpiryDate
        {
            get;
            set;
        }

        DateTime DateOfPurchase
        {
            get;
            set;
        }

        string VideoId
        {
            get;
            set;
        }
    }
}
