﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Modules.Rental;

namespace Dstv.Online.VideoOnDemand.Modules.Rental.Interfaces
{
    public  interface IRentalResult
    {
        List<VideoRentalType> Rentals { get; }
        int TotalResults { get; }
        int CurrentPage { get; }
    }
}
