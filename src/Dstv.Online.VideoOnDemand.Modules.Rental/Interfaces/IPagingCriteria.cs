﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental.Interfaces
{
    public interface IPagingCriteria
    {
        int ItemsPerPage
        {
            get;
            set;
        }

        int CurrentPage
        {
            get;
            set;
        }
    }
}
