﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
    public interface IResult
    {
        bool Success
        {
            get;
            set;
        }

        string ResultInfo
        {
            get;
            set;
        }
    }
}
