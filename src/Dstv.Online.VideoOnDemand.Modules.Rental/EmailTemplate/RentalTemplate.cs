﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Modules.Rental.EmailTemplate;

namespace Dstv.Online.VideoOnDemand.Common.Smtp.Template
{
    public class RentalTemplate
    {
        private StringBuilder templateContent = new StringBuilder();

        private const string usernameToken = @"{UserFullName}";

        public RentalTemplate()
        {
            string templatePath = EmailTemplate.Default.TemplateFileFullPath;

            if (templatePath.StartsWith("~") && System.Web.HttpContext.Current != null)
            {
                templatePath = System.Web.HttpContext.Current.Server.MapPath(templatePath);
            }

            // Load html content into string builder
            templateContent.Clear();
            templateContent.Append(File.OpenText(templatePath).ReadToEnd());
        }

        public RentalTemplate(string templateFilePath)
        {
            // Load html content into string builder
            templateContent.Clear();
            templateContent.Append(File.OpenText(templateFilePath).ReadToEnd());
        }

        public string GetRentalTemplateContent(string username, VideoRentalType[] rentalItems)
        {
            const string rentalTableToken = "{RentalTable}";

            // replace user's full name in the template
            templateContent.Replace(usernameToken, username);

            RentalTemplateTable table = new RentalTemplateTable();

            foreach (VideoRentalType rental in rentalItems)
            {
                RentalTemplateRow row = new RentalTemplateRow();
                row.Cost = string.Format("{0} {1:#,##0.00}", "R", rental.Amount);
                row.RentalDate = rental.DateOfPurchase;
                row.RentalName = rental.RentalName;
                row.TranactionReference = rental.TransactionReference;

                table.Add(row);
            }

            return templateContent.Replace(rentalTableToken, table.ToString()).ToString();
        }
    }

    internal class RentalTemplateRow
    {
        private const string tdValue = "{Value}";
        private const string trValue = "{RowData}";

        public string RentalName { get; set; }
        public string TranactionReference { get; set; }
        public DateTime RentalDate { get; set; }
        public string Cost { get; set; }

        public override string ToString()
        {
            string rentalTitle = EmailTemplate.Default.RentalNameTd.Replace(tdValue, RentalName);
            string rentalCost = EmailTemplate.Default.CostTd.Replace(tdValue, Cost);
            string rentalReference = EmailTemplate.Default.TrxnRefTd.Replace(tdValue, TranactionReference);
            string rentalDate = EmailTemplate.Default.TrxnRefTd.Replace(tdValue, RentalDate.ToString("yyyy/MM/dd HH:mm"));

            // Generating table markup, the order of the variables is important
            return EmailTemplate.Default.Row.Replace(trValue, rentalTitle + rentalDate + rentalReference + rentalCost);
        }
    }

    internal class RentalTemplateTable
    {
        private const string trStart = "{Rows}";

        private List<RentalTemplateRow> rows = new List<RentalTemplateRow>();

        public void Add(RentalTemplateRow row)
        {
            rows.Add(row);
        }

        public void Clear()
        {
            rows.Clear();
        }

        public string GetMarkup()
        {
            return ToString();
        }

        public override string ToString()
        {
            StringBuilder rowMarkup = new StringBuilder();

            foreach (RentalTemplateRow rentalTemplateRow in rows)
            {
                rowMarkup.Append(rentalTemplateRow);
            }

            return EmailTemplate.Default.Table.Replace(trStart, rowMarkup.ToString());
        }
    }
}
