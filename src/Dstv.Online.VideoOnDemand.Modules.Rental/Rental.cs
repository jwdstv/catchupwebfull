﻿using System;
using System.Collections.Generic;
using System.Transactions;
using System.Linq;
using Dstv.Online.VideoOnDemand.Common;
using Dstv.Online.VideoOnDemand.Common.Configuration;
using Dstv.Online.VideoOnDemand.Common.Exceptions;
using Dstv.Online.VideoOnDemand.Common.Smtp.Template;
using Dstv.Online.VideoOnDemand.Data;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;

namespace Dstv.Online.VideoOnDemand.Modules.Rental
{
	public partial class Rental
	{
		/// <summary>
		/// Gets the video rentals for the given customer identifier
		/// </summary>
		/// <param name="userId">Customer identifier</param>
		/// <returns>List of VideoRentalType (not paged)</returns>
		public RentalResult GetVideoRentalsByUserId ( string userId )
		{
			PagingCriteriaType pagingCriteria = new PagingCriteriaType { CurrentPage = 0, ItemsPerPage = 0 };

			return GetVideoRentalsByUserId(userId, pagingCriteria);
		}

		/// <summary>
		/// Gets the video rentals for the given customer identifier
		/// </summary>
		/// <param name="userId">Customer identifier</param>
		/// <param name="itemsPerPage">The number of items to return per page</param>
		/// <param name="currentPage">The zero-based page index</param>
		/// <returns>List of VideoRentalType (paged at index provided)</returns>
		public RentalResult GetVideoRentalsByUserId ( string userId, int itemsPerPage, int currentPage )
		{
			PagingCriteriaType pagingCriteria = new PagingCriteriaType { CurrentPage = currentPage, ItemsPerPage = itemsPerPage };

			return GetVideoRentalsByUserId(userId, pagingCriteria);
		}


		/// <summary>
		/// Gets the video rentals for the given customer identifier
		/// </summary>
		/// <param name="userId">Customer identifier</param>
		/// <param name="pagingCriteria">The paging criteria indicating paging requirements</param>
		/// <returns>List of VideoRentalType (paged at index provided)</returns>
		public RentalResult GetVideoRentalsByUserId ( string userId, PagingCriteriaType pagingCriteria )
		{
			List<VideoRentalType> videoRentals = new List<VideoRentalType>();
			int totalResults = 0;
			foreach (OrderDTO order in RentalDb.GetOrdersByUserId(userId, pagingCriteria.ItemsPerPage, pagingCriteria.CurrentPage, out totalResults))
			{
				videoRentals.AddRange(RentalDb.GetOrderDetailById(order.Id).Select(orderDetail => ParseToVideoRental(order, orderDetail)));
			}

			if (videoRentals != null && videoRentals.Count > 0)
			{
				SetVideoRentalsExpiry(videoRentals);
			}

			return new RentalResult(videoRentals, totalResults, pagingCriteria.CurrentPage);
		}


		private void SetVideoRentalsExpiry ( List<VideoRentalType> videoRentals )
		{
			// Check which rental videos have expired (studio) i.e. no longer in MMS.
			CatalogueCriteria catalogueCriteria = new CatalogueCriteria();
			catalogueCriteria.ClassificationCriteria = new ClassificationCriteria();
			catalogueCriteria.ClassificationCriteria.ClassificationType = CatalogueItemClassificationTypes.ProductType;
			catalogueCriteria.Paging.ItemsPerPage = videoRentals.Count;
			catalogueCriteria.Paging.PageNumber = 1;
			CatalogueItemIdentifier[] catalogueItemIds = new CatalogueItemIdentifier[videoRentals.Count];
			for (int i = 0; i < videoRentals.Count; i++)
			{
				catalogueItemIds[i] = new CatalogueItemIdentifier(videoRentals[i].VideoId);
			}
			catalogueCriteria.CatalogueItemIds = catalogueItemIds;
			
			CatalogueResult catalogueResult = VideoCatalogue.VideoCatalogue.GetVideoItems(catalogueCriteria);

			List<ICatalogueItem> catalogueItems = new List<ICatalogueItem>();
			if (catalogueResult != null && catalogueResult.CatalogueItems != null && catalogueResult.CatalogueItems.Length > 0)
			{
				catalogueItems = catalogueResult.CatalogueItems.ToList<ICatalogueItem>();
			}
			
			foreach (VideoRentalType videoRental in videoRentals)
			{
				if (catalogueItems != null)
				{
					ICatalogueItem catalogueItem = catalogueItems.Find(delegate( ICatalogueItem item ) { return item.CatalogueItemId.Value.Equals(videoRental.VideoId, StringComparison.OrdinalIgnoreCase); });
					videoRental.HasVideoExpired = catalogueItem == null || catalogueItem.ExpiryDate < DateTime.Now;
				}
				else
				{
					videoRental.HasVideoExpired = true;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="videoItem"></param>
		/// <returns></returns>
		public ResultType AddVideoRentalByUser ( RentalUser user, CatalogueItemIdentifier videoItemresultRef )
		{
            return AddVideoRentalByUser(user, videoItemresultRef, null);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="user"></param>
		/// <param name="videoItem"></param>
        /// <param name="resultRef"></param>
		/// <returns></returns>
        public ResultType AddVideoRentalByUser(RentalUser user, CatalogueItemIdentifier videoItem, string resultRef)
		{
			VideoCatalogue.VideoDetailResult videoDetail = VideoCatalogue.VideoCatalogue.GetVideoDetail(videoItem);

			VideoRentalType videoRental = new VideoRentalType();
			if (videoDetail.CatalogueItem != null)
			{
				videoRental.Amount = videoDetail.CatalogueItem.RentalAmount.Value;
				videoRental.DateOfPurchase = DateTime.Now;
				videoRental.RentalName = videoDetail.CatalogueItem.VideoTitle; // is it video title?
				videoRental.RentalDescription = videoDetail.CatalogueItem.Abstract;
				videoRental.VideoId = videoDetail.CatalogueItem.CatalogueItemId.Value;
				videoRental.VideoRentalPeriodInHours = videoDetail.CatalogueItem.RentalPeriodInHours;
			}
            return AddVideoRentalByUser(user, videoRental, resultRef);
		}


		/// <summary>
		/// Adds a video rental and does wallet transaction
		/// </summary>
		/// <param name="user">The RentalUser renting the video </param>
		/// <param name="videoRental">The VideoRental that is being rented</param>
		/// <returns>ResultType indicating success / (failure + reason) </returns>
		public ResultType AddVideoRentalByUser ( RentalUser user, VideoRentalType videoRental)
		{
            return AddVideoRentalByUser(user, videoRental, null);
		}


		/// <summary>
		/// Adds a video rental and does wallet transaction
		/// </summary>
		/// <param name="user">The RentalUser renting the video </param>
		/// <param name="videoRental">The VideoRental that is being rented</param>
        /// <param name="resultRef">Reference to completed IBS transaction</param>
		/// <returns>ResultType indicating success / (failure + reason) </returns>
		public ResultType AddVideoRentalByUser ( RentalUser user, VideoRentalType videoRental, string resultRef )
		{
			// Lets assume the worst
			ResultType result = new ResultType { Success = false, ResultInfo = "Unknown error." };

			try
			{
				using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    OrderDTO order;
                    OrderDetailDTO orderDetail;

                    // Get values into db DTO
                    ParseToDto(user, videoRental, out order, out orderDetail);

                    // Add the Order to the db
                    RentalDb.AddOrder(order);
                    orderDetail.OrderId = order.Id;  // this value is updated with the AddOrder method
                    RentalDb.AddOrderDetail(orderDetail);

                    PurchaseDetails purchaseDetails = new PurchaseDetails(
                        new CustomerIdentifier(user.UserId),
                        new CatalogueItemIdentifier(videoRental.VideoId), DateTime.Now,
                        new CurrencyAmount(new CurrencyIdentifier("R"), videoRental.Amount),
                        order.Id.ToString(),
                        videoRental.RentalName
                        );


                    if (String.IsNullOrWhiteSpace(resultRef))
                    {
                        if (!String.IsNullOrWhiteSpace(user.UserPin))
                        {
                            TransactionResult trxnResult = IntegrationBroker.Instance.DstvWallet.Purchase(purchaseDetails, new CustomerPin(user.UserPin));

                            // Check success of integration call
                            if (trxnResult.Result == OperationResults.Success)
                            {
                                orderDetail.TransactionReference = trxnResult.ResultReference.Value;
                            }
                            else
                            {
                                throw new IntegrationException(trxnResult.Details);
                            }
                        }
                    }
                    else
                    {
                        orderDetail.TransactionReference = resultRef;
                    }


                    if (!String.IsNullOrWhiteSpace(orderDetail.TransactionReference))
                    {
                        orderDetail.ActivationDate = DateTime.Now;
                        orderDetail.ExpiryDate = DateTime.Now.AddHours(videoRental.VideoRentalPeriodInHours);

                        RentalDb.AddOrderDetailTransactionReference(orderDetail);
                        videoRental.TransactionReference = orderDetail.TransactionReference; // update trxn ref on video rental

                        scope.Complete();  // commit

                        result.Success = true;
                        result.ResultInfo = string.Empty;
                    }
                }

				// Send email to client if successful. 
				// Do outside scope of transaction as any error logging will fail as Tx is commited for rental.
				if (result.Success)
				{
					try
					{
						RentalTemplate emailTemplate = new RentalTemplate();
						string emailContent = emailTemplate.GetRentalTemplateContent(user.UserFullName, new[] { videoRental });

						Common.Smtp.Emailer.SendHtmlEmail(user.UserEmail, "On Demand Rental", emailContent);
					}
					catch (Exception exception)
					{
						// Not rolling back transaction so still successful.
						// TODO: Log the exception with user data.

                        Logger log = new Logger();
                        log.LogException(Config.EventLogSource.Unknown, exception);
					    result.ResultException = exception;
					}
				}
			}
			catch (Exception ex)
			{
				// TODO: Log the exception with user data.
                Logger log = new Logger();
                log.LogException(Config.EventLogSource.Unknown, ex);

				result.Success = false;
				result.ResultInfo = ex.Message;
			    result.ResultException = ex;
			}

			return result;
		}


		internal void ParseToDto ( RentalUser user, VideoRentalType videoRental, out OrderDTO order, out OrderDetailDTO orderDetail )
		{  // this method assumes a 1:1 relationship |.| order and order details
			order = new OrderDTO();
			orderDetail = new OrderDetailDTO();

			order.Total = videoRental.Amount;
			order.UserEmail = user.UserEmail;
			order.UserFullName = user.UserFullName;
			order.UserId = user.UserId;
			order.DateOfPurchase = videoRental.DateOfPurchase;

			orderDetail.ActivationDate = videoRental.ActivationDate == DateTime.MinValue ? new DateTime(1900, 01, 01) : videoRental.ActivationDate;
			orderDetail.Amount = videoRental.Amount;
			orderDetail.ExpiryDate = videoRental.ExpiryDate == DateTime.MinValue ? new DateTime(1900, 01, 01) : videoRental.ExpiryDate;
			orderDetail.SkuDescription = videoRental.RentalTypeDescription;
			orderDetail.SkuId = videoRental.VideoId;
			orderDetail.SkuName = videoRental.RentalName;
			orderDetail.SkuType = videoRental.RentalTypeDescription;
		}


		internal VideoRentalType ParseToVideoRental ( OrderDTO order, OrderDetailDTO orderDetail )
		{
			VideoRentalType vrt = new VideoRentalType();
			vrt.ActivationDate = orderDetail.ActivationDate;
			vrt.Amount = orderDetail.Amount;
			vrt.DateOfPurchase = order.DateOfPurchase;
			vrt.ExpiryDate = orderDetail.ExpiryDate;
			vrt.OrderDetailsId = orderDetail.Id;
			vrt.RentalDescription = orderDetail.SkuDescription;
			vrt.RentalName = orderDetail.SkuName;
			vrt.TransactionReference = orderDetail.TransactionReference;
			vrt.VideoId = orderDetail.SkuId;


			return vrt;
		}

        /// <summary>
        /// Gets the video rental item for the given customer identifier and item (catalogue) identifier.
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// <param name="catalogueItemId">Item (catalogue) identifier</param>
        /// <returns>List of VideoRentalType</returns>
        public RentalResult GetVideoRentalItemByUserIdAndItemId(string userId, string catalogueItemId)
        {
            List<VideoRentalType> videoRentals = new List<VideoRentalType>();

            OrderDetailDTO orderDetail = RentalDb.GetOrderDetailByUserIdAndSkuId(userId, catalogueItemId);
            if (orderDetail != null)
            {
                VideoRentalType vrt = new VideoRentalType();
                vrt.ActivationDate = orderDetail.ActivationDate;
                vrt.Amount = orderDetail.Amount;
                vrt.ExpiryDate = orderDetail.ExpiryDate;
                vrt.OrderDetailsId = orderDetail.Id;
                vrt.RentalDescription = orderDetail.SkuDescription;
                vrt.RentalName = orderDetail.SkuName;
                vrt.TransactionReference = orderDetail.TransactionReference;
                videoRentals.Add(vrt);
                return new RentalResult(videoRentals, 1, 0);
            }
            else
            {
                return new RentalResult(videoRentals, 0, 0);
            }
        }
	}
}
