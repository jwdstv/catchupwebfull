﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

#endregion

namespace Dstv.Online.Contracts
{
    [ServiceContract]
    public interface IInvoiceService
    {
        [OperationContract]
        ServiceResult Generate
            (
                uint customerId,
                uint accountNumber,
                List<string> transactionCodes,
                DateTime startDate,
                DateTime endDate,
                out byte[] documentBytes
            );
    }
}
