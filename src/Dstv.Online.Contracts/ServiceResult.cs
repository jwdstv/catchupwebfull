﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.Contracts
{
    /// <summary>
    /// Represents a Service result
    /// </summary>
    [Serializable]
    public class ServiceResult
    {
        /// <summary>
        /// Successfull
        /// </summary>
        public bool Success;

        /// <summary>
        /// Error Message
        /// </summary>
        public string ErrorMsg;

        /// <summary>
        /// Creates a blank ServiceResult instance
        /// </summary>
        /// <returns></returns>
        public static ServiceResult CreateBlank()
        {
            ServiceResult res = new ServiceResult();

            res.Success = true;
            res.ErrorMsg = String.Empty;

            return res;
        }
    }
}
