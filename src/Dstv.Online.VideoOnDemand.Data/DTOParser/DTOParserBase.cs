﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;
using Dstv.Online.VideoOnDemand.Common.Exceptions;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class DTOParserBase : DALBase
    {
        protected DTOParserBase()
        {
        }

        protected static SqlConnection GetDbConnection()
        {
            return new SqlConnection(Rental.Default.ConnectionString);
        }

        protected static SqlCommand GetDbSprocCommand(SqlConnection sqlConnection, string sqlStoredProcedure)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = sqlConnection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = sqlStoredProcedure;
            return command;
        }

        protected static SqlConnection GetDbConnection(string sqlConnectionString)
        {
            return new SqlConnection(sqlConnectionString);
        }

        protected static SqlCommand GetDbSprocCommand(string sqlStoredProcedure)
        {
            return GetDbSprocCommand(GetDbConnection(), sqlStoredProcedure);
        }


        public static List<T> GetDTOList<T>(ref SqlCommand command) where T : DTOBase
        {
            List<T> dtoList = new List<T>();

            try
            {
                command.Connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        DTOParser parser = DTOParserFactory.GetParser(typeof(T));

                        while (reader.Read())
                        {
                            T dto = null;
                            dto = (T)parser.PopulateDTO(reader);
                            dtoList.Add(dto);
                        }

                        reader.Close();
                    }
                    else
                    {
                        dtoList = null;  // null when no data
                    }
                }
            }
            catch (Exception ex)
            {
                //Login causes "OrderGetAllByUserId" sp to cause exception, to be fixed by 315
               throw new RentalDataException("Error fetching SQL data in DAL.", ex);
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
            }

            return dtoList;
        }


        public static T GetSingleDTO<T>(ref SqlCommand command) where T : DTOBase
        {
            T dto = null;

            try
            {
                command.Connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        DTOParser parser = DTOParserFactory.GetParser(typeof(T));
                        dto = (T)parser.PopulateDTO(reader);
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RentalDataException("Error fetching SQL data in DAL.", ex);
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
            }

            return dto;
        }
    }
}
