﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    internal static class DTOParserFactory
    {
        internal static DTOParser GetParser(System.Type DTOType)
        {
            switch (DTOType.Name)
            {
                case "OrderDTO":
                    return new Order_DTOParser();

                case "OrderDetailDTO":
                    return new OrderDetail_DTOParser();

                case "BillboardDTO":
                    return new Billboard_DTOParser();

                case "AdvertDTO":
                    return new Advert_DTOParser();
                case "ErrorDTO":
                    return new Error_DTOParser();

                default:
                    throw new ApplicationException("Unknown type");
            }
        }
    }
}
