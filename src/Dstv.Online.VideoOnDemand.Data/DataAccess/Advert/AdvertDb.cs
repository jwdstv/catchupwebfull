﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class AdvertDb : DTOParserBase
    {
        public static List<AdvertDTO> GetAdTagByPageNameAndSize(string pageURL, string advertSize)
        {

            SqlCommand command = GetDbSprocCommand(new SqlConnection(DbConnections.Default.Ondemand2010ContectionString), "Site_OnDemandAdvert_getPageAndSize");

            command.Parameters.Add(CreateInputParameter("@PageUrl", pageURL));
            command.Parameters.Add(CreateInputParameter("@AdvertSize", advertSize));

            List<AdvertDTO> advertDetails = GetDTOList<AdvertDTO>(ref command);

            if (advertDetails != null)
                return advertDetails;

            return new List<AdvertDTO>();
        }
    }
}
