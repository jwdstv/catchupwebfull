﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;
using System.Data.SqlClient;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class Advert_DTOParser: DTOParser
    {

        public override DTOBase PopulateDTO(SqlDataReader reader)
        {
            AdvertDTO advert = new AdvertDTO();

            advert.ID = reader.GetInt32(reader.GetOrdinal("ID"));
            advert.Title = reader.GetString(reader.GetOrdinal("Title"));
            advert.Tag = reader.GetString(reader.GetOrdinal("Tag"));
            advert.DefaultImageURL = reader.GetString(reader.GetOrdinal("DefaultImageURL"));
            advert.LinkURL = reader.GetString(reader.GetOrdinal("LinkURL"));

            return advert;
        }
    }
}
