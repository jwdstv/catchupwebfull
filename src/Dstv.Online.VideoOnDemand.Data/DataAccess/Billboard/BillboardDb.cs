﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{    
    public class BillboardDb : DTOParserBase
    {
        public static List<BillboardDTO> GetBillBoardByProductId(int productId)
        {
            SqlCommand command = GetDbSprocCommand(new SqlConnection(DbConnections.Default.Ondemand2010ContectionString), "Site_OnDemandBillBoard_getByProductID");

            command.Parameters.Add(CreateInputParameter("@ProductID", productId));

            List<BillboardDTO> billboardDetails = GetDTOList<BillboardDTO>(ref command);

            if (billboardDetails != null)
                return billboardDetails;

            return new List<BillboardDTO>();
        }

    }
}
