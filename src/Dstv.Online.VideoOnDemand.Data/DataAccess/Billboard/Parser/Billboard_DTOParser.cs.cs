﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;
using System.Data.SqlClient;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class Billboard_DTOParser : DTOParser
    {

        public override DTOBase PopulateDTO(SqlDataReader reader)
        {
            BillboardDTO billboard = new BillboardDTO();

            billboard.ID = reader.GetInt32(reader.GetOrdinal("ID"));
            billboard.BillBoarditemTitle = reader.GetString(reader.GetOrdinal("BillBoarditemTitle"));
            billboard.Blurb = reader.GetString(reader.GetOrdinal("Blurb"));
            billboard.ImageUrl = reader.GetString(reader.GetOrdinal("ImageUrl"));
            billboard.DisplayDate = reader.GetDateTime(reader.GetOrdinal("DisplayDate"));
            billboard.StartDate = reader.GetDateTime(reader.GetOrdinal("StartDate"));
            billboard.EndDate = reader.GetDateTime(reader.GetOrdinal("EndDate"));
            billboard.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
            billboard.CategoryTitle = reader.GetString(reader.GetOrdinal("CategoryTitle"));
            billboard.ProductTitle = reader.GetString(reader.GetOrdinal("ProductTitle"));
            billboard.LinkUrl = reader.GetString(reader.GetOrdinal("LinkUrl"));
            billboard.ChannelNumber = reader.GetString(reader.GetOrdinal("ChannelNumber"));
            billboard.ChannelImageUrl = reader.GetString(reader.GetOrdinal("ChannelImageUrl"));
            billboard.SponsorImageUrl = reader.GetString(reader.GetOrdinal("SponsorImageUrl"));

            return billboard;
        }
    }
}
