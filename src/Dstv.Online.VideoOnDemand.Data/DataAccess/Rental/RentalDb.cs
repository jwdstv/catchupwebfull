﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class RentalDb : DTOParserBase
    {
        public static OrderDTO GetOrderById(Guid orderId)
        {
            SqlCommand command = GetDbSprocCommand("OrderGetById");
            command.Parameters.Add(CreateInputParameter("@OrderId", orderId));
            return GetSingleDTO<OrderDTO>(ref command);
        }

        public static List<OrderDetailDTO> GetOrderDetailById(Guid orderId)
        {
            SqlCommand command = GetDbSprocCommand("OrderDetailsGetByOrderId");

            command.Parameters.Add(
                    CreateInputParameter("@OrderId", orderId)
                );

            List<OrderDetailDTO> orderDetails = GetDTOList<OrderDetailDTO>(ref command);

            if (orderDetails != null)
                return orderDetails;

            return new List<OrderDetailDTO>();
        }

		  public static OrderDetailDTO GetOrderDetailByUserIdAndSkuId ( string userId, string skuId )
		  {
			  SqlCommand command = GetDbSprocCommand("OrderDetailsGetByUserIdAndSkuId");
			  command.Parameters.Add(CreateInputParameter("@UserId", userId));
			  command.Parameters.Add(CreateInputParameter("@SkuId", skuId));
			  return GetSingleDTO<OrderDetailDTO>(ref command);
		  }


          public static List<OrderDetailDTO> GetOrderDetailByUserIdAndSkuType(string userId, string skuType)
          {
              SqlCommand command = GetDbSprocCommand("OrderDetailsGetByUserIdAndSkuType");
              command.Parameters.Add(CreateInputParameter("@UserId", userId));
              command.Parameters.Add(CreateInputParameter("@SkuType", skuType));
              return GetDTOList<OrderDetailDTO>(ref command);
          }

        public static List<OrderDTO> GetOrdersByUserId(string userId)
        {
            SqlCommand command = GetDbSprocCommand("OrderGetAllByUserId");

            command.Parameters.Add(
                    CreateInputParameter("@UserId", userId)
                );

            List<OrderDTO> orders = GetDTOList<OrderDTO>(ref command);

            if (orders != null)
                return orders;

            return new List<OrderDTO>();
        }

        public static List<OrderDTO> GetOrdersByUserId(string userId, int numberOfItemsPerPage, int currentPage, out int totalResults)
        {
            SqlCommand command = GetDbSprocCommand("OrderGetAllByUserId");

            command.Parameters.Add(CreateInputParameter("@UserId", userId));
            command.Parameters.Add(CreateInputParameter("@PageSize", numberOfItemsPerPage));
            command.Parameters.Add(CreateInputParameter("@PageNumber", currentPage));
            SqlParameter resultCount = CreateOutputParameter("@ResultCount", SqlDbType.Int);
            command.Parameters.Add(resultCount);


            List<OrderDTO> orders = GetDTOList<OrderDTO>(ref command);
            if (resultCount.Value != null)
            {
                totalResults = (int)resultCount.Value;
            }
            else
            {
                totalResults = 0;
            }

            if (orders != null)
                return orders;

            return new List<OrderDTO>();
        }

       public static void AddOrderDetail(OrderDetailDTO orderDetail)
        {
            orderDetail.Id = Guid.NewGuid();

            SqlCommand command = GetDbSprocCommand("OrderDetailInsert");
            command.Parameters.Add(CreateInputParameter("@Id", orderDetail.Id));
            command.Parameters.Add(CreateInputParameter("@OrderId", orderDetail.OrderId));
            command.Parameters.Add(CreateInputParameter("@SkuId", orderDetail.SkuId));
            command.Parameters.Add(CreateInputParameter("@SkuName", orderDetail.SkuName));
            command.Parameters.Add(CreateInputParameter("@SkuDescription", orderDetail.SkuDescription));
            command.Parameters.Add(CreateInputParameter("@SkuType", orderDetail.SkuType));
            command.Parameters.Add(CreateInputParameter("@Amount", orderDetail.Amount));
            command.Parameters.Add(CreateInputParameter("@TransactionReference", orderDetail.TransactionReference));
            command.Parameters.Add(CreateInputParameter("@ActivationDate", orderDetail.ActivationDate));
            command.Parameters.Add(CreateInputParameter("@ExpiryDate", orderDetail.ExpiryDate));
            RunCommand(ref command);
        }


        public static void AddOrderDetailTransactionReference(OrderDetailDTO orderDetail)
        {
            SqlCommand command = GetDbSprocCommand("OrderDetailTransactionRefUpdate");
            command.Parameters.Add(CreateInputParameter("@Id", orderDetail.Id));
            command.Parameters.Add(CreateInputParameter("@TransactionReference", orderDetail.TransactionReference));
            command.Parameters.Add(CreateInputParameter("@ActivationDate", orderDetail.ActivationDate));
            command.Parameters.Add(CreateInputParameter("@ExpiryDate", orderDetail.ExpiryDate));
            RunCommand(ref command);
        }

        public static void AddOrder(OrderDTO order)
        {
            order.Id = Guid.NewGuid(); // unique identifier for new record

            SqlCommand command = GetDbSprocCommand("OrderInsert");
            command.Parameters.Add(CreateInputParameter("@Id", order.Id));
            command.Parameters.Add(CreateInputParameter("@UserId", order.UserId));
            command.Parameters.Add(CreateInputParameter("@UserFullName", order.UserFullName));
            command.Parameters.Add(CreateInputParameter("@UserEmail", order.UserEmail));
            command.Parameters.Add(CreateInputParameter("@DateOfPurchase", order.DateOfPurchase));
            command.Parameters.Add(CreateInputParameter("@Total", order.Total));
            RunCommand(ref command);
        }
    }
}
