﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class Order_DTOParser : DTOParser
    {
        public override DTOBase PopulateDTO(SqlDataReader reader)
        {
            OrderDTO order = new OrderDTO();

            order.Id = reader.GetGuid(reader.GetOrdinal("Id"));
            order.UserId = reader.GetString(reader.GetOrdinal("UserId"));
            order.UserFullName = reader.GetString(reader.GetOrdinal("UserFullName"));
            order.DateOfPurchase = reader.GetDateTime(reader.GetOrdinal("DateOfPurchase"));
            order.Total = reader.GetDecimal(reader.GetOrdinal("Total"));

            return order;
        }
    }
}
