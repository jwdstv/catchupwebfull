﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class OrderDetail_DTOParser : DTOParser
    {
        public override DTOBase PopulateDTO(SqlDataReader reader)
        {
            OrderDetailDTO orderDetail = new OrderDetailDTO();

            orderDetail.Id = reader.GetGuid(reader.GetOrdinal("Id"));
            orderDetail.OrderId = reader.GetGuid(reader.GetOrdinal("OrderId"));
            orderDetail.SkuId = reader.GetString(reader.GetOrdinal("SkuId"));
            orderDetail.SkuName = reader.GetString(reader.GetOrdinal("SkuName"));
            orderDetail.SkuType = reader.GetString(reader.GetOrdinal("SkuType"));
            orderDetail.SkuDescription = reader.GetString(reader.GetOrdinal("SkuDescription"));
            orderDetail.Amount = reader.GetDecimal(reader.GetOrdinal("Amount"));
            orderDetail.TransactionReference = reader.GetString(reader.GetOrdinal("TransactionReference"));
            orderDetail.ActivationDate = reader.GetDateTime(reader.GetOrdinal("ActivationDate"));
            orderDetail.ExpiryDate = reader.GetDateTime(reader.GetOrdinal("ExpiryDate"));

            return orderDetail;
        }
    }
}
