﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;
using System.Data.SqlClient;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class Error_DTOParser : DTOParser
    {
        public override DTOBase PopulateDTO(SqlDataReader reader)
        {
            ErrorDTO errorInfo = new ErrorDTO();

            errorInfo.errorId = reader.GetInt32(reader.GetOrdinal("ID"));
            errorInfo.location = reader.GetString(reader.GetOrdinal("location"));
            errorInfo.errorDetail = reader.GetString(reader.GetOrdinal("errorDetail"));
            errorInfo.resolved = reader.GetBoolean(reader.GetOrdinal("resolved"));
            errorInfo.createdDate = reader.GetDateTime(reader.GetOrdinal("createdDate"));

            return errorInfo;
        }

    }
}
