﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    /// <summary>
    /// this will be used to log errors to the configured SQL database
    /// </summary>
    public class ErrorDb : DTOParserBase
    {
        public static void AddErrorToDB(ErrorDTO errorDetail)
        {            
            SqlCommand command = GetDbSprocCommand(new SqlConnection(DbConnections.Default.Ondemand2010ContectionString), "site_ErrorLog_Insert");
            command.Parameters.Add(CreateInputParameter("@Location", errorDetail.location));
            command.Parameters.Add(CreateInputParameter("@ErrorDetails", errorDetail.errorDetail));
            GetDTOList<ErrorDTO>(ref command);
        }
    }
}
