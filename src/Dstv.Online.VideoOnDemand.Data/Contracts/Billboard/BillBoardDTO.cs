﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class BillboardDTO : DTOBase, IBillboard
    {

        #region IBillboard Members

        public int ID
        {
            get;
            set;
        }

        public string BillBoarditemTitle
        {
            get;
            set;
        }

        public string Blurb
        {
            get;
            set;
        }

        public string ImageUrl
        {
            get;
            set;
        }

        public DateTime DisplayDate
        {
            get;
            set;
        }

        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }

        public DateTime CreatedDate
        {
            get;
            set;
        }

        public string CategoryTitle
        {
            get;
            set;
        }

        public string ProductTitle
        {
            get;
            set;
        }

        public string LinkUrl
        {
            get;
            set;
        }

        public string ChannelNumber
        {
            get;
            set;
        }

        public string ChannelImageUrl
        {
            get;
            set;
        }

        public string SponsorImageUrl
        {
            get;
            set;
        }

        #endregion
    }
}
