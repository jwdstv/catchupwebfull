﻿using System;
namespace Dstv.Online.VideoOnDemand.Data
{
    public interface IBillboard
    {   
        
        int ID { get; set; }
		string BillBoarditemTitle { get; set; }
		string Blurb { get; set; }
		string ImageUrl { get; set; }
        DateTime DisplayDate { get; set; }
		DateTime StartDate { get; set; }
		DateTime EndDate { get; set; }		
        DateTime CreatedDate { get; set; }            
        string CategoryTitle { get; set; }          
        string ProductTitle { get; set; }
        string LinkUrl { get; set; }
        string ChannelNumber { get; set; }
        string ChannelImageUrl { get; set; }
        string SponsorImageUrl { get; set; }
        
    }
}
