﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Data
{
    public interface IError
    {
        /// <summary>
        ///  sets the error id. unique idenetifier of the error
        /// </summary>
        /// <value>The error id.</value>
        int errorId { get; set; }
        /// <summary>
        /// Gets or sets the location. this is where the error orgiginated from
        /// </summary>
        /// <value>The location.</value>
        string location { get; set; }
        /// <summary>
        /// Gets or sets the error detail. this is the details about the error. Usually an exception message
        /// </summary>
        /// <value>The error detail.</value>
        string errorDetail { get; set; }
        /// <summary>
        /// Gets a value indicating whether the error has been resolved.
        /// </summary>
        /// <value><c>true</c> if resolved; otherwise, <c>false</c>.</value>
        bool resolved { get; set; }
        /// <summary>
        /// Gets the created date. this is the date the error was logged
        /// </summary>
        /// <value>The created date.</value>
        DateTime createdDate { get; set; }
    }
}
