﻿using System;
namespace Dstv.Online.VideoOnDemand.Data
{
    public interface IAdvert
    {
        // dt_OndemandAdvert.ID, 
        //dt_OndemandAdvert.Title, 
        //dt_OndemandAdvert.Tag, 
        //dt_OndemandAdvert.DefaultImageURL, 
        //dt_OndemandAdvert.LinkURL

        int ID { get; set; }
        string Title { get; set; }
        string Blurb { get; set; }
        string Tag { get; set; }
        string DefaultImageURL { get; set; }
        string LinkURL { get; set; }
    }
}
