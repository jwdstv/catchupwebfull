﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class AdvertDTO : DTOBase, IAdvert
    {

        #region IAdvert Members

        public int ID
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Blurb
        {
            get;
            set;
        }

        public string Tag
        {
            get;
            set;
        }

        public string DefaultImageURL
        {
            get;
            set;
        }

        public string LinkURL
        {
            get;
            set;
        }

        #endregion
    }
}
