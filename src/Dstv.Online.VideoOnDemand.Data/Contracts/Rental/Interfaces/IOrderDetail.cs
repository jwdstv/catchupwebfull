﻿using System;
namespace Dstv.Online.VideoOnDemand.Data
{
    public interface IOrderDetail
    {
        DateTime ActivationDate { get; set; }
        decimal Amount { get; set; }
        DateTime ExpiryDate { get; set; }
        Guid Id { get; set; }
        Guid OrderId { get; set; }
        string SkuDescription { get; set; }
        string SkuId { get; set; }
        string SkuName { get; set; }
        string SkuType { get; set; }
        string TransactionReference { get; set; }
    }
}
