﻿using System;
namespace Dstv.Online.VideoOnDemand.Data
{
    public interface IOrder
    {
        DateTime DateOfPurchase { get; set; }
        Guid Id { get; set; }
        decimal Total { get; set; }
        string UserEmail { get; set; }
        string UserFullName { get; set; }
        string UserId { get; set; }
    }
}
