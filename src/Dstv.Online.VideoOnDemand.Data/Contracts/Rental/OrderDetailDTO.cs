﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public partial class OrderDetailDTO : DTOBase, IOrderDetail
    {
        #region IOrderDetails Members

        public DateTime ActivationDate
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public DateTime ExpiryDate
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public Guid OrderId
        {
            get;
            set;
        }

        public string SkuDescription
        {
            get;
            set;
        }

        public string SkuId
        {
            get;
            set;
        }

        public string SkuType
        {
            get;
            set;
        }

        public string SkuName
        {
            get;
            set;
        }

        public string TransactionReference
        {
            get;
            set;
        }

        #endregion
    }
}
