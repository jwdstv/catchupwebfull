﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;

namespace Dstv.Online.VideoOnDemand.Data
{
    public class OrderDTO : DTOBase, IOrder
    {
        #region IOrder Members

        public DateTime DateOfPurchase
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public decimal Total
        {
            get;
            set;
        }

        public string UserEmail
        {
            get;
            set;
        }

        public string UserFullName
        {
            get;
            set;
        }

        public string UserId
        {
            get;
            set;
        }

        #endregion
    }
}
