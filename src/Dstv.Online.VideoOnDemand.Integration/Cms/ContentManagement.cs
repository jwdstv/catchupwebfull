﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Content;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using Dstv.Online.VideoOnDemand.Integration.Properties;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.MemCached;

namespace Dstv.Online.VideoOnDemand.Integration.Cms
{
    public class ContentManagement : IContentManagement
    {
        public IPlaylist GetPlaylist(PlaylistIdentifier playlistId)
        {
            Playlist playlist;
            string cacheKey = string.Format("{0}_GetPlaylist_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, playlistId).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                playlist = MemCacheManager.Get<Playlist>(cacheKey);
            }
            else
            {

                GetEditorialListRequest listRequest = new GetEditorialListRequest();
                listRequest.AuthenticationKey = authenticationKey;
                listRequest.EditorialListID = int.Parse(playlistId.Value);

                EditorialListCollection lists = service.GetEditorialList(listRequest);

                if (lists == null)
                {
                    return null;
                }
                if (lists.Count != 1)
                {
                    //throw new ApplicationException(string.Format("Unexpected number of lists returned;  expected 1, got {0}", lists.Count));
                    return null;
                }

                GetEditorialListItemsRequest itemsRequest = new GetEditorialListItemsRequest();
                itemsRequest.EditorialListTitle = lists[0].Title;

                EditorialListItemsCollection items = service.GetEditorialListItems(itemsRequest);

                playlist = new Playlist(lists[0], items.ToArray());

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, playlist);
                }
            }

            return playlist;
        }


        private static string authenticationKey
        {
            get
            {
                return Settings.Default.AuthenticationKey.ToString();
            }
        }

        private static string authKeyCacheString
        {
            get
            {
                return authenticationKey.Substring(0, CacheConstants.AuthKeyLength);
            }
        }

        private MediaContractClient _service = null;
        /// <summary>
        /// GetEditorialList
        /// GetEditorialListItems
        /// </summary>
        private MediaContractClient service
        {
            get
            {
                if (_service == null)
                {
                    _service = new MediaContractClient();
                }
                return _service;
            }
        }
    }
}
