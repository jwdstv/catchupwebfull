﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Content;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Integration.Cms
{
    [DataContract]
    [KnownType(typeof(Playlist))]
    [Serializable()]
    public class Playlist : IPlaylist
    {
        EditorialList underlyingEditorialList;
        EditorialListItem[] underlyingItems;
        public Playlist(EditorialList list, EditorialListItem[] items)
        {
            underlyingEditorialList = list;
            underlyingItems = items;
        }
        [DataMember]
        public CatalogueItemIdentifier[] CatalogueItemIds
        {
            get {
                List<CatalogueItemIdentifier> playlistIdList = new List<CatalogueItemIdentifier>();
                if (underlyingItems != null)
                {
                    foreach (EditorialListItem thisItem in underlyingItems)
                    {
                        playlistIdList.Add(new CatalogueItemIdentifier(thisItem.ItemID.ToString()));
                    }
                }
                return playlistIdList.ToArray();
            }
            private set { } 
        }
        [DataMember]
        public string Name
        {
            get { 
                return underlyingEditorialList.DisplayTitle.EvaluateNull(); 
            }
            private set { } 
        }
    }
}
