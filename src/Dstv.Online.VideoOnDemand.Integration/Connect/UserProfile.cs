﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Security;
using DStvConnect.WebUI.Platform;

namespace Dstv.Online.VideoOnDemand.Integration.Connect
{
    public class UserProfile : IUserProfile
    {

        public IProfileInformation GetCurrentUserProfile()
        {
            DStvPrincipal principal = DStvPrincipal.CurrentHttpPrincipal;
            string connectId = principal.DStvConnectID;

            //string connectId = "47F91288-43FE-42F3-8655-33832B49BD4C";
            ProfileInformation profileInfo = ProfileInformation.Open(connectId);
            return profileInfo;
        }

        public IProfileInformation GetUserProfile(string userId)
        {
            ProfileInformation profileInfo = ProfileInformation.Open(userId);
            return profileInfo;
        }
    }
}
