﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using DstvoConnectWrapper;

namespace Dstv.Online.VideoOnDemand.Integration.Connect.Obsolete
{
    [Obsolete("Entitlement should not be done through the Connect wrapper", true)]
    public class Entitlement //: IEntitlement
    {
        public TransactionResult GenerateEntitlement(CustomerIdentifier connectUserId, CatalogueItemIdentifier catalogueItemId)
        {
            List<string> items = new List<string>();
            items.Add(catalogueItemId.Value);
            string result = wrapper.GenerateIrdetoEntitlement(connectUserId.Value, 1, items);

            if (result == null)
            {
                return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Failure, null, null, null);
            }
            if (result == string.Empty)
            {
                return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Success, null, null, null);
            }
            return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Failure, result, null, null);
        }

        ConnectWrapper _wrapper = null;
        protected ConnectWrapper wrapper
        {
            get
            {
                if (_wrapper == null)
                {
                    _wrapper = new ConnectWrapper();
                }
                return _wrapper;
            }
        }
    }

}
