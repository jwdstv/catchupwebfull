﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DstvoConnectWrapper;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using ConnectRemoteImpl.Wallet;

namespace Dstv.Online.VideoOnDemand.Integration.Connect
{
    public class ConnectDstvWallet : DstvWallet
    {
        //protected override CurrencyAmount GetBalance()
        //{
        //    UserProfile userProfile = new UserProfile();
        //    IProfileInformation profile = userProfile.GetCurrentUserProfile();
        //    return profile.WalletBalance;
        //}

        protected override void performRentalTransaction(Dstv.Online.VideoOnDemand.Transactions.PurchaseDetails purchaseDetails, CustomerPin pin)
        {
            PrepaidTransactionResult result = wrapper.PerformRentalTransaction(
                purchaseDetails.CustomerId.Value, 
                pin.PinValue,
                purchaseDetails.CatalogItemId.Value, 
                purchaseDetails.Amount.Value, 
                purchaseDetails.RequestReference.Value, 
                purchaseDetails.Description);
            if (result.Result == false)
            {
                throw new ApplicationException(result.Details);
            }
            
            string foo = result.ResultReference;
            uint Fooo = result.TransactionId;
        }


        ConnectWrapper _wrapper = null;
        protected ConnectWrapper wrapper
        {
            get
            {
                if (_wrapper == null)
                {
                    _wrapper = new ConnectWrapper();
                }
                return _wrapper;
            }
        }
    }



}
