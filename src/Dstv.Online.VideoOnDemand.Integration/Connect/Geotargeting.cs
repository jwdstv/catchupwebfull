﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Globalization;
using DstvoConnectWrapper;
using ConnectRemoteImpl;
using System.Web;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.Integration.Properties;

namespace Dstv.Online.VideoOnDemand.Integration.Connect
{
    public class Geotargeting : IGeotargeting
    {

        public ITerritory ResolveUserTerritory()
        {
            return ResolveUserTerritory(ResolveIpAddress());
        }

        public static string ResolveIpAddress()
        {
            string userIpAddress = string.Empty;

            if (Settings.Default.UseTestIpAddress)
            {
                userIpAddress = Settings.Default.TestIpAddress;
            }
            else
            {
                HttpContext httpContext = HttpContext.Current;
                if (httpContext != null)
                {
                    userIpAddress = httpContext.Request.UserHostAddress;
                }
                else
                {
                    userIpAddress = "127.0.0.1";
                }
            }
            return userIpAddress;
        }
        /// <summary>
        /// Resolve user host address to territory using Connect integration
        /// </summary>
        /// <param name="userHostAddress">User Ip Address</param>
        /// <returns></returns>
        /// <exception cref="">ConnectTerritoryResolutionException</exception>
        public ITerritory ResolveUserTerritory(string userHostAddress)
        {
            ClientCountryItem country = wrapper.getCountryFromIP(userHostAddress);
            if (country == null) {
                throw new ConnectTerritoryResolutionException();
            }
            return new Territory(country);
        }

        ConnectWrapper _wrapper = null;
        protected ConnectWrapper wrapper
        {
            get
            {
                if (_wrapper == null)
                {
                    _wrapper = new ConnectWrapper();
                }
                return _wrapper;
            }
        }
    }

    [DataContract, KnownType(typeof(Territory))]
    public class Territory : ITerritory
    {
        ClientCountryItem underlyingCountryItem = null;
        public Territory(ClientCountryItem item)
        {
            underlyingCountryItem = item;
        }

        [DataMember]
        public string Name
        {
            get { return underlyingCountryItem.CountryName.EvaluateNull(); }
            set { }
        }

        [DataMember]
        public TerritoryIdentifier TerritoryId
        {
            get {
                if (string.IsNullOrEmpty(underlyingCountryItem.CountryCode))
                {
                    return null;
                }
                return new TerritoryIdentifier(
                    underlyingCountryItem.CountryCode);
            }
            set { }
        }
    }
    public class ConnectTerritoryResolutionException : Exception { }

}
