﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Globalization;
using DstvoConnectWrapper;
using ConnectRemoteImpl;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.Integration.Properties;
using System.Collections;
using ConnectRemoteImpl.Profile;

namespace Dstv.Online.VideoOnDemand.Integration.Connect
{
    [DataContract]
    public class ProfileInformation : IProfileInformation
    {


        private ProfileInformation()
        {
        }

        protected string underlyingConnectId = null;
        protected ClientUserDetails underlyingClientUserDetails = null;

        public static ProfileInformation Open(string connectId)
        {
            ProfileInformation p = new ProfileInformation();
            p.underlyingConnectId = connectId;
            p.underlyingClientUserDetails = p.wrapper.RequestUserDetails(p.underlyingConnectId);
            return p;
        }
        [DataMember]
        public CustomerIdentifier CustomerId
        {
            get
            {
                // Connect profile
                return new CustomerIdentifier(underlyingConnectId);
            }
            private set { }
        }
        [DataMember]
        public string Email
        {
            get
            {
                return underlyingClientUserDetails.clientUserRegisterItem.EMailAddress.EvaluateNull();
            }
            private set { }
        }

        [DataMember]
        public bool HasWallet
        {
            get {
                try
                {
                    return wrapper.IsWalletSubscriber(underlyingConnectId);
                }
                catch (Exception ex){
                    return false;
                }
            }
            private set { }
        }

        //public bool IsPlayerInstalled
        //{
        //    get { 
        //        // Deprecated;
        //        throw new NotImplementedException(); }
        //}

        [DataMember]
        public string UserName
        {
            get { 
                return underlyingClientUserDetails.clientUserRegisterItem.UserName.EvaluateNull();
            }
            private set { }
        }

        [DataMember]
        public CurrencyAmount WalletBalance
        {
            get
            {
                //// IBS profile
                if (!HasWallet)
                {
                    return new CurrencyAmount(new CurrencyIdentifier("ZAR"), 0);
                }
                decimal balance = wrapper.GetWalletBalance(underlyingConnectId);
                CurrencyAmount c = new CurrencyAmount(new CurrencyIdentifier("ZAR"), balance);
                return c;

                //Todo: Remove this hardcoded value after this has been fixed

                //CurrencyAmount c = new CurrencyAmount(new CurrencyIdentifier("ZAR"), (decimal)100.45);
                //return c;


            }
            private set { }
        }

        [DataMember]
        public bool IsSmartCardLinked
        {
            get
            {
                string smartCardNumber = ReadProfileEntryValue(ProfileEntryEnum.SmartcardNumber);
                if (!string.IsNullOrWhiteSpace(smartCardNumber)) {
                    return false;
                }
                return true;
            }
            private set { }
        }

        [DataMember]
        public bool IsPremiumDstvSubscriber
        {
            get
            {
                string smartCardNumber = ReadProfileEntryValue(ProfileEntryEnum.SmartcardNumber);
                if (string.IsNullOrWhiteSpace(smartCardNumber))
                {
                    return false;
                }
                try
                {
                    return wrapper.isPremiumSubscriberBySmartCardNumber(smartCardNumber);
                }
                catch
                {
                    return false;
                }
            }
            private set { }
        }


        [DataMember]
        public bool IsLinkedToISP
        {
            get {
                //string ispPartner = ReadProfileEntryValue(ProfileEntryEnum.ISPPartner);
                string ispPartner = ReadProfileEntryValue(ProfileEntryEnum.MWebPartnerID);
                if (string.IsNullOrEmpty(ispPartner))
                {
                    return false;
                }
                return true;
            }
            private set { }
        }

        ConnectWrapper  _wrapper = null;
        protected ConnectWrapper wrapper
        {
            get
            {
                if (_wrapper == null)
                {
                    _wrapper = new ConnectWrapper();
                }
                return _wrapper;
            }
        }

        private string ReadProfileEntryValue(ProfileEntryEnum profileEntry)
        {
            string profileEntryValue = null;
            ArrayList profileEntries = underlyingClientUserDetails.profileController.GetProfiles();
            ProfileEntry ispPartnerEntry = null;
            foreach (ProfileEntry thisProfileEntry in profileEntries)
            {
                if (thisProfileEntry.ProfileTypeKeyID == profileEntry.GetHashCode())
                {
                    ispPartnerEntry = thisProfileEntry;
                    break;
                }
            }
            if (ispPartnerEntry == null)
            {
                return null;
            }
            profileEntryValue = ispPartnerEntry.EntryValue;
            return profileEntryValue;
        }
    }

}
