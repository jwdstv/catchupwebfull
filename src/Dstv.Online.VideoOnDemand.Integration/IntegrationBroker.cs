﻿
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Content;
using Dstv.Online.VideoOnDemand.Player;
using Dstv.Online.VideoOnDemand.Globalization;
using Dstv.Online.VideoOnDemand.Transactions.Wallet;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Integration.Connect;
using Dstv.Online.VideoOnDemand.Integration.Mms;
using Dstv.Online.VideoOnDemand.Integration.LuceneSearch;
using Dstv.Online.VideoOnDemand.Integration.Cms;
using Dstv.Online.VideoOnDemand.Integration.Properties;
using Dstv.Online.VideoOnDemand.Integration.Irdeto;
namespace Dstv.Online.VideoOnDemand
{
    public class IntegrationBroker : IIntegrationBroker
    {
        private IntegrationBroker() { }
        private static IIntegrationBroker instance = null;
        #region IIntegrationBroker Members
        public static IIntegrationBroker Instance
        {
            get
            {
                if (instance == null)
                {
                    //if (Settings.Default.UseStubs)
                    //{
                    //    instance = new Stubs.IntegrationBroker();
                    //}
                    //else
                    //{
                        instance = new IntegrationBroker();
                    //}
                }
                return instance;
            }
        }

        private IEntitlement entitlement = null;
        public IEntitlement Entitlement
        {
            get
            {
                return entitlement ?? (entitlement = new Entitlement());
            }
        }

        private ICatalogue _mmsCatalogue = null;
        public ICatalogue Catalogue
        {
            get
            {
                if (_mmsCatalogue == null)
                {
                    _mmsCatalogue = new Catalogue();
                }
                return _mmsCatalogue;
            }
        }

        private ICatalogueSearch _catalogueSearch;
        public ICatalogueSearch CatalogueSearch
        {
            get
            {
                return _catalogueSearch ?? (_catalogueSearch = new CatalogueSearch());
            }
        }

        private IContentManagement _contentManagement;
        public IContentManagement ContentManagement
        {
            get
            {
                return _contentManagement ?? (_contentManagement = new ContentManagement());
            }
        }

        IGeotargeting _geoTargeting;
        public IGeotargeting Geotargeting
        {
            get
            {
                return _geoTargeting ?? (_geoTargeting = new Geotargeting());
            }
        }

        DstvWallet _dstvWallet;
        public DstvWallet DstvWallet
        {
            get
            {
                return _dstvWallet ?? (_dstvWallet = new ConnectDstvWallet());
            }
        }

        IUserProfile _userProfile;
        public IUserProfile UserProfile
        {
            get
            {
                return _userProfile ?? (_userProfile = new UserProfile());
            }
        }

        #endregion
    }
}
