﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using DStvConnect.WebUI.Controls;
using DStvConnect.WebUI.Platform;
using System.Web.UI;
using Dstv.Online.VideoOnDemand.Transactions;
using IrdetoAPICalls = IrdetoLibrary;
using BaseX;

namespace Dstv.Online.VideoOnDemand.Integration.Irdeto
{
    public class IrdetoAPIWrapper
    {
        public string AuthorizePackages(CustomerIdentifier connectUserId)
        {
            string errorMsg = string.Empty;

            if (SessionManager.GetSession("IsEntitled") == null
                || SessionManager.GetSession("IsEntitled").Equals(bool.FalseString, StringComparison.OrdinalIgnoreCase)//Check if you are not entitled yet.
                || HttpContext.Current.Response.Cookies["MAN"] == null) //Check if you do not have the MAN cookie yet.
            {
                errorMsg = IrdetoSessionManager(connectUserId, true);
            }

            return errorMsg;
        }

        public string DeauthorizePackages(CustomerIdentifier connectUserId)
        {
            return IrdetoSessionManager(connectUserId, false);
        }

        private string IrdetoSessionManager(CustomerIdentifier connectUserId, bool authorize)
        {
            string errorMsg = string.Empty;

            //UseTestIpAddress will only be set to true on our local servers/machines, so we don't want to set the cookies domain to dstv.com when this is true
            bool useCookieDomain = !Properties.Settings.Default.UseTestIpAddress;

            // Irdeto Session creation
            string IrdetoAccountId = ConfigurationManager.AppSettings["AccountId"];
            string IrdetoCrmId = ConfigurationManager.AppSettings["CrmId"];
            string ConnectId = connectUserId.Value;
            string Packages = ConfigurationManager.AppSettings["IrdetoPackageIds"];
            string[] packages = Packages.Split(',');

            //2418,2372,2386,2427,2373
            List<String> itemList = new List<string>(packages);

            IrdetoAPICalls.Utils.ListTypeEnum type = IrdetoAPICalls.Utils.ListTypeEnum.PackageID;
            IrdetoAPICalls.Utils.RequestCollection requestCollection = new IrdetoAPICalls.Utils.RequestCollection(type, itemList);

            #region CreateSession
            // Create an instance of the request object and assign parameters.
            IrdetoAPICalls.Irdeto.Entities.CreateSessionRequest createSessionRequest = new IrdetoAPICalls.Irdeto.Entities.CreateSessionRequest();
            createSessionRequest.UserId = ConnectId;
            createSessionRequest.CreateUser = true;
            createSessionRequest.AccountId = IrdetoAccountId;
            createSessionRequest.CrmId = IrdetoCrmId;

            // Create an instance of the response object passing the request object;
            IrdetoAPICalls.Irdeto.APICalls createSession = new IrdetoAPICalls.Irdeto.APICalls();
            IrdetoAPICalls.Irdeto.Entities.CreateSessionResponse createSessionResponse;

            createSessionResponse = createSession.CreateUserSession(createSessionRequest);

            if (createSessionResponse == null)
            {
                throw new Exception("Call to Irdeto failed or returned NULL.");
            }

            #endregion

            string IrdetoSessionId = createSessionResponse.SessionId;
            SessionManager.SetSession("IrdetoSessionId", IrdetoSessionId);
            string base36ConnectID = ConnectId.ConvertGuidToBase36();

            if (authorize)
            {

                #region CreateManCookie

                string cookieVal = "SessionId=" + createSessionResponse.SessionId;
                cookieVal += "&Ticket=" + createSessionResponse.Ticket;
                cookieVal += "&CrmId=" + IrdetoCrmId;
                cookieVal += "&UserId=" + base36ConnectID;
                cookieVal += "&AccountId=" + IrdetoAccountId;
                cookieVal += "&Token=" + createSessionResponse.Token;
                cookieVal += "&AgentHost=man";
                cookieVal += "&AgentId=man";
                HttpCookie manCookie = new HttpCookie("MAN", cookieVal);
                manCookie.Expires = DateTime.Now.AddDays(30);

                if (useCookieDomain)
                {
                    manCookie.Domain = ConfigurationManager.AppSettings["CookieDomain"];
                }
                HttpContext.Current.Response.Cookies.Add(manCookie);

                //Adding session variables for the Async call that inserts the Download link

                SessionManager.SetSession("Ticket", createSessionResponse.Ticket);
                //SessionManager.SetSession("SessionId", createSessionResponse.SessionId);


                #endregion

                #region Create DCCookie
                HttpCookie aCookie = new HttpCookie("dctoken");
                aCookie.Value = createSessionResponse.Token;
                aCookie.Expires = DateTime.Now.AddDays(30);
                if (useCookieDomain)
                {
                    aCookie.Domain = ConfigurationManager.AppSettings["CookieDomain"];
                }
                HttpContext.Current.Response.Cookies.Add(aCookie);

                HttpCookie bCookie = new HttpCookie("dctoken_time");
                bCookie.Value = DateTime.Now.ToString();
                bCookie.Expires = DateTime.Now.AddDays(30);
                if (useCookieDomain)
                {
                    bCookie.Domain = ConfigurationManager.AppSettings["CookieDomain"];
                }
                HttpContext.Current.Response.Cookies.Add(bCookie);
                #endregion


            }
            else
            {
                HttpContext.Current.Response.Cookies.Remove("MAN");
                HttpContext.Current.Response.Cookies.Remove("dctoken_time");
                HttpContext.Current.Response.Cookies.Remove("dctoken");
            }

            //QuerySession
            IrdetoAPICalls.Irdeto.APICalls querySessionAuthorization = new IrdetoAPICalls.Irdeto.APICalls();
            IrdetoAPICalls.Irdeto.Entities.QuerySessionAuthorizationRequest QuerySessionAuthorizationRequest = new IrdetoAPICalls.Irdeto.Entities.QuerySessionAuthorizationRequest();
            QuerySessionAuthorizationRequest.AccountId = IrdetoAccountId;
            QuerySessionAuthorizationRequest.SessionId = IrdetoSessionId;
            QuerySessionAuthorizationRequest.RequestQueryList = requestCollection;

            IrdetoAPICalls.Irdeto.Entities.SessionAuthorizationResponse querySessionAuthorizationResponse = querySessionAuthorization.QuerySessionAutherization(QuerySessionAuthorizationRequest);

            if (authorize)
            {
                if (querySessionAuthorizationResponse.itemTable.Count > 0)
                {
                    errorMsg = IrdetoAPICalls.Utils.IrdetoUtils.AuthorizeItems(querySessionAuthorizationResponse, ConnectId, IrdetoSessionId);
                }
            }
            else
            {
                if (querySessionAuthorizationResponse.itemTable.Count < itemList.Count)
                {
                    //This a temp fix for the IrdetoAPICalls dll
                    //Currently the dll is only adding NON authorized items to the itemTable of the querySessionAuthorizationResponse
                    List<KeyValuePair<string, string>> tempItemTable = new List<KeyValuePair<string, string>>();

                    foreach (string item in itemList)
                    {
                        if (!querySessionAuthorizationResponse.itemTable.Contains(new KeyValuePair<string,string>(item, bool.FalseString)))
                        {
                            tempItemTable.Add(new KeyValuePair<string,string>(item, bool.TrueString));
                        }
                    }
                    querySessionAuthorizationResponse.itemTable = tempItemTable;

                    errorMsg = IrdetoAPICalls.Utils.IrdetoUtils.DeAuthorizeItems(querySessionAuthorizationResponse, ConnectId, IrdetoSessionId);
                }
            }

            if (errorMsg == string.Empty)
            {
                SessionManager.SetSession("IsEntitled", authorize.ToString());
            }

            return errorMsg;
        }
                
    }

    
}