﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using DstvoConnectWrapper;
using DStvConnect.WebUI.Controls;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Integration.Irdeto
{
    public class Entitlement : IEntitlement
    {
        public TransactionResult AuthorizeCatchupPackages(CustomerIdentifier connectUserId)
        {
            IrdetoAPIWrapper wrapper = new IrdetoAPIWrapper();
            string errorMsg = wrapper.AuthorizePackages(connectUserId);

            if (errorMsg == string.Empty)
            {
                return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Success, null, null, null);
            }
            else
            {
                return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Failure, errorMsg, null, null);
            }
        }

        public TransactionResult DeauthorizeCatchupPackages(CustomerIdentifier connectUserId)
        {
            IrdetoAPIWrapper wrapper = new IrdetoAPIWrapper();
            string errorMsg = wrapper.DeauthorizePackages(connectUserId);

            if (errorMsg == string.Empty)
            {
                return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Success, null, null, null);
            }
            else
            {
                return new TransactionResult(Dstv.Online.VideoOnDemand.OperationResults.Failure, errorMsg, null, null);
            }
        }

        public TransactionResult GenerateVideoEntitlement(CustomerIdentifier connectUserId, CatalogueItemIdentifier catalogueItemId)
        {
            throw new NotImplementedException();
        }
    }

}
