﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    [Serializable]
    [DataContract]
    internal class Channel : IChannel {
        private ChannelData underlyingChannel = null;

        public Channel(ChannelData channelData) {
            underlyingChannel = channelData;
        }

        [DataMember]
        public ChannelItemIdentifier Id {
            get {
                return new ChannelItemIdentifier(underlyingChannel.ID.ToString());
            }
            private set { }
        }

        [DataMember]
        public string Name {
            get {
                return underlyingChannel.ChannelName.EvaluateNull();
            }
            private set { }
        }

        [DataMember]
        public string ImageFilename {
            get { return underlyingChannel.Logo.EvaluateNull(); }
            private set { }
        }
    }
}
