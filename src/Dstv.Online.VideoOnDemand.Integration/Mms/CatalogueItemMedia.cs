﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(CatalogueItemMedia))]
    public class CatalogueItemMedia : ICatalogueItemMedia {
        VideoFile underlyingVideoFile;
        public CatalogueItemMedia(VideoFile videoFile) {
            underlyingVideoFile = videoFile;
        }

        [DataMember]
        public string FileName {
            get { return underlyingVideoFile.FileName.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public long? FileSizeInBytes {
            get { return (long)underlyingVideoFile.SizeInKB * 1024; }
            private set { }
        }

        [DataMember]
        public string ManId {
            get { return underlyingVideoFile.ManItemId.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public MediaTypeIdentifier MediaType {
            get { return new MediaTypeIdentifier(underlyingVideoFile.VideoFileTypeID.ToString()); }
            private set { }
        }

        [DataMember]
        public string ServerPath {
            get { return underlyingVideoFile.ServerPath.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public string Dimensions
        {
            get { return underlyingVideoFile.VideoDimension.EvaluateNull(); }
            private set { }
        }

    }
}
