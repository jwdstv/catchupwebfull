﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using Dstv.Online.VideoOnDemand.Integration.Properties;
using Dstv.Online.VideoOnDemand;
using System.Web;
using Dstv.Online.VideoOnDemand.Integration.Connect;
using Dstv.Online.VideoOnDemand.MemCached;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    public class Catalogue : ICatalogue
    {

        private static string getCurrentIpAddress()
        {
            return Geotargeting.ResolveIpAddress();
        }

        /// <summary>
        /// Gets the catalogue item.
        /// </summary>
        /// <param name="catalogueItemId">The catalogue item id.</param>
        /// <param name="getRelatedMedia">if set to <c>true</c> [get related media].</param>
        /// <returns></returns>
        public CatalogueResult GetCatalogueItem(CatalogueItemIdentifier catalogueItemId, bool getRelatedMedia)
        {
            List<ICatalogueItem> items = new List<ICatalogueItem>();
            Video searchedVideo = getVideo(catalogueItemId);
            if (searchedVideo != null)
            {
                Program program = getProgram(searchedVideo.ProgramID);
                if (getRelatedMedia)
                {
                    Video[] relatedVideos = getProgramVideos(searchedVideo.ProgramID);
                    foreach (Video thisVideo in relatedVideos)
                    {
                        items.Add(new CatalogueItem(program, thisVideo));
                    }
                }
                else
                {
                    items.Add(new CatalogueItem(program, searchedVideo));
                }
            }
            CatalogueResult result = new CatalogueResult(items.Count, -1, items.ToArray());
            return result;
        }

        /// <summary>
        /// Gets the catalogue item cast and crew.
        /// </summary>
        /// <param name="catalogueItemId">The catalogue item id.</param>
        /// <returns></returns>
        public ICastMember[] GetCatalogueItemCastAndCrew(CatalogueItemIdentifier catalogueItemId)
        {
            List<CastMember> castAndCrewList;
            Video video = getVideo(catalogueItemId);

            string cacheKey = string.Format("{0}_GetCatalogueItemCastAndCrew_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, catalogueItemId.Value).CacheRemoveInvalidChars();

            if (video == null)
            {
                return new List<CastMember>().ToArray();
            }


            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                castAndCrewList = MemCacheManager.Get<List<CastMember>>(cacheKey);
            }
            else
            {
                GetPersonMetaRequest request = new GetPersonMetaRequest();

                request.AuthenticationKey = authenticationKey;
                request.ProgramID = video.ProgramID;

                PersonMetaCollection castAndCrew = service.GetPersonMeta(request);

                castAndCrewList = new List<CastMember>();
                foreach (PersonMetaData thisPerson in castAndCrew)
                {
                    castAndCrewList.Add(new CastMember(thisPerson));
                }

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, castAndCrewList);
                }
            }

            return castAndCrewList.ToArray();
        }

        /// <summary>
        /// Gets the catalogue item media.
        /// </summary>
        /// <param name="catalogueItemId">The catalogue item id.</param>
        /// <param name="mediaTypeId">The media type id.</param>
        /// <returns></returns>
        public ICatalogueItemMedia GetCatalogueItemMedia(CatalogueItemIdentifier catalogueItemId, MediaTypeIdentifier mediaTypeId)
        {
            CatalogueItemMedia file;
            string cacheKey = string.Format("{0}_GetCatalogueItemMedia_{1}_{2}_{3}", CacheConstants.CachePrefix, authKeyCacheString, catalogueItemId.Value, mediaTypeId.Value).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                file = MemCacheManager.Get<CatalogueItemMedia>(cacheKey);
            }
            else
            {
                GetVideoFileRequest request = new GetVideoFileRequest();
                request.AuthenticationKey = authenticationKey;
                request.VideoID = new int[] { int.Parse(catalogueItemId.Value) };
                request.ProgramID = new int[] { 0 };
                request.VideoFileTypeID = int.Parse(mediaTypeId.Value);
                VideoFileCollection files = service.GetVideoFile(request);

                if (files.Count > 1)
                {
                    throw new ApplicationException(string.Format("One media item expected, returned {0}", files.Count));
                }
                if (files.Count == 0)
                {
                    return null;
                }

                file = new CatalogueItemMedia(files[0]);

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, file);
                }
            }

            return file;
        }


        public CatalogueResult GetCatalogueItems(CatalogueCriteria criteria)
        {
            CatalogueResult result;
            string cacheKey = string.Format("{0}_GetCatalogueItems_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, criteria.CacheKeyString()).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                result = MemCacheManager.Get<CatalogueResult>(cacheKey);
            }
            else
            {
                GetVideoRequest videoRequest = new GetVideoRequest();
                videoRequest.AuthenticationKey = authenticationKey;

                List<int> requestedVideoList = new List<int>();
                if (criteria.CatalogueItemIds != null)
                {
                    foreach (CatalogueItemIdentifier thisCatalogueItem in criteria.CatalogueItemIds)
                    {
                        requestedVideoList.Add(int.Parse(thisCatalogueItem.Value));
                    }
                }

                if (criteria.ClassificationCriteria.ProductTypeId != null)
                {
                    videoRequest.ProductId = criteria.ClassificationCriteria.ProductTypeId.Value.Map();
                }

                if (criteria.ClassificationCriteria.ClassificationItemId != null)
                {
                    videoRequest.SubGenreID = Int32.Parse(criteria.ClassificationCriteria.ClassificationItemId.Value);
                }

                if (criteria.ClassificationCriteria.ParentClassificationId != null)
                {
                    videoRequest.GenreID = Int32.Parse(criteria.ClassificationCriteria.ParentClassificationId.Value);
                }

                // Add videoProgramTypes filter
                videoRequest.VideoProgramType = criteria.ClassificationCriteria.VideoProgramTypes.Select(t => (int)t).ToArray();

                videoRequest.PageNumber = criteria.Paging.PageNumber;

                videoRequest.PageSize = criteria.Paging.ItemsPerPage;

                videoRequest.SortBy = criteria.SortOrder.Map();

                videoRequest.VideoID = requestedVideoList.ToArray();
                System.Net.ServicePointManager.Expect100Continue = false;
                VideoCollection videos = service.GetVideo(videoRequest);

                if (videos == null)
                {
                    return null;
                }

                // Create a list of unique programs for these videos
                List<int> programsRequiredList = new List<int>();
                foreach (Video thisVideo in videos)
                {
                    if (!programsRequiredList.Contains(thisVideo.ProgramID))
                    {
                        programsRequiredList.Add(thisVideo.ProgramID);
                    }
                }

                // Fetch the programs
                GetProgramRequest programRequest = new GetProgramRequest();
                programRequest.AuthenticationKey = authenticationKey;
                programRequest.ProgramID = programsRequiredList.ToArray();
                programRequest.PageSize = programsRequiredList.Count;
                int sortByNoDateFilter = 10;
                programRequest.SortBy = sortByNoDateFilter;
                programRequest.PageNumber = 1;
                ProgramCollection programs = service.GetProgram(programRequest);

                Dictionary<int, Program> programDictionary = new Dictionary<int, Program>();
                foreach (Program thisProgram in programs)
                {
                    if (!programDictionary.ContainsKey(thisProgram.ID))
                    {
                        programDictionary.Add(thisProgram.ID, thisProgram);
                    }
                }

                List<CatalogueItem> catalogueItemList = new List<CatalogueItem>();
                foreach (Video thisVideo in videos)
                {
                    //This needs to be investigated that is why this code is not removed. - JA
                    //if (!programDictionary.ContainsKey(thisVideo.ProgramID))
                    //{
                    //    throw new ApplicationException("Related program not returned for video");
                    //}
                    //Program relatedProgram = programDictionary[thisVideo.ProgramID];
                    //if (relatedProgram == null)
                    //{
                    //    throw new ApplicationException("Related program not returned for video");
                    //}
                    //catalogueItemList.Add(new CatalogueItem(relatedProgram, thisVideo));


                    //New code
                    Program relatedProgram = null;
                    if (programDictionary.ContainsKey(thisVideo.ProgramID))
                    {
                        relatedProgram = programDictionary[thisVideo.ProgramID];
                        if (relatedProgram != null)
                        {
                            catalogueItemList.Add(new CatalogueItem(relatedProgram, thisVideo));
                        }
                    }

                }

                int totalNumberOfItemsReturned = 0;
                if (videos.Count > 0)
                {
                    totalNumberOfItemsReturned = videos[0].TotalRows;
                }

                result = new CatalogueResult(totalNumberOfItemsReturned, criteria.Paging.PageNumber, catalogueItemList.ToArray());

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, result);
                }
            }


            return result;
        }

        public IClassificationItem[] GetClassificationItems(ClassificationCriteria criteria)
        {
            List<ClassificationItem> categoryList;
            string cacheKey = string.Format("{0}_GetClassificationItems_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, criteria.CacheKeyString()).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                categoryList = MemCacheManager.Get<List<ClassificationItem>>(cacheKey);
            }
            else
            {
                GetEPGGenreRequest request = new GetEPGGenreRequest();
                request.AuthenticationKey = authenticationKey;

                if (criteria.ClassificationItemId != null && criteria.ClassificationItemId.Value != null)
                {
                    request.ParentID = int.Parse(criteria.ClassificationItemId.Value);
                }

                if (criteria.ProductTypeId != null)
                {
                    request.ProductID = criteria.ProductTypeId.Value.Map();
                }

                request.VideoProgramTypeID = criteria.VideoProgramTypes.Select(t => (int)t).ToArray();

                EPGGenreCollection categories = service.GetEPGGenre(request);

                categoryList = categories.Select(thisCategory => new ClassificationItem(thisCategory)).ToList();

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, categoryList);
                }
            }

            return categoryList.ToArray();
        }

        public IChannel[] GetChannels(CatalogueItemIdentifier catalogueItemId)
        {
            List<Channel> channelList;
            string cacheKey = string.Format("{0}_GetChannels_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, catalogueItemId.Value).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                channelList = MemCacheManager.Get<List<Channel>>(cacheKey);
            }
            else
            {
                GetChannelsRequest req = new GetChannelsRequest();
                req.AuthenticationKey = authenticationKey;

                Video video = getVideo(catalogueItemId);
                if (video == null)
                {
                    return null;
                }

                req.ProgramId = video.ProgramID;

                ChannelCollection channels = service.GetChannels(req);

                if (channels == null)
                {
                    throw new ApplicationException("Null channel returned");
                }

                channelList = new List<Channel>();
                foreach (ChannelData thisChannelData in channels)
                {
                    channelList.Add(new Channel(thisChannelData));
                }

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, channelList);
                }
            }

            return channelList.ToArray();
        }


        #region private members
        private Program getProgram(int programId)
        {
            Program program;
            string cacheKey = string.Format("{0}_GetProgram_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, programId).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                program = MemCacheManager.Get<Program>(cacheKey);
            }
            else
            {
                GetProgramRequest getProgramRequest = new GetProgramRequest();
                getProgramRequest.AuthenticationKey = authenticationKey;

                getProgramRequest.ProgramID = new int[1] { programId };
                getProgramRequest.PageNumber = 1;
                getProgramRequest.PageSize = 2;

                ProgramCollection programCollection = service.GetProgram(getProgramRequest);

                if (programCollection.Count != 1)
                {
                    throw new ApplicationException(string.Format("Expected one program result;  received {0}", programCollection.Count));
                }

                program = programCollection[0];

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, program);
                }

            }

            return program;
        }

        private Video getVideo(CatalogueItemIdentifier catalogueItemId)
        {
            Video video;
            string cacheKey = string.Format("{0}_GetVideo_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, catalogueItemId.Value).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                video = MemCacheManager.Get<Video>(cacheKey);
            }
            else
            {
                GetVideoRequest getVideoRequest = new GetVideoRequest();

                getVideoRequest.AuthenticationKey = authenticationKey;
                getVideoRequest.SortBy = 1; // Dunno what 1 means
                getVideoRequest.ProductId = 0; //This becomes manditory?
                getVideoRequest.PageNumber = 1;
                getVideoRequest.PageSize = 2;
                //getVideoRequest.ProgramID = new int[1] { 0 }; // TODO:  Remove this line!
                getVideoRequest.VideoID = new int[1] { Convert.ToInt32(catalogueItemId.Value) };
                getVideoRequest.VideoProgramType = null;
                getVideoRequest.IPAddress = getCurrentIpAddress();
                VideoCollection videoCollection = service.GetVideo(getVideoRequest);

                if (videoCollection.Count > 1)
                {
                    throw new ApplicationException(string.Format("Expected one video result;  received {0}", videoCollection.Count));
                }
                if (videoCollection.Count == 0)
                {
                    video = null;
                }

                video = videoCollection[0];

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, video);
                }
            }

            return video;
        }

        private Video[] getProgramVideos(int programId)
        {
            VideoCollection programVideos;
            string cacheKey = string.Format("{0}_GetProgramVideos_{1}_{2}", CacheConstants.CachePrefix, authKeyCacheString, programId).CacheRemoveInvalidChars();

            if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
            {
                programVideos = MemCacheManager.Get<VideoCollection>(cacheKey);
            }
            else
            {
                GetVideoRequest getVideoRequest = new GetVideoRequest();

                getVideoRequest.AuthenticationKey = authenticationKey;
                getVideoRequest.SortBy = 1; // Dunno what 5 means
                getVideoRequest.ProductId = 0; //This becomes manditory?
                getVideoRequest.PageNumber = 1;
                getVideoRequest.PageSize = 1000;
                getVideoRequest.ProgramID = new int[1] { programId };

                programVideos = service.GetVideo(getVideoRequest);

                if (cacheKey.CacheKeyIsValid())
                {
                    MemCacheManager.Add(cacheKey, programVideos);
                }
            }

            return programVideos.ToArray();
        }

        private string authenticationKey
        {
            get
            {
                return Settings.Default.AuthenticationKey.ToString();
            }
        }

        private string authKeyCacheString
        {
            get
            {
                return authenticationKey.Substring(0, CacheConstants.AuthKeyLength);
            }
        }

        private MediaContractClient _service = null;
        /// <summary>
        /// GetPersonMeta
        /// GetVideoFile
        /// GetProgram
        /// GetEPGGenre
        /// GetVideo
        /// GetChannels
        /// </summary>
        private MediaContractClient service
        {
            get
            {
                if (_service == null)
                {
                    _service = new MediaContractClient();
                }
                return _service;
            }
        }
        #endregion

        public void InsertCatalogueItemPlayHit(int videoID)
        {
            service.InsertVideoLog(authenticationKey, videoID);
        }



    }

}
