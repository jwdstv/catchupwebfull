﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    public enum MmsVideoFileTypes
    {
        FlashVideo700K = 1,
	    _3GpSd = 2,
        WmVideo700K = 3,
        FlashVideo300K = 4,
	    WmVideo300K = 5,
	    WmVideo1000K = 6,
    }
}
