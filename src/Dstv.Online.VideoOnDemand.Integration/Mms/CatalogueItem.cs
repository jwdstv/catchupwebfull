﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using Dstv.Online.VideoOnDemand.Integration.Properties;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Globalization;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(CatalogueItem))]
    internal class CatalogueItem : ICatalogueItem {
        private Program underlyingProgram;
        private Video underlyingVideo;

        public CatalogueItem(Program program, Video video) {
            underlyingProgram = program;
            underlyingVideo = video;
        }

        [DataMember]
        public string Abstract {
            get { return underlyingVideo.Abstract.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public string ProgramAbstract
        {
            get { return underlyingProgram.Abstract.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public string AgeRestriction {
            get { return underlyingVideo.AgeRestriction.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public DateTime? Airdate {
            get { return underlyingVideo.Airdate; }
            private set { }
        }

        [DataMember]
        public CatalogueItemIdentifier CatalogueItemId {
            get { return new CatalogueItemIdentifier(underlyingVideo.ID.ToString()); }
            private set { }
        }

        [DataMember]
        public ClassificationItemIdentifier ClassificationId {
            get { return ClassificationItemId; } // TODO:  Can we deprecate this?
            private set { }
        }

        [DataMember]
        public ClassificationItemIdentifier ClassificationItemId {
            get {
                if (!underlyingVideo.SubGenreID.HasValue) {
                    return null;
                }
                return new ClassificationItemIdentifier(underlyingVideo.SubGenreID.Value.ToString());
            } // Can we deprecate this?
            private set { }
        }

        [DataMember]
        public string DurationDescription {
            get { return underlyingVideo.Runtime.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public int? EpisodeNumber {
            get { return underlyingVideo.Episode; }
            private set { }
        }

        [DataMember]
        public string EpisodeTitle {
            get { return underlyingVideo.VideoTitle.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public DateTime? ExpiryDate {
            get { return underlyingVideo.Expirydate; }
            private set { }
        }

        [DataMember]
        public bool? IsRestricted {
            get { return underlyingVideo.IsRestricted; }
            private set { }
        }

        [DataMember]
        public string Keywords {
            get {
                List<string> keywordList = new List<string>();

                keywordList.AddRange(underlyingProgram.Keywords.Split(',').Select(x => x.Trim()));
                keywordList.AddRange(underlyingVideo.Keywords.Split(',').Select(x => x.Trim()));

                return string.Join(", ", keywordList.Where(x => !string.IsNullOrWhiteSpace(x)).Distinct().ToArray()); 
            } 
            private set { }
        }

        [DataMember]
        public string MediaImageUri {
            get {
                if (string.IsNullOrEmpty(underlyingVideo.ImageURL))
                {
                    return string.Empty;
                }
                return string.Format("{0}{1}", Settings.Default.ImageServerPath, underlyingVideo.BillboardImageURL);
            }
            private set { }
        }

        [DataMember]
        public string MediaThumbnailUri {
            get {
                if (string.IsNullOrEmpty(underlyingVideo.ImageURL))
                {
                    return string.Empty;
                }
                return string.Format("{0}{1}", Settings.Default.ImageServerPath, underlyingVideo.ImageURL); 
            }
            private set { }
        }

        [DataMember]
        public ProductTypeIdentifier ProductTypeId 
        {
            get {
                ProductTypes? type = underlyingProgram.MapProductType();
                if (type.HasValue) {
                    return new ProductTypeIdentifier(type.Value);
                }
                return null;
            } // TODO:  Should this come off video?
            private set { }
        }

        [DataMember]
        public int? ProgramId {
            get { return underlyingProgram.ID; }
            private set { }
        }

        [DataMember]
        public string ProgramTitle {
            get { return underlyingProgram.ProgramName.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public string ProgramKeywords {
            get { return underlyingProgram.Keywords.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public string VideoKeywords {
            get { return underlyingVideo.Keywords.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public int? Rating {
            get {
                return underlyingVideo.Rating.EvaluateNull();
            }
            private set { }
        }



        [DataMember]
        public CurrencyAmount RentalAmount {
            get {
                CurrencyIdentifier currencyId = new CurrencyIdentifier(underlyingVideo.CurrencyIdentifier);
                return new CurrencyAmount(currencyId, underlyingVideo.Price);
            }
            private set { }
        }

        [DataMember]
        public int RentalPeriodInHours {
            get { return underlyingVideo.RentalPeriodInHours; }
            private set { }
        }

        [DataMember]
        public int RunTimeInMinutes {
            get { return underlyingVideo.RuntimeInMinutes; }
            private set { }
        }

        [DataMember]
        public int? SeasonNumber {
            get { return underlyingVideo.Season; }
            private set { }
        }

        [DataMember]
        public DateTime? StartDate {
            get { return underlyingVideo.Airdate; }
            private set { }
        }

        [DataMember]
        public string Synopsis {
            get { return underlyingProgram.Abstract.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public VideoProgramType VideoProgramType {
            get {
                return (Dstv.Online.VideoOnDemand.MediaCatalogue.VideoProgramType)underlyingVideo.VideoProgramTypeID;
            }
            set { }
        }

        [DataMember]
        public int? VideoProgramTypeId {
            get { return underlyingVideo.VideoProgramTypeID; }
            private set { }
        }

        [DataMember]
        public string VideoTitle {
            get { return underlyingVideo.VideoTitle.EvaluateNull(); }
            private set { }
        }


        [DataMember]
        public string ProgramGenre {
            get {
                return underlyingVideo.GenreName.EvaluateNull();
            }
            set { }
        }

        [DataMember]
        public string ProgramSubGenre
        {
            get
            {
                return underlyingVideo.SubGenreName.EvaluateNull();
            }
            set { }
        }

        [DataMember]
        public string ProgramType {
            get {
                return underlyingVideo.VideoProgramTypeID.ToString();
            }
            set { }
        }

        [DataMember]
        public string ProgramBillboardUri {
            get
            {
                return underlyingProgram.BillboardURL.EvaluateNull();
            }
            set { }
        }

        [DataMember]
        public string IBSProductId
        {
            get { return underlyingVideo.IBSProductID.EvaluateNull(); }
            private set { }
        }


        [DataMember]
        public string AssetId
        {
            get { return underlyingVideo.AssetId.EvaluateNull(); }
            private set { }
        }

        [DataMember]
        public int ProductId
        {
            get { return underlyingVideo.Product; }
            private set { }
        }

    }
}
