﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using System.Runtime.Serialization;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(CastMember))]
    internal class CastMember : ICastMember
    {
        private PersonMetaData underlyingCastMember = null;

        internal CastMember(PersonMetaData person) {
            this.underlyingCastMember = person;
        }

        [DataMember]
        public string FullName
        {
            get {
                return underlyingCastMember.Fullname.EvaluateNull();
            }
            private set { }
        }

        [DataMember]
        public bool IsCast
        {
            get {
                return underlyingCastMember.IsCast;
            }
            private set { }
        }

        [DataMember]
        public bool IsCrew
        {
            get { return underlyingCastMember.IsCrew;
            }
            private set { }
        }

        [DataMember]
        public string Role
        {
            get { return underlyingCastMember.ProfessionName.EvaluateNull();
            }
            private set { }
        }
    }
}
