﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand;

namespace Dstv.Online.VideoOnDemand.Integration.Mms
{
    [Serializable]
    [DataContract]
    [KnownType(typeof(ClassificationItem))]
    internal class ClassificationItem : IClassificationItem {
        EPGGenre underlyingVideoCategory;
        public ClassificationItem(EPGGenre videoCategory) {
            this.underlyingVideoCategory = videoCategory;
        }
        [DataMember]
        public ClassificationItemIdentifier ClassificationItemId {
            get {
                return new ClassificationItemIdentifier(underlyingVideoCategory.ID.ToString());
            }
            private set { }
        }

        [DataMember]
        public bool HasAdditionalClassification {
            get {
                if (underlyingVideoCategory.ChildCount.EvaluateNull() > 0)
                {
                    return true;
                }
                return false;
            }
            private set { }
        }

        [DataMember]
        public string Name {
            get {
                return underlyingVideoCategory.Description.EvaluateNull();
            }
            private set { }
        }

        [DataMember(EmitDefaultValue = false)]
        public ClassificationItemIdentifier ParentClassificationItemId {
            get {
                if (underlyingVideoCategory.ParentID.HasValue) {
                    return new ClassificationItemIdentifier(underlyingVideoCategory.ParentID.ToString());
                }
                return null;
            }
            private set { }
        }

        [DataMember]
        public int TotalNumberOfCatalogueItems {
            get {
                return underlyingVideoCategory.VideoCount.EvaluateNull();
            }
            private set { } 
        }
    }
}
