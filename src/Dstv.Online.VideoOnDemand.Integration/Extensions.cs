﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Integration.MediaCatalogueService;
using System.Text.RegularExpressions;

namespace Dstv.Online.VideoOnDemand.Integration
{
    public static partial class Extensions
    {
        public static VideoProgramType MapVideoProgramType(this Video thisVideo)
        {
            return (Dstv.Online.VideoOnDemand.MediaCatalogue.VideoProgramType)thisVideo.VideoProgramTypeID;
        }

        public static ProductTypes MapProductType(this IClassificationItem classificationItem)
        {
            switch (int.Parse(classificationItem.ClassificationItemId.Value))
            {
                case 9:
                    return (ProductTypes.CatchUp);
                case 10:
                    return (ProductTypes.BoxOffice);
                default:
                    throw new ApplicationException(string.Format("Product type not mapped: {0}",
                                                                 classificationItem.ClassificationItemId.Value));
            }
        }

        public static ProductTypes? MapProductType(this Program program)
        {
            switch (program.Product)
            {
                case 9:
                    return (ProductTypes.CatchUp);
                case 10:
                    return (ProductTypes.BoxOffice);
            }
            return null;
        }

        public static int Map(this ProductTypes type) {
            switch (type) {
                case ProductTypes.BoxOffice:
                    return 10;
                case ProductTypes.CatchUp:
                    return 9;
                case ProductTypes.OnDemand:
                    return 0;
                default:
                    throw new ApplicationException(string.Format("Product type not mapped: {0}", type));
            }
        }

        public static int Map(this SortOrders sortOrder) {
            //1 = alpha
            //2 = start desc
            //3 = highest rated
            //4 = most views
            //5 = related
            //6 = last chance 
            //7 = Ordinal

            switch (sortOrder) {
                case SortOrders.ProgramTitleAsc:
                    return 1;
                case SortOrders.LatestReleases:
                    return 2;
                case SortOrders.MostPopular:
                    return 4;
                case SortOrders.LastChance:
                    return 6;
                case SortOrders.Ordinal:
                    return 7;
                case SortOrders.ByShowProgramTitleAsc:
                    return 8;
                case SortOrders.ByShowVideoStartDateDesc:
                    return 9;
                case SortOrders.ForthcomingReleases:
                    return 10;
                case SortOrders.ByShowProgramTitleAllVideosAsc:
                    return 11;
                case SortOrders.StartDateDesc:
                case SortOrders.ExpiryDateDesc:
                case SortOrders.ProgramTitleDesc:
                case SortOrders.StartDateAsc:
                case SortOrders.StartDateDescFilterFuture:
                case SortOrders.StartDateAscFilterCurrent:
                default:
                    throw new UnsupportedSortOrderException(string.Format("Unsupported sort order: {0}", sortOrder));
            }
        }

        public static string GetImageServerPathSetting()
        {
            return Properties.Settings.Default.ImageServerPath;
        }

        public static string EvaluateNull(this string inString)
        {
            if (string.IsNullOrEmpty(inString))
            {
                return string.Empty;
            }
            return inString;
        }

        public static int EvaluateNull(this int? inInt)
        {
            if (inInt.HasValue)
            {
                return inInt.Value;
            }
            return 0;
        }

        public static string CacheKeyString(this CatalogueCriteria criteria)
        {
            StringBuilder cacheKeySB = new StringBuilder();
            string valueSeparator = "_";
            string arraySeparator = "|";

            cacheKeySB.Append("CatalogueCriteria_");

            cacheKeySB.Append((criteria.CatalogueItemIds != null && criteria.CatalogueItemIds.Length > 0 ? string.Join(arraySeparator, criteria.CatalogueItemIds.Select(itemId => itemId.Value)) : string.Empty)).Append(valueSeparator);

            cacheKeySB.Append(criteria.ClassificationCriteria.ClassificationItemId != null ? criteria.ClassificationCriteria.ClassificationItemId.Value : string.Empty).Append(valueSeparator);
            cacheKeySB.Append(criteria.ClassificationCriteria.ClassificationItemName ?? string.Empty).Append(valueSeparator);
            cacheKeySB.Append((int)criteria.ClassificationCriteria.ClassificationType).Append(valueSeparator);
            cacheKeySB.Append(criteria.ClassificationCriteria.ParentClassificationId != null ? criteria.ClassificationCriteria.ParentClassificationId.Value : string.Empty).Append(valueSeparator);
            cacheKeySB.Append((int)criteria.ClassificationCriteria.ProductTypeId.Value).Append(valueSeparator);
            cacheKeySB.Append(criteria.ClassificationCriteria.TerritoryId != null ? criteria.ClassificationCriteria.TerritoryId.Value : string.Empty).Append(valueSeparator);
            cacheKeySB.Append(criteria.ClassificationCriteria.VideoProgramTypes != null && criteria.ClassificationCriteria.VideoProgramTypes.Length > 0 ? string.Join(arraySeparator, criteria.ClassificationCriteria.VideoProgramTypes.Select(vpt => (int)vpt)) : string.Empty).Append(valueSeparator);

            cacheKeySB.Append(criteria.Paging.ItemsPerPage).Append(valueSeparator);
            cacheKeySB.Append(criteria.Paging.PageNumber).Append(valueSeparator);

            cacheKeySB.Append((int)criteria.SortOrder);

            return cacheKeySB.ToString();
        }

        public static string CacheKeyString(this ClassificationCriteria criteria)
        {
            StringBuilder cacheKeySB = new StringBuilder();
            string valueSeparator = "_";
            string arraySeparator = "|";

            cacheKeySB.Append("ClassificationCriteria_");

            cacheKeySB.Append(criteria.ClassificationItemId != null ? criteria.ClassificationItemId.Value : string.Empty).Append(valueSeparator);
            cacheKeySB.Append(criteria.ClassificationItemName ?? string.Empty).Append(valueSeparator);
            cacheKeySB.Append((int)criteria.ClassificationType).Append(valueSeparator);
            cacheKeySB.Append(criteria.ParentClassificationId != null ? criteria.ParentClassificationId.Value : string.Empty).Append(valueSeparator);
            cacheKeySB.Append((int)criteria.ProductTypeId.Value).Append(valueSeparator);
            cacheKeySB.Append(criteria.TerritoryId != null ? criteria.TerritoryId.Value : string.Empty).Append(valueSeparator);
            cacheKeySB.Append(criteria.VideoProgramTypes != null && criteria.VideoProgramTypes.Length > 0 ? string.Join(arraySeparator, criteria.VideoProgramTypes.Select(vpt => (int)vpt)) : string.Empty);

            return cacheKeySB.ToString();
        }

        public static bool CacheKeyIsValid(this string cacheKey)
        {
            return !string.IsNullOrWhiteSpace(cacheKey) && cacheKey.Length <= CacheConstants.MaxCacheKeyLength;
        }

        public static string CacheRemoveInvalidChars(this string cacheKey)
        {
            string newCacheString = cacheKey.Trim().Replace(" ", "~");

            return newCacheString;
        }
    }
    public class UnsupportedSortOrderException : Exception
    {
        public UnsupportedSortOrderException(string message) : base(message) { }
    }
}
