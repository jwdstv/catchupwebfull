﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Dstv.Online.VideoOnDemand.Integration.Properties;
using SysIO = System.IO;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Dstv.Online.VideoOnDemand.MemCached;

namespace Dstv.Online.VideoOnDemand.Integration.LuceneSearch
{
    public class CatalogueSearch : ICatalogueSearch
    {
        private Directory dir;
        private IndexReader ir;
        private IndexSearcher searcher;
        private MultiFieldQueryParser mfqp;

        private enum IndexedFields
        {
            VideoID,
            ProgramID,
            ProductID,
            VideoTitle,
            Abstract,
            Keywords,
            ProgramName,
            ProgramKeywords,
            ProgramAbstract,
            ProgramTypeName,
            GenreName,
            SubGenreName
        }

        static Dictionary<string, float> fieldBoostDictionary = new Dictionary<string, float>() { { IndexedFields.Abstract.ToString(), 1 }, { IndexedFields.Keywords.ToString(), 1 }, { IndexedFields.ProgramAbstract.ToString(), 1 }, { IndexedFields.ProgramKeywords.ToString(), 1 }, { IndexedFields.VideoTitle.ToString(), 4 }, { IndexedFields.ProgramName.ToString(), 8 } };

        static Dictionary<string, float> relatedFieldBoostDictionary = new Dictionary<string, float>() { { IndexedFields.Keywords.ToString(), 2 }, { IndexedFields.ProgramKeywords.ToString(), 1 } };

        public CatalogueSearch()
        {
            try
            {
                bool isReadonly = true;
                dir = FSDirectory.Open(new SysIO.DirectoryInfo(Settings.Default.MmsIndexPath));

                ir = IndexReader.Open(dir, isReadonly);
                searcher = new IndexSearcher(dir, isReadonly);

                mfqp = new Lucene.Net.QueryParsers.MultiFieldQueryParser(
                    Lucene.Net.Util.Version.LUCENE_CURRENT,
                    fieldBoostDictionary.Keys.ToArray(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), fieldBoostDictionary);

            }
            catch (Exception ex)
            {
                closeIndex();
                throw ex;
            }
        }


        #region ICatalogueSearch Members

        public CatalogueSearchResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm, List<int> productIdList)
        {
            CatalogueSearchResult result = null;

            try
            {
                if (ir != null)
                {
                    string updatedSearchTerm = getUpdatedSearchTerm(searchTerm);

                    BooleanQuery bq = new BooleanQuery();
                    Query finalQuery = getSearchFieldQuery(fieldBoostDictionary, searchTerm, updatedSearchTerm);

                    bq.Add(finalQuery, BooleanClause.Occur.MUST);

                    addProductRestriction(productIdList, bq);

                    TopDocs topDocs = searcher.Search(bq, 1000);
                    List<CatalogueItemIdentifier> resultList = new List<CatalogueItemIdentifier>();
                    int counter = 0;

                    foreach (ScoreDoc thisTopDoc in topDocs.scoreDocs)
                    {
                        Document thisDoc = ir.Document(thisTopDoc.doc);
                        string videoId = thisDoc.ToInt(IndexedFields.VideoID.ToString()).ToString();

                        counter++;

                        resultList.Add(new CatalogueItemIdentifier(videoId));

                        if (counter >= Settings.Default.MaxSearchResults && Settings.Default.MaxSearchResults > 0)
                        {
                            break;
                        }
                    }

                    result = new CatalogueSearchResult(topDocs.totalHits, 1, resultList.ToArray());
                }
            }
            finally
            {
                closeIndex();
            }

            return result ?? new CatalogueSearchResult(0, 0, new List<CatalogueItemIdentifier>().ToArray());

        }

        public CatalogueSearchResult SearchCatalogForRelated(CatalogueCriteria criteria, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList)
        {
            CatalogueSearchResult result = null;

            try
            {
                //Only cache results for related search
                string cacheKey = string.Format("{0}_SearchCatalogRelated_{1}", CacheConstants.CachePrefix, relatedVideoId).CacheRemoveInvalidChars();

                if (cacheKey.CacheKeyIsValid() && MemCacheManager.Exists(cacheKey))
                {
                    result = MemCacheManager.Get<CatalogueSearchResult>(cacheKey);
                }
                else if (ir != null)
                {
                    string updatedSearchTerm = getUpdatedSearchTerm(videoKeywords);

                    BooleanQuery bq = new BooleanQuery();
                    Query finalQuery = getSearchFieldQuery(relatedFieldBoostDictionary, videoKeywords, updatedSearchTerm);

                    bq.Add(finalQuery, BooleanClause.Occur.MUST);


                    //Exclude videos from the same program
                    #region Exclude Same Program

                    QueryParser qpProgram = new QueryParser(Lucene.Net.Util.Version.LUCENE_CURRENT, IndexedFields.ProgramID.ToString(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT));
                    Query excludeProgramQuery = qpProgram.Parse(string.Format("\"{0}\"", excludedProgramId));

                    bq.Add(excludeProgramQuery, BooleanClause.Occur.MUST_NOT);

                    #endregion

                    //Genre query
                    #region Genre query

                    QueryParser qpGenre = new QueryParser(Lucene.Net.Util.Version.LUCENE_CURRENT, IndexedFields.GenreName.ToString(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT));
                    Query genreQuery = qpGenre.Parse(string.Format("\"{0}\"", genre));

                    genreQuery.SetBoost(3);
                    bq.Add(genreQuery, BooleanClause.Occur.SHOULD);

                    #endregion

                    //Sub genre query
                    #region Sub genre query

                    QueryParser qpSubGenre = new QueryParser(Lucene.Net.Util.Version.LUCENE_CURRENT, IndexedFields.SubGenreName.ToString(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT));
                    Query subGenreQuery = qpSubGenre.Parse(string.Format("\"{0}\"", subGenre));

                    subGenreQuery.SetBoost(6);
                    bq.Add(subGenreQuery, BooleanClause.Occur.SHOULD);

                    #endregion


                    addProductRestriction(productIdList, bq);

                    TopDocs topDocs = searcher.Search(bq, 1000);

                    List<CatalogueItemIdentifier> resultList = new List<CatalogueItemIdentifier>();
                    List<CatalogueItemIdentifier> firstProgramResults = new List<CatalogueItemIdentifier>();
                    List<CatalogueItemIdentifier> followingProgramResults = new List<CatalogueItemIdentifier>();
                    List<string> listedPrograms = new List<string>();

                    int counter = 0;
                    foreach (ScoreDoc thisTopDoc in topDocs.scoreDocs)
                    {
                        Document thisDoc = ir.Document(thisTopDoc.doc);
                        string videoId = thisDoc.ToInt(IndexedFields.VideoID.ToString()).ToString();
                        string programId = thisDoc.Get(IndexedFields.ProgramID.ToString());

                        if (programId != excludedProgramId.ToString())
                        {
                            counter++;

                            // The related search is for "You Might Also Like"
                            // Instead of showing three episodes of the same program for "You Might Also Like", 
                            // try to put the first video of each program result at the top of the result list
                            if (!listedPrograms.Contains(programId.ToString()))
                            {
                                listedPrograms.Add(programId.ToString());
                                firstProgramResults.Add(new CatalogueItemIdentifier(videoId));
                            }
                            else
                            {
                                followingProgramResults.Add(new CatalogueItemIdentifier(videoId));
                            }

                            if (counter >= Settings.Default.MaxSearchResults && Settings.Default.MaxSearchResults > 0)
                            {
                                break;
                            }
                        }
                    }


                    firstProgramResults.AddRange(followingProgramResults);

                    resultList.AddRange(firstProgramResults.Distinct());

                    result = new CatalogueSearchResult(topDocs.totalHits, 1, resultList.ToArray());

                    if (cacheKey.CacheKeyIsValid())
                    {
                        MemCacheManager.Add<CatalogueSearchResult>(cacheKey, result);
                    }
                }

                // MMS treats ordinal as alphabetical first and then orders the results in the sequence which the IDs originally sent
                // If you limit the page size, the result set returned will be incorrect
                if (criteria.Paging.ItemsPerPage == 0)
                {
                    criteria.Paging.ItemsPerPage = result.CatalogueItemIds.Length;
                }
            }
            finally
            {
                closeIndex();
            }

            return result ?? new CatalogueSearchResult(0, 0, new List<CatalogueItemIdentifier>().ToArray());

        }

        #endregion

        private static string getUpdatedSearchTerm(string searchTerm)
        {
            string newSearchTerm = searchTerm;
            string[] terms = searchTerm.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            if (terms.Length > 0)
            {
                newSearchTerm = string.Format("\"{0}\"", string.Join("\",\"", terms));
            }

            return newSearchTerm;
        }

        private Query getSearchFieldQuery(Dictionary<string, float> fieldDictionary, string searchTerm, string updatedSearchTerm)
        {
            BooleanQuery finalQuery = new BooleanQuery();

            if (!string.IsNullOrWhiteSpace(searchTerm))
            {

                finalQuery.Add(getSearchTermQuery(mfqp, fieldDictionary, searchTerm, updatedSearchTerm), BooleanClause.Occur.MUST);
            }
            else
            {
                return finalQuery;
            }

            return finalQuery;
        }

        private Query getSearchTermQuery(MultiFieldQueryParser mfqp, Dictionary<string, float> fieldDictionary, string term, string updatedTerm)
        {
            Query standardQuery = mfqp.Parse(term);
            BooleanQuery combinedQuery = new BooleanQuery();

            combinedQuery.Add(standardQuery, BooleanClause.Occur.SHOULD);

            if (!updatedTerm.Equals(string.Format("\"{0}\"", term), StringComparison.OrdinalIgnoreCase))
            {
                Query boostedQuery = mfqp.Parse(updatedTerm);
                boostedQuery.SetBoost(3);
                combinedQuery.Add(boostedQuery, BooleanClause.Occur.SHOULD);
            }

            if (term.WordCount() == 1 && term.Length >= 3 && !term.Contains('*'))
            {
                BooleanQuery wildcardQuery = new BooleanQuery();

                foreach (string searchField in fieldDictionary.Keys)
                {
                    Query wildcardFieldQuery = mfqp.GetPrefixQuery(searchField, term);

                    wildcardFieldQuery.SetBoost(fieldDictionary[searchField]);
                    wildcardQuery.Add(wildcardFieldQuery, BooleanClause.Occur.SHOULD);
                }

                wildcardQuery.SetBoost(0.1f);
                combinedQuery.Add(wildcardQuery, BooleanClause.Occur.SHOULD);
            }

            return combinedQuery;
        }

        private void addProductRestriction(List<int> productIdList, BooleanQuery bq)
        {
            if (productIdList != null && productIdList.Count > 0)
            {
                BooleanQuery bqProduct = new BooleanQuery();

                foreach (int productId in productIdList)
                {
                    QueryParser qpProduct = new QueryParser(Lucene.Net.Util.Version.LUCENE_CURRENT, IndexedFields.ProductID.ToString(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT));

                    Query productQuery = qpProduct.Parse(string.Format("\"{0}\"", productId));
                    bqProduct.Add(productQuery, BooleanClause.Occur.SHOULD);
                }

                bq.Add(bqProduct, BooleanClause.Occur.MUST);
            }
        }

        private void closeIndex()
        {
            if (searcher != null)
            {
                searcher.Close();
            }
            if (ir != null)
            {
                ir.Close();
            }
            if (dir != null)
            {
                dir.Close();
            }
            onDisposed();
        }
        private void onDisposed()
        {
            if (Disposed != null)
            {
                Disposed(this, EventArgs.Empty);
            }
        }
        public event EventHandler Disposed;
    }

    internal static class Extensions
    {
        internal static int ToInt(this Document doc, string fieldName)
        {
            int docField;
            return int.TryParse(doc.Get(fieldName), out docField) ? docField : 0;
        }

        public static int WordCount(this string inString)
        {
            if (string.IsNullOrWhiteSpace(inString))
            {
                return 0;
            }
            else
            {
                return inString.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length;
            }
        }
    }
}
