﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Data;
using System.Web;
namespace Dstv.Online.VideoOnDemand.Modules.Advert
{
    public class Advert
    {
        /// <summary>
        /// Uses the current URL to get the ad tag data from SQL
        /// </summary>
        /// <returns>string representation of the ad tag</returns>
        public static string GetAdData(string advertSize, out string errorMessage)
        {
            string adTag = string.Empty;
            errorMessage = string.Empty;
            string url = HttpContext.Current.Request.Url.AbsolutePath;

            try
            {

                List<AdvertDTO> advertItems = AdvertDb.GetAdTagByPageNameAndSize(url, advertSize);
                if (advertItems != null)
                {
                    if (advertItems.Count > 0)
                    {
                        adTag = HttpContext.Current.Server.HtmlDecode(advertItems[0].Tag);
                        if (string.IsNullOrEmpty(adTag.Trim()))//If not tag is supplied. Use Default Image and Link URL
                        {
                            string image = advertItems[0].DefaultImageURL;
                            string adUrl = advertItems[0].LinkURL;
                            if (!string.IsNullOrEmpty(image) && !string.IsNullOrEmpty(adUrl))
                            {
                                adTag = "<a href=\"" + adUrl + "\" target=\"_blank\"><img src=\"" + image + "\" border=\"0\" /></a>";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.ToString();
            }

            return adTag;
        }

    }
}
