﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Common.Exceptions
{
    [Serializable]
    public class IntegrationException : Exception
    {
        public IntegrationException(string message)
            : base(message)
        {
        }

        public IntegrationException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
