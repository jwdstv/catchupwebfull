﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Common.Exceptions
{
    [Serializable]
    public class RentalDataException : Exception
    {
        public RentalDataException(string message)
            : base(message)
        {
        }

        public RentalDataException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
