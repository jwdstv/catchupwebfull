﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Common.Exceptions
{
    [Serializable]
    public class VideoCatalogueItemNotFoundException : Exception
    {
        public VideoCatalogueItemNotFoundException(string videoCatalogueItemId)
            : base(string.Format("Video Catalogue Item with Id {0} was not found.", videoCatalogueItemId))
        {
        }

        public VideoCatalogueItemNotFoundException(string videoCatalogueItemId, Exception innerException)
            : base(string.Format("Video Catalogue Item with Id {0} was not found.", videoCatalogueItemId), innerException)
        {

        }
    }
}
