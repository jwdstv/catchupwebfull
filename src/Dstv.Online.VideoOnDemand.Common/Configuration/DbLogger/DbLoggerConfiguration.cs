﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Common.Configuration.DbLogger
{
    public sealed class DbLoggerConfiguration
    {
        public static string ConnectionString
        {
            get { return ConnectionStrings.Default.DbLoggerConnectionString; }
        }
    }
}
