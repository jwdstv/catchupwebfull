﻿namespace Dstv.Online.VideoOnDemand.Common
{
    public sealed class Config
    {
        public const string EventLogName = "DStvOnline OnDemand";

        public enum LogTo
        {
            /// <summary>
            /// Log to Windows Event Log
            /// </summary>
            WindowsEventLog,

            /// <summary>
            /// Log to the database
            /// </summary>
            Database
        }

        public enum EventLogSource
        {
            /// <summary>
            /// Box Office
            /// </summary>
            BoxOffice,

            /// <summary>
            /// Catch Up
            /// </summary>
            CatchUp,

            /// <summary>
            /// Open Time
            /// </summary>
            OpenTime,

            /// <summary>
            /// User controls or other
            /// </summary>
            Unknown,

            /// <summary>
            /// Database errors
            /// </summary>
            Database
        }



    }
}
