﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.Configuration.Email;
using System.Net.Mail;

namespace Dstv.Online.VideoOnDemand.Common.Configuration
{
    public sealed class EmailConfiguration
    {
        public static bool UseNetworkCredentials
        {
            get { return EmailSettings.Default.UseNetworkCredential; }
        }

        public static bool UseSsl
        {
            get { return EmailSettings.Default.UseSSL; }
        }


        public static string Username
        {
            get { return EmailSettings.Default.Username; }
        }


        public static string Password
        {
            get { return EmailSettings.Default.Password; }
        }


        public static string SmtpServer
        {
            get { return EmailSettings.Default.SmtpServer; }
        }

        public static int SmtpPort
        {
            get { return EmailSettings.Default.SmtpPortNumber; }
        }

        public static MailAddress SmtpFromMailAddress
        {
            get { return new MailAddress(EmailSettings.Default.FromAddress, EmailSettings.Default.FromDisplayName); }
        }



    }
}
