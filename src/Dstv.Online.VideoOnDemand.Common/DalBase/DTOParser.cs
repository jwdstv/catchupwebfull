﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Dstv.Online.VideoOnDemand.Common.DalBase
{
    public abstract class DTOParser
    {
        abstract public DTOBase PopulateDTO(SqlDataReader reader);
    }
}
