﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Dstv.Online.VideoOnDemand.Common.Configuration;
using Dstv.Online.VideoOnDemand.Common.Exceptions;
using Dstv.Online.VideoOnDemand.Common;

namespace Dstv.Online.VideoOnDemand.Common.DalBase
{
    public abstract class DALBase
    {
        protected static SqlParameter CreateNullParameter(string name, SqlDbType paramType)
        {
            SqlParameter parameter = CreateInputParameter(name, paramType);
            parameter.Value = null;
            return parameter;
        }

        #region CreateInputParameter overloads
        protected static SqlParameter CreateInputParameter(string name, SqlDbType paramType)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.SqlDbType = paramType;
            parameter.Direction = ParameterDirection.Input;
            parameter.ParameterName = name;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, int? value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.Int);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, int value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.Int);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, DateTime value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.DateTime);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, DateTime? value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.DateTime);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, string value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.NVarChar);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, decimal value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.Money);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, decimal? value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.Money);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, Guid value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.UniqueIdentifier);
            parameter.Value = value;
            return parameter;
        }

        protected static SqlParameter CreateInputParameter(string name, Guid? value)
        {
            SqlParameter parameter = CreateInputParameter(name, SqlDbType.UniqueIdentifier);
            parameter.Value = value;
            return parameter;
        }

        #endregion

        protected static SqlParameter CreateOutputParameter(string name, SqlDbType paramtype)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.SqlDbType = paramtype;
            parameter.ParameterName = name;
            parameter.Direction = ParameterDirection.Output;
            return parameter;
        }

   

        public static void RunCommand(ref SqlCommand command)
        {
            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // DB errors get logged to the Event Log
                Logger log = new Logger();

                log.LogEntryTarget = Config.LogTo.WindowsEventLog;
                log.LogException(Config.EventLogSource.Database, ex);
                log.LogEntryTarget = Logger.DefaultLogEntryTarget;

                throw new RentalDataException("Error executing SQL command in DAL.", ex);
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
            }
        }

      
    }
}
