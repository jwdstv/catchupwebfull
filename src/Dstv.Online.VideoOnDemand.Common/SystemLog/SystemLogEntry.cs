﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Common
{
    [DataContract]
    public class SystemLogEntry
    {
        [DataMember]
        public Guid LogId { get; set; }
       
        [DataMember]
        public string Level { get; set; }
        
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string ExceptionType { get; set; }

        [DataMember]
        public string ExceptionDetail { get; set; }
        
        [DataMember]
        public string PageUrl { get; set; }
        
        [DataMember]
        public string Action { get; set; }
        
        [DataMember]
        public string Server { get; set; }
        
        [DataMember]
        public string UserFullName { get; set; }
       
        [DataMember]
        public string UserEmailAddress { get; set; }
        
        [DataMember]
        public string ReferenceNumber { get; set; }
       
    }
}
