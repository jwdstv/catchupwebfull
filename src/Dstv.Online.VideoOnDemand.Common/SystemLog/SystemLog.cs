﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.DalBase;
using Dstv.Online.VideoOnDemand.Common.Configuration.DbLogger;

namespace Dstv.Online.VideoOnDemand.Common
{
    internal class SystemLog : DALBase
    {
        public static void WriteDbEntry(SystemLogEntry entry)
        {
            SqlCommand command = GetDbSprocCommand("SystemLogAddEntry");
            command.Parameters.Add(CreateInputParameter("@LogId", entry.LogId));
            command.Parameters.Add(CreateInputParameter("@Level", entry.Level));
            command.Parameters.Add(CreateInputParameter("@UserId", entry.UserId));
            command.Parameters.Add(CreateInputParameter("@ExceptionDetail", entry.ExceptionDetail));
            command.Parameters.Add(CreateInputParameter("@ExceptionType", entry.ExceptionType));
            command.Parameters.Add(CreateInputParameter("@PageUrl", entry.PageUrl));
            command.Parameters.Add(CreateInputParameter("@Action", entry.Action));
            command.Parameters.Add(CreateInputParameter("@Server", entry.Server));
            command.Parameters.Add(CreateInputParameter("@UserFullname", entry.UserFullName));
            command.Parameters.Add(CreateInputParameter("@UserEmailAddress", entry.UserEmailAddress));
            command.Parameters.Add(CreateInputParameter("@RefNumber", entry.ReferenceNumber));

            RunCommand(ref command);
        }

        private static SqlConnection GetDbConnection()
        {
            return new SqlConnection(DbLoggerConfiguration.ConnectionString);
        }

        private static SqlCommand GetDbSprocCommand(string sqlStoredProcedure)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = GetDbConnection();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = sqlStoredProcedure;
            return command;
        }


    }
}
