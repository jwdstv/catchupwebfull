﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.Configuration.Email;
using System.Net;
using Dstv.Online.VideoOnDemand.Common.Configuration;

namespace Dstv.Online.VideoOnDemand.Common.Smtp
{
    public class Emailer
    {
        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="to">Person / persons seperated by semi-colon ';'</param>
        /// <param name="subject">The subject or title of the email</param>
        /// <param name="body">The body / content of the email</param>
        public static void SendHtmlEmail(string to, string subject, string body)
        {
            SendHtmlEmail(to, string.Empty, string.Empty, subject, body);
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="to">Person / persons seperated by semi-colon ';'</param>
        /// <param name="from">From address</param>
        /// <param name="subject">The subject or title of the email</param>
        /// <param name="body">The body / content of the email</param>
        public static void SendHtmlEmail(string to, MailAddress from, string subject, string body)
        {
            SendHtmlEmail(to, string.Empty, string.Empty, from, subject, body);
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="to">Person or persons seperated by semi-colon ';'</param>
        /// <param name="cc">Person or persons seperated by semi-colon ';'</param>
        /// <param name="bcc">Person or persons seperated by semi-colon ';'</param>
        /// <param name="subject">The subject or title of the email</param>
        /// <param name="body">The body or content of the email</param>
        public static void SendHtmlEmail(string to, string cc, string bcc, string subject, string body)
        {
            SendHtmlEmail(to, string.Empty, string.Empty, EmailConfiguration.SmtpFromMailAddress, subject, body);
        }



        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="to">Person or persons seperated by semi-colon ';'</param>
        /// <param name="cc">Person or persons seperated by semi-colon ';'</param>
        /// <param name="bcc">Person or persons seperated by semi-colon ';'</param>
        /// <param name="from">From address</param>
        /// <param name="subject">The subject or title of the email</param>
        /// <param name="body">The body or content of the email</param>
        public static void SendHtmlEmail(string to, string cc, string bcc, MailAddress from, string subject, string body)
        {
            SmtpClient smtp = new SmtpClient();

            // Configure smtp client
            if (EmailConfiguration.UseNetworkCredentials)
            {
                smtp.EnableSsl = EmailConfiguration.UseSsl;
                smtp.Credentials = new NetworkCredential(EmailConfiguration.Username,
                    EmailConfiguration.Password);
                smtp.UseDefaultCredentials = false;
            }
            else
            {
                smtp.UseDefaultCredentials = false;
            }

            smtp.Host = EmailConfiguration.SmtpServer;
            smtp.Port = EmailConfiguration.SmtpPort;

            MailMessage message = new MailMessage();
            message.From = from;
            message.IsBodyHtml = true;

            // Add multiple 'to' people
            foreach (string toPerson in to.Split(';'))
            {
                if (!string.IsNullOrEmpty(toPerson))
                    message.To.Add(toPerson);
            }

            // Add multiple 'carbon copy' people
            foreach (string toPerson in cc.Split(';'))
            {
                if (!string.IsNullOrEmpty(toPerson))
                    message.CC.Add(toPerson);
            }

            // Add multiple 'blind carbon copy' people
            foreach (string toPerson in bcc.Split(';'))
            {
                if (!string.IsNullOrEmpty(toPerson))
                    message.Bcc.Add(toPerson);
            }

            message.Subject = subject;
            message.Body = body;

            // send the mail message
            smtp.Send(message);
        }
    }
}
