﻿using System;
using System.Diagnostics;

namespace Dstv.Online.VideoOnDemand.Common
{
    internal class DatabaseEventLogger : ILogger
    {
        private Guid recordUniqueIdentifier = Guid.Empty;
        private bool recordIdentifierUsed; // indicates if the Guid has been used.
        public Guid LogEntryId
        {
            get
            {
                if (Guid.Empty == recordUniqueIdentifier || recordIdentifierUsed)
                {
                    recordUniqueIdentifier = Guid.NewGuid();
                    recordIdentifierUsed = false;
                }
                return recordUniqueIdentifier;
            }
        }

        public string LogEntryReference
        {
            get
            {
                // formats a reference number with date and last 5 digits of the LogId GUID.
                return string.Format("{0}-{1}", DateTime.Now.ToString("ddMMyy"), LogEntryId.ToString("N").Substring(27)).ToUpper();
            }
        }

        public void LogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message)
        {
            SystemLogEntry logEntry = MapLogEntry(eventSource, entryType, message);
            LogEntry(logEntry);
        }


        private SystemLogEntry MapLogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message)
        {
              SystemLogEntry entry = new SystemLogEntry();
            entry.Level = entry.ExceptionType = entryType.ToString("f");
            entry.ExceptionDetail = message;
            entry.Action = eventSource.ToString("f");
            entry.Server = Environment.MachineName;

            return entry;
        }

        private SystemLogEntry MapLogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            SystemLogEntry mappedEntry = MapLogEntry(eventSource, entryType, message);

            mappedEntry.PageUrl = pageUrl;
            mappedEntry.Action = action;
            mappedEntry.UserEmailAddress = userEmail;
            mappedEntry.UserFullName = userName;
            mappedEntry.UserId = userId;

            return mappedEntry;
        }

        public void LogEntry(SystemLogEntry entry)
        {
            entry.LogId = LogEntryId;

            // Replace nulls with empty strings
            entry.Action = entry.Action ?? string.Empty;
            entry.ExceptionDetail = entry.ExceptionDetail ?? string.Empty;
            entry.Level = entry.Level ?? string.Empty;
            entry.PageUrl = entry.PageUrl ?? string.Empty;
            entry.ExceptionType = entry.ExceptionType ?? string.Empty;
            entry.Server = entry.Server ?? string.Empty;
            entry.UserEmailAddress = entry.UserEmailAddress ?? string.Empty;
            entry.UserFullName = entry.UserFullName ?? string.Empty;
            entry.UserId = entry.UserId ?? string.Empty;

            entry.ReferenceNumber = LogEntryReference;

            // Ensure a new Log Reference Number for next log entry
            recordIdentifierUsed = true;

            SystemLog.WriteDbEntry(entry);
        }

        public void LogInformation(Config.EventLogSource eventSource, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Information, message);
        }

        public void LogError(Config.EventLogSource eventSource, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Error, message);
        }

        public void LogWarning(Config.EventLogSource eventSource, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Warning, message);
        }

        #region ILogger Members


        public void LogException(Config.EventLogSource eventSource, Exception originalException, string message)
        {
            SystemLogEntry entry = new SystemLogEntry();

            entry.ExceptionDetail = message;
            entry.ExceptionType = originalException.GetType().ToString();
            entry.Level = "Exception";
            entry.Server = originalException.Source;
            entry.Action = eventSource.ToString("f");
            LogEntry(entry);
        }

        #endregion

        #region ILogger Members


        public void LogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            SystemLogEntry entry = MapLogEntry(eventSource, entryType, message, pageUrl, action, userEmail, userName, userId);
            LogEntry(entry);
        }

        public void LogInformation(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            EventLogEntryType entryType = EventLogEntryType.Information;
            LogEntry(eventSource, entryType, message, pageUrl, action, userEmail, userName, userId);
        }

        public void LogError(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            EventLogEntryType entryType = EventLogEntryType.Error;
            LogEntry(eventSource, entryType, message, pageUrl, action, userEmail, userName, userId);
        }

        public void LogException(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            EventLogEntryType entryType = EventLogEntryType.Error;
            LogEntry(eventSource, entryType, message, pageUrl, action, userEmail, userName, userId);
        }

        public void LogWarning(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            EventLogEntryType entryType = EventLogEntryType.Warning;
            LogEntry(eventSource, entryType, message, pageUrl, action, userEmail, userName, userId);
        }

        #endregion
    }
}
