﻿using System;
using System.Diagnostics;
using System.Text;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Common
{
    public sealed class Logger
    {
        #region Properties

        #region public static LogTo WriteLogEntryTo
        public const Config.LogTo DefaultLogEntryTarget = Config.LogTo.Database; // default 

        private Config.LogTo writeLogEntryTo = DefaultLogEntryTarget;

        /// <summary>
        ///   Indicates where the log entry will be written off to
        /// </summary>
        public Config.LogTo LogEntryTarget
        {
            get
            {
                return writeLogEntryTo;
            }
            set
            {
                writeLogEntryTo = value;
            }
        }
        #endregion

        #endregion

        #region Public methods

        #region public void LogException
        /// <summary>
        ///   Logs an exception to the configured log
        /// </summary>
        /// <param name = "exception">An exception to be logged</param>
        /// <param name = "logSource">Indicates which subsite the message originates from</param>
       public string LogException(Config.EventLogSource logSource, Exception exception)
        {
            string message = GetExceptionAsString(exception);
            return LogError(logSource, message);
        }

       public string LogException(Config.EventLogSource logSource, Exception exception, string pageUrl, string action, string userEmail, string userName, string userId)
       {
			 return Log(logSource, EventLogEntryType.Error, GetExceptionAsString(exception), pageUrl, action, userEmail, userName, userId);
       }

		 public string LogException ( Config.EventLogSource logSource, Exception exception, HttpRequest pageRequest, string action, string userEmail, string userName, string userId )
		 {
			 return Log(logSource, EventLogEntryType.Error, GetExceptionAsString(exception), pageRequest, action, userEmail, userName, userId);
		 }

        #endregion

        #region public void LogError
        /// <summary>
        ///   Logs an error to the configured log
        /// </summary>
        /// <param name = "logSource">Indicates which subsite the message originates from</param>
        /// <param name = "message">The message (entry) which will be logged</param>
        public string LogError(Config.EventLogSource logSource, string message)
        {
            return WriteLogEntry(logSource, EventLogEntryType.Error, message);
        }

        public string LogError(Config.EventLogSource logSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
			  return Log(logSource, EventLogEntryType.Error, message, pageUrl, action, userEmail, userName, userId);
        }

		  public string LogError ( Config.EventLogSource logSource, string message, HttpRequest pageRequest, string action, string userEmail, string userName, string userId )
		  {
			  return Log(logSource, EventLogEntryType.Error, message, pageRequest, action, userEmail, userName, userId);
		  }

        #endregion


        #region public void LogInformation
        /// <summary>
        ///   Logs information to the configured log
        /// </summary>
        /// <param name = "logSource">Indicates which subsite the message originates from</param>
        /// <param name = "message">The message (entry) which will be logged</param>
        public string LogInformation(Config.EventLogSource logSource, string message)
        {
            return WriteLogEntry(logSource, EventLogEntryType.Information, message);
        }

        public string LogInformation(Config.EventLogSource logSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
			  return Log(logSource, EventLogEntryType.Information, message, pageUrl, action, userEmail, userName, userId);
        }

		  public string LogInformation ( Config.EventLogSource logSource, string message, HttpRequest pageRequest, string action, string userEmail, string userName, string userId )
		  {
			  return Log(logSource, EventLogEntryType.Information, message, pageRequest, action, userEmail, userName, userId);
		  }

        #endregion


        #region public void LogWarning
        /// <summary>
        ///   Logs a warning to the configured log
        /// </summary>
        /// <param name = "logSource">Indicates which subsite the message originates from</param>
        /// <param name = "message">The message (entry) which will be logged</param>
        public string LogWarning(Config.EventLogSource logSource, string message)
        {
            return WriteLogEntry(logSource, EventLogEntryType.Warning, message);
        }

        public string LogWarning(Config.EventLogSource logSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
				return Log(logSource, EventLogEntryType.Warning, message, pageUrl, action, userEmail, userName, userId);
        }

		  public string LogWarning ( Config.EventLogSource logSource, string message, HttpRequest pageRequest, string action, string userEmail, string userName, string userId )
		  {
			  return Log(logSource, EventLogEntryType.Warning, message, pageRequest, action, userEmail, userName, userId);
		  }

        #endregion


		  public string Log ( Config.EventLogSource logSource, EventLogEntryType eventLogEntryType, string message, string pageUrl, string action, string userEmail, string userName, string userId )
		  {
			  return WriteLogEntry(logSource, eventLogEntryType, message, pageUrl, action, userEmail, userName, userId);
		  }

		  public string Log ( Config.EventLogSource logSource, EventLogEntryType eventLogEntryType, string message, HttpRequest pageRequest, string action, string userEmail, string userName, string userId )
		  {
			  return WriteLogEntry(
				  logSource, 
				  eventLogEntryType, 
				  message, 
				  string.Format("Url: {0} UrlReferrer: {1}", pageRequest.Url.AbsoluteUri, pageRequest.UrlReferrer.AbsoluteUri),
				  action, 
				  userEmail, 
				  userName, 
				  userId
				  );
		  }

        #endregion


        #region Private methods

        #region private void WriteLogEntry
        /// <summary>
        ///   Writes the entry type and entry to the configured log
        /// </summary>
        /// <param name = "logSource">Indicates which subsite the message originates from</param>
        /// <param name = "message">The message (entry) which will be logged</param>
        /// <param name = "messageType">The entry type : Warning / Error / Information</param>
        private string WriteLogEntry(Config.EventLogSource logSource, EventLogEntryType messageType, string message)
        {
            ILogger logger = LoggerFactory.GetLogger(LogEntryTarget);
            try
            {
                logger.LogEntry(logSource, messageType, message);
            }
            catch (Exception ex)
            {
                if (LogEntryTarget == Config.LogTo.Database)
                {
                    // problem writing to db, write to eventlog
                    ILogger secondaryLogger = LoggerFactory.GetLogger(Config.LogTo.WindowsEventLog);
                    secondaryLogger.LogEntry(logSource, messageType, message);
                    return logger.LogEntryReference;
                }
            }
            return logger.LogEntryReference;
        }


        private string WriteLogEntry(Config.EventLogSource logSource, EventLogEntryType messageType, string message, string pageUrl,
                                        string action, string userEmail, string userName, string userId)
        {
            ILogger logger = LoggerFactory.GetLogger(LogEntryTarget);
            try
            {
                logger.LogEntry(logSource, messageType, message, pageUrl, action, userEmail, userName, userId);
            }
            catch (Exception ex)
            {
                if (LogEntryTarget == Config.LogTo.Database)
                {
                    // problem writing to db, write to eventlog
                    ILogger secondaryLogger = LoggerFactory.GetLogger(Config.LogTo.WindowsEventLog);
                    secondaryLogger.LogEntry(logSource, messageType, message, pageUrl, action, userEmail, userName, userId);
                    return logger.LogEntryReference;
                }
            }
            return logger.LogEntryReference;
        }



        #endregion

        #region private string GetExceptionAsString
        /// <summary>
        ///   Returns a string with the exception and inner exception information
        /// </summary>
        /// <param name = "exception">The exception to traverse</param>
        /// <returns>string</returns>
        public static string GetExceptionAsString(Exception exception)
        {
            StringBuilder exceptionString = new StringBuilder();

            while (exception != null)
            {
                exceptionString.AppendFormat("Exception :: ({0})\r\n" +
                                             "===================================================\r\n" +
                                             "Message:  {1}\r\n" +
                                             "Source: {2}\r\n" +
                                             "StackTrace: {3}\r\n\r\n" +
                                             "----------------------------------------------------\r\n",
                                             exception.GetType(),
                                             exception.Message,
                                             exception.Source,
                                             exception.StackTrace
                    );

                exception = exception.InnerException;
            }

            return exceptionString.ToString();
        }
        #endregion

        #endregion
    }
}