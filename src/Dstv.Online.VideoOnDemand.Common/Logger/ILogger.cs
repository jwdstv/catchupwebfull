﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Dstv.Online.VideoOnDemand.Common.Configuration;

namespace Dstv.Online.VideoOnDemand.Common
{
    internal interface ILogger
    {
        string LogEntryReference { get; }

        void LogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message);
        void LogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message, string pageUrl, string action, string userEmail, string userName, string userId);
    }
}
