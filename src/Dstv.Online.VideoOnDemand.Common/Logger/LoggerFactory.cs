﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.Common.Configuration;

namespace Dstv.Online.VideoOnDemand.Common
{
    internal sealed class LoggerFactory
    {
        internal static ILogger GetLogger(Config.LogTo logTo)
        {
            switch (logTo)
            {
                case Config.LogTo.WindowsEventLog:
                    return new WindowsEventLogger();

                case Config.LogTo.Database:
                    return new DatabaseEventLogger();

                default:
                    throw new NotImplementedException(string.Format("Enum value {0} not implemented in LoggerFactory.", logTo));
            }
        }
    }
}
