﻿using System;
using System.Diagnostics;
using System.Text;

namespace Dstv.Online.VideoOnDemand.Common
{
    internal class WindowsEventLogger : ILogger
    {
        private Guid recordUniqueIdentifier = Guid.Empty;
        private bool recordIdentifierUsed; // indicates if the Guid has been used.
        public Guid LogEntryId
        {
            get
            {
                if (Guid.Empty == recordUniqueIdentifier || recordIdentifierUsed)
                {
                    recordUniqueIdentifier = Guid.NewGuid();
                    recordIdentifierUsed = false;
                }
                return recordUniqueIdentifier;
            }
        }

        #region ILogger Members

        public string LogEntryReference
        {
            get
            {
                // formats a reference number with date and last 5 digits of the LogId GUID.
                return string.Format("{0}-{1}", DateTime.Now.ToString("ddMMyy"), LogEntryId.ToString("N").Substring(27)).ToUpper();
            }
        }

        /// <summary>
        ///   Logs an entry to the Windows event log
        /// </summary>
        /// <param name = "eventSource"></param>
        /// <param name = "entryType"></param>
        /// <param name = "message"></param>
        public void LogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message)
        {
            // Ensure a new Log Reference Number for next log entry
            recordIdentifierUsed = true;

            using (EventLog eventLog = new EventLog(Config.EventLogName))
            {
                eventLog.Source = eventSource.ToString();
                eventLog.WriteEntry(message, entryType);
            }
        }

        /// <summary>
        ///   Logs an informational entry to the Windows event log
        /// </summary>
        /// <param name = "eventSource"></param>
        /// <param name = "message"></param>
        public void LogInformation(Config.EventLogSource eventSource, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Information, message);
        }

        /// <summary>
        ///   Logs an error entry to the Windows event log
        /// </summary>
        /// <param name = "eventSource"></param>
        /// <param name = "message"></param>
        public void LogError(Config.EventLogSource eventSource, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Error, message);
        }

        /// <summary>
        ///   Logs a warning entry to the Windows event log
        /// </summary>
        /// <param name = "eventSource"></param>
        /// <param name = "message"></param>
        public void LogWarning(Config.EventLogSource eventSource, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Warning, message);
        }

        public void LogException(Config.EventLogSource eventSource, Exception originalException, string message)
        {
            LogEntry(eventSource, EventLogEntryType.Error, message);
        }


        public void LogEntry(Config.EventLogSource eventSource, EventLogEntryType entryType, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            string shapedErrorMessage = FormatUserErrorInfo(message, pageUrl, action, userEmail, userName, userId);
            LogEntry(eventSource, entryType, shapedErrorMessage);
        }

        public void LogInformation(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            string shapedErrorMessage = FormatUserErrorInfo(message, pageUrl, action, userEmail, userName, userId);
            LogInformation(eventSource, shapedErrorMessage);
        }

        public void LogError(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            string shapedErrorMessage = FormatUserErrorInfo(message, pageUrl, action, userEmail, userName, userId);
            LogError(eventSource, shapedErrorMessage);
        }

        public void LogException(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            string shapedErrorMessage = FormatUserErrorInfo(message, pageUrl, action, userEmail, userName, userId);
            LogError(eventSource, shapedErrorMessage);
        }

        public void LogWarning(Config.EventLogSource eventSource, string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            string shapedErrorMessage = FormatUserErrorInfo(message, pageUrl, action, userEmail, userName, userId);
            LogWarning(eventSource, shapedErrorMessage);
        }

        #endregion

        private string FormatUserErrorInfo(string message, string pageUrl, string action, string userEmail, string userName, string userId)
        {
            StringBuilder userInfo = new StringBuilder();

            userInfo.AppendLine("============= USER INFO =============");
            userInfo.AppendLine(string.Format("Page URL : {0}\r\nConnect User Id: {1}\r\nUser full name : {2}", pageUrl, userId, userName));
            userInfo.AppendLine(string.Format("User Email address : {0}\r\nAction : {1}", userEmail, action));
            userInfo.AppendLine(string.Format("Reference : {0}\r\nEventLog Id : {1}", LogEntryReference, LogEntryId));
            userInfo.AppendLine("=====================================");
            userInfo.AppendLine("=== Error Information ===============");
            userInfo.AppendLine("=====================================");
            userInfo.AppendLine("");
            userInfo.Append(message);

            return userInfo.ToString();
        }
    }
}