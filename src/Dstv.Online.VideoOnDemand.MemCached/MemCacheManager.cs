﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Memcached.ClientLibrary;
using Dstv.Online.VideoOnDemand.MemCached.Config;

#endregion

namespace Dstv.Online.VideoOnDemand.MemCached
{
    /// <summary>
    /// Encapsulates MemCache functionality in a single class.
    /// </summary>
    public class MemCacheManager
    {
        #region Private Members

        /// <summary>
        /// Cache pool
        /// </summary>
        static SockIOPool pool;

        /// <summary>
        /// Client used to interact with cache
        /// </summary>
        static MemcachedClient cache;

        /// <summary>
        /// Contains the caching settings
        /// for each control type
        /// </summary>
        static ControlCacheCollection controlConfig;

        /// <summary>
        /// Default expiry = 15 min
        /// </summary>
        static int defaultExpiry = 15;

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        static MemCacheManager()
        {
            ServerAddressCollection collection = MemCacheConfiguration.GetConfig().Servers;

            string[] servers = new string[collection.Count];

            for (int i = 0; i < collection.Count; i++)
                servers[i] = string.Format("{0}:{1}", collection[i].Address, collection[i].Port);

            // Get the control configuration
            controlConfig = ControlCacheConfiguration.GetConfig().Config;

            if (servers.Length != 0)
            {
                pool = SockIOPool.GetInstance();
                pool.SetServers(servers);
                pool.Initialize();

                cache = new MemcachedClient();
            }
        }


        /// <summary>
        /// Persists an object to MemCache
        /// </summary>
        /// <param name="_key"></param>
        /// <param name="_val"></param>
        /// <param name="_expiryInMinutes"></param>
        public static void Add(string _key, object _val, int _expiryInMinutes)
        {
			  if (Exists(_key))
			  {
				  cache.Replace(_key, _val, DateTime.Now.AddMinutes(_expiryInMinutes));
			  }
			  else
			  {
				  cache.Add
						(
							 _key,
							 _val,
							 DateTime.Now.AddMinutes(_expiryInMinutes)
						);
			  }
        }

		  /// <summary>
		  /// Persists an object to MemCache
		  /// </summary>
		  /// <param name="_key"></param>
		  /// <param name="_val"></param>
		  /// <param name="_expiryTime"></param>
		  public static void Add ( string _key, object _val, DateTime _expiryTime )
		  {
			  if (Exists(_key))
			  {
				  cache.Replace(_key, _val, _expiryTime);
			  }
			  else
			  {
				  cache.Add
						(
							 _key,
							 _val,
							 _expiryTime
						);
			  }
		  }

		  /// <summary>
		  /// Persists an object to MemCache
		  /// </summary>
		  /// <param name="_key"></param>
		  /// <param name="_val"></param>
		  /// <param name="_expiryInMinutes"></param>
		  public static void Add ( string _key, object _val )
		  {
              Add<object>
                    (
                        _key,
                        _val
                    );
		  }

        /// <summary>
        /// Persist a value to the control cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_key"></param>
        /// <param name="_val"></param>
        public static void Add<T>(string _key, object _val)
        {
            ControlCache config = GetControlConfig(typeof(T));

            Add
                (
                    _key,
                    _val,
                    (config != null ? config.ExpiryInMinutes : defaultExpiry)
                );
        }

        /// <summary>
        /// Get control caching configuration
        /// </summary>
        /// <param name="_type"></param>
        /// <returns></returns>
        private static ControlCache GetControlConfig(Type _type)
        {
            return controlConfig.Get(_type);
        }

        /// <summary>
        /// Retrieves an object from MemCache
        /// </summary>
        /// <param name="_key"></param>
        /// <returns></returns>
        public static object Get(string _key)
        {
            return Get(_key, -1);
        }

        /// <summary>
        /// Retrieves an object from MemCache
        /// </summary>
        /// <param name="_key"></param>
        /// <param name="_slidingExpirationAmount"></param>
        /// <returns></returns>
        public static object Get(string _key, int _slidingExpirationAmount)
        {
            if (!Exists(_key))
                return null;

            object result = cache.Get(_key);

            // Reset the expiration
            if (_slidingExpirationAmount > 0)
                Add(_key, result, _slidingExpirationAmount);

            return result;
        }

        /// <summary>
        /// Retrieves an object from MemCache and casts
        /// it to the specified output type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_key"></param>
        /// <returns></returns>
        public static T Get<T>(string _key) where T : class
        {
            ControlCache config = GetControlConfig(typeof(T));

            if (config != null && config.IsSliding)
            {
                return (T)Get(_key, config.ExpiryInMinutes); // Apply sliding expiration
            }
            else
            {
                return (T)Get(_key);
            }
        }

		  public static bool Remove ( string _key )
		  {
			  return cache.Delete(_key);
		  }

        /// <summary>
        /// Checks if a object exists
        /// in MemCache
        /// </summary>
        /// <param name="_key"></param>
        /// <returns></returns>
        public static bool Exists(string _key)
        {
            return cache.KeyExists(_key);
        }

        /// <summary>
        /// Get control caching settings
        /// </summary>
        public static ControlCacheCollection ControlConfiguration
        {
            get { return controlConfig; }
        }

        /// <summary>
        /// Releases all cached object and
        /// closes all connections
        /// </summary>
        public static void Close()
        {
            // Flush all cached values
            cache.FlushAll();

            // Close the cache pool
            pool.Shutdown();
        }
    }
}