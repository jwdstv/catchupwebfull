
#region References

using System;
using System.Collections;
using System.Xml.Serialization;

#endregion

namespace Dstv.Online.VideoOnDemand.MemCached.Config
{
	[Serializable()]
	public class ControlCacheCollection : CollectionBase
	{
        public virtual void Add(ControlCache _config) {
			this.InnerList.Add(_config);
		}

        public ControlCache this[int index] {
            get { return (ControlCache)this.InnerList[index]; }
			set { this.InnerList[index] = value; }
		}

        public ControlCache Get(Type _type)
        {
            string typeStr = _type.ToString().ToLower();
            ControlCache typeConfig = null;
            ControlCache objectConfig = null;

            foreach (ControlCache c in this)
            {
                if (typeStr.Contains(c.Type.ToLower()))
                {
                    typeConfig = c;
                }

                if (c.Type.ToLower() == "object")
                {
                    objectConfig = c;
                }
            }


            return typeConfig ?? objectConfig;
        }
	}

    [Serializable()]
    public class ControlCache
    {
        [XmlAttribute()]
        public string Type {
            get;
            set;
        }

        [XmlAttribute()]
        public bool IsSliding
        {
            get;
            set;
        }

        [XmlAttribute()]
        public int ExpiryInMinutes
        {
            get;
            set;
        }
    }
}
