
#region References

using System;
using System.Collections;
using System.Xml.Serialization;

#endregion

namespace Dstv.Online.VideoOnDemand.MemCached.Config
{
	[Serializable()]
	public class ServerAddressCollection : CollectionBase
	{
        public virtual void Add(ServerAddress _addr) {
			this.InnerList.Add(_addr);
		}

        public ServerAddress this[int index] {
            get { return (ServerAddress)this.InnerList[index]; }
			set { this.InnerList[index] = value; }
		}
	}

    [Serializable()]
    public class ServerAddress
    {
        [XmlAttribute()]
        public string Address
        {
            get;
            set;
        }

        [XmlAttribute()]
        public string Port
        {
            get;
            set;
        }
    }
}
