﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Configuration;
using System.Xml;
using System.Web;

#endregion

namespace Dstv.Online.VideoOnDemand.MemCached.Config
{
    /// <summary>
    /// Specifies the MemCache configuration settings.
    /// </summary>
    [Serializable()]
    [XmlRoot("MemCacheConfig")]
    public class MemCacheConfiguration
    {
        /// <summary>
        /// Instance of the rules collection
        /// </summary>
        private ServerAddressCollection servers;

        /// <summary>
        /// GetConfig() returns an instance of the <b>MemCacheConfiguration</b> class with the values populated from
        /// the Web.config file.  It uses XML deserialization to convert the XML structure in Web.config into
        /// a <b>CacheConfiguration</b> instance.
        /// </summary>
        /// <returns>A <see cref="RewriterConfiguration"/> instance.</returns>
        public static MemCacheConfiguration GetConfig()
        {
            if (HttpContext.Current.Cache["MemCacheConfig"] == null)
                HttpContext.Current.Cache.Insert("MemCacheConfig", ConfigurationSettings.GetConfig("MemCacheConfig"));

            return (MemCacheConfiguration)HttpContext.Current.Cache["MemCacheConfig"];
        }

        /// <summary>
        /// A <see cref="ServerAddressCollection"/> instance that provides access to a set of server ip addresses.
        /// </summary>
        public ServerAddressCollection Servers
        {
            get { return servers; }
            set { servers = value; }
        }
    }

    /// <summary>
    /// Deserializes the markup in Web.config into an instance of the CacheConfiguration class.
    /// </summary>
    public class MemCacheConfigSerializerSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlSerializer ser = new XmlSerializer(typeof(MemCacheConfiguration));

            // Return the Deserialized object from the Web.config XML
            return ser.Deserialize(new XmlNodeReader(section));
        }
    }
}