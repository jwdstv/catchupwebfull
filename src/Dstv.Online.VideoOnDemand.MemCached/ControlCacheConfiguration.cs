﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Configuration;
using System.Xml;
using System.Web;

#endregion

namespace Dstv.Online.VideoOnDemand.MemCached.Config
{
    /// <summary>
    /// Specifies the control caching configuration settings.
    /// </summary>
    [Serializable()]
    [XmlRoot("ControlCacheConfig")]
    public class ControlCacheConfiguration
    {
        /// <summary>
        /// Instance of the config collection
        /// </summary>
        private ControlCacheCollection config;

        /// <summary>
        /// GetConfig() returns an instance of the <b>ControlCacheConfiguration</b> class with the values populated from
        /// the Web.config file.  It uses XML deserialization to convert the XML structure in Web.config into
        /// a <b>CacheConfiguration</b> instance.
        /// </summary>
        /// <returns>A <see cref="RewriterConfiguration"/> instance.</returns>
        public static ControlCacheConfiguration GetConfig()
        {
            if (HttpContext.Current.Cache["ControlCacheConfig"] == null)
                HttpContext.Current.Cache.Insert("ControlCacheConfig", ConfigurationSettings.GetConfig("ControlCacheConfig"));

            return (ControlCacheConfiguration)HttpContext.Current.Cache["ControlCacheConfig"];
        }

        /// <summary>
        /// A <see cref="ServerAddressCollection"/> instance that provides access to a set of server ip addresses.
        /// </summary>
        public ControlCacheCollection Config
        {
            get { return config; }
            set { config = value; }
        }
    }

    /// <summary>
    /// Deserializes the markup in Web.config into an instance of the CacheConfiguration class.
    /// </summary>
    public class ControlCacheSerializerSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ControlCacheConfiguration));

            // Return the Deserialized object from the Web.config XML
            return ser.Deserialize(new XmlNodeReader(section));
        }
    }
}