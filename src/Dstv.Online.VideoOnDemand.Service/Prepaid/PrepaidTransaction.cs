﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Dstv.Online.VideoOnDemand.Data;

namespace MultiChoice.DStvOnline.OnDemand.Service.Prepaid
{
    [DataContract]
    public sealed class PrepaidTransaction
    {
        public PrepaidTransaction()
        {
        }

        internal PrepaidTransaction(IOrder order, IEnumerable<IOrderDetail> orderDetail)
        {
            orderHeader = order as OrderHeader;
            orderItems = orderDetail.Cast<OrderItems>().ToList();
        }

       private OrderHeader orderHeader;
        
        [DataMember]
        public OrderHeader Order
        {
            get { return orderHeader ?? (orderHeader = new OrderHeader()); }
            set { orderHeader = value; }
        }


        private List<OrderItems> orderItems;

        [DataMember]
        public List<OrderItems> OrderDetailItems
        {
            get { return orderItems ?? (orderItems = new List<OrderItems>()); }
            set { orderItems = value; }
        }
    }
}