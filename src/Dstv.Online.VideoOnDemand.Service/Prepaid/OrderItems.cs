﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dstv.Online.VideoOnDemand.Data;
using System.Runtime.Serialization;

namespace MultiChoice.DStvOnline.OnDemand.Service.Prepaid
{
    [DataContract]
    public class OrderItems : IOrderDetail
    {
        #region IOrderDetail Members

        [DataMember]
        public DateTime ActivationDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal Amount
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ExpiryDate
        {
            get;
            set;
        }

        [DataMember]
        public Guid Id
        {
            get;
            set;
        }

        [DataMember]
        public Guid OrderId
        {
            get;
            set;
        }

        [DataMember]
        public string SkuDescription
        {
            get;
            set;
        }

        [DataMember]
        public string SkuId
        {
            get;
            set;
        }

        [DataMember]
        public string SkuName
        {
            get;
            set;
        }

        [DataMember]
        public string SkuType
        {
            get;
            set;
        }

        [DataMember]
        public string TransactionReference
        {
            get;
            set;
        }

        #endregion
    }
}