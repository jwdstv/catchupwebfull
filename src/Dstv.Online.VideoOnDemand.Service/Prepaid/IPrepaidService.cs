﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MultiChoice.DStvOnline.OnDemand.Service.Prepaid
{
    [ServiceContract]
    public interface IPrepaidService
    {
        [OperationContract]
        string AddPrepaidTransaction(PrepaidTransaction prepaidTransaction);

        [OperationContract(Name = "GetPrepaidTransactionByUserId")]
        PrepaidTransaction GetPrepaidTransaction(string userId, string skuType);

        [OperationContract(Name = "GetPrepaidTransactionByOrderId")]
        PrepaidTransaction GetPrepaidTransaction(Guid orderId);
    }
}
