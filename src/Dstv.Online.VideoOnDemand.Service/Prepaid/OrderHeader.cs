﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Dstv.Online.VideoOnDemand.Data;

namespace MultiChoice.DStvOnline.OnDemand.Service.Prepaid
{
    [DataContract]
    public class OrderHeader :  IOrder
    {
        #region IOrder Members

        [DataMember]
        public DateTime DateOfPurchase
        {
            get;
            set;
        }

        [DataMember]
        public Guid Id
        {
            get;
            set;
        }

        [DataMember]
        public decimal Total
        {
            get;
            set;
        }

        [DataMember]
        public string UserEmail
        {
            get;
            set;
        }

        [DataMember]
        public string UserFullName
        {
            get;
            set;
        }

        [DataMember]
        public string UserId
        {
            get;
            set;
        }

        #endregion
    }
}