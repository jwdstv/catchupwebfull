﻿using System;
using System.Collections.Generic;
using Dstv.Online.VideoOnDemand.Data;


namespace MultiChoice.DStvOnline.OnDemand.Service.Prepaid
{
    public class Prepaid : IPrepaidService
    {

        #region IPrepaidService Members

        public string AddPrepaidTransaction(PrepaidTransaction prepaidTransaction)
        {
            // TODO:  Wallet debit

            IOrder order = prepaidTransaction.Order;
            RentalDb.AddOrder((OrderDTO)order);

            foreach (OrderItems orderItem in prepaidTransaction.OrderDetailItems)
            {
                orderItem.OrderId = prepaidTransaction.Order.Id;
                IOrderDetail orderDetail = orderItem;
                RentalDb.AddOrderDetail((OrderDetailDTO)orderDetail);
            }

            return "Success";
        }

        public PrepaidTransaction GetPrepaidTransaction(string userId, string skuType)
        {
            List<OrderDetailDTO> orderDetail = RentalDb.GetOrderDetailByUserIdAndSkuType(userId, skuType);

            OrderDTO order = new OrderDTO();

            if (orderDetail.Count > 0)
                order = RentalDb.GetOrderById(orderDetail[0].OrderId);

            return new PrepaidTransaction(order, orderDetail);
        }

        public PrepaidTransaction GetPrepaidTransaction(Guid orderId)
        {
            List<OrderDetailDTO> orderDetail = RentalDb.GetOrderDetailById(orderId);

            OrderDTO order = RentalDb.GetOrderById(orderId);

            return new PrepaidTransaction(order, orderDetail);
        }



        #endregion
    }
}
