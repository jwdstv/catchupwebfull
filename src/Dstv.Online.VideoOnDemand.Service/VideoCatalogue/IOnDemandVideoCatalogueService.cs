﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;

namespace Dstv.Online.VideoOnDemand.Service
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "OnDemandService" in both code and config file together.
    [ServiceContract]
    public interface IOnDemandVideoCatalogueService
    {
        [OperationContract]
        CatalogueResult GetVideoItems(CatalogueCriteria criteria);

        [OperationContract]
        EditorialPlaylist GetEditorialList(string listName);

        [OperationContract]
        List<IClassificationItem> GetCatalogueClassification(ClassificationCriteria classificationCriteria);

        [OperationContract]
        List<ICatalogueItem> GetVideoItemDetails(CatalogueItemIdentifier catalogueItemIdentifier, bool includeRelatedMedia);

        [OperationContract]
        CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm);
 
        [OperationContract]
       CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm, List<int> productIdList);

        [OperationContract]
        CatalogueResult SearchCatalogForRelated(CatalogueCriteria criteria, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList);
    }
}
