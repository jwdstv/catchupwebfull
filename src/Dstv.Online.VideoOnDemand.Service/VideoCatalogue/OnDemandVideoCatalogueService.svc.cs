﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;

namespace Dstv.Online.VideoOnDemand.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOnDemandRentalService" in both code and config file together.
    public class OnDemandVideoCatalogueService : IOnDemandVideoCatalogueService
    {

        #region IOnDemandVideoCatalogueService Members

        public CatalogueResult GetVideoItems(CatalogueCriteria criteria)
        {
            return VideoCatalogue.GetVideoItems(criteria);
        }


        public ProgramResult GetProgramVideoDetail(CatalogueItemIdentifier itemId, VideoProgramType programType)
        {
            return VideoCatalogue.GetProgramVideoDetail(itemId, programType);
        }

        public EditorialPlaylist GetEditorialList(string listName)
        {
            return VideoCatalogue.GetEditorialList(listName);
        }

        public List<IClassificationItem> GetCatalogueClassification(ClassificationCriteria classificationCriteria)
        {
            return VideoCatalogue.GetCatalogueClassification(classificationCriteria);
        }

        public List<ICatalogueItem> GetVideoItemDetails(CatalogueItemIdentifier catalogueItemIdentifier, bool includeRelatedMedia)
        {
            return VideoCatalogue.GetVideoItemDetails(catalogueItemIdentifier, includeRelatedMedia);
        }

        public CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm)
        {
            return VideoCatalogue.SearchCatalogForSearchTerm(criteria, searchTerm);
        }

        public CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm, List<int> productIdList)
        {
            return VideoCatalogue.SearchCatalogForSearchTerm(criteria, searchTerm, productIdList);
        }

        public CatalogueResult SearchCatalogForRelated(CatalogueCriteria criteria, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList)
        {
            return VideoCatalogue.SearchCatalogForRelated(criteria, relatedVideoId, videoKeywords, excludedProgramId, genre, subGenre, productIdList);
        }

        #endregion
    }
}
