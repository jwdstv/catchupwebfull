﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Dstv.Online.VideoOnDemand.Modules.Rental;

namespace Dstv.Online.VideoOnDemand.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOnDemandRentalService" in both code and config file together.
    [ServiceContract]
    public interface IOnDemandRentalService
    {
        /// <summary>
        /// Gets the video rentals for the given customer identifier
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// <returns>List of VideoRentalType (not paged)</returns>
        [OperationContract]
        RentalResult GetVideoRentalsByUserId(string userId);

        /// <summary>
        /// Gets the video rentals for the given customer identifier
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// <param name="itemsPerPage">The number of items to return per page</param>
        /// <param name="currentPage">The zero-based page index</param>
        /// <returns>List of VideoRentalType (paged at index provided)</returns>
        [OperationContract]
        RentalResult GetVideoRentalsByUserId(string userId, int itemsPerPage, int currentPage);

        /// <summary>
        /// Gets the video rentals for the given customer identifier
        /// </summary>
        /// <param name="userId">Customer identifier</param>
        /// <param name="pagingCriteria">The paging criteria indicating paging requirements</param>
        /// <returns>List of VideoRentalType (paged at index provided)</returns>
        [OperationContract]
        RentalResult GetVideoRentalsByUserId(string userId, PagingCriteriaType pagingCriteria);

        /// <summary>
        /// Adds a video rental and does wallet transaction
        /// </summary>
        /// <param name="user">The RentalUser renting the video </param>
        /// <param name="videoRental">The VideoRental that is being rented</param>
        /// <returns>ResultType indicating success / (failure + reason) </returns>
        [OperationContract]
        ResultType AddVideoRentalByUser(RentalUser user, VideoRentalType videoRental);
    
    }
}
