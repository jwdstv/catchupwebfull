﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Common.Exceptions;
using Dstv.Online.VideoOnDemand.Data;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Modules.Rental;
using Dstv.Online.VideoOnDemand.Modules.VideoCatalogue;
using Dstv.Online.VideoOnDemand.Security;
using Dstv.Online.VideoOnDemand.Service;
using MultiChoice.DStvOnline.OnDemand.Service.Prepaid;

namespace MultiChoice.DStvOnline.OnDemand.Service
{
    public class OnDemandRentalService : IOnDemandRentalService
    {

        #region IOnDemandRentalService Members

        public RentalResult GetVideoRentalsByUserId(string userId)
        {
            Rental rental = new Rental();
            return rental.GetVideoRentalsByUserId(userId);
        }

        public RentalResult GetVideoRentalsByUserId(string userId, int itemsPerPage, int currentPage)
        {
            Rental rental = new Rental();
            return rental.GetVideoRentalsByUserId(userId,itemsPerPage, currentPage);
        }

        public RentalResult GetVideoRentalsByUserId(string userId, PagingCriteriaType pagingCriteria)
        {
            Rental rental = new Rental();
            return rental.GetVideoRentalsByUserId(userId, pagingCriteria);
        }

        public ResultType AddVideoRentalByUser(RentalUser user, VideoRentalType videoRental)
        {
            Rental rental = new Rental();
            return rental.AddVideoRentalByUser(user, videoRental);
        }


        public string AddPrepaidTransaction(string userId, string userPin, string catalogueItemId)
        {
            VideoRentalType video = new VideoRentalType();

            RentalUser user = ParseRentalUser(userId, userPin);

            VideoDetailResult videoItem = VideoCatalogue.GetVideoDetail(new CatalogueItemIdentifier(catalogueItemId));

            if (videoItem == null || videoItem.CatalogueItem == null)
                throw new VideoCatalogueItemNotFoundException(catalogueItemId);

            VideoRentalType videoRental = ParseCatalogueItemToVideoRental(videoItem);

            Rental rental = new Rental();

            ResultType result = rental.AddVideoRentalByUser(user, videoRental);

            return !result.Success ? result.ResultInfo : "Success";
        }

        private static RentalUser ParseRentalUser(string userId, string userPin)
        {
            RentalUser user = new RentalUser();

            IProfileInformation profile = IntegrationBroker.Instance.UserProfile.GetUserProfile(userId);

            user.UserEmail = profile.Email;
            user.UserFullName = profile.UserName;
            user.UserId = profile.CustomerId.ToString();
            user.UserPin = userPin;

            return user;
        }

        private static VideoRentalType ParseCatalogueItemToVideoRental(VideoDetailResult videoDetail)
        {
            VideoRentalType rental = new VideoRentalType();

            rental.Amount = videoDetail.CatalogueItem.RentalAmount.Value;
            rental.DateOfPurchase = DateTime.Now;
            rental.RentalDescription = videoDetail.CatalogueItem.Abstract;
            rental.RentalName = videoDetail.CatalogueItem.ProgramTitle;
            rental.VideoId = videoDetail.CatalogueItem.CatalogueItemId.Value;
            rental.VideoRentalPeriodInHours = videoDetail.CatalogueItem.RentalPeriodInHours;

            return rental;
        }


      


        #endregion
    }
}
