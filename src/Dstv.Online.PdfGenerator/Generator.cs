﻿
#region References

using System;
using System.Collections.Generic;
using System.Text;
using WebSupergoo.ABCpdf7;
using System.Reflection;
using System.ComponentModel;
using System.IO;

#endregion

namespace Dstv.Online.PdfGenerator
{
    /// <summary>
    /// Generates a pdf from html template
    /// </summary>
    public abstract class Generator : IDisposable
    {
        #region Private Members

        private const string BODY_RECT  = "10 10 595 770";
        private const string HEADER_COLOR = "4 66 128";
        private const string HEADER_FONT = "Arial";
        private const string HEADER_RECT = "10 650 595 780";

        #endregion

        #region ABCPdf Methods

        /// <summary>
        /// Generate from a raw html string. (Default timeout 5 min)
        /// </summary>
        /// <param name="Html"></param>
        /// <returns></returns>
        protected byte[] CreateFromRawHtml(string Html) {
            return CreateFromRawHtml(Html, 5);
        }

        /// <summary>
        /// Generate from a raw html string.
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="timeoutInMinutes"></param>
        /// <returns></returns>
        protected byte[] CreateFromRawHtml(string Html, int timeoutInMinutes)  {
            using (Doc _pdfDocument = new Doc())
            {
                _pdfDocument.Rect.String = "10 10 595 770";
                _pdfDocument.HtmlOptions.Timeout = timeoutInMinutes * 60 * 1000; // 5 minutes

                int _pdfDocumentId = _pdfDocument.AddImageHtml(Html);

                while (true)
                {
                    if (!_pdfDocument.Chainable(_pdfDocumentId))
                    {
                        break;
                    }

                    _pdfDocument.Page = _pdfDocument.AddPage();
                    _pdfDocumentId = _pdfDocument.AddImageToChain(_pdfDocumentId);
                }

                int _pageCount = _pdfDocument.PageCount;

                for (int i = 1; i <= _pageCount; i++)
                {
                    //_pdfDocument.FontSize = 8;
                    //_pdfDocument.AddFont("Arial");
                    _pdfDocument.PageNumber = i;
                    _pdfDocument.Color.String = "4 66 128";
                    _pdfDocument.Rect.String = "10 650 595 780";
                    _pdfDocument.HPos = 1.0;
                    _pdfDocument.AddText(string.Format("Page {0} of {1}", i, _pageCount));
                    _pdfDocument.Flatten();

                }

                return _pdfDocument.GetData();
            }
        }

        /// <summary>
        /// Generate from a raw html string and include header & footer html.
        /// </summary>
        /// <param name="bodyHtml"></param>
        /// <param name="headerHtml"></param>
        /// <param name="footerHtml"></param>
        /// <param name="timeoutInMinutes"></param>
        /// <returns></returns>
        protected byte[] CreateFromRawHtml(string bodyHtml, string headerHtml, string footerHtml, int timeoutInMinutes)
        {
            using (Doc _pdfDocument = new Doc())
            {
                _pdfDocument.Rect.String = "10 56 595 660";
                _pdfDocument.HtmlOptions.Timeout = timeoutInMinutes * 60 * 1000; // 5 minutes

                int _pdfDocumentId = _pdfDocument.AddImageHtml(bodyHtml);

                while (true)
                {
                    if (!_pdfDocument.Chainable(_pdfDocumentId))
                    {
                        break;
                    }

                    _pdfDocument.Page = _pdfDocument.AddPage();
                    _pdfDocumentId = _pdfDocument.AddImageToChain(_pdfDocumentId);
                }

                int _pageCount = _pdfDocument.PageCount;

                // Add header html.    
                _pdfDocument.Rect.String = "10 650 595 770";

                for (int i = 1; i <= _pageCount; i++)
                {
                    _pdfDocument.PageNumber = i;

                    // Header.

                    _pdfDocument.HPos = 0;
                    _pdfDocument.VPos = 0;
                    _pdfDocument.AddImageHtml(headerHtml);
                    _pdfDocument.Flatten();
                }

                // Add and footer html with page count.
                _pdfDocument.Rect.String = "10 10 595 55";

                for (int i = 1; i <= _pageCount; i++)
                {
                    _pdfDocument.PageNumber = i;
                    _pdfDocument.HPos = 0;
                    _pdfDocument.AddImageHtml(footerHtml.Replace("$PAGE_NUMBER$", i.ToString()).Replace("$PAGE_COUNT$", _pageCount.ToString()));
                    _pdfDocument.Flatten();
                }

                return _pdfDocument.GetData();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Replace all the slugs with the properties
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <param name="inputHtml"></param>
        /// <returns></returns>
        protected string MapData<T>(T input, string inputHtml)
        {
            PropertyInfo[] properties = input.GetType().GetProperties();

            foreach (PropertyInfo pi in properties)
            {
                object[] attributes = pi.GetCustomAttributes(true);

                foreach (object attribute in attributes)
                {
                    if (attribute is Slug)
                    {
                        // Get the slug from the attibutes
                        Slug slug = (Slug)attribute; 
                        
                        // Get the property value
                        object val = pi.GetValue(input, null); 
                        
                        // Convert the property value to a string
                        string valStr = (val is DateTime ? string.Format("{0:d}", (DateTime)val) : (string)val);      

                        // Replace the slug with the property value
                        inputHtml = inputHtml.Replace(string.Format("[{0}]", slug.Name), valStr);
                    }
                }
            }

            return inputHtml;
        }

        /// <summary>
        /// Generate the pdf document as a byte array
        /// </summary>
        /// <returns></returns>
        protected byte[] Generate<T>(string rawHtml, T inputParameters) {
            return CreateFromRawHtml(MapData<T>(inputParameters, rawHtml));
        }

        /// <summary>
        /// Generate and download the byte array as attachment
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rawHtml"></param>
        /// <param name="inputParameters"></param>
        /// <param name="fileName"></param>
        protected void Download<T>(string rawHtml, T inputParameters, string fileName) {
            Download(Generate(rawHtml, inputParameters), fileName);
        }

        /// <summary>
        /// Download the byte array as attachment
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="fileName"></param>
        protected void Download(byte[] buffer, string fileName)
        {
            System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.Web.HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length);
            System.Web.HttpContext.Current.Response.Flush();
        }

        #endregion

        public virtual void Dispose() {
        }
    }

    /// <summary>
    /// Provides the list of template slugs
    /// </summary>
    public abstract class GeneratorParameters
    {
        public GeneratorParameters()
        {
            // Set all the properties to their default values
            foreach (PropertyDescriptor p in TypeDescriptor.GetProperties(this))
            {
                DefaultValueAttribute a = p.Attributes[typeof(DefaultValueAttribute)] as DefaultValueAttribute;

                if (a == null)
                    continue;

                p.SetValue(this, a.Value);
            }
        }
    }
}
