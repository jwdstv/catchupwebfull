﻿
#region References

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#endregion

namespace Dstv.Online.PdfGenerator
{
    /// <summary>
    /// Provides the list of template slugs
    /// </summary>
    public class InvoiceParameters : GeneratorParameters
    {
        /// <summary>
        /// Customer's full name
        /// </summary>
        [Slug("CUST_NAME")]
        public string CustomerName { get; set; }

        /// <summary>
        /// Address line 1
        /// </summary>
        [Slug("CUST_ADDR1")]
        public string CustomerAddr1 { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        [Slug("CUST_ADDR2")]
        public string CustomerAddr2 { get; set; }

        /// <summary>
        /// Address line 3
        /// </summary>
        [Slug("CUST_ADDR3")]
        public string CustomerAddr3 { get; set; }

        /// <summary>
        /// Address line 4
        /// </summary>
        [Slug("CUST_ADDR4")]
        public string CustomerAddr4 { get; set; }

        /// <summary>
        /// Customer's VAT number
        /// </summary>
        [Slug("CUST_VAT_NUMBER")]
        public string CustomerVATNumber { get; set; }

        /// <summary>
        /// Customer's IBS number
        /// </summary>
        [Slug("CUST_NUMBER")]
        public string CustomerNumber { get; set; }

        /// <summary>
        /// The date of invoice
        /// </summary>
        [Slug("INVOICE_DATE")]
        public DateTime InvoiceDate { get; set; }

        /// <summary>
        /// Invoice period start
        /// </summary>
        [Slug("INVOICE_PERIOD_START")]
        public DateTime InvoicePeriodStart { get; set; }

        /// <summary>
        /// Invoice period end
        /// </summary>
        [Slug("INVOICE_PERIOD_END")]
        public DateTime InvoicePeriodEnd { get; set; }

        /// <summary>
        /// Detail (transaction) lines in html format
        /// </summary>
        [Slug("DETAIL_LINES")]
        public string DetailLines { get; set; }

        /// <summary>
        /// The document title
        /// </summary>
        [Slug("DOC_TITLE"), DefaultValue("TAX INVOICE")]
        public string DocumentTitle { get; set; }
    }
}
