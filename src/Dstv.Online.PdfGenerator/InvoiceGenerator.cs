﻿
#region References

using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.ComponentModel;
using Dstv.Online.PdfGenerator.Properties;
using multichoice.customercare.service.DataContracts;
using System.Net;

#endregion

namespace Dstv.Online.PdfGenerator
{
    /// <summary>
    /// Generates a customer invoice
    /// </summary>
    public class InvoiceGenerator : Generator
    {
        #region Private Members

        /// <summary>
        /// Customer IBS Identification Number (same as Connect ID)
        /// </summary>
        private uint _customerId;

        /// <summary>
        /// Customer’s IBS Account Number
        /// </summary>
        private uint _accountNumber;

        /// <summary>
        /// A list of IBS transaction codes to filter the document line items on
        /// </summary>
        private List<string> _transactionCodes = new List<string>();

        /// <summary>
        /// The start date (inclusive) of the invoice period for which to return transactions
        /// </summary>
        private DateTime _startDate;

        /// <summary>
        /// The end date (inclusive) of the invoice period for which to return transactions
        /// </summary>
        private DateTime _endDate;

        /// <summary>
        /// The invoice parameters (slugs) used to 
        /// construct the invoice
        /// </summary>
        private InvoiceParameters _parameters = new InvoiceParameters();

        /// <summary>
        /// The connect client proxy
        /// </summary>
        private CustomerServiceClient _clientProxy;

        /// <summary>
        /// The data source name
        /// </summary>
        private string _dataSource = "South_Africa_QA";

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for the invoice generator class
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="accountNumber"></param>
        /// <param name="transactionCodes"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public InvoiceGenerator
            (
                uint customerId,
                uint accountNumber, 
                List<string> transactionCodes, 
                DateTime startDate, 
                DateTime endDate
            )
        {
            _customerId       = customerId;
            _accountNumber    = accountNumber;
            _transactionCodes = transactionCodes;
            _startDate        = startDate;
            _endDate          = endDate;

            _clientProxy = new CustomerServiceClient();

            //WebRequest.DefaultWebProxy = new WebProxy("10.1.10.36", 8080);
            //WebRequest.DefaultWebProxy.Credentials = new NetworkCredential("dirkie.kunz", "", "africa");  
            
            GetData();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get all the information needed
        /// to generate the invoice
        /// </summary>
        private void GetData()
        {
            GetCustomerDetails();
            GetCustomerTransactions();
        }

        /// <summary>
        /// Get the customer's transactions
        /// that need to display on the invoice
        /// </summary>
        private void GetCustomerTransactions()
        {
            var transactions = _clientProxy.GetTransactionHistory
                (
                    _dataSource,
                    _customerId,
                    _accountNumber,
                    _startDate,
                    _endDate
                );

            if (transactions != null)
            {
                for (int i = 0; i < transactions.Length; i++)
                {
                    // Row opening tag
                    _parameters.DetailLines += "<tr>";

                    // Date
                    _parameters.DetailLines += string.Format("<td>{0:d}</td>", transactions[i].TimeStamp);

                    // Description
                    _parameters.DetailLines += string.Format("<td align=\"center\">{0}</td>", transactions[i].ProductDescription);

                    // Amount
                    _parameters.DetailLines += string.Format("<td align=\"right\">{0:C}</td>", transactions[i].Amount);

                    // Row closing tag
                    _parameters.DetailLines += "</tr>";
                }
            }
        }

        /// <summary>
        /// Get the customer details
        /// </summary>
        private void GetCustomerDetails()
        {
            // Get the customer object
            var customer = _clientProxy.GetByCustomerNumber(_dataSource, _customerId);

            if (customer != null)
            {
                // Customer details
                _parameters.CustomerName         = string.Format("{0} {1}", customer.Initials, customer.Surname);
                _parameters.CustomerVATNumber    = "1234";
                _parameters.CustomerNumber       = customer.Number.ToString();

                var addresses = _clientProxy.GetAddresses(_dataSource, _customerId);

                bool isFound = false;

                for (int i = 0; i < addresses.Length; i++) {
                    if (addresses[i].Type == AddressType.Postal) 
                    {
                        isFound = true;
                        SetAddressDetails(addresses[i]);
                    }
                }

                if (!isFound && addresses.Length > 0) {
                    SetAddressDetails(addresses[0]);
                }

                if (addresses.Length == 0)
                    throw new Exception("Customer address not found.");

                // Invoice details
                _parameters.InvoiceDate          = DateTime.Now;
                _parameters.InvoicePeriodStart   = _startDate;
                _parameters.InvoicePeriodEnd     = _endDate;
            }
            else
                throw new Exception("Customer not found.");
        }

        private void SetAddressDetails(Address _address)
        {
            // Customer address
            _parameters.CustomerAddr1 = _address.Street;
            _parameters.CustomerAddr2 = _address.SmallCity;
            _parameters.CustomerAddr3 = _address.BigCity;
            _parameters.CustomerAddr4 = _address.PostalCode;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Generate the pdf document as a byte array
        /// </summary>
        /// <returns></returns>
        public byte[] Generate()  {
            return Generate<InvoiceParameters>(Resources.Invoice, _parameters);
        }

        /// <summary>
        /// Download the pdf document as a file attachment
        /// </summary>
        /// <param name="_fileName"></param>
        public void Download(string _fileName)  {
            Download<InvoiceParameters>(Resources.Invoice, _parameters, _fileName);
        }

        #endregion

        #region Public Members

        /// <summary>
        /// The title displayed in the PDF document e.g. TAX Invoice, Statement etc
        /// </summary>
        public string DocumentTitle
        {
            get { return string.Format("{0} {1}_{2}",_parameters.DocumentTitle,
                _parameters.CustomerNumber, _endDate.ToString("yyyyMMdd"));  }
            set { _parameters.DocumentTitle = value; }
        }

        #endregion
    }
}
