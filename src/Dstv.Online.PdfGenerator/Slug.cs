﻿
using System;

namespace Dstv.Online.PdfGenerator
{
    [AttributeUsage(AttributeTargets.All)]
    public class Slug : System.Attribute
    {
        /// <summary>
        /// The target slug's name
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Attribute constructor
        /// </summary>
        /// <param name="_name">The name of the slug in the destination template</param>
        public Slug(string _name) {
            this.Name = _name;
        }
    }
}

