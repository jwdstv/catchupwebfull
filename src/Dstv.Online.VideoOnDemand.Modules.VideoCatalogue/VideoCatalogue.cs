﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand;
using Dstv.Online.VideoOnDemand.Content;
using Dstv.Online.VideoOnDemand.MediaCatalogue;
using Dstv.Online.VideoOnDemand.Transactions;
using Dstv.Online.VideoOnDemand.Integration.LuceneSearch;
using Dstv.Online.VideoOnDemand.Common;
using System.Web;

namespace Dstv.Online.VideoOnDemand.Modules.VideoCatalogue
{
    public class VideoCatalogue
	 {

		 #region public static List<ProgramChannel> GetChannelsByCatalogueId(CatalogueItemIdentifier catalogueItemId)
		 /// <summary>
        /// Returns the channels that broadcast the provided catalogueItem
        /// </summary>
        /// <param name="catalogueItemId">the id of the catalogue item</param>
        /// <returns>the channels broadcasting the catalogue item</returns>
        public static List<ProgramChannel> GetChannelsByCatalogueId(CatalogueItemIdentifier catalogueItemId)
        {
            IChannel[] channels = IntegrationBroker.Instance.Catalogue.GetChannels(catalogueItemId);

            return channels == null ? new List<ProgramChannel>() : channels.Select(chan => new ProgramChannel() {Id = chan.Id, Name = chan.Name, ImageFilename = chan.ImageFilename}).ToList();
        }
        #endregion

        #region public static CatalogueResult GetVideoItems ( CatalogueCriteria criteria )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public static CatalogueResult GetVideoItems(CatalogueCriteria criteria)
        {
            return IntegrationBroker.Instance.Catalogue.GetCatalogueItems(criteria);
        }
        #endregion

        #region public static EditorialPlaylist GetEditorialList ( string listId )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public static EditorialPlaylist GetEditorialList(string listId)
        {
            return GetEditorialList(listId, null);
        }
        #endregion

        #region public static EditorialPlaylist GetEditorialList ( string listId, CatalogueCriteria criteria )
        /// <summary>
        /// Retrieves the editorial list for the given listid and catalogue criteria
        /// </summary>
        /// <param name="listId">string, the list id to retrieve</param>
        /// <param name="criteria">CatalogueCriteria, the catalogue criteria</param>
        /// <returns></returns>
        public static EditorialPlaylist GetEditorialList(string listId, CatalogueCriteria criteria)
        {
            if (string.IsNullOrEmpty(listId))
                throw new ArgumentException("listId must contain the id of the list [GetEditorialList].");

            IPlaylist playList = IntegrationBroker.Instance.ContentManagement.GetPlaylist(new PlaylistIdentifier(listId));

            EditorialPlaylist editorialPlaylist = new EditorialPlaylist();

            if (criteria == null && playList != null)
            {
                criteria = new CatalogueCriteria { CatalogueItemIds = playList.CatalogueItemIds };
            }
            else
            {
                criteria.CatalogueItemIds = playList.CatalogueItemIds;
            }

            editorialPlaylist.catalogueItemSummaryList = GetVideoItems(criteria);
            editorialPlaylist.playlistName = playList.Name;

            return editorialPlaylist;
        }
        #endregion

        #region public static List<IClassificationItem> GetCatalogueClassification ( ClassificationCriteria classificationCriteria )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="classificationCriteria"></param>
        /// <returns></returns>
        public static List<IClassificationItem> GetCatalogueClassification(ClassificationCriteria classificationCriteria)
        {
            return IntegrationBroker.Instance.Catalogue.GetClassificationItems(classificationCriteria).ToList();
        }
        #endregion

        #region private static void GetCatalogueClassificationHierarchyForCriteria ( ClassificationCriteria classificationCriteria, ClassificationResultItem classificationResultItem )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="classificationCriteria"></param>
        /// <param name="classificationResultItem"></param>
        private static void GetCatalogueClassificationHierarchyForCriteria(ClassificationCriteria classificationCriteria, ClassificationResultItem classificationResultItem)
        {
            classificationCriteria.ClassificationItemId = classificationResultItem.ClassificationItem.ClassificationItemId;
            List<IClassificationItem> classificationItems = IntegrationBroker.Instance.Catalogue.GetClassificationItems(classificationCriteria).ToList();
            foreach (IClassificationItem classificationItem in classificationItems)
            {
                ClassificationResultItem classificationResultSubItem = new ClassificationResultItem(classificationItem);
                GetCatalogueClassificationHierarchyForCriteria(classificationCriteria, classificationResultSubItem);
                classificationResultItem.AddSubItem(classificationResultSubItem);
            }
        }
        #endregion

        #region public static ProgramResult GetProgramVideoDetail(CatalogueItemIdentifier catalogueItemIdentifier, VideoProgramType primaryVideoType)
        /// <summary>
        /// Returns a program for a given catalogueItem
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <param name="primaryVideoType"></param>
        /// <returns></returns>
        public static ProgramResult GetProgramVideoDetail(CatalogueItemIdentifier catalogueItemIdentifier, VideoProgramType primaryVideoType)
        {
            return GetProgramVideoDetail(catalogueItemIdentifier, primaryVideoType, false);
        }
        #endregion

        #region public static ProgramResult GetProgramVideoDetail(CatalogueItemIdentifier catalogueItemIdentifier, VideoProgramType primaryVideoType, bool isForthcoming)
        /// <summary>
        /// Returns a program for a given catalogueItem
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <param name="primaryVideoType"></param>
        /// <param name="isForthcoming"></param>
        /// <returns></returns>
        public static ProgramResult GetProgramVideoDetail(CatalogueItemIdentifier catalogueItemIdentifier, VideoProgramType primaryVideoType, bool isForthcoming)
        {
            ProgramResult programResult = new ProgramResult(catalogueItemIdentifier.Value, primaryVideoType);
            programResult.ReadProgram(isForthcoming);

            return programResult;
        }
        #endregion
        #region public static CatalogueDetailResult GetVideoDetail ( CatalogueItemIdentifier catalogueItemIdentifier )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <returns></returns>
        public static VideoDetailResult GetVideoDetail(CatalogueItemIdentifier catalogueItemIdentifier)
        {
            List<ICatalogueItem> videoItems = GetVideoItemDetails(catalogueItemIdentifier, false);
            if (videoItems == null && videoItems.Count == 0)
            {
                return new VideoDetailResult();
            }

            VideoDetailResult detailResult = new VideoDetailResult(videoItems[0]);

            // Get media.
            List<ICatalogueItemMedia> videoMedia = GetVideoMedia(catalogueItemIdentifier);
            detailResult.AddMediaFiles(videoMedia);

            // Get cast and crew.
            List<ICastMember> videoCastAndCrew = GetVideoCastAndCrew(catalogueItemIdentifier);
            detailResult.AddCastAndCrew(videoCastAndCrew);

            return detailResult;
        }
        #endregion

        #region public static List<ICatalogueItem> GetVideoItemDetails ( CatalogueItemIdentifier catalogueItemIdentifier, bool includeRelatedProgramMedia )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <returns></returns>
        public static List<ICatalogueItem> GetVideoItemDetails(CatalogueItemIdentifier catalogueItemIdentifier, bool includeRelatedProgramMedia)
        {
            CatalogueResult result = IntegrationBroker.Instance.Catalogue.GetCatalogueItem(catalogueItemIdentifier, includeRelatedProgramMedia);
            return result.CatalogueItems.ToList<ICatalogueItem>();
        }
        #endregion

        #region public static List<ICatalogueItem> GetVideoItemDetails ( CatalogueCriteria catalogueCriteria )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueCriteria"></param>
        /// <returns></returns>
        public static List<ICatalogueItem> GetVideoItemDetails(CatalogueCriteria catalogueCriteria)
        {
            CatalogueResult result = IntegrationBroker.Instance.Catalogue.GetCatalogueItems(catalogueCriteria);
            return result.CatalogueItems.ToList<ICatalogueItem>();
        }
        #endregion
        #region public static List<ICastMember> GetVideoCastAndCrew ( CatalogueItemIdentifier catalogueItemIdentifier )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <returns></returns>
        public static List<ICastMember> GetVideoCastAndCrew(CatalogueItemIdentifier catalogueItemIdentifier)
        {
            return IntegrationBroker.Instance.Catalogue.GetCatalogueItemCastAndCrew(catalogueItemIdentifier).ToList<ICastMember>();
        }
        #endregion

        #region public static ICatalogueItemMedia GetVideoMedia ( CatalogueItemIdentifier catalogueItemIdentifier, MediaTypeEnum mediaType )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        public static ICatalogueItemMedia GetVideoMedia(CatalogueItemIdentifier catalogueItemIdentifier, MediaTypeEnum mediaType)
        {
            return IntegrationBroker.Instance.Catalogue.GetCatalogueItemMedia(catalogueItemIdentifier, new MediaTypeIdentifier(mediaType.ToString("D")));
        }
        #endregion

        #region public static List<ICatalogueItemMedia> GetVideoMedia ( CatalogueItemIdentifier catalogueItemIdentifier )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogueItemIdentifier"></param>
        /// <returns></returns>
        public static List<ICatalogueItemMedia> GetVideoMedia(CatalogueItemIdentifier catalogueItemIdentifier)
        {
            List<ICatalogueItemMedia> videoMediaList = new List<ICatalogueItemMedia>();

            foreach (int mediaType in Enum.GetValues(typeof(MediaTypeEnum)))
            {
                if(Convert.ToInt32(MediaTypeEnum.Unknown) != mediaType)
                videoMediaList.Add(
                    IntegrationBroker.Instance.Catalogue.GetCatalogueItemMedia(catalogueItemIdentifier, 
                    new MediaTypeIdentifier(mediaType.ToString())));
            }

            return videoMediaList;
        }
        #endregion

        #region public static CatalogueResult SearchCatalogue ( CatalogueCriteria criteria, string searchTerm )
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public static CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm)
        {
            return SearchCatalogForSearchTerm(criteria, searchTerm, null);
        }
        #endregion


        #region public static CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm, List<int> productIdList)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="searchTerm"></param>
        /// <param name="productIdList"></param>
        /// <returns></returns>
        public static CatalogueResult SearchCatalogForSearchTerm(CatalogueCriteria criteria, string searchTerm, List<int> productIdList)
        {
            CatalogueSearchResult searchResult = null;

            try
            {
                CatalogueSearch CS = new CatalogueSearch();
                searchResult = CS.SearchCatalogForSearchTerm(criteria, searchTerm, productIdList);
            }
            catch (Exception ex)
            {
                Logger logger = new Logger();
                // Log exception
                string supportReferenceNumber = logger.LogException(
                    Config.EventLogSource.CatchUp,
                    ex,
                    HttpContext.Current.Request,
                    "VideoCatalogue.SearchCatalogForSearchTerm",
                    string.Empty,
                    string.Empty,
                    string.Empty
                    );
            }

            return GetVideoItemsFromSearchResult(searchResult, criteria);
        }

        #endregion

        #region public static CatalogueResult SearchCatalogForRelated(CatalogueCriteria criteria, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="relatedVideoId"></param>
        /// <param name="videoKeywords"></param>
        /// <param name="excludedProgramId"></param>
        /// <param name="genre"></param>
        /// <param name="subGenre"></param>
        /// <param name="productIdList"></param>
        /// <returns></returns>
        public static CatalogueResult SearchCatalogForRelated(CatalogueCriteria criteria, int relatedVideoId, string videoKeywords, int excludedProgramId, string genre, string subGenre, List<int> productIdList)
        {
            CatalogueSearchResult searchResult = null;

            try
            {
                CatalogueSearch CS = new CatalogueSearch();
                searchResult = CS.SearchCatalogForRelated(criteria, relatedVideoId, videoKeywords, excludedProgramId, genre, subGenre, productIdList);
            }
            catch (Exception ex)
            {
                Logger logger = new Logger();
                // Log exception
                string supportReferenceNumber = logger.LogException(
                    Config.EventLogSource.CatchUp,
                    ex,
                    HttpContext.Current.Request,
                    "VideoCatalogue.SearchCatalogForRelated",
                    string.Empty,
                    string.Empty,
                    string.Empty
                    );
            }

            return GetVideoItemsFromSearchResult(searchResult, criteria);

        }

        #endregion

        #region private static CatalogueResult GetVideoItemsFromSearchResult(CatalogueSearchResult searchResult, CatalogueCriteria criteria)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchResult"></param>
        /// <param name="criteria"></param>
        /// <returns></returns>
        private static CatalogueResult GetVideoItemsFromSearchResult(CatalogueSearchResult searchResult, CatalogueCriteria criteria)
        {
            if (searchResult == null || searchResult.CatalogueItemIds == null || searchResult.CatalogueItemIds.Length == 0 || searchResult.TotalResultCount == 0)
            {
                return new CatalogueResult(0, 0, new ICatalogueItem[] { });
            }

            if (criteria == null)
            {
                criteria = new CatalogueCriteria { CatalogueItemIds = searchResult.CatalogueItemIds };
            }
            else
            {
                criteria.CatalogueItemIds = searchResult.CatalogueItemIds;
            }

            return GetVideoItems(criteria);
        }

        #endregion

     }
}
