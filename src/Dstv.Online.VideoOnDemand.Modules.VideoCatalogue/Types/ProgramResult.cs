﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Modules.VideoCatalogue
{
    [Serializable]
	public class VideoDetailItem
	{

		#region CatalogueItem
		private ICatalogueItem _catalogueItem = null;
		/// <summary>
		/// 
		/// </summary>
		public ICatalogueItem CatalogueItem
		{
			get { return _catalogueItem; }
		}
		#endregion


		#region VideoId
		/// <summary>
		/// 
		/// </summary>
		public string VideoId
		{
			get { return _catalogueItem != null && _catalogueItem.CatalogueItemId != null ? _catalogueItem.CatalogueItemId.Value : ""; }
		}
		#endregion


		#region MediaFiles
		private List<ICatalogueItemMedia> _mediaFiles = new List<ICatalogueItemMedia>();
		/// <summary>
		/// 
		/// </summary>
		public List<ICatalogueItemMedia> MediaFiles
		{
			get { return _mediaFiles; }
			set { _mediaFiles = value; }
		}
		#endregion


		public VideoDetailItem ()
		{
		}

		public VideoDetailItem ( ICatalogueItem catalogueItem, List<ICatalogueItemMedia> mediaFiles )
		{
			_catalogueItem = catalogueItem;
			_mediaFiles = mediaFiles;
		}



		public bool ContainsMediaType ( MediaTypeEnum mediaType )
		{
			return GetMediaFile(mediaType) != null;
		}

		public ICatalogueItemMedia GetMediaFile ( MediaTypeEnum mediaType )
		{
			return _mediaFiles.Find(delegate( ICatalogueItemMedia mediaFile )
			{
				return mediaFile != null && mediaFile.MediaType.Value == ((int)mediaType).ToString();
			}
			);
		}

		public List<ICatalogueItemMedia> GetMediaFiles ( MediaTypeEnum[] mediaTypes )
		{
            List<ICatalogueItemMedia> files = new List<ICatalogueItemMedia>();

            if (_mediaFiles != null)
            {
                files = (from f in _mediaFiles
                         where f != null && f.MediaType != null && System.Text.RegularExpressions.Regex.IsMatch(f.MediaType.Value, @"^\d+$") && mediaTypes.Contains((MediaTypeEnum)int.Parse(f.MediaType.Value))
                         select f).ToList();
            }

            return files;
		}
	}

	[Serializable]
	public class ProgramChannel : IChannel
	{
		#region IChannel Members

		public ChannelItemIdentifier Id { get; set; }

		public string Name { get; set; }

		public string ImageFilename { get; set; }

		#endregion

    }

    [Serializable]
	public class ProgramResult
	{

		public int? ProgramId
		{
			get { return PrimaryVideoItem != null ? PrimaryVideoItem.CatalogueItem.ProgramId : -1; }
		}

		public string ProgramTitle
		{
			get { return PrimaryVideoItem != null ? PrimaryVideoItem.CatalogueItem.ProgramTitle : ""; }
		}


		#region VideoId
		private CatalogueItemIdentifier _videoId;
		/// <summary>
		/// 
		/// </summary>
		public string VideoId
		{
			get { return _videoId.Value; }
		}
		#endregion


		#region PrimaryVideoType
		private VideoProgramType _primaryVideoType = VideoProgramType.Unknown;
		/// <summary>
		/// 
		/// </summary>
		public VideoProgramType PrimaryVideoType
		{
			get { return _primaryVideoType; }
		}
		#endregion


		#region PrimaryVideoItem
		private VideoDetailItem _primaryVideoItem = null;
		/// <summary>
		/// 
		/// </summary>
		public VideoDetailItem PrimaryVideoItem
		{
			get
			{
				if (_primaryVideoItem == null && _videoItems.Count > 0)
				{
					_primaryVideoItem = _videoItems[0];
				}
				return _primaryVideoItem;
			}
		}
		#endregion


		#region VideoItems
		private List<VideoDetailItem> _videoItems = new List<VideoDetailItem>();
		/// <summary>
		/// 
		/// </summary>
		public List<VideoDetailItem> VideoItems
		{
			get { return _videoItems; }
		}
		#endregion


		#region CastAndCrew
		private List<ICastMember> _castAndCrew = new List<ICastMember>();
		/// <summary>
		/// 
		/// </summary>
		public List<ICastMember> CastAndCrew
		{
			get { return _castAndCrew; }
			set { _castAndCrew = value; }
		}
		#endregion


		#region Channels
		private int? _firstValidChannelIndex = null;
		private List<ProgramChannel> _channels = new List<ProgramChannel>();
		/// <summary>
		/// 
		/// </summary>
		public List<ProgramChannel> Channels
		{
			get { return _channels; }
			set { _channels = value; }
		}
		#endregion


		#region HasEpisodes
		private bool _hasEpisodes = false;
		/// <summary>
		/// 
		/// </summary>
		public bool HasEpisodes
		{
			get { return _hasEpisodes; }
		}
		#endregion


		public ProgramResult ( string videoId, VideoProgramType primaryVideoType )
		{
			_videoId = new CatalogueItemIdentifier(videoId);
			_primaryVideoType = primaryVideoType;
			// What is difference between Movie and FeatureFilm ??
			if (_primaryVideoType == VideoProgramType.FeatureFilm)
			{
				_primaryVideoType = VideoProgramType.Movie;
			}
		}


		public void SetPrimaryVideo ( string videoId )
		{
			foreach (VideoDetailItem videoDetailItem in _videoItems)
			{
				if (videoDetailItem.VideoId.Equals(videoId, StringComparison.OrdinalIgnoreCase))
				{
					_primaryVideoItem = videoDetailItem;
					break;
				}
			}
		}

        internal void ReadProgram()
        {
            ReadProgram(false);
        }

		internal void ReadProgram (bool isForthcoming)
		{
			_videoItems.Clear();
			_primaryVideoItem = null;

			// Video detail items with media files (items).
            List<ICatalogueItem> videoItems;

            if (isForthcoming)
            {
                CatalogueCriteria criteria = new CatalogueCriteria();
                CatalogueItemIdentifier[] id = {_videoId};

                criteria.SortOrder = SortOrders.ForthcomingReleases;
                criteria.CatalogueItemIds = id;
                criteria.Paging.PageNumber = 1;
                criteria.Paging.ItemsPerPage = 1;

                videoItems = VideoCatalogue.GetVideoItemDetails(criteria);
            }
            else
            {
                videoItems = VideoCatalogue.GetVideoItemDetails(_videoId, true);
            }

			foreach (ICatalogueItem videoItem in videoItems)
			{
				if (
					videoItem.ProductTypeId.Value != ProductTypes.CatchUp ||
					(videoItem.ProductTypeId.Value == ProductTypes.CatchUp && videoItem.StartDate.HasValue && videoItem.StartDate.Value <= DateTime.Now)
					)
				{
					List<ICatalogueItemMedia> videoItemMediaFiles = VideoCatalogue.GetVideoMedia(_videoId);
					videoItemMediaFiles.Remove(null);

					VideoDetailItem videoDetailItem = new VideoDetailItem(videoItem, videoItemMediaFiles);
					_videoItems.Add(videoDetailItem);

					// Check if primary video program type
					// What is difference between Movie and FeatureFilm ??
					if (videoItem.VideoProgramType == _primaryVideoType || (_primaryVideoType == VideoProgramType.Movie && videoItem.VideoProgramType == VideoProgramType.FeatureFilm))
					{
						_primaryVideoItem = videoDetailItem;
					}

					if (!_hasEpisodes && videoItem.VideoProgramType == VideoProgramType.ShowEpisode)
					{
						if (videoDetailItem.CatalogueItem.EpisodeNumber.HasValue)
						{
							_hasEpisodes = true;
						}
					}
				}
			}

			// Cast and Crew - common for program.
			if (PrimaryVideoItem != null)
			{
				_castAndCrew = VideoCatalogue.GetVideoCastAndCrew(PrimaryVideoItem.CatalogueItem.CatalogueItemId);
			}

			// Channel(s) - common for program.
			if (PrimaryVideoItem != null)
			{
				_channels = VideoCatalogue.GetChannelsByCatalogueId(PrimaryVideoItem.CatalogueItem.CatalogueItemId);
			}
		}


		public List<VideoDetailItem> GetVideoDetailItems ( VideoProgramType[] videoProgramTypes, MediaTypeEnum mediaType )
		{
			List<VideoDetailItem> videoDetailItems = new List<VideoDetailItem>();

			foreach (VideoDetailItem videoItem in _videoItems)
			{
				if (videoProgramTypes.Contains<VideoProgramType>(videoItem.CatalogueItem.VideoProgramType) && videoItem.ContainsMediaType(mediaType))
				{
					videoDetailItems.Add(videoItem);
				}
			}

			return videoDetailItems;
		}

		public ProgramChannel GetFirstValidChannel ()
		{
			if (_firstValidChannelIndex.HasValue)
			{
				return _firstValidChannelIndex.Value >= 0 ? _channels[_firstValidChannelIndex.Value] : null;
			}

			for (int i=0; i < _channels.Count; i++)
			{
				if (!string.IsNullOrEmpty(_channels[i].Name) && !string.IsNullOrEmpty(_channels[i].ImageFilename))
				{
					_firstValidChannelIndex = i;
				}
			}

			if (!_firstValidChannelIndex.HasValue)
			{
				_firstValidChannelIndex = -1;
			}

			return GetFirstValidChannel();
		}
	}

}
