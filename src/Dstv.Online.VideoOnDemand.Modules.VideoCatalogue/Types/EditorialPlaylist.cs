﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Modules.VideoCatalogue
{
    public class EditorialPlaylist
    {
        internal string playlistName;
        public string PlaylistName
        {
         get { return playlistName; }
        }

        internal CatalogueResult catalogueItemSummaryList;
        public CatalogueResult CatalogueItemSummaryList
        {
            get { return catalogueItemSummaryList; }
        }
    }
}
