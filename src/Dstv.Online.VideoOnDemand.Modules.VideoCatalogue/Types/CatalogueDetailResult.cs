﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Dstv.Online.VideoOnDemand.MediaCatalogue;

namespace Dstv.Online.VideoOnDemand.Modules.VideoCatalogue
{

	
	public class VideoDetailResult
	{

		#region CatalogueItem
		private ICatalogueItem _catalogueItem = null;
		/// <summary>
		/// 
		/// </summary>
		public ICatalogueItem CatalogueItem
		{
			get { return _catalogueItem; }
		}
		#endregion


		#region MediaFiles
		private List<ICatalogueItemMedia> _mediaFiles = new List<ICatalogueItemMedia>();
		/// <summary>
		/// 
		/// </summary>
		public List<ICatalogueItemMedia> MediaFiles
		{
			get { return _mediaFiles; }
			set { _mediaFiles = value; }
		}
		#endregion


		#region CastAndCrew
		private List<ICastMember> _castAndCrew = new List<ICastMember>();
		/// <summary>
		/// 
		/// </summary>
		public List<ICastMember> CastAndCrew
		{
			get { return _castAndCrew; }
			set { _castAndCrew = value; }
		}
		#endregion


		public VideoDetailResult ()
		{
		}

		public VideoDetailResult ( ICatalogueItem catalogueItem )
		{
			_catalogueItem = catalogueItem;
		}

		public void AddMediaFiles ( List<ICatalogueItemMedia> mediaFiles )
		{
			foreach (ICatalogueItemMedia mediaFile in mediaFiles)
			{
				_mediaFiles.Add(mediaFile);
			}
		}

		public void AddCastAndCrew ( List<ICastMember> castandCrew )
		{
			foreach (ICastMember castMember in castandCrew)
			{
				_castAndCrew.Add(castMember);
			}
		}

		public bool ContainsMediaType ( MediaTypeEnum mediaType )
		{
			foreach (ICatalogueItemMedia mediaFile in _mediaFiles)
			{
				if (mediaFile.MediaType.Value == mediaType.ToString())
				{
					return true;
				}
			}

			return false;
		}

		public ICatalogueItemMedia GetMediaFile ( MediaTypeEnum mediaType )
		{
			foreach (ICatalogueItemMedia mediaFile in _mediaFiles)
			{
				if (mediaFile.MediaType.Value == mediaType.ToString())
				{
					return mediaFile;
				}
			}

			return null;
		}
	}
}
